declare interface SANDBOX {
    [index: string | number | symbol]: any;

    /**
     * This hook is used to add default categories to spawnmenu tool tabs.
     */
    AddGamemodeToolMenuCategories(): void;

    /**
     * This hook is used to add default tool tabs to spawnmenu.
     */
    AddGamemodeToolMenuTabs(): void;

    /**
     * This hook is used to add new categories to spawnmenu tool tabs.
     */
    AddToolMenuCategories(): void;

    /**
     * This hook is used to add new tool tabs to spawnmenu.
     */
    AddToolMenuTabs(): void;

    /**
     * Called when a player attempts to "arm" a duplication with the Duplicator tool. Return false to prevent the player from sending data to server, and to ignore data if it was somehow sent anyway.
     *
     * @arg Player ply - The player who attempted to arm a dupe.
     * @return {boolean} Can the player arm a dupe or not.
     */
    CanArmDupe(ply: Player): boolean;

    /**
     * Called when a player attempts to drive a prop via Prop Drive
     *
     * @arg Player ply - The player who attempted to use Prop Drive.
     * @arg Entity ent - The entity the player is attempting to drive
     * @return {boolean} Return true to allow driving, false to disallow
     */
    CanDrive(ply: Player, ent: Entity): boolean;

    /**
 * Controls if a property can be used or not.
* 
* @arg Player ply - Player, that tried to use the property
* @arg string property - Class of the property that is tried to use, for example - bonemanipulate
This is not guaranteed to be the internal property name used in properties.Add!
* @arg Entity ent - The entity, on which property is tried to be used on
* @return {boolean} Return false to disallow using that property
 */
    CanProperty(ply: Player, property: string, ent: Entity): boolean;

    /**
     * Called when a player attempts to fire their tool gun. Return true to specifically allow the attempt, false to block it.
     *
     * @arg Player ply - The player who attempted to use their toolgun.
     * @arg TraceResultStruct tr - A trace from the players eye to where in the world their crosshair/cursor is pointing. See Structures/TraceResult
     * @arg string toolname - The tool mode the player currently has selected.
     * @arg object tool - The tool mode table the player currently has selected.
     * @arg number button - The tool button pressed.
     * @return {boolean} Can use toolgun or not.
     */
    CanTool(ply: Player, tr: TraceResultStruct, toolname: string, tool: object, button: number): boolean;

    /**
     * Called when player selects an item on the spawnmenu sidebar at the left.
     *
     * @arg Panel parent - The panel that holds spawnicons and the sidebar of spawnmenu
     * @arg Panel node - The item player selected
     */
    ContentSidebarSelection(parent: Panel, node: Panel): void;

    /**
     * Called when the context menu is supposedly closed.
     */
    ContextMenuClosed(): void;

    /**
     * Called when the context menu is created.
     *
     * @arg Panel g_ContextMenu - The created context menu panel
     */
    ContextMenuCreated(g_ContextMenu: Panel): void;

    /**
     * Allows to prevent the creation of the context menu. If the context menu is already created, this will have no effect.
     *
     * @return {boolean} Return false to prevent the context menu from being created.
     */
    ContextMenuEnabled(): boolean;

    /**
     * Called when the context menu is trying to be opened.
     *
     * @return {boolean} Return false to block the opening.
     */
    ContextMenuOpen(): boolean;

    /**
     * Called when the context menu is supposedly opened.
     */
    ContextMenuOpened(): void;

    /**
     * Called from GM:HUDPaint; does nothing by default.
     */
    PaintNotes(): void;

    /**
     * Called from GM:HUDPaint to draw world tips. By default, enabling cl_drawworldtooltips will stop world tips from being drawn here.
     * See AddWorldTip for more information.
     */
    PaintWorldTips(): void;

    /**
     * Called when persistent props are loaded.
     *
     * @arg string name - Save from which to load.
     */
    PersistenceLoad(name: string): void;

    /**
     * Called when persistent props are saved.
     *
     * @arg string name - Where to save. By default is convar "sbox_persist".
     */
    PersistenceSave(name: string): void;

    /**
     * Called when a player attempts to give themselves a weapon from the Q menu. ( Left mouse clicks on an icon )
     *
     * @arg Player ply - The player who attempted to give themselves a weapon.
     * @arg string weapon - Class name of the weapon the player tried to give themselves.
     * @arg SWEPStruct swep - The swep table of this weapon, see Structures/SWEP
     * @return {boolean} Can the SWEP be given to the player
     */
    PlayerGiveSWEP(ply: Player, weapon: string, swep: SWEPStruct): boolean;

    /**
     * Called after the player spawned an effect.
     *
     * @arg Player ply - The player that spawned the effect
     * @arg string model - The model of spawned effect
     * @arg Entity ent - The spawned effect itself
     */
    PlayerSpawnedEffect(ply: Player, model: string, ent: Entity): void;

    /**
     * Called after the player spawned an NPC.
     *
     * @arg Player ply - The player that spawned the NPC
     * @arg Entity ent - The spawned NPC itself
     */
    PlayerSpawnedNPC(ply: Player, ent: Entity): void;

    /**
     * Called when a player has successfully spawned a prop from the Q menu.
     *
     * @arg Player ply - The player who spawned a prop.
     * @arg string model - Path to the model of the prop the player is attempting to spawn.
     * @arg Entity entity - The entity that was spawned.
     */
    PlayerSpawnedProp(ply: Player, model: string, entity: Entity): void;

    /**
     * Called after the player spawned a ragdoll.
     *
     * @arg Player ply - The player that spawned the ragdoll
     * @arg string model - The ragdoll model that player wants to spawn
     * @arg Entity ent - The spawned ragdoll itself
     */
    PlayerSpawnedRagdoll(ply: Player, model: string, ent: Entity): void;

    /**
     * Called after the player has spawned a scripted entity.
     *
     * @arg Player ply - The player that spawned the SENT
     * @arg Entity ent - The spawned SENT
     */
    PlayerSpawnedSENT(ply: Player, ent: Entity): void;

    /**
     * Called after the player has spawned a scripted weapon from the spawnmenu with a middle mouse click.
     *
     * @arg Player ply - The player that spawned the SWEP
     * @arg Entity ent - The SWEP itself
     */
    PlayerSpawnedSWEP(ply: Player, ent: Entity): void;

    /**
     * Called after the player spawned a vehicle.
     *
     * @arg Player ply - The player that spawned the vehicle
     * @arg Entity ent - The vehicle itself
     */
    PlayerSpawnedVehicle(ply: Player, ent: Entity): void;

    /**
     * Called to ask if player allowed to spawn a particular effect or not.
     *
     * @arg Player ply - The player that wants to spawn an effect
     * @arg string model - The effect model that player wants to spawn
     * @return {boolean} Return false to disallow spawning that effect
     */
    PlayerSpawnEffect(ply: Player, model: string): boolean;

    /**
     * Called to ask if player allowed to spawn a particular NPC or not.
     *
     * @arg Player ply - The player that wants to spawn that NPC
     * @arg string npc_type - The npc type that player is trying to spawn
     * @arg string weapon - The weapon of that NPC
     * @return {boolean} Return false to disallow spawning that NPC
     */
    PlayerSpawnNPC(ply: Player, npc_type: string, weapon: string): boolean;

    /**
     * Called to ask whether player is allowed to spawn a given model. This includes props, effects, and ragdolls and is called before the respective PlayerSpawn* hook.
     *
     * @arg Player ply - The player in question
     * @arg string model - Model path
     * @arg number skin - Skin number
     * @return {boolean} Return false to disallow the player to spawn the given model.
     */
    PlayerSpawnObject(ply: Player, model: string, skin: number): boolean;

    /**
     * Called when a player attempts to spawn a prop from the Q menu.
     *
     * @arg Player ply - The player who attempted to spawn a prop.
     * @arg string model - Path to the model of the prop the player is attempting to spawn.
     * @return {boolean} Should the player be able to spawn the prop or not.
     */
    PlayerSpawnProp(ply: Player, model: string): boolean;

    /**
     * Called when a player attempts to spawn a ragdoll from the Q menu.
     *
     * @arg Player ply - The player who attempted to spawn a ragdoll.
     * @arg string model - Path to the model of the ragdoll the player is attempting to spawn.
     * @return {boolean} Should the player be able to spawn the ragdoll or not.
     */
    PlayerSpawnRagdoll(ply: Player, model: string): boolean;

    /**
     * Called when a player attempts to spawn an Entity from the Q menu.
     *
     * @arg Player ply - The player who attempted to spawn the entity.
     * @arg string _class - Class name of the entity the player tried to spawn.
     * @return {boolean} Should the player be able to spawn the entity or not.
     */
    PlayerSpawnSENT(ply: Player, _class: string): boolean;

    /**
     * Called when a player attempts to spawn a weapon from the Q menu. ( Mouse wheel clicks on an icon )
     *
     * @arg Player ply - The player who attempted to spawn a weapon.
     * @arg string weapon - Class name of the weapon the player tried to spawn.
     * @arg SWEPStruct swep - Information about the weapon the player is trying to spawn, see Structures/SWEP
     * @return {boolean} Can the SWEP be spawned
     */
    PlayerSpawnSWEP(ply: Player, weapon: string, swep: SWEPStruct): boolean;

    /**
     * Called to ask if player allowed to spawn a particular vehicle or not.
     *
     * @arg Player ply - The player that wants to spawn that vehicle
     * @arg string model - The vehicle model that player wants to spawn
     * @arg string name - Vehicle name
     * @arg object table - Table of that vehicle, containing info about it
     * @return {boolean} Return false to disallow spawning that vehicle
     */
    PlayerSpawnVehicle(ply: Player, model: string, name: string, table: object): boolean;

    /**
     * This hook makes the engine load the spawnlist text files.
     * It calls spawnmenu.PopulateFromEngineTextFiles by default.
     */
    PopulatePropMenu(): void;

    /**
     * Called to populate the Scripted Tool menu.
     */
    PopulateSTOOLMenu(): void;

    /**
     * Add the STOOLS to the tool menu. You want to call spawnmenu.AddToolMenuOption in this hook.
     */
    PopulateToolMenu(): void;

    /**
     * Called right after the Lua Loaded tool menus are reloaded. This is a good place to set up any ControlPanels.
     */
    PostReloadToolsMenu(): void;

    /**
     * Called right before the Lua Loaded tool menus are reloaded.
     */
    PreReloadToolsMenu(): void;

    /**
     * Called when there's one or more items selected in the spawnmenu by the player, to open the multi selection right click menu (DMenu)
     *
     * @arg Panel canvas - The canvas that has the selection. (SANDBOX:SpawnlistOpenGenericMenu)
     */
    SpawnlistOpenGenericMenu(canvas: Panel): void;

    /**
     * If false is returned then the spawn menu is never created. This saves load times if your mod doesn't actually use the spawn menu for any reason.
     *
     * @return {boolean} Whether to create spawnmenu or not.
     */
    SpawnMenuEnabled(): boolean;

    /**
     * Called when spawnmenu is trying to be opened.
     *
     * @return {boolean} Return false to dissallow opening the spawnmenu
     */
    SpawnMenuOpen(): boolean;
}

declare type SANDBOX__HOOKS = keyof SANDBOX;
