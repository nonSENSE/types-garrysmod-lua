declare interface TOOL {
    [index: string | number | symbol]: any;

    /**
     * Called when WEAPON:Deploy of the toolgun is called.
     */
    Deploy(): void;

    /**
     * Called when WEAPON:DrawHUD of the toolgun is called, only when the user has this tool selected.
     */
    DrawHUD(): void;

    /**
     * Called after the default tool screen has been drawn from WEAPON:RenderScreen.
     *
     * @arg number width - The width of the tool's screen in pixels.
     * @arg number height - The height of the tool's screen in pixels.
     */
    DrawToolScreen(width: number, height: number): void;

    /**
     * Called when WEAPON:Think of the toolgun is called, only when the user has this tool selected.
     *
     * @return {boolean} Return true to freeze the player
     */
    FreezeMovement(): boolean;

    /**
     * Called when WEAPON:Holster of the toolgun is called, as well as when switching between different toolguns.
     */
    Holster(): void;

    /**
     * Called when the user left clicks with the tool.
     *
     * @arg TraceResultStruct tr - A trace from user's eyes to wherever they aim at. See Structures/TraceResult
     * @return {boolean} Return true to draw the tool gun beam and play fire animations, false otherwise.
     */
    LeftClick(tr: TraceResultStruct): boolean;

    /**
     * Called when the user presses the reload key with the tool out.
     *
     * @arg TraceResultStruct tr - A trace from user's eyes to wherever they aim at. See Structures/TraceResult
     * @return {boolean} Return true to draw the tool gun beam and play fire animations, false otherwise
     */
    Reload(tr: TraceResultStruct): boolean;

    /**
     * Called when the user right clicks with the tool.
     *
     * @arg TraceResultStruct tr - A trace from user's eyes to wherever they aim at. See Structures/TraceResult
     * @return {boolean} Return true to draw the tool gun beam and play fire animations, false otherwise
     */
    RightClick(tr: TraceResultStruct): boolean;

    /**
     * Called when WEAPON:Think of the toolgun is called. This only happens when the tool gun is currently equipped/selected by the player and the selected tool is this tool.
     */
    Think(): void;
}

declare type TOOL__HOOKS = keyof TOOL;
