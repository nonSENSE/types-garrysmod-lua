declare interface WEAPON {
    [index: string | number | symbol]: any;

    /**
     * Called when another entity fires an event to this entity.
     *
     * @arg string inputName - The name of the input that was triggered.
     * @arg Entity activator - The initial cause for the input getting triggered.
     * @arg Entity called - The entity that directly trigger the input.
     * @arg string data - The data passed.
     * @return {boolean} Should we suppress the default action for this input?
     */
    AcceptInput(inputName: string, activator: Entity, called: Entity, data: string): boolean;

    /**
     * Allows you to adjust the mouse sensitivity. This hook only works if you haven't overridden GM:AdjustMouseSensitivity.
     *
     * @return {number} Sensitivity scale
     */
    AdjustMouseSensitivity(): number;

    /**
     * Returns how much of primary ammo the player has.
     *
     * @return {number} The amount of primary ammo player has
     */
    Ammo1(): number;

    /**
     * Returns how much of secondary ammo the player has.
     *
     * @return {number} The amount of secondary ammo player has
     */
    Ammo2(): number;

    /**
     * Allows you to adjust player view while this weapon in use.
     *
     * @arg Player ply - The owner of weapon
     * @arg Vector pos - Current position of players view
     * @arg Angle ang - Current angles of players view
     * @arg number fov - Current FOV of players view
     * @return {Vector} New position of players view
     * @return {Angle} New angle of players view
     * @return {number} New FOV of players view
     */
    CalcView(ply: Player, pos: Vector, ang: Angle, fov: number): LuaMultiReturn<[Vector, Angle, number]>;

    /**
     * Allows overriding the position and angle of the viewmodel. This hook only works if you haven't overridden GM:CalcViewModelView.
     *
     * @arg Entity ViewModel - The viewmodel entity
     * @arg Vector OldEyePos - Original position (before viewmodel bobbing and swaying)
     * @arg Angle OldEyeAng - Original angle (before viewmodel bobbing and swaying)
     * @arg Vector EyePos - Current position
     * @arg Angle EyeAng - Current angle
     * @return {Vector} New position
     * @return {Angle} New angle
     */
    CalcViewModelView(
        ViewModel: Entity,
        OldEyePos: Vector,
        OldEyeAng: Angle,
        EyePos: Vector,
        EyeAng: Angle
    ): LuaMultiReturn<[Vector, Angle]>;

    /**
     * Called when a Citizen NPC is looking around to a (better) weapon to pickup.
     *
     * @return {boolean} Return true to allow this weapon to be picked up by NPCs.
     */
    CanBePickedUpByNPCs(): boolean;

    /**
     * Helper function for checking for no ammo.
     *
     * @return {boolean} Can use primary attack
     */
    CanPrimaryAttack(): boolean;

    /**
     * Helper function for checking for no ammo.
     *
     * @return {boolean} Can use secondary attack
     */
    CanSecondaryAttack(): boolean;

    /**
 * Allows you to use any numbers you want for the ammo display on the HUD.
* 
* @return {boolean} The new ammo display settings. A table with 4 possible keys:

boolean Draw - Whether to draw the ammo display or not
number PrimaryClip - Amount of primary ammo in the clip
number PrimaryAmmo - Amount of primary ammo in the reserves
number SecondaryAmmo - Amount of secondary ammo. It is shown like alt-fire for SMG1 and AR2 are shown.

There is no SecondaryClip!
 */
    CustomAmmoDisplay(): boolean;

    /**
     * Called when player has just switched to this weapon.
     *
     * @return {boolean} Return true to allow switching away from this weapon using lastinv command
     */
    Deploy(): boolean;

    /**
     * Called when the crosshair is about to get drawn, and allows you to override it.
     *
     * @arg number x - X coordinate of the crosshair.
     * @arg number y - Y coordinate of the crosshair.
     * @return {boolean} Return true to override the default crosshair.
     */
    DoDrawCrosshair(x: number, y: number): boolean;

    /**
     * Called so the weapon can override the impact effects it makes.
     *
     * @arg TraceResultStruct tr - A Structures/TraceResult from player's eyes to the impact point
     * @arg number damageType - The damage type of bullet. See Enums/DMG
     * @return {boolean} Return true to not do the default thing - which is to call UTIL_ImpactTrace in C++
     */
    DoImpactEffect(tr: TraceResultStruct, damageType: number): boolean;

    /**
     * This hook allows you to draw on screen while this weapon is in use.
     */
    DrawHUD(): void;

    /**
     * This hook allows you to draw on screen while this weapon is in use. This hook is called before WEAPON:DrawHUD and is equivalent of GM:HUDPaintBackground.
     */
    DrawHUDBackground(): void;

    /**
     * This hook draws the selection icon in the weapon selection menu.
     *
     * @arg number x - X coordinate of the selection panel
     * @arg number y - Y coordinate of the selection panel
     * @arg number width - Width of the selection panel
     * @arg number height - Height of the selection panel
     * @arg number alpha - Alpha value of the selection panel
     */
    DrawWeaponSelection(x: number, y: number, width: number, height: number, alpha: number): void;

    /**
     * Called when we are about to draw the world model.
     *
     * @arg number flags - The STUDIO_ flags for this render operation.
     */
    DrawWorldModel(flags: number): void;

    /**
     * Called when we are about to draw the translucent world model.
     *
     * @arg number flags - The STUDIO_ flags for this render operation.
     */
    DrawWorldModelTranslucent(flags: number): void;

    /**
     * Called when a player or NPC has picked the weapon up.
     *
     * @arg Entity NewOwner - The one who picked the weapon up. Can be Player or NPC.
     */
    Equip(NewOwner: Entity): void;

    /**
     * The player has picked up the weapon and has taken the ammo from it.
     * The weapon will be removed immidiately after this call.
     *
     * @arg Player ply - The player who picked up the weapon
     */
    EquipAmmo(ply: Player): void;

    /**
     * Called before firing animation events, such as muzzle flashes or shell ejections.
     *
     * @arg Vector pos - Position of the effect.
     * @arg Angle ang - Angle of the effect.
     * @arg number event - The event ID of happened even. See this page.
     * @arg string options - Name or options of the event.
     * @arg Entity source - The source entity. This will be a viewmodel on the client and the weapon itself on the server
     * @return {boolean} Return true to disable the effect.
     */
    FireAnimationEvent(pos: Vector, ang: Angle, event: number, options: string, source: Entity): boolean;

    /**
     * This hook allows you to freeze players screen.
     *
     * @return {boolean} Return true to freeze moving the view
     */
    FreezeMovement(): boolean;

    /**
     * This hook is for NPCs, you return what they should try to do with it.
     *
     * @return {number} A number defining what NPC should do with the weapon. Use the Enums/CAP.
     */
    GetCapabilities(): number;

    /**
     * Called when the weapon is used by NPCs to determine how accurate the bullets fired should be.
     *
     * @arg number proficiency - How proficient the NPC is with this gun. See Enums/WEAPON_PROFICIENCY
     * @return {number} An amount of degrees the bullets should deviate from the NPC's NPC:GetAimVector. Default is 15.
     */
    GetNPCBulletSpread(proficiency: number): number;

    /**
     * Called when the weapon is used by NPCs to tell the NPC how to use this weapon. Controls how long the NPC can or should shoot continuously.
     *
     * @return {number} Minimum amount of bullets per burst. Default is 1.
     * @return {number} Maximum amount of bullets per burst. Default is 1.
     * @return {number} Delay between each shot, aka firerate. Default is 1.
     */
    GetNPCBurstSettings(): LuaMultiReturn<[number, number, number]>;

    /**
     * Called when the weapon is used by NPCs to tell the NPC how to use this weapon. Controls amount of time the NPC can rest (not shoot) between bursts.
     *
     * @return {number} Minimum amount of time the NPC can rest (not shoot) between bursts in seconds. Default is 0.3 seconds.
     * @return {number} Maximum amount of time the NPC can rest (not shoot) between bursts in seconds. Default is 0.66 seconds.
     */
    GetNPCRestTimes(): LuaMultiReturn<[number, number]>;

    /**
     * Allows you to override where the tracer effect comes from. ( Visual bullets )
     *
     * @return {Vector} The new position to start tracer effect from
     */
    GetTracerOrigin(): Vector;

    /**
     * This hook allows you to adjust view model position and angles.
     *
     * @arg Vector EyePos - Current position
     * @arg Angle EyeAng - Current angle
     * @return {Vector} New position
     * @return {Angle} New angle
     */
    GetViewModelPosition(EyePos: Vector, EyeAng: Angle): LuaMultiReturn<[Vector, Angle]>;

    /**
 * Called when weapon tries to holster.
* 
* @arg Entity weapon - The weapon we are trying switch to.
* @return {boolean} Return true to allow weapon to holster.
This will not have an effect if weapon was switched away from using Player:SetActiveWeapon
 */
    Holster(weapon: Entity): boolean;

    /**
     * This hook determines which parts of the HUD to draw.
     *
     * @arg string element - The HUD element in question
     * @return {boolean} Return false to hide this HUD element
     */
    HUDShouldDraw(element: string): boolean;

    /**
     * Called when the weapon entity is created.
     */
    Initialize(): void;

    /**
     * Called when the engine sets a value for this scripted weapon.
     *
     * @arg string key - The key that was affected.
     * @arg string value - The new value.
     * @return {boolean} Return true to suppress this KeyValue or return false or nothing to apply this key value.
     */
    KeyValue(key: string, value: string): boolean;

    /**
     * Called when weapon is dropped by Player:DropWeapon.
     */
    OnDrop(): void;

    /**
     * Called whenever the weapons Lua script is reloaded.
     */
    OnReloaded(): void;

    /**
     * Called when the swep is about to be removed.
     */
    OnRemove(): void;

    /**
     * Called when the weapon entity is reloaded from a Source Engine save (not the Sandbox saves or dupes) or on a changelevel (for example Half-Life 2 campaign level transitions).
     */
    OnRestore(): void;

    /**
     * Called when weapon is dropped or picked up by a new player.
     */
    OwnerChanged(): void;

    /**
     * Called after the view model has been drawn while the weapon in use. This hook is called from the default implementation of GM:PostDrawViewModel, and as such, will not occur if it has been overridden.
     *
     * @arg Entity vm - This is the view model entity after it is drawn
     * @arg Weapon weapon - This is the weapon that is from the view model (same as self)
     * @arg Player ply - The owner of the view model
     */
    PostDrawViewModel(vm: Entity, weapon: Weapon, ply: Player): void;

    /**
     * Allows you to modify viewmodel while the weapon in use before it is drawn. This hook only works if you haven't overridden GM:PreDrawViewModel.
     *
     * @arg Entity vm - This is the view model entity before it is drawn.
     * @arg Weapon weapon - This is the weapon that is from the view model.
     * @arg Player ply - The the owner of the view model.
     */
    PreDrawViewModel(vm: Entity, weapon: Weapon, ply: Player): void;

    /**
     * Called when primary attack button ( +attack ) is pressed.
     */
    PrimaryAttack(): void;

    /**
     * A convenience function that draws the weapon info box, used in WEAPON:DrawWeaponSelection.
     *
     * @arg number x - The x co-ordinate of box position
     * @arg number y - The y co-ordinate of box position
     * @arg number alpha - Alpha value for the box
     */
    PrintWeaponInfo(x: number, y: number, alpha: number): void;

    /**
     * Called when the reload key ( +reload ) is pressed.
     */
    Reload(): void;

    /**
     * Called every frame just before GM:RenderScene.
     */
    RenderScreen(): void;

    /**
     * Called when secondary attack button ( +attack2 ) is pressed.
     */
    SecondaryAttack(): void;

    /**
     * Sets the weapon deploy speed. This value needs to match on client and server.
     *
     * @arg number speed - The value to set deploy speed to. Negative will slow down playback.
     */
    SetDeploySpeed(speed: number): void;

    /**
     * Called when the SWEP should set up its  Data Tables.
     */
    SetupDataTables(): void;

    /**
     * Sets the hold type of the weapon. This must be called on both the server and the client to work properly.
     *
     * @arg string name - Name of the hold type. You can find all default hold types here
     */
    SetWeaponHoldType(name: string): void;

    /**
     * A convenient function to shoot bullets.
     *
     * @arg number damage - The damage of the bullet
     * @arg number num_bullets - Amount of bullets to shoot
     * @arg number aimcone - Spread of bullets
     * @arg string [ammo_type="self.Primary.Ammo"] - Ammo type of the bullets
     * @arg number [force=1] - Force of the bullets
     * @arg number [tracer=5] - Show a tracer on every x bullets
     */
    ShootBullet(
        damage: number,
        num_bullets: number,
        aimcone: number,
        ammo_type?: string,
        force?: number,
        tracer?: number
    ): void;

    /**
     * A convenience function to create shoot effects.
     */
    ShootEffects(): void;

    /**
     * Called to determine if the view model should be drawn or not.
     *
     * @return {boolean} Return true to draw the view model, false otherwise.
     */
    ShouldDrawViewModel(): boolean;

    /**
     * Should this weapon be dropped when its owner dies?
     *
     * @return {boolean} Return true to drop the weapon, false otherwise. Default ( if you don't return anything ) is false.
     */
    ShouldDropOnDie(): boolean;

    /**
     * A convenience function to remove primary ammo from clip.
     *
     * @arg number amount - Amount of primary ammo to remove
     */
    TakePrimaryAmmo(amount: number): void;

    /**
     * A convenience function to remove secondary ammo from clip.
     *
     * @arg number amount - How much of secondary ammo to remove
     */
    TakeSecondaryAmmo(amount: number): void;

    /**
     * Called when the swep thinks.
     */
    Think(): void;

    /**
     * Translate a player's Activity into a weapon's activity, depending on how you want the player to be holding the weapon.
     *
     * @arg number act - The activity to translate
     * @return {number} The translated activity
     */
    TranslateActivity(act: number): number;

    /**
     * Allows to change players field of view while player holds the weapon.
     *
     * @arg number fov - The current/default FOV.
     * @return {number} The target FOV.
     */
    TranslateFOV(fov: number): number;

    /**
     * Called straight after the view model has been drawn. This is called before GM:PostDrawViewModel and WEAPON:PostDrawViewModel.
     *
     * @arg Entity ViewModel - Players view model
     */
    ViewModelDrawn(ViewModel: Entity): void;
}

declare type WEAPON__HOOKS = keyof WEAPON;

declare type SWEP = WEAPON;
