declare interface NEXTBOT {
    [index: string | number | symbol]: any;

    /**
     * Called to initialize the behaviour.
     */
    BehaveStart(): void;

    /**
     * Called to update the bot's behaviour.
     *
     * @arg number interval - How long since the last update
     */
    BehaveUpdate(interval: number): void;

    /**
     * Called to update the bot's animation.
     */
    BodyUpdate(): void;

    /**
     * Called when the nextbot touches another entity.
     *
     * @arg Entity ent - The entity the nextbot came in contact with.
     */
    OnContact(ent: Entity): void;

    /**
     * Called when the nextbot NPC sees another Nextbot NPC or a Player.
     *
     * @arg Entity ent - the entity that was seen
     */
    OnEntitySight(ent: Entity): void;

    /**
     * Called when the nextbot NPC loses sight of another Nextbot NPC or a Player.
     *
     * @arg Entity ent - the entity that we lost sight of
     */
    OnEntitySightLost(ent: Entity): void;

    /**
     * Called when the bot is ignited.
     */
    OnIgnite(): void;

    /**
     * Called when the bot gets hurt. This is a good place to play hurt sounds or voice lines.
     *
     * @arg CTakeDamageInfo info - The damage info
     */
    OnInjured(info: CTakeDamageInfo): void;

    /**
     * Called when the bot gets killed.
     *
     * @arg CTakeDamageInfo info - The damage info
     */
    OnKilled(info: CTakeDamageInfo): void;

    /**
     * Called when the bot's feet return to the ground.
     *
     * @arg Entity ent - The entity the nextbot has landed on.
     */
    OnLandOnGround(ent: Entity): void;

    /**
     * Called when the bot's feet leave the ground - for whatever reason.
     *
     * @arg Entity ent - The entity the bot "jumped" from.
     */
    OnLeaveGround(ent: Entity): void;

    /**
     * Called when the nextbot enters a new navigation area.
     *
     * @arg CNavArea old - The navigation area the bot just left
     * @arg CNavArea _new - The navigation area the bot just entered
     */
    OnNavAreaChanged(old: CNavArea, _new: CNavArea): void;

    /**
     * Called when someone else or something else has been killed.
     *
     * @arg Entity victim - The victim that was killed
     * @arg CTakeDamageInfo info - The damage info
     */
    OnOtherKilled(victim: Entity, info: CTakeDamageInfo): void;

    /**
     * Called when the bot thinks it is stuck.
     */
    OnStuck(): void;

    /**
     * Called when a trace attack is done against the nextbot, allowing override of the damage being dealt by altering the CTakeDamageInfo.
     *
     * @arg CTakeDamageInfo info - The damage info
     * @arg Vector dir - The direction the damage goes in
     * @arg TraceResultStruct trace - The Structures/TraceResult of the attack, containing the hitgroup.
     */
    OnTraceAttack(info: CTakeDamageInfo, dir: Vector, trace: TraceResultStruct): void;

    /**
     * Called when the bot thinks it is un-stuck.
     */
    OnUnStuck(): void;

    /**
     * A hook called to process nextbot logic.
     */
    RunBehaviour(): void;
}

declare type NEXTBOT__HOOKS = keyof NEXTBOT;
