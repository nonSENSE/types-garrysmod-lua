declare interface EFFECT {
    [index: string | number | symbol]: any;

    /**
     *
     */
    EndTouch(): void;

    /**
 * Used to get the "real" start position of a trace, for weapon tracer effects.
* 
* @arg Vector pos - Default position if we fail
* @arg Weapon ent - The weapon to use.
* @arg number attachment - Attachment ID of on the weapon "muzzle", to use as the start position.
Please note that it is expected that the same attachment ID is used on both, the world and the view model.
* @return {Vector} The "real" start position.
 */
    GetTracerShootPos(pos: Vector, ent: Weapon, attachment: number): Vector;

    /**
     * Called when the effect is created.
     *
     * @arg CEffectData effectData - The effect data used to create the effect.
     */
    Init(effectData: CEffectData): void;

    /**
     * Called when the effect collides with anything.
     *
     * @arg CollisionDataStruct colData - Information regarding the collision. See Structures/CollisionData
     * @arg PhysObj collider - The physics object of the entity that collided with the effect.
     */
    PhysicsCollide(colData: CollisionDataStruct, collider: PhysObj): void;

    /**
     * Called when the effect should be rendered.
     */
    Render(): void;

    /**
     *
     */
    StartTouch(): void;

    /**
     * Called when the effect should think, return false to kill the effect.
     *
     * @return {boolean} Return false to remove this effect.
     */
    Think(): boolean;

    /**
     *
     */
    Touch(): void;
}

declare type EFFECT__HOOKS = keyof EFFECT;
