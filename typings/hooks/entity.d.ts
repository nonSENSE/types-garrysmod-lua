declare interface ENTITY {
    [index: string | number | symbol]: any;

    /**
     * Called when another entity fires an event to this entity.
     *
     * @arg string inputName - The name of the input that was triggered.
     * @arg Entity activator - The initial cause for the input getting triggered. (EG the player who pushed a button)
     * @arg Entity caller - The entity that directly triggered the input. (EG the button that was pushed)
     * @arg string data - The data passed.
     * @return {boolean} Should we suppress the default action for this input?
     */
    AcceptInput(inputName: string, activator: Entity, caller: Entity, data: string): boolean;

    /**
     * A helper function for creating Scripted Entities.
     *
     * @arg string name - The input name from ENTITY:AcceptInput.
     * @arg string data - The input data from ENTITY:AcceptInput.
     * @return {boolean} Whether any outputs were added or not.
     */
    AddOutputFromAcceptInput(name: string, data: string): boolean;

    /**
     * A helper function for creating Scripted Entities.
     *
     * @arg string key - The key-value key.
     * @arg string value - The key-value value.
     * @return {boolean} Whether any outputs were added or not.
     */
    AddOutputFromKeyValue(key: string, value: string): boolean;

    /**
     * Called whenever the entity's position changes. A callback for when an entity's angle changes is available via Entity:AddCallback.
     *
     * @arg Vector pos - The entity's actual position. May differ from Entity:GetPos
     * @arg Angle ang - The entity's actual angles. May differ from Entity:GetAngles
     * @return {Vector} New position
     * @return {Angle} New angles
     */
    CalcAbsolutePosition(pos: Vector, ang: Angle): LuaMultiReturn<[Vector, Angle]>;

    /**
 * Controls if a property can be used on this entity or not.
* 
* @arg Player ply - Player, that tried to use the property
* @arg string property - Class of the property that is tried to use, for example - bonemanipulate
* @return {boolean} Return false to disallow using that property, return true to allow.
You must return a value. Not returning anything can cause unexpected results.
 */
    CanProperty(ply: Player, property: string): boolean;

    /**
     * Controls if a tool can be used on this entity or not.
     *
     * @arg Player ply - Player, that tried to use the tool
     * @arg TraceResultStruct tr - The trace of the tool. See TraceResult.
     * @arg string toolname - Class of the tool that is tried to use, for example - weld
     * @arg object tool - The tool mode table the player currently has selected.
     * @arg number button - The tool button pressed.
     * @return {boolean} Return false to disallow using that tool on this entity, return true to allow.
     */
    CanTool(ply: Player, tr: TraceResultStruct, toolname: string, tool: object, button: number): boolean;

    /**
     * Called just before ENTITY:Initialize for "ai" type entities only.
     */
    CreateSchedulesInternal(): void;

    /**
     * Called so the entity can override the bullet impact effects it makes. This is called when the entity itself fires bullets via Entity:FireBullets, not when it gets hit.
     *
     * @arg TraceResultStruct tr - A Structures/TraceResult from the bullet's start point to the impact point
     * @arg number damageType - The damage type of bullet. See Enums/DMG
     * @return {boolean} Return true to not do the default thing - which is to call UTIL_ImpactTrace in C++
     */
    DoImpactEffect(tr: TraceResultStruct, damageType: number): boolean;

    /**
     * Called whenever an engine schedule is being ran.
     */
    DoingEngineSchedule(): void;

    /**
     * Runs a Lua schedule. Runs tasks inside the schedule.
     *
     * @arg object sched - The schedule to run.
     */
    DoSchedule(sched: object): void;

    /**
     * Called if and when the entity should be drawn opaquely, based on the Entity:GetRenderGroup of the entity.
     *
     * @arg number flags - The bit flags from Enums/STUDIO
     */
    Draw(flags: number): void;

    /**
     * Called when the entity should be drawn translucently. If your scripted entity has a translucent model, it will be invisible unless it is drawn here.
     *
     * @arg number flags - The bit flags from Enums/STUDIO
     */
    DrawTranslucent(flags: number): void;

    /**
     * Called when the entity stops touching another entity.
     *
     * @arg Entity entity - The entity which was touched.
     */
    EndTouch(entity: Entity): void;

    /**
     * Called whenever an engine schedule is finished.
     */
    EngineScheduleFinish(): void;

    /**
     * Called when an NPC's expression has finished.
     *
     * @arg string strExp - The path of the expression.
     */
    ExpressionFinished(strExp: string): void;

    /**
     * Called before firing clientside animation events, such as muzzle flashes or shell ejections.
     *
     * @arg Vector pos - Position of the effect
     * @arg Angle ang - Angle of the effect
     * @arg number event - The event ID of happened even. See this page.
     * @arg string name - Name of the event
     * @return {boolean} Return true to disable the effect
     */
    FireAnimationEvent(pos: Vector, ang: Angle, event: number, name: string): boolean;

    /**
     * Called to determine how good an NPC is at using a particular weapon.
     *
     * @arg Entity wep - The weapon being used by the NPC.
     * @arg Entity target - The target the NPC is attacking
     * @return {number} The number of degrees of inaccuracy in the NPC's attack.
     */
    GetAttackSpread(wep: Entity, target: Entity): number;

    /**
     * Called when scripted NPC needs to check how he "feels" against another entity, such as when NPC:Disposition is called.
     *
     * @arg Entity ent - The entity in question
     * @return {number} How our scripter NPC "feels" towards the entity in question. See Enums/D.
     */
    GetRelationship(ent: Entity): number;

    /**
 * Specify a mesh that should be rendered instead of this SENT's model.
* 
* @return {IMesh} A table containing the following keys:

IMesh Mesh - Required
IMaterial Material - Required
VMatrix Matrix - Optional
 */
    GetRenderMesh(): IMesh;

    /**
 * Called when the shadow needs to be recomputed. Allows shadow angles to be customized. This only works for anim type entities.
* 
* @arg number type - Type of the shadow this entity uses. Possible values:

0 - No shadow
1 - Simple 'blob' shadow
2 - Render To Texture shadow (updates only when necessary)
3 - Dynamic RTT - updates always
4 - Render to Depth Texture
* @return {Vector} The new shadow direction to use.
 */
    GetShadowCastDirection(type: number): Vector;

    /**
     * Called every second to poll the sound hint interests of this SNPC. This is used in conjunction with other sound hint functions, such as sound.EmitHint and NPC:GetBestSoundHint.
     *
     * @return {number} A bitflag representing which sound types this NPC wants to react to.  See SOUND_ enums.
     */
    GetSoundInterests(): number;

    /**
     * Called by GM:GravGunPickupAllowed on ALL entites in Sandbox-derived  gamemodes and acts as an override.
     *
     * @arg Player ply - The player aiming at us
     * @return {boolean} Return true to allow the entity to be picked up
     */
    GravGunPickupAllowed(ply: Player): boolean;

    /**
     * Called when this entity is about to be punted with the gravity gun (primary fire).
     *
     * @arg Player ply - The player pressing left-click with the gravity gun at an entity
     * @return {boolean} Return true or false to enable or disable punting respectively.
     */
    GravGunPunt(ply: Player): boolean;

    /**
     * Called before firing serverside animation events, such as weapon reload, drawing and holstering for NPCs, scripted sequences, etc.
     *
     * @arg number event - The event ID of happened even. See this page.
     * @arg number eventTime - The absolute time this event occurred using CurTime.
     * @arg number cycle - The frame this event occurred as a number between 0 and 1.
     * @arg number type - Event type. See the Source SDK.
     * @arg string options - Name or options of this event.
     */
    HandleAnimEvent(event: number, eventTime: number, cycle: number, type: number, options: string): void;

    /**
 * Called when a bullet trace hits this entity and allows you to override the default behavior by returning true.
* 
* @arg TraceResultStruct traceResult - The trace that hit this entity as a Structures/TraceResult.
* @arg number damageType - The damage bits associated with the trace, see Enums/DMG
* @arg string [customImpactName="nil"] - The effect name to override the impact effect with.
Possible arguments are ImpactJeep, AirboatGunImpact, HelicopterImpact, ImpactGunship.
* @return {boolean} Return true to override the default impact effects.
 */
    ImpactTrace(traceResult: TraceResultStruct, damageType: number, customImpactName?: string): boolean;

    /**
     * Called when the entity is created. This is called when you Entity:Spawn the custom entity.
     */
    Initialize(): void;

    /**
 * Called when deciding if the Scripted NPC should be able to perform a certain jump or not.
* 
* @arg Vector startPos - Start of the jump
* @arg Vector apex - Apex point of the jump
* @arg Vector endPos - The landing position
* @return {boolean} Return true if this jump should be allowed to be performed, false otherwise.
Not returning anything, or returning a non boolean will perform the default action.
 */
    IsJumpLegal(startPos: Vector, apex: Vector, endPos: Vector): boolean;

    /**
     * Called when the engine sets a value for this scripted entity.
     *
     * @arg string key - The key that was affected.
     * @arg string value - The new value.
     * @return {boolean} Return true to suppress this KeyValue or return false or nothing to apply this key value.
     */
    KeyValue(key: string, value: string): boolean;

    /**
     * Start the next task in specific schedule.
     *
     * @arg object sched - The schedule to start next task in.
     */
    NextTask(sched: object): void;

    /**
     * Called when the currently active weapon of the SNPC changes.
     *
     * @arg Weapon old - The previous active weapon.
     * @arg Weapon _new - The new active weapon.
     */
    OnChangeActiveWeapon(old: Weapon, _new: Weapon): void;

    /**
     * Called when the NPC has changed its activity.
     *
     * @arg number act - The new activity. See Enums/ACT.
     */
    OnChangeActivity(act: number): void;

    /**
     * Called each time the NPC updates its condition.
     *
     * @arg number conditionID - The ID of condition. See NPC:ConditionName.
     */
    OnCondition(conditionID: number): void;

    /**
     * Called on any entity after it has been created by the duplicator and before any bone/entity modifiers have been applied.
     *
     * @arg EntityCopyDataStruct entTable - Structures/EntityCopyData of the source entity.
     */
    OnDuplicated(entTable: EntityCopyDataStruct): void;

    /**
     * Called after duplicator finishes saving the entity, allowing you to modify the save data.
     *
     * @arg EntityCopyDataStruct data - The save Structures/EntityCopyData that you can modify.
     */
    OnEntityCopyTableFinish(data: EntityCopyDataStruct): void;

    /**
     * Called when the SNPC completes its movement to its destination.
     */
    OnMovementComplete(): void;

    /**
     * Called when the SNPC failed to move to its destination.
     */
    OnMovementFailed(): void;

    /**
     * Called when the entity is reloaded by the lua auto-refresh system, i.e. when the developer edits the lua file for the entity while the game is running.
     */
    OnReloaded(): void;

    /**
     * Called when the entity is about to be removed.
     */
    OnRemove(): void;

    /**
     * Called when the entity is reloaded from a Source Engine save (not the Sandbox saves or dupes) or on a changelevel (for example Half-Life 2 campaign level transitions).
     */
    OnRestore(): void;

    /**
     * Called when the entity is taking damage.
     *
     * @arg CTakeDamageInfo damage - The damage to be applied to the entity.
     * @return {number} How much damage the entity took. Basically > 0 means took damage, 0 means did not take damage.
     */
    OnTakeDamage(damage: CTakeDamageInfo): number;

    /**
     * Called from the engine when TaskComplete is called.
     * This allows us to move onto the next task - even when TaskComplete was called from an engine side task.
     */
    OnTaskComplete(): void;

    /**
     * Polls whenever the entity should trigger the brush.
     *
     * @arg Entity ent - The entity that is about to trigger.
     * @return {boolean} Should trigger or not.
     */
    PassesTriggerFilters(ent: Entity): boolean;

    /**
     * Called when the entity collides with anything. The move type and solid type must be VPHYSICS for the hook to be called.
     *
     * @arg CollisionDataStruct colData - Information regarding the collision. See Structures/CollisionData.
     * @arg PhysObj collider - The physics object that collided.
     */
    PhysicsCollide(colData: CollisionDataStruct, collider: PhysObj): void;

    /**
     * Called from the Entity's motion controller to simulate physics.
     *
     * @arg PhysObj phys - The physics object of the entity.
     * @arg number deltaTime - Time since the last call.
     * @return {Vector} Angular force
     * @return {Vector} Linear force
     * @return {number} One of the Enums/SIM.
     */
    PhysicsSimulate(phys: PhysObj, deltaTime: number): LuaMultiReturn<[Vector, Vector, number]>;

    /**
     * Called whenever the physics of the entity are updated.
     *
     * @arg PhysObj phys - The physics object of the entity.
     */
    PhysicsUpdate(phys: PhysObj): void;

    /**
     * Called after the duplicator finished copying the entity.
     */
    PostEntityCopy(): void;

    /**
 * Called after the duplicator pastes the entity, after the bone/entity modifiers have been applied to the entity.
* 
* @arg Player ply - The player who pasted the entity.
This may not be a valid player in some circumstances. For example, when a save is loaded from the main menu, this hook will be called before the player is spawned. This argument will be a NULL entity in that case.This will be nil for invalid players.
* @arg Entity ent - The entity itself. Same as 'self'.
* @arg object createdEntities - All entities that are within the placed dupe. <note>The keys of each value in this table are the original entity indexes when the duplication was created. This can be utilized to restore entity references that don't get saved in duplications.</note>
 */
    PostEntityPaste(ply: Player, ent: Entity, createdEntities: object): void;

    /**
     * Called before the duplicator copies the entity.
     */
    PreEntityCopy(): void;

    /**
     * Called instead of the engine drawing function of the entity. This hook works on any entity (scripted or not) it is applied on.
     *
     * @arg number flags - The STUDIO_ flags for this render operation.
     */
    RenderOverride(flags: number): void;

    /**
     * Called from the engine every 0.1 seconds.
     */
    RunAI(): void;

    /**
     * Called when an engine task is ran on the entity.
     *
     * @arg number taskID - The task ID, see ai_task.h
     * @arg number taskData - The task data.
     * @return {boolean} true to prevent default action
     */
    RunEngineTask(taskID: number, taskData: number): boolean;

    /**
     * Called every think on running task.
     * The actual task function should tell us when the task is finished.
     *
     * @arg object task - The task to run
     */
    RunTask(task: object): void;

    /**
     * Called whenever a schedule is finished.
     */
    ScheduleFinished(): void;

    /**
     * Set the schedule we should be playing right now.
     *
     * @arg number iNPCState
     */
    SelectSchedule(iNPCState: number): void;

    /**
     * Toggles automatic frame advancing for animated sequences on an entity.
     *
     * @arg boolean enable - Whether or not to set automatic frame advancing.
     */
    SetAutomaticFrameAdvance(enable: boolean): void;

    /**
     * Sets the current task.
     *
     * @arg object task - The task to set.
     */
    SetTask(task: object): void;

    /**
     * Called when the entity should set up its  Data Tables.
     */
    SetupDataTables(): void;

    /**
     * This is the spawn function. It's called when a player spawns the entity from the spawnmenu.
     *
     * @arg Player ply - The player that is spawning this SENT
     * @arg TraceResultStruct tr - A Structures/TraceResult from player eyes to their aim position
     * @arg string ClassName - The classname of your entity
     */
    SpawnFunction(ply: Player, tr: TraceResultStruct, ClassName: string): void;

    /**
     * Starts an engine schedule.
     *
     * @arg number scheduleID - Schedule ID to start. See Enums/SCHED
     */
    StartEngineSchedule(scheduleID: number): void;

    /**
     * Called when an engine task has been started on the entity.
     *
     * @arg number taskID - Task ID to start, see ai_task.h
     * @arg number TaskData - Task data
     * @return {boolean} true to stop default action
     */
    StartEngineTask(taskID: number, TaskData: number): boolean;

    /**
     * Starts a schedule previously created by ai_schedule.New.
     *
     * @arg Schedule sched - Schedule to start.
     */
    StartSchedule(sched: Schedule): void;

    /**
     * Called once on starting task.
     *
     * @arg Task task - The task to start, created by ai_task.New.
     */
    StartTask(task: Task): void;

    /**
     * Called when the entity starts touching another entity.
     *
     * @arg Entity entity - The entity which is being touched.
     */
    StartTouch(entity: Entity): void;

    /**
     * Used to store an output so it can be triggered with ENTITY:TriggerOutput.
     * Outputs compiled into a map are passed to entities as key/value pairs through ENTITY:KeyValue.
     *
     * @arg string name - Name of output to store
     * @arg string info - Output info
     */
    StoreOutput(name: string, info: string): void;

    /**
     * Returns true if the current running Task is finished.
     *
     * @return {boolean} Is the current running Task is finished or not.
     */
    TaskFinished(): boolean;

    /**
     * Returns how many seconds we've been doing this current task
     *
     * @return {number} How many seconds we've been doing this current task
     */
    TaskTime(): number;

    /**
 * Allows you to override trace result when a trace hits the entitys Bounding Box.
* 
* @arg Vector startpos - Start position of the trace.
* @arg Vector delta - Offset from startpos to the endpos of the trace.
* @arg boolean isbox - Is the trace a hull trace?
* @arg Vector extents - Size of the hull trace?
* @arg number mask - The Enums/CONTENTS mask.
* @return {Vector} Returning a table will allow you to override trace results. Table should contain the following keys, all optional:

Vector HitPos - The new hitpos of the trace.
number Fraction - A number from 0 to 1, describing how far the trace went from its origin point, 1 = did not hit.
Vector Normal - A unit vector (length=1) describing the direction perpendicular to the hit surface.

Returning true will allow "normal" collisions to happen for SOLID_VPHYSICS and SOLID_BBOX entities.
Returning nothing or false allows the trace to ignore the entity completely.
 */
    TestCollision(startpos: Vector, delta: Vector, isbox: boolean, extents: Vector, mask: number): Vector;

    /**
     * Called every frame on the client.
     * Called every tick on the server.
     *
     * @return {boolean} Return true if you used Entity:NextThink to override the next execution time.
     */
    Think(): boolean;

    /**
     * Called every tick for every entity being "touched".
     *
     * @arg Entity entity - The entity that touched it.
     */
    Touch(entity: Entity): void;

    /**
     * Triggers all outputs stored using ENTITY:StoreOutput.
     *
     * @arg string output - Name of output to fire
     * @arg Entity activator - Activator entity
     * @arg string [data="nil"] - The data to give to the output.
     */
    TriggerOutput(output: string, activator: Entity, data?: string): void;

    /**
     * Called whenever the transmit state should be updated.
     *
     * @return {number} Transmit state to set, see Enums/TRANSMIT.
     */
    UpdateTransmitState(): number;

    /**
     * Called when an entity "uses" this entity, for example a player pressing their +use key (default e) on this entity.
     *
     * @arg Entity activator - The entity that caused this input. This will usually be the player who pressed their use key
     * @arg Entity caller - The entity responsible for the input. This will typically be the same as activator unless some other entity is acting as a proxy
     * @arg number useType - Use type, see Enums/USE.
     * @arg number value - Any passed value.
     */
    Use(activator: Entity, caller: Entity, useType: number, value: number): void;
}

declare type ENTITY__HOOKS = keyof ENTITY;

declare type ENT = ENTITY;
