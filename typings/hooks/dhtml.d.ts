declare interface DHTML {
    [index: string | number | symbol]: any;

    /**
     * Called when this panel begins loading a page.
     *
     * @arg string url - The URL of the current page.
     */
    OnBeginLoadingDocument(url: string): void;

    /**
     * Called by the engine when a callback function is called.
     *
     * @arg string library - Library name of the JS function you are defining.
     * @arg string name - Name of the JS function you are defining.
     * @arg GMLua.CallbackNoContext callback - Function called when the JS function is called. Arguments passed to the JS function will be passed here.
     * @return {boolean} Return true to suppress default engine action.
     */
    OnCallback(library: string, name: string, callback: GMLua.CallbackNoContext): boolean;

    /**
     * Called by HTML panels when the target URL of the frame has changed, this happens when you hover over a link.
     *
     * @arg string url - New target URL.
     */
    OnChangeTargetURL(url: string): void;

    /**
     * Called by HTML panels when the title of the loaded page has been changed.
     *
     * @arg string newTitle - The new title of the page.
     */
    OnChangeTitle(newTitle: string): void;

    /**
     * Called by HTML panels when the page attempts to open a new child view (such as a popup or new tab).
     *
     * @arg string sourceURL - The URL of the page requesting to create a child.
     * @arg string targetURL - The URL of the requested child.
     * @arg boolean isPopup - True if the requested view is a popup.
     */
    OnChildViewCreated(sourceURL: string, targetURL: string, isPopup: boolean): void;

    /**
     * Called by HTML panels when the panel's DOM has been set up. You can run JavaScript in here.
     *
     * @arg string url - The URL of the current page.
     */
    OnDocumentReady(url: string): void;

    /**
     * Called when this panel successfully loads a page.
     *
     * @arg string url - The URL of the current page.
     */
    OnFinishLoadingDocument(url: string): void;
}

declare type DHTML__HOOKS = keyof DHTML;
