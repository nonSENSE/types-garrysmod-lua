declare interface GM {
    [index: string | number | symbol]: any;

    /**
     * Called when a map I/O event occurs.
     *
     * @arg Entity ent - Entity that receives the input
     * @arg string input - The input name. Is not guaranteed to be a valid input on the entity.
     * @arg Entity activator - Activator of the input
     * @arg Entity caller - Caller of the input
     * @arg any value - Data provided with the input. Will be either a string, a number, a boolean or a nil.
     * @return {boolean} Return true to prevent this input from being processed. Do not return otherwise.
     */
    AcceptInput(ent: Entity, input: string, activator: Entity, caller: Entity, value: any): boolean;

    /**
     * Adds a death notice entry.
     *
     * @arg string attacker - The name of the attacker
     * @arg number attackerTeam - The team of the attacker
     * @arg string inflictor - Class name of the entity inflicting the damage
     * @arg string victim - Name of the victim
     * @arg number victimTeam - Team of the victim
     */
    AddDeathNotice(attacker: string, attackerTeam: number, inflictor: string, victim: string, victimTeam: number): void;

    /**
 * Allows you to adjust the mouse sensitivity.
* 
* @arg number defaultSensitivity - The old sensitivity
In general it will be 0, which is equivalent to a sensitivity of 1.
* @return {number} A fraction of the normal sensitivity (0.5 would be half as sensitive).
Return -1 to not override and prevent subsequent hooks and WEAPON:AdjustMouseSensitivity from running.
Return nil to not override and allow subsequent hooks and WEAPON:AdjustMouseSensitivity to run.
 */
    AdjustMouseSensitivity(defaultSensitivity: number): number;

    /**
     * Called when a player tries to pick up something using the "use" key, return to override.
     *
     * @arg Player ply - The player trying to pick up something.
     * @arg Entity ent - The Entity the player attempted to pick up.
     * @return {boolean} Allow the player to pick up the entity or not.
     */
    AllowPlayerPickup(ply: Player, ent: Entity): boolean;

    /**
     * This hook is used to calculate animations for a player.
     *
     * @arg Player ply - The player to apply the animation.
     * @arg Vector vel - The velocity of the player.
     * @return {number} Enums/ACT for the activity the player should use. A nil return will be treated as ACT_INVALID.
     * @return {number} Sequence for the player to use. This takes precedence over the activity (the activity is still used for layering). Return -1 or nil to let the activity determine the sequence.
     */
    CalcMainActivity(ply: Player, vel: Vector): LuaMultiReturn<[number, number]>;

    /**
     * Called from GM:CalcView when player is in driving a vehicle.
     *
     * @arg Vehicle veh - The vehicle the player is driving
     * @arg Player ply - The vehicle driver
     * @arg CamDataStruct view - The view data containing players FOV, view position and angles, see Structures/CamData
     * @return {CamDataStruct} The modified view table containing new values, see Structures/CamData
     */
    CalcVehicleView(veh: Vehicle, ply: Player, view: CamDataStruct): CamDataStruct;

    /**
     * Allows override of the default view.
     *
     * @arg Player ply - The local player.
     * @arg Vector origin - The player's view position.
     * @arg Angle angles - The player's view angles.
     * @arg number fov - Field of view.
     * @arg number znear - Distance to near clipping plane.
     * @arg number zfar - Distance to far clipping plane.
     * @return {CamDataStruct} View data table. See Structures/CamData
     */
    CalcView(ply: Player, origin: Vector, angles: Angle, fov: number, znear: number, zfar: number): CamDataStruct;

    /**
     * Allows overriding the position and angle of the viewmodel.
     *
     * @arg Weapon wep - The weapon entity
     * @arg Entity vm - The viewmodel entity
     * @arg Vector oldPos - Original position (before viewmodel bobbing and swaying)
     * @arg Angle oldAng - Original angle (before viewmodel bobbing and swaying)
     * @arg Vector pos - Current position
     * @arg Angle ang - Current angle
     * @return {Vector} New position
     * @return {Angle} New angle
     */
    CalcViewModelView(
        wep: Weapon,
        vm: Entity,
        oldPos: Vector,
        oldAng: Angle,
        pos: Vector,
        ang: Angle
    ): LuaMultiReturn<[Vector, Angle]>;

    /**
     * Called when a variable is edited on an Entity (called by Edit Properties... menu), to determine if the edit should be permitted.
     *
     * @arg Entity ent - The entity being edited.
     * @arg Player ply - The player doing the editing.
     * @arg string key - The name of the variable.
     * @arg string val - The new value, as a string which will later be converted to its appropriate type.
     * @arg any editor - The edit table defined in Entity:NetworkVar.
     * @return {boolean} Return true to allow editing.
     */
    CanEditVariable(ent: Entity, ply: Player, key: string, val: string, editor: any): boolean;

    /**
     * Determines if the player can exit the vehicle.
     *
     * @arg Vehicle veh - The vehicle entity
     * @arg Player ply - The player
     * @return {boolean} True if the player can exit the vehicle.
     */
    CanExitVehicle(veh: Vehicle, ply: Player): boolean;

    /**
     * Determines whether or not the player can enter the vehicle.
     *
     * @arg Player player - The player.
     * @arg Vehicle vehicle - The vehicle.
     * @arg number role - The seat number.
     * @return {boolean} false if the player is not allowed to enter the vehicle.
     */
    CanPlayerEnterVehicle(player: Player, vehicle: Vehicle, role: number): boolean;

    /**
     * Determines if the player can kill themselves using the concommands kill or explode.
     *
     * @arg Player player - The player
     * @return {boolean} True if they can suicide.
     */
    CanPlayerSuicide(player: Player): boolean;

    /**
     * Determines if the player can unfreeze the entity.
     *
     * @arg Player player - The player
     * @arg Entity entity - The entity
     * @arg PhysObj phys - The physics object of the entity
     * @return {boolean} True if they can unfreeze.
     */
    CanPlayerUnfreeze(player: Player, entity: Entity, phys: PhysObj): boolean;

    /**
     * Called whenever a players tries to undo.
     *
     * @arg Player ply - The player who tried to undo something.
     * @arg UndoStruct undo - The undo table as a Structures/Undo.
     * @return {boolean} Return false to disallow the undo.
     */
    CanUndo(ply: Player, undo: UndoStruct): boolean;

    /**
     * Called each frame to record demos to video using IVideoWriter.
     */
    CaptureVideo(): void;

    /**
 * Called when a message is printed to the chat box. Note, that this isn't working with player messages even though there are arguments for it.
* 
* @arg number index - The index of the player.
* @arg string name - The name of the player.
* @arg string text - The text that is being sent.
* @arg string type - Chat filter type. Possible values are:

joinleave - Player join and leave messages
namechange - Player name change messages
servermsg - Server messages such as convar changes
teamchange - Team changes?
chat - (Obsolete?) Player chat? Seems to trigger when server console uses the say command
none - A fallback value
* @return {boolean} Return true to suppress the chat message.
 */
    ChatText(index: number, name: string, text: string, type: string): boolean;

    /**
     * Called whenever the content of the user's chat input box is changed.
     *
     * @arg string text - The new contents of the input box
     */
    ChatTextChanged(text: string): void;

    /**
     * Called when a non local player connects to allow the Lua system to check the password.
     *
     * @arg string steamID64 - The 64bit Steam ID of the joining player, use util.SteamIDFrom64 to convert it to a STEAM_0: one.
     * @arg string ipAddress - The IP of the connecting client
     * @arg string svPassword - The current value of sv_password (the password set by the server)
     * @arg string clPassword - The password provided by the client
     * @arg string name - The name of the joining player
     * @return {boolean} If the hook returns false then the player is disconnected
     * @return {string} If returning false in the first argument, then this should be the disconnect message. This will default to #GameUI_ServerRejectBadPassword, which is Bad Password. translated to the client's language.
     */
    CheckPassword(
        steamID64: string,
        ipAddress: string,
        svPassword: string,
        clPassword: string,
        name: string
    ): LuaMultiReturn<[boolean, string]>;

    /**
     * Called when a player's sign on state changes.
     *
     * @arg number userID - The userID of the player whose sign on state has changed.
     * @arg number oldState - The previous sign on state. See SIGNONSTATE enums.
     * @arg number newState - The new/current sign on state. See SIGNONSTATE enums.
     */
    ClientSignOnStateChanged(userID: number, oldState: number, newState: number): void;

    /**
     * Called when derma menus are closed with CloseDermaMenus.
     */
    CloseDermaMenus(): void;

    /**
     * Called whenever an entity becomes a clientside ragdoll.
     *
     * @arg Entity entity - The Entity that created the ragdoll
     * @arg Entity ragdoll - The ragdoll being created.
     */
    CreateClientsideRagdoll(entity: Entity, ragdoll: Entity): void;

    /**
     * Called when a serverside ragdoll of an entity has been created.
     *
     * @arg Entity owner - Entity that owns the ragdoll
     * @arg Entity ragdoll - The ragdoll entity
     */
    CreateEntityRagdoll(owner: Entity, ragdoll: Entity): void;

    /**
 * Allows you to change the players movements before they're sent to the server.
* 
* @arg CUserCmd cmd - The User Command data
* @return {boolean} Return true to:

Disable Sandbox C menu "screen clicking"
Disable Teammate nocollide (verification required)
Prevent calling of C_BaseHLPlayer::CreateMove & subsequently C_BasePlayer::CreateMove
 */
    CreateMove(cmd: CUserCmd): boolean;

    /**
     * Teams are created within this hook using team.SetUp.
     */
    CreateTeams(): void;

    /**
     * Called upon an animation event, this is the ideal place to call player animation functions such as Player:AddVCDSequenceToGestureSlot, Player:AnimRestartGesture and so on.
     *
     * @arg Player ply - Player who is being animated
     * @arg number event - Animation event. See Enums/PLAYERANIMEVENT
     * @arg number [data=0] - The data for the event. This is interpreted as an Enums/ACT by PLAYERANIMEVENT_CUSTOM and PLAYERANIMEVENT_CUSTOM_GESTURE, or a sequence by PLAYERANIMEVENT_CUSTOM_SEQUENCE.
     * @return {number} The translated activity to send to the weapon. See Enums/ACT. Return ACT_INVALID if you don't want to send an activity.
     */
    DoAnimationEvent(ply: Player, event: number, data?: number): number;

    /**
     * Handles the player's death.
     *
     * @arg Player ply - The player
     * @arg Entity attacker - The entity that killed the player
     * @arg CTakeDamageInfo dmg - Damage info
     */
    DoPlayerDeath(ply: Player, attacker: Entity, dmg: CTakeDamageInfo): void;

    /**
     * This hook is called every frame to draw all of the current death notices.
     *
     * @arg number x - X position to draw death notices as a ratio
     * @arg number y - Y position to draw death notices as a ratio
     */
    DrawDeathNotice(x: number, y: number): void;

    /**
     * Called every frame before drawing the in-game monitors ( Breencast, in-game TVs, etc ), but doesn't seem to be doing anything, trying to render 2D or 3D elements fail.
     */
    DrawMonitors(): void;

    /**
     * Called after all other 2D draw hooks are called. Draws over all VGUI Panels and HUDs.
     */
    DrawOverlay(): void;

    /**
     * Allows you to override physgun beam drawing.
     *
     * @arg Player ply - Physgun owner
     * @arg Weapon physgun - The physgun
     * @arg boolean enabled - Is the beam enabled
     * @arg Entity target - Entity we are grabbing. This will be NULL if nothing is being held
     * @arg number physBone - ID of the physics bone (PhysObj) we are grabbing at. Use Entity:TranslatePhysBoneToBone to translate to an actual bone.
     * @arg Vector hitPos - Beam hit position relative to the physics bone (PhysObj) we are grabbing.
     * @return {boolean} Return false to hide default beam
     */
    DrawPhysgunBeam(
        ply: Player,
        physgun: Weapon,
        enabled: boolean,
        target: Entity,
        physBone: number,
        hitPos: Vector
    ): boolean;

    /**
     * Called right before an entity stops driving. Overriding this hook will cause it to not call drive.End and the player will not stop driving.
     *
     * @arg Entity ent - The entity being driven
     * @arg Player ply - The player driving the entity
     */
    EndEntityDriving(ent: Entity, ply: Player): void;

    /**
 * Called whenever a sound has been played. This will not be called clientside if the server played the sound without the client also calling Entity:EmitSound.
* 
* @arg EmitSoundInfoStruct data - Information about the played sound. Changes done to this table can be applied by returning true from this hook.
See Structures/EmitSoundInfo.
* @return {boolean} Return true to apply all changes done to the data table.
Return false to prevent the sound from playing.
Return nil or nothing to play the sound without altering it.
 */
    EntityEmitSound(data: EmitSoundInfoStruct): boolean;

    /**
 * Called every time a bullet is fired from an entity.
* 
* @arg Entity entity - The entity that fired the bullet
* @arg BulletStruct data - The bullet data. See Structures/Bullet.
* @return {boolean} Return true to apply all changes done to the bullet table.
Return false to suppress the bullet.
 */
    EntityFireBullets(entity: Entity, data: BulletStruct): boolean;

    /**
     * Called when a key-value pair is set on an entity, either by the engine (for example when map spawns) or Entity:SetKeyValue.
     *
     * @arg Entity ent - Entity that the keyvalue is being set on
     * @arg string key - Key of the key/value pair
     * @arg string value - Value of the key/value pair
     * @return {string} If set, the value of the key-value pair will be overridden by this string.
     */
    EntityKeyValue(ent: Entity, key: string, value: string): string;

    /**
     * Called when an NWVar is changed.
     *
     * @arg Entity ent - The owner entity of changed NWVar
     * @arg string name - The name if changed NWVar
     * @arg any oldval - The old value of the NWVar
     * @arg any newval - The new value of the NWVar
     */
    EntityNetworkedVarChanged(ent: Entity, name: string, oldval: any, newval: any): void;

    /**
     * Called right before the removal of an entity.
     *
     * @arg Entity ent - Entity being removed
     */
    EntityRemoved(ent: Entity): void;

    /**
     * Called when an entity takes damage. You can modify all parts of the damage info in this hook.
     *
     * @arg Entity target - The entity taking damage
     * @arg CTakeDamageInfo dmg - Damage info
     * @return {boolean} Return true to completely block the damage event
     */
    EntityTakeDamage(target: Entity, dmg: CTakeDamageInfo): boolean;

    /**
     * This hook polls the entity the player use action should be applied to.
     *
     * @arg Player ply - The player who initiated the use action.
     * @arg Entity defaultEnt - The entity that was chosen by the engine.
     * @return {Entity} The entity to use instead of default entity
     */
    FindUseEntity(ply: Player, defaultEnt: Entity): Entity;

    /**
     * Runs when user cancels/finishes typing.
     */
    FinishChat(): void;

    /**
     * Called after GM:Move, applies all the changes from the CMoveData to the player.
     *
     * @arg Player ply - Player
     * @arg CMoveData mv - Movement data
     * @return {boolean} Return true to suppress default engine behavior, i.e. declare that you have already moved the player according to the move data in a custom way.
     */
    FinishMove(ply: Player, mv: CMoveData): boolean;

    /**
 * Called to allow override of the default Derma skin for all panels.
* 
* @return {string} A case sensitive Derma skin name to be used as default, registered previously via derma.DefineSkin.
Returning nothing, nil or invalid name will make it fallback to the "Default" skin.
 */
    ForceDermaSkin(): string;

    /**
     * Called when game content has been changed, for example an addon or a mountable game was (un)mounted.
     */
    GameContentChanged(): void;

    /**
     * Called when a player takes damage from falling, allows to override the damage.
     *
     * @arg Player ply - The player
     * @arg number speed - The fall speed
     * @return {number} New fall damage
     */
    GetFallDamage(ply: Player, speed: number): number;

    /**
     * Called when the game(server) needs to update the text shown in the server browser as the gamemode.
     *
     * @return {string} The text to be shown in the server browser as the gamemode.
     */
    GetGameDescription(): string;

    /**
     * Allows you to modify the Source Engine's motion blur shaders.
     *
     * @arg number horizontal - The amount of horizontal blur.
     * @arg number vertical - The amount of vertical  blur.
     * @arg number forward - The amount of forward/radial blur.
     * @arg number rotational - The amount of rotational blur.
     * @return {number} New amount of horizontal blur.
     * @return {number} New amount of vertical blur.
     * @return {number} New amount of forward/radial blur.
     * @return {number} New amount of rotational blur.
     */
    GetMotionBlurValues(
        horizontal: number,
        vertical: number,
        forward: number,
        rotational: number
    ): LuaMultiReturn<[number, number, number, number]>;

    /**
     * Called to determine preferred carry angles for the entity. It works for both, +use pickup and gravity gun pickup.
     *
     * @arg Entity ent - The entity to generate carry angles for
     * @arg Player ply - The player who is holding the object
     * @return {Angle} The preferred carry angles for the entity.
     */
    GetPreferredCarryAngles(ent: Entity, ply: Player): Angle;

    /**
     * Returns the color for the given entity's team. This is used in chat and deathnotice text.
     *
     * @arg Entity ent - Entity
     * @return {any} Team Color
     */
    GetTeamColor(ent: Entity): any;

    /**
     * Returns the team color for the given team index.
     *
     * @arg number team - Team index
     * @return {any} Team Color
     */
    GetTeamNumColor(team: number): any;

    /**
     * Override this hook to disable/change ear-grabbing in your gamemode.
     *
     * @arg Player ply - Player
     */
    GrabEarAnimation(ply: Player): void;

    /**
     * Called when an entity is released by a gravity gun.
     *
     * @arg Player ply - Player who is wielding the gravity gun
     * @arg Entity ent - The entity that has been dropped
     */
    GravGunOnDropped(ply: Player, ent: Entity): void;

    /**
     * Called when an entity is picked up by a gravity gun.
     *
     * @arg Player ply - The player wielding the gravity gun
     * @arg Entity ent - The entity that has been picked up by the gravity gun
     */
    GravGunOnPickedUp(ply: Player, ent: Entity): void;

    /**
     * Called every tick to poll whether a player is allowed to pick up an entity with the gravity gun or not.
     *
     * @arg Player ply - The player wielding the gravity gun
     * @arg Entity ent - The entity the player is attempting to pick up
     * @return {boolean} Return true to allow entity pick up
     */
    GravGunPickupAllowed(ply: Player, ent: Entity): boolean;

    /**
     * Called when an entity is about to be punted with the gravity gun (primary fire).
     *
     * @arg Player ply - The player wielding the gravity gun
     * @arg Entity ent - The entity the player is attempting to punt
     * @return {boolean} Return true to allow and false to disallow.
     */
    GravGunPunt(ply: Player, ent: Entity): boolean;

    /**
     * Called when the mouse has been double clicked on any panel derived from CGModBase, such as the panel used by gui.EnableScreenClicker and the panel used by Panel:ParentToHUD.
     *
     * @arg number mouseCode - The code of the mouse button pressed, see Enums/MOUSE
     * @arg Vector aimVector - A normalized vector pointing in the direction the client has clicked
     */
    GUIMouseDoublePressed(mouseCode: number, aimVector: Vector): void;

    /**
     * Called whenever a players presses a mouse key on the context menu in Sandbox or on any panel derived from CGModBase, such as the panel used by gui.EnableScreenClicker and the panel used by Panel:ParentToHUD.
     *
     * @arg number mouseCode - The key that the player pressed using Enums/MOUSE.
     * @arg Vector aimVector - A normalized direction vector local to the camera. Internally, this is  gui.ScreenToVector( gui.MousePos() ).
     */
    GUIMousePressed(mouseCode: number, aimVector: Vector): void;

    /**
     * Called whenever a players releases a mouse key on the context menu in Sandbox or on any panel derived from CGModBase, such as the panel used by gui.EnableScreenClicker and the panel used by Panel:ParentToHUD.
     *
     * @arg number mouseCode - The key the player released, see Enums/MOUSE
     * @arg Vector aimVector - A normalized direction vector local to the camera. Internally this is  gui.ScreenToVector( gui.MousePos() ).
     */
    GUIMouseReleased(mouseCode: number, aimVector: Vector): void;

    /**
     * Allows to override player driving animations.
     *
     * @arg Player ply - Player to process
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerDriving(ply: Player): boolean;

    /**
     * Allows to override player crouch animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerDucking(ply: Player, velocity: number): boolean;

    /**
     * Called every frame by the player model animation system. Allows to override player jumping animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerJumping(ply: Player, velocity: number): boolean;

    /**
     * Allows to override player landing animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @arg boolean onGround - Was the player on ground?
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerLanding(ply: Player, velocity: number, onGround: boolean): boolean;

    /**
     * Allows to override player noclip animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerNoClipping(ply: Player, velocity: number): boolean;

    /**
     * Allows to override player swimming animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerSwimming(ply: Player, velocity: number): boolean;

    /**
     * Allows to override player flying ( in mid-air, not noclipping ) animations.
     *
     * @arg Player ply - The player
     * @arg number velocity - Players velocity
     * @return {boolean} Return true if we've changed/set the animation, false otherwise
     */
    HandlePlayerVaulting(ply: Player, velocity: number): boolean;

    /**
     * Hides the team selection panel.
     */
    HideTeam(): void;

    /**
     * Called when the client has picked up ammo. Override to disable default HUD notification.
     *
     * @arg string itemName - Name of the item (ammo) picked up
     * @arg number amount - Amount of the item (ammo) picked up
     */
    HUDAmmoPickedUp(itemName: string, amount: number): void;

    /**
     * Renders the HUD pick-up history. Override to hide default or draw your own HUD.
     */
    HUDDrawPickupHistory(): void;

    /**
     * Called every frame to render the scoreboard.
     */
    HUDDrawScoreBoard(): void;

    /**
     * Called from GM:HUDPaint to draw player info when you hover over a player with your crosshair or mouse.
     */
    HUDDrawTargetID(): void;

    /**
     * Called when an item has been picked up. Override to disable the default HUD notification.
     *
     * @arg string itemName - Name of the picked up item
     */
    HUDItemPickedUp(itemName: string): void;

    /**
     * Called whenever the HUD should be drawn.
     */
    HUDPaint(): void;

    /**
     * Called before GM:HUDPaint when the HUD background is being drawn.
     */
    HUDPaintBackground(): void;

    /**
     * Called when the Gamemode is about to draw a given element on the client's HUD (heads-up display).
     *
     * @arg string name - The name of the HUD element. You can find a full list of HUD elements for this hook here.
     * @return {boolean} Return false to prevent the given element from being drawn on the client's screen.
     */
    HUDShouldDraw(name: string): boolean;

    /**
     * Called when a weapon has been picked up. Override to disable the default HUD notification.
     *
     * @arg Weapon weapon - The picked up weapon
     */
    HUDWeaponPickedUp(weapon: Weapon): void;

    /**
     * Called after the gamemode loads and starts.
     */
    Initialize(): void;

    /**
     * Called after all the entities are initialized. Starting from this hook LocalPlayer will return valid object.
     */
    InitPostEntity(): void;

    /**
     * Allows you to modify the supplied User Command with mouse input. This could be used to make moving the mouse do funky things to view angles.
     *
     * @arg CUserCmd cmd - User command.
     * @arg number x - The amount of mouse movement across the X axis this frame.
     * @arg number y - The amount of mouse movement across the Y axis this frame.
     * @arg Angle ang - The current view angle.
     * @return {boolean} Return true if we modified something.
     */
    InputMouseApply(cmd: CUserCmd, x: number, y: number, ang: Angle): boolean;

    /**
     * Check if a player can spawn at a certain spawnpoint.
     *
     * @arg Player ply - The player who is spawned
     * @arg Entity spawnpoint - The spawnpoint entity (on the map).
     * @arg boolean makeSuitable - If this is true, it'll kill any players blocking the spawnpoint.
     * @return {boolean} Return true to indicate that the spawnpoint is suitable (Allow for the player to spawn here), false to prevent spawning.
     */
    IsSpawnpointSuitable(ply: Player, spawnpoint: Entity, makeSuitable: boolean): boolean;

    /**
     * Called whenever a player pressed a key included within the IN keys.
     *
     * @arg Player ply - The player pressing the key. If running client-side, this will always be LocalPlayer.
     * @arg number key - The key that the player pressed using Enums/IN.
     */
    KeyPress(ply: Player, key: number): void;

    /**
     * Runs when a IN key was released by a player.
     *
     * @arg Player ply - The player releasing the key. If running client-side, this will always be LocalPlayer.
     * @arg number key - The key that the player released using Enums/IN.
     */
    KeyRelease(ply: Player, key: number): void;

    /**
     * Called from gm_load when the game should load a map.
     *
     * @arg string data - Compressed save data
     * @arg string map - The name of the map the save was created on
     * @arg number timestamp - The time the save was created on. Will always be 0.
     */
    LoadGModSave(data: string, map: string, timestamp: number): void;

    /**
     * Called when menu.lua has finished loading.
     */
    MenuStart(): void;

    /**
     * Override this gamemode function to disable mouth movement when talking on voice chat.
     *
     * @arg Player ply - Player in question
     */
    MouthMoveAnimation(ply: Player): void;

    /**
     * The Move hook is called for you to manipulate the player's MoveData.
     *
     * @arg Player ply - Player
     * @arg CMoveData mv - Movement information
     * @return {boolean} Return true to suppress default engine action.
     */
    Move(ply: Player, mv: CMoveData): boolean;

    /**
     * Returning true in this hook will cause it to render depth buffers defined with render.GetResolvedFullFrameDepth.
     *
     * @return {boolean} Render depth buffer
     */
    NeedsDepthPass(): boolean;

    /**
     * Called when an entity has been created over the network.
     *
     * @arg Entity ent - Created entity
     */
    NetworkEntityCreated(ent: Entity): void;

    /**
     * Called when a player's SteamID has been validated by Steam.
     *
     * @arg string name - Player name
     * @arg string steamID - Player SteamID
     */
    NetworkIDValidated(name: string, steamID: string): void;

    /**
     * Called whenever this entity changes its transmission state for this LocalPlayer, such as exiting or re entering the PVS.
     *
     * @arg Entity entity - The entity that changed its transmission state.
     * @arg boolean shouldtransmit - True if we started transmitting to this client and false if we stopped.
     */
    NotifyShouldTransmit(entity: Entity, shouldtransmit: boolean): void;

    /**
     * Called when a player has achieved an achievement. You can get the name and other information from an achievement ID with the achievements library.
     *
     * @arg Player ply - The player that earned the achievement
     * @arg number achievement - The index of the achievement
     */
    OnAchievementAchieved(ply: Player, achievement: number): void;

    /**
     * Called when the local player presses TAB while having their chatbox opened.
     *
     * @arg string text - The currently typed into chatbox text
     * @return {string} What should be placed into the chatbox instead of what currently is when player presses tab
     */
    OnChatTab(text: string): string;

    /**
     * Called when the player cleans up something.
     *
     * @arg string name - The name of the cleanup type
     * @return {boolean} Return false to suppress the cleanup notification.
     */
    OnCleanup(name: string): boolean;

    /**
     * Called when the context menu keybind (+menu_context) is released, which by default is C.
     */
    OnContextMenuClose(): void;

    /**
     * Called when the context menu keybind (+menu_context) is pressed, which by default is c.
     */
    OnContextMenuOpen(): void;

    /**
     * Called when the crazy physics detection detects an entity with Crazy Physics.
     *
     * @arg Entity ent - The entity that was detected as crazy
     * @arg PhysObj physobj - The physics object that is going crazy
     */
    OnCrazyPhysics(ent: Entity, physobj: PhysObj): void;

    /**
     * Called when a player has been hurt by an explosion. Override to disable default sound effect.
     *
     * @arg Player ply - Player who has been hurt
     * @arg CTakeDamageInfo dmginfo - Damage info from explsion
     */
    OnDamagedByExplosion(ply: Player, dmginfo: CTakeDamageInfo): void;

    /**
     * Called as soon as the entity is created. Very little of the entity's properties will be initialized at this stage. (keyvalues, classname, flags, anything), especially on the serverside.
     *
     * @arg Entity entity - The entity
     */
    OnEntityCreated(entity: Entity): void;

    /**
     * Called when the Entity:WaterLevel of an entity is changed.
     *
     * @arg Entity entity - The entity.
     * @arg number old - Previous water level.
     * @arg number _new - The new water level.
     */
    OnEntityWaterLevelChanged(entity: Entity, old: number, _new: number): void;

    /**
     * Called when the gamemode is loaded.
     */
    OnGamemodeLoaded(): void;

    /**
     * Called when a Lua error occurs, only works in the Menu realm.
     *
     * @arg string error - The error that occurred.
     * @arg number realm - Where the Lua error took place
     * @arg object stack - The Lua error stack trace
     * @arg string name - Title of the addon that is creating the Lua errors.
     * @arg number id - Steam Workshop ID of the addon creating Lua errors, if it is an addon.
     */
    OnLuaError(error: string, realm: number, stack: object, name: string, id: number): void;

    /**
     * Called whenever an NPC is killed.
     *
     * @arg NPC npc - The killed NPC
     * @arg Entity attacker - The NPCs attacker, the entity that gets the kill credit, for example a player or an NPC.
     * @arg Entity inflictor - Death inflictor. The entity that did the killing. Not necessarily a weapon.
     */
    OnNPCKilled(npc: NPC, attacker: Entity, inflictor: Entity): void;

    /**
     * Called when a player freezes an entity with the physgun.
     *
     * @arg Entity weapon - The weapon that was used to freeze the entity.
     * @arg PhysObj physobj - Physics object of the entity.
     * @arg Entity ent - The target entity.
     * @arg Player ply - The player who tried to freeze the entity.
     * @return {boolean} Allows you to override whether the player can freeze the entity
     */
    OnPhysgunFreeze(weapon: Entity, physobj: PhysObj, ent: Entity, ply: Player): boolean;

    /**
     * Called to when a player has successfully picked up an entity with their Physics Gun.
     *
     * @arg Player ply - The player that has picked up something using the physics gun.
     * @arg Entity ent - The entity that was picked up.
     */
    OnPhysgunPickup(ply: Player, ent: Entity): void;

    /**
     * Called when a player reloads with the physgun. Override this to disable default unfreezing behavior.
     *
     * @arg Weapon physgun - The physgun in question
     * @arg Player ply - The player wielding the physgun
     * @return {boolean} Whether the player can reload with the physgun or not
     */
    OnPhysgunReload(physgun: Weapon, ply: Player): boolean;

    /**
     * Called when a player has changed team using GM:PlayerJoinTeam.
     *
     * @arg Player ply - Player who has changed team
     * @arg number oldTeam - Index of the team the player was originally in
     * @arg number newTeam - Index of the team the player has changed to
     */
    OnPlayerChangedTeam(ply: Player, oldTeam: number, newTeam: number): void;

    /**
     * Called whenever a player sends a chat message. For the serverside equivalent, see GM:PlayerSay.
     *
     * @arg Player ply - The player
     * @arg string text - The message's text
     * @arg boolean teamChat - Is the player typing in team chat?
     * @arg boolean isDead - Is the player dead?
     * @return {boolean} Should the message be suppressed?
     */
    OnPlayerChat(ply: Player, text: string, teamChat: boolean, isDead: boolean): boolean;

    /**
     * Called when a player makes contact with the ground after a jump or a fall.
     *
     * @arg Entity player - Player
     * @arg boolean inWater - Did the player land in water?
     * @arg boolean onFloater - Did the player land on an object floating in the water?
     * @arg number speed - The speed at which the player hit the ground
     * @return {boolean} Return true to suppress default action
     */
    OnPlayerHitGround(player: Entity, inWater: boolean, onFloater: boolean, speed: number): boolean;

    /**
     * Called when a player +use drops an entity.
     *
     * @arg Player ply - The player that dropped the object
     * @arg Entity ent - The object that was dropped.
     * @arg boolean thrown - Whether the object was throw or simply let go of.
     */
    OnPlayerPhysicsDrop(ply: Player, ent: Entity, thrown: boolean): void;

    /**
     * Called when a player +use pickups up an entity. This will be called after the entity passes though GM:AllowPlayerPickup.
     *
     * @arg Player ply - The player that picked up the object
     * @arg Entity ent - The object that was picked up.
     */
    OnPlayerPhysicsPickup(ply: Player, ent: Entity): void;

    /**
     * Called when gamemode has been reloaded by auto refresh.
     */
    OnReloaded(): void;

    /**
     * Called when the player's screen resolution of the game changes.
     * ScrW and ScrH will return the new values when this hook is called.
     *
     * @arg number oldWidth - The previous width of the game's window.
     * @arg number oldHeight - The previous height of the game's window.
     */
    OnScreenSizeChanged(oldWidth: number, oldHeight: number): void;

    /**
     * Called when a player releases the +menu bind on their keyboard, which is bound to Q by default.
     */
    OnSpawnMenuClose(): void;

    /**
     * Called when a player presses the +menu bind on their keyboard, which is bound to Q by default.
     */
    OnSpawnMenuOpen(): void;

    /**
     * Called when a DTextEntry gets focus.
     *
     * @arg Panel panel - The panel that got focus
     */
    OnTextEntryGetFocus(panel: Panel): void;

    /**
     * Called when DTextEntry loses focus.
     *
     * @arg Panel panel - The panel that lost focus
     */
    OnTextEntryLoseFocus(panel: Panel): void;

    /**
     * Called when the player undoes something.
     *
     * @arg string name - The name of the undo action
     * @arg string customText - The custom text for the undo, set by undo.SetCustomUndoText
     * @return {boolean} Return false to suppress the undo notification.
     */
    OnUndo(name: string, customText: string): boolean;

    /**
     * Called when the player changes their weapon to another one - and their viewmodel model changes.
     *
     * @arg Entity viewmodel - The viewmodel that is changing
     * @arg string oldModel - The old model
     * @arg string newModel - The new model
     */
    OnViewModelChanged(viewmodel: Entity, oldModel: string, newModel: string): void;

    /**
     * Called when a player drops an entity with the Physgun.
     *
     * @arg Player ply - The player who dropped an entitiy
     * @arg Entity ent - The dropped entity
     */
    PhysgunDrop(ply: Player, ent: Entity): void;

    /**
     * Called to determine if a player should be able to pick up an entity with the Physics Gun.
     *
     * @arg Player player - The player that is picking up using the Physics Gun.
     * @arg Entity entity - The entity that is being picked up.
     * @return {boolean} Returns whether the player can pick up the entity or not.
     */
    PhysgunPickup(player: Player, entity: Entity): boolean;

    /**
     * Called after player's reserve ammo count changes.
     *
     * @arg Player ply - The player whose ammo is being affected.
     * @arg number ammoID - The ammo type ID.
     * @arg number oldCount - The old ammo count.
     * @arg number newCount - The new ammo count.
     */
    PlayerAmmoChanged(ply: Player, ammoID: number, oldCount: number, newCount: number): void;

    /**
     * Called after the player is authenticated by Steam. This hook will also be called in singleplayer. See also GM:NetworkIDValidated
     *
     * @arg Player ply - The player
     * @arg string steamid - The player's SteamID
     * @arg string uniqueid - The player's UniqueID
     */
    PlayerAuthed(ply: Player, steamid: string, uniqueid: string): void;

    /**
     * Runs when a bind has been pressed. Allows to block commands.
     *
     * @arg Player ply - The player who used the command; this will always be equal to LocalPlayer.
     * @arg string bind - The bind command.
     * @arg boolean pressed - If the bind was activated or deactivated.
     * @arg number code - The button code. See BUTTON_CODE Enums.
     * @return {boolean} Return true to prevent the bind.
     */
    PlayerBindPress(ply: Player, bind: string, pressed: boolean, code: number): boolean;

    /**
     * Called when a player presses a button.
     *
     * @arg Player ply - Player who pressed the button
     * @arg number button - The button, see Enums/BUTTON_CODE
     */
    PlayerButtonDown(ply: Player, button: number): void;

    /**
     * Called when a player releases a button.
     *
     * @arg Player ply - Player who released the button
     * @arg number button - The button, see Enums/BUTTON_CODE
     */
    PlayerButtonUp(ply: Player, button: number): void;

    /**
     * Decides whether a player can hear another player using voice chat.
     *
     * @arg Player listener - The listening player.
     * @arg Player talker - The talking player.
     * @return {boolean} Return true if the listener should hear the talker, false if they shouldn't.
     * @return {boolean} 3D sound. If set to true, will fade out the sound the further away listener is from the  talker, the voice will also be in stereo, and not mono.
     */
    PlayerCanHearPlayersVoice(listener: Player, talker: Player): LuaMultiReturn<[boolean, boolean]>;

    /**
     * Returns whether or not a player is allowed to join a team
     *
     * @arg Player ply - Player attempting to switch teams
     * @arg number team - Index of the team
     * @return {boolean} Allowed to switch
     */
    PlayerCanJoinTeam(ply: Player, team: number): boolean;

    /**
     * Returns whether or not a player is allowed to pick an item up. (ammo, health, armor)
     *
     * @arg Player ply - Player attempting to pick up
     * @arg Entity item - The item the player is attempting to pick up
     * @return {boolean} Allow pick up
     */
    PlayerCanPickupItem(ply: Player, item: Entity): boolean;

    /**
     * Returns whether or not a player is allowed to pick up a weapon.
     *
     * @arg Player ply - The player attempting to pick up the weapon.
     * @arg Weapon weapon - The weapon entity in question.
     * @return {boolean} Allowed pick up or not.
     */
    PlayerCanPickupWeapon(ply: Player, weapon: Weapon): boolean;

    /**
     * Returns whether or not the player can see the other player's chat.
     *
     * @arg string text - The chat text
     * @arg boolean teamOnly - If the message is team-only
     * @arg Player listener - The player receiving the message
     * @arg Player speaker - The player sending the message.
     * @return {boolean} Can see other player's chat
     */
    PlayerCanSeePlayersChat(text: string, teamOnly: boolean, listener: Player, speaker: Player): boolean;

    /**
     * Called when a player has changed team using Player:SetTeam.
     *
     * @arg Player ply - Player whose team has changed.
     * @arg number oldTeam - Index of the team the player was originally in. See team.GetName and the team library.
     * @arg number newTeam - Index of the team the player has changed to.
     */
    PlayerChangedTeam(ply: Player, oldTeam: number, newTeam: number): void;

    /**
     * Called whenever a player is about to spawn something to see if they hit a limit for whatever they are spawning.
     *
     * @arg Player ply - The player who is trying to spawn something.
     * @arg string limitName - The limit's name.
     * @arg number current - The amount of whatever player is trying to spawn that the player already has spawned.
     * @arg number defaultMax - The default maximum count, as dictated by the sbox_max<limitName> convar on the server. This is the amount that will be used if nothing is returned from this hook.
     * @return {boolean} Return false to indicate the limit was hit, or nothing otherwise
     */
    PlayerCheckLimit(ply: Player, limitName: string, current: number, defaultMax: number): boolean;

    /**
 * Called whenever a player's class is changed on the server-side with player_manager.SetPlayerClass.
* 
* @arg Player ply - The player whose class has been changed.
* @arg number newID - The network ID of the player class's name string, or 0 if we are clearing a player class from the player.
Pass this into util.NetworkIDToString to retrieve the proper name of the player class.
 */
    PlayerClassChanged(ply: Player, newID: number): void;

    /**
 * Executes when a player connects to the server. Called before the player has been assigned a UserID and entity. See the player_connect gameevent for a version of this hook called after the player entity has been created.
* 
* @arg string name - The player's name.
* @arg string ip - The player's IP address. Will be "none" for bots.
This argument will only be passed serverside.
 */
    PlayerConnect(name: string, ip: string): void;

    /**
     * Called when a player is killed by Player:Kill or any other normal means.
     *
     * @arg Player victim - The player who died
     * @arg Entity inflictor - Item used to kill the victim
     * @arg Entity attacker - Player or entity that killed the victim
     */
    PlayerDeath(victim: Player, inflictor: Entity, attacker: Entity): void;

    /**
     * Returns whether or not the default death sound should be muted.
     *
     * @arg Player ply - The player
     * @return {boolean} Mute death sound
     */
    PlayerDeathSound(ply: Player): boolean;

    /**
     * Called every think while the player is dead. The return value will determine if the player respawns.
     *
     * @arg Player ply - The player affected in the hook.
     * @return {boolean} Allow spawn
     */
    PlayerDeathThink(ply: Player): boolean;

    /**
     * Called when a player leaves the server. See the player_disconnect gameevent for a shared version of this hook.
     *
     * @arg Player ply - the player
     */
    PlayerDisconnected(ply: Player): void;

    /**
     * Called to update the player's animation during a drive.
     *
     * @arg Player ply - The driving player
     */
    PlayerDriveAnimate(ply: Player): void;

    /**
     * Called when a weapon is dropped by a player via Player:DropWeapon.
     *
     * @arg Player owner - The player who owned this weapon before it was dropped
     * @arg Weapon wep - The weapon that was dropped
     */
    PlayerDroppedWeapon(owner: Player, wep: Weapon): void;

    /**
     * Called when player stops using voice chat.
     *
     * @arg Player ply - Player who stopped talking
     */
    PlayerEndVoice(ply: Player): void;

    /**
     * Called when a player enters a vehicle.
     *
     * @arg Player ply - Player who entered vehicle.
     * @arg Vehicle veh - Vehicle the player entered.
     * @arg number role - The seat number.
     */
    PlayerEnteredVehicle(ply: Player, veh: Vehicle, role: number): void;

    /**
     * Called before firing clientside animation events on a player model.
     *
     * @arg Player ply - The player who has triggered the event.
     * @arg Vector pos - Position of the effect
     * @arg Angle ang - Angle of the effect
     * @arg number event - The event ID of happened even. See this page.
     * @arg string name - Name of the event
     * @return {boolean} Return true to disable the effect
     */
    PlayerFireAnimationEvent(ply: Player, pos: Vector, ang: Angle, event: number, name: string): boolean;

    /**
     * Called whenever a player steps. Return true to mute the normal sound.
     *
     * @arg Player ply - The stepping player
     * @arg Vector pos - The position of the step
     * @arg number foot - Foot that is stepped. 0 for left, 1 for right
     * @arg string sound - Sound that is going to play
     * @arg number volume - Volume of the footstep
     * @arg CRecipientFilter filter - The Recipient filter of players who can hear the footstep
     * @return {boolean} Prevent default step sound
     */
    PlayerFootstep(
        ply: Player,
        pos: Vector,
        foot: number,
        sound: string,
        volume: number,
        filter: CRecipientFilter
    ): boolean;

    /**
     * Called when a player freezes an object.
     *
     * @arg Player ply - Player who has frozen an object
     * @arg Entity ent - The frozen object
     * @arg PhysObj physobj - The frozen physics object of the frozen entity ( For ragdolls )
     */
    PlayerFrozeObject(ply: Player, ent: Entity, physobj: PhysObj): void;

    /**
     * Called before firing serverside animation events on the player models.
     *
     * @arg Player ply - The player who has triggered the event.
     * @arg number event - The event ID of happened even. See this page.
     * @arg number eventTime - The absolute time this event occurred using CurTime.
     * @arg number cycle - The frame this event occurred as a number between 0 and 1.
     * @arg number type - Event type. See the Source SDK.
     * @arg string options - Name or options of this event.
     */
    PlayerHandleAnimEvent(
        ply: Player,
        event: number,
        eventTime: number,
        cycle: number,
        type: number,
        options: string
    ): void;

    /**
     * Called when a player gets hurt.
     *
     * @arg Player victim - Victim
     * @arg Entity attacker - Attacker Entity
     * @arg number healthRemaining - Remaining Health
     * @arg number damageTaken - Damage Taken
     */
    PlayerHurt(victim: Player, attacker: Entity, healthRemaining: number, damageTaken: number): void;

    /**
     * Called when the player spawns for the first time.
     *
     * @arg Player player - The player who spawned.
     * @arg boolean transition - If true, the player just spawned from a map transition.
     */
    PlayerInitialSpawn(player: Player, transition: boolean): void;

    /**
     * Makes the player join a specified team. This is a convenience function that calls Player:SetTeam and runs the GM:OnPlayerChangedTeam hook.
     *
     * @arg Player ply - Player to force
     * @arg number team - The team to put player into
     */
    PlayerJoinTeam(ply: Player, team: number): void;

    /**
     * Called when a player leaves a vehicle.
     *
     * @arg Player ply - Player who left a vehicle.
     * @arg Vehicle veh - Vehicle the player left.
     */
    PlayerLeaveVehicle(ply: Player, veh: Vehicle): void;

    /**
     * Called to give players the default set of weapons.
     *
     * @arg Player ply - Player to give weapons to.
     */
    PlayerLoadout(ply: Player): void;

    /**
     * Called when a player tries to switch noclip mode.
     *
     * @arg Player ply - The person who entered/exited noclip
     * @arg boolean desiredState - Represents the noclip state (on/off) the user will enter if this hook allows them to.
     * @return {boolean} Return false to disallow the switch.
     */
    PlayerNoClip(ply: Player, desiredState: boolean): boolean;

    /**
     * Called after the player's think.
     *
     * @arg Player ply - The player
     */
    PlayerPostThink(ply: Player): void;

    /**
     * Request a player to join the team. This function will check if the team is available to join or not.
     *
     * @arg Player ply - The player to try to put into a team
     * @arg number team - Team to put the player into if the checks succeeded
     */
    PlayerRequestTeam(ply: Player, team: number): void;

    /**
     * Called when a player dispatched a chat message. For the clientside equivalent, see GM:OnPlayerChat.
     *
     * @arg Player sender - The player which sent the message.
     * @arg string text - The message's content.
     * @arg boolean teamChat - Return false when the message is for everyone, true when the message is for the sender's team.
     * @return {string} What to show instead of original text. Set to "" to stop the message from displaying.
     */
    PlayerSay(sender: Player, text: string, teamChat: boolean): string;

    /**
     * Called to determine a spawn point for a player to spawn at.
     *
     * @arg Player ply - The player who needs a spawn point
     * @arg boolean transition - If true, the player just spawned from a map transition (trigger_changelevel). You probably want to not return an entity for that case to not override player's position.
     * @return {Entity} The spawnpoint entity to spawn the player at
     */
    PlayerSelectSpawn(ply: Player, transition: boolean): Entity;

    /**
     * Find a team spawn point entity for this player.
     *
     * @arg number team - Players team
     * @arg Player ply - The player
     * @return {Entity} The entity to use as a spawn point.
     */
    PlayerSelectTeamSpawn(team: number, ply: Player): Entity;

    /**
     * Called whenever view model hands needs setting a model. By default this calls PLAYER:GetHandsModel and if that fails, sets the hands model according to his player model.
     *
     * @arg Player ply - The player whose hands needs a model set
     * @arg Entity ent - The hands to set model of
     */
    PlayerSetHandsModel(ply: Player, ent: Entity): void;

    /**
     * Called whenever a player spawns and must choose a model. A good place to assign a model to a player.
     *
     * @arg Player ply - The player being chosen
     */
    PlayerSetModel(ply: Player): void;

    /**
     * Returns true if the player should take damage from the given attacker.
     *
     * @arg Player ply - The player
     * @arg Entity attacker - The attacker
     * @return {boolean} Allow damage
     */
    PlayerShouldTakeDamage(ply: Player, attacker: Entity): boolean;

    /**
     * Allows to suppress player taunts.
     *
     * @arg Player ply - Player who tried to taunt
     * @arg number act - Act ID of the taunt player tries to do, see Enums/ACT
     * @return {boolean} Return false to disallow player taunting
     */
    PlayerShouldTaunt(ply: Player, act: number): boolean;

    /**
     * Called when the player is killed by Player:KillSilent.
     *
     * @arg Player ply - The player who was killed
     */
    PlayerSilentDeath(ply: Player): void;

    /**
     * Called whenever a player spawns, including respawns.
     *
     * @arg Player player - The player who spawned.
     * @arg boolean transition - If true, the player just spawned from a map transition. You probably want to not touch player's weapons if this is set to true from this hook.
     */
    PlayerSpawn(player: Player, transition: boolean): void;

    /**
     * Called to spawn the player as a spectator.
     *
     * @arg Player ply - The player to spawn as a spectator
     */
    PlayerSpawnAsSpectator(ply: Player): void;

    /**
     * Determines if the player can spray using the impulse 201 console command.
     *
     * @arg Player sprayer - The player.
     * @return {boolean} Return false to allow spraying, return true to prevent spraying.
     */
    PlayerSpray(sprayer: Player): boolean;

    /**
     * Called when player starts taunting.
     *
     * @arg Player ply - The player who is taunting
     * @arg number act - The sequence ID of the taunt
     * @arg number length - Length of the taunt
     */
    PlayerStartTaunt(ply: Player, act: number, length: number): void;

    /**
     * Called when a player starts using voice chat.
     *
     * @arg Player ply - Player who started using voice chat.
     * @return {boolean} Set true to hide player's CHudVoiceStatus.
     */
    PlayerStartVoice(ply: Player): boolean;

    /**
     * Allows you to override the time between footsteps.
     *
     * @arg Player ply - Player who is walking
     * @arg number type - The type of footsteps, see Enums/STEPSOUNDTIME
     * @arg boolean walking - Is the player walking or not ( +walk? )
     * @return {number} Time between footsteps, in ms
     */
    PlayerStepSoundTime(ply: Player, type: number, walking: boolean): number;

    /**
     * Called whenever a player attempts to either turn on or off their flashlight, returning false will deny the change.
     *
     * @arg Player ply - The player who attempts to change their flashlight state.
     * @arg boolean enabled - The new state the player requested, true for on, false for off.
     * @return {boolean} Can toggle the flashlight or not
     */
    PlayerSwitchFlashlight(ply: Player, enabled: boolean): boolean;

    /**
 * Called when a player attempts to switch their weapon.
* 
* @arg Player player - The player switching weapons.
* @arg Weapon oldWeapon - The previous weapon. Will be NULL if the previous weapon was removed or the player is switching from nothing.
* @arg Weapon newWeapon - The weapon the player switched to. Will be NULL if the player is switching to nothing.
This can be NULL on the client if the weapon hasn't been created over the network yet.Issue Tracker: 2922
* @return {boolean} Return true to prevent weapon switch.
 */
    PlayerSwitchWeapon(player: Player, oldWeapon: Weapon, newWeapon: Weapon): boolean;

    /**
     * The Move hook is called for you to manipulate the player's CMoveData. This hook is called moments before GM:Move and GM:PlayerNoClip.
     *
     * @arg Player player - The player
     * @arg CMoveData mv - The current movedata for the player.
     */
    PlayerTick(player: Player, mv: CMoveData): void;

    /**
     * Called when a player has been hit by a trace and damaged (such as from a bullet). Returning true overrides the damage handling and prevents GM:ScalePlayerDamage from being called.
     *
     * @arg Player ply - The player that has been hit
     * @arg CTakeDamageInfo dmginfo - The damage info of the bullet
     * @arg Vector dir - Normalized vector direction of the bullet's path
     * @arg TraceResultStruct trace - The trace of the bullet's path, see Structures/TraceResult
     * @return {boolean} Override engine handling
     */
    PlayerTraceAttack(ply: Player, dmginfo: CTakeDamageInfo, dir: Vector, trace: TraceResultStruct): boolean;

    /**
     * Called when a player unfreezes an object.
     *
     * @arg Player ply - Player who has unfrozen an object
     * @arg Entity ent - The unfrozen object
     * @arg PhysObj physobj - The frozen physics object of the unfrozen entity ( For ragdolls )
     */
    PlayerUnfrozeObject(ply: Player, ent: Entity, physobj: PhysObj): void;

    /**
 * Triggered when the player presses use on an object. Continuously runs until USE is released but will not activate other Entities until the USE key is released; dependent on activation type of the Entity.
* 
* @arg Player ply - The player pressing the "use" key.
* @arg Entity ent - The entity which the player is looking at / activating USE on.
* @return {boolean} Return false if the player is not allowed to USE the entity.
Do not return true if using a hook, otherwise other mods may not get a chance to block a player's use.
 */
    PlayerUse(ply: Player, ent: Entity): boolean;

    /**
     * Called when it's time to populate the context menu menu bar at the top.
     *
     * @arg Panel menubar - The DMenuBar itself.
     */
    PopulateMenuBar(menubar: Panel): void;

    /**
     * Called right after the map has cleaned up (usually because game.CleanUpMap was called)
     */
    PostCleanupMap(): void;

    /**
     * Called right after the 2D skybox has been drawn - allowing you to draw over it.
     */
    PostDraw2DSkyBox(): void;

    /**
     * Called after rendering effects. This is where halos are drawn. Called just before GM:PreDrawHUD.
     */
    PostDrawEffects(): void;

    /**
     * Called after GM:PreDrawHUD,  GM:HUDPaintBackground and GM:HUDPaint but before  GM:DrawOverlay.
     */
    PostDrawHUD(): void;

    /**
 * Called after drawing opaque entities.
* 
* @arg boolean bDrawingDepth - Whether the current draw is writing depth.
* @arg boolean bDrawingSkybox - Whether the current draw is drawing the 3D or 2D skybox.
In case of 2D skyboxes it is possible for this hook to always be called with this parameter set to true.
* @arg boolean isDraw3DSkybox - Whether the current draw is drawing the 3D.
 */
    PostDrawOpaqueRenderables(bDrawingDepth: boolean, bDrawingSkybox: boolean, isDraw3DSkybox: boolean): void;

    /**
     * Called after the player hands are drawn.
     *
     * @arg Entity hands - This is the gmod_hands entity.
     * @arg Entity vm - This is the view model entity.
     * @arg Player ply - The the owner of the view model.
     * @arg Weapon weapon - This is the weapon that is from the view model.
     */
    PostDrawPlayerHands(hands: Entity, vm: Entity, ply: Player, weapon: Weapon): void;

    /**
     * Called after drawing the 3D skybox. This will not be called if skybox rendering was prevented via the GM:PreDrawSkyBox hook.
     */
    PostDrawSkyBox(): void;

    /**
 * Called after all translucent entities are drawn.
* 
* @arg boolean bDrawingDepth - Whether the current call is writing depth.
* @arg boolean bDrawingSkybox - Whether the current draw is drawing the 3D or 2D skybox.
In case of 2D skyboxes it is possible for this hook to always be called with this parameter set to true.
* @arg boolean isDraw3DSkybox - Whether the current draw is drawing the 3D.
 */
    PostDrawTranslucentRenderables(bDrawingDepth: boolean, bDrawingSkybox: boolean, isDraw3DSkybox: boolean): void;

    /**
     * Called after view model is drawn.
     *
     * @arg Entity viewmodel - Players view model
     * @arg Player player - The owner of the weapon/view model
     * @arg Weapon weapon - The weapon the player is currently holding
     */
    PostDrawViewModel(viewmodel: Entity, player: Player, weapon: Weapon): void;

    /**
     * Called when an entity receives a damage event, after passing damage filters, etc.
     *
     * @arg Entity ent - The entity that took the damage.
     * @arg CTakeDamageInfo dmg
     * @arg boolean took - Whether the entity actually took the damage. (For example, shooting a Strider will generate this event, but it won't take bullet damage).
     */
    PostEntityTakeDamage(ent: Entity, dmg: CTakeDamageInfo, took: boolean): void;

    /**
     * Called after the gamemode has loaded.
     */
    PostGamemodeLoaded(): void;

    /**
     * Called right after GM:DoPlayerDeath, GM:PlayerDeath and GM:PlayerSilentDeath.
     *
     * @arg Player ply - The player
     */
    PostPlayerDeath(ply: Player): void;

    /**
     * Called after the player was drawn.
     *
     * @arg Player ply - The player that was drawn.
     * @arg number flags - The STUDIO_ flags for this render operation.
     */
    PostPlayerDraw(ply: Player, flags: number): void;

    /**
     * Allows you to suppress post processing effect drawing.
     *
     * @arg string ppeffect - The classname of Post Processing effect
     * @return {boolean} Return true/false depending on whether this post process should be allowed
     */
    PostProcessPermitted(ppeffect: string): boolean;

    /**
     * Called after the frame has been rendered.
     */
    PostRender(): void;

    /**
     * Called after the VGUI has been drawn.
     */
    PostRenderVGUI(): void;

    /**
     * Called just after performing an undo.
     *
     * @arg UndoStruct undo - The undo table. See Undo struct.
     * @arg number count - The amount of props/actions undone. This will be 0 for undos that are skipped in cases where for example the entity that is meant to be undone is already deleted.
     */
    PostUndo(undo: UndoStruct, count: number): void;

    /**
     * Called right before the map cleans up (usually because game.CleanUpMap was called)
     */
    PreCleanupMap(): void;

    /**
     * Called just after GM:PreDrawViewModel and can technically be considered "PostDrawAllViewModels".
     */
    PreDrawEffects(): void;

    /**
     * Called before rendering the halos. This is the place to call halo.Add. This hook is actually running inside of GM:PostDrawEffects.
     */
    PreDrawHalos(): void;

    /**
     * Called just after GM:PostDrawEffects. Drawing anything in it seems to work incorrectly.
     */
    PreDrawHUD(): void;

    /**
 * Called before all opaque entities are drawn.
* 
* @arg boolean isDrawingDepth - Whether the current draw is writing depth.
* @arg boolean isDrawSkybox - Whether the current draw is drawing the 3D or 2D skybox.
In case of 2D skyboxes it is possible for this hook to always be called with this parameter set to true.
* @arg boolean isDraw3DSkybox - Whether the current draw is drawing the 3D.
* @return {boolean} Return true to prevent opaque renderables from drawing.
 */
    PreDrawOpaqueRenderables(isDrawingDepth: boolean, isDrawSkybox: boolean, isDraw3DSkybox: boolean): boolean;

    /**
     * Called before the player hands are drawn.
     *
     * @arg Entity hands - This is the gmod_hands entity before it is drawn.
     * @arg Entity vm - This is the view model entity before it is drawn.
     * @arg Player ply - The the owner of the view model.
     * @arg Weapon weapon - This is the weapon that is from the view model.
     * @return {boolean} Return true to prevent the viewmodel hands from rendering
     */
    PreDrawPlayerHands(hands: Entity, vm: Entity, ply: Player, weapon: Weapon): boolean;

    /**
     * Called before the 3D sky box is drawn. This will not be called for maps with no 3D skybox, or when the 3d skybox is disabled. (r_3dsky 0)
     *
     * @return {boolean} Return true to disable skybox drawing (both 2D and 3D skybox)
     */
    PreDrawSkyBox(): boolean;

    /**
 * Called before all the translucent entities are drawn.
* 
* @arg boolean isDrawingDepth - Whether the current draw is writing depth.
* @arg boolean isDrawSkybox - Whether the current draw is drawing the 3D or 2D skybox.
In case of 2D skyboxes it is possible for this hook to always be called with this parameter set to true.
* @arg boolean isDraw3DSkybox - Whether the current draw is drawing the 3D.
* @return {boolean} Return true to prevent translucent renderables from drawing.
 */
    PreDrawTranslucentRenderables(isDrawingDepth: boolean, isDrawSkybox: boolean, isDraw3DSkybox: boolean): boolean;

    /**
     * Called before the view model has been drawn. This hook by default also calls this on weapons, so you can use WEAPON:PreDrawViewModel.
     *
     * @arg Entity vm - This is the view model entity before it is drawn. On server-side, this entity is the predicted view model.
     * @arg Player ply - The owner of the view model.
     * @arg Weapon weapon - This is the weapon that is from the view model.
     * @return {boolean} Return true to prevent the default view model rendering. This also affects GM:PostDrawViewModel.
     */
    PreDrawViewModel(vm: Entity, ply: Player, weapon: Weapon): boolean;

    /**
     * Called before view models and entities with RENDERGROUP_VIEWMODEL are drawn.
     */
    PreDrawViewModels(): void;

    /**
     * Called before the gamemode is loaded.
     */
    PreGamemodeLoaded(): void;

    /**
     * Called before the player is drawn.
     *
     * @arg Player player - The player that is about to be drawn.
     * @arg number flags - The STUDIO_ flags for this render operation.
     * @return {boolean} Prevent default player rendering. Return true to hide the player.
     */
    PrePlayerDraw(player: Player, flags: number): boolean;

    /**
     * Called by scripted_ents.Register.
     *
     * @arg object ent - The entity table to be registered.
     * @arg string _class - The class name to be assigned.
     * @return {boolean} Return false to prevent the entity from being registered. Returning any other value has no effect.
     */
    PreRegisterSENT(ent: object, _class: string): boolean;

    /**
     * Called when a Scripted Weapon (SWEP) is about to be registered, allowing addons to alter the weapon's SWEP table with custom data for later usage. Called internally from weapons.Register.
     *
     * @arg object swep - The SWEP table to be registered.
     * @arg string _class - The class name to be assigned.
     * @return {boolean} Return false to prevent the weapon from being registered. Returning any other value has no effect.
     */
    PreRegisterSWEP(swep: object, _class: string): boolean;

    /**
     * Called before the renderer is about to start rendering the next frame.
     *
     * @return {boolean} Return true to prevent all rendering. This can make the whole game stop rendering anything.
     */
    PreRender(): boolean;

    /**
     * Called just before performing an undo.
     *
     * @arg UndoStruct undo - The undo table. See Undo struct.
     */
    PreUndo(undo: UndoStruct): void;

    /**
     * This will prevent in_attack from sending to server when player tries to shoot from C menu.
     *
     * @return {boolean} Return true to prevent screen clicks.
     */
    PreventScreenClicks(): boolean;

    /**
     * Called when a prop has been destroyed.
     *
     * @arg Player attacker - The person who broke the prop.
     * @arg Entity prop - The entity that has been broken by the attacker.
     */
    PropBreak(attacker: Player, prop: Entity): void;

    /**
     * Render the scene. Used by the Stereoscopy post-processing effect.
     *
     * @arg Vector origin - View origin
     * @arg Angle angles - View angles
     * @arg number fov - View FOV
     * @return {boolean} Return true to override drawing the scene.
     */
    RenderScene(origin: Vector, angles: Angle, fov: number): boolean;

    /**
     * Used to render post processing effects.
     */
    RenderScreenspaceEffects(): void;

    /**
     * Called when the game is reloaded from a Source Engine save system ( not the Sandbox saves or dupes ).
     */
    Restored(): void;

    /**
     * Called when the game is saved using the Source Engine save system (not the Sandbox saves or dupes).
     */
    Saved(): void;

    /**
     * Called when an NPC takes damage.
     *
     * @arg NPC npc - The NPC that takes damage
     * @arg number hitgroup - The hitgroup (hitbox) enum where the NPC took damage. See Enums/HITGROUP
     * @arg CTakeDamageInfo dmginfo - Damage info
     */
    ScaleNPCDamage(npc: NPC, hitgroup: number, dmginfo: CTakeDamageInfo): void;

    /**
 * This hook allows you to change how much damage a player receives when one takes damage to a specific body part.
* 
* @arg Player ply - The player taking damage.
* @arg number hitgroup - The hitgroup where the player took damage. See Enums/HITGROUP
* @arg CTakeDamageInfo dmginfo - The damage info.
* @return {boolean} Return true to prevent damage that this hook is called for, stop blood particle effects and blood decals.
It is possible to return true only on client ( This will work only in multiplayer ) to stop the effects but still take damage.
 */
    ScalePlayerDamage(ply: Player, hitgroup: number, dmginfo: CTakeDamageInfo): boolean;

    /**
     * Called when player released the scoreboard button. ( TAB by default )
     */
    ScoreboardHide(): void;

    /**
     * Called when player presses the scoreboard button. ( TAB by default )
     */
    ScoreboardShow(): void;

    /**
     * Sets player run and sprint speeds.
     *
     * @arg Player ply - The player to set the speed of.
     * @arg number walkSpeed - The walk speed.
     * @arg number runSpeed - The run speed.
     */
    SetPlayerSpeed(ply: Player, walkSpeed: number, runSpeed: number): void;

    /**
     * SetupMove is called before the engine process movements. This allows us to override the players movement.
     *
     * @arg Player ply - The player whose movement we are about to process
     * @arg CMoveData mv - The move data to override/use
     * @arg CUserCmd cmd - The command data
     */
    SetupMove(ply: Player, mv: CMoveData, cmd: CUserCmd): void;

    /**
     * Allows you to add extra positions to the player's PVS. This is the place to call AddOriginToPVS.
     *
     * @arg Player ply - The player
     * @arg Entity viewEntity - Players Player:GetViewEntity
     */
    SetupPlayerVisibility(ply: Player, viewEntity: Entity): void;

    /**
     * Allows you to use render.Fog* functions to manipulate skybox fog.
     * This will not be called for maps with no 3D skybox, or when the 3d skybox is disabled. (r_3dsky 0)
     *
     * @arg number scale - The scale of 3D skybox
     * @return {boolean} Return true to tell the engine that fog is set up
     */
    SetupSkyboxFog(scale: number): boolean;

    /**
     * Allows you to use render.Fog* functions to manipulate world fog.
     *
     * @return {boolean} Return true to tell the engine that fog is set up
     */
    SetupWorldFog(): boolean;

    /**
     * Called to decide whether a pair of entities should collide with each other. This is only called if Entity:SetCustomCollisionCheck was used on one or both entities.
     *
     * @arg Entity ent1 - The first entity in the collision poll.
     * @arg Entity ent2 - The second entity in the collision poll.
     * @return {boolean} Whether the entities should collide.
     */
    ShouldCollide(ent1: Entity, ent2: Entity): boolean;

    /**
     * Called to determine if the LocalPlayer should be drawn.
     *
     * @arg Player ply - The player
     * @return {boolean} True to draw the player, false to hide.
     */
    ShouldDrawLocalPlayer(ply: Player): boolean;

    /**
     * Called when a player executes gm_showhelp console command. ( Default bind is F1 )
     *
     * @arg Player ply - Player who executed the command
     */
    ShowHelp(ply: Player): void;

    /**
     * Called when a player executes gm_showspare1 console command ( Default bind is f3 ).
     *
     * @arg Player ply - Player who executed the command.
     */
    ShowSpare1(ply: Player): void;

    /**
     * Called when a player executes gm_showspare2 console command ( Default bind is f4 ).
     *
     * @arg Player ply - Player who executed the command.
     */
    ShowSpare2(ply: Player): void;

    /**
     * Called when a player executes gm_showteam console command. ( Default bind is F2 )
     *
     * @arg Player ply - Player who executed the command
     */
    ShowTeam(ply: Player): void;

    /**
     * Called whenever the Lua environment is about to be shut down, for example on map change, or when the server is going to shut down.
     */
    ShutDown(): void;

    /**
     * Called when spawn icon is generated.
     *
     * @arg string lastmodel - File path of previously generated model.
     * @arg string imagename - File path of the generated icon.
     * @arg number modelsleft - Amount of models left to generate.
     */
    SpawniconGenerated(lastmodel: string, imagename: string, modelsleft: number): void;

    /**
     * Runs when the user tries to open the chat box.
     *
     * @arg boolean isTeamChat - Whether the message was sent through team chat.
     * @return {boolean} Return true to hide the default chat box.
     */
    StartChat(isTeamChat: boolean): boolean;

    /**
     * Allows you to change the players inputs before they are processed by the server.
     *
     * @arg Player ply - The player
     * @arg CUserCmd ucmd - The usercommand
     */
    StartCommand(ply: Player, ucmd: CUserCmd): void;

    /**
     * Called right before an entity starts driving. Overriding this hook will cause it to not call drive.Start and the player will not begin driving the entity.
     *
     * @arg Entity ent - The entity that is going to be driven
     * @arg Player ply - The player that is going to drive the entity
     */
    StartEntityDriving(ent: Entity, ply: Player): void;

    /**
     * Called when you start a new game via the menu.
     */
    StartGame(): void;

    /**
     * Called every frame on client and server. This will be the same as GM:Tick on the server when there is no lag, but will only be called once every processed server frame during lag.
     */
    Think(): void;

    /**
     * Called every server tick. Serverside, this is similar to GM:Think.
     */
    Tick(): void;

    /**
     * Allows you to translate player activities.
     *
     * @arg Player ply - The player
     * @arg number act - The activity. See Enums/ACT
     * @return {number} The new, translated activity
     */
    TranslateActivity(ply: Player, act: number): number;

    /**
     * Animation updates (pose params etc) should be done here.
     *
     * @arg Player ply - The player to update the animation info for.
     * @arg Vector velocity - The player's velocity.
     * @arg number maxSeqGroundSpeed - Speed of the animation - used for playback rate scaling.
     */
    UpdateAnimation(ply: Player, velocity: Vector, maxSeqGroundSpeed: number): void;

    /**
     * Called when a variable is edited on an Entity (called by Edit Properties... menu). See Editable Entities for more information.
     *
     * @arg Entity ent - The entity being edited
     * @arg Player ply - The player doing the editing
     * @arg string key - The name of the variable
     * @arg string val - The new value, as a string which will later be converted to its appropriate type
     * @arg any editor - The edit table defined in Entity:NetworkVar
     */
    VariableEdited(ent: Entity, ply: Player, key: string, val: string, editor: any): void;

    /**
     * Called when you are driving a vehicle. This hook works just like GM:Move.
     *
     * @arg Player ply - Player who is driving the vehicle
     * @arg Vehicle veh - The vehicle being driven
     * @arg CMoveData mv - Move data
     */
    VehicleMove(ply: Player, veh: Vehicle, mv: CMoveData): void;

    /**
     * Called when user clicks on a VGUI panel.
     *
     * @arg number button - The button that was pressed, see Enums/MOUSE
     * @return {boolean} Return true if the mouse click should be ignored or not.
     */
    VGUIMousePressAllowed(button: number): boolean;

    /**
     * Called when a mouse button is pressed on a VGUI element or menu.
     *
     * @arg Panel pnl - Panel that currently has focus.
     * @arg number mouseCode - The key that the player pressed using Enums/MOUSE.
     */
    VGUIMousePressed(pnl: Panel, mouseCode: number): void;

    /**
     * Called as a weapon entity is picked up by a player.
     *
     * @arg Weapon weapon - The equipped weapon.
     * @arg Player owner - The player that is picking up the weapon.
     */
    WeaponEquip(weapon: Weapon, owner: Player): void;

    /**
     * Called when an addon from the Steam workshop finishes downloading. Used by default to update details on the workshop downloading panel.
     *
     * @arg number id - Workshop ID of addon.
     * @arg string title - Name of addon.
     */
    WorkshopDownloadedFile(id: number, title: string): void;

    /**
 * Called when an addon from the Steam workshop begins downloading. Used by default to place details on the workshop downloading panel.
* 
* @arg number id - Workshop ID of addon.
* @arg number imageID - ID of addon's preview image.
For example, for Extended Spawnmenu addon, the image URL is
http://cloud-4.steamusercontent.com/ugc/702859018846106764/9E7E1946296240314751192DA0AD15B6567FF92D/
So, the value of this argument would be 702859018846106764.
* @arg string title - Name of addon.
* @arg number size - File size of addon in bytes.
 */
    WorkshopDownloadFile(id: number, imageID: number, title: string, size: number): void;

    /**
 * Called while an addon from the Steam workshop is downloading. Used by default to update details on the fancy workshop download panel.
* 
* @arg number id - Workshop ID of addon.
* @arg number imageID - ID of addon's preview image.
For example, for Extended Spawnmenu addon, the image URL is
http://cloud-4.steamusercontent.com/ugc/702859018846106764/9E7E1946296240314751192DA0AD15B6567FF92D/
So, the value of this argument would be 702859018846106764.
* @arg string title - Name of addon.
* @arg number downloaded - Current bytes of addon downloaded.
* @arg number expected - Expected file size of addon in bytes.
 */
    WorkshopDownloadProgress(id: number, imageID: number, title: string, downloaded: number, expected: number): void;

    /**
     * Called after GM:WorkshopStart.
     *
     * @arg number remain - Remaining addons to download
     * @arg number total - Total addons needing to be downloaded
     */
    WorkshopDownloadTotals(remain: number, total: number): void;

    /**
     * Called when downloading content from Steam workshop ends. Used by default to hide fancy workshop downloading panel.
     */
    WorkshopEnd(): void;

    /**
 * Called while an addon from the Steam workshop is extracting. Used by default to update details on the fancy workshop download panel.
* 
* @arg number id - Workshop ID of addon.
* @arg number ImageID - ID of addon's preview image.
For example, for Extended Spawnmenu addon, the image URL is
http://cloud-4.steamusercontent.com/ugc/702859018846106764/9E7E1946296240314751192DA0AD15B6567FF92D/
So, the value of this argument would be 702859018846106764.
* @arg string title - Name of addon.
* @arg number percent - Current bytes of addon extracted.
 */
    WorkshopExtractProgress(id: number, ImageID: number, title: string, percent: number): void;

    /**
     * Called when downloading content from Steam workshop begins. Used by default to show fancy workshop downloading panel.
     */
    WorkshopStart(): void;

    /**
     * Called by the engine when the game initially fetches subscriptions to be displayed on the bottom of the main menu screen.
     *
     * @arg number num - Amount of subscribed addons that have info retrieved.
     * @arg number max - Total amount of subscribed addons that need their info retrieved.
     */
    WorkshopSubscriptionsProgress(num: number, max: number): void;
}

declare type GM__HOOKS = keyof GM;

declare type GAMEMODE = GM;
