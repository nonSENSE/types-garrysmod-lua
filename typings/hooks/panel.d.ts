declare interface PANEL {
    [index: string | number | symbol]: any;

    /**
     * Called whenever a panel receives a command signal from one of its children.
     *
     * @arg string signalName - The name of the signal, usually the sender of the signal or the command name.
     * @arg string signalValue - The value of the signal, usually a command argument.
     */
    ActionSignal(signalName: string, signalValue: string): void;

    /**
     * Called every frame unless Panel:IsVisible is set to false. Similar to PANEL:Think, but can be disabled by Panel:SetAnimationEnabled as explained below.
     */
    AnimationThink(): void;

    /**
     * Called whenever the panel should apply its scheme (colors, fonts, style).
     */
    ApplySchemeSettings(): void;

    /**
     * Called when an object is dragged and hovered over this panel for 0.1 seconds.
     *
     * @arg number hoverTime - The time the object was hovered over this panel.
     */
    DragHoverClick(hoverTime: number): void;

    /**
     * Called when this panel is dropped onto another panel.
     *
     * @arg Panel pnl - The panel we are dropped onto
     */
    DroppedOn(pnl: Panel): void;

    /**
     * Called when the panel should generate example use case / example code to use for this panel. Used in the panel opened by derma_controls console command.
     *
     * @arg string _class - The classname of the panel to generate example for. This will be the class name of your panel.
     * @arg Panel dpropertysheet - A DPropertySheet to add your example to. See examples below.
     * @arg number width - Width of the property sheet?
     * @arg number height - Width of the property sheet?
     */
    GenerateExample(_class: string, dpropertysheet: Panel, width: number, height: number): void;

    /**
     * Called when the panel is created. This is called for each base type that the panel has.
     */
    Init(): void;

    /**
     * Called after Panel:SetCookieName is called on this panel to apply the just loaded cookie values for this panel.
     */
    LoadCookies(): void;

    /**
     * Called when we are activated during level load. Used by the loading screen panel.
     */
    OnActivate(): void;

    /**
     * Called whenever a child was parented to the panel.
     *
     * @arg Panel child - The child which was added.
     */
    OnChildAdded(child: Panel): void;

    /**
     * Called whenever a child of the panel is about to removed.
     *
     * @arg Panel child - The child which is about to be removed.
     */
    OnChildRemoved(child: Panel): void;

    /**
     * Called whenever the cursor entered the panels bounds.
     */
    OnCursorEntered(): void;

    /**
     * Called whenever the cursor left the panels bounds.
     */
    OnCursorExited(): void;

    /**
     * Called whenever the cursor was moved with the panels bounds.
     *
     * @arg number cursorX - The new x position of the cursor relative to the panels origin.
     * @arg number cursorY - The new y position of the cursor relative to the panels origin.
     * @return {boolean} Return true to suppress default action.
     */
    OnCursorMoved(cursorX: number, cursorY: number): boolean;

    /**
     * Called when we are deactivated during level load. Used by the loading screen panel.
     */
    OnDeactivate(): void;

    /**
     * We're being dropped on something
     * We can create a new panel here and return it, so that instead of dropping us - it drops the new panel instead! We remain where we are!
     *
     * @return {Panel} The panel to drop instead of us. By default you should return self.
     */
    OnDrop(): Panel;

    /**
     * Called whenever the panel gained or lost focus.
     *
     * @arg boolean gained - If the focus was gained ( true ) or lost ( false )
     */
    OnFocusChanged(gained: boolean): void;

    /**
     * Called whenever a keyboard key was pressed while the panel is focused.
     *
     * @arg number keyCode - The key code of the pressed key, see Enums/KEY.
     * @return {boolean} Return true to suppress default action.
     */
    OnKeyCodePressed(keyCode: number): boolean;

    /**
     * Called whenever a keyboard key was released while the panel is focused.
     *
     * @arg number keyCode - The key code of the released key, see Enums/KEY.
     * @return {boolean} Return true to suppress default action.
     */
    OnKeyCodeReleased(keyCode: number): boolean;

    /**
     * Called whenever a mouse key was pressed while the panel is focused.
     *
     * @arg number keyCode - They key code of the key pressed, see Enums/MOUSE.
     * @return {boolean} Return true to suppress default action such as right click opening edit menu for DTextEntry.
     */
    OnMousePressed(keyCode: number): boolean;

    /**
     * Called whenever a mouse key was released while the panel is focused.
     *
     * @arg number keyCode - They key code of the key released, see Enums/MOUSE.
     * @return {boolean} Return true to suppress default action.
     */
    OnMouseReleased(keyCode: number): boolean;

    /**
     * Called whenever the mouse wheel was used.
     *
     * @arg number scrollDelta - The scroll delta, indicating how much the user turned the mouse wheel.
     * @return {boolean} Return true to suppress default action.
     */
    OnMouseWheeled(scrollDelta: number): boolean;

    /**
     * Called when the panel is about to be removed.
     */
    OnRemove(): void;

    /**
     * Called when the player's screen resolution of the game changes.
     *
     * @arg number oldWidth - The previous width  of the game's window
     * @arg number oldHeight - The previous height of the game's window
     */
    OnScreenSizeChanged(oldWidth: number, oldHeight: number): void;

    /**
     * Called just after the panel size changes.
     *
     * @arg number newWidth - The new width of the panel
     * @arg number newHeight - The new height of the panel
     */
    OnSizeChanged(newWidth: number, newHeight: number): void;

    /**
     * Called by dragndrop.StartDragging when the panel starts being dragged.
     */
    OnStartDragging(): void;

    /**
     * Called by Panel:DragMouseRelease when the panel object is released after being dragged.
     */
    OnStopDragging(): void;

    /**
     * Called whenever the panel should be drawn.
     *
     * @arg number width - The panel's width.
     * @arg number height - The panel's height.
     * @return {boolean} Returning true prevents the background from being drawn.
     */
    Paint(width: number, height: number): boolean;

    /**
     * Called whenever the panel and all its children were drawn, return true to override the default drawing.
     *
     * @arg number width - The panels current width.
     * @arg number height - The panels current height.
     * @return {boolean} Should we disable default PaintOver rendering? This is useful in case with Derma panels that use Derma hooks.
     */
    PaintOver(width: number, height: number): boolean;

    /**
     * Called whenever the panels layout was invalidated. This means all child panels must be re-positioned to fit the possibly new size of this panel.
     *
     * @arg number width - The panels current width.
     * @arg number height - The panels current height.
     */
    PerformLayout(width: number, height: number): void;

    /**
     * Only works on elements defined with derma.DefineControl and only if the panel has AllowAutoRefresh set to true.
     */
    PostAutoRefresh(): void;

    /**
     * Only works on elements defined with derma.DefineControl and only if the panel has AllowAutoRefresh set to true.
     */
    PreAutoRefresh(): void;

    /**
     * Called to test if the panel is being hovered by the mouse. This will only be called if the panel's parent is being hovered.
     *
     * @arg number x - The x coordinate of the cursor, in screen space.
     * @arg number y - The y coordinate of the cursor, in screen space.
     * @return {boolean} Return false when the cursor is not considered on the panel, true if it is considered on the panel. Do not return anything for default behavior.
     */
    TestHover(x: number, y: number): boolean;

    /**
     * Called every frame while Panel:IsVisible is true.
     */
    Think(): void;
}

declare type PANEL__HOOKS = keyof PANEL;
