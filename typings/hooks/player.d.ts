declare interface PLAYER {
    [index: string | number | symbol]: any;

    /**
     * Called when the player's class was changed from this class.
     */
    ClassChanged(): void;

    /**
     * Called when the player dies
     */
    Death(): void;

    /**
     * Called from GM:FinishMove.
     *
     * @arg CMoveData mv
     * @return {boolean} Return true to prevent default action
     */
    FinishMove(mv: CMoveData): boolean;

    /**
     * Called on player spawn to determine which hand model to use
     *
     * @return {object} A table containing info about view model hands model to be set. See examples.
     */
    GetHandsModel(): object;

    /**
     * Called when the class object is created
     */
    Init(): void;

    /**
     * Called on spawn to give the player their default loadout
     */
    Loadout(): void;

    /**
     * Called from GM:Move.
     *
     * @arg CMoveData mv - Movement information
     * @return {boolean} Return true to prevent default action
     */
    Move(mv: CMoveData): boolean;

    /**
     * Called after the viewmodel has been drawn
     *
     * @arg Entity viewmodel - The viewmodel
     * @arg Entity weapon - The weapon
     */
    PostDrawViewModel(viewmodel: Entity, weapon: Entity): void;

    /**
     * Called before the viewmodel is drawn
     *
     * @arg Entity viewmodel - The viewmodel
     * @arg Entity weapon - The weapon
     */
    PreDrawViewModel(viewmodel: Entity, weapon: Entity): void;

    /**
     * Called when we need to set player model from the class.
     */
    SetModel(): void;

    /**
     * Setup the network table accessors.
     */
    SetupDataTables(): void;

    /**
     * Called when the player spawns
     */
    Spawn(): void;

    /**
     * Called from GM:CreateMove.
     *
     * @arg CMoveData mv
     * @arg CUserCmd cmd
     * @return {boolean} Return true to prevent default action
     */
    StartMove(mv: CMoveData, cmd: CUserCmd): boolean;

    /**
     * Called when the player changes their weapon to another one causing their viewmodel model to change
     *
     * @arg Entity viewmodel - The viewmodel that is changing
     * @arg string old - The old model
     * @arg string _new - The new model
     */
    ViewModelChanged(viewmodel: Entity, old: string, _new: string): void;
}

declare type PLAYER__HOOKS = keyof PLAYER;
