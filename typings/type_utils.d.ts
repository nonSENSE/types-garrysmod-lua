declare namespace GMLua {
    type CallbackNoContext<F extends Function = (...args: any[]) => any> = F extends (...args: infer T) => any
        ? (this: void, ...args: T) => ReturnType<F>
        : never;
}
