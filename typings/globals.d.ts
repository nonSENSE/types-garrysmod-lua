/** @noSelfInFile */
declare interface _G {
    [index: string | number | symbol]: any;

    /**
     * Adds simple Get/Set accessor functions on the specified table.
     * Can also force the value to be set to a number, bool or string.
     *
     * @arg object tab - The table to add the accessor functions too.
     * @arg any key - The key of the table to be get/set.
     * @arg string name - The name of the functions (will be prefixed with Get and Set).
     * @arg number [force=NaN] - The type the setter should force to (uses Enums/FORCE).
     */
    AccessorFunc(tab: object, key: any, name: string, force?: number): void;

    /**
     * Defines a global entity class variable with an automatic value in order to prevent collisions with other Enums/CLASS. You should prefix your variable with CLASS_ for consistency.
     *
     * @arg string name - The name of the new enum/global variable.
     */
    Add_NPC_Class(name: string): void;

    /**
     * Adds the specified image path to the main menu background pool. Image can be png or jpeg.
     *
     * @arg string path - Path to the image.
     */
    AddBackgroundImage(path: string): void;

    /**
     * Tells the engine to register a console command. If the command was ran, the engine calls concommand.Run.
     *
     * @arg string name - The name of the console command to add.
     * @arg string helpText - The help text.
     * @arg number flags - Concommand flags using Enums/FCVAR
     */
    AddConsoleCommand(name: string, helpText: string, flags: number): void;

    /**
 * Marks a Lua file to be sent to clients when they join the server. Doesn't do anything on the client - this means you can use it in a shared file without problems.
* 
* @arg string [file="current file"] - The name/path to the Lua file that should be sent, relative to the garrysmod/lua folder. If no parameter is specified, it sends the current file.
The file path can be relative to the script it's ran from. For example, if your script is in lua/myfolder/stuff.lua, calling AddCSLuaFile("otherstuff.lua") and AddCSLuaFile("myfolder/otherstuff.lua") is the same thing.
Please make sure your file names are unique, the filesystem is shared across all addons, so a file named lua/config.lua in your addon may be overwritten by the same file in another addon.
 */
    AddCSLuaFile(file?: string): void;

    /**
     * Loads the specified image from the /cache folder, used in combination steamworks.Download. Most addons will provide a 512x512 png image.
     *
     * @arg string name - The name of the file.
     * @return {IMaterial} The material, returns nil if the cached file is not an image.
     */
    AddonMaterial(name: string): IMaterial;

    /**
     * Adds the specified vector to the PVS which is currently building. This allows all objects in visleafs visible from that vector to be drawn.
     *
     * @arg Vector position - The origin to add.
     */
    AddOriginToPVS(position: Vector): void;

    /**
     * This function creates a World Tip, similar to the one shown when aiming at a Thruster where it shows you its force.
     *
     * @arg number [entindex=NaN] - This argument is no longer used; it has no effect on anything. You can use nil in this argument.
     * @arg string text - The text for the world tip to display.
     * @arg number [dieTime=NaN] - This argument is no longer used; when you add a World Tip it will always last only 0.05 seconds. You can use nil in this argument.
     * @arg Vector [pos=ent:GetPos()] - Where in the world you want the World Tip to be drawn. If you add a valid Entity in the next argument, this argument will have no effect on the actual World Tip.
     * @arg Entity [ent=nil] - Which entity you want to associate with the World Tip. This argument is optional. If set to a valid entity, this will override the position set in pos with the Entity's position.
     */
    AddWorldTip(entindex?: number, text?: string, dieTime?: number, pos?: Vector, ent?: Entity): void;

    /**
 * Creates an Angle object.
* 
* @arg number [pitch=0] - The pitch value of the angle.
If this is an Angle, this function will return a copy of the given angle.
If this is a string, this function will try to parse the string as a angle. If it fails, it returns a 0 angle.
(See examples)
* @arg number [yaw=0] - The yaw value of the angle.
* @arg number [roll=0] - The roll value of the angle.
* @return {Angle} Created angle
 */
    Angle(pitch?: number, yaw?: number, roll?: number): Angle;

    /**
     * Returns an angle with a randomized pitch, yaw, and roll between min(inclusive), max(exclusive).
     *
     * @arg number [min=-90] - Min bound inclusive.
     * @arg number [max=90] - Max bound exclusive.
     * @return {Angle} The randomly generated angle.
     */
    AngleRand(min?: number, max?: number): Angle;

    /**
 * If the result of the first argument is false or nil, an error is thrown with the second argument as the message.
* 
* @arg any expression - The expression to assert.
* @arg string [errorMessage="assertion failed!"] - The error message to throw when assertion fails. This is only type-checked if the assertion fails.
* @arg args[] [returns=nil] - Any arguments past the error message will be returned by a successful assert.
* @return {any} If successful, returns the first argument.
* @return {any} If successful, returns the error message. This will be nil if the second argument wasn't specified.
Since the second argument is only type-checked if the assertion fails, this doesn't have to be a string.
* @return {args[]} Returns any arguments past the error message.
 */
    assert(expression: any, errorMessage?: string, ...returns: any[]): LuaMultiReturn<[any, any, ...any[]]>;

    /**
     * Sends the specified Lua code to all connected clients and executes it.
     *
     * @arg string code - The code to be executed. Capped at length of 254 characters.
     */
    BroadcastLua(code: string): void;

    /**
 * Dumps the networked variables of all entities into one table and returns it.
* 
* @return {Entity} Format:

key = Entity for NWVars or number (always 0) for global vars
value = table formatted as:

key = string var name
value = any type var value
 */
    BuildNetworkedVarsTable(): Entity;

    /**
     * Used internally to check if the current server the player is on can be added to favorites or not. Does not check if the server is ALREADY in the favorites.
     *
     * @return {boolean} Can add to favorites
     */
    CanAddServerToFavorites(): boolean;

    /**
     * Aborts joining of the server you are currently joining.
     */
    CancelLoading(): void;

    /**
     * Sets the active main menu background image to a random entry from the background images pool. Images are added with AddBackgroundImage.
     *
     * @arg string currentgm - Apparently does nothing.
     */
    ChangeBackground(currentgm: string): void;

    /**
     * Automatically called by the engine when a panel is hovered over with the mouse
     *
     * @arg Panel panel - Panel that has been hovered over
     */
    ChangeTooltip(panel: Panel): void;

    /**
     * Empties the pool of main menu background images.
     */
    ClearBackgroundImages(): void;

    /**
 * Creates a non physical entity that only exists on the client. See also ents.CreateClientProp.
* 
* @arg string model - The file path to the model.
Model must be precached with util.PrecacheModel on the server before usage.
* @arg number [renderGroup=NaN] - The render group of the entity for the clientside leaf system, see Enums/RENDERGROUP.
* @return {CSEnt} Created client-side model (C_BaseFlex).
 */
    ClientsideModel(model: string, renderGroup?: number): CSEnt;

    /**
 * Creates a fully clientside ragdoll.
* 
* @arg string model - The file path to the model.
Model must be precached with util.PrecacheModel on the server before usage.
* @arg number [renderGroup=NaN] - The Enums/RENDERGROUP to assign.
* @return {CSEnt} The newly created client-side ragdoll. ( C_ClientRagdoll )
 */
    ClientsideRagdoll(model: string, renderGroup?: number): CSEnt;

    /**
     * Creates a scene entity based on the scene name and the entity.
     *
     * @arg string name - The name of the scene.
     * @arg Entity targetEnt - The entity to play the scene on.
     * @return {CSEnt} C_SceneEntity
     */
    ClientsideScene(name: string, targetEnt: Entity): CSEnt;

    /**
     * Closes all Derma menus that have been passed to RegisterDermaMenuForClose and calls GM:CloseDermaMenus
     */
    CloseDermaMenus(): void;

    /**
 * Executes the specified action on the garbage collector.
* 
* @arg string [action="collect"] - The action to run.
Valid actions are collect, stop, restart, count, step, setpause and setstepmul.
* @arg number arg - The argument of the specified action, only applicable for step, setpause and setstepmul.
* @return {any} If the action is count this is the number of kilobytes of memory used by Lua.
If the action is step this is true if a garbage collection cycle was finished.
If the action is setpause this is the previous value for the GC's pause.
If the action is setstepmul this is the previous value for the GC's step.
 */
    collectgarbage(action?: string, arg?: number): any;

    /**
     * Creates a Color.
     *
     * @arg number r - An integer from 0-255 describing the red value of the color.
     * @arg number g - An integer from 0-255 describing the green value of the color.
     * @arg number b - An integer from 0-255 describing the blue value of the color.
     * @arg number [a=255] - An integer from 0-255 describing the alpha (transparency) of the color.
     * @return {Color} The created Color.
     */
    Color(r: number, g: number, b: number, a?: number): Color;

    /**
     * Returns a new Color with the RGB components of the given Color and the alpha value specified.
     *
     * @arg Color color - The Color from which to take RGB values. This color will not be modified.
     * @arg number alpha - The new alpha value, a number between 0 and 255. Values above 255 will be clamped.
     * @return {Color} The new Color with the modified alpha value
     */
    ColorAlpha(color: Color, alpha: number): Color;

    /**
     * Creates a Color with randomized red, green, and blue components. If the alpha argument is true, alpha will also be randomized.
     *
     * @arg boolean [a=false] - Should alpha be randomized.
     * @return {Color} The created Color.
     */
    ColorRand(a?: boolean): Color;

    /**
     * Converts a Color into HSL color space.
     *
     * @arg Color color - The Color.
     * @return {number} The hue in degrees [0, 360].
     * @return {number} The saturation in the range [0, 1].
     * @return {number} The lightness in the range [0, 1].
     */
    ColorToHSL(color: Color): LuaMultiReturn<[number, number, number]>;

    /**
     * Converts a Color into HSV color space.
     *
     * @arg Color color - The Color.
     * @return {number} The hue in degrees [0, 360].
     * @return {number} The saturation in the range [0, 1].
     * @return {number} The value in the range [0, 1].
     */
    ColorToHSV(color: Color): LuaMultiReturn<[number, number, number]>;

    /**
     * Attempts to compile the given file. If successful, returns a function that can be called to perform the actual execution of the script.
     *
     * @arg string path - Path to the file, relative to the garrysmod/lua/ directory.
     * @return {Function} The function which executes the script.
     */
    CompileFile(path: string): Function;

    /**
 * This function will compile the code argument as lua code and return a function that will execute that code.
* 
* @arg string code - The code to compile.
* @arg string identifier - An identifier in case an error is thrown. (The same identifier can be used multiple times)
* @arg boolean [HandleError=true] - If false this function will return an error string instead of throwing an error.
* @return {Function} A function that, when called, will execute the given code.
Returns the error string if there was a Lua error and third argument is false.
 */
    CompileString(code: string, identifier: string, HandleError?: boolean): Function;

    /**
     * Returns a table of console command names beginning with the given text.
     *
     * @arg string text - Text that the console commands must begin with.
     * @return {object} Table of console command names.
     */
    ConsoleAutoComplete(text: string): object;

    /**
     * Returns whether a ConVar with the given name exists or not
     *
     * @arg string name - Name of the ConVar.
     * @return {boolean} True if the ConVar exists, false otherwise.
     */
    ConVarExists(name: string): boolean;

    /**
 * Makes a clientside-only console variable
* 
* @arg string name - Name of the ConVar to be created and able to be accessed.
This cannot be a name of existing console command or console variable. It will silently fail if it is.
* @arg string def - Default value of the ConVar.
* @arg boolean [shouldsave=true] - Should the ConVar be saved across sessions in the cfg/client.vdf file.
* @arg boolean [userinfo=false] - Should the ConVar and its containing data be sent to the server when it has changed. This make the convar accessible from server using Player:GetInfoNum and similar functions.
* @arg string helptext - Help text to display in the console.
* @arg number [min=NaN] - If set, the convar cannot be changed to a number lower than this value.
* @arg number [max=NaN] - If set, the convar cannot be changed to a number higher than this value.
* @return {ConVar} Created convar.
 */
    CreateClientConVar(
        name: string,
        def: string,
        shouldsave?: boolean,
        userinfo?: boolean,
        helptext?: string,
        min?: number,
        max?: number
    ): ConVar;

    /**
 * Creates a console variable (ConVar), in general these are for things like gamemode/server settings.
* 
* @arg string name - Name of the ConVar.
This cannot be a name of an engine console command or console variable. It will silently fail if it is. If it is the same name as another lua ConVar, it will return that ConVar object.
* @arg string value - Default value of the convar. Can also be a number.
* @arg number [flags=NaN] - Flags of the convar, see Enums/FCVAR, either as bitflag or as table.
* @arg string helptext - The help text to show in the console.
* @arg number [min=NaN] - If set, the ConVar cannot be changed to a number lower than this value.
* @arg number [max=NaN] - If set, the ConVar cannot be changed to a number higher than this value.
* @return {ConVar} The convar created.
 */
    CreateConVar(name: string, value: string, flags?: number, helptext?: string, min?: number, max?: number): ConVar;

    /**
 * Creates a new material with the specified name and shader.
* 
* @arg string name - The material name. Must be unique.
* @arg string shaderName - The shader name. See Shaders.
* @arg object materialData - Key-value table that contains shader parameters and proxies.

See: List of Shader Parameters on Valve Developers Wiki and each shader's page from .

Unlike IMaterial:SetTexture, this table will not accept ITexture values. Instead, use the texture's name (see ITexture:GetName).
* @return {IMaterial} Created material
 */
    CreateMaterial(name: string, shaderName: string, materialData: object): IMaterial;

    /**
     * Creates a new particle system.
     *
     * @arg Entity ent - The entity to attach the control point to.
     * @arg string effect - The name of the effect to create. It must be precached.
     * @arg number partAttachment - See Enums/PATTACH.
     * @arg number [entAttachment=0] - The attachment ID on the entity to attach the particle system to
     * @arg Vector [offset=Vector( 0, 0, 0 )] - The offset from the Entity:GetPos of the entity we are attaching this CP to.
     * @return {CNewParticleEffect} The created particle system.
     */
    CreateParticleSystem(
        ent: Entity,
        effect: string,
        partAttachment: number,
        entAttachment?: number,
        offset?: Vector
    ): CNewParticleEffect;

    /**
     * Creates a new PhysCollide from the given bounds.
     *
     * @arg Vector mins - Min corner of the box. This is not automatically ordered with the maxs and must contain the smallest vector components. See OrderVectors.
     * @arg Vector maxs - Max corner of the box. This is not automatically ordered with the mins and must contain the largest vector components.
     * @return {PhysCollide} The new PhysCollide. This will be a NULL PhysCollide (PhysCollide:IsValid returns false) if given bad vectors or no more PhysCollides can be created in the physics engine.
     */
    CreatePhysCollideBox(mins: Vector, maxs: Vector): PhysCollide;

    /**
     * Creates PhysCollide objects for every physics object the model has. The model must be precached with util.PrecacheModel before being used with this function.
     *
     * @arg string modelName - Model path to get the collision objects of.
     * @return {PhysCollide} Table of PhysCollide objects. The number of entries will match the model's physics object count. See also Entity:GetPhysicsObjectCount. Returns no value if the model doesn't exist, or has not been precached.
     */
    CreatePhysCollidesFromModel(modelName: string): PhysCollide;

    /**
 * Returns a sound parented to the specified entity.
* 
* @arg Entity targetEnt - The target entity.
* @arg string soundName - The sound to play.
* @arg CRecipientFilter [filter=nil] - A CRecipientFilter of the players that will have this sound networked to them.
If not set, the default is a CPASAttenuationFilter.
This argument only works serverside.
* @return {CSoundPatch} The sound object
 */
    CreateSound(targetEnt: Entity, soundName: string, filter?: CRecipientFilter): CSoundPatch;

    /**
     * Creates and returns a new DSprite element with the supplied material.
     *
     * @arg IMaterial material - Material the sprite should draw.
     * @return {Panel} The new DSprite element.
     */
    CreateSprite(material: IMaterial): Panel;

    /**
     * Returns the uptime of the server in seconds (to at least 4 decimal places)
     *
     * @return {number} Time synced with the game server.
     */
    CurTime(): number;

    /**
     * Returns an CTakeDamageInfo object.
     *
     * @return {CTakeDamageInfo} The CTakeDamageInfo object.
     */
    DamageInfo(): CTakeDamageInfo;

    /**
     * Writes text to the right hand side of the screen, like the old error system. Messages disappear after a couple of seconds.
     *
     * @arg number slot - The location on the right hand screen to write the debug info to. Starts at 0, no upper limit
     * @arg string info - The debugging information to be written to the screen
     */
    DebugInfo(slot: number, info: string): void;

    /**
     * This is not a function. This is a preprocessor keyword that translates to:
     *
     * @arg string value - Baseclass name
     */
    DEFINE_BASECLASS(value: string): void;

    /**
     * Loads and registers the specified gamemode, setting the GM table's DerivedFrom field to the value provided, if the table exists. The DerivedFrom field is used post-gamemode-load as the "derived" parameter for gamemode.Register.
     *
     * @arg string base - Gamemode name to derive from.
     */
    DeriveGamemode(base: string): void;

    /**
 * Creates a new derma animation.
* 
* @arg string name - Name of the animation to create
* @arg Panel panel - Panel to run the animation on
* @arg GMLua.CallbackNoContext func - Function to call to process the animation
Arguments:

Panel pnl - the panel passed to Derma_Anim
table anim - the anim table
number delta - the fraction of the progress through the animation
any data - optional data passed to the run metatable method
* @return {object} A lua metatable containing four methods:

Run() - Should be called each frame you want the animation to be ran.
Active() - Returns if the animation is currently active (has not finished and stop has not been called)
Stop() - Halts the animation at its current progress.
Start( Length, Data ) - Prepares the animation to be ran for Length seconds. Must be called once before calling Run(). The data parameter will be passed to the func function.
 */
    Derma_Anim(name: string, panel: Panel, func: GMLua.CallbackNoContext): object;

    /**
     * Draws background blur around the given panel.
     *
     * @arg Panel panel - Panel to draw the background blur around
     * @arg number startTime - Time that the blur began being painted
     */
    Derma_DrawBackgroundBlur(panel: Panel, startTime: number): void;

    /**
     * Creates panel method that calls the supplied Derma skin hook via derma.SkinHook
     *
     * @arg Panel panel - Panel to add the hook to
     * @arg string functionName - Name of panel function to create
     * @arg string hookName - Name of Derma skin hook to call within the function
     * @arg string typeName - Type of element to call Derma skin hook for
     */
    Derma_Hook(panel: Panel, functionName: string, hookName: string, typeName: string): void;

    /**
     * Makes the panel (usually an input of sorts) respond to changes in console variables by adding next functions to the panel:
     *
     * @arg Panel target - The panel the functions should be added to.
     */
    Derma_Install_Convar_Functions(target: Panel): void;

    /**
     * Creates a derma window to display information
     *
     * @arg string Text - The text within the created panel.
     * @arg string Title - The title of the created panel.
     * @arg string Button - The text of the button to close the panel.
     * @return {Panel} The created DFrame
     */
    Derma_Message(Text: string, Title: string, Button: string): Panel;

    /**
     * Shows a message box in the middle of the screen, with up to 4 buttons they can press.
     *
     * @arg string [text="Message Text (Second Parameter)"] - The message to display.
     * @arg string [title="Message Title (First Parameter)"] - The title to give the message box.
     * @arg string btn1text - The text to display on the first button.
     * @arg GMLua.CallbackNoContext [btn1func=nil] - The function to run if the user clicks the first button.
     * @arg string [btn2text="nil"] - The text to display on the second button.
     * @arg GMLua.CallbackNoContext [btn2func=nil] - The function to run if the user clicks the second button.
     * @arg string [btn3text="nil"] - The text to display on the third button
     * @arg GMLua.CallbackNoContext [btn3func=nil] - The function to run if the user clicks the third button.
     * @arg string [btn4text="nil"] - The text to display on the fourth button
     * @arg GMLua.CallbackNoContext [btn4func=nil] - The function to run if the user clicks the fourth button.
     * @return {Panel} The Panel object of the created window.
     */
    Derma_Query(
        text?: string,
        title?: string,
        btn1text?: string,
        btn1func?: GMLua.CallbackNoContext,
        btn2text?: string,
        btn2func?: GMLua.CallbackNoContext,
        btn3text?: string,
        btn3func?: GMLua.CallbackNoContext,
        btn4text?: string,
        btn4func?: GMLua.CallbackNoContext
    ): Panel;

    /**
     * Creates a derma window asking players to input a string.
     *
     * @arg string title - The title of the created panel.
     * @arg string subtitle - The text above the input box
     * @arg string def - The default text for the input box.
     * @arg GMLua.CallbackNoContext confirm - The function to be called once the user has confirmed their input.
     * @arg GMLua.CallbackNoContext [cancel=nil] - The function to be called once the user has cancelled their input
     * @arg string [confirmText="OK"] - Allows you to override text of the "OK" button
     * @arg string [cancelText="Cancel"] - Allows you to override text of the "Cancel" button
     * @return {Panel} The created DFrame
     */
    Derma_StringRequest(
        title: string,
        subtitle: string,
        def: string,
        confirm: GMLua.CallbackNoContext,
        cancel?: GMLua.CallbackNoContext,
        confirmText?: string,
        cancelText?: string
    ): Panel;

    /**
     * Creates a DMenu and closes any current menus.
     *
     * @arg boolean [keepOpen=false] - If we should keep other DMenus open (true) or not (false).
     * @arg Panel [parent=nil] - The panel to parent the created menu to.
     * @return {Panel} The created DMenu.
     */
    DermaMenu(keepOpen?: boolean, parent?: Panel): Panel;

    /**
     * Sets whether rendering should be limited to being inside a panel or not.
     *
     * @arg boolean disable - Whether or not clipping should be disabled
     * @return {boolean} Whether the clipping was enabled or not before this function call
     */
    DisableClipping(disable: boolean): boolean;

    /**
     * Cancels current DOF post-process effect started with DOF_Start
     */
    DOF_Kill(): void;

    /**
     * Cancels any existing DOF post-process effects.
     * Begins the DOF post-process effect.
     */
    DOF_Start(): void;

    /**
     * A hacky method used to fix some bugs regarding DoF. What this basically does it force all C_BaseAnimating entities to have the translucent rendergroup, even if they use opaque or two-pass models.
     *
     * @arg boolean enable - Enables or disables depth-of-field mode
     */
    DOFModeHack(enable: boolean): void;

    /**
     * Draws the currently active main menu background image and handles transitioning between background images.
     */
    DrawBackground(): void;

    /**
     * Draws the bloom shader, which creates a glowing effect from bright objects.
     *
     * @arg number Darken - Determines how much to darken the effect. A lower number will make the glow come from lower light levels. A value of 1 will make the bloom effect unnoticeable. Negative values will make even pitch black areas glow.
     * @arg number Multiply - Will affect how bright the glowing spots are. A value of 0 will make the bloom effect unnoticeable.
     * @arg number SizeX - The size of the bloom effect along the horizontal axis.
     * @arg number SizeY - The size of the bloom effect along the vertical axis.
     * @arg number Passes - Determines how much to exaggerate the effect.
     * @arg number ColorMultiply - Will multiply the colors of the glowing spots, making them more vivid.
     * @arg number Red - How much red to multiply with the glowing color. Should be between 0 and 1.
     * @arg number Green - How much green to multiply with the glowing color. Should be between 0 and 1.
     * @arg number Blue - How much blue to multiply with the glowing color. Should be between 0 and 1.
     */
    DrawBloom(
        Darken: number,
        Multiply: number,
        SizeX: number,
        SizeY: number,
        Passes: number,
        ColorMultiply: number,
        Red: number,
        Green: number,
        Blue: number
    ): void;

    /**
     * Draws the Bokeh Depth Of Field effect .
     *
     * @arg number intensity - Intensity of the effect.
     * @arg number distance - Not worldspace distance. Value range is from 0 to 1.
     * @arg number focus - Focus. Recommended values are from 0 to 12.
     */
    DrawBokehDOF(intensity: number, distance: number, focus: number): void;

    /**
     * Draws the Color Modify shader, which can be used to adjust colors on screen.
     *
     * @arg any modifyParameters - Color modification parameters. See Shaders/g_colourmodify and the example below. Note that if you leave out a field, it will retain its last value which may have changed if another caller uses this function.
     */
    DrawColorModify(modifyParameters: any): void;

    /**
     * Draws a material overlay on the screen.
     *
     * @arg string Material - This will be the material that is drawn onto the screen.
     * @arg number RefractAmount - This will adjust how much the material will refract your screen.
     */
    DrawMaterialOverlay(Material: string, RefractAmount: number): void;

    /**
     * Creates a motion blur effect by drawing your screen multiple times.
     *
     * @arg number AddAlpha - How much alpha to change per frame.
     * @arg number DrawAlpha - How much alpha the frames will have. A value of 0 will not render the motion blur effect.
     * @arg number Delay - Determines the amount of time between frames to capture.
     */
    DrawMotionBlur(AddAlpha: number, DrawAlpha: number, Delay: number): void;

    /**
     * Draws the sharpen shader, which creates more contrast.
     *
     * @arg number Contrast - How much contrast to create.
     * @arg number Distance - How large the contrast effect will be.
     */
    DrawSharpen(Contrast: number, Distance: number): void;

    /**
     * Draws the sobel shader, which detects edges and draws a black border.
     *
     * @arg number Threshold - Determines the threshold of edges. A value of 0 will make your screen completely black.
     */
    DrawSobel(Threshold: number): void;

    /**
     * Renders the post-processing effect of beams of light originating from the map's sun. Utilises the pp/sunbeams material.
     *
     * @arg number darken - $darken property for sunbeams material.
     * @arg number multiplier - $multiply property for sunbeams material.
     * @arg number sunSize - $sunsize property for sunbeams material.
     * @arg number sunX - $sunx property for sunbeams material.
     * @arg number sunY - $suny property for sunbeams material.
     */
    DrawSunbeams(darken: number, multiplier: number, sunSize: number, sunX: number, sunY: number): void;

    /**
     * Draws the texturize shader, which replaces each pixel on your screen with a different part of the texture depending on its brightness. See g_texturize for information on making the texture.
     *
     * @arg number Scale - Scale of the texture. A smaller number creates a larger texture.
     * @arg number BaseTexture - This will be the texture to use in the effect. Make sure you use Material to get the texture number.
     */
    DrawTexturize(Scale: number, BaseTexture: number): void;

    /**
     * Draws the toy town shader, which blurs the top and bottom of your screen. This can make very large objects look like toys, hence the name.
     *
     * @arg number Passes - An integer determining how many times to draw the effect. A higher number creates more blur.
     * @arg number Height - The amount of screen which should be blurred on the top and bottom.
     */
    DrawToyTown(Passes: number, Height: number): void;

    /**
     * Drops the specified entity if it is being held by any player with Gravity Gun or +use pickup.
     *
     * @arg Entity ent - The entity to drop.
     */
    DropEntityIfHeld(ent: Entity): void;

    /**
     * Creates or replaces a dynamic light with the given id.
     *
     * @arg number index - An unsigned Integer. Usually an entity index is used here.
     * @arg boolean [elight=false] - Allocates an elight instead of a dlight. Elights have a higher light limit and do not light the world (making the "noworld" parameter have no effect).
     * @return {DynamicLightStruct} A DynamicLight structured table. See Structures/DynamicLight
     */
    DynamicLight(index: number, elight?: boolean): DynamicLightStruct;

    /**
     * Returns a CEffectData object to be used with util.Effect.
     *
     * @return {CEffectData} The CEffectData object.
     */
    EffectData(): CEffectData;

    /**
     * An eagerly evaluated ternary operator, or, in layman's terms, a compact "if then else" statement.
     *
     * @arg any condition - The condition to check if true or false.
     * @arg any truevar - If the condition isn't nil/false, returns this value.
     * @arg any falsevar - If the condition is nil/false, returns this value.
     * @return {any} The result.
     */
    Either(condition: any, truevar: any, falsevar: any): any;

    /**
     * Plays a sentence from scripts/sentences.txt
     *
     * @arg string soundName - The sound to play
     * @arg Vector position - The position to play at
     * @arg number entity - The entity to emit the sound from. Must be Entity:EntIndex
     * @arg number [channel=NaN] - The sound channel, see Enums/CHAN.
     * @arg number [volume=1] - The volume of the sound, from 0 to 1
     * @arg number [soundLevel=75] - The sound level of the sound, see Enums/SNDLVL
     * @arg number [soundFlags=0] - The flags of the sound, see Enums/SND
     * @arg number [pitch=100] - The pitch of the sound, 0-255
     */
    EmitSentence(
        soundName: string,
        position: Vector,
        entity: number,
        channel?: number,
        volume?: number,
        soundLevel?: number,
        soundFlags?: number,
        pitch?: number
    ): void;

    /**
 * Emits the specified sound at the specified position.
* 
* @arg string soundName - The sound to play
* @arg Vector position - The position where the sound is meant to play, used only for a network  filter (CPASAttenuationFilter) to decide which players will hear the sound.
* @arg number entity - The entity to emit the sound from. Can be an Entity:EntIndex or one of the following:

0 - Plays sound on the world (position set to 0,0,0)
-1 - Plays sound on the local player (on server acts as 0)
-2 - Plays UI sound (position set to 0,0,0, no spatial sound, on server acts as 0)
* @arg number [channel=NaN] - The sound channel, see Enums/CHAN.
* @arg number [volume=1] - The volume of the sound, from 0 to 1
* @arg number [soundLevel=75] - The sound level of the sound, see Enums/SNDLVL
* @arg number [soundFlags=0] - The flags of the sound, see Enums/SND
* @arg number [pitch=100] - The pitch of the sound, 0-255
* @arg number [dsp=0] - The DSP preset for this sound. List of DSP presets
 */
    EmitSound(
        soundName: string,
        position: Vector,
        entity: number,
        channel?: number,
        volume?: number,
        soundLevel?: number,
        soundFlags?: number,
        pitch?: number,
        dsp?: number
    ): void;

    /**
     * Removes the currently active tool tip from the screen.
     *
     * @arg Panel panel - This is the panel that has a tool tip.
     */
    EndTooltip(panel: Panel): void;

    /**
     * Returns the entity with the matching Entity:EntIndex.
     *
     * @arg number entityIndex - The entity index.
     * @return {Entity} The entity if it exists, or NULL if it doesn't.
     */
    Entity(entityIndex: number): Entity;

    /**
     * Throws a Lua error and breaks out of the current call stack.
     *
     * @arg string message - The error message to throw
     * @arg number [errorLevel=1] - The level to throw the error at.
     */
    error(message: string, errorLevel?: number): void;

    /**
     * Throws a Lua error but does not break out of the current call stack.
     * This function will not print a stack trace like a normal error would.
     * Essentially similar if not equivalent to Msg.
     *
     * @arg args[] arguments - Converts all arguments to strings and prints them with no spacing.
     */
    ErrorNoHalt(...arguments: any[]): void;

    /**
     * Throws a Lua error but does not break out of the current call stack.
     *
     * @arg args[] arguments - Converts all arguments to strings and prints them with no spacing.
     */
    ErrorNoHaltWithStack(...arguments: any[]): void;

    /**
     * Returns the angles of the current render context as calculated by GM:CalcView.
     *
     * @return {Angle} The angle of the currently rendered scene.
     */
    EyeAngles(): Angle;

    /**
     * Returns the origin of the current render context as calculated by GM:CalcView.
     *
     * @return {Vector} Camera position.
     */
    EyePos(): Vector;

    /**
     * Returns the normal vector of the current render context as calculated by GM:CalcView, similar to EyeAngles.
     *
     * @return {Vector} View direction of the currently rendered scene.
     */
    EyeVector(): Vector;

    /**
     * Returns the meta table for the class with the matching name.
     *
     * @arg string metaName - The object type to retrieve the meta table of.
     * @return {object} The corresponding meta table.
     */
    FindMetaTable(metaName: string): object;

    /**
     * Returns the tool-tip text and tool-tip-panel (if any) of the given panel as well as itself
     *
     * @arg Panel panel - Panel to find tool-tip of
     * @return {string} tool-tip text
     * @return {Panel} tool-tip panel
     * @return {Panel} panel that the function was called with
     */
    FindTooltip(panel: Panel): LuaMultiReturn<[string, Panel, Panel]>;

    /**
 * Formats the specified values into the string given. Same as string.format.
* 
* @arg string format - The string to be formatted.
Follows this format: http://www.cplusplus.com/reference/cstdio/printf/
* @arg args[] formatParameters - Values to be formatted into the string.
* @return {string} The formatted string
 */
    Format(format: string, ...formatParameters: any[]): string;

    /**
     * Returns the number of frames rendered since the game was launched.
     */
    FrameNumber(): void;

    /**
     * Returns the CurTime-based time in seconds it took to render the last frame.
     *
     * @return {number} time (in seconds)
     */
    FrameTime(): number;

    /**
     * Callback function for when the client has joined a server. This function shows the server's loading URL by default.
     *
     * @arg string servername - Server's name.
     * @arg string serverurl - Server's loading screen URL, or "" if the URL is not set.
     * @arg string mapname - Server's current map's name.
     * @arg number maxplayers - Max player count of server.
     * @arg string steamid - The local player's Player:SteamID64.
     * @arg string gamemode - Server's current gamemode's folder name.
     */
    GameDetails(
        servername: string,
        serverurl: string,
        mapname: string,
        maxplayers: number,
        steamid: string,
        gamemode: string
    ): void;

    /**
     * Returns the current floored dynamic memory usage of Lua in kilobytes.
     *
     * @return {number} The current floored dynamic memory usage of Lua, in kilobytes.
     */
    gcinfo(): number;

    /**
 * Gets miscellaneous information from Facepunches API.
* 
* @arg GMLua.CallbackNoContext callback - Callback to be called when the API request is done.
Callback is called with one argument, a JSON which when converted into a table using util.JSONToTable contains the following:
{
	"ManifestVersion": 	number - Version of the manifest

	"Date": 			string - Date in WDDX format

	// Contains all the blog posts, the things in the top right of the menu
	"News": {
		"Blogs": [

			// Structure of blog posts
			{ 
				"Date": 		string - Date in WDDX format of the post
				"ShortName": 	string - Short name of the post, identifier of it on the blog website
				"Title": 		string - Title of the post
				"HeaderImage": 	string - Main image of the post, showed in the top right
				"SummaryHtml": 	string - Summary of the blogpost, text thats shown
				"Url": 			string - URL to the post on the blog
				"Tags": 		string - String of the posts tag
			}
		]
	}
	
	// Array of Facepunches Mods, Admins and Devs
	"Administrators": [
		{
			"UserId": 		string - SteamID64 of the person
			"Level": 		string - Level of the user (Administrator, Developer or Moderator)
		}
	]

	// Unused and contains nothing useful
	"Heroes": {}

	"SentryUrl": 		string - Nothing
	"DatabaseUrl" 		string - URL to the Facepunch API (/database/{action}/)
	"FeedbackUrl" 		string - URL to the Facepunch API (/feedback/add/)
	"ReportUrl" 		string - URL to the Facepunch API (/feedback/report/)
	"LeaderboardUrl" 	string - URL to the Facepunch API (/leaderboard/{action}/)
	"BenchmarkUrl" 		string - URL to the Facepunch API (/benchmark/add/)
	"AccountUrl" 		string - URL to the Facepunch API (/account/{action}/)

	"Servers": {
		"Official": [] // Nothing
		
		// List of blacklisted servers
		"Banned": [
			string 	- IP of the blacklisted server
		]
	}
}
 */
    GetAPIManifest(callback: GMLua.CallbackNoContext): void;

    /**
     * Gets the ConVar with the specified name.
     *
     * @arg string name - Name of the ConVar to get
     * @return {ConVar} The ConVar object, or nil if no such ConVar was found.
     */
    GetConVar(name: string): ConVar;

    /**
     * Gets the ConVar with the specified name. This function doesn't cache the convar.
     *
     * @arg string name - Name of the ConVar to get
     * @return {ConVar} The ConVar object
     */
    GetConVar_Internal(name: string): ConVar;

    /**
     * Gets the numeric value ConVar with the specified name.
     *
     * @arg string name - Name of the ConVar to get.
     * @return {number} The ConVar's value.
     */
    GetConVarNumber(name: string): number;

    /**
     * Gets the string value ConVar with the specified name.
     *
     * @arg string name - Name of the ConVar to get.
     * @return {string} The ConVar's value.
     */
    GetConVarString(name: string): string;

    /**
     * Returns the default loading screen URL (asset://garrysmod/html/loading.html)
     *
     * @return {string} Default loading url (asset://garrysmod/html/loading.html)
     */
    GetDefaultLoadingHTML(): string;

    /**
     * Retrieves data about the demo with the specified filename. Similar to GetSaveFileDetails.
     *
     * @arg string filename - The file name of the demo.
     * @return {object} Demo data.
     */
    GetDemoFileDetails(filename: string): object;

    /**
     * Returns a table with the names of files needed from the server you are currently joining.
     *
     * @return {object} table of file names
     */
    GetDownloadables(): object;

    /**
     * Returns the environment table of either the stack level or the function specified.
     *
     * @arg GMLua.CallbackNoContext [location=1] - The object to get the enviroment from. Can also be a number that specifies the function at that stack level: Level 1 is the function calling getfenv.
     * @return {object} The environment.
     */
    getfenv(location?: GMLua.CallbackNoContext): object;

    /**
     * Returns an angle that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg Angle [def=Angle( 0, 0, 0 )] - The value to return if the global value is not set.
     * @return {Angle} The global value, or default if the global is not set.
     */
    GetGlobalAngle(index: string, def?: Angle): Angle;

    /**
     * Returns a boolean that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg boolean [def=false] - The value to return if the global value is not set.
     * @return {boolean} The global value, or the default if the global value is not set.
     */
    GetGlobalBool(index: string, def?: boolean): boolean;

    /**
     * Returns an entity that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg Entity [def=NULL] - The value to return if the global value is not set.
     * @return {Entity} The global value, or the default if the global value is not set.
     */
    GetGlobalEntity(index: string, def?: Entity): Entity;

    /**
     * Returns a float that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg number [def=0] - The value to return if the global value is not set.
     * @return {number} The global value, or the default if the global value is not set.
     */
    GetGlobalFloat(index: string, def?: number): number;

    /**
     * Returns an integer that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg number [def=0] - The value to return if the global value is not set.
     * @return {number} The global value, or the default if the global value is not set.
     */
    GetGlobalInt(index: string, def?: number): number;

    /**
     * Returns a string that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg string def - The value to return if the global value is not set.
     * @return {string} The global value, or the default if the global value is not set.
     */
    GetGlobalString(index: string, def: string): string;

    /**
     * Returns a vector that is shared between the server and all clients.
     *
     * @arg string Index - The unique index to identify the global value with.
     * @arg Vector Default - The value to return if the global value is not set.
     * @return {Vector} The global value, or the default if the global value is not set.
     */
    GetGlobalVector(Index: string, Default: Vector): Vector;

    /**
     * Returns the name of the current server.
     *
     * @return {string} The name of the server.
     */
    GetHostName(): string;

    /**
     * Returns the panel that is used as a wrapper for the HUD. If you want your panel to be hidden when the main menu is opened, parent it to this. Child panels will also have their controls disabled.
     *
     * @return {Panel} The HUD panel
     */
    GetHUDPanel(): Panel;

    /**
     * Returns the loading screen panel and creates it if it doesn't exist.
     *
     * @return {Panel} The loading screen panel
     */
    GetLoadPanel(): Panel;

    /**
     * Returns the current status of the server join progress.
     *
     * @return {string} The current status
     */
    GetLoadStatus(): string;

    /**
     * Returns a table with the names of all maps and categories that you have on your client.
     *
     * @return {object} table of map names and categories
     */
    GetMapList(): object;

    /**
     * Returns the metatable of an object. This function obeys the metatable's __metatable field, and will return that field if the metatable has it set.
     *
     * @arg any object - The value to return the metatable of.
     * @return {any} The metatable of the value. This is not always a table.
     */
    getmetatable(object: any): any;

    /**
     * Returns the menu overlay panel, a container for panels like the error panel created in GM:OnLuaError.
     *
     * @return {Panel} The overlay panel
     */
    GetOverlayPanel(): Panel;

    /**
     * Returns the player whose movement commands are currently being processed. The player this returns can safely have Player:GetCurrentCommand() called on them. See Prediction.
     *
     * @return {Player} The player currently being predicted, or NULL if no command processing is currently being done.
     */
    GetPredictionPlayer(): Player;

    /**
     * Creates or gets the rendertarget with the given name.
     *
     * @arg string name - The internal name of the render target.
     * @arg number width - The width of the render target, must be power of 2. If not set to PO2, the size will be automatically converted to the nearest PO2 size.
     * @arg number height - The height of the render target, must be power of 2. If not set to PO2, the size will be automatically converted to the nearest PO2 size.
     * @return {ITexture} The render target
     */
    GetRenderTarget(name: string, width: number, height: number): ITexture;

    /**
 * Gets (or creates if it does not exist) the rendertarget with the given name, this function allows to adjust the creation of a rendertarget more than GetRenderTarget.
* 
* @arg string name - The internal name of the render target.
The name is treated like a path and gets its extension discarded."name.1" and "name.2" are considered the same name and will result in the same render target being reused.
* @arg number width - The width of the render target, must be power of 2.
* @arg number height - The height of the render target, must be power of 2.
* @arg number sizeMode - Bitflag that influences the sizing of the render target, see Enums/RT_SIZE.
* @arg number depthMode - Bitflag that determines the depth buffer usage of the render target Enums/MATERIAL_RT_DEPTH.
* @arg number textureFlags - Bitflag that configurates the texture, see Enums/TEXTUREFLAGS.
List of flags can also be found on the Valve's Developer Wiki:
https://developer.valvesoftware.com/wiki/Valve_Texture_Format
* @arg number rtFlags - Flags that controll the HDR behaviour of the render target, see Enums/CREATERENDERTARGETFLAGS.
* @arg number imageFormat - Image format, see Enums/IMAGE_FORMAT.
* @return {ITexture} The new render target.
 */
    GetRenderTargetEx(
        name: string,
        width: number,
        height: number,
        sizeMode: number,
        depthMode: number,
        textureFlags: number,
        rtFlags: number,
        imageFormat: number
    ): ITexture;

    /**
     * Retrieves data about the save with the specified filename. Similar to GetDemoFileDetails.
     *
     * @arg string filename - The file name of the save.
     * @return {object} Save data.
     */
    GetSaveFileDetails(filename: string): object;

    /**
     * Returns if the client is timing out, and time since last ping from the server. Similar to the server side Player:IsTimingOut.
     *
     * @return {boolean} Is timing out?
     * @return {number} Get time since last pinged received.
     */
    GetTimeoutInfo(): LuaMultiReturn<[boolean, number]>;

    /**
     * Returns the entity the client is using to see from (such as the player itself, the camera, or another entity).
     *
     * @return {Entity} The view entity.
     */
    GetViewEntity(): Entity;

    /**
     * Converts a color from HSL color space into RGB color space and returns a Color.
     *
     * @arg number hue - The hue in degrees from 0-360.
     * @arg number saturation - The saturation from 0-1.
     * @arg number value - The lightness from 0-1.
     * @return {Color} The Color created from the HSL color space.
     */
    HSLToColor(hue: number, saturation: number, value: number): Color;

    /**
     * Converts a color from HSV color space into RGB color space and returns a Color.
     *
     * @arg number hue - The hue in degrees from 0-360.
     * @arg number saturation - The saturation from 0-1.
     * @arg number value - The value from 0-1.
     * @return {Color} The Color created from the HSV color space.
     */
    HSVToColor(hue: number, saturation: number, value: number): Color;

    /**
     * Launches an asynchronous http request with the given parameters.
     *
     * @arg HTTPRequestStruct parameters - The request parameters. See Structures/HTTPRequest.
     * @return {boolean} true if we made a request, nil if we failed.
     */
    HTTP(parameters: HTTPRequestStruct): boolean;

    /**
 * Executes a Lua script.
* 
* @arg string fileName - The name of the script to be executed. The path must be either relative to the current file, or be an absolute path (relative to and excluding the lua/ folder).
Please make sure your file names are unique, the filesystem is shared across all addons, so a file named lua/config.lua in your addon may be overwritten by the same file in another addon.
* @return {args[]} Anything that the executed Lua script returns.
 */
    include(fileName: string): any;

    /**
     * This function works exactly the same as include both clientside and serverside.
     *
     * @arg string filename - The filename of the Lua file you want to include.
     */
    IncludeCS(filename: string): void;

    /**
     * Returns an iterator function for a for loop, to return ordered key-value pairs from a table.
     *
     * @arg object tab - The table to iterate over.
     * @return {Function} The iterator function.
     * @return {object} The table being iterated over.
     * @return {number} The origin index =0.
     */
    ipairs(tab: object): LuaMultiReturn<[Function, object, number]>;

    /**
     * Returns if the passed object is an Angle.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is an Angle.
     */
    isangle(variable: any): boolean;

    /**
     * Returns if the passed object is a boolean.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a boolean.
     */
    isbool(variable: any): boolean;

    /**
     * Returns whether the given object does or doesn't have a metatable of a color.
     *
     * @arg any Object - The object to be tested
     * @return {boolean} Whether the given object is a color or not
     */
    IsColor(Object: any): boolean;

    /**
     * Determines whether or not the provided console command will be blocked if it's ran through Lua functions, such as RunConsoleCommand or Player:ConCommand.
     *
     * @arg string name - The console command to test.
     * @return {boolean} Whether the command will be blocked.
     */
    IsConCommandBlocked(name: string): boolean;

    /**
     * Returns if the given NPC class name is an enemy.
     *
     * @arg string className - Class name of the entity to check
     * @return {boolean} Is an enemy
     */
    IsEnemyEntityName(className: string): boolean;

    /**
     * Returns if the passed object is an Entity. Alias of isentity.
     *
     * @arg any variable - The variable to check.
     * @return {boolean} True if the variable is an Entity.
     */
    IsEntity(variable: any): boolean;

    /**
     * Returns if this is the first time this hook was predicted.
     *
     * @return {boolean} Whether or not this is the first time being predicted.
     */
    IsFirstTimePredicted(): boolean;

    /**
     * Returns if the given NPC class name is a friend.
     *
     * @arg string className - Class name of the entity to check
     * @return {boolean} Is a friend
     */
    IsFriendEntityName(className: string): boolean;

    /**
     * Returns if the passed object is a function.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a function.
     */
    isfunction(variable: any): boolean;

    /**
     * Returns true if the client is currently playing either a singleplayer or multiplayer game.
     *
     * @return {boolean} True if we are in a game.
     */
    IsInGame(): boolean;

    /**
     * Returns whether the passed object is a VMatrix.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a VMatrix.
     */
    ismatrix(variable: any): boolean;

    /**
     * Checks whether or not a game is currently mounted. Uses data given by engine.GetGames.
     *
     * @arg string game - The game string/app ID to check.
     * @return {boolean} True if the game is mounted.
     */
    IsMounted(game: string): boolean;

    /**
     * Returns if the passed object is a number.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a number.
     */
    isnumber(variable: any): boolean;

    /**
     * Returns if the passed object is a Panel.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a Panel.
     */
    ispanel(variable: any): boolean;

    /**
     * Returns if the passed object is a string.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a string.
     */
    isstring(variable: any): boolean;

    /**
     * Returns if the passed object is a table.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a table.
     */
    istable(variable: any): boolean;

    /**
     * Returns whether or not every element within a table is a valid entity
     *
     * @arg object table - Table containing entities to check
     * @return {boolean} All entities valid
     */
    IsTableOfEntitiesValid(table: object): boolean;

    /**
     * Returns whether or not a model is useless by checking that the file path is that of a proper model.
     *
     * @arg string modelName - The model name to be checked
     * @return {boolean} Whether or not the model is useless
     */
    IsUselessModel(modelName: string): boolean;

    /**
     * Returns whether an object is valid or not. (Such as Entitys, Panels, custom table objects and more).
     * Checks that an object is not nil, has an IsValid method and if this method returns true.
     *
     * @arg any toBeValidated - The table or object to be validated.
     * @return {boolean} True if the object is valid.
     */
    IsValid(toBeValidated: any): boolean;

    /**
     * Returns if the passed object is a Vector.
     *
     * @arg any variable - The variable to perform the type check for.
     * @return {boolean} True if the variable is a Vector.
     */
    isvector(variable: any): boolean;

    /**
     * Joins the server with the specified IP.
     *
     * @arg string IP - The IP of the server to join
     */
    JoinServer(IP: string): void;

    /**
     * Adds javascript function 'language.Update' to an HTML panel as a method to call Lua's language.GetPhrase function.
     *
     * @arg Panel htmlPanel - Panel to add javascript function 'language.Update' to.
     */
    JS_Language(htmlPanel: Panel): void;

    /**
     * Adds javascript function 'util.MotionSensorAvailable' to an HTML panel as a method to call Lua's motionsensor.IsAvailable function.
     *
     * @arg Panel htmlPanel - Panel to add javascript function 'util.MotionSensorAvailable' to.
     */
    JS_Utility(htmlPanel: Panel): void;

    /**
     * Adds workshop related javascript functions to an HTML panel, used by the "Dupes" and "Saves" tabs in the spawnmenu.
     *
     * @arg Panel htmlPanel - Panel to add javascript functions to.
     */
    JS_Workshop(htmlPanel: Panel): void;

    /**
     * Convenience function that creates a DLabel, sets the text, and returns it
     *
     * @arg string text - The string to set the label's text to
     * @arg Panel [parent=nil] - Optional. The panel to parent the DLabel to
     * @return {Panel} The created DLabel
     */
    Label(text: string, parent?: Panel): Panel;

    /**
     * Callback function for when the client's language changes. Called by the engine.
     *
     * @arg string lang - The new language code.
     */
    LanguageChanged(lang: string): void;

    /**
     * Performs a linear interpolation from the start number to the end number.
     *
     * @arg number t - The fraction for finding the result. This number is clamped between 0 and 1. Shouldn't be a constant.
     * @arg number from - The starting number. The result will be equal to this if delta is 0.
     * @arg number to - The ending number. The result will be equal to this if delta is 1.
     * @return {number} The result of the linear interpolation, from + (to - from) * t.
     */
    Lerp(t: number, from: number, to: number): number;

    /**
     * Returns point between first and second angle using given fraction and linear interpolation
     *
     * @arg number ratio - Ratio of progress through values
     * @arg Angle angleStart - Angle to begin from
     * @arg Angle angleEnd - Angle to end at
     * @return {Angle} angle
     */
    LerpAngle(ratio: number, angleStart: Angle, angleEnd: Angle): Angle;

    /**
     * Linear interpolation between two vectors. It is commonly used to smooth movement between two vectors
     *
     * @arg number fraction - Fraction ranging from 0 to 1
     * @arg Vector from - The initial Vector
     * @arg Vector to - The desired Vector
     * @return {Vector} The lerped vector.
     */
    LerpVector(fraction: number, from: Vector, to: Vector): Vector;

    /**
     * Returns the contents of addonpresets.txt located in the garrysmod/settings folder. By default, this file stores your addon presets as JSON.
     *
     * @return {string} The contents of the file.
     */
    LoadAddonPresets(): string;

    /**
     * This function is used to get the last map and category to which the map belongs from the cookie saved with SaveLastMap.
     */
    LoadLastMap(): void;

    /**
     * Loads all preset settings for the presets and returns them in a table
     *
     * @return {object} Preset data
     */
    LoadPresets(): object;

    /**
     * Returns a localisation for the given token, if none is found it will return the default (second) parameter.
     *
     * @arg string localisationToken - The token to find a translation for.
     * @arg string def - The default value to be returned if no translation was found.
     */
    Localize(localisationToken: string, def: string): void;

    /**
     * Returns the player object of the current client.
     *
     * @return {Player} The player object representing the client.
     */
    LocalPlayer(): Player;

    /**
     * Translates the specified position and angle from the specified local coordinate system into worldspace coordinates.
     *
     * @arg Vector localPos - The position vector in the source coordinate system, that should be translated to world coordinates
     * @arg Angle localAng - The angle in the source coordinate system, that should be converted to a world angle. If you don't need to convert an angle, you can supply an arbitrary valid angle (e.g. Angle()).
     * @arg Vector originPos - The origin point of the source coordinate system, in world coordinates
     * @arg Angle originAngle - The angles of the source coordinate system, as a world angle
     * @return {Vector} The world position of the supplied local position.
     * @return {Angle} The world angles of the supplied local angle.
     */
    LocalToWorld(
        localPos: Vector,
        localAng: Angle,
        originPos: Vector,
        originAngle: Angle
    ): LuaMultiReturn<[Vector, Angle]>;

    /**
 * Either returns the material with the given name, or loads the material interpreting the first argument as the path.
* 
* @arg string materialName - The material name or path. The path is relative to the materials/ folder. You do not need to add materials/ to your path.
To retrieve a Lua material created with CreateMaterial, just prepend a "!" to the material name.
Since paths are relative to the materials folder, resource paths like ../data/MyImage.jpg will work since ".." translates to moving up a parent directory in the file tree.
.
* @arg string [pngParameters="nil"] - A string containing space separated keywords which will be used to add material parameters.
See Material Parameters for more information.
This feature only works when importing .png or .jpeg image files.
* @return {IMaterial} Generated material.
* @return {number} How long it took for the function to run.
 */
    Material(materialName: string, pngParameters?: string): LuaMultiReturn<[IMaterial, number]>;

    /**
 * Returns a VMatrix object.
* 
* @arg VMatrix [data={{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}] - Initial data to initialize the matrix with. Leave empty to initialize an identity matrix. See examples for usage.
Can be a VMatrix to copy its data.
* @return {VMatrix} New matrix.
 */
    Matrix(data?: VMatrix): VMatrix;

    /**
     * Returns a new mesh object.
     *
     * @arg IMaterial [mat=nil] - The material the mesh is intended to be rendered with. It's merely a hint that tells that mesh what vertex format it should use.
     * @return {IMesh} The created object.
     */
    Mesh(mat?: IMaterial): IMesh;

    /**
     * Runs util.PrecacheModel and returns the string.
     *
     * @arg string model - The model to precache.
     * @return {string} The same string entered as an argument.
     */
    Model(model: string): string;

    /**
     * Creates a table with the specified module name and sets the function environment for said table.
     *
     * @arg string name - The name of the module. This will be used to access the module table in the runtime environment.
     * @arg args[] loaders - Calls each function passed with the new table as an argument.
     */
    module(name: string, ...loaders: any[]): void;

    /**
     * Writes every given argument to the console.
     *
     * @arg args[] args - List of values to print.
     */
    Msg(...args: any[]): void;

    /**
     * Works exactly like Msg except that, if called on the server, will print to all players consoles plus the server console.
     *
     * @arg args[] args - List of values to print.
     */
    MsgAll(...args: any[]): void;

    /**
     * Just like Msg, except it can also print colored text, just like chat.AddText.
     *
     * @arg args[] args - Values to print. If you put in a color, all text after that color will be printed in that color.
     */
    MsgC(...args: any[]): void;

    /**
     * Same as print, except it concatinates the arguments without inserting any whitespace in between them.
     *
     * @arg args[] args - List of values to print. They can be of any type and will be converted to strings with tostring.
     */
    MsgN(...args: any[]): void;

    /**
     * Returns named color defined in resource/ClientScheme.res.
     *
     * @arg string name - Name of color
     * @return {Color} A Color or nil
     */
    NamedColor(name: string): Color;

    /**
     * Returns a new userdata object.
     *
     * @arg boolean [addMetatable=false] - If true, the userdata will get its own metatable automatically. If another newproxy is passed, it will create new one and copy its metatable.
     * @return {any} The newly created userdata.
     */
    newproxy(addMetatable?: boolean): any;

    /**
     * Returns the next key and value pair in a table.
     *
     * @arg object tab - The table
     * @arg any [prevKey=nil] - The previous key in the table.
     * @return {any} The next key for the table. If the previous key was nil, this will be the first key in the table. If the previous key was the last key in the table, this will be nil.
     * @return {any} The value associated with that key. If the previous key was the last key in the table, this will be nil.
     */
    next(tab: object, prevKey?: any): LuaMultiReturn<[any, any]>;

    /**
     * Returns the number of files needed from the server you are currently joining.
     *
     * @return {number} The number of downloadables
     */
    NumDownloadables(): number;

    /**
     * Returns the amount of skins the specified model has.
     *
     * @arg string modelName - Model to return amount of skins of
     * @return {number} Amount of skins
     */
    NumModelSkins(modelName: string): number;

    /**
     * Called by the engine when a model has been loaded. Caches model information with the sql.
     *
     * @arg string modelName - Name of the model.
     * @arg number numPostParams - Number of pose parameters the model has.
     * @arg number numSeq - Number of sequences the model has.
     * @arg number numAttachments - Number of attachments the model has.
     * @arg number numBoneControllers - Number of bone controllers the model has.
     * @arg number numSkins - Number of skins that the model has.
     * @arg number size - Size of the model.
     */
    OnModelLoaded(
        modelName: string,
        numPostParams: number,
        numSeq: number,
        numAttachments: number,
        numBoneControllers: number,
        numSkins: number,
        size: number
    ): void;

    /**
     * Opens a folder with the given name in the garrysmod folder using the operating system's file browser.
     *
     * @arg string folder - The subdirectory to open in the garrysmod folder.
     */
    OpenFolder(folder: string): void;

    /**
     * Modifies the given vectors so that all of vector2's axis are larger than vector1's by switching them around. Also known as ordering vectors.
     *
     * @arg Vector vector1 - Bounding box min resultant
     * @arg Vector vector2 - Bounding box max resultant
     */
    OrderVectors(vector1: Vector, vector2: Vector): void;

    /**
     * Returns an iterator function(next) for a for loop that will return the values of the specified table in an arbitrary order.
     *
     * @arg object tab - The table to iterate over.
     * @return {Function} The iterator (next).
     * @return {object} The table being iterated over.
     * @return {any} nil (for the constructor).
     */
    pairs(tab: object): LuaMultiReturn<[Function, object, any]>;

    /**
     * Calls game.AddParticles and returns given string.
     *
     * @arg string file - The particle file.
     * @return {string} The particle file.
     */
    Particle(file: string): string;

    /**
     * Creates a particle effect.
     *
     * @arg string particleName - The name of the particle effect.
     * @arg Vector position - The start position of the effect.
     * @arg Angle angles - The orientation of the effect.
     * @arg Entity [parent=NULL] - If set, the particle will be parented to the entity.
     */
    ParticleEffect(particleName: string, position: Vector, angles: Angle, parent?: Entity): void;

    /**
     * Creates a particle effect with specialized parameters.
     *
     * @arg string particleName - The name of the particle effect.
     * @arg number attachType - Attachment type using Enums/PATTACH.
     * @arg Entity entity - The entity to be used in the way specified by the attachType.
     * @arg number attachmentID - The id of the attachment to be used in the way specified by the attachType.
     */
    ParticleEffectAttach(particleName: string, attachType: number, entity: Entity, attachmentID: number): void;

    /**
 * Creates a new CLuaEmitter.
* 
* @arg Vector position - The start position of the emitter.
This is only used to determine particle drawing order for translucent particles.
* @arg boolean use3D - Whenever to render the particles in 2D or 3D mode.
* @return {CLuaEmitter} The new particle emitter.
 */
    ParticleEmitter(position: Vector, use3D: boolean): CLuaEmitter;

    /**
 * Creates a path for the bot to follow
* 
* @arg string type - The name of the path to create.
This is going to be "Follow" or "Chase" right now.
* @return {PathFollower} The path
 */
    Path(type: string): PathFollower;

    /**
     * Calls a function and catches an error that can be thrown while the execution of the call.
     *
     * @arg GMLua.CallbackNoContext func - Function to be executed and of which the errors should be caught of
     * @arg args[] arguments - Arguments to call the function with.
     * @return {boolean} If the function had no errors occur within it.
     * @return {args[]} If an error occurred, this will be a string containing the error message. Otherwise, this will be the return values of the function passed in.
     */
    pcall(func: GMLua.CallbackNoContext, ...arguments: any[]): LuaMultiReturn<[boolean, ...any[]]>;

    /**
     * Returns the player with the matching Player:UserID.
     *
     * @arg number playerIndex - The player index.
     * @return {Player} The retrieved player.
     */
    Player(playerIndex: number): Player;

    /**
     * Moves the given model to the given position and calculates appropriate camera parameters for rendering the model to an icon.
     *
     * @arg Entity model - Model that is being rendered to the spawn icon
     * @arg Vector position - Position that the model is being rendered at
     * @arg boolean noAngles - If true the function won't reset the angles to 0 for the model.
     * @return {object} Table of information of the view which can be used for rendering
     */
    PositionSpawnIcon(model: Entity, position: Vector, noAngles: boolean): object;

    /**
     * Precaches the particle with the specified name.
     *
     * @arg string particleSystemName - The name of the particle system.
     */
    PrecacheParticleSystem(particleSystemName: string): void;

    /**
     * Precaches a scene file.
     *
     * @arg string scene - Path to the scene file to precache.
     */
    PrecacheScene(scene: string): void;

    /**
     * Load and precache a custom sentence file.
     *
     * @arg string filename - The path to the custom sentences.txt.
     */
    PrecacheSentenceFile(filename: string): void;

    /**
     * Precache a sentence group in a sentences.txt definition file.
     *
     * @arg string group - The group to precache.
     */
    PrecacheSentenceGroup(group: string): void;

    /**
     * Writes every given argument to the console.
     * Automatically attempts to convert each argument to a string. (See tostring)
     *
     * @arg args[] args - List of values to print.
     */
    print(...args: any[]): void;

    /**
     * Displays a message in the chat, console, or center of screen of every player.
     *
     * @arg number type - Which type of message should be sent to the players (see Enums/HUD)
     * @arg string message - Message to be sent to the players
     */
    PrintMessage(type: number, message: string): void;

    /**
     * Recursively prints the contents of a table to the console.
     *
     * @arg object tableToPrint - The table to be printed
     * @arg number [indent=0] - Number of tabs to start indenting at. Increases by 2 when entering another table.
     * @arg object [done={}] - Internal argument, you shouldn't normally change this. Used to check if a nested table has already been printed so it doesn't get caught in a loop.
     */
    PrintTable(tableToPrint: object, indent?: number, done?: object): void;

    /**
     * Creates a new ProjectedTexture.
     *
     * @return {ProjectedTexture} Newly created projected texture.
     */
    ProjectedTexture(): ProjectedTexture;

    /**
     * Runs a function without stopping the whole script on error.
     *
     * @arg GMLua.CallbackNoContext func - Function to run
     * @return {boolean} Whether the function executed successfully or not
     */
    ProtectedCall(func: GMLua.CallbackNoContext): boolean;

    /**
     * Returns an iterator function that can be used to loop through a table in random order
     *
     * @arg object table - Table to create iterator for
     * @arg boolean descending - Whether the iterator should iterate descending or not
     * @return {Function} Iterator function
     */
    RandomPairs(table: object, descending: boolean): Function;

    /**
     * Compares the two values without calling their __eq operator.
     *
     * @arg any value1 - The first value to compare.
     * @arg any value2 - The second value to compare.
     * @return {boolean} Whether or not the two values are equal.
     */
    rawequal(value1: any, value2: any): boolean;

    /**
     * Gets the value with the specified key from the table without calling the __index method.
     *
     * @arg object table - Table to get the value from.
     * @arg any index - The index to get the value from.
     * @return {any} The value.
     */
    rawget(table: object, index: any): any;

    /**
     * Sets the value with the specified key from the table without calling the __newindex method.
     *
     * @arg object table - Table to get the value from.
     * @arg any index - The index to get the value from.
     * @arg any value - The value to set for the specified key.
     */
    rawset(table: object, index: any, value: any): void;

    /**
     * Returns the real frame-time which is unaffected by host_timescale. To be used for GUI effects (for example)
     *
     * @return {number} Real frame time
     */
    RealFrameTime(): number;

    /**
     * Returns the uptime of the game/server in seconds (to at least 4 decimal places). This value updates itself once every time the realm thinks. For servers, this is the server tickrate. For clients, its their current FPS.
     *
     * @return {number} Uptime of the game/server.
     */
    RealTime(): number;

    /**
 * Creates a new CRecipientFilter.
* 
* @arg boolean [unreliable=false] - If set to true, makes the filter unreliable.
This means, when sending over the network in cases like CreateSound (and its subsequent updates), the message is not guaranteed to reach all clients.
* @return {CRecipientFilter} The new created recipient filter.
 */
    RecipientFilter(unreliable?: boolean): CRecipientFilter;

    /**
     * Adds a frame to the currently recording demo.
     */
    RecordDemoFrame(): void;

    /**
     * Registers a Derma element to be closed the next time CloseDermaMenus is called
     *
     * @arg Panel menu - Menu to be registered for closure
     */
    RegisterDermaMenuForClose(menu: Panel): void;

    /**
     * Saves position of your cursor on screen. You can restore it by using
     * RestoreCursorPosition.
     */
    RememberCursorPosition(): void;

    /**
     * Does the removing of the tooltip panel. Called by EndTooltip.
     */
    RemoveTooltip(): void;

    /**
     * Returns the angle that the clients view is being rendered at
     *
     * @return {Angle} Render Angles
     */
    RenderAngles(): Angle;

    /**
     * Renders a Depth of Field effect
     *
     * @arg Vector origin - Origin to render the effect at
     * @arg Angle angle - Angle to render the effect at
     * @arg Vector usableFocusPoint - Point to focus the effect at
     * @arg number angleSize - Angle size of the effect
     * @arg number radialSteps - Amount of radial steps to render the effect with
     * @arg number passes - Amount of render passes
     * @arg boolean spin - Whether to cycle the frame or not
     * @arg object inView - Table of view data
     * @arg number fov - FOV to render the effect with
     */
    RenderDoF(
        origin: Vector,
        angle: Angle,
        usableFocusPoint: Vector,
        angleSize: number,
        radialSteps: number,
        passes: number,
        spin: boolean,
        inView: object,
        fov: number
    ): void;

    /**
     * Renders the stereoscopic post-process effect
     *
     * @arg Vector viewOrigin - Origin to render the effect at
     * @arg Angle viewAngles - Angles to render the effect at
     */
    RenderStereoscopy(viewOrigin: Vector, viewAngles: Angle): void;

    /**
     * Renders the Super Depth of Field post-process effect
     *
     * @arg Vector viewOrigin - Origin to render the effect at
     * @arg Angle viewAngles - Angles to render the effect at
     * @arg number viewFOV - Field of View to render the effect at
     */
    RenderSuperDoF(viewOrigin: Vector, viewAngles: Angle, viewFOV: number): void;

    /**
     * First tries to load a binary module with the given name, if unsuccessful, it tries to load a Lua module with the given name.
     *
     * @arg string name - The name of the module to be loaded.
     */
    require(name: string): void;

    /**
     * Restores position of your cursor on screen. You can save it by using RememberCursorPosition.
     */
    RestoreCursorPosition(): void;

    /**
     * Executes the given console command with the parameters.
     *
     * @arg string command - The command to be executed.
     * @arg args[] arguments - The arguments. Note, that unlike Player:ConCommand, you must pass each argument as a new string, not separating them with a space.
     */
    RunConsoleCommand(command: string, ...arguments: any[]): void;

    /**
 * Runs a menu command. Equivalent to RunConsoleCommand( "gamemenucommand", command ) unless the command starts with the "engine" keyword in which case it is equivalent to RunConsoleCommand( command ).
* 
* @arg string command - The menu command to run
Should be one of the following:

Disconnect - Disconnects from the current server.
OpenBenchmarkDialog - Opens the "Video Hardware Stress Test" dialog.
OpenChangeGameDialog - Does not work in GMod.
OpenCreateMultiplayerGameDialog - Opens the Source dialog for creating a listen server.
OpenCustomMapsDialog - Does nothing.
OpenFriendsDialog - Does nothing.
OpenGameMenu - Does not work in GMod.
OpenLoadCommentaryDialog - Opens the "Developer Commentary" selection dialog. Useless in GMod.
OpenLoadDemoDialog - Does nothing.
OpenLoadGameDialog - Opens the Source "Load Game" dialog.
OpenNewGameDialog - Opens the "New Game" dialog. Useless in GMod.
OpenOptionsDialog - Opens the options dialog.
OpenPlayerListDialog - Opens the "Mute Players" dialog that shows all players connected to the server and allows to mute them.
OpenSaveGameDialog - Opens the Source "Save Game" dialog.
OpenServerBrowser - Opens the legacy server browser.
Quit - Quits the game without confirmation (unlike other Source games).
QuitNoConfirm - Quits the game without confirmation (like other Source games).
ResumeGame - Closes the menu and returns to the game.
engine <concommand> - Runs a console command. Equivalent to RunConsoleCommand( <concommand> ).
 */
    RunGameUICommand(command: string): void;

    /**
     * Evaluates and executes the given code, will throw an error on failure.
     *
     * @arg string code - The code to execute.
     * @arg string [identifier="RunString"] - The name that should appear in any error messages caused by this code.
     * @arg boolean [handleError=true] - If false, this function will return a string containing any error messages instead of throwing an error.
     * @return {string} If handleError is false, the error message (if any).
     */
    RunString(code: string, identifier?: string, handleError?: boolean): string;

    /**
     * Alias of RunString.
     */
    RunStringEx(): void;

    /**
     * Removes the given entity unless it is a player or the world entity
     *
     * @arg Entity ent - Entity to safely remove.
     */
    SafeRemoveEntity(ent: Entity): void;

    /**
     * Removes entity after delay using SafeRemoveEntity
     *
     * @arg Entity entity - Entity to be removed
     * @arg number delay - Delay for entity removal in seconds
     */
    SafeRemoveEntityDelayed(entity: Entity, delay: number): void;

    /**
     * Sets the content of addonpresets.txt located in the garrysmod/settings folder. By default, this file stores your addon presets as JSON.
     *
     * @arg string JSON - The new contents of the file.
     */
    SaveAddonPresets(JSON: string): void;

    /**
     * This function is used to save the last map and category to which the map belongs as a .
     *
     * @arg string map - The name of the map.
     * @arg string category - The name of the category to which this map belongs.
     */
    SaveLastMap(map: string, category: string): void;

    /**
     * Overwrites all presets with the supplied table. Used by the presets for preset saving
     *
     * @arg object presets - Presets to be saved
     */
    SavePresets(presets: object): void;

    /**
     * Returns a number based on the Size argument and your screen's width. The screen's width is always equal to size 640. This function is primarily used for scaling font sizes.
     *
     * @arg number Size - The number you want to scale.
     * @return {number} The scaled number based on your screen's width
     */
    ScreenScale(Size: number): number;

    /**
     * Gets the height of the game's window (in pixels).
     *
     * @return {number} The height of the game's window in pixels
     */
    ScrH(): number;

    /**
     * Gets the width of the game's window (in pixels).
     *
     * @return {number} The width of the game's window in pixels
     */
    ScrW(): number;

    /**
 * Used to select single values from a vararg or get the count of values in it.
* 
* @arg any parameter - Can be a number or string.

If it's a string and starts with "#", the function will return the amount of values in the vararg (ignoring the rest of the string).
If it's a positive number, the function will return all values starting from the given index.
If the number is negative, it will return the amount specified from the end instead of the beginning. This mode will not be compiled by LuaJIT.
* @arg args[] vararg - The vararg. These are the values from which you want to select.
* @return {any} Returns a number or vararg, depending on the select method.
 */
    select(parameter: any, ...vararg: any[]): any;

    /**
     * Send a usermessage
     *
     * @arg string name - The name of the usermessage
     * @arg any recipients - Can be a CRecipientFilter, table or Player object.
     * @arg args[] args - Data to send in the usermessage
     */
    SendUserMessage(name: string, recipients: any, ...args: any[]): void;

    /**
     * Returns approximate duration of a sentence by name. See EmitSentence.
     *
     * @arg string name - The sentence name.
     * @return {number} The approximate duration.
     */
    SentenceDuration(name: string): number;

    /**
     * Prints "ServerLog: PARAM" without a newline, to the server log and console.
     *
     * @arg string parameter - The value to be printed to console.
     */
    ServerLog(parameter: string): void;

    /**
     * Adds the given string to the computers clipboard, which can then be pasted in or outside of GMod with Ctrl + V.
     *
     * @arg string text - The text to add to the clipboard.
     */
    SetClipboardText(text: string): void;

    /**
     * Sets the enviroment for a function or a stack level.
     *
     * @arg GMLua.CallbackNoContext location - The function to set the enviroment for or a number representing stack level.
     * @arg object enviroment - Table to be used as enviroment.
     * @return {Function} The function passed, otherwise nil.
     */
    setfenv(location: GMLua.CallbackNoContext, enviroment: object): Function;

    /**
     * Defines an angle to be automatically networked to clients
     *
     * @arg any index - Index to identify the global angle with
     * @arg Angle angle - Angle to be networked
     */
    SetGlobalAngle(index: any, angle: Angle): void;

    /**
     * Defined a boolean to be automatically networked to clients
     *
     * @arg any index - Index to identify the global boolean with
     * @arg boolean bool - Boolean to be networked
     */
    SetGlobalBool(index: any, bool: boolean): void;

    /**
     * Defines an entity to be automatically networked to clients
     *
     * @arg any index - Index to identify the global entity with
     * @arg Entity ent - Entity to be networked
     */
    SetGlobalEntity(index: any, ent: Entity): void;

    /**
     * Defines a floating point number to be automatically networked to clients
     *
     * @arg any index - Index to identify the global float with
     * @arg number float - Float to be networked
     */
    SetGlobalFloat(index: any, float: number): void;

    /**
     * Sets an integer that is shared between the server and all clients.
     *
     * @arg string index - The unique index to identify the global value with.
     * @arg number value - The value to set the global value to
     */
    SetGlobalInt(index: string, value: number): void;

    /**
     * Defines a string with a maximum of 199 characters to be automatically networked to clients
     *
     * @arg any index - Index to identify the global string with
     * @arg string string - String to be networked
     */
    SetGlobalString(index: any, string: string): void;

    /**
     * Defines a vector to be automatically networked to clients
     *
     * @arg any index - Index to identify the global vector with
     * @arg Vector vec - Vector to be networked
     */
    SetGlobalVector(index: any, vec: Vector): void;

    /**
     * Sets, changes or removes a table's metatable. Returns Tab (the first argument).
     *
     * @arg object Tab - The table who's metatable to change.
     * @arg object Metatable - The metatable to assign.  If it's nil, the metatable will be removed.
     * @return {object} The first argument.
     */
    setmetatable(Tab: object, Metatable: object): object;

    /**
     * Called by the engine to set which constraint system the next created constraints should use.
     *
     * @arg Entity constraintSystem - Constraint system to use
     */
    SetPhysConstraintSystem(constraintSystem: Entity): void;

    /**
     * This function can be used in a for loop instead of pairs. It sorts all keys alphabetically.
     *
     * @arg object table - The table to sort
     * @arg boolean [desc=false] - Reverse the sorting order
     * @return {Function} Iterator function
     * @return {object} The table being iterated over
     */
    SortedPairs(table: object, desc?: boolean): LuaMultiReturn<[Function, object]>;

    /**
     * Returns an iterator function that can be used to loop through a table in order of member values, when the values of the table are also tables and contain that member.
     *
     * @arg object table - Table to create iterator for.
     * @arg any memberKey - Key of the value member to sort by.
     * @arg boolean [descending=false] - Whether the iterator should iterate in descending order or not.
     * @return {Function} Iterator function
     * @return {object} The table the iterator was created for.
     */
    SortedPairsByMemberValue(table: object, memberKey: any, descending?: boolean): LuaMultiReturn<[Function, object]>;

    /**
     * Returns an iterator function that can be used to loop through a table in order of its values.
     *
     * @arg object table - Table to create iterator for
     * @arg boolean [descending=false] - Whether the iterator should iterate in descending order or not
     * @return {Function} Iterator function
     * @return {object} The table which will be iterated over
     */
    SortedPairsByValue(table: object, descending?: boolean): LuaMultiReturn<[Function, object]>;

    /**
     * Runs util.PrecacheSound and returns the string.
     *
     * @arg string soundPath - The soundpath to precache.
     * @return {string} The string passed as the first argument.
     */
    Sound(soundPath: string): string;

    /**
     * Returns the duration of the specified sound in seconds.
     *
     * @arg string soundName - The sound file path.
     * @return {number} Sound duration in seconds.
     */
    SoundDuration(soundName: string): number;

    /**
     * Returns the input value in an escaped form so that it can safely be used inside of queries. The returned value is surrounded by quotes unless noQuotes is true. Alias of sql.SQLStr
     *
     * @arg string input - String to be escaped
     * @arg boolean [noQuotes=false] - Whether the returned value should be surrounded in quotes or not
     * @return {string} Escaped input
     */
    SQLStr(input: string, noQuotes?: boolean): string;

    /**
     * Returns a number based on the Size argument and your screen's width. Alias of ScreenScale.
     *
     * @arg number Size - The number you want to scale.
     */
    SScale(Size: number): void;

    /**
     * Returns the ordinal suffix of a given number.
     *
     * @arg number number - The number to find the ordinal suffix of.
     * @return {string} suffix
     */
    STNDRD(number: number): string;

    /**
     * Suppress any networking from the server to the specified player. This is automatically called by the engine before/after a player fires their weapon, reloads, or causes any other similar shared-predicted event to occur.
     *
     * @arg Player suppressPlayer - The player to suppress any networking to.
     */
    SuppressHostEvents(suppressPlayer: Player): void;

    /**
     * Returns a highly accurate time in seconds since the start up, ideal for benchmarking. Unlike RealTime, this value will be updated any time the function is called, allowing for sub-think precision.
     *
     * @return {number} Uptime of the server.
     */
    SysTime(): number;

    /**
     * Returns a TauntCamera object
     *
     * @return {object} TauntCamera
     */
    TauntCamera(): object;

    /**
     * Clears focus from any text entries player may have focused.
     */
    TextEntryLoseFocus(): void;

    /**
     * Returns a cosine value that fluctuates based on the current time
     *
     * @arg number frequency - The frequency of fluctuation
     * @arg number min - Minimum value
     * @arg number max - Maximum value
     * @arg number offset - Offset variable that doesn't affect the rate of change, but causes the returned value to be offset by time
     * @return {number} Cosine value
     */
    TimedCos(frequency: number, min: number, max: number, offset: number): number;

    /**
     * Returns a sine value that fluctuates based on CurTime. The value returned will be between the start value plus/minus the range value.
     *
     * @arg number frequency - The frequency of fluctuation, in
     * @arg number origin - The center value of the sine wave.
     * @arg number max - This argument's distance from origin defines the size of the full range of the sine wave. For example, if origin is 3 and max is 5, then the full range of the sine wave is 5-3 = 2. 3 is the center point of the sine wave, so the sine wave will range between 2 and 4.
     * @arg number offset - Offset variable that doesn't affect the rate of change, but causes the returned value to be offset by time
     * @return {number} Sine value
     */
    TimedSin(frequency: number, origin: number, max: number, offset: number): number;

    /**
 * Attempts to return an appropriate boolean for the given value
* 
* @arg any val - The object to be converted to a boolean
* @return {boolean} false for the boolean false.
false for "false".
false for "0".
false for numeric 0.
false for nil.
true otherwise.
 */
    tobool(val: any): boolean;

    /**
     * Toggles whether or not the named map is favorited in the new game list.
     *
     * @arg string map - Map to toggle favorite.
     */
    ToggleFavourite(map: string): void;

    /**
     * Attempts to convert the value to a number.
     *
     * @arg any value - The value to convert. Can be a number or string.
     * @arg number [base=10] - The  used in the string. Can be any integer between 2 and 36, inclusive.
     * @return {number} The numeric representation of the value with the given base, or nil if the conversion failed.
     */
    tonumber(value: any, base?: number): number;

    /**
     * Attempts to convert the value to a string. If the value is an object and its metatable has defined the __tostring metamethod, this will call that function.
     *
     * @arg any value - The object to be converted to a string.
     * @return {string} The string representation of the value.
     */
    tostring(value: any): string;

    /**
     * Returns "Lua Cache File" if the given file name is in a certain string table, nothing otherwise.
     *
     * @arg string filename - File name to test
     * @return {string} "Lua Cache File" if the given file name is in a certain string table, nothing otherwise.
     */
    TranslateDownloadableName(filename: string): string;

    /**
     * Returns a string representing the name of the type of the passed object.
     *
     * @arg any variable - The object to get the type of.
     * @return {string} The name of the object's type.
     */
    type(variable: any): string;

    /**
     * Gets the associated type ID of the variable. Unlike type, this does not work with no value - an argument must be provided.
     *
     * @arg any variable - The variable to get the type ID of.
     * @return {number} The type ID of the variable. See the Enums/TYPE.
     */
    TypeID(variable: any): number;

    /**
     * This function takes a numeric indexed table and return all the members as a vararg. If specified, it will start at the given index and end at end index.
     *
     * @arg object tbl - The table to generate the vararg from.
     * @arg number [startIndex=1] - Which index to start from. Optional.
     * @arg number [endIndex=NaN] - Which index to end at. Optional, even if you set StartIndex.
     * @return {args[]} Output values
     */
    unpack(tbl: object, startIndex?: number, endIndex?: number): any;

    /**
     * Returns the current asynchronous in-game time.
     *
     * @return {number} The asynchronous in-game time.
     */
    UnPredictedCurTime(): number;

    /**
     * Runs JavaScript on the loading screen panel (GetLoadPanel).
     *
     * @arg string javascript - JavaScript to run on the loading panel.
     */
    UpdateLoadPanel(javascript: string): void;

    /**
     * Returns whether or not a model is useless by checking that the file path is that of a proper model.
     *
     * @arg string modelName - The model name to be checked
     * @return {boolean} Whether or not the model is useless
     */
    UTIL_IsUselessModel(modelName: string): boolean;

    /**
     * Returns if a panel is safe to use.
     *
     * @arg Panel panel - The panel to validate.
     */
    ValidPanel(panel: Panel): void;

    /**
 * Creates a Vector object.
* 
* @arg number [x=0] - The x component of the vector.
If this is a Vector, this function will return a copy of the given vector.
If this is a string, this function will try to parse the string as a vector. If it fails, it returns a 0 vector.
(See examples)
* @arg number [y=0] - The y component of the vector.
* @arg number [z=0] - The z component of the vector.
* @return {Vector} The created vector object.
 */
    Vector(x?: number, y?: number, z?: number): Vector;

    /**
     * Returns a random vector whose components are each between min(inclusive), max(exclusive).
     *
     * @arg number [min=-1] - Min bound inclusive.
     * @arg number [max=1] - Max bound exclusive.
     * @return {Vector} The random direction vector.
     */
    VectorRand(min?: number, max?: number): Vector;

    /**
     * Returns the time in seconds it took to render the VGUI.
     */
    VGUIFrameTime(): void;

    /**
     * Creates and returns a DShape rectangle GUI element with the given dimensions.
     *
     * @arg number x - X position of the created element
     * @arg number y - Y position of the created element
     * @arg number w - Width of the created element
     * @arg number h - Height of the created element
     * @return {Panel} DShape element
     */
    VGUIRect(x: number, y: number, w: number, h: number): Panel;

    /**
     * Briefly displays layout details of the given panel on-screen
     *
     * @arg Panel panel - Panel to display layout details of
     */
    VisualizeLayout(panel: Panel): void;

    /**
     * Returns a new WorkshopFileBase element
     *
     * @arg string namespace - Namespace for the file base
     * @arg object requiredTags - Tags required for a Workshop submission to be interacted with by the filebase
     * @return {object} WorkshopFileBase element
     */
    WorkshopFileBase(namespace: string, requiredTags: object): object;

    /**
     * Translates the specified position and angle into the specified coordinate system.
     *
     * @arg Vector position - The position that should be translated from the current to the new system.
     * @arg Angle angle - The angles that should be translated from the current to the new system.
     * @arg Vector newSystemOrigin - The origin of the system to translate to.
     * @arg Angle newSystemAngles - The angles of the system to translate to.
     * @return {Vector} Local position
     * @return {Angle} Local angles
     */
    WorldToLocal(
        position: Vector,
        angle: Angle,
        newSystemOrigin: Vector,
        newSystemAngles: Angle
    ): LuaMultiReturn<[Vector, Angle]>;

    /**
 * Attempts to call the first function. If the execution succeeds, this returns true followed by the returns of the function. If execution fails, this returns false and the second function is called with the error message.
* 
* @arg GMLua.CallbackNoContext func - The function to call initially.
* @arg GMLua.CallbackNoContext errorCallback - The function to be called if execution of the first fails; the error message is passed as a string.
You cannot throw an error() from this callback: it will have no effect (not even stopping the callback).
* @arg args[] arguments - Arguments to pass to the initial function.
* @return {boolean} Status of the execution; true for success, false for failure.
* @return {args[]} The returns of the first function if execution succeeded, otherwise the first return value of the error callback.
 */
    xpcall(
        func: GMLua.CallbackNoContext,
        errorCallback: GMLua.CallbackNoContext,
        ...arguments: any[]
    ): LuaMultiReturn<[boolean, ...any[]]>;
}

/**
 * Adds simple Get/Set accessor functions on the specified table.
 * Can also force the value to be set to a number, bool or string.
 *
 * @arg object tab - The table to add the accessor functions too.
 * @arg any key - The key of the table to be get/set.
 * @arg string name - The name of the functions (will be prefixed with Get and Set).
 * @arg number [force=NaN] - The type the setter should force to (uses Enums/FORCE).
 */
declare function AccessorFunc(tab: object, key: any, name: string, force?: number): void;

/**
 * Defines a global entity class variable with an automatic value in order to prevent collisions with other Enums/CLASS. You should prefix your variable with CLASS_ for consistency.
 *
 * @arg string name - The name of the new enum/global variable.
 */
declare function Add_NPC_Class(name: string): void;

/**
 * Adds the specified image path to the main menu background pool. Image can be png or jpeg.
 *
 * @arg string path - Path to the image.
 */
declare function AddBackgroundImage(path: string): void;

/**
 * Tells the engine to register a console command. If the command was ran, the engine calls concommand.Run.
 *
 * @arg string name - The name of the console command to add.
 * @arg string helpText - The help text.
 * @arg number flags - Concommand flags using Enums/FCVAR
 */
declare function AddConsoleCommand(name: string, helpText: string, flags: number): void;

/**
 * Marks a Lua file to be sent to clients when they join the server. Doesn't do anything on the client - this means you can use it in a shared file without problems.
* 
* @arg string [file="current file"] - The name/path to the Lua file that should be sent, relative to the garrysmod/lua folder. If no parameter is specified, it sends the current file.
The file path can be relative to the script it's ran from. For example, if your script is in lua/myfolder/stuff.lua, calling AddCSLuaFile("otherstuff.lua") and AddCSLuaFile("myfolder/otherstuff.lua") is the same thing.
Please make sure your file names are unique, the filesystem is shared across all addons, so a file named lua/config.lua in your addon may be overwritten by the same file in another addon.
 */
declare function AddCSLuaFile(file?: string): void;

/**
 * Loads the specified image from the /cache folder, used in combination steamworks.Download. Most addons will provide a 512x512 png image.
 *
 * @arg string name - The name of the file.
 * @return {IMaterial} The material, returns nil if the cached file is not an image.
 */
declare function AddonMaterial(name: string): IMaterial;

/**
 * Adds the specified vector to the PVS which is currently building. This allows all objects in visleafs visible from that vector to be drawn.
 *
 * @arg Vector position - The origin to add.
 */
declare function AddOriginToPVS(position: Vector): void;

/**
 * This function creates a World Tip, similar to the one shown when aiming at a Thruster where it shows you its force.
 *
 * @arg number [entindex=NaN] - This argument is no longer used; it has no effect on anything. You can use nil in this argument.
 * @arg string text - The text for the world tip to display.
 * @arg number [dieTime=NaN] - This argument is no longer used; when you add a World Tip it will always last only 0.05 seconds. You can use nil in this argument.
 * @arg Vector [pos=ent:GetPos()] - Where in the world you want the World Tip to be drawn. If you add a valid Entity in the next argument, this argument will have no effect on the actual World Tip.
 * @arg Entity [ent=nil] - Which entity you want to associate with the World Tip. This argument is optional. If set to a valid entity, this will override the position set in pos with the Entity's position.
 */
declare function AddWorldTip(entindex?: number, text?: string, dieTime?: number, pos?: Vector, ent?: Entity): void;

/**
 * Creates an Angle object.
* 
* @arg number [pitch=0] - The pitch value of the angle.
If this is an Angle, this function will return a copy of the given angle.
If this is a string, this function will try to parse the string as a angle. If it fails, it returns a 0 angle.
(See examples)
* @arg number [yaw=0] - The yaw value of the angle.
* @arg number [roll=0] - The roll value of the angle.
* @return {Angle} Created angle
 */
declare function Angle(pitch?: number, yaw?: number, roll?: number): Angle;

/**
 * Returns an angle with a randomized pitch, yaw, and roll between min(inclusive), max(exclusive).
 *
 * @arg number [min=-90] - Min bound inclusive.
 * @arg number [max=90] - Max bound exclusive.
 * @return {Angle} The randomly generated angle.
 */
declare function AngleRand(min?: number, max?: number): Angle;

/**
 * If the result of the first argument is false or nil, an error is thrown with the second argument as the message.
* 
* @arg any expression - The expression to assert.
* @arg string [errorMessage="assertion failed!"] - The error message to throw when assertion fails. This is only type-checked if the assertion fails.
* @arg args[] [returns=nil] - Any arguments past the error message will be returned by a successful assert.
* @return {any} If successful, returns the first argument.
* @return {any} If successful, returns the error message. This will be nil if the second argument wasn't specified.
Since the second argument is only type-checked if the assertion fails, this doesn't have to be a string.
* @return {args[]} Returns any arguments past the error message.
 */
declare function assert(
    expression: any,
    errorMessage?: string,
    ...returns: any[]
): LuaMultiReturn<[any, any, ...any[]]>;

/**
 * Sends the specified Lua code to all connected clients and executes it.
 *
 * @arg string code - The code to be executed. Capped at length of 254 characters.
 */
declare function BroadcastLua(code: string): void;

/**
 * Dumps the networked variables of all entities into one table and returns it.
* 
* @return {Entity} Format:

key = Entity for NWVars or number (always 0) for global vars
value = table formatted as:

key = string var name
value = any type var value
 */
declare function BuildNetworkedVarsTable(): Entity;

/**
 * Used internally to check if the current server the player is on can be added to favorites or not. Does not check if the server is ALREADY in the favorites.
 *
 * @return {boolean} Can add to favorites
 */
declare function CanAddServerToFavorites(): boolean;

/**
 * Aborts joining of the server you are currently joining.
 */
declare function CancelLoading(): void;

/**
 * Sets the active main menu background image to a random entry from the background images pool. Images are added with AddBackgroundImage.
 *
 * @arg string currentgm - Apparently does nothing.
 */
declare function ChangeBackground(currentgm: string): void;

/**
 * Automatically called by the engine when a panel is hovered over with the mouse
 *
 * @arg Panel panel - Panel that has been hovered over
 */
declare function ChangeTooltip(panel: Panel): void;

/**
 * Empties the pool of main menu background images.
 */
declare function ClearBackgroundImages(): void;

/**
 * Creates a non physical entity that only exists on the client. See also ents.CreateClientProp.
* 
* @arg string model - The file path to the model.
Model must be precached with util.PrecacheModel on the server before usage.
* @arg number [renderGroup=NaN] - The render group of the entity for the clientside leaf system, see Enums/RENDERGROUP.
* @return {CSEnt} Created client-side model (C_BaseFlex).
 */
declare function ClientsideModel(model: string, renderGroup?: number): CSEnt;

/**
 * Creates a fully clientside ragdoll.
* 
* @arg string model - The file path to the model.
Model must be precached with util.PrecacheModel on the server before usage.
* @arg number [renderGroup=NaN] - The Enums/RENDERGROUP to assign.
* @return {CSEnt} The newly created client-side ragdoll. ( C_ClientRagdoll )
 */
declare function ClientsideRagdoll(model: string, renderGroup?: number): CSEnt;

/**
 * Creates a scene entity based on the scene name and the entity.
 *
 * @arg string name - The name of the scene.
 * @arg Entity targetEnt - The entity to play the scene on.
 * @return {CSEnt} C_SceneEntity
 */
declare function ClientsideScene(name: string, targetEnt: Entity): CSEnt;

/**
 * Closes all Derma menus that have been passed to RegisterDermaMenuForClose and calls GM:CloseDermaMenus
 */
declare function CloseDermaMenus(): void;

/**
 * Executes the specified action on the garbage collector.
* 
* @arg string [action="collect"] - The action to run.
Valid actions are collect, stop, restart, count, step, setpause and setstepmul.
* @arg number arg - The argument of the specified action, only applicable for step, setpause and setstepmul.
* @return {any} If the action is count this is the number of kilobytes of memory used by Lua.
If the action is step this is true if a garbage collection cycle was finished.
If the action is setpause this is the previous value for the GC's pause.
If the action is setstepmul this is the previous value for the GC's step.
 */
declare function collectgarbage(action?: string, arg?: number): any;

/**
 * Creates a Color.
 *
 * @arg number r - An integer from 0-255 describing the red value of the color.
 * @arg number g - An integer from 0-255 describing the green value of the color.
 * @arg number b - An integer from 0-255 describing the blue value of the color.
 * @arg number [a=255] - An integer from 0-255 describing the alpha (transparency) of the color.
 * @return {Color} The created Color.
 */
declare function Color(r: number, g: number, b: number, a?: number): Color;

/**
 * Returns a new Color with the RGB components of the given Color and the alpha value specified.
 *
 * @arg Color color - The Color from which to take RGB values. This color will not be modified.
 * @arg number alpha - The new alpha value, a number between 0 and 255. Values above 255 will be clamped.
 * @return {Color} The new Color with the modified alpha value
 */
declare function ColorAlpha(color: Color, alpha: number): Color;

/**
 * Creates a Color with randomized red, green, and blue components. If the alpha argument is true, alpha will also be randomized.
 *
 * @arg boolean [a=false] - Should alpha be randomized.
 * @return {Color} The created Color.
 */
declare function ColorRand(a?: boolean): Color;

/**
 * Converts a Color into HSL color space.
 *
 * @arg Color color - The Color.
 * @return {number} The hue in degrees [0, 360].
 * @return {number} The saturation in the range [0, 1].
 * @return {number} The lightness in the range [0, 1].
 */
declare function ColorToHSL(color: Color): LuaMultiReturn<[number, number, number]>;

/**
 * Converts a Color into HSV color space.
 *
 * @arg Color color - The Color.
 * @return {number} The hue in degrees [0, 360].
 * @return {number} The saturation in the range [0, 1].
 * @return {number} The value in the range [0, 1].
 */
declare function ColorToHSV(color: Color): LuaMultiReturn<[number, number, number]>;

/**
 * Attempts to compile the given file. If successful, returns a function that can be called to perform the actual execution of the script.
 *
 * @arg string path - Path to the file, relative to the garrysmod/lua/ directory.
 * @return {Function} The function which executes the script.
 */
declare function CompileFile(path: string): Function;

/**
 * This function will compile the code argument as lua code and return a function that will execute that code.
* 
* @arg string code - The code to compile.
* @arg string identifier - An identifier in case an error is thrown. (The same identifier can be used multiple times)
* @arg boolean [HandleError=true] - If false this function will return an error string instead of throwing an error.
* @return {Function} A function that, when called, will execute the given code.
Returns the error string if there was a Lua error and third argument is false.
 */
declare function CompileString(code: string, identifier: string, HandleError?: boolean): Function;

/**
 * Returns a table of console command names beginning with the given text.
 *
 * @arg string text - Text that the console commands must begin with.
 * @return {object} Table of console command names.
 */
declare function ConsoleAutoComplete(text: string): object;

/**
 * Returns whether a ConVar with the given name exists or not
 *
 * @arg string name - Name of the ConVar.
 * @return {boolean} True if the ConVar exists, false otherwise.
 */
declare function ConVarExists(name: string): boolean;

/**
 * Makes a clientside-only console variable
* 
* @arg string name - Name of the ConVar to be created and able to be accessed.
This cannot be a name of existing console command or console variable. It will silently fail if it is.
* @arg string def - Default value of the ConVar.
* @arg boolean [shouldsave=true] - Should the ConVar be saved across sessions in the cfg/client.vdf file.
* @arg boolean [userinfo=false] - Should the ConVar and its containing data be sent to the server when it has changed. This make the convar accessible from server using Player:GetInfoNum and similar functions.
* @arg string helptext - Help text to display in the console.
* @arg number [min=NaN] - If set, the convar cannot be changed to a number lower than this value.
* @arg number [max=NaN] - If set, the convar cannot be changed to a number higher than this value.
* @return {ConVar} Created convar.
 */
declare function CreateClientConVar(
    name: string,
    def: string,
    shouldsave?: boolean,
    userinfo?: boolean,
    helptext?: string,
    min?: number,
    max?: number
): ConVar;

/**
 * Creates a console variable (ConVar), in general these are for things like gamemode/server settings.
* 
* @arg string name - Name of the ConVar.
This cannot be a name of an engine console command or console variable. It will silently fail if it is. If it is the same name as another lua ConVar, it will return that ConVar object.
* @arg string value - Default value of the convar. Can also be a number.
* @arg number [flags=NaN] - Flags of the convar, see Enums/FCVAR, either as bitflag or as table.
* @arg string helptext - The help text to show in the console.
* @arg number [min=NaN] - If set, the ConVar cannot be changed to a number lower than this value.
* @arg number [max=NaN] - If set, the ConVar cannot be changed to a number higher than this value.
* @return {ConVar} The convar created.
 */
declare function CreateConVar(
    name: string,
    value: string,
    flags?: number,
    helptext?: string,
    min?: number,
    max?: number
): ConVar;

/**
 * Creates a new material with the specified name and shader.
* 
* @arg string name - The material name. Must be unique.
* @arg string shaderName - The shader name. See Shaders.
* @arg object materialData - Key-value table that contains shader parameters and proxies.

See: List of Shader Parameters on Valve Developers Wiki and each shader's page from .

Unlike IMaterial:SetTexture, this table will not accept ITexture values. Instead, use the texture's name (see ITexture:GetName).
* @return {IMaterial} Created material
 */
declare function CreateMaterial(name: string, shaderName: string, materialData: object): IMaterial;

/**
 * Creates a new particle system.
 *
 * @arg Entity ent - The entity to attach the control point to.
 * @arg string effect - The name of the effect to create. It must be precached.
 * @arg number partAttachment - See Enums/PATTACH.
 * @arg number [entAttachment=0] - The attachment ID on the entity to attach the particle system to
 * @arg Vector [offset=Vector( 0, 0, 0 )] - The offset from the Entity:GetPos of the entity we are attaching this CP to.
 * @return {CNewParticleEffect} The created particle system.
 */
declare function CreateParticleSystem(
    ent: Entity,
    effect: string,
    partAttachment: number,
    entAttachment?: number,
    offset?: Vector
): CNewParticleEffect;

/**
 * Creates a new PhysCollide from the given bounds.
 *
 * @arg Vector mins - Min corner of the box. This is not automatically ordered with the maxs and must contain the smallest vector components. See OrderVectors.
 * @arg Vector maxs - Max corner of the box. This is not automatically ordered with the mins and must contain the largest vector components.
 * @return {PhysCollide} The new PhysCollide. This will be a NULL PhysCollide (PhysCollide:IsValid returns false) if given bad vectors or no more PhysCollides can be created in the physics engine.
 */
declare function CreatePhysCollideBox(mins: Vector, maxs: Vector): PhysCollide;

/**
 * Creates PhysCollide objects for every physics object the model has. The model must be precached with util.PrecacheModel before being used with this function.
 *
 * @arg string modelName - Model path to get the collision objects of.
 * @return {PhysCollide} Table of PhysCollide objects. The number of entries will match the model's physics object count. See also Entity:GetPhysicsObjectCount. Returns no value if the model doesn't exist, or has not been precached.
 */
declare function CreatePhysCollidesFromModel(modelName: string): PhysCollide;

/**
 * Returns a sound parented to the specified entity.
* 
* @arg Entity targetEnt - The target entity.
* @arg string soundName - The sound to play.
* @arg CRecipientFilter [filter=nil] - A CRecipientFilter of the players that will have this sound networked to them.
If not set, the default is a CPASAttenuationFilter.
This argument only works serverside.
* @return {CSoundPatch} The sound object
 */
declare function CreateSound(targetEnt: Entity, soundName: string, filter?: CRecipientFilter): CSoundPatch;

/**
 * Creates and returns a new DSprite element with the supplied material.
 *
 * @arg IMaterial material - Material the sprite should draw.
 * @return {Panel} The new DSprite element.
 */
declare function CreateSprite(material: IMaterial): Panel;

/**
 * Returns the uptime of the server in seconds (to at least 4 decimal places)
 *
 * @return {number} Time synced with the game server.
 */
declare function CurTime(): number;

/**
 * Returns an CTakeDamageInfo object.
 *
 * @return {CTakeDamageInfo} The CTakeDamageInfo object.
 */
declare function DamageInfo(): CTakeDamageInfo;

/**
 * Writes text to the right hand side of the screen, like the old error system. Messages disappear after a couple of seconds.
 *
 * @arg number slot - The location on the right hand screen to write the debug info to. Starts at 0, no upper limit
 * @arg string info - The debugging information to be written to the screen
 */
declare function DebugInfo(slot: number, info: string): void;

/**
 * This is not a function. This is a preprocessor keyword that translates to:
 *
 * @arg string value - Baseclass name
 */
declare function DEFINE_BASECLASS(value: string): void;

/**
 * Loads and registers the specified gamemode, setting the GM table's DerivedFrom field to the value provided, if the table exists. The DerivedFrom field is used post-gamemode-load as the "derived" parameter for gamemode.Register.
 *
 * @arg string base - Gamemode name to derive from.
 */
declare function DeriveGamemode(base: string): void;

/**
 * Creates a new derma animation.
* 
* @arg string name - Name of the animation to create
* @arg Panel panel - Panel to run the animation on
* @arg GMLua.CallbackNoContext func - Function to call to process the animation
Arguments:

Panel pnl - the panel passed to Derma_Anim
table anim - the anim table
number delta - the fraction of the progress through the animation
any data - optional data passed to the run metatable method
* @return {object} A lua metatable containing four methods:

Run() - Should be called each frame you want the animation to be ran.
Active() - Returns if the animation is currently active (has not finished and stop has not been called)
Stop() - Halts the animation at its current progress.
Start( Length, Data ) - Prepares the animation to be ran for Length seconds. Must be called once before calling Run(). The data parameter will be passed to the func function.
 */
declare function Derma_Anim(name: string, panel: Panel, func: GMLua.CallbackNoContext): object;

/**
 * Draws background blur around the given panel.
 *
 * @arg Panel panel - Panel to draw the background blur around
 * @arg number startTime - Time that the blur began being painted
 */
declare function Derma_DrawBackgroundBlur(panel: Panel, startTime: number): void;

/**
 * Creates panel method that calls the supplied Derma skin hook via derma.SkinHook
 *
 * @arg Panel panel - Panel to add the hook to
 * @arg string functionName - Name of panel function to create
 * @arg string hookName - Name of Derma skin hook to call within the function
 * @arg string typeName - Type of element to call Derma skin hook for
 */
declare function Derma_Hook(panel: Panel, functionName: string, hookName: string, typeName: string): void;

/**
 * Makes the panel (usually an input of sorts) respond to changes in console variables by adding next functions to the panel:
 *
 * @arg Panel target - The panel the functions should be added to.
 */
declare function Derma_Install_Convar_Functions(target: Panel): void;

/**
 * Creates a derma window to display information
 *
 * @arg string Text - The text within the created panel.
 * @arg string Title - The title of the created panel.
 * @arg string Button - The text of the button to close the panel.
 * @return {Panel} The created DFrame
 */
declare function Derma_Message(Text: string, Title: string, Button: string): Panel;

/**
 * Shows a message box in the middle of the screen, with up to 4 buttons they can press.
 *
 * @arg string [text="Message Text (Second Parameter)"] - The message to display.
 * @arg string [title="Message Title (First Parameter)"] - The title to give the message box.
 * @arg string btn1text - The text to display on the first button.
 * @arg GMLua.CallbackNoContext [btn1func=nil] - The function to run if the user clicks the first button.
 * @arg string [btn2text="nil"] - The text to display on the second button.
 * @arg GMLua.CallbackNoContext [btn2func=nil] - The function to run if the user clicks the second button.
 * @arg string [btn3text="nil"] - The text to display on the third button
 * @arg GMLua.CallbackNoContext [btn3func=nil] - The function to run if the user clicks the third button.
 * @arg string [btn4text="nil"] - The text to display on the fourth button
 * @arg GMLua.CallbackNoContext [btn4func=nil] - The function to run if the user clicks the fourth button.
 * @return {Panel} The Panel object of the created window.
 */
declare function Derma_Query(
    text?: string,
    title?: string,
    btn1text?: string,
    btn1func?: GMLua.CallbackNoContext,
    btn2text?: string,
    btn2func?: GMLua.CallbackNoContext,
    btn3text?: string,
    btn3func?: GMLua.CallbackNoContext,
    btn4text?: string,
    btn4func?: GMLua.CallbackNoContext
): Panel;

/**
 * Creates a derma window asking players to input a string.
 *
 * @arg string title - The title of the created panel.
 * @arg string subtitle - The text above the input box
 * @arg string def - The default text for the input box.
 * @arg GMLua.CallbackNoContext confirm - The function to be called once the user has confirmed their input.
 * @arg GMLua.CallbackNoContext [cancel=nil] - The function to be called once the user has cancelled their input
 * @arg string [confirmText="OK"] - Allows you to override text of the "OK" button
 * @arg string [cancelText="Cancel"] - Allows you to override text of the "Cancel" button
 * @return {Panel} The created DFrame
 */
declare function Derma_StringRequest(
    title: string,
    subtitle: string,
    def: string,
    confirm: GMLua.CallbackNoContext,
    cancel?: GMLua.CallbackNoContext,
    confirmText?: string,
    cancelText?: string
): Panel;

/**
 * Creates a DMenu and closes any current menus.
 *
 * @arg boolean [keepOpen=false] - If we should keep other DMenus open (true) or not (false).
 * @arg Panel [parent=nil] - The panel to parent the created menu to.
 * @return {Panel} The created DMenu.
 */
declare function DermaMenu(keepOpen?: boolean, parent?: Panel): Panel;

/**
 * Sets whether rendering should be limited to being inside a panel or not.
 *
 * @arg boolean disable - Whether or not clipping should be disabled
 * @return {boolean} Whether the clipping was enabled or not before this function call
 */
declare function DisableClipping(disable: boolean): boolean;

/**
 * Cancels current DOF post-process effect started with DOF_Start
 */
declare function DOF_Kill(): void;

/**
 * Cancels any existing DOF post-process effects.
 * Begins the DOF post-process effect.
 */
declare function DOF_Start(): void;

/**
 * A hacky method used to fix some bugs regarding DoF. What this basically does it force all C_BaseAnimating entities to have the translucent rendergroup, even if they use opaque or two-pass models.
 *
 * @arg boolean enable - Enables or disables depth-of-field mode
 */
declare function DOFModeHack(enable: boolean): void;

/**
 * Draws the currently active main menu background image and handles transitioning between background images.
 */
declare function DrawBackground(): void;

/**
 * Draws the bloom shader, which creates a glowing effect from bright objects.
 *
 * @arg number Darken - Determines how much to darken the effect. A lower number will make the glow come from lower light levels. A value of 1 will make the bloom effect unnoticeable. Negative values will make even pitch black areas glow.
 * @arg number Multiply - Will affect how bright the glowing spots are. A value of 0 will make the bloom effect unnoticeable.
 * @arg number SizeX - The size of the bloom effect along the horizontal axis.
 * @arg number SizeY - The size of the bloom effect along the vertical axis.
 * @arg number Passes - Determines how much to exaggerate the effect.
 * @arg number ColorMultiply - Will multiply the colors of the glowing spots, making them more vivid.
 * @arg number Red - How much red to multiply with the glowing color. Should be between 0 and 1.
 * @arg number Green - How much green to multiply with the glowing color. Should be between 0 and 1.
 * @arg number Blue - How much blue to multiply with the glowing color. Should be between 0 and 1.
 */
declare function DrawBloom(
    Darken: number,
    Multiply: number,
    SizeX: number,
    SizeY: number,
    Passes: number,
    ColorMultiply: number,
    Red: number,
    Green: number,
    Blue: number
): void;

/**
 * Draws the Bokeh Depth Of Field effect .
 *
 * @arg number intensity - Intensity of the effect.
 * @arg number distance - Not worldspace distance. Value range is from 0 to 1.
 * @arg number focus - Focus. Recommended values are from 0 to 12.
 */
declare function DrawBokehDOF(intensity: number, distance: number, focus: number): void;

/**
 * Draws the Color Modify shader, which can be used to adjust colors on screen.
 *
 * @arg any modifyParameters - Color modification parameters. See Shaders/g_colourmodify and the example below. Note that if you leave out a field, it will retain its last value which may have changed if another caller uses this function.
 */
declare function DrawColorModify(modifyParameters: any): void;

/**
 * Draws a material overlay on the screen.
 *
 * @arg string Material - This will be the material that is drawn onto the screen.
 * @arg number RefractAmount - This will adjust how much the material will refract your screen.
 */
declare function DrawMaterialOverlay(Material: string, RefractAmount: number): void;

/**
 * Creates a motion blur effect by drawing your screen multiple times.
 *
 * @arg number AddAlpha - How much alpha to change per frame.
 * @arg number DrawAlpha - How much alpha the frames will have. A value of 0 will not render the motion blur effect.
 * @arg number Delay - Determines the amount of time between frames to capture.
 */
declare function DrawMotionBlur(AddAlpha: number, DrawAlpha: number, Delay: number): void;

/**
 * Draws the sharpen shader, which creates more contrast.
 *
 * @arg number Contrast - How much contrast to create.
 * @arg number Distance - How large the contrast effect will be.
 */
declare function DrawSharpen(Contrast: number, Distance: number): void;

/**
 * Draws the sobel shader, which detects edges and draws a black border.
 *
 * @arg number Threshold - Determines the threshold of edges. A value of 0 will make your screen completely black.
 */
declare function DrawSobel(Threshold: number): void;

/**
 * Renders the post-processing effect of beams of light originating from the map's sun. Utilises the pp/sunbeams material.
 *
 * @arg number darken - $darken property for sunbeams material.
 * @arg number multiplier - $multiply property for sunbeams material.
 * @arg number sunSize - $sunsize property for sunbeams material.
 * @arg number sunX - $sunx property for sunbeams material.
 * @arg number sunY - $suny property for sunbeams material.
 */
declare function DrawSunbeams(darken: number, multiplier: number, sunSize: number, sunX: number, sunY: number): void;

/**
 * Draws the texturize shader, which replaces each pixel on your screen with a different part of the texture depending on its brightness. See g_texturize for information on making the texture.
 *
 * @arg number Scale - Scale of the texture. A smaller number creates a larger texture.
 * @arg number BaseTexture - This will be the texture to use in the effect. Make sure you use Material to get the texture number.
 */
declare function DrawTexturize(Scale: number, BaseTexture: number): void;

/**
 * Draws the toy town shader, which blurs the top and bottom of your screen. This can make very large objects look like toys, hence the name.
 *
 * @arg number Passes - An integer determining how many times to draw the effect. A higher number creates more blur.
 * @arg number Height - The amount of screen which should be blurred on the top and bottom.
 */
declare function DrawToyTown(Passes: number, Height: number): void;

/**
 * Drops the specified entity if it is being held by any player with Gravity Gun or +use pickup.
 *
 * @arg Entity ent - The entity to drop.
 */
declare function DropEntityIfHeld(ent: Entity): void;

/**
 * Creates or replaces a dynamic light with the given id.
 *
 * @arg number index - An unsigned Integer. Usually an entity index is used here.
 * @arg boolean [elight=false] - Allocates an elight instead of a dlight. Elights have a higher light limit and do not light the world (making the "noworld" parameter have no effect).
 * @return {DynamicLightStruct} A DynamicLight structured table. See Structures/DynamicLight
 */
declare function DynamicLight(index: number, elight?: boolean): DynamicLightStruct;

/**
 * Returns a CEffectData object to be used with util.Effect.
 *
 * @return {CEffectData} The CEffectData object.
 */
declare function EffectData(): CEffectData;

/**
 * An eagerly evaluated ternary operator, or, in layman's terms, a compact "if then else" statement.
 *
 * @arg any condition - The condition to check if true or false.
 * @arg any truevar - If the condition isn't nil/false, returns this value.
 * @arg any falsevar - If the condition is nil/false, returns this value.
 * @return {any} The result.
 */
declare function Either(condition: any, truevar: any, falsevar: any): any;

/**
 * Plays a sentence from scripts/sentences.txt
 *
 * @arg string soundName - The sound to play
 * @arg Vector position - The position to play at
 * @arg number entity - The entity to emit the sound from. Must be Entity:EntIndex
 * @arg number [channel=NaN] - The sound channel, see Enums/CHAN.
 * @arg number [volume=1] - The volume of the sound, from 0 to 1
 * @arg number [soundLevel=75] - The sound level of the sound, see Enums/SNDLVL
 * @arg number [soundFlags=0] - The flags of the sound, see Enums/SND
 * @arg number [pitch=100] - The pitch of the sound, 0-255
 */
declare function EmitSentence(
    soundName: string,
    position: Vector,
    entity: number,
    channel?: number,
    volume?: number,
    soundLevel?: number,
    soundFlags?: number,
    pitch?: number
): void;

/**
 * Emits the specified sound at the specified position.
* 
* @arg string soundName - The sound to play
* @arg Vector position - The position where the sound is meant to play, used only for a network  filter (CPASAttenuationFilter) to decide which players will hear the sound.
* @arg number entity - The entity to emit the sound from. Can be an Entity:EntIndex or one of the following:

0 - Plays sound on the world (position set to 0,0,0)
-1 - Plays sound on the local player (on server acts as 0)
-2 - Plays UI sound (position set to 0,0,0, no spatial sound, on server acts as 0)
* @arg number [channel=NaN] - The sound channel, see Enums/CHAN.
* @arg number [volume=1] - The volume of the sound, from 0 to 1
* @arg number [soundLevel=75] - The sound level of the sound, see Enums/SNDLVL
* @arg number [soundFlags=0] - The flags of the sound, see Enums/SND
* @arg number [pitch=100] - The pitch of the sound, 0-255
* @arg number [dsp=0] - The DSP preset for this sound. List of DSP presets
 */
declare function EmitSound(
    soundName: string,
    position: Vector,
    entity: number,
    channel?: number,
    volume?: number,
    soundLevel?: number,
    soundFlags?: number,
    pitch?: number,
    dsp?: number
): void;

/**
 * Removes the currently active tool tip from the screen.
 *
 * @arg Panel panel - This is the panel that has a tool tip.
 */
declare function EndTooltip(panel: Panel): void;

/**
 * Returns the entity with the matching Entity:EntIndex.
 *
 * @arg number entityIndex - The entity index.
 * @return {Entity} The entity if it exists, or NULL if it doesn't.
 */
declare function Entity(entityIndex: number): Entity;

/**
 * Throws a Lua error and breaks out of the current call stack.
 *
 * @arg string message - The error message to throw
 * @arg number [errorLevel=1] - The level to throw the error at.
 */
declare function error(message: string, errorLevel?: number): void;

/**
 * Throws a Lua error but does not break out of the current call stack.
 * This function will not print a stack trace like a normal error would.
 * Essentially similar if not equivalent to Msg.
 *
 * @arg args[] arguments - Converts all arguments to strings and prints them with no spacing.
 */
declare function ErrorNoHalt(...arguments: any[]): void;

/**
 * Throws a Lua error but does not break out of the current call stack.
 *
 * @arg args[] arguments - Converts all arguments to strings and prints them with no spacing.
 */
declare function ErrorNoHaltWithStack(...arguments: any[]): void;

/**
 * Returns the angles of the current render context as calculated by GM:CalcView.
 *
 * @return {Angle} The angle of the currently rendered scene.
 */
declare function EyeAngles(): Angle;

/**
 * Returns the origin of the current render context as calculated by GM:CalcView.
 *
 * @return {Vector} Camera position.
 */
declare function EyePos(): Vector;

/**
 * Returns the normal vector of the current render context as calculated by GM:CalcView, similar to EyeAngles.
 *
 * @return {Vector} View direction of the currently rendered scene.
 */
declare function EyeVector(): Vector;

/**
 * Returns the meta table for the class with the matching name.
 *
 * @arg string metaName - The object type to retrieve the meta table of.
 * @return {object} The corresponding meta table.
 */
declare function FindMetaTable(metaName: string): object;

/**
 * Returns the tool-tip text and tool-tip-panel (if any) of the given panel as well as itself
 *
 * @arg Panel panel - Panel to find tool-tip of
 * @return {string} tool-tip text
 * @return {Panel} tool-tip panel
 * @return {Panel} panel that the function was called with
 */
declare function FindTooltip(panel: Panel): LuaMultiReturn<[string, Panel, Panel]>;

/**
 * Formats the specified values into the string given. Same as string.format.
* 
* @arg string format - The string to be formatted.
Follows this format: http://www.cplusplus.com/reference/cstdio/printf/
* @arg args[] formatParameters - Values to be formatted into the string.
* @return {string} The formatted string
 */
declare function Format(format: string, ...formatParameters: any[]): string;

/**
 * Returns the number of frames rendered since the game was launched.
 */
declare function FrameNumber(): void;

/**
 * Returns the CurTime-based time in seconds it took to render the last frame.
 *
 * @return {number} time (in seconds)
 */
declare function FrameTime(): number;

/**
 * Callback function for when the client has joined a server. This function shows the server's loading URL by default.
 *
 * @arg string servername - Server's name.
 * @arg string serverurl - Server's loading screen URL, or "" if the URL is not set.
 * @arg string mapname - Server's current map's name.
 * @arg number maxplayers - Max player count of server.
 * @arg string steamid - The local player's Player:SteamID64.
 * @arg string gamemode - Server's current gamemode's folder name.
 */
declare function GameDetails(
    servername: string,
    serverurl: string,
    mapname: string,
    maxplayers: number,
    steamid: string,
    gamemode: string
): void;

/**
 * Returns the current floored dynamic memory usage of Lua in kilobytes.
 *
 * @return {number} The current floored dynamic memory usage of Lua, in kilobytes.
 */
declare function gcinfo(): number;

/**
 * Gets miscellaneous information from Facepunches API.
* 
* @arg GMLua.CallbackNoContext callback - Callback to be called when the API request is done.
Callback is called with one argument, a JSON which when converted into a table using util.JSONToTable contains the following:
{
	"ManifestVersion": 	number - Version of the manifest

	"Date": 			string - Date in WDDX format

	// Contains all the blog posts, the things in the top right of the menu
	"News": {
		"Blogs": [

			// Structure of blog posts
			{ 
				"Date": 		string - Date in WDDX format of the post
				"ShortName": 	string - Short name of the post, identifier of it on the blog website
				"Title": 		string - Title of the post
				"HeaderImage": 	string - Main image of the post, showed in the top right
				"SummaryHtml": 	string - Summary of the blogpost, text thats shown
				"Url": 			string - URL to the post on the blog
				"Tags": 		string - String of the posts tag
			}
		]
	}
	
	// Array of Facepunches Mods, Admins and Devs
	"Administrators": [
		{
			"UserId": 		string - SteamID64 of the person
			"Level": 		string - Level of the user (Administrator, Developer or Moderator)
		}
	]

	// Unused and contains nothing useful
	"Heroes": {}

	"SentryUrl": 		string - Nothing
	"DatabaseUrl" 		string - URL to the Facepunch API (/database/{action}/)
	"FeedbackUrl" 		string - URL to the Facepunch API (/feedback/add/)
	"ReportUrl" 		string - URL to the Facepunch API (/feedback/report/)
	"LeaderboardUrl" 	string - URL to the Facepunch API (/leaderboard/{action}/)
	"BenchmarkUrl" 		string - URL to the Facepunch API (/benchmark/add/)
	"AccountUrl" 		string - URL to the Facepunch API (/account/{action}/)

	"Servers": {
		"Official": [] // Nothing
		
		// List of blacklisted servers
		"Banned": [
			string 	- IP of the blacklisted server
		]
	}
}
 */
declare function GetAPIManifest(callback: GMLua.CallbackNoContext): void;

/**
 * Gets the ConVar with the specified name.
 *
 * @arg string name - Name of the ConVar to get
 * @return {ConVar} The ConVar object, or nil if no such ConVar was found.
 */
declare function GetConVar(name: string): ConVar;

/**
 * Gets the ConVar with the specified name. This function doesn't cache the convar.
 *
 * @arg string name - Name of the ConVar to get
 * @return {ConVar} The ConVar object
 */
declare function GetConVar_Internal(name: string): ConVar;

/**
 * Gets the numeric value ConVar with the specified name.
 *
 * @arg string name - Name of the ConVar to get.
 * @return {number} The ConVar's value.
 */
declare function GetConVarNumber(name: string): number;

/**
 * Gets the string value ConVar with the specified name.
 *
 * @arg string name - Name of the ConVar to get.
 * @return {string} The ConVar's value.
 */
declare function GetConVarString(name: string): string;

/**
 * Returns the default loading screen URL (asset://garrysmod/html/loading.html)
 *
 * @return {string} Default loading url (asset://garrysmod/html/loading.html)
 */
declare function GetDefaultLoadingHTML(): string;

/**
 * Retrieves data about the demo with the specified filename. Similar to GetSaveFileDetails.
 *
 * @arg string filename - The file name of the demo.
 * @return {object} Demo data.
 */
declare function GetDemoFileDetails(filename: string): object;

/**
 * Returns a table with the names of files needed from the server you are currently joining.
 *
 * @return {object} table of file names
 */
declare function GetDownloadables(): object;

/**
 * Returns the environment table of either the stack level or the function specified.
 *
 * @arg GMLua.CallbackNoContext [location=1] - The object to get the enviroment from. Can also be a number that specifies the function at that stack level: Level 1 is the function calling getfenv.
 * @return {object} The environment.
 */
declare function getfenv(location?: GMLua.CallbackNoContext): object;

/**
 * Returns an angle that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg Angle [def=Angle( 0, 0, 0 )] - The value to return if the global value is not set.
 * @return {Angle} The global value, or default if the global is not set.
 */
declare function GetGlobalAngle(index: string, def?: Angle): Angle;

/**
 * Returns a boolean that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg boolean [def=false] - The value to return if the global value is not set.
 * @return {boolean} The global value, or the default if the global value is not set.
 */
declare function GetGlobalBool(index: string, def?: boolean): boolean;

/**
 * Returns an entity that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg Entity [def=NULL] - The value to return if the global value is not set.
 * @return {Entity} The global value, or the default if the global value is not set.
 */
declare function GetGlobalEntity(index: string, def?: Entity): Entity;

/**
 * Returns a float that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg number [def=0] - The value to return if the global value is not set.
 * @return {number} The global value, or the default if the global value is not set.
 */
declare function GetGlobalFloat(index: string, def?: number): number;

/**
 * Returns an integer that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg number [def=0] - The value to return if the global value is not set.
 * @return {number} The global value, or the default if the global value is not set.
 */
declare function GetGlobalInt(index: string, def?: number): number;

/**
 * Returns a string that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg string def - The value to return if the global value is not set.
 * @return {string} The global value, or the default if the global value is not set.
 */
declare function GetGlobalString(index: string, def: string): string;

/**
 * Returns a vector that is shared between the server and all clients.
 *
 * @arg string Index - The unique index to identify the global value with.
 * @arg Vector Default - The value to return if the global value is not set.
 * @return {Vector} The global value, or the default if the global value is not set.
 */
declare function GetGlobalVector(Index: string, Default: Vector): Vector;

/**
 * Returns the name of the current server.
 *
 * @return {string} The name of the server.
 */
declare function GetHostName(): string;

/**
 * Returns the panel that is used as a wrapper for the HUD. If you want your panel to be hidden when the main menu is opened, parent it to this. Child panels will also have their controls disabled.
 *
 * @return {Panel} The HUD panel
 */
declare function GetHUDPanel(): Panel;

/**
 * Returns the loading screen panel and creates it if it doesn't exist.
 *
 * @return {Panel} The loading screen panel
 */
declare function GetLoadPanel(): Panel;

/**
 * Returns the current status of the server join progress.
 *
 * @return {string} The current status
 */
declare function GetLoadStatus(): string;

/**
 * Returns a table with the names of all maps and categories that you have on your client.
 *
 * @return {object} table of map names and categories
 */
declare function GetMapList(): object;

/**
 * Returns the metatable of an object. This function obeys the metatable's __metatable field, and will return that field if the metatable has it set.
 *
 * @arg any object - The value to return the metatable of.
 * @return {any} The metatable of the value. This is not always a table.
 */
declare function getmetatable(object: any): any;

/**
 * Returns the menu overlay panel, a container for panels like the error panel created in GM:OnLuaError.
 *
 * @return {Panel} The overlay panel
 */
declare function GetOverlayPanel(): Panel;

/**
 * Returns the player whose movement commands are currently being processed. The player this returns can safely have Player:GetCurrentCommand() called on them. See Prediction.
 *
 * @return {Player} The player currently being predicted, or NULL if no command processing is currently being done.
 */
declare function GetPredictionPlayer(): Player;

/**
 * Creates or gets the rendertarget with the given name.
 *
 * @arg string name - The internal name of the render target.
 * @arg number width - The width of the render target, must be power of 2. If not set to PO2, the size will be automatically converted to the nearest PO2 size.
 * @arg number height - The height of the render target, must be power of 2. If not set to PO2, the size will be automatically converted to the nearest PO2 size.
 * @return {ITexture} The render target
 */
declare function GetRenderTarget(name: string, width: number, height: number): ITexture;

/**
 * Gets (or creates if it does not exist) the rendertarget with the given name, this function allows to adjust the creation of a rendertarget more than GetRenderTarget.
* 
* @arg string name - The internal name of the render target.
The name is treated like a path and gets its extension discarded."name.1" and "name.2" are considered the same name and will result in the same render target being reused.
* @arg number width - The width of the render target, must be power of 2.
* @arg number height - The height of the render target, must be power of 2.
* @arg number sizeMode - Bitflag that influences the sizing of the render target, see Enums/RT_SIZE.
* @arg number depthMode - Bitflag that determines the depth buffer usage of the render target Enums/MATERIAL_RT_DEPTH.
* @arg number textureFlags - Bitflag that configurates the texture, see Enums/TEXTUREFLAGS.
List of flags can also be found on the Valve's Developer Wiki:
https://developer.valvesoftware.com/wiki/Valve_Texture_Format
* @arg number rtFlags - Flags that controll the HDR behaviour of the render target, see Enums/CREATERENDERTARGETFLAGS.
* @arg number imageFormat - Image format, see Enums/IMAGE_FORMAT.
* @return {ITexture} The new render target.
 */
declare function GetRenderTargetEx(
    name: string,
    width: number,
    height: number,
    sizeMode: number,
    depthMode: number,
    textureFlags: number,
    rtFlags: number,
    imageFormat: number
): ITexture;

/**
 * Retrieves data about the save with the specified filename. Similar to GetDemoFileDetails.
 *
 * @arg string filename - The file name of the save.
 * @return {object} Save data.
 */
declare function GetSaveFileDetails(filename: string): object;

/**
 * Returns if the client is timing out, and time since last ping from the server. Similar to the server side Player:IsTimingOut.
 *
 * @return {boolean} Is timing out?
 * @return {number} Get time since last pinged received.
 */
declare function GetTimeoutInfo(): LuaMultiReturn<[boolean, number]>;

/**
 * Returns the entity the client is using to see from (such as the player itself, the camera, or another entity).
 *
 * @return {Entity} The view entity.
 */
declare function GetViewEntity(): Entity;

/**
 * Converts a color from HSL color space into RGB color space and returns a Color.
 *
 * @arg number hue - The hue in degrees from 0-360.
 * @arg number saturation - The saturation from 0-1.
 * @arg number value - The lightness from 0-1.
 * @return {Color} The Color created from the HSL color space.
 */
declare function HSLToColor(hue: number, saturation: number, value: number): Color;

/**
 * Converts a color from HSV color space into RGB color space and returns a Color.
 *
 * @arg number hue - The hue in degrees from 0-360.
 * @arg number saturation - The saturation from 0-1.
 * @arg number value - The value from 0-1.
 * @return {Color} The Color created from the HSV color space.
 */
declare function HSVToColor(hue: number, saturation: number, value: number): Color;

/**
 * Launches an asynchronous http request with the given parameters.
 *
 * @arg HTTPRequestStruct parameters - The request parameters. See Structures/HTTPRequest.
 * @return {boolean} true if we made a request, nil if we failed.
 */
declare function HTTP(parameters: HTTPRequestStruct): boolean;

/**
 * Executes a Lua script.
* 
* @arg string fileName - The name of the script to be executed. The path must be either relative to the current file, or be an absolute path (relative to and excluding the lua/ folder).
Please make sure your file names are unique, the filesystem is shared across all addons, so a file named lua/config.lua in your addon may be overwritten by the same file in another addon.
* @return {args[]} Anything that the executed Lua script returns.
 */
declare function include(fileName: string): any;

/**
 * This function works exactly the same as include both clientside and serverside.
 *
 * @arg string filename - The filename of the Lua file you want to include.
 */
declare function IncludeCS(filename: string): void;

/**
 * Returns an iterator function for a for loop, to return ordered key-value pairs from a table.
 *
 * @arg object tab - The table to iterate over.
 * @return {Function} The iterator function.
 * @return {object} The table being iterated over.
 * @return {number} The origin index =0.
 */
declare function ipairs(tab: object): LuaMultiReturn<[Function, object, number]>;

/**
 * Returns if the passed object is an Angle.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is an Angle.
 */
declare function isangle(variable: any): boolean;

/**
 * Returns if the passed object is a boolean.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a boolean.
 */
declare function isbool(variable: any): boolean;

/**
 * Returns whether the given object does or doesn't have a metatable of a color.
 *
 * @arg any Object - The object to be tested
 * @return {boolean} Whether the given object is a color or not
 */
declare function IsColor(Object: any): boolean;

/**
 * Determines whether or not the provided console command will be blocked if it's ran through Lua functions, such as RunConsoleCommand or Player:ConCommand.
 *
 * @arg string name - The console command to test.
 * @return {boolean} Whether the command will be blocked.
 */
declare function IsConCommandBlocked(name: string): boolean;

/**
 * Returns if the given NPC class name is an enemy.
 *
 * @arg string className - Class name of the entity to check
 * @return {boolean} Is an enemy
 */
declare function IsEnemyEntityName(className: string): boolean;

/**
 * Returns if the passed object is an Entity. Alias of isentity.
 *
 * @arg any variable - The variable to check.
 * @return {boolean} True if the variable is an Entity.
 */
declare function IsEntity(variable: any): boolean;

/**
 * Returns if this is the first time this hook was predicted.
 *
 * @return {boolean} Whether or not this is the first time being predicted.
 */
declare function IsFirstTimePredicted(): boolean;

/**
 * Returns if the given NPC class name is a friend.
 *
 * @arg string className - Class name of the entity to check
 * @return {boolean} Is a friend
 */
declare function IsFriendEntityName(className: string): boolean;

/**
 * Returns if the passed object is a function.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a function.
 */
declare function isfunction(variable: any): boolean;

/**
 * Returns true if the client is currently playing either a singleplayer or multiplayer game.
 *
 * @return {boolean} True if we are in a game.
 */
declare function IsInGame(): boolean;

/**
 * Returns whether the passed object is a VMatrix.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a VMatrix.
 */
declare function ismatrix(variable: any): boolean;

/**
 * Checks whether or not a game is currently mounted. Uses data given by engine.GetGames.
 *
 * @arg string game - The game string/app ID to check.
 * @return {boolean} True if the game is mounted.
 */
declare function IsMounted(game: string): boolean;

/**
 * Returns if the passed object is a number.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a number.
 */
declare function isnumber(variable: any): boolean;

/**
 * Returns if the passed object is a Panel.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a Panel.
 */
declare function ispanel(variable: any): boolean;

/**
 * Returns if the passed object is a string.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a string.
 */
declare function isstring(variable: any): boolean;

/**
 * Returns if the passed object is a table.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a table.
 */
declare function istable(variable: any): boolean;

/**
 * Returns whether or not every element within a table is a valid entity
 *
 * @arg object table - Table containing entities to check
 * @return {boolean} All entities valid
 */
declare function IsTableOfEntitiesValid(table: object): boolean;

/**
 * Returns whether or not a model is useless by checking that the file path is that of a proper model.
 *
 * @arg string modelName - The model name to be checked
 * @return {boolean} Whether or not the model is useless
 */
declare function IsUselessModel(modelName: string): boolean;

/**
 * Returns whether an object is valid or not. (Such as Entitys, Panels, custom table objects and more).
 * Checks that an object is not nil, has an IsValid method and if this method returns true.
 *
 * @arg any toBeValidated - The table or object to be validated.
 * @return {boolean} True if the object is valid.
 */
declare function IsValid(toBeValidated: any): boolean;

/**
 * Returns if the passed object is a Vector.
 *
 * @arg any variable - The variable to perform the type check for.
 * @return {boolean} True if the variable is a Vector.
 */
declare function isvector(variable: any): boolean;

/**
 * Joins the server with the specified IP.
 *
 * @arg string IP - The IP of the server to join
 */
declare function JoinServer(IP: string): void;

/**
 * Adds javascript function 'language.Update' to an HTML panel as a method to call Lua's language.GetPhrase function.
 *
 * @arg Panel htmlPanel - Panel to add javascript function 'language.Update' to.
 */
declare function JS_Language(htmlPanel: Panel): void;

/**
 * Adds javascript function 'util.MotionSensorAvailable' to an HTML panel as a method to call Lua's motionsensor.IsAvailable function.
 *
 * @arg Panel htmlPanel - Panel to add javascript function 'util.MotionSensorAvailable' to.
 */
declare function JS_Utility(htmlPanel: Panel): void;

/**
 * Adds workshop related javascript functions to an HTML panel, used by the "Dupes" and "Saves" tabs in the spawnmenu.
 *
 * @arg Panel htmlPanel - Panel to add javascript functions to.
 */
declare function JS_Workshop(htmlPanel: Panel): void;

/**
 * Convenience function that creates a DLabel, sets the text, and returns it
 *
 * @arg string text - The string to set the label's text to
 * @arg Panel [parent=nil] - Optional. The panel to parent the DLabel to
 * @return {Panel} The created DLabel
 */
declare function Label(text: string, parent?: Panel): Panel;

/**
 * Callback function for when the client's language changes. Called by the engine.
 *
 * @arg string lang - The new language code.
 */
declare function LanguageChanged(lang: string): void;

/**
 * Performs a linear interpolation from the start number to the end number.
 *
 * @arg number t - The fraction for finding the result. This number is clamped between 0 and 1. Shouldn't be a constant.
 * @arg number from - The starting number. The result will be equal to this if delta is 0.
 * @arg number to - The ending number. The result will be equal to this if delta is 1.
 * @return {number} The result of the linear interpolation, from + (to - from) * t.
 */
declare function Lerp(t: number, from: number, to: number): number;

/**
 * Returns point between first and second angle using given fraction and linear interpolation
 *
 * @arg number ratio - Ratio of progress through values
 * @arg Angle angleStart - Angle to begin from
 * @arg Angle angleEnd - Angle to end at
 * @return {Angle} angle
 */
declare function LerpAngle(ratio: number, angleStart: Angle, angleEnd: Angle): Angle;

/**
 * Linear interpolation between two vectors. It is commonly used to smooth movement between two vectors
 *
 * @arg number fraction - Fraction ranging from 0 to 1
 * @arg Vector from - The initial Vector
 * @arg Vector to - The desired Vector
 * @return {Vector} The lerped vector.
 */
declare function LerpVector(fraction: number, from: Vector, to: Vector): Vector;

/**
 * Returns the contents of addonpresets.txt located in the garrysmod/settings folder. By default, this file stores your addon presets as JSON.
 *
 * @return {string} The contents of the file.
 */
declare function LoadAddonPresets(): string;

/**
 * This function is used to get the last map and category to which the map belongs from the cookie saved with SaveLastMap.
 */
declare function LoadLastMap(): void;

/**
 * Loads all preset settings for the presets and returns them in a table
 *
 * @return {object} Preset data
 */
declare function LoadPresets(): object;

/**
 * Returns a localisation for the given token, if none is found it will return the default (second) parameter.
 *
 * @arg string localisationToken - The token to find a translation for.
 * @arg string def - The default value to be returned if no translation was found.
 */
declare function Localize(localisationToken: string, def: string): void;

/**
 * Returns the player object of the current client.
 *
 * @return {Player} The player object representing the client.
 */
declare function LocalPlayer(): Player;

/**
 * Translates the specified position and angle from the specified local coordinate system into worldspace coordinates.
 *
 * @arg Vector localPos - The position vector in the source coordinate system, that should be translated to world coordinates
 * @arg Angle localAng - The angle in the source coordinate system, that should be converted to a world angle. If you don't need to convert an angle, you can supply an arbitrary valid angle (e.g. Angle()).
 * @arg Vector originPos - The origin point of the source coordinate system, in world coordinates
 * @arg Angle originAngle - The angles of the source coordinate system, as a world angle
 * @return {Vector} The world position of the supplied local position.
 * @return {Angle} The world angles of the supplied local angle.
 */
declare function LocalToWorld(
    localPos: Vector,
    localAng: Angle,
    originPos: Vector,
    originAngle: Angle
): LuaMultiReturn<[Vector, Angle]>;

/**
 * Either returns the material with the given name, or loads the material interpreting the first argument as the path.
* 
* @arg string materialName - The material name or path. The path is relative to the materials/ folder. You do not need to add materials/ to your path.
To retrieve a Lua material created with CreateMaterial, just prepend a "!" to the material name.
Since paths are relative to the materials folder, resource paths like ../data/MyImage.jpg will work since ".." translates to moving up a parent directory in the file tree.
.
* @arg string [pngParameters="nil"] - A string containing space separated keywords which will be used to add material parameters.
See Material Parameters for more information.
This feature only works when importing .png or .jpeg image files.
* @return {IMaterial} Generated material.
* @return {number} How long it took for the function to run.
 */
declare function Material(materialName: string, pngParameters?: string): LuaMultiReturn<[IMaterial, number]>;

/**
 * Returns a VMatrix object.
* 
* @arg VMatrix [data={{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}] - Initial data to initialize the matrix with. Leave empty to initialize an identity matrix. See examples for usage.
Can be a VMatrix to copy its data.
* @return {VMatrix} New matrix.
 */
declare function Matrix(data?: VMatrix): VMatrix;

/**
 * Returns a new mesh object.
 *
 * @arg IMaterial [mat=nil] - The material the mesh is intended to be rendered with. It's merely a hint that tells that mesh what vertex format it should use.
 * @return {IMesh} The created object.
 */
declare function Mesh(mat?: IMaterial): IMesh;

/**
 * Runs util.PrecacheModel and returns the string.
 *
 * @arg string model - The model to precache.
 * @return {string} The same string entered as an argument.
 */
declare function Model(model: string): string;

/**
 * Creates a table with the specified module name and sets the function environment for said table.
 *
 * @arg string name - The name of the module. This will be used to access the module table in the runtime environment.
 * @arg args[] loaders - Calls each function passed with the new table as an argument.
 */
declare function module(name: string, ...loaders: any[]): void;

/**
 * Writes every given argument to the console.
 *
 * @arg args[] args - List of values to print.
 */
declare function Msg(...args: any[]): void;

/**
 * Works exactly like Msg except that, if called on the server, will print to all players consoles plus the server console.
 *
 * @arg args[] args - List of values to print.
 */
declare function MsgAll(...args: any[]): void;

/**
 * Just like Msg, except it can also print colored text, just like chat.AddText.
 *
 * @arg args[] args - Values to print. If you put in a color, all text after that color will be printed in that color.
 */
declare function MsgC(...args: any[]): void;

/**
 * Same as print, except it concatinates the arguments without inserting any whitespace in between them.
 *
 * @arg args[] args - List of values to print. They can be of any type and will be converted to strings with tostring.
 */
declare function MsgN(...args: any[]): void;

/**
 * Returns named color defined in resource/ClientScheme.res.
 *
 * @arg string name - Name of color
 * @return {Color} A Color or nil
 */
declare function NamedColor(name: string): Color;

/**
 * Returns a new userdata object.
 *
 * @arg boolean [addMetatable=false] - If true, the userdata will get its own metatable automatically. If another newproxy is passed, it will create new one and copy its metatable.
 * @return {any} The newly created userdata.
 */
declare function newproxy(addMetatable?: boolean): any;

/**
 * Returns the next key and value pair in a table.
 *
 * @arg object tab - The table
 * @arg any [prevKey=nil] - The previous key in the table.
 * @return {any} The next key for the table. If the previous key was nil, this will be the first key in the table. If the previous key was the last key in the table, this will be nil.
 * @return {any} The value associated with that key. If the previous key was the last key in the table, this will be nil.
 */
declare function next(tab: object, prevKey?: any): LuaMultiReturn<[any, any]>;

/**
 * Returns the number of files needed from the server you are currently joining.
 *
 * @return {number} The number of downloadables
 */
declare function NumDownloadables(): number;

/**
 * Returns the amount of skins the specified model has.
 *
 * @arg string modelName - Model to return amount of skins of
 * @return {number} Amount of skins
 */
declare function NumModelSkins(modelName: string): number;

/**
 * Called by the engine when a model has been loaded. Caches model information with the sql.
 *
 * @arg string modelName - Name of the model.
 * @arg number numPostParams - Number of pose parameters the model has.
 * @arg number numSeq - Number of sequences the model has.
 * @arg number numAttachments - Number of attachments the model has.
 * @arg number numBoneControllers - Number of bone controllers the model has.
 * @arg number numSkins - Number of skins that the model has.
 * @arg number size - Size of the model.
 */
declare function OnModelLoaded(
    modelName: string,
    numPostParams: number,
    numSeq: number,
    numAttachments: number,
    numBoneControllers: number,
    numSkins: number,
    size: number
): void;

/**
 * Opens a folder with the given name in the garrysmod folder using the operating system's file browser.
 *
 * @arg string folder - The subdirectory to open in the garrysmod folder.
 */
declare function OpenFolder(folder: string): void;

/**
 * Modifies the given vectors so that all of vector2's axis are larger than vector1's by switching them around. Also known as ordering vectors.
 *
 * @arg Vector vector1 - Bounding box min resultant
 * @arg Vector vector2 - Bounding box max resultant
 */
declare function OrderVectors(vector1: Vector, vector2: Vector): void;

/**
 * Returns an iterator function(next) for a for loop that will return the values of the specified table in an arbitrary order.
 *
 * @arg object tab - The table to iterate over.
 * @return {Function} The iterator (next).
 * @return {object} The table being iterated over.
 * @return {any} nil (for the constructor).
 */
declare function pairs(tab: object): LuaMultiReturn<[Function, object, any]>;

/**
 * Calls game.AddParticles and returns given string.
 *
 * @arg string file - The particle file.
 * @return {string} The particle file.
 */
declare function Particle(file: string): string;

/**
 * Creates a particle effect.
 *
 * @arg string particleName - The name of the particle effect.
 * @arg Vector position - The start position of the effect.
 * @arg Angle angles - The orientation of the effect.
 * @arg Entity [parent=NULL] - If set, the particle will be parented to the entity.
 */
declare function ParticleEffect(particleName: string, position: Vector, angles: Angle, parent?: Entity): void;

/**
 * Creates a particle effect with specialized parameters.
 *
 * @arg string particleName - The name of the particle effect.
 * @arg number attachType - Attachment type using Enums/PATTACH.
 * @arg Entity entity - The entity to be used in the way specified by the attachType.
 * @arg number attachmentID - The id of the attachment to be used in the way specified by the attachType.
 */
declare function ParticleEffectAttach(
    particleName: string,
    attachType: number,
    entity: Entity,
    attachmentID: number
): void;

/**
 * Creates a new CLuaEmitter.
* 
* @arg Vector position - The start position of the emitter.
This is only used to determine particle drawing order for translucent particles.
* @arg boolean use3D - Whenever to render the particles in 2D or 3D mode.
* @return {CLuaEmitter} The new particle emitter.
 */
declare function ParticleEmitter(position: Vector, use3D: boolean): CLuaEmitter;

/**
 * Creates a path for the bot to follow
* 
* @arg string type - The name of the path to create.
This is going to be "Follow" or "Chase" right now.
* @return {PathFollower} The path
 */
declare function Path(type: string): PathFollower;

/**
 * Calls a function and catches an error that can be thrown while the execution of the call.
 *
 * @arg GMLua.CallbackNoContext func - Function to be executed and of which the errors should be caught of
 * @arg args[] arguments - Arguments to call the function with.
 * @return {boolean} If the function had no errors occur within it.
 * @return {args[]} If an error occurred, this will be a string containing the error message. Otherwise, this will be the return values of the function passed in.
 */
declare function pcall(func: GMLua.CallbackNoContext, ...arguments: any[]): LuaMultiReturn<[boolean, ...any[]]>;

/**
 * Returns the player with the matching Player:UserID.
 *
 * @arg number playerIndex - The player index.
 * @return {Player} The retrieved player.
 */
declare function Player(playerIndex: number): Player;

/**
 * Moves the given model to the given position and calculates appropriate camera parameters for rendering the model to an icon.
 *
 * @arg Entity model - Model that is being rendered to the spawn icon
 * @arg Vector position - Position that the model is being rendered at
 * @arg boolean noAngles - If true the function won't reset the angles to 0 for the model.
 * @return {object} Table of information of the view which can be used for rendering
 */
declare function PositionSpawnIcon(model: Entity, position: Vector, noAngles: boolean): object;

/**
 * Precaches the particle with the specified name.
 *
 * @arg string particleSystemName - The name of the particle system.
 */
declare function PrecacheParticleSystem(particleSystemName: string): void;

/**
 * Precaches a scene file.
 *
 * @arg string scene - Path to the scene file to precache.
 */
declare function PrecacheScene(scene: string): void;

/**
 * Load and precache a custom sentence file.
 *
 * @arg string filename - The path to the custom sentences.txt.
 */
declare function PrecacheSentenceFile(filename: string): void;

/**
 * Precache a sentence group in a sentences.txt definition file.
 *
 * @arg string group - The group to precache.
 */
declare function PrecacheSentenceGroup(group: string): void;

/**
 * Writes every given argument to the console.
 * Automatically attempts to convert each argument to a string. (See tostring)
 *
 * @arg args[] args - List of values to print.
 */
declare function print(...args: any[]): void;

/**
 * Displays a message in the chat, console, or center of screen of every player.
 *
 * @arg number type - Which type of message should be sent to the players (see Enums/HUD)
 * @arg string message - Message to be sent to the players
 */
declare function PrintMessage(type: number, message: string): void;

/**
 * Recursively prints the contents of a table to the console.
 *
 * @arg object tableToPrint - The table to be printed
 * @arg number [indent=0] - Number of tabs to start indenting at. Increases by 2 when entering another table.
 * @arg object [done={}] - Internal argument, you shouldn't normally change this. Used to check if a nested table has already been printed so it doesn't get caught in a loop.
 */
declare function PrintTable(tableToPrint: object, indent?: number, done?: object): void;

/**
 * Creates a new ProjectedTexture.
 *
 * @return {ProjectedTexture} Newly created projected texture.
 */
declare function ProjectedTexture(): ProjectedTexture;

/**
 * Runs a function without stopping the whole script on error.
 *
 * @arg GMLua.CallbackNoContext func - Function to run
 * @return {boolean} Whether the function executed successfully or not
 */
declare function ProtectedCall(func: GMLua.CallbackNoContext): boolean;

/**
 * Returns an iterator function that can be used to loop through a table in random order
 *
 * @arg object table - Table to create iterator for
 * @arg boolean descending - Whether the iterator should iterate descending or not
 * @return {Function} Iterator function
 */
declare function RandomPairs(table: object, descending: boolean): Function;

/**
 * Compares the two values without calling their __eq operator.
 *
 * @arg any value1 - The first value to compare.
 * @arg any value2 - The second value to compare.
 * @return {boolean} Whether or not the two values are equal.
 */
declare function rawequal(value1: any, value2: any): boolean;

/**
 * Gets the value with the specified key from the table without calling the __index method.
 *
 * @arg object table - Table to get the value from.
 * @arg any index - The index to get the value from.
 * @return {any} The value.
 */
declare function rawget(table: object, index: any): any;

/**
 * Sets the value with the specified key from the table without calling the __newindex method.
 *
 * @arg object table - Table to get the value from.
 * @arg any index - The index to get the value from.
 * @arg any value - The value to set for the specified key.
 */
declare function rawset(table: object, index: any, value: any): void;

/**
 * Returns the real frame-time which is unaffected by host_timescale. To be used for GUI effects (for example)
 *
 * @return {number} Real frame time
 */
declare function RealFrameTime(): number;

/**
 * Returns the uptime of the game/server in seconds (to at least 4 decimal places). This value updates itself once every time the realm thinks. For servers, this is the server tickrate. For clients, its their current FPS.
 *
 * @return {number} Uptime of the game/server.
 */
declare function RealTime(): number;

/**
 * Creates a new CRecipientFilter.
* 
* @arg boolean [unreliable=false] - If set to true, makes the filter unreliable.
This means, when sending over the network in cases like CreateSound (and its subsequent updates), the message is not guaranteed to reach all clients.
* @return {CRecipientFilter} The new created recipient filter.
 */
declare function RecipientFilter(unreliable?: boolean): CRecipientFilter;

/**
 * Adds a frame to the currently recording demo.
 */
declare function RecordDemoFrame(): void;

/**
 * Registers a Derma element to be closed the next time CloseDermaMenus is called
 *
 * @arg Panel menu - Menu to be registered for closure
 */
declare function RegisterDermaMenuForClose(menu: Panel): void;

/**
 * Saves position of your cursor on screen. You can restore it by using
 * RestoreCursorPosition.
 */
declare function RememberCursorPosition(): void;

/**
 * Does the removing of the tooltip panel. Called by EndTooltip.
 */
declare function RemoveTooltip(): void;

/**
 * Returns the angle that the clients view is being rendered at
 *
 * @return {Angle} Render Angles
 */
declare function RenderAngles(): Angle;

/**
 * Renders a Depth of Field effect
 *
 * @arg Vector origin - Origin to render the effect at
 * @arg Angle angle - Angle to render the effect at
 * @arg Vector usableFocusPoint - Point to focus the effect at
 * @arg number angleSize - Angle size of the effect
 * @arg number radialSteps - Amount of radial steps to render the effect with
 * @arg number passes - Amount of render passes
 * @arg boolean spin - Whether to cycle the frame or not
 * @arg object inView - Table of view data
 * @arg number fov - FOV to render the effect with
 */
declare function RenderDoF(
    origin: Vector,
    angle: Angle,
    usableFocusPoint: Vector,
    angleSize: number,
    radialSteps: number,
    passes: number,
    spin: boolean,
    inView: object,
    fov: number
): void;

/**
 * Renders the stereoscopic post-process effect
 *
 * @arg Vector viewOrigin - Origin to render the effect at
 * @arg Angle viewAngles - Angles to render the effect at
 */
declare function RenderStereoscopy(viewOrigin: Vector, viewAngles: Angle): void;

/**
 * Renders the Super Depth of Field post-process effect
 *
 * @arg Vector viewOrigin - Origin to render the effect at
 * @arg Angle viewAngles - Angles to render the effect at
 * @arg number viewFOV - Field of View to render the effect at
 */
declare function RenderSuperDoF(viewOrigin: Vector, viewAngles: Angle, viewFOV: number): void;

/**
 * First tries to load a binary module with the given name, if unsuccessful, it tries to load a Lua module with the given name.
 *
 * @arg string name - The name of the module to be loaded.
 */
declare function require(name: string): void;

/**
 * Restores position of your cursor on screen. You can save it by using RememberCursorPosition.
 */
declare function RestoreCursorPosition(): void;

/**
 * Executes the given console command with the parameters.
 *
 * @arg string command - The command to be executed.
 * @arg args[] arguments - The arguments. Note, that unlike Player:ConCommand, you must pass each argument as a new string, not separating them with a space.
 */
declare function RunConsoleCommand(command: string, ...arguments: any[]): void;

/**
 * Runs a menu command. Equivalent to RunConsoleCommand( "gamemenucommand", command ) unless the command starts with the "engine" keyword in which case it is equivalent to RunConsoleCommand( command ).
* 
* @arg string command - The menu command to run
Should be one of the following:

Disconnect - Disconnects from the current server.
OpenBenchmarkDialog - Opens the "Video Hardware Stress Test" dialog.
OpenChangeGameDialog - Does not work in GMod.
OpenCreateMultiplayerGameDialog - Opens the Source dialog for creating a listen server.
OpenCustomMapsDialog - Does nothing.
OpenFriendsDialog - Does nothing.
OpenGameMenu - Does not work in GMod.
OpenLoadCommentaryDialog - Opens the "Developer Commentary" selection dialog. Useless in GMod.
OpenLoadDemoDialog - Does nothing.
OpenLoadGameDialog - Opens the Source "Load Game" dialog.
OpenNewGameDialog - Opens the "New Game" dialog. Useless in GMod.
OpenOptionsDialog - Opens the options dialog.
OpenPlayerListDialog - Opens the "Mute Players" dialog that shows all players connected to the server and allows to mute them.
OpenSaveGameDialog - Opens the Source "Save Game" dialog.
OpenServerBrowser - Opens the legacy server browser.
Quit - Quits the game without confirmation (unlike other Source games).
QuitNoConfirm - Quits the game without confirmation (like other Source games).
ResumeGame - Closes the menu and returns to the game.
engine <concommand> - Runs a console command. Equivalent to RunConsoleCommand( <concommand> ).
 */
declare function RunGameUICommand(command: string): void;

/**
 * Evaluates and executes the given code, will throw an error on failure.
 *
 * @arg string code - The code to execute.
 * @arg string [identifier="RunString"] - The name that should appear in any error messages caused by this code.
 * @arg boolean [handleError=true] - If false, this function will return a string containing any error messages instead of throwing an error.
 * @return {string} If handleError is false, the error message (if any).
 */
declare function RunString(code: string, identifier?: string, handleError?: boolean): string;

/**
 * Alias of RunString.
 */
declare function RunStringEx(): void;

/**
 * Removes the given entity unless it is a player or the world entity
 *
 * @arg Entity ent - Entity to safely remove.
 */
declare function SafeRemoveEntity(ent: Entity): void;

/**
 * Removes entity after delay using SafeRemoveEntity
 *
 * @arg Entity entity - Entity to be removed
 * @arg number delay - Delay for entity removal in seconds
 */
declare function SafeRemoveEntityDelayed(entity: Entity, delay: number): void;

/**
 * Sets the content of addonpresets.txt located in the garrysmod/settings folder. By default, this file stores your addon presets as JSON.
 *
 * @arg string JSON - The new contents of the file.
 */
declare function SaveAddonPresets(JSON: string): void;

/**
 * This function is used to save the last map and category to which the map belongs as a .
 *
 * @arg string map - The name of the map.
 * @arg string category - The name of the category to which this map belongs.
 */
declare function SaveLastMap(map: string, category: string): void;

/**
 * Overwrites all presets with the supplied table. Used by the presets for preset saving
 *
 * @arg object presets - Presets to be saved
 */
declare function SavePresets(presets: object): void;

/**
 * Returns a number based on the Size argument and your screen's width. The screen's width is always equal to size 640. This function is primarily used for scaling font sizes.
 *
 * @arg number Size - The number you want to scale.
 * @return {number} The scaled number based on your screen's width
 */
declare function ScreenScale(Size: number): number;

/**
 * Gets the height of the game's window (in pixels).
 *
 * @return {number} The height of the game's window in pixels
 */
declare function ScrH(): number;

/**
 * Gets the width of the game's window (in pixels).
 *
 * @return {number} The width of the game's window in pixels
 */
declare function ScrW(): number;

/**
 * Used to select single values from a vararg or get the count of values in it.
* 
* @arg any parameter - Can be a number or string.

If it's a string and starts with "#", the function will return the amount of values in the vararg (ignoring the rest of the string).
If it's a positive number, the function will return all values starting from the given index.
If the number is negative, it will return the amount specified from the end instead of the beginning. This mode will not be compiled by LuaJIT.
* @arg args[] vararg - The vararg. These are the values from which you want to select.
* @return {any} Returns a number or vararg, depending on the select method.
 */
declare function select(parameter: any, ...vararg: any[]): any;

/**
 * Send a usermessage
 *
 * @arg string name - The name of the usermessage
 * @arg any recipients - Can be a CRecipientFilter, table or Player object.
 * @arg args[] args - Data to send in the usermessage
 */
declare function SendUserMessage(name: string, recipients: any, ...args: any[]): void;

/**
 * Returns approximate duration of a sentence by name. See EmitSentence.
 *
 * @arg string name - The sentence name.
 * @return {number} The approximate duration.
 */
declare function SentenceDuration(name: string): number;

/**
 * Prints "ServerLog: PARAM" without a newline, to the server log and console.
 *
 * @arg string parameter - The value to be printed to console.
 */
declare function ServerLog(parameter: string): void;

/**
 * Adds the given string to the computers clipboard, which can then be pasted in or outside of GMod with Ctrl + V.
 *
 * @arg string text - The text to add to the clipboard.
 */
declare function SetClipboardText(text: string): void;

/**
 * Sets the enviroment for a function or a stack level.
 *
 * @arg GMLua.CallbackNoContext location - The function to set the enviroment for or a number representing stack level.
 * @arg object enviroment - Table to be used as enviroment.
 * @return {Function} The function passed, otherwise nil.
 */
declare function setfenv(location: GMLua.CallbackNoContext, enviroment: object): Function;

/**
 * Defines an angle to be automatically networked to clients
 *
 * @arg any index - Index to identify the global angle with
 * @arg Angle angle - Angle to be networked
 */
declare function SetGlobalAngle(index: any, angle: Angle): void;

/**
 * Defined a boolean to be automatically networked to clients
 *
 * @arg any index - Index to identify the global boolean with
 * @arg boolean bool - Boolean to be networked
 */
declare function SetGlobalBool(index: any, bool: boolean): void;

/**
 * Defines an entity to be automatically networked to clients
 *
 * @arg any index - Index to identify the global entity with
 * @arg Entity ent - Entity to be networked
 */
declare function SetGlobalEntity(index: any, ent: Entity): void;

/**
 * Defines a floating point number to be automatically networked to clients
 *
 * @arg any index - Index to identify the global float with
 * @arg number float - Float to be networked
 */
declare function SetGlobalFloat(index: any, float: number): void;

/**
 * Sets an integer that is shared between the server and all clients.
 *
 * @arg string index - The unique index to identify the global value with.
 * @arg number value - The value to set the global value to
 */
declare function SetGlobalInt(index: string, value: number): void;

/**
 * Defines a string with a maximum of 199 characters to be automatically networked to clients
 *
 * @arg any index - Index to identify the global string with
 * @arg string string - String to be networked
 */
declare function SetGlobalString(index: any, string: string): void;

/**
 * Defines a vector to be automatically networked to clients
 *
 * @arg any index - Index to identify the global vector with
 * @arg Vector vec - Vector to be networked
 */
declare function SetGlobalVector(index: any, vec: Vector): void;

/**
 * Sets, changes or removes a table's metatable. Returns Tab (the first argument).
 *
 * @arg object Tab - The table who's metatable to change.
 * @arg object Metatable - The metatable to assign.  If it's nil, the metatable will be removed.
 * @return {object} The first argument.
 */
declare function setmetatable(Tab: object, Metatable: object): object;

/**
 * Called by the engine to set which constraint system the next created constraints should use.
 *
 * @arg Entity constraintSystem - Constraint system to use
 */
declare function SetPhysConstraintSystem(constraintSystem: Entity): void;

/**
 * This function can be used in a for loop instead of pairs. It sorts all keys alphabetically.
 *
 * @arg object table - The table to sort
 * @arg boolean [desc=false] - Reverse the sorting order
 * @return {Function} Iterator function
 * @return {object} The table being iterated over
 */
declare function SortedPairs(table: object, desc?: boolean): LuaMultiReturn<[Function, object]>;

/**
 * Returns an iterator function that can be used to loop through a table in order of member values, when the values of the table are also tables and contain that member.
 *
 * @arg object table - Table to create iterator for.
 * @arg any memberKey - Key of the value member to sort by.
 * @arg boolean [descending=false] - Whether the iterator should iterate in descending order or not.
 * @return {Function} Iterator function
 * @return {object} The table the iterator was created for.
 */
declare function SortedPairsByMemberValue(
    table: object,
    memberKey: any,
    descending?: boolean
): LuaMultiReturn<[Function, object]>;

/**
 * Returns an iterator function that can be used to loop through a table in order of its values.
 *
 * @arg object table - Table to create iterator for
 * @arg boolean [descending=false] - Whether the iterator should iterate in descending order or not
 * @return {Function} Iterator function
 * @return {object} The table which will be iterated over
 */
declare function SortedPairsByValue(table: object, descending?: boolean): LuaMultiReturn<[Function, object]>;

/**
 * Runs util.PrecacheSound and returns the string.
 *
 * @arg string soundPath - The soundpath to precache.
 * @return {string} The string passed as the first argument.
 */
declare function Sound(soundPath: string): string;

/**
 * Returns the duration of the specified sound in seconds.
 *
 * @arg string soundName - The sound file path.
 * @return {number} Sound duration in seconds.
 */
declare function SoundDuration(soundName: string): number;

/**
 * Returns the input value in an escaped form so that it can safely be used inside of queries. The returned value is surrounded by quotes unless noQuotes is true. Alias of sql.SQLStr
 *
 * @arg string input - String to be escaped
 * @arg boolean [noQuotes=false] - Whether the returned value should be surrounded in quotes or not
 * @return {string} Escaped input
 */
declare function SQLStr(input: string, noQuotes?: boolean): string;

/**
 * Returns a number based on the Size argument and your screen's width. Alias of ScreenScale.
 *
 * @arg number Size - The number you want to scale.
 */
declare function SScale(Size: number): void;

/**
 * Returns the ordinal suffix of a given number.
 *
 * @arg number number - The number to find the ordinal suffix of.
 * @return {string} suffix
 */
declare function STNDRD(number: number): string;

/**
 * Suppress any networking from the server to the specified player. This is automatically called by the engine before/after a player fires their weapon, reloads, or causes any other similar shared-predicted event to occur.
 *
 * @arg Player suppressPlayer - The player to suppress any networking to.
 */
declare function SuppressHostEvents(suppressPlayer: Player): void;

/**
 * Returns a highly accurate time in seconds since the start up, ideal for benchmarking. Unlike RealTime, this value will be updated any time the function is called, allowing for sub-think precision.
 *
 * @return {number} Uptime of the server.
 */
declare function SysTime(): number;

/**
 * Returns a TauntCamera object
 *
 * @return {object} TauntCamera
 */
declare function TauntCamera(): object;

/**
 * Clears focus from any text entries player may have focused.
 */
declare function TextEntryLoseFocus(): void;

/**
 * Returns a cosine value that fluctuates based on the current time
 *
 * @arg number frequency - The frequency of fluctuation
 * @arg number min - Minimum value
 * @arg number max - Maximum value
 * @arg number offset - Offset variable that doesn't affect the rate of change, but causes the returned value to be offset by time
 * @return {number} Cosine value
 */
declare function TimedCos(frequency: number, min: number, max: number, offset: number): number;

/**
 * Returns a sine value that fluctuates based on CurTime. The value returned will be between the start value plus/minus the range value.
 *
 * @arg number frequency - The frequency of fluctuation, in
 * @arg number origin - The center value of the sine wave.
 * @arg number max - This argument's distance from origin defines the size of the full range of the sine wave. For example, if origin is 3 and max is 5, then the full range of the sine wave is 5-3 = 2. 3 is the center point of the sine wave, so the sine wave will range between 2 and 4.
 * @arg number offset - Offset variable that doesn't affect the rate of change, but causes the returned value to be offset by time
 * @return {number} Sine value
 */
declare function TimedSin(frequency: number, origin: number, max: number, offset: number): number;

/**
 * Attempts to return an appropriate boolean for the given value
* 
* @arg any val - The object to be converted to a boolean
* @return {boolean} false for the boolean false.
false for "false".
false for "0".
false for numeric 0.
false for nil.
true otherwise.
 */
declare function tobool(val: any): boolean;

/**
 * Toggles whether or not the named map is favorited in the new game list.
 *
 * @arg string map - Map to toggle favorite.
 */
declare function ToggleFavourite(map: string): void;

/**
 * Attempts to convert the value to a number.
 *
 * @arg any value - The value to convert. Can be a number or string.
 * @arg number [base=10] - The  used in the string. Can be any integer between 2 and 36, inclusive.
 * @return {number} The numeric representation of the value with the given base, or nil if the conversion failed.
 */
declare function tonumber(value: any, base?: number): number;

/**
 * Attempts to convert the value to a string. If the value is an object and its metatable has defined the __tostring metamethod, this will call that function.
 *
 * @arg any value - The object to be converted to a string.
 * @return {string} The string representation of the value.
 */
declare function tostring(value: any): string;

/**
 * Returns "Lua Cache File" if the given file name is in a certain string table, nothing otherwise.
 *
 * @arg string filename - File name to test
 * @return {string} "Lua Cache File" if the given file name is in a certain string table, nothing otherwise.
 */
declare function TranslateDownloadableName(filename: string): string;

/**
 * Returns a string representing the name of the type of the passed object.
 *
 * @arg any variable - The object to get the type of.
 * @return {string} The name of the object's type.
 */
declare function type(variable: any): string;

/**
 * Gets the associated type ID of the variable. Unlike type, this does not work with no value - an argument must be provided.
 *
 * @arg any variable - The variable to get the type ID of.
 * @return {number} The type ID of the variable. See the Enums/TYPE.
 */
declare function TypeID(variable: any): number;

/**
 * This function takes a numeric indexed table and return all the members as a vararg. If specified, it will start at the given index and end at end index.
 *
 * @arg object tbl - The table to generate the vararg from.
 * @arg number [startIndex=1] - Which index to start from. Optional.
 * @arg number [endIndex=NaN] - Which index to end at. Optional, even if you set StartIndex.
 * @return {args[]} Output values
 */
declare function unpack(tbl: object, startIndex?: number, endIndex?: number): any;

/**
 * Returns the current asynchronous in-game time.
 *
 * @return {number} The asynchronous in-game time.
 */
declare function UnPredictedCurTime(): number;

/**
 * Runs JavaScript on the loading screen panel (GetLoadPanel).
 *
 * @arg string javascript - JavaScript to run on the loading panel.
 */
declare function UpdateLoadPanel(javascript: string): void;

/**
 * Returns whether or not a model is useless by checking that the file path is that of a proper model.
 *
 * @arg string modelName - The model name to be checked
 * @return {boolean} Whether or not the model is useless
 */
declare function UTIL_IsUselessModel(modelName: string): boolean;

/**
 * Returns if a panel is safe to use.
 *
 * @arg Panel panel - The panel to validate.
 */
declare function ValidPanel(panel: Panel): void;

/**
 * Creates a Vector object.
* 
* @arg number [x=0] - The x component of the vector.
If this is a Vector, this function will return a copy of the given vector.
If this is a string, this function will try to parse the string as a vector. If it fails, it returns a 0 vector.
(See examples)
* @arg number [y=0] - The y component of the vector.
* @arg number [z=0] - The z component of the vector.
* @return {Vector} The created vector object.
 */
declare function Vector(x?: number, y?: number, z?: number): Vector;

/**
 * Returns a random vector whose components are each between min(inclusive), max(exclusive).
 *
 * @arg number [min=-1] - Min bound inclusive.
 * @arg number [max=1] - Max bound exclusive.
 * @return {Vector} The random direction vector.
 */
declare function VectorRand(min?: number, max?: number): Vector;

/**
 * Returns the time in seconds it took to render the VGUI.
 */
declare function VGUIFrameTime(): void;

/**
 * Creates and returns a DShape rectangle GUI element with the given dimensions.
 *
 * @arg number x - X position of the created element
 * @arg number y - Y position of the created element
 * @arg number w - Width of the created element
 * @arg number h - Height of the created element
 * @return {Panel} DShape element
 */
declare function VGUIRect(x: number, y: number, w: number, h: number): Panel;

/**
 * Briefly displays layout details of the given panel on-screen
 *
 * @arg Panel panel - Panel to display layout details of
 */
declare function VisualizeLayout(panel: Panel): void;

/**
 * Returns a new WorkshopFileBase element
 *
 * @arg string namespace - Namespace for the file base
 * @arg object requiredTags - Tags required for a Workshop submission to be interacted with by the filebase
 * @return {object} WorkshopFileBase element
 */
declare function WorkshopFileBase(namespace: string, requiredTags: object): object;

/**
 * Translates the specified position and angle into the specified coordinate system.
 *
 * @arg Vector position - The position that should be translated from the current to the new system.
 * @arg Angle angle - The angles that should be translated from the current to the new system.
 * @arg Vector newSystemOrigin - The origin of the system to translate to.
 * @arg Angle newSystemAngles - The angles of the system to translate to.
 * @return {Vector} Local position
 * @return {Angle} Local angles
 */
declare function WorldToLocal(
    position: Vector,
    angle: Angle,
    newSystemOrigin: Vector,
    newSystemAngles: Angle
): LuaMultiReturn<[Vector, Angle]>;

/**
 * Attempts to call the first function. If the execution succeeds, this returns true followed by the returns of the function. If execution fails, this returns false and the second function is called with the error message.
* 
* @arg GMLua.CallbackNoContext func - The function to call initially.
* @arg GMLua.CallbackNoContext errorCallback - The function to be called if execution of the first fails; the error message is passed as a string.
You cannot throw an error() from this callback: it will have no effect (not even stopping the callback).
* @arg args[] arguments - Arguments to pass to the initial function.
* @return {boolean} Status of the execution; true for success, false for failure.
* @return {args[]} The returns of the first function if execution succeeded, otherwise the first return value of the error callback.
 */
declare function xpcall(
    func: GMLua.CallbackNoContext,
    errorCallback: GMLua.CallbackNoContext,
    ...arguments: any[]
): LuaMultiReturn<[boolean, ...any[]]>;
