// Global tables

/**
 * The table of the currently active gamemode, outside the gamemode files.
 */
declare const GAMEMODE: GAMEMODE;

/**
 * Same as GAMEMODE, but only exists gamemode files. (gamemodes/<gamemodeName>/gamemode/*.lua)
 */
declare const GM: GM;

/**
 * Similar to GM, but for Scripted Entities. Exists only in the files defining the entity. (lua/entities/*.lua)
 */
declare const ENT: ENT;

/**
 * Similar to ENT, but for Scripted Weapons. Exists only in the files defining the weapon. (lua/weapons/*.lua)
 */
declare const SWEP: SWEP;

/**
 * Similar to ENT, but for Scripted Effects. Exists only in the files defining the effect. (lua/effects/*.lua)
 */
declare const EFFECT: EFFECT;

/**
 * This table contains all global objects, including itself.
 */
declare const _G: _G;

/**
 * Contains a list of all modules loaded from /modules/.
 */
declare const _MODULES: object;

// Global variables

/**
 * This is true whenever the current script is executed on the client. ( client and menu states ).
 */
declare const CLIENT: boolean;

/**
 * This is true whenever the current script is executed on the client state.
 */
declare const CLIENT_DLL: boolean;

/**
 * This is true whenever the current script is executed on the server state.
 */
declare const SERVER: boolean;

/**
 * This is true whenever the current script is executed on the server state.
 */
declare const GAME_DLL: boolean;

/**
 * This is true when the script is being executed in the menu state.
 */
declare const MENU_DLL: boolean;

/**
 * Contains the name of the current active gamemode
 */
declare const GAMEMODE_NAME: string;

/**
 * Represents a non existent entity
 */
declare const NULL: Entity;

/**
 * Contains the version number of GMod.
 */
declare const VERSION: number;

/**
 * Contains a nicely formatted version of GMod.
 */
declare const VERSIONSTR: string;

/**
 * The branch the game is running on.
 */
declare const BRANCH: string;

/**
 * Current Lua version. This contains "Lua 5.1" in GMod at the moment.
 */
declare const _VERSION: string;

/**
 * The active env_skypaint entity.
 */
declare const g_SkyPaint: Entity;

/**
 * Base panel used for context menus.
 */
declare const g_ContextMenu: Entity;

/**
 * Base panel for displaying incoming/outgoing voice messages.
 */
declare const g_VoicePanelList: Entity;

/**
 * Base panel for the spawn menu.
 */
declare const g_SpawnMenu: Entity;

// Global constants

/**
 * Vector( 0, 0, 0 )
 */
declare const vector_origin: Vector;

/**
 * Vector( 0, 0, 1 )
 */
declare const vector_up: Vector;

/**
 * Angle( 0, 0, 0 )
 */
declare const angle_zero: Angle;

/**
 * Color( 255, 255, 255, 255 )
 */
declare const color_white: Color;

/**
 * Color( 0, 0, 0, 255 )
 */
declare const color_black: Color;

/**
 * Color( 255, 255, 255, 0 )
 */
declare const color_transparent: Color;
