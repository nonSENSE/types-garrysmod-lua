/** @noSelfInFile */
declare namespace mesh {
    /**
     * Pushes the new vertex data onto the render stack.
     */
    function AdvanceVertex(): void;

    /**
     * Starts a new dynamic mesh. If an IMesh is passed, it will use that mesh instead.
     *
     * @arg IMesh [mesh=nil] - Mesh to build. This argument can be removed if you wish to build a "dynamic" mesh. See examples below.
     * @arg number primitiveType - Primitive type, see Enums/MATERIAL.
     * @arg number primiteCount - The amount of primitives.
     */
    function Begin(mesh?: IMesh, primitiveType?: number, primiteCount?: number): void;

    /**
     * Sets the color to be used for the next vertex.
     *
     * @arg number r - Red component.
     * @arg number g - Green component.
     * @arg number b - Blue component.
     * @arg number a - Alpha component.
     */
    function Color(r: number, g: number, b: number, a: number): void;

    /**
     * Ends the mesh and renders it.
     */
    function End(): void;

    /**
     * Sets the normal to be used for the next vertex.
     *
     * @arg Vector normal - The normal of the vertex.
     */
    function Normal(normal: Vector): void;

    /**
     * Sets the position to be used for the next vertex.
     *
     * @arg Vector position - The position of the vertex.
     */
    function Position(position: Vector): void;

    /**
     * Draws a quad using 4 vertices.
     *
     * @arg Vector vertex1 - The first vertex.
     * @arg Vector vertex2 - The second vertex.
     * @arg Vector vertex3 - The third vertex.
     * @arg Vector vertex4 - The fourth vertex.
     */
    function Quad(vertex1: Vector, vertex2: Vector, vertex3: Vector, vertex4: Vector): void;

    /**
     * Draws a quad using a position, a normal and the size.
     *
     * @arg Vector position - The center of the quad.
     * @arg Vector normal - The normal of the quad.
     * @arg number sizeX - X size in pixels.
     * @arg number sizeY - Y size in pixels.
     */
    function QuadEasy(position: Vector, normal: Vector, sizeX: number, sizeY: number): void;

    /**
     * Sets the specular map values.
     *
     * @arg number r - The red channel multiplier of the specular map.
     * @arg number g - The green channel multiplier of the specular map.
     * @arg number b - The blue channel multiplier of the specular map.
     * @arg number a - The alpha channel multiplier of the specular map.
     */
    function Specular(r: number, g: number, b: number, a: number): void;

    /**
     * Sets the s tangent to be used.
     *
     * @arg Vector sTanger - The s tangent.
     */
    function TangentS(sTanger: Vector): void;

    /**
     * Sets the T tangent to be used.
     *
     * @arg Vector tTanger - The t tangent.
     */
    function TangentT(tTanger: Vector): void;

    /**
     * Sets the texture coordinates for the next vertex.
     *
     * @arg number stage - The stage of the texture coordinate.
     * @arg number u - U coordinate.
     * @arg number v - V coordinate.
     */
    function TexCoord(stage: number, u: number, v: number): void;

    /**
     * A table of four numbers. This is used by most shaders in Source to hold tangent information of the vertex ( tangentX, tangentY, tangentZ, tangentHandedness ).
     *
     * @arg number tangentX
     * @arg number tangentY
     * @arg number tangentZ
     * @arg number tangentHandedness
     */
    function UserData(tangentX: number, tangentY: number, tangentZ: number, tangentHandedness: number): void;

    /**
     * Returns the amount of vertex that have yet been pushed.
     *
     * @return {number} vertexCount
     */
    function VertexCount(): number;
}
