/** @noSelfInFile */
declare namespace debugoverlay {
    /**
     * Displays an axis indicator at the specified position.
     *
     * @arg Vector origin - Position origin
     * @arg Angle ang - Angle of the axis
     * @arg number size - Size of the axis
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg boolean [ignoreZ=false] - If true, will draw on top of everything; ignoring the Z buffer
     */
    function Axis(origin: Vector, ang: Angle, size: number, lifetime?: number, ignoreZ?: boolean): void;

    /**
     * Displays a solid coloured box at the specified position.
     *
     * @arg Vector origin - Position origin
     * @arg Vector mins - Minimum bounds of the box
     * @arg Vector maxs - Maximum bounds of the box
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     */
    function Box(origin: Vector, mins: Vector, maxs: Vector, lifetime?: number, color?: Color): void;

    /**
     * Displays a solid colored rotated box at the specified position.
     *
     * @arg Vector pos - World position
     * @arg Vector mins - The mins of the box (lowest corner)
     * @arg Vector maxs - The maxs of the box (highest corner)
     * @arg Angle ang - The angle to draw the box at
     * @arg number [lifetime=1] - Amount of seconds to show the box
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     */
    function BoxAngles(pos: Vector, mins: Vector, maxs: Vector, ang: Angle, lifetime?: number, color?: Color): void;

    /**
     * Creates a coloured cross at the specified position for the specified time.
     *
     * @arg Vector position - Position origin
     * @arg number size - Size of the cross
     * @arg number [lifetime=1] - Number of seconds the cross to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the cross. Uses the Color
     * @arg boolean [ignoreZ=false] - If true, will draw on top of everything; ignoring the Z buffer
     */
    function Cross(position: Vector, size: number, lifetime?: number, color?: Color, ignoreZ?: boolean): void;

    /**
     * Displays 2D text at the specified coordinates.
     *
     * @arg Vector pos - The position in 3D to display the text.
     * @arg number line - Line of text, will offset text on the to display the new line unobstructed
     * @arg string text - The text to display
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     */
    function EntityTextAtPosition(pos: Vector, line: number, text: string, lifetime?: number, color?: Color): void;

    /**
     * Draws a 3D grid of limited size in given position.
     *
     * @arg Vector position
     */
    function Grid(position: Vector): void;

    /**
     * Displays a coloured line at the specified position.
     *
     * @arg Vector pos1 - First position of the line
     * @arg Vector pos2 - Second position of the line
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the line. Uses the Color
     * @arg boolean [ignoreZ=false] - If true, will draw on top of everything; ignoring the Z buffer
     */
    function Line(pos1: Vector, pos2: Vector, lifetime?: number, color?: Color, ignoreZ?: boolean): void;

    /**
     * Displays text triangle at the specified coordinates.
     *
     * @arg number x - The position of the text, from 0 ( left ) to 1 ( right ).
     * @arg number y - The position of the text, from 0 ( top ) to 1 ( bottom ).
     * @arg string text - The text to display
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     */
    function ScreenText(x: number, y: number, text: string, lifetime?: number, color?: Color): void;

    /**
     * Displays a coloured sphere at the specified position.
     *
     * @arg Vector origin - Position origin
     * @arg number size - Size of the sphere
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the sphere. Uses the Color
     * @arg boolean [ignoreZ=false] - If true, will draw on top of everything; ignoring the Z buffer
     */
    function Sphere(origin: Vector, size: number, lifetime?: number, color?: Color, ignoreZ?: boolean): void;

    /**
     * Displays "swept" box, two boxes connected with lines by their verices.
     *
     * @arg Vector vStart - The start position of the box.
     * @arg Vector vEnd - The end position of the box.
     * @arg Vector vMins - The "minimum" edge of the box.
     * @arg Vector vMaxs - The "maximum" edge of the box.
     * @arg Angle ang
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     */
    function SweptBox(
        vStart: Vector,
        vEnd: Vector,
        vMins: Vector,
        vMaxs: Vector,
        ang: Angle,
        lifetime?: number,
        color?: Color
    ): void;

    /**
     * Displays text at the specified position.
     *
     * @arg Vector origin - Position origin
     * @arg string text - String message to display
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg boolean [viewCheck=false] - Clip text that is obscured
     */
    function Text(origin: Vector, text: string, lifetime?: number, viewCheck?: boolean): void;

    /**
     * Displays a colored triangle at the specified coordinates.
     *
     * @arg Vector pos1 - First point of the triangle
     * @arg Vector pos2 - Second point of the triangle
     * @arg Vector pos3 - Third point of the triangle
     * @arg number [lifetime=1] - Number of seconds to appear
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color
     * @arg boolean [ignoreZ=false] - If true, will draw on top of everything; ignoring the Z buffer
     */
    function Triangle(
        pos1: Vector,
        pos2: Vector,
        pos3: Vector,
        lifetime?: number,
        color?: Color,
        ignoreZ?: boolean
    ): void;
}
