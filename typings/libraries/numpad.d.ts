/** @noSelfInFile */
declare namespace numpad {
    /**
     * Activates numpad key owned by the player
     *
     * @arg Player ply - The player whose numpad should be simulated
     * @arg number key - The key to press, see Enums/KEY
     * @arg boolean isButton - Should this keypress pretend to be a from a gmod_button? (causes numpad.FromButton to return true)
     */
    function Activate(ply: Player, key: number, isButton: boolean): void;

    /**
     * Deactivates numpad key owned by the player
     *
     * @arg Player ply - The player whose numpad should be simulated
     * @arg number key - The key to press, corresponding to Enums/KEY
     * @arg boolean isButton - Should this keypress pretend to be a from a gmod_button? (causes numpad.FromButton to return true)
     */
    function Deactivate(ply: Player, key: number, isButton: boolean): void;

    /**
     * Returns true during a function added with numpad.Register when the third argument to numpad.Activate is true.
     *
     * @return {boolean} wasButton
     */
    function FromButton(): boolean;

    /**
     * Calls a function registered with numpad.Register when a player presses specified key.
     *
     * @arg Player ply - The player whose numpad should be watched
     * @arg number key - The key, corresponding to Enums/KEY
     * @arg string name - The name of the function to run, corresponding with the one used in numpad.Register
     * @arg args[] args - Arguments to pass to the function passed to numpad.Register.
     * @return {number} The impulse ID
     */
    function OnDown(ply: Player, key: number, name: string, ...args: any[]): number;

    /**
     * Calls a function registered with numpad.Register when a player releases specified key.
     *
     * @arg Player ply - The player whose numpad should be watched
     * @arg number key - The key, corresponding to Enums/KEY
     * @arg string name - The name of the function to run, corresponding with the one used in numpad.Register
     * @arg args[] args - Arguments to pass to the function passed to numpad.Register.
     * @return {number} The impulse ID
     */
    function OnUp(ply: Player, key: number, name: string, ...args: any[]): number;

    /**
 * Registers a numpad library action for use with numpad.OnDown and numpad.OnUp
* 
* @arg string id - The unique id of your action.
* @arg GMLua.CallbackNoContext func - The function to be executed.
Arguments are:
Player ply - The player who pressed the button
vararg ... - The 4th and all subsequent arguments passed from numpad.OnDown and/or numpad.OnUp
Returning false in this function will remove the listener which triggered this function (example: return false if one of your varargs is an entity which is no longer valid)
 */
    function Register(id: string, func: GMLua.CallbackNoContext): void;

    /**
     * Removes a function added by either numpad.OnUp or numpad.OnDown
     *
     * @arg number ID - The impulse ID returned by numpad.OnUp or numpad.OnDown
     */
    function Remove(ID: number): void;

    /**
     * Either runs numpad.Activate or numpad.Deactivate depending on the key's current state
     *
     * @arg Player ply - The player whose numpad should be simulated
     * @arg number key - The key to press, corresponding to Enums/KEY
     */
    function Toggle(ply: Player, key: number): void;
}
