/** @noSelfInFile */
declare namespace gameevent {
    /**
 * Add a game event listener.
* 
* @arg string eventName - The event to listen to, travels through hooks with eventName as event.

List of valid events can be found here.
 */
    function Listen(eventName: string): void;
}
