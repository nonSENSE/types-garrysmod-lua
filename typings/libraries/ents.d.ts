/** @noSelfInFile */
declare namespace ents {
    /**
     * Creates an entity. This function will fail and return NULL if the networked-edict limit is hit (around 8176), or the provided entity class doesn't exist.
     *
     * @arg string _class - The classname of the entity to create.
     * @return {Entity} The created entity, or NULL if failed.
     */
    function Create(_class: string): Entity;

    /**
 * Creates a clientside only prop. See also ClientsideModel.
* 
* @arg string [model="models/error.mdl"] - The model for the entity to be created.
Model must be precached with util.PrecacheModel on the server before usage.
* @return {Entity} Created entity (C_PhysPropClientside).
 */
    function CreateClientProp(model?: string): Entity;

    /**
     * Creates a clientside only scripted entity. The scripted entity must be of "anim" type.
     *
     * @arg string _class - The class name of the entity to create.
     * @return {Entity} Created entity.
     */
    function CreateClientside(_class: string): Entity;

    /**
     * Returns a table of all entities along the ray. The ray does not stop on collisions, meaning it will go through walls/entities.
     *
     * @arg Vector start - The start position of the ray
     * @arg Vector end - The end position of the ray
     * @arg Vector [mins=nil] - The mins corner of the ray
     * @arg Vector [maxs=nil] - The maxs corner of the ray
     * @return {object} Table of the found entities.
     */
    function FindAlongRay(start: Vector, end: Vector, mins?: Vector, maxs?: Vector): object;

    /**
     * Gets all entities with the given class, supports wildcards. This works internally by iterating over ents.GetAll. Even if internally ents.GetAll is used, It is faster to use ents.FindByClass than ents.GetAll with a single class comparison.
     *
     * @arg string _class - The class of the entities to find.
     * @return {object} A table containing all found entities
     */
    function FindByClass(_class: string): object;

    /**
     * Finds all entities that are of given class and are children of given entity. This works internally by iterating over ents.GetAll.
     *
     * @arg string _class - The class of entities to search for
     * @arg Entity parent - Parent of entities that are being searched for
     * @return {object} A table of found entities or nil if none are found
     */
    function FindByClassAndParent(_class: string, parent: Entity): object;

    /**
     * Gets all entities with the given model, supports wildcards. This works internally by iterating over ents.GetAll.
     *
     * @arg string model - The model of the entities to find.
     * @return {object} A table of all found entities.
     */
    function FindByModel(model: string): object;

    /**
     * Gets all entities with the given hammer targetname. This works internally by iterating over ents.GetAll.
     *
     * @arg string name - The targetname to look for
     * @return {object} A table of all found entities
     */
    function FindByName(name: string): object;

    /**
     * Returns all entities within the specified box.
     *
     * @arg Vector boxMins - The box minimum coordinates.
     * @arg Vector boxMaxs - The box maximum coordinates.
     * @return {object} A table of all found entities.
     */
    function FindInBox(boxMins: Vector, boxMaxs: Vector): object;

    /**
 * 
* Finds and returns all entities within the specified cone. Only entities whose Entity:WorldSpaceCenter is within the cone are considered to be in it.
* 
* @arg Vector origin - The tip of the cone.
* @arg Vector normal - Direction of the cone.
* @arg number range - The range of the cone/box around the origin.
The function internally adds 1 to this argument before using it.
* @arg number angle_cos - The cosine of the angle between the center of the cone to its edges, which is half the overall angle of the cone.
1 makes a 0° cone, 0.707 makes approximately 90°, 0 makes 180°, and so on.
* @return {Entity} A table of all found Entitys.
 */
    function FindInCone(origin: Vector, normal: Vector, range: number, angle_cos: number): Entity;

    /**
     * Finds all entities that lie within a PVS.
     *
     * @arg any viewPoint - Entity or Vector to find entities within the PVS of. If a player is given, this function will use the player's view entity.
     * @return {Entity} The found Entitys.
     */
    function FindInPVS(viewPoint: any): Entity;

    /**
     * Gets all entities within the specified sphere.
     *
     * @arg Vector origin - Center of the sphere.
     * @arg number radius - Radius of the sphere.
     * @return {Entity} A table of all found Entitys. Has a limit of 1024 entities.
     */
    function FindInSphere(origin: Vector, radius: number): Entity;

    /**
     * Fires a use event.
     *
     * @arg string target - Name of the target entity.
     * @arg Entity activator - Activator of the event.
     * @arg Entity caller - Caller of the event.
     * @arg number usetype - Use type. See the Enums/USE.
     * @arg number value - This value is passed to ENTITY:Use, but isn't used by any default entities in the engine.
     */
    function FireTargets(target: string, activator: Entity, caller: Entity, usetype: number, value: number): void;

    /**
     * Returns a table of all existing entities. The table is sequential
     *
     * @return {Entity} Table of all existing Entitys.
     */
    function GetAll(): Entity;

    /**
     * Returns an entity by its index. Same as Entity.
     *
     * @arg number entIdx - The index of the entity.
     * @return {Entity} The entity if it exists.
     */
    function GetByIndex(entIdx: number): Entity;

    /**
     * Gives you the amount of currently existing entities.
     *
     * @arg boolean [IncludeKillMe=false] - Include entities with the FL_KILLME flag. This will skip an internal loop, and the function will be more efficient as a byproduct.
     * @return {number} Number of entities
     */
    function GetCount(IncludeKillMe?: boolean): number;

    /**
     * Returns the amount of networked entities, which is limited to 8192. ents.Create will fail somewhere between 8064 and 8176 - this can vary based on the amount of existing temp ents.
     *
     * @return {number} Number of networked entities
     */
    function GetEdictCount(): number;

    /**
     * Returns entity that has given Entity:MapCreationID.
     *
     * @arg number id - Entity's creation id
     * @return {Entity} Found entity
     */
    function GetMapCreatedEntity(id: number): Entity;
}
