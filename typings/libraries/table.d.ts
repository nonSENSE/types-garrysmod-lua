/** @noSelfInFile */
declare namespace table {
    /**
     * Adds the contents from one table into another. The target table will be modified.
     *
     * @arg object target - The table to insert the new values into.
     * @arg object source - The table to retrieve the values from.
     * @return {object} The target table.
     */
    function Add(target: object, source: object): object;

    /**
     * Changes all keys to sequential integers. This creates a new table object and does not affect the original.
     *
     * @arg object table - The original table to modify.
     * @arg boolean [saveKeys=false] - Save the keys within each member table. This will insert a new field __key into each value, and should not be used if the table contains non-table values.
     * @return {object} Table with integer keys.
     */
    function ClearKeys(table: object, saveKeys?: boolean): object;

    /**
     * Collapses a table with keyvalue structure
     *
     * @arg object input - Input table
     * @return {object} Output table
     */
    function CollapseKeyValue(input: object): object;

    /**
     * Concatenates the contents of a table to a string.
     *
     * @arg object tbl - The table to concatenate.
     * @arg string concatenator - A separator to insert between strings
     * @arg number [startPos=1] - The key to start at
     * @arg number [endPos=NaN] - The key to end at
     * @return {string} Concatenated values
     */
    function concat(tbl: object, concatenator: string, startPos?: number, endPos?: number): string;

    /**
     * Creates a deep copy and returns that copy.
     *
     * @arg object originalTable - The table to be copied.
     * @return {object} A deep copy of the original table
     */
    function Copy(originalTable: object): object;

    /**
     * Empties the target table, and merges all values from the source table into it.
     *
     * @arg object source - The table to copy from.
     * @arg object target - The table to write to.
     */
    function CopyFromTo(source: object, target: object): void;

    /**
     * Counts the amount of keys in a table. This should only be used when a table is not numerically and sequentially indexed. For those tables, consider the length (#) operator.
     *
     * @arg object tbl - The table to count the keys of.
     * @return {number} The number of keyvalue pairs. This includes non-numeric and non-sequential keys, unlike the length (#) operator.
     */
    function Count(tbl: object): number;

    /**
     * Converts a table that has been sanitised with table.Sanitise back to its original form
     *
     * @arg object tbl - Table to be de-sanitised
     * @return {object} De-sanitised table
     */
    function DeSanitise(tbl: object): object;

    /**
     * Removes all values from a table.
     *
     * @arg object tbl - The table to empty.
     */
    function Empty(tbl: object): void;

    /**
     * Returns the value positioned after the supplied value in a table. If it isn't found then the first element in the table is returned
     *
     * @arg object tbl - Table to search
     * @arg any value - Value to return element after
     * @return {any} Found element
     */
    function FindNext(tbl: object, value: any): any;

    /**
     * Returns the value positioned before the supplied value in a table. If it isn't found then the last element in the table is returned
     *
     * @arg object tbl - Table to search
     * @arg any value - Value to return element before
     * @return {any} Found element
     */
    function FindPrev(tbl: object, value: any): any;

    /**
     * Inserts a value in to the given table even if the table is non-existent
     *
     * @arg object [tab={}] - Table to insert value in to
     * @arg any value - Value to insert
     * @return {object} The supplied or created table
     */
    function ForceInsert(tab?: object, value?: any): object;

    /**
     * Iterates for each key-value pair in the table, calling the function with the key and value of the pair. If the function returns anything, the loop is broken.
     *
     * @arg object tbl - The table to iterate over.
     * @arg GMLua.CallbackNoContext callback - The function to run for each key and value.
     */
    function foreach(tbl: object, callback: GMLua.CallbackNoContext): void;

    /**
     * Iterates for each numeric index in the table in order.
     *
     * @arg object table - The table to iterate over.
     * @arg GMLua.CallbackNoContext func - The function to run for each index.
     */
    function foreachi(table: object, func: GMLua.CallbackNoContext): void;

    /**
     *
     *
     * @arg object tab - Table to retrieve key from
     * @return {any} Key
     */
    function GetFirstKey(tab: object): any;

    /**
     *
     *
     * @arg object tab - Table to retrieve value from
     * @return {any} Value
     */
    function GetFirstValue(tab: object): any;

    /**
     * Returns all keys of a table.
     *
     * @arg object tabl - The table to get keys of
     * @return {object} Table of keys
     */
    function GetKeys(tabl: object): object;

    /**
     * Returns the last key found in the given table
     *
     * @arg object tab - Table to retrieve key from
     * @return {any} Key
     */
    function GetLastKey(tab: object): any;

    /**
     * Returns the last value found in the given table
     *
     * @arg object tab - Table to retrieve value from
     * @return {any} Value
     */
    function GetLastValue(tab: object): any;

    /**
     * Returns the length of the table.
     *
     * @arg object tbl - The table to check.
     * @return {number} Sequential length.
     */
    function getn(tbl: object): number;

    /**
     * Returns a key of the supplied table with the highest number value.
     *
     * @arg object inputTable - The table to search in.
     * @return {any} winningKey
     */
    function GetWinningKey(inputTable: object): any;

    /**
     * Checks if a table has a value.
     *
     * @arg object tbl - Table to check
     * @arg any value - Value to search for
     * @return {boolean} Returns true if the table has that value, false otherwise
     */
    function HasValue(tbl: object, value: any): boolean;

    /**
     * Copies any missing data from base to target, and sets the target's BaseClass member to the base table's pointer.
     *
     * @arg object target - Table to copy data to
     * @arg object base - Table to copy data from
     * @return {object} Target
     */
    function Inherit(target: object, base: object): object;

    /**
     * Inserts a value into a table at the end of the table or at the given position.
     *
     * @arg object tbl - The table to insert the variable into.
     * @arg number position - The position in the table to insert the variable. If the third argument is nil this argument becomes the value to insert at the end of given table.
     * @arg any value - The variable to insert into the table.
     * @return {number} The index the object was placed at.
     */
    function insert(tbl: object, position: number, value: any): number;

    /**
     * Returns whether or not the given table is empty.
     *
     * @arg object tab - Table to check.
     * @return {boolean} Is empty?
     */
    function IsEmpty(tab: object): boolean;

    /**
     * Returns whether or not the table's keys are sequential
     *
     * @arg object tab - Table to check
     * @return {boolean} Is sequential
     */
    function IsSequential(tab: object): boolean;

    /**
     * Returns the first key found to be containing the supplied value
     *
     * @arg object tab - Table to search
     * @arg any value - Value to search for
     * @return {any} Key
     */
    function KeyFromValue(tab: object, value: any): any;

    /**
     * Returns a table of keys containing the supplied value
     *
     * @arg object tab - Table to search
     * @arg any value - Value to search for
     * @return {object} Keys
     */
    function KeysFromValue(tab: object, value: any): object;

    /**
     * Returns a copy of the input table with all string keys converted to be lowercase recursively
     *
     * @arg object tbl - Table to convert
     * @return {object} New table
     */
    function LowerKeyNames(tbl: object): object;

    /**
     * Returns the highest numerical key.
     *
     * @arg object tbl - The table to search.
     * @return {number} The highest numerical key.
     */
    function maxn(tbl: object): number;

    /**
     * Returns an array of values of given with given key from each table of given table.
     *
     * @arg object inputTable - The table to search in.
     * @arg any keyName - The key to lookup.
     * @return {object} A list of found values, or an empty table.
     */
    function MemberValuesFromKey(inputTable: object, keyName: any): object;

    /**
     * Merges the contents of the second table with the content in the first one. The destination table will be modified.
     *
     * @arg object destination - The table you want the source table to merge with
     * @arg object source - The table you want to merge with the destination table
     * @return {object} Destination table
     */
    function Merge(destination: object, source: object): object;

    /**
     * Returns a random value from the supplied table.
     *
     * @arg object haystack - The table to choose from.
     * @return {any} A random value from the table.
     * @return {any} The key associated with the random value.
     */
    function Random(haystack: object): LuaMultiReturn<[any, any]>;

    /**
     * Removes a value from a table and shifts any other values down to fill the gap.
     *
     * @arg object tbl - The table to remove the value from.
     * @arg number [index=NaN] - The index of the value to remove.
     * @return {any} The value that was removed.
     */
    function remove(tbl: object, index?: number): any;

    /**
     * Removes the first instance of a given value from the specified table with table.remove, then returns the key that the value was found at.
     *
     * @arg object tbl - The table that will be searched.
     * @arg any val - The value to find within the table.
     * @return {any} The key at which the value was found, or false if the value was not found.
     */
    function RemoveByValue(tbl: object, val: any): any;

    /**
     * Returns a reversed copy of a sequential table. Any non-sequential and non-numeric keyvalue pairs will not be copied.
     *
     * @arg object tbl - Table to reverse.
     * @return {object} A reversed copy of the table.
     */
    function Reverse(tbl: object): object;

    /**
     * Converts Vectors, Angles and booleans to be able to be converted to and from key-values. table.DeSanitise does the opposite
     *
     * @arg object tab - Table to sanitise
     * @return {object} Sanitised table
     */
    function Sanitise(tab: object): object;

    /**
     * Performs an inline Fisher-Yates shuffle on the table in O(n) time
     *
     * @arg object target - The table to shuffle.
     */
    function Shuffle(target: object): void;

    /**
 * Sorts a sequential table either ascending or by the given sort function.
* 
* @arg object tbl - The table to sort.
* @arg GMLua.CallbackNoContext sorter - If specified, the function will be called with 2 parameters each.
Return true in this function if you want the first parameter to come first in the sorted array.
 */
    function sort(tbl: object, sorter: GMLua.CallbackNoContext): void;

    /**
     * Returns a list of keys sorted based on values of those keys.
     *
     * @arg object tab - Table to sort. All values of this table must be of same type.
     * @arg boolean [descending=false] - Should the order be descending?
     * @return {object} A table of keys sorted by values from supplied table.
     */
    function SortByKey(tab: object, descending?: boolean): object;

    /**
     * Sorts a table by a named member.
     *
     * @arg object tab - Table to sort.
     * @arg any memberKey - The key used to identify the member.
     * @arg boolean [ascending=false] - Whether or not the order should be ascending.
     */
    function SortByMember(tab: object, memberKey: any, ascending?: boolean): void;

    /**
     * Sorts a table in reverse order from table.sort.
     *
     * @arg object tbl - The table to sort in descending order.
     */
    function SortDesc(tbl: object): void;

    /**
     * Converts a table into a string
     *
     * @arg object tbl - The table to iterate over.
     * @arg string displayName - Optional. A name for the table.
     * @arg boolean niceFormatting - Adds new lines and tabs to the string.
     * @return {string} The table formatted as a string.
     */
    function ToString(tbl: object, displayName: string, niceFormatting: boolean): string;
}
