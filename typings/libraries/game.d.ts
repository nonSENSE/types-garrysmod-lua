/** @noSelfInFile */
declare namespace game {
    /**
     * Adds a new ammo type to the game.
     *
     * @arg AmmoDataStruct ammoData - The attributes of the ammo. See the Structures/AmmoData.
     */
    function AddAmmoType(ammoData: AmmoDataStruct): void;

    /**
     * Registers a new decal.
     *
     * @arg string decalName - The name of the decal.
     * @arg string materialName - The material to be used for the decal. May also be a list of material names, in which case a random material from that list will be chosen every time the decal is placed.
     */
    function AddDecal(decalName: string, materialName: string): void;

    /**
     * Loads a particle file.
     *
     * @arg string particleFileName - The path of the file to add. Must be (file).pcf.
     */
    function AddParticles(particleFileName: string): void;

    /**
     * Called by the engine to retrieve the ammo types.
     *
     * @return {any} All ammo types registered via game.AddAmmoType, sorted by its name value.
     */
    function BuildAmmoTypes(): any;

    /**
     * If called serverside it will remove ALL entities which were not created by the map (not players or weapons held by players).
     *
     * @arg boolean [dontSendToClients=false] - If set to true, don't run this functions on all clients.
     * @arg object [extraFilters={}] - Entity classes not to reset during cleanup.
     */
    function CleanUpMap(dontSendToClients?: boolean, extraFilters?: object): void;

    /**
     * Runs a console command.
     * Make sure to add a newline ("\n") at the end of the command.
     *
     * @arg string stringCommand - String containing the command and arguments to be ran.
     */
    function ConsoleCommand(stringCommand: string): void;

    /**
     * Returns the damage type of given ammo type.
     *
     * @arg number id - Ammo ID to retrieve the damage type of. Starts from 1.
     * @return {number} See Enums/DMG
     */
    function GetAmmoDamageType(id: number): number;

    /**
     * Returns the Structures/AmmoData for given ID.
     *
     * @arg number id - ID of the ammo type to look up the data for
     * @return {AmmoDataStruct} The Structures/AmmoData containing all ammo data
     */
    function GetAmmoData(id: number): AmmoDataStruct;

    /**
     * Returns the ammo bullet force that is applied when an entity is hit by a bullet of given ammo type.
     *
     * @arg number id - Ammo ID to retrieve the force of. Starts from 1.
     * @return {number}
     */
    function GetAmmoForce(id: number): number;

    /**
     * Returns the ammo type ID for given ammo type name.
     *
     * @arg string name - Name of the ammo type to look up ID of
     * @return {number} The ammo type ID of given ammo type name, or -1 if not found
     */
    function GetAmmoID(name: string): number;

    /**
     * Returns the real maximum amount of ammo of given ammo ID, regardless of the setting of gmod_maxammo convar.
     *
     * @arg number id - Ammo type ID
     * @return {number} The maximum amount of reserve ammo a player can hold of this ammo type.
     */
    function GetAmmoMax(id: number): number;

    /**
     * Returns the ammo name for given ammo type ID.
     *
     * @arg number id - Ammo ID to retrieve the name of. Starts from 1.
     * @return {string} The name of given ammo type ID or nil if ammo type ID is invalid.
     */
    function GetAmmoName(id: number): string;

    /**
     * Returns the damage given ammo type should do to NPCs.
     *
     * @arg number id - Ammo ID to retrieve the damage info of. Starts from 1.
     * @return {number}
     */
    function GetAmmoNPCDamage(id: number): number;

    /**
     * Returns the damage given ammo type should do to players.
     *
     * @arg number id - Ammo ID to retrieve the damage info of. Starts from 1.
     * @return {number}
     */
    function GetAmmoPlayerDamage(id: number): number;

    /**
     * Returns a list of all ammo types currently registered.
     *
     * @return {object} A table containing all ammo types. The keys are ammo IDs, the values are the names associated with those IDs.
     */
    function GetAmmoTypes(): object;

    /**
 * Returns the counter of a Global State.
* 
* @arg string name - The name of the Global State to set.
If the Global State by that name does not exist, it will be created.
See Global States for a list of default global states.
* @return {number} The value of the given Global State, 0 if the global state doesn't exist.
 */
    function GetGlobalCounter(name: string): number;

    /**
 * Returns whether a Global State is off, active or dead ( inactive )
* 
* @arg string name - The name of the Global State to retrieve the state of.
If the Global State by that name does not exist, GLOBAL_DEAD will be returned.
See Global States for a list of default global states.
* @return {number} The state of the Global State. See Enums/GLOBAL
 */
    function GetGlobalState(name: string): number;

    /**
     * Returns the public IP address and port of the current server. This will return the IP/port that you are connecting through when ran clientside.
     *
     * @return {string} The IP address and port in the format "x.x.x.x:x"
     */
    function GetIPAddress(): string;

    /**
     * Returns the name of the current map, without a file extension.
     * On the menu state, returns "menu".
     *
     * @return {string} The name of the current map, without a file extension.
     */
    function GetMap(): string;

    /**
     * Returns the next map that would be loaded according to the file that is set by the mapcyclefile convar.
     *
     * @return {string} nextMap
     */
    function GetMapNext(): string;

    /**
     * Returns the VBSP version of the current map.
     *
     * @return {number} VBSP version of the currently loaded map, will be either 19, 20 or 21 for L4D1+ maps
     */
    function GetMapVersion(): number;

    /**
     * Returns the difficulty level of the game.
     *
     * @return {number} The difficulty level, Easy( 1 ), Normal( 2 ), Hard( 3 ).
     */
    function GetSkillLevel(): number;

    /**
     * Returns the time scale of the game
     *
     * @return {number} The time scale
     */
    function GetTimeScale(): number;

    /**
     * Returns the worldspawn entity.
     *
     * @return {Entity} The world
     */
    function GetWorld(): Entity;

    /**
     * Returns true if the server is a dedicated server, false if it is a listen server or a singleplayer game.
     *
     * @return {boolean} Is the server dedicated or not.
     */
    function IsDedicated(): boolean;

    /**
 * Kicks a player from the server. This can be ran before the player has spawned.
* 
* @arg string id - UserID or SteamID of the player to kick.
* @arg string [reason="No reason given"] - Reason to display to the player. This can span across multiple lines.
This will be shortened to ~512 chars, though this includes the command itself and the player index so will realistically be more around ~498. It is recommended to avoid going near the limit to avoid truncation.
 */
    function KickID(id: string, reason?: string): void;

    /**
     * Loads the next map according to the nextlevel convar, or from the current mapcycle file set by the respective convar.
     */
    function LoadNextMap(): void;

    /**
     * Returns the map load type of the current map.
     *
     * @return {string} The load type. Possible values are: "newgame", "loadgame", "transition", "background".
     */
    function MapLoadType(): string;

    /**
     * Returns the maximum amount of players (including bots) that the server can have.
     *
     * @return {number} The maximum amount of players
     */
    function MaxPlayers(): number;

    /**
     * Mounts a GMA addon from the disk.
     * Can be used with steamworks.DownloadUGC
     *
     * @arg string path - Location of the GMA file to mount, retrieved from steamworks.DownloadUGC. This file does not have to end with the .gma extension, but will be interpreted as a GMA.
     * @return {boolean} success
     * @return {object} If successful, a table of files that have been mounted
     */
    function MountGMA(path: string): LuaMultiReturn<[boolean, object]>;

    /**
     * Removes all the clientside ragdolls.
     */
    function RemoveRagdolls(): void;

    /**
 * Sets the counter of a Global State.
* 
* @arg string name - The name of the Global State to set.
If the Global State by that name does not exist, it will be created.
See Global States for a list of default global states.
* @arg number count - The value to set for that Global State.
 */
    function SetGlobalCounter(name: string, count: number): void;

    /**
 * Sets whether a Global State is off, active or dead ( inactive )
* 
* @arg string name - The name of the Global State to set.
If the Global State by that name does not exist, it will be created.
See Global States for a list of default global states.
* @arg number state - The state of the Global State. See Enums/GLOBAL
 */
    function SetGlobalState(name: string, state: number): void;

    /**
     * Sets the difficulty level of the game, can be retrieved with game.GetSkillLevel.
     *
     * @arg number level - The difficulty level, Easy( 1 ), Normal( 2 ), Hard( 3 ).
     */
    function SetSkillLevel(level: number): void;

    /**
     * Sets the time scale of the game.
     *
     * @arg number timeScale - The new timescale, minimum value is 0.001 and maximum is 5.
     */
    function SetTimeScale(timeScale: number): void;

    /**
     * Returns whether the current session is a single player game.
     *
     * @return {boolean} isSinglePlayer
     */
    function SinglePlayer(): boolean;

    /**
     * Returns position the player should start from, this is not the same thing as spawn points, it is used to properly transit the player between maps.
     *
     * @return {Vector} startSpot
     */
    function StartSpot(): Vector;
}
