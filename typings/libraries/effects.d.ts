/** @noSelfInFile */
declare namespace effects {
    /**
 * Creates a "beam ring point" effect.
* 
* @arg Vector pos - The origin position of the effect.
* @arg number lifetime - How long the effect will be drawing for, in seconds.
* @arg number startRad - Initial radius of the effect.
* @arg number endRad - Final radius of the effect, at the end of the effect's lifetime.
* @arg number width - How thick the beam should be.
* @arg number amplitude - How noisy the beam should be.
* @arg any color - Beam's Color.
* @arg object extra - Extra info, all optional. A table with the following keys: (any combination)

number speed - ?
number spread - ?
number delay - Delay in seconds after which the effect should appear.
number flags- Beam flags.
number framerate - texture framerate.
string material - The material to use instead of the default one.
 */
    function BeamRingPoint(
        pos: Vector,
        lifetime: number,
        startRad: number,
        endRad: number,
        width: number,
        amplitude: number,
        color: any,
        extra: object
    ): void;

    /**
     * Creates a bunch of bubbles inside a defined box.
     *
     * @arg Vector mins - The lowest extents of the box.
     * @arg Vector maxs - The highest extents of the box.
     * @arg number count - How many bubbles to spawn. There's a hard limit of 500 tempents at any time.
     * @arg number height - How high the bubbles can fly up before disappearing.
     * @arg number [speed=0] - How quickly the bubbles move.
     * @arg number [delay=0] - Delay in seconds after the function call and before the effect actually spawns.
     */
    function Bubbles(mins: Vector, maxs: Vector, count: number, height: number, speed?: number, delay?: number): void;

    /**
     * Creates a bubble trail effect, the very same you get when shooting underwater.
     *
     * @arg Vector startPos - The start position of the effect.
     * @arg Vector endPos - The end position of the effects.
     * @arg number count - How many bubbles to spawn. There's a hard limit of 500 tempents at any time.
     * @arg number height - How high the bubbles can fly up before disappearing.
     * @arg number [speed=0] - How quickly the bubbles move.
     * @arg number [delay=0] - Delay in seconds after the function call and before the effect actually spawns.
     */
    function BubbleTrail(
        startPos: Vector,
        endPos: Vector,
        count: number,
        height: number,
        speed?: number,
        delay?: number
    ): void;

    /**
     * Returns the table of the effect specified.
     *
     * @arg string name - Effect name.
     * @return {object} Effect table.
     */
    function Create(name: string): object;

    /**
     * Returns a list of all Lua-defined effects.
     *
     * @return {object} The effects table.
     */
    function GetList(): object;

    /**
     * Registers a new effect.
     *
     * @arg object effect_table - Effect table.
     * @arg string name - Effect name.
     */
    function Register(effect_table: object, name: string): void;
}
