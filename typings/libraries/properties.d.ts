/** @noSelfInFile */
declare namespace properties {
    /**
     * Add properties to the properties module
     *
     * @arg string name - A unique name used to identify the property
     * @arg PropertyAddStruct propertyData - A table that defines the property. Uses the Structures/PropertyAdd.
     */
    function Add(name: string, propertyData: PropertyAddStruct): void;

    /**
     * Returns true if given entity can be targeted by the player via the properties system.
     *
     * @arg Entity ent - The entity to test
     * @arg Player ply - If given, will also perform a distance check based on the entity's Orientated Bounding Box.
     * @return {boolean} True if entity can be targeted, false otherwise
     */
    function CanBeTargeted(ent: Entity, ply: Player): boolean;

    /**
     * Returns an entity player is hovering over with his cursor.
     *
     * @arg Vector pos - Eye position of local player, Entity:EyePos
     * @arg Vector aimVec - Aim vector of local player, Player:GetAimVector
     * @return {Entity} The hovered entity
     */
    function GetHovered(pos: Vector, aimVec: Vector): Entity;

    /**
     * Checks if player hovers over any entities and open a properties menu for it.
     *
     * @arg Vector eyepos - The eye pos of a player
     * @arg Vector eyevec - The aim vector of a player
     */
    function OnScreenClick(eyepos: Vector, eyevec: Vector): void;

    /**
     * Opens properties menu for given entity.
     *
     * @arg Entity ent - The entity to open menu for
     * @arg object tr - The trace that is passed as second argument to Action callback of a property
     */
    function OpenEntityMenu(ent: Entity, tr: object): void;
}
