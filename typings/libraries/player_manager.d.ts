/** @noSelfInFile */
declare namespace player_manager {
    /**
     * Assigns view model hands to player model.
     *
     * @arg string name - Player model name
     * @arg string model - Hands model
     * @arg number skin - Skin to apply to the hands
     * @arg string bodygroups - Bodygroups to apply to the hands
     */
    function AddValidHands(name: string, model: string, skin: number, bodygroups: string): void;

    /**
     * Associates a simplified name with a path to a valid player model.
     *
     * @arg string name - Simplified name
     * @arg string model - Valid PlayerModel path
     */
    function AddValidModel(name: string, model: string): void;

    /**
     * Returns the entire list of valid player models.
     */
    function AllValidModels(): void;

    /**
     * Clears a player's class association by setting their ClassID to 0
     *
     * @arg Player ply - Player to clear class from
     */
    function ClearPlayerClass(ply: Player): void;

    /**
     * Gets a players class
     *
     * @arg Player ply - Player to get class
     * @return {string} The players class
     */
    function GetPlayerClass(ply: Player): string;

    /**
     * Retrieves a copy of all registered player classes.
     *
     * @return {object} A copy of all registered player classes.
     */
    function GetPlayerClasses(): object;

    /**
     * Applies basic class variables when the player spawns.
     *
     * @arg Player ply - Player to setup
     */
    function OnPlayerSpawn(ply: Player): void;

    /**
     * Register a class metatable to be assigned to players later
     *
     * @arg string name - Class name
     * @arg object table - Class metatable
     * @arg string base - Base class name
     */
    function RegisterClass(name: string, table: object, base: string): void;

    /**
     * Execute a named function within the player's set class
     *
     * @arg Player ply - Player to execute function on.
     * @arg string funcName - Name of function.
     * @arg args[] arguments - Optional arguments. Can be of any type.
     * @return {args[]} The values returned by the called function.
     */
    function RunClass(ply: Player, funcName: string, ...arguments: any[]): any;

    /**
     * Sets a player's class
     *
     * @arg Player ply - Player to set class
     * @arg string classname - Name of class to set
     */
    function SetPlayerClass(ply: Player, classname: string): void;

    /**
 * Retrieves correct hands for given player model. By default returns citizen hands.
* 
* @arg string name - Player model name
* @return {string} A table with following contents:

string model - Model of hands
number skin - Skin of hands
string body - Bodygroups of hands
 */
    function TranslatePlayerHands(name: string): string;

    /**
     * Returns the valid model path for a simplified name.
     *
     * @arg string shortName - The short name of the model.
     * @return {string} The valid model path for the short name.
     */
    function TranslatePlayerModel(shortName: string): string;

    /**
     * Returns the simplified name for a valid model path of a player model.
     *
     * @arg string model - The model path to a player model
     * @return {string} The simplified name for that model
     */
    function TranslateToPlayerModelName(model: string): string;
}
