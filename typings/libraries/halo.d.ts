/** @noSelfInFile */
declare namespace halo {
    /**
     * Applies a halo glow effect to one or multiple entities.
     *
     * @arg object entities - A table of entities to add the halo effect to.
     * @arg Color color - The desired color of the halo. See Color.
     * @arg number [blurX=2] - The strength of the halo's blur on the x axis.
     * @arg number [blurY=2] - The strength of the halo's blur on the y axis.
     * @arg number [passes=1] - The number of times the halo should be drawn per frame. Increasing this may hinder player FPS.
     * @arg boolean [additive=true] - Sets the render mode of the halo to additive.
     * @arg boolean [ignoreZ=false] - Renders the halo through anything when set to true.
     */
    function Add(
        entities: object,
        color: Color,
        blurX?: number,
        blurY?: number,
        passes?: number,
        additive?: boolean,
        ignoreZ?: boolean
    ): void;

    /**
     * Renders a halo according to the specified table, only used internally, called from a GM:PostDrawEffects hook added by the halo library.
     *
     * @arg object entry - Table with info about the halo to draw.
     */
    function Render(entry: object): void;

    /**
     * Returns the entity the halo library is currently rendering the halo for.
     *
     * @return {Entity} If set, the currently rendered entity by the halo library.
     */
    function RenderedEntity(): Entity;
}
