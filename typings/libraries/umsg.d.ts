/** @noSelfInFile */
declare namespace umsg {
    /**
     * Writes an angle to the usermessage.
     *
     * @arg Angle angle - The angle to be sent.
     */
    function Angle(angle: Angle): void;

    /**
     * Writes a bool to the usermessage.
     *
     * @arg boolean bool - The bool to be sent.
     */
    function Bool(bool: boolean): void;

    /**
     * Writes a signed char to the usermessage.
     *
     * @arg number char - The char to be sent.
     */
    function Char(char: number): void;

    /**
     * Dispatches the usermessage to the client(s).
     */
    function End(): void;

    /**
     * Writes an entity object to the usermessage.
     *
     * @arg Entity entity - The entity to be sent.
     */
    function Entity(entity: Entity): void;

    /**
     * Writes a float to the usermessage.
     *
     * @arg number float - The float to be sent.
     */
    function Float(float: number): void;

    /**
     * Writes a signed int (32 bit) to the usermessage.
     *
     * @arg number int - The int to be sent.
     */
    function Long(int: number): void;

    /**
     * The string specified will be networked to the client and receive a identifying number, which will be sent instead of the string to optimize networking.
     *
     * @arg string string - The string to be pooled.
     */
    function PoolString(string: string): void;

    /**
     * Writes a signed short (16 bit) to the usermessage.
     *
     * @arg number short - The short to be sent.
     */
    function Short(short: number): void;

    /**
     * Starts a new usermessage.
     *
     * @arg string name - The name of the message to be sent.
     * @arg Player filter - If passed a player object, it will only be sent to the player, if passed a CRecipientFilter of players, it will be sent to all specified players, if passed nil (or another invalid value), the message will be sent to all players.
     */
    function Start(name: string, filter: Player): void;

    /**
     * Writes a null terminated string to the usermessage.
     *
     * @arg string string - The string to be sent.
     */
    function String(string: string): void;

    /**
     * Writes a Vector to the usermessage.
     *
     * @arg Vector vector - The vector to be sent.
     */
    function Vector(vector: Vector): void;

    /**
     * Writes a vector normal to the usermessage.
     *
     * @arg Vector normal - The vector normal to be sent.
     */
    function VectorNormal(normal: Vector): void;
}
