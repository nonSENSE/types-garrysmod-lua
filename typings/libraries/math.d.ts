/** @noSelfInFile */
declare namespace math {
    /**
     * A variable that effectively represents infinity, in the sense that in any numerical comparison every number will be less than this.
     *
     * @return {number} The effective infinity.
     */
    function huge(): number;

    /**
     * A variable containing the mathematical constant pi. (3.1415926535898)
     *
     * @return {number} The effective infinity.
     */
    function pi(): number;

    /**
     * Calculates the absolute value of a number (effectively removes any negative sign).
     *
     * @arg number x - The number to get the absolute value of.
     * @return {number} The absolute value.
     */
    function abs(x: number): number;

    /**
 * Returns the arccosine of the given number.
* 
* @arg number cos - Cosine value in range of -1 to 1.
* @return {number} An angle in radians, between 0 and pi, which has the given cos value.
nan if the argument is out of range.
 */
    function acos(cos: number): number;

    /**
     * Calculates the difference between two angles.
     *
     * @arg number a - The first angle.
     * @arg number b - The second angle.
     * @return {number} The difference between the angles between -180 and 180
     */
    function AngleDifference(a: number, b: number): number;

    /**
     * Gradually approaches the target value by the specified amount.
     *
     * @arg number current - The value we're currently at.
     * @arg number target - The target value. This function will never overshoot this value.
     * @arg number change - The amount that the current value is allowed to change by to approach the target. (It makes no difference whether this is positive or negative.)
     * @return {number} New current value, closer to the target than it was previously.
     */
    function Approach(current: number, target: number, change: number): number;

    /**
     * Increments an angle towards another by specified rate.
     *
     * @arg number currentAngle - The current angle to increase
     * @arg number targetAngle - The angle to increase towards
     * @arg number rate - The amount to approach the target angle by
     * @return {number} Modified angle
     */
    function ApproachAngle(currentAngle: number, targetAngle: number, rate: number): number;

    /**
 * Returns the arcsine of the given number.
* 
* @arg number normal - Sine value in the range of -1 to 1.
* @return {number} An angle in radians, in the range -pi/2 to pi/2, which has the given sine value.
nan if the argument is out of range.
 */
    function asin(normal: number): number;

    /**
     * Returns the arctangent of the given number.
     *
     * @arg number normal - Tangent value.
     * @return {number} An angle in radians, in the range -pi/2 to pi/2, which has the given tangent.
     */
    function atan(normal: number): number;

    /**
     * functions like math.atan(y / x), except it also takes into account the quadrant of the angle and so doesn't have a limited range of output.
     *
     * @arg number y - Y coordinate.
     * @arg number x - X coordinate.
     * @return {number} The angle of the line from (0, 0) to (x, y) in radians, in the range -pi to pi.
     */
    function atan2(y: number, x: number): number;

    /**
     * Converts a binary string into a number.
     *
     * @arg string string - Binary string to convert
     * @return {number} Base 10 number.
     */
    function BinToInt(string: string): number;

    /**
     * Basic code for  algorithm.
     *
     * @arg number tDiff - From 0 to 1, where alongside the spline the point will be.
     * @arg Vector tPoints - A table of Vectors. The amount cannot be less than 4.
     * @arg number tMax - Just leave this at 1.
     * @return {Vector} Point on Bezier curve, related to tDiff.
     */
    function BSplinePoint(tDiff: number, tPoints: Vector, tMax: number): Vector;

    /**
     * Basic code for Bezier-Spline algorithm, helper function for math.BSplinePoint.
     *
     * @arg number i
     * @arg number k - Sending in a value < 1 will result in an infinite loop.
     * @arg number t
     * @arg number tinc
     * @return {number}
     */
    function calcBSplineN(i: number, k: number, t: number, tinc: number): number;

    /**
     * Ceils or rounds a number up.
     *
     * @arg number number - The number to be rounded up.
     * @return {number} ceiled numbers
     */
    function ceil(number: number): number;

    /**
     * Clamps a number between a minimum and maximum value.
     *
     * @arg number input - The number to clamp.
     * @arg number min - The minimum value, this function will never return a number less than this.
     * @arg number max - The maximum value, this function will never return a number greater than this.
     * @return {number} The clamped value.
     */
    function Clamp(input: number, min: number, max: number): number;

    /**
     * Returns the cosine of given angle.
     *
     * @arg number number - Angle in radians
     * @return {number} Cosine of given angle
     */
    function cos(number: number): number;

    /**
     * Returns the hyperbolic cosine of the given angle.
     *
     * @arg number number - Angle in radians.
     * @return {number} The hyperbolic cosine of the given angle.
     */
    function cosh(number: number): number;

    /**
     * Converts radians to degrees.
     *
     * @arg number radians - Value to be converted to degrees.
     * @return {number} degrees
     */
    function deg(radians: number): number;

    /**
     * Returns the difference between two points in 2D space. Alias of math.Distance.
     *
     * @arg number x1 - X position of first point
     * @arg number y1 - Y position of first point
     * @arg number x2 - X position of second point
     * @arg number y2 - Y position of second point
     * @return {number} Distance between the two points.
     */
    function Dist(x1: number, y1: number, x2: number, y2: number): number;

    /**
     * Returns the difference between two points in 2D space.
     *
     * @arg number x1 - X position of first point
     * @arg number y1 - Y position of first point
     * @arg number x2 - X position of second point
     * @arg number y2 - Y position of second point
     * @return {number} Distance between the two points
     */
    function Distance(x1: number, y1: number, x2: number, y2: number): number;

    /**
     * Returns the squared difference between two points in 2D space. This is computationally faster than math.Distance.
     *
     * @arg number x1 - X position of first point
     * @arg number y1 - Y position of first point
     * @arg number x2 - X position of second point
     * @arg number y2 - Y position of second point
     * @return {number} The squared distance between the two points.
     */
    function DistanceSqr(x1: number, y1: number, x2: number, y2: number): number;

    /**
     * Calculates the progress of a value fraction, taking in to account given easing fractions
     *
     * @arg number progress - Fraction of the progress to ease, from 0 to 1
     * @arg number easeIn - Fraction of how much easing to begin with
     * @arg number easeOut - Fraction of how much easing to end with
     * @return {number} "Eased" Value, from 0 to 1
     */
    function EaseInOut(progress: number, easeIn: number, easeOut: number): number;

    /**
     * Returns the x power of the Euler constant.
     *
     * @arg number exponent - The exponent for the function.
     * @return {number} e to the specified power
     */
    function exp(exponent: number): number;

    /**
     * Floors or rounds a number down.
     *
     * @arg number number - The number to be rounded down.
     * @return {number} floored numbers
     */
    function floor(number: number): number;

    /**
     * Returns the modulus of the specified values.
     *
     * @arg number base - The base value.
     * @arg number modulator - The modulator.
     * @return {number} The calculated modulus.
     */
    function fmod(base: number, modulator: number): number;

    /**
     * Lua reference description: Returns m and e such that x = m2e, e is an integer and the absolute value of m is in the range ((0.5, 1) (or zero when x is zero).
     *
     * @arg number x - The value to get the normalized fraction and the exponent from.
     * @return {number} m, multiplier - between 0.5 and 1.
     * @return {number} e, exponent - always an integer.
     */
    function frexp(x: number): LuaMultiReturn<[number, number]>;

    /**
     * Converts an integer to a binary (base-2) string.
     *
     * @arg number int - Number to be converted.
     * @return {string} Binary number string. The length of this will always be a multiple of 3.
     */
    function IntToBin(int: number): string;

    /**
     * Takes a normalised number and returns the floating point representation.
     *
     * @arg number normalizedFraction - The value to get the normalized fraction and the exponent from.
     * @arg number exponent - The value to get the normalized fraction and the exponent from.
     * @return {number} result
     */
    function ldexp(normalizedFraction: number, exponent: number): number;

    /**
     * With one argument, return the natural logarithm of x (to base e).
     *
     * @arg number x - The value to get the base from exponent from.
     * @arg number [base=NaN] - The logarithmic base.
     * @return {number} Logarithm of x to the given base
     */
    function log(x: number, base?: number): number;

    /**
     * Returns the base-10 logarithm of x. This is usually more accurate than math.log(x, 10).
     *
     * @arg number x - The value to get the base from exponent from.
     * @return {number} The result.
     */
    function log10(x: number): number;

    /**
     * Returns the largest value of all arguments.
     *
     * @arg args[] numbers - Numbers to get the largest from
     * @return {number} The largest number
     */
    function max(...numbers: any[]): number;

    /**
     * Returns the smallest value of all arguments.
     *
     * @arg args[] numbers - Numbers to get the smallest from.
     * @return {number} The smallest number
     */
    function min(...numbers: any[]): number;

    /**
     * Returns the modulus of the specified values. Same as math.fmod.
     *
     * @arg number base - The base value
     * @arg number modulator - Modulator
     * @return {number} The calculated modulus
     */
    function mod(base: number, modulator: number): number;

    /**
     * Returns the integral and fractional component of the modulo operation.
     *
     * @arg number base - The base value.
     * @return {number} The integral component.
     * @return {number} The fractional component.
     */
    function modf(base: number): LuaMultiReturn<[number, number]>;

    /**
     * Normalizes angle, so it returns value between -180 and 180.
     *
     * @arg number angle - The angle to normalize, in degrees.
     * @return {number} The normalized angle, in the range of -180 to 180 degrees.
     */
    function NormalizeAngle(angle: number): number;

    /**
     * Returns x raised to the power y.
     * In particular, math.pow(1.0, x) and math.pow(x, 0.0) always return 1.0, even when x is a zero or a nan. If both x and y are finite, x is negative, and y is not an integer then math.pow(x, y) is undefined.
     *
     * @arg number x - Base.
     * @arg number y - Exponent.
     * @return {number} y power of x
     */
    function pow(x: number, y: number): number;

    /**
     * Converts an angle in degrees to it's equivalent in radians.
     *
     * @arg number degrees - The angle measured in degrees.
     * @return {number} radians
     */
    function rad(degrees: number): number;

    /**
     * Returns a random float between min and max.
     *
     * @arg number min - The minimum value.
     * @arg number max - The maximum value.
     * @return {number} Random float between min and max.
     */
    function Rand(min: number, max: number): number;

    /**
 * When called without arguments, returns a uniform pseudo-random real number in the range 0 to 1 which includes 0 but excludes 1.
* 
* @arg number [m=NaN] - If m is the only parameter: upper limit.
If n is also provided: lower limit.
If provided, this must be an integer.
* @arg number [n=NaN] - Upper limit.
If provided, this must be an integer.
* @return {number} Random value
 */
    function random(m?: number, n?: number): number;

    /**
     * Seeds the random number generator. The same seed will guarantee the same sequence of numbers each time with math.random.
     *
     * @arg number seed - The new seed
     */
    function randomseed(seed: number): void;

    /**
     * Remaps the value from one range to another
     *
     * @arg number value - The value
     * @arg number inMin - The minimum of the initial range
     * @arg number inMax - The maximum of the initial range
     * @arg number outMin - The minimum of new range
     * @arg number outMax - The maximum of new range
     * @return {number} The number in the new range
     */
    function Remap(value: number, inMin: number, inMax: number, outMin: number, outMax: number): number;

    /**
     * Rounds the given value to the nearest whole number or to the given decimal places.
     *
     * @arg number value - The value to round.
     * @arg number [decimals=0] - The decimal places to round to.
     * @return {number} The rounded value.
     */
    function Round(value: number, decimals?: number): number;

    /**
     * Returns the sine of given angle.
     *
     * @arg number number - Angle in radians
     * @return {number} Sine for given angle
     */
    function sin(number: number): number;

    /**
     * Returns the hyperbolic sine of the given angle.
     *
     * @arg number number - Angle in radians.
     * @return {number} The hyperbolic sine of the given angle.
     */
    function sinh(number: number): number;

    /**
     * Returns the square root of the number.
     *
     * @arg number value - Value to get the square root of.
     * @return {number} squareRoot
     */
    function sqrt(value: number): number;

    /**
     * Returns the tangent of the given angle.
     *
     * @arg number value - Angle in radians
     * @return {number} The tangent of the given angle.
     */
    function tan(value: number): number;

    /**
     * Returns the hyperbolic tangent of the given number.
     *
     * @arg number number - Angle in radians.
     * @return {number} The hyperbolic tangent of the given angle.
     */
    function tanh(number: number): number;

    /**
     * Returns the fraction of where the current time is relative to the start and end times
     *
     * @arg number start - Start time in seconds
     * @arg number end - End time in seconds
     * @arg number current - Current time in seconds
     * @return {number} Fraction
     */
    function TimeFraction(start: number, end: number, current: number): number;

    /**
     * Rounds towards zero.
     *
     * @arg number num - The number to truncate
     * @arg number [digits=0] - The amount of digits to keep after the point.
     * @return {number} The result.
     */
    function Truncate(num: number, digits?: number): number;
}
