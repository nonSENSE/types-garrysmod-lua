/** @noSelfInFile */
declare namespace menu {
    /**
     * Used by "Demo to Video" to record the frame.
     */
    function RecordFrame(): void;
}
