/** @noSelfInFile */
declare namespace timer {
    /**
     * Adjusts the timer if the timer with the given identifier exists.
     *
     * @arg any identifier - Identifier of the timer to adjust.
     * @arg number delay - The delay interval in seconds. Must be specified.
     * @arg number repetitions - Repetitions. Use 0 for infinite or nil to keep previous value.
     * @arg GMLua.CallbackNoContext func - The new function. Use nil to keep previous value.
     * @return {boolean} true if succeeded.
     */
    function Adjust(identifier: any, delay: number, repetitions: number, func: GMLua.CallbackNoContext): boolean;

    /**
     * This function does nothing.
     */
    function Check(): void;

    /**
     * Creates a new timer that will repeat its function given amount of times.
     * This function also requires the timer to be named, which allows you to control it after it was created via the timer.
     *
     * @arg string identifier - Identifier of the timer to create. Must be unique. If a timer already exists with the same identifier, that timer will be updated to the new settings and reset.
     * @arg number delay - The delay interval in seconds. If the delay is too small, the timer will fire on the next frame/tick.
     * @arg number repetitions - The number of times to repeat the timer. Enter 0 for infinite repetitions.
     * @arg GMLua.CallbackNoContext func - Function called when timer has finished the countdown.
     */
    function Create(identifier: string, delay: number, repetitions: number, func: GMLua.CallbackNoContext): void;

    /**
     * Stops and destroys the given timer. Alias of timer.Remove.
     *
     * @arg string identifier - Identifier of the timer to destroy.
     */
    function Destroy(identifier: string): void;

    /**
     * Returns whenever the given timer exists or not.
     *
     * @arg string identifier - Identifier of the timer.
     * @return {boolean} Returns true if the timer exists, false if it doesn't
     */
    function Exists(identifier: string): boolean;

    /**
     * Pauses the given timer.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {boolean} false if the timer didn't exist or was already paused, true otherwise.
     */
    function Pause(identifier: any): boolean;

    /**
     * Stops and removes a timer created by timer.Create.
     *
     * @arg string identifier - Identifier of the timer to remove.
     */
    function Remove(identifier: string): void;

    /**
     * Returns amount of repetitions/executions left before the timer destroys itself.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {number} The amount of executions left.
     */
    function RepsLeft(identifier: any): number;

    /**
     * Creates a simple timer that runs the given function after a specified delay.
     *
     * @arg number delay - How long until the function should be ran (in seconds). Use 0 to have the function run in the next GM:Think.
     * @arg GMLua.CallbackNoContext func - The function to run after the specified delay.
     */
    function Simple(delay: number, func: GMLua.CallbackNoContext): void;

    /**
     * Restarts the given timer.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {boolean} true if the timer exists, false if it doesn't.
     */
    function Start(identifier: any): boolean;

    /**
     * Stops the given timer and rewinds it.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {boolean} false if the timer didn't exist or was already stopped, true otherwise.
     */
    function Stop(identifier: any): boolean;

    /**
     * Returns amount of time left (in seconds) before the timer executes its function.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {number} The amount of time left (in seconds).
     */
    function TimeLeft(identifier: any): number;

    /**
     * Runs either timer.Pause or timer.UnPause based on the timer's current status.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {boolean} status of the timer.
     */
    function Toggle(identifier: any): boolean;

    /**
     * Unpauses the timer.
     *
     * @arg any identifier - Identifier of the timer.
     * @return {boolean} false if the timer didn't exist or was already running, true otherwise.
     */
    function UnPause(identifier: any): boolean;
}
