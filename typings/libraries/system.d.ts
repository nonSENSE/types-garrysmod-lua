/** @noSelfInFile */
declare namespace system {
    /**
     * Returns the total uptime of the current application as reported by Steam.
     *
     * @return {number} Seconds of game uptime as an integer.
     */
    function AppTime(): number;

    /**
 * Returns the current battery power.
* 
* @return {number} 0-100 if a battery (laptop, UPS, etc) is present.
Will instead return 255 if plugged in without a battery.
 */
    function BatteryPower(): number;

    /**
     * Flashes the window, turning the border to white briefly
     */
    function FlashWindow(): void;

    /**
     * Returns the country code of this computer, determined by the localisation settings of the OS.
     *
     * @return {string} Two-letter country code, using ISO 3166-1 standard.
     */
    function GetCountry(): string;

    /**
     * Returns whether or not the game window has focus.
     *
     * @return {boolean} Whether or not the game window has focus.
     */
    function HasFocus(): boolean;

    /**
     * Returns whether the current OS is Linux.
     *
     * @return {boolean} Whether or not the game is running on Linux.
     */
    function IsLinux(): boolean;

    /**
     * Returns whether the current OS is OSX.
     *
     * @return {boolean} Whether or not the game is running on OSX.
     */
    function IsOSX(): boolean;

    /**
     * Returns whether the game is being run in a window or in fullscreen (you can change this by opening the menu, clicking 'Options', then clicking the 'Video' tab, and changing the Display Mode using the dropdown menu):
     *
     * @return {boolean} Is the game running in a window?
     */
    function IsWindowed(): boolean;

    /**
     * Returns whether the current OS is Windows.
     *
     * @return {boolean} Whether the system the game runs on is Windows or not.
     */
    function IsWindows(): boolean;

    /**
     * Returns the synchronized Steam time. This is the number of seconds since the Unix epoch.
     *
     * @return {number} Current Steam-synchronized Unix time.
     */
    function SteamTime(): number;

    /**
     * Returns the amount of seconds since the Steam user last moved their mouse.
     *
     * @return {number} The amount of seconds since the Steam user last moved their mouse.
     */
    function UpTime(): number;
}
