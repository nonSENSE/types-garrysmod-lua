/** @noSelfInFile */
declare namespace notification {
    /**
     * Adds a standard notification to your screen.
     *
     * @arg string text - The text to display.
     * @arg number type - Determines the notification method (e.g. icon) for displaying the notification. See the Enums/NOTIFY.
     * @arg number length - The number of seconds to display the notification for.
     */
    function AddLegacy(text: string, type: number, length: number): void;

    /**
     * Adds a notification with an animated progress bar.
     *
     * @arg any id - Can be any type. It's used as an index.
     * @arg string strText - The text to show
     * @arg number [frac=NaN] - If set, overrides the progress bar animation with given percentage. Range is 0 to 1.
     */
    function AddProgress(id: any, strText: string, frac?: number): void;

    /**
     * Removes the notification after 0.8 seconds.
     *
     * @arg any uid - The unique ID of the notification
     */
    function Kill(uid: any): void;
}
