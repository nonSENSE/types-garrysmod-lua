/** @noSelfInFile */
declare namespace frame_blend {
    /**
     * Adds a frame to the blend. Calls frame_blend.CompleteFrame once enough frames have passed since last frame_blend.CompleteFrame call.
     */
    function AddFrame(): void;

    /**
     * Blends the frame(s).
     */
    function BlendFrame(): void;

    /**
     * Renders the frame onto internal render target.
     */
    function CompleteFrame(): void;

    /**
     * Actually draws the frame blend effect.
     */
    function DrawPreview(): void;

    /**
     * Returns whether frame blend post processing effect is enabled or not.
     *
     * @return {boolean} Is frame blend enabled or not
     */
    function IsActive(): boolean;

    /**
     * Returns whether the current frame is the last frame?
     *
     * @return {boolean} Whether the current frame is the last frame?
     */
    function IsLastFrame(): boolean;

    /**
     * Returns amount of frames needed to render?
     *
     * @return {number} Amount of frames needed to render?
     */
    function RenderableFrames(): number;

    /**
     * Returns whether we should skip frame or not
     *
     * @return {boolean} Should the frame be skipped or not
     */
    function ShouldSkipFrame(): boolean;
}
