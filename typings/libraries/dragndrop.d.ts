/** @noSelfInFile */
declare namespace dragndrop {
    /**
     * Calls the receiver function of hovered panel.
     *
     * @arg boolean bDoDrop - true if the mouse was released, false if we right clicked.
     * @arg number command - The command value. This should be the ID of the clicked dropdown menu ( if right clicked, or nil )
     * @arg number mx - The local to the panel mouse cursor X position when the click happened.
     * @arg number my - The local to the panel  mouse cursor Y position when the click happened.
     */
    function CallReceiverFunction(bDoDrop: boolean, command: number, mx: number, my: number): void;

    /**
     * Clears all the internal drag'n'drop variables.
     */
    function Clear(): void;

    /**
     * Handles the drop action of drag'n'drop library.
     */
    function Drop(): void;

    /**
     * Returns a table of currently dragged panels.
     *
     * @arg string [name="nil"] - If set, the function will return only the panels with this Panel:Droppable name.
     * @return {object} A table of all panels that are being currently dragged, if any.
     */
    function GetDroppable(name?: string): object;

    /**
     * If returns true, calls dragndrop.StopDragging in dragndrop.Drop. Seems to be broken and does nothing. Is it for override?
     */
    function HandleDroppedInGame(): void;

    /**
     * Handles the hover think. Called from dragndrop.Think.
     */
    function HoverThink(): void;

    /**
     * Returns whether the user is dragging something with the drag'n'drop system.
     *
     * @return {boolean} True if the user is dragging something with the drag'n'drop system.
     */
    function IsDragging(): boolean;

    /**
     * Starts the drag'n'drop.
     */
    function StartDragging(): void;

    /**
     * Stops the drag'n'drop and calls dragndrop.Clear.
     */
    function StopDragging(): void;

    /**
     * Handles all the drag'n'drop processes. Calls dragndrop.UpdateReceiver and dragndrop.HoverThink.
     */
    function Think(): void;

    /**
     * Updates the receiver to drop the panels onto. Called from dragndrop.Think.
     */
    function UpdateReceiver(): void;
}
