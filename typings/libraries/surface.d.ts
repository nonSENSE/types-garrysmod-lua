/** @noSelfInFile */
declare namespace surface {
    /**
     * Creates a new font.
     *
     * @arg string fontName - The new font name.
     * @arg FontDataStruct fontData - The font properties. See the Structures/FontData.
     */
    function CreateFont(fontName: string, fontData: FontDataStruct): void;

    /**
     * Enables or disables the clipping used by the VGUI that limits the drawing operations to a panels bounds.
     *
     * @arg boolean disable - True to disable, false to enable the clipping
     */
    function DisableClipping(disable: boolean): void;

    /**
     * Draws a hollow circle, made of lines. For a filled circle, see examples for surface.DrawPoly.
     *
     * @arg number originX - The center x integer coordinate.
     * @arg number originY - The center y integer coordinate.
     * @arg number radius - The radius of the circle.
     * @arg number r - The red value of the color to draw the circle with, or a Color.
     * @arg number g - The green value of the color to draw the circle with. Unused if a Color was given.
     * @arg number b - The blue value of the color to draw the circle with. Unused if a Color was given.
     * @arg number [a=255] - The alpha value of the color to draw the circle with. Unused if a Color was given.
     */
    function DrawCircle(
        originX: number,
        originY: number,
        radius: number,
        r: number,
        g: number,
        b: number,
        a?: number
    ): void;

    /**
     * Draws a line from one point to another.
     *
     * @arg number startX - The start x integer coordinate.
     * @arg number startY - The start y integer coordinate.
     * @arg number endX - The end x integer coordinate.
     * @arg number endY - The end y integer coordinate.
     */
    function DrawLine(startX: number, startY: number, endX: number, endY: number): void;

    /**
     * Draws a hollow box with a given border width.
     *
     * @arg number x - The start x integer coordinate.
     * @arg number y - The start y integer coordinate.
     * @arg number w - The integer width.
     * @arg number h - The integer height.
     * @arg number thickness - The thickness of the outlined box border.
     */
    function DrawOutlinedRect(x: number, y: number, w: number, h: number, thickness: number): void;

    /**
 * Draws a textured polygon (secretly a triangle fan) with a maximum of 4096 vertices.
* Only works properly with convex polygons. You may try to render concave polygons, but there is no guarantee that things wont get messed up.
* 
* @arg PolygonVertexStruct vertices - A table containing integer vertices. See the Structures/PolygonVertex.
The vertices must be in clockwise order.
 */
    function DrawPoly(vertices: PolygonVertexStruct): void;

    /**
     * Draws a solid rectangle on the screen.
     *
     * @arg number x - The X integer co-ordinate.
     * @arg number y - The Y integer co-ordinate.
     * @arg number width - The integer width of the rectangle.
     * @arg number height - The integer height of the rectangle.
     */
    function DrawRect(x: number, y: number, width: number, height: number): void;

    /**
     * Draw the specified text on the screen, using the previously set position, font and color.
     *
     * @arg string text - The text to be rendered.
     * @arg boolean [forceAdditive=false] - true to force text to render additive, false to force not additive, nil to use font's value.
     */
    function DrawText(text: string, forceAdditive?: boolean): void;

    /**
     * Draw a textured rectangle with the given position and dimensions on the screen, using the current active texture set with surface.SetMaterial. It is also affected by surface.SetDrawColor.
     *
     * @arg number x - The X integer co-ordinate.
     * @arg number y - The Y integer co-ordinate.
     * @arg number width - The integer width of the rectangle.
     * @arg number height - The integer height of the rectangle.
     */
    function DrawTexturedRect(x: number, y: number, width: number, height: number): void;

    /**
     * Draw a textured rotated rectangle with the given position and dimensions and angle on the screen, using the current active texture.
     *
     * @arg number x - The X integer co-ordinate, representing the center of the rectangle.
     * @arg number y - The Y integer co-ordinate, representing the center of the rectangle.
     * @arg number width - The integer width of the rectangle.
     * @arg number height - The integer height of the rectangle.
     * @arg number rotation - The rotation of the rectangle, in degrees.
     */
    function DrawTexturedRectRotated(x: number, y: number, width: number, height: number, rotation: number): void;

    /**
     * Draws a textured rectangle with a repeated or partial texture.
     *
     * @arg number x - The X integer coordinate.
     * @arg number y - The Y integer coordinate.
     * @arg number width - The integer width of the rectangle.
     * @arg number height - The integer height of the rectangle.
     * @arg number startU - The U texture mapping of the rectangle origin.
     * @arg number startV - The V texture mapping of the rectangle origin.
     * @arg number endU - The U texture mapping of the rectangle end.
     * @arg number endV - The V texture mapping of the rectangle end.
     */
    function DrawTexturedRectUV(
        x: number,
        y: number,
        width: number,
        height: number,
        startU: number,
        startV: number,
        endU: number,
        endV: number
    ): void;

    /**
     * Returns the current alpha multiplier affecting drawing operations. This is set by surface.SetAlphaMultiplier or by the game engine in certain other cases.
     *
     * @return {number} The multiplier ranging from 0 to 1.
     */
    function GetAlphaMultiplier(): number;

    /**
     * Returns the current color affecting draw operations.
     *
     * @return {Color} The color that drawing operations will use as a Color.
     */
    function GetDrawColor(): Color;

    /**
     * Gets the HUD icon TextureID with the specified name.
     *
     * @arg string name - The name of the texture.
     * @return {number}
     */
    function GetHUDTexture(name: string): number;

    /**
     * Returns the current color affecting text draw operations.
     *
     * @return {Color} The color that text drawing operations will use as a Color.
     */
    function GetTextColor(): Color;

    /**
     * Returns the width and height (in pixels) of the given text, but only if the font has been set with surface.SetFont.
     *
     * @arg string text - The string to check the size of.
     * @return {number} Width of the provided text.
     * @return {number} Height of the provided text.
     */
    function GetTextSize(text: string): LuaMultiReturn<[number, number]>;

    /**
     * Returns the texture id of the material with the given name/path.
     *
     * @arg string name_path - Name or path of the texture.
     * @return {number} The texture ID
     */
    function GetTextureID(name_path: string): number;

    /**
     * Returns the size of the texture with the associated texture ID.
     *
     * @arg number textureID - The texture ID, returned by surface.GetTextureID.
     * @return {number} The texture width.
     * @return {number} The texture height.
     */
    function GetTextureSize(textureID: number): LuaMultiReturn<[number, number]>;

    /**
     * Play a sound file directly on the client (such as UI sounds, etc).
     *
     * @arg string soundfile - The path to the sound file, which must be relative to the sound/ folder.
     */
    function PlaySound(soundfile: string): void;

    /**
     * Returns the height of the current client's screen.
     *
     * @return {number} screenHeight
     */
    function ScreenHeight(): number;

    /**
     * Returns the width of the current client's screen.
     *
     * @return {number} screenWidth
     */
    function ScreenWidth(): number;

    /**
     * Sets the alpha multiplier that will influence all upcoming drawing operations.
     * See also render.SetBlend.
     *
     * @arg number multiplier - The multiplier ranging from 0 to 1.
     */
    function SetAlphaMultiplier(multiplier: number): void;

    /**
     * Set the color of any future shapes to be drawn, can be set by either using R, G, B, A as separate values or by a Color. Using a color structure is not recommended to be created procedurally.
     *
     * @arg number r - The red value of color, or a Color.
     * @arg number g - The green value of color. Unused if a Color was given.
     * @arg number b - The blue value of color. Unused if a Color was given.
     * @arg number [a=255] - The alpha value of color. Unused if a Color was given.
     */
    function SetDrawColor(r: number, g: number, b: number, a?: number): void;

    /**
     * Set the current font to be used for text operations later.
     *
     * @arg string fontName - The name of the font to use.
     */
    function SetFont(fontName: string): void;

    /**
     * Sets the material to be used in all upcoming draw operations using the surface library.
     *
     * @arg IMaterial material - The material to be used.
     */
    function SetMaterial(material: IMaterial): void;

    /**
     * Set the color of any future text to be drawn, can be set by either using R, G, B, A as separate numbers or by a Color.
     *
     * @arg number r - The red value of color, or a Color.
     * @arg number g - The green value of color
     * @arg number b - The blue value of color
     * @arg number [a=255] - The alpha value of color
     */
    function SetTextColor(r: number, g: number, b: number, a?: number): void;

    /**
     * Set the top-left position to draw any future text at.
     *
     * @arg number x - The X integer co-ordinate.
     * @arg number y - The Y integer co-ordinate.
     */
    function SetTextPos(x: number, y: number): void;

    /**
     * Sets the texture to be used in all upcoming draw operations using the surface library.
     *
     * @arg number textureID - The ID of the texture to draw with returned by surface.GetTextureID.
     */
    function SetTexture(textureID: number): void;
}
