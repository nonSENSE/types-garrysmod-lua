/** @noSelfInFile */
declare namespace physenv {
    /**
     * Adds surface properties to the game's physics environment.
     *
     * @arg string properties - The properties to add. Each one should include "base" or the game will crash due to some values being missing.
     */
    function AddSurfaceData(properties: string): void;

    /**
     * Returns the air density.
     *
     * @return {number} airDensity
     */
    function GetAirDensity(): number;

    /**
     * Gets the global gravity.
     *
     * @return {Vector} gravity
     */
    function GetGravity(): Vector;

    /**
     * Gets the current performance settings in table form.
     *
     * @return {PhysEnvPerformanceSettingsStruct} Performance settings. See Structures/PhysEnvPerformanceSettings
     */
    function GetPerformanceSettings(): PhysEnvPerformanceSettingsStruct;

    /**
     * Sets the air density.
     *
     * @arg number airDensity - The new air density.
     */
    function SetAirDensity(airDensity: number): void;

    /**
     * Sets the directional gravity, does not work on players.
     *
     * @arg Vector gravity - The new gravity.
     */
    function SetGravity(gravity: Vector): void;

    /**
     * Sets the performance settings.
     *
     * @arg PhysEnvPerformanceSettingsStruct performanceSettings - The new performance settings. See Structures/PhysEnvPerformanceSettings
     */
    function SetPerformanceSettings(performanceSettings: PhysEnvPerformanceSettingsStruct): void;
}
