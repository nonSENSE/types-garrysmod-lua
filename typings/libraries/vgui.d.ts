/** @noSelfInFile */
declare namespace vgui {
    /**
     * Creates a panel by the specified classname.
     *
     * @arg string classname - Classname of the panel to create. Valid classnames are listed at: VGUI Element List.
     * @arg Panel [parent=nil] - Parent of the created panel.
     * @arg string [name="nil"] - Name of the created panel.
     * @return {Panel} panel
     */
    function Create(classname: string, parent?: Panel, name?: string): Panel;

    /**
     * Creates a panel from table.
     *
     * @arg object metatable - Your PANEL table
     * @arg Panel [parent=nil] - Which panel to parent the newly created panel to
     * @arg string [name="nil"] - Name of your panel
     * @return {Panel} Created panel
     */
    function CreateFromTable(metatable: object, parent?: Panel, name?: string): Panel;

    /**
     * Creates an engine panel.
     *
     * @arg string _class - Class of the panel to create
     * @arg Panel [parent=nil] - If specified, parents created panel to given one
     * @arg string [name="nil"] - Name of the created panel
     * @return {Panel} Created panel
     */
    function CreateX(_class: string, parent?: Panel, name?: string): Panel;

    /**
     * Returns whenever the cursor is currently active and visible.
     *
     * @return {boolean} isCursorVisible
     */
    function CursorVisible(): boolean;

    /**
     * Returns whether the currently focused panel is a child of the given one.
     *
     * @arg Panel parent - The parent panel to check the currently focused one against. This doesn't need to be a direct parent (focused panel can be a child of a child and so on).
     * @return {boolean} Whether or not the focused panel is a child of the passed one.
     */
    function FocusedHasParent(parent: Panel): boolean;

    /**
     * Gets the method table of this panel. Does not return parent methods!
     *
     * @arg string Panelname - The name of the panel
     * @return {object} methods
     */
    function GetControlTable(Panelname: string): object;

    /**
     * Returns the panel the cursor is hovering above.
     *
     * @return {Panel} The panel that the user is currently hovering over with their cursor.
     */
    function GetHoveredPanel(): Panel;

    /**
     * Returns the panel which is currently receiving keyboard input.
     *
     * @return {Panel} The panel with keyboard focus
     */
    function GetKeyboardFocus(): Panel;

    /**
     * Returns the global world panel which is the parent to all others, except for the HUD panel.
     *
     * @return {Panel} The world panel
     */
    function GetWorldPanel(): Panel;

    /**
     * Returns whenever the cursor is hovering the world panel.
     *
     * @return {boolean} isHoveringWorld
     */
    function IsHoveringWorld(): boolean;

    /**
     * Registers a panel for later creation.
     *
     * @arg string classname - Classname of the panel to create.
     * @arg object panelTable - The table containg the panel information.
     * @arg string [baseName="Panel"] - Name of the base of the panel.
     * @return {object} The given panel table from second argument
     */
    function Register(classname: string, panelTable: object, baseName?: string): object;

    /**
     * Registers a new VGUI panel from a file.
     *
     * @arg string file - The file to register
     * @return {any} A table containing info about the panel. Can be supplied to vgui.CreateFromTable
     */
    function RegisterFile(file: string): any;

    /**
     * Registers a table to use as a panel. All this function does is assigns Base key to your table and returns the table.
     *
     * @arg object panel - The PANEL table
     * @arg string [base="Panel"] - A base for the panel
     * @return {object} The PANEL table
     */
    function RegisterTable(panel: object, base?: string): object;
}
