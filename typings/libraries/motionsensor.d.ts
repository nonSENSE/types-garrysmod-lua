/** @noSelfInFile */
declare namespace motionsensor {
    /**
     *
     *
     * @arg object translator
     * @arg Player player
     * @arg Angle rotation
     * @return {Vector} Pos
     * @return {Angle} ang
     * @return {object} sensor
     */
    function BuildSkeleton(
        translator: object,
        player: Player,
        rotation: Angle
    ): LuaMultiReturn<[Vector, Angle, object]>;

    /**
     *
     *
     * @arg Entity ent - Entity to choose builder for
     * @return {string} Chosen builder
     */
    function ChooseBuilderFromEntity(ent: Entity): string;

    /**
     * Returns the depth map material.
     *
     * @return {IMaterial} The material
     */
    function GetColourMaterial(): IMaterial;

    /**
     *
     */
    function GetSkeleton(): void;

    /**
     * Return whether a kinect is connected - and active (ie - Start has been called).
     *
     * @return {boolean} Connected and active or not
     */
    function IsActive(): boolean;

    /**
     * Returns true if we have detected that there's a kinect connected to the PC
     *
     * @return {boolean} Connected or not
     */
    function IsAvailable(): boolean;

    /**
     *
     *
     * @arg object translator
     * @arg object sensor
     * @arg Vector pos
     * @arg Angle ang
     * @arg object special_vectors
     * @arg number boneid
     * @arg object v
     * @return {boolean} Return nil on failure
     */
    function ProcessAngle(
        translator: object,
        sensor: object,
        pos: Vector,
        ang: Angle,
        special_vectors: object,
        boneid: number,
        v: object
    ): boolean;

    /**
     *
     *
     * @arg object translator
     * @arg object sensor
     * @arg Vector pos
     * @arg Angle rotation
     * @return {Angle} Ang. If !translator.AnglesTable then return - {}
     */
    function ProcessAnglesTable(translator: object, sensor: object, pos: Vector, rotation: Angle): Angle;

    /**
     *
     *
     * @arg object translator
     * @arg object sensor
     * @return {Vector} Pos. if !translator.PositionTable then return - {}
     */
    function ProcessPositionTable(translator: object, sensor: object): Vector;

    /**
     * This starts access to the kinect sensor. Note that this usually freezes the game for a couple of seconds.
     *
     * @return {boolean} true if the access has been started
     */
    function Start(): boolean;

    /**
     * Stops the motion capture.
     */
    function Stop(): void;
}
