/** @noSelfInFile */
declare namespace sql {
    /**
     * Tells the engine a set of queries is coming. Will wait until sql.Commit is called to run them.
     * This is most useful when you run more than 100+ queries.
     * This is equivalent to :
     */
    function Begin(): void;

    /**
     * Tells the engine to execute a series of queries queued for execution, must be preceded by sql.Begin.
     */
    function Commit(): void;

    /**
     * Returns true if the index with the specified name exists.
     *
     * @arg string indexName - The name of the index to check.
     * @return {boolean} exists
     */
    function IndexExists(indexName: string): boolean;

    /**
     * Returns the last error from a SQLite query.
     *
     * @return {string} Last error from SQLite database.
     */
    function LastError(): string;

    /**
     * Performs a query on the local SQLite database, returns a table as result set, nil if result is empty and false on error.
     *
     * @arg string query - The query to execute.
     * @return {object} false is returned if there is an error, nil if the query returned no data.
     */
    function Query(query: string): object;

    /**
     * Performs the query like sql.Query, but returns the first row found.
     *
     * @arg string query - The input query.
     * @arg number [row=1] - The row number. Say we receive back 5 rows, putting 3 as this argument will give us row #3.
     * @return {object} The returned row.
     */
    function QueryRow(query: string, row?: number): object;

    /**
     * Performs the query like sql.QueryRow, but returns the first value found.
     *
     * @arg string query - The input query.
     * @return {string} The returned value.
     */
    function QueryValue(query: string): string;

    /**
     * Escapes dangerous characters and symbols from user input used in an SQLite SQL Query.
     *
     * @arg string string - The string to be escaped.
     * @arg boolean [bNoQuotes=false] - Set this as true, and the function will not wrap the input string in apostrophes.
     * @return {string} The escaped input.
     */
    function SQLStr(string: string, bNoQuotes?: boolean): string;

    /**
     * Returns true if the table with the specified name exists.
     *
     * @arg string tableName - The name of the table to check.
     * @return {boolean} exists
     */
    function TableExists(tableName: string): boolean;
}
