/** @noSelfInFile */
declare namespace video {
    /**
     * Attempts to create an IVideoWriter.
     *
     * @arg VideoDataStruct config - The video config. See Structures/VideoData.
     * @return {IVideoWriter} The video object (returns false if there is an error)
     * @return {string} The error string, if there is an error
     */
    function Record(config: VideoDataStruct): LuaMultiReturn<[IVideoWriter, string]>;
}
