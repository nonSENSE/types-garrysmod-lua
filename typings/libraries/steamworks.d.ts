/** @noSelfInFile */
declare namespace steamworks {
    /**
     * Refreshes clients addons.
     */
    function ApplyAddons(): void;

    /**
 * Downloads a file from the supplied addon and saves it as a .cache file in garrysmod/cache folder.
* 
* @arg string workshopPreviewID - The Preview ID of workshop item.
* @arg boolean uncompress - Whether to uncompress the file or not, assuming it was compressed with LZMA.
You will usually want to set this to true.
* @arg GMLua.CallbackNoContext resultCallback - The function to process retrieved data. The first and only argument is a string, containing path to the saved file.
 */
    function Download(workshopPreviewID: string, uncompress: boolean, resultCallback: GMLua.CallbackNoContext): void;

    /**
 * Downloads a Steam Workshop file by its ID and returns a path to it.
* 
* @arg string workshopID - The ID of workshop item to download. NOT a file ID.
* @arg GMLua.CallbackNoContext resultCallback - The function to process retrieved data. Arguments passed are:

string path - Contains a path to the saved file, or nil if the download failed for any reason.
file_class file - A file object pointing to the downloaded .gma file. The file handle will be closed after the function exits.
 */
    function DownloadUGC(workshopID: string, resultCallback: GMLua.CallbackNoContext): void;

    /**
 * Retrieves info about supplied Steam Workshop addon.
* 
* @arg string workshopItemID - The ID of Steam Workshop item.
* @arg GMLua.CallbackNoContext resultCallback - The function to process retrieved data, with the following arguments:

table data - The data about the item, if the request succeeded, nil otherwise. See Structures/UGCFileInfo.
 */
    function FileInfo(workshopItemID: string, resultCallback: GMLua.CallbackNoContext): void;

    /**
 * Retrieves a customized list of Steam Workshop addons.
* 
* @arg string type - The type of items to retrieve. Possible values include:

popular (All invalid options will equal to this)
trending
latest
friends
followed - Items of people the player is following on Steam
friend_favs - Favorites of player's friends
favorite - Player's favorites
* @arg object tags - A table of tags to match.
* @arg number offset - How much of results to skip from first one. Mainly used for pages.
* @arg number numRetrieve - How much items to retrieve, up to 50 at a time.
* @arg number days - When getting Most Popular content from Steam, this determines a time period. ( 7 = most popular addons in last 7 days, 1 = most popular addons today, etc )
* @arg string userID - "0" to retrieve all addons, "1" to retrieve addons only published by you, or a valid SteamID64 of a user to get workshop items of.
* @arg GMLua.CallbackNoContext resultCallback - The function to process retrieved data. The first and only argument is a table, containing all the info, or nil in case of error
 */
    function GetList(
        type: string,
        tags: object,
        offset: number,
        numRetrieve: number,
        days: number,
        userID: string,
        resultCallback: GMLua.CallbackNoContext
    ): void;

    /**
     * Retrieves players name by his 64bit SteamID.
     *
     * @arg string steamID64 - The 64bit Steam ID ( aka Community ID ) of the player
     * @return {string} The name of that player
     */
    function GetPlayerName(steamID64: string): string;

    /**
     * Returns whenever the client is subscribed to the specified Steam Workshop item.
     *
     * @arg string workshopItemID - The ID of the Steam Workshop item.
     * @return {boolean} Is the client subscribed to the addon or not.
     */
    function IsSubscribed(workshopItemID: string): boolean;

    /**
     * Opens the workshop website in the steam overlay browser.
     */
    function OpenWorkshop(): void;

    /**
     * Publishes dupes, saves or demos to workshop.
     *
     * @arg object tags - The workshop tags to apply
     * @arg string filename - Path to the file to upload
     * @arg string image - Path to the image to use as icon
     * @arg string name - Name of the Workshop submission
     * @arg string desc - Description of the Workshop submission
     */
    function Publish(tags: object, filename: string, image: string, name: string, desc: string): void;

    /**
     * Requests information of the player with SteamID64 for later use with steamworks.GetPlayerName.
     *
     * @arg string steamID64 - The 64bit Steam ID of player.
     * @arg GMLua.CallbackNoContext callback - A callback function with only 1 argument - string name.
     */
    function RequestPlayerInfo(steamID64: string, callback: GMLua.CallbackNoContext): void;

    /**
     *
     *
     * @arg string workshopid - The Steam Workshop item id
     * @return {string} Whatever you have put in as first argument
     */
    function SetFileCompleted(workshopid: string): string;

    /**
     * Sets whether you have played this addon or not. This will be shown to the user in the Steam Workshop itself:
     *
     *
     * @arg string workshopid - The Steam Workshop item ID
     * @return {string} Whatever you have put in as first argument
     */
    function SetFilePlayed(workshopid: string): string;

    /**
     * Sets if an addon should be enabled or disabled. Call steamworks.ApplyAddons afterwards to update.
     *
     * @arg string workshopItemID - The ID of the Steam Workshop item we should enable/disable
     * @arg boolean shouldMount - true to enable the item, false to disable.
     */
    function SetShouldMountAddon(workshopItemID: string, shouldMount: boolean): void;

    /**
     * Returns whenever the specified Steam Workshop addon will be mounted or not.
     *
     * @arg string workshopItemID - The ID of the Steam Workshop
     * @return {boolean} Will the workshop item be mounted or not
     */
    function ShouldMountAddon(workshopItemID: string): boolean;

    /**
     * Subscribes to the specified workshop addon. Call steamworks.ApplyAddons afterwards to update.
     *
     * @arg string workshopItemID - The ID of the Steam Workshop item we should subscribe to
     */
    function Subscribe(workshopItemID: string): void;

    /**
     * Unsubscribes to the specified workshop addon. Call steamworks.ApplyAddons afterwards to update.
     *
     * @arg string workshopItemID - The ID of the Steam Workshop item we should unsubscribe from.
     */
    function Unsubscribe(workshopItemID: string): void;

    /**
     * Opens the workshop website for specified Steam Workshop item in the Steam overlay browser.
     *
     * @arg string workshopItemID - The ID of workshop item.
     */
    function ViewFile(workshopItemID: string): void;

    /**
     * Makes the user vote for the specified addon
     *
     * @arg string workshopItemID - The ID of workshop item.
     * @arg boolean upOrDown - Sets if the user should vote up/down. True makes them upvote, false down
     */
    function Vote(workshopItemID: string, upOrDown: boolean): void;

    /**
     * Retrieves vote info of supplied addon.
     *
     * @arg string workshopItemID - The ID of workshop item.
     * @arg GMLua.CallbackNoContext resultCallback - The function to process retrieved data. The first and only argument is a table, containing all the info.
     */
    function VoteInfo(workshopItemID: string, resultCallback: GMLua.CallbackNoContext): void;
}
