/** @noSelfInFile */
declare namespace utf8 {
    /**
     * Receives zero or more integers, converts each one to its corresponding UTF-8 byte sequence and returns a string with the concatenation of all these sequences.
     *
     * @arg args[] codepoints - Unicode code points to be converted in to a UTF-8 string.
     * @return {string} UTF-8 string generated from given arguments.
     */
    function char(...codepoints: any[]): string;

    /**
     * Returns the codepoints (as numbers) from all characters in the given string that start between byte position startPos and endPos. It raises an error if it meets any invalid byte sequence. This functions similarly to string.byte.
     *
     * @arg string string - The string that you will get the code(s) from.
     * @arg number [startPos=1] - The starting byte of the string to get the codepoint of.
     * @arg number [endPos=1] - The ending byte of the string to get the codepoint of.
     * @return {args[]} The codepoint number(s).
     */
    function codepoint(string: string, startPos?: number, endPos?: number): any;

    /**
     * Returns an iterator (like string.gmatch) which returns both the position and codepoint of each utf8 character in the string. It raises an error if it meets any invalid byte sequence.
     *
     * @arg string string - The string that you will get the codes from.
     * @return {Function} The iterator (to be used in a for loop).
     */
    function codes(string: string): Function;

    /**
     * Forces a string to contain only valid UTF-8 data. Invalid sequences are replaced with U+FFFD (the Unicode replacement character).
     *
     * @arg string string - The string that will become a valid UTF-8 string.
     * @return {string} The UTF-8 string.
     */
    function force(string: string): string;

    /**
     * A UTF-8 compatible version of string.GetChar.
     *
     * @arg string str - The string that you will be searching with the supplied index.
     * @arg number index - The index's value of the string to be returned.
     * @return {string} str
     */
    function GetChar(str: string, index: number): string;

    /**
     * Returns the number of UTF-8 sequences in the given string between positions startPos and endPos (both inclusive). If it finds any invalid UTF-8 byte sequence, returns false as well as the position of the first invalid byte.
     *
     * @arg string string - The string to calculate the length of.
     * @arg number [startPos=1] - The starting position to get the length from.
     * @arg number [endPos=-1] - The ending position to get the length from.
     * @return {number} The number of UTF-8 characters in the string. If there are invalid bytes, this will be false.
     * @return {number} The position of the first invalid byte. If there were no invalid bytes, this will be nil.
     */
    function len(string: string, startPos?: number, endPos?: number): LuaMultiReturn<[number, number]>;

    /**
     * Returns the byte-index of the n'th UTF-8-character after the given startPos (nil if none). startPos defaults to 1 when n is positive and -1 when n is negative. If n is zero, this function instead returns the byte-index of the UTF-8-character startPos lies within.
     *
     * @arg string string - The string that you will get the byte position from.
     * @arg number n - The position to get the beginning byte position from.
     * @arg number [startPos=1] - The offset for n.
     * @return {number} Starting byte-index of the given position.
     */
    function offset(string: string, n: number, startPos?: number): number;

    /**
     * A UTF-8 compatible version of string.sub.
     *
     * @arg string string - The string you'll take a sub-string out of.
     * @arg number StartPos - The position of the first character that will be included in the sub-string.
     * @arg number [EndPos=NaN] - The position of the last character to be included in the sub-string. It can be negative to count from the end.
     * @return {string} The substring.
     */
    function sub(string: string, StartPos: number, EndPos?: number): string;
}
