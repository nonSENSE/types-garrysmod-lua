/** @noSelfInFile */
declare namespace string {
    /**
     * Returns the given string's characters in their numeric ASCII representation.
     *
     * @arg string string - The string to get the chars from.
     * @arg number [startPos=1] - The first character of the string to get the byte of.
     * @arg number [endPos=NaN] - The last character of the string to get the byte of.
     * @return {args[]} Numerical bytes
     */
    function byte(string: string, startPos?: number, endPos?: number): any;

    /**
     * Takes the given numerical bytes and converts them to a string.
     *
     * @arg args[] bytes - The bytes to create the string from.
     * @return {string} String built from given bytes
     */
    function char(...bytes: any[]): string;

    /**
     * Inserts commas for every third digit.
     *
     * @arg number InputNumber - The input number to commafy
     * @return {string} Prettystring
     */
    function Comma(InputNumber: number): string;

    /**
     * Returns the binary bytecode of the given function.
     *
     * @arg GMLua.CallbackNoContext func - The function to get the bytecode of
     * @arg boolean [stripDebugInfo=false] - True to strip the debug data, false to keep it
     * @return {string} Bytecode
     */
    function dump(func: GMLua.CallbackNoContext, stripDebugInfo?: boolean): string;

    /**
     * Returns whether or not the second passed string matches the end of the first.
     *
     * @arg string str - The string whose end is to be checked.
     * @arg string end - The string to be matched with the end of the first.
     * @return {boolean} true if the first string ends with the second, or the second is empty, otherwise false.
     */
    function EndsWith(str: string, end: string): boolean;

    /**
     * Splits a string up wherever it finds the given separator.
     *
     * @arg string separator - The string will be separated wherever this sequence is found.
     * @arg string str - The string to split up.
     * @arg boolean use_patterns - Set this to true if your separator is a pattern.
     * @return {object} Exploded string as a numerical sequential table.
     */
    function Explode(separator: string, str: string, use_patterns: boolean): object;

    /**
     * Attempts to find the specified substring in a string.
     *
     * @arg string haystack - The string to search in.
     * @arg string needle - The string to find, can contain patterns if enabled.
     * @arg number [startPos=1] - The position to start the search from, can be negative start position will be relative to the end position.
     * @arg boolean [noPatterns=false] - Disable patterns.
     * @return {number} Starting position of the found text, or nil if the text wasn't found
     * @return {number} Ending position of found text, or nil if the text wasn't found
     * @return {string} Matched text for each group if patterns are enabled and used, or nil if the text wasn't found
     */
    function find(
        haystack: string,
        needle: string,
        startPos?: number,
        noPatterns?: boolean
    ): LuaMultiReturn<[number, number, string]>;

    /**
 * Formats the specified values into the string given.
* 
* @arg string format - The string to be formatted.
Follows this format: http://www.cplusplus.com/reference/cstdio/printf/
LuaJIT supports all specifiers and doesn't support * width and .* presision.
LuaJIT exclusives:



Format
Description
Example of the output




%p
Returns pointer to supplied structure (table/function)
0xf20a8968


%q
Formats a string between double quotes, using escape sequences when necessary to ensure that it can safely be read back by the Lua interpreter
"test\1\2test"
* @arg args[] formatParameters - Values to be formatted into the string.
* @return {string} The formatted string
 */
    function format(format: string, ...formatParameters: any[]): string;

    /**
 * Returns the time as a formatted string or as a table if no format is given.
* 
* @arg number float - The time in seconds to format.
* @arg string [format="nil"] - An optional formatting to use. If no format it specified, a table will be returned instead.
* @return {string} Returns the time as a formatted string only if a format was specified.
Returns a table only if no format was specified. The table will contain these fields:

number ms - milliseconds
number s - seconds
number m - minutes
number h - hours
 */
    function FormattedTime(float: number, format?: string): string;

    /**
     * Creates a string from a Color variable.
     *
     * @arg object color - The color to put in the string.
     * @return {string} Output
     */
    function FromColor(color: object): string;

    /**
     * Returns char value from the specified index in the supplied string.
     *
     * @arg string str - The string that you will be searching with the supplied index.
     * @arg number index - The index's value of the string to be returned.
     * @return {string} str
     */
    function GetChar(str: string, index: number): string;

    /**
     * Returns extension of the file.
     *
     * @arg string file - String eg. file-path to get the file extensions from.
     * @return {string} fileExtension
     */
    function GetExtensionFromFilename(file: string): string;

    /**
     * Returns file name and extension.
     *
     * @arg string pathString - The string eg. file-path to get the file-name from.
     * @return {string} The file name
     */
    function GetFileFromFilename(pathString: string): string;

    /**
     * Returns the path only from a file's path.
     *
     * @arg string Inputstring - String to get path from.
     * @return {string} Path
     */
    function GetPathFromFilename(Inputstring: string): string;

    /**
     * Returns an iterator function that is called for every complete match of the pattern, all sub matches will be passed as to the loop.
     *
     * @arg string data - The string to search in
     * @arg string pattern - The pattern to search for
     * @return {Function} The iterator function that can be used in a for-in loop
     */
    function gfind(data: string, pattern: string): Function;

    /**
     * Using Patterns, returns an iterator which will return either one value if no capture groups are defined, or any capture group matches.
     *
     * @arg string data - The string to search in
     * @arg string pattern - The pattern to search for
     * @return {Function} The iterator function that can be used in a for-in loop
     */
    function gmatch(data: string, pattern: string): Function;

    /**
 * This functions main purpose is to replace certain character sequences in a string using Patterns.
* 
* @arg string string - String which should be modified.
* @arg string pattern - The pattern that defines what should be matched and eventually be replaced.
* @arg string replacement - In case of a string the matched sequence will be replaced with it.
In case of a table, the matched sequence will be used as key and the table will tested for the key, if a value exists it will be used as replacement.
In case of a function all matches will be passed as parameters to the function, the return value(s) of the function will then be used as replacement.
* @arg number [maxReplaces=NaN] - Maximum number of replacements to be made.
* @return {string} replaceResult
* @return {number} replaceCount
 */
    function gsub(
        string: string,
        pattern: string,
        replacement: string,
        maxReplaces?: number
    ): LuaMultiReturn<[string, number]>;

    /**
     * Joins the values of a table together to form a string.
     *
     * @arg string separator - The separator to insert between each piece.
     * @arg object pieces - The table of pieces to concatenate. The keys for these must be numeric and sequential.
     * @return {string} Imploded pieces
     */
    function Implode(separator: string, pieces: object): string;

    /**
     * Escapes special characters for JavaScript in a string, making the string safe for inclusion in to JavaScript strings.
     *
     * @arg string str - The string that should be escaped.
     * @return {string} The escaped string.
     */
    function JavascriptSafe(str: string): string;

    /**
     * Returns everything left of supplied place of that string.
     *
     * @arg string str - The string to extract from.
     * @arg number num - Amount of chars relative to the beginning (starting from 1).
     * @return {string} Returns a string containing a specified number of characters from the left side of a string.
     */
    function Left(str: string, num: number): string;

    /**
     * Counts the number of characters in the string (length). This is equivalent to using the length operator (#).
     *
     * @arg string str - The string to find the length of.
     * @return {number} Length of the string
     */
    function len(str: string): number;

    /**
     * Changes any upper-case letters in a string to lower-case letters.
     *
     * @arg string str - The string to convert.
     * @return {string} A string representing the value of a string converted to lower-case.
     */
    function lower(str: string): string;

    /**
     * Finds a Pattern in a string.
     *
     * @arg string string - String which should be searched in for matches.
     * @arg string pattern - The pattern that defines what should be matched.
     * @arg number [startPosition=1] - The start index to start the matching from, can be negative to start the match from a position relative to the end.
     * @return {args[]} Matched text(s)
     */
    function match(string: string, pattern: string, startPosition?: number): any;

    /**
     * Converts a digital filesize to human-readable text.
     *
     * @arg number bytes - The filesize in bytes.
     * @return {string} The human-readable filesize, in Bytes/KB/MB/GB (whichever is appropriate).
     */
    function NiceSize(bytes: number): string;

    /**
     * Formats the supplied number (in seconds) to the highest possible time unit.
     *
     * @arg number num - The number to format, in seconds.
     * @return {string} A nicely formatted time string.
     */
    function NiceTime(num: number): string;

    /**
     * Escapes all special characters within a string, making the string safe for inclusion in a Lua pattern.
     *
     * @arg string str - The string to be sanitized
     * @return {string} The string that has been sanitized for inclusion in Lua patterns
     */
    function PatternSafe(str: string): string;

    /**
     * Repeats a string by the provided number, with an optional separator.
     *
     * @arg string str - The string to convert.
     * @arg number repetitions - Timer to repeat, this values gets rounded internally.
     * @arg string separator - String that will separate the repeated piece. Notice that it doesn't add this string to the start or the end of the result, only between the repeated parts.
     * @return {string} Repeated string.
     */
    function rep(str: string, repetitions: number, separator: string): string;

    /**
     * Replaces all occurrences of the supplied second string.
     *
     * @arg string str - The string we are seeking to replace an occurrence(s).
     * @arg string find - What we are seeking to replace.
     * @arg string replace - What to replace find with.
     * @return {string} string
     */
    function Replace(str: string, find: string, replace: string): string;

    /**
     * Reverses a string.
     *
     * @arg string str - The string to be reversed.
     * @return {string} reversed string
     */
    function reverse(str: string): string;

    /**
     * Returns the last n-th characters of the string.
     *
     * @arg string str - The string to extract from.
     * @arg number num - Amount of chars relative to the end (starting from 1).
     * @return {string} Returns a string containing a specified number of characters from the right side of a string.
     */
    function Right(str: string, num: number): string;

    /**
     * Sets the character at the specific index of the string.
     *
     * @arg string InputString - The input string
     * @arg number Index - The character index, 1 is the first from left.
     * @arg string ReplacementChar - String to replace with.
     * @return {string} ModifiedString
     */
    function SetChar(InputString: string, Index: number, ReplacementChar: string): string;

    /**
     * Splits the string into a table of strings, separated by the second argument.
     *
     * @arg string Inputstring - String to split
     * @arg string Separator - Character(s) to split with.
     * @return {object} Split table
     */
    function Split(Inputstring: string, Separator: string): object;

    /**
     * Returns whether or not the first string starts with the second.
     *
     * @arg string inputStr - String to check.
     * @arg string start - String to check with.
     * @return {boolean} Whether the first string starts with the second.
     */
    function StartWith(inputStr: string, start: string): boolean;

    /**
     * Removes the extension of a path.
     *
     * @arg string Inputstring - The path to change.
     * @return {string} Modifiedstring
     */
    function StripExtension(Inputstring: string): string;

    /**
     * Returns a sub-string, starting from the character at position StartPos of the string (inclusive), and optionally ending at the character at position EndPos of the string (also inclusive). If EndPos is not given, the rest of the string is returned.
     *
     * @arg string string - The string you'll take a sub-string out of.
     * @arg number StartPos - The position of the first character that will be included in the sub-string.
     * @arg number [EndPos=NaN] - The position of the last character to be included in the sub-string. It can be negative to count from the end.
     * @return {string} The substring.
     */
    function sub(string: string, StartPos: number, EndPos?: number): string;

    /**
     * Fetches a Color type from a string.
     *
     * @arg string Inputstring - The string to convert from.
     * @return {Color} The output Color
     */
    function ToColor(Inputstring: string): Color;

    /**
     * Returns given time in "MM:SS" format.
     *
     * @arg number time - Time in seconds
     * @return {string} Formatted time
     */
    function ToMinutesSeconds(time: number): string;

    /**
     * Returns given time in "MM:SS:MS" format.
     *
     * @arg number time - Time in seconds
     * @return {string} Formatted time
     */
    function ToMinutesSecondsMilliseconds(time: number): string;

    /**
     * Splits the string into characters and creates a sequential table of characters.
     *
     * @arg string str - The string you'll turn into a table.
     * @return {object} A sequential table where each value is a character from the given string
     */
    function ToTable(str: string): object;

    /**
     * Removes leading and trailing matches of a string.
     *
     * @arg string Inputstring - The string to trim.
     * @arg string [Char="%s"] - String to match - can be multiple characters. Matches spaces by default.
     * @return {string} Modified string
     */
    function Trim(Inputstring: string, Char?: string): string;

    /**
     * Removes leading spaces/characters from a string.
     *
     * @arg string str - String to trim
     * @arg string char - Custom character to remove
     * @return {string} Trimmed string
     */
    function TrimLeft(str: string, char: string): string;

    /**
     * Removes trailing spaces/passed character from a string.
     *
     * @arg string str - String to remove from
     * @arg string char - Custom character to remove, default is a space
     * @return {string} Trimmed string
     */
    function TrimRight(str: string, char: string): string;

    /**
     * Changes any lower-case letters in a string to upper-case letters.
     *
     * @arg string str - The string to convert.
     * @return {string} A string representing the value of a string converted to upper-case.
     */
    function upper(str: string): string;
}
