/** @noSelfInFile */
declare namespace os {
    /**
     * Returns the approximate cpu time the application ran.
     * See also SysTime
     *
     * @return {number} runtime
     */
    function clock(): number;

    /**
 * Returns the date/time as a formatted string or in a table.
* 
* @arg string format - The format string.
If this is equal to *t or !*t then this function will return a Structures/DateData, otherwise it will return a string.
If this starts with an !, the returned data will use the UTC timezone rather than the local timezone.
See http://www.mkssoftware.com/docs/man3/strftime.3.asp for available format flags.
Not all flags are available on all operating systems and the result of using an invalid flag is undefined. This currently crashes the game on Windows. Most or all flags are available on OS X and Linux but considerably fewer are available on Windows. See http://msdn.microsoft.com/en-us/library/fe06s4ak.aspx for a list of available flags on Windows. Note that the # flags also crashes the game on Windows.Issue Tracker: 329
Known formats that work on all platforms:



Format
Description
Example of the output




%a
Abbreviated weekday name
Wed


%A
Full weekday name
Wednesday


%b
Abbreviated month name
Sep


%B
Full month name
September


%c
Locale-appropriate date and time
Varies by platform and language settings


%d
Day of the month [01-31]
16


%H
Hour, using a 24-hour clock [00-23]
23


%I
Hour, using a 12-hour clock [01-12]
11


%j
Day of the year [001-365]
259


%m
Month [01-12]
09


%M
Minute [00-59]
48


%p
Either am or pm
pm


%S
Second [00-60]
10


%w
Weekday [0-6 = Sunday-Saturday]
3


%W
Week of the year [00-53]
37


%x
Date (Same as %m/%d/%y)
09/16/98


%X
Time (Same as %H:%M:%S)
24:48:10


%y
Two-digit year [00-99]
98


%Y
Full year
1998


%z
Timezone
-0300


%%
A percent sign
%
* @arg number [time=NaN] - Time to use for the format.
* @return {string} Formatted date
This will be a Structures/DateData if the first argument equals to *t or !*t
 */
    function date(format: string, time?: number): string;

    /**
     * Subtracts the second from the first value and rounds the result.
     *
     * @arg number timeA - The first value.
     * @arg number timeB - The value to subtract.
     * @return {number} diffTime
     */
    function difftime(timeA: number, timeB: number): number;

    /**
     * Returns the system time in seconds past the unix epoch. If a table is supplied, the function attempts to build a system time with the specified table members.
     *
     * @arg DateDataStruct [dateData=nil] - Table to generate the time from. This table's data is interpreted as being in the local timezone. See Structures/DateData
     * @return {number} Seconds passed since Unix epoch
     */
    function time(dateData?: DateDataStruct): number;
}
