/** @noSelfInFile */
declare namespace ai {
    /**
     * Translates a schedule name to its corresponding ID.
     *
     * @arg string sched - Then schedule name. In most cases, this will be the same as the Enums/SCHED name.
     * @return {number} The schedule ID, see Enums/SCHED. Returns -1 if the schedule name isn't valid.
     */
    function GetScheduleID(sched: string): number;

    /**
     * Returns the squad leader of the given squad.
     *
     * @arg string squad - The squad name.
     * @return {NPC} The squad leader.
     */
    function GetSquadLeader(squad: string): NPC;

    /**
     * Returns the amount of members a given squad has.
     *
     * @arg string squad - The squad name.
     * @return {number} The member count.
     */
    function GetSquadMemberCount(squad: string): number;

    /**
     * Returns all members of a given squad.
     *
     * @arg string squad - The squad name.
     * @return {NPC} A table of NPCs.
     */
    function GetSquadMembers(squad: string): NPC;

    /**
     * Translates a task name to its corresponding ID.
     *
     * @arg string task - The task name.
     * @return {number} The task ID, see ai_task.h. Returns -1 if the schedule name isn't valid.
     */
    function GetTaskID(task: string): number;
}
