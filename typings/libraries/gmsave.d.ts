/** @noSelfInFile */
declare namespace gmsave {
    /**
     * Loads a saved map.
     *
     * @arg string mapData - The JSON encoded string containing all the map data.
     * @arg Player [ply=NULL] - The player to load positions for.
     * @arg GMLua.CallbackNoContext [callback=nil] - A function to be called after all the entities have been placed.
     */
    function LoadMap(mapData: string, ply?: Player, callback?: GMLua.CallbackNoContext): void;

    /**
     * Sets player position and angles from supplied table
     *
     * @arg Player ply - The player to "load" values for
     * @arg object data - A table containing Origin and Angle keys for position and angles to set.
     */
    function PlayerLoad(ply: Player, data: object): void;

    /**
     * Returns a table containing player position and angles. Used by gmsave.SaveMap.
     *
     * @arg Player ply - The player to "save"
     * @return {object} A table containing player position ( Origin ) and angles ( Angle )
     */
    function PlayerSave(ply: Player): object;

    /**
     * Saves the map
     *
     * @arg Player ply - The player, whose position should be saved for loading the save
     * @return {string} The encoded to JSON string containing save data
     */
    function SaveMap(ply: Player): string;

    /**
     * Returns if we should save this entity in a duplication or a map save or not.
     *
     * @arg Entity ent - The entity
     * @arg object t - A table containing classname key with entities classname.
     * @return {boolean} Should save entity or not
     */
    function ShouldSaveEntity(ent: Entity, t: object): boolean;
}
