/** @noSelfInFile */
declare namespace render {
    /**
     * Adds a beam segment to the beam started by render.StartBeam.
     *
     * @arg Vector startPos - Beam start position.
     * @arg number width - The width of the beam.
     * @arg number textureEnd - The end coordinate of the texture used.
     * @arg Color color - The color to be used. Uses the Color.
     */
    function AddBeam(startPos: Vector, width: number, textureEnd: number, color: Color): void;

    /**
     * Blurs the render target ( or a given texture )
     *
     * @arg ITexture rendertarget - The texture to blur
     * @arg number blurx - Horizontal amount of blur
     * @arg number blury - Vertical amount of blur
     * @arg number passes - Amount of passes to go through
     */
    function BlurRenderTarget(rendertarget: ITexture, blurx: number, blury: number, passes: number): void;

    /**
     * This function overrides the brush material for next render operations. It can be used with Entity:DrawModel.
     *
     * @arg IMaterial [mat=nil]
     */
    function BrushMaterialOverride(mat?: IMaterial): void;

    /**
     * Captures a part of the current render target and returns the data as a binary string in the given format.
     *
     * @arg RenderCaptureDataStruct captureData - Parameters of the capture. See Structures/RenderCaptureData.
     * @return {string} binaryData
     */
    function Capture(captureData: RenderCaptureDataStruct): string;

    /**
     * Dumps the current render target and allows the pixels to be accessed by render.ReadPixel.
     */
    function CapturePixels(): void;

    /**
     * Clears the current render target and the specified buffers.
     *
     * @arg number r - Red component to clear to.
     * @arg number g - Green component to clear to.
     * @arg number b - Blue component to clear to.
     * @arg number a - Alpha component to clear to.
     * @arg boolean [clearDepth=false] - Clear the depth.
     * @arg boolean [clearStencil=false] - Clear the stencil.
     */
    function Clear(r: number, g: number, b: number, a: number, clearDepth?: boolean, clearStencil?: boolean): void;

    /**
     * Clears the current rendertarget for obeying the current stencil buffer conditions.
     *
     * @arg number r - Value of the red channel to clear the current rt with.
     * @arg number g - Value of the green channel to clear the current rt with.
     * @arg number b - Value of the blue channel to clear the current rt with.
     * @arg number a - Value of the alpha channel to clear the current rt with.
     * @arg boolean depth - Clear the depth buffer.
     */
    function ClearBuffersObeyStencil(r: number, g: number, b: number, a: number, depth: boolean): void;

    /**
     * Resets the depth buffer.
     */
    function ClearDepth(): void;

    /**
     * Clears a render target
     *
     * @arg ITexture texture
     * @arg Color color - The color, see Color
     */
    function ClearRenderTarget(texture: ITexture, color: Color): void;

    /**
     * Resets all values in the stencil buffer to zero.
     */
    function ClearStencil(): void;

    /**
     * Sets the stencil value in a specified rect.
     *
     * @arg number originX - X origin of the rectangle.
     * @arg number originY - Y origin of the rectangle.
     * @arg number endX - The end X coordinate of the rectangle.
     * @arg number endY - The end Y coordinate of the rectangle.
     * @arg number stencilValue - Value to set cleared stencil buffer to.
     */
    function ClearStencilBufferRectangle(
        originX: number,
        originY: number,
        endX: number,
        endY: number,
        stencilValue: number
    ): void;

    /**
     * Calculates the lighting caused by dynamic lights for the specified surface.
     *
     * @arg Vector position - The position to sample from.
     * @arg Vector normal - The normal of the surface.
     * @return {Vector} A vector representing the light at that point.
     */
    function ComputeDynamicLighting(position: Vector, normal: Vector): Vector;

    /**
     * Calculates the light color of a certain surface.
     *
     * @arg Vector position - The position of the surface to get the light from.
     * @arg Vector normal - The normal of the surface to get the light from.
     * @return {Vector} A vector representing the light at that point.
     */
    function ComputeLighting(position: Vector, normal: Vector): Vector;

    /**
     * Copies the currently active Render Target to the specified texture.
     *
     * @arg ITexture Target - The texture to copy to
     */
    function CopyRenderTargetToTexture(Target: ITexture): void;

    /**
     * Copies the contents of one texture to another. Only works with rendertargets.
     *
     * @arg ITexture texture_from
     * @arg ITexture texture_to
     */
    function CopyTexture(texture_from: ITexture, texture_to: ITexture): void;

    /**
     * Changes the cull mode.
     *
     * @arg number cullMode - Cullmode, see Enums/MATERIAL_CULLMODE
     */
    function CullMode(cullMode: number): void;

    /**
     * Set's the depth range of the upcoming render.
     *
     * @arg number depthmin - The minimum depth of the upcoming render. 0.0 = render normally; 1.0 = render nothing.
     * @arg number depthmax - The maximum depth of the upcoming render. 0.0 = render everything (through walls); 1.0 = render normally.
     */
    function DepthRange(depthmin: number, depthmax: number): void;

    /**
     * Draws textured beam.
     *
     * @arg Vector startPos - Beam start position.
     * @arg Vector endPos - Beam end position.
     * @arg number width - The width of the beam.
     * @arg number textureStart - The start coordinate of the texture used.
     * @arg number textureEnd - The end coordinate of the texture used.
     * @arg Color [color=Color( 255, 255, 255 )] - The color to be used. Uses the Color.
     */
    function DrawBeam(
        startPos: Vector,
        endPos: Vector,
        width: number,
        textureStart: number,
        textureEnd: number,
        color?: Color
    ): void;

    /**
     * Draws a box in 3D space.
     *
     * @arg Vector position - Origin of the box.
     * @arg Angle angles - Orientation of the box.
     * @arg Vector mins - Start position of the box, relative to origin.
     * @arg Vector maxs - End position of the box, relative to origin.
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color.
     */
    function DrawBox(position: Vector, angles: Angle, mins: Vector, maxs: Vector, color?: Color): void;

    /**
 * Draws a line in 3D space.
* 
* @arg Vector startPos - Line start position in world coordinates.
* @arg Vector endPos - Line end position in world coordinates.
* @arg Color [color=Color( 255, 255, 255 )] - The color to be used. Uses the Color.
* @arg boolean [writeZ=false] - Whether or not to consider the Z buffer. If false, the line will be drawn over everything currently drawn, if true, the line will be drawn with depth considered, as if it were a regular object in 3D space.
Enabling this option will cause the line to ignore the color's alpha.Issue Tracker: 1086
 */
    function DrawLine(startPos: Vector, endPos: Vector, color?: Color, writeZ?: boolean): void;

    /**
     * Draws 2 connected triangles. Expects material to be set by render.SetMaterial.
     *
     * @arg Vector vert1 - First vertex.
     * @arg Vector vert2 - The second vertex.
     * @arg Vector vert3 - The third vertex.
     * @arg Vector vert4 - The fourth vertex.
     * @arg any [color=Color( 255, 255, 255 )] - The color of the quad. See Color
     */
    function DrawQuad(vert1: Vector, vert2: Vector, vert3: Vector, vert4: Vector, color?: any): void;

    /**
     * Draws a quad.
     *
     * @arg Vector position - Origin of the sprite.
     * @arg Vector normal - The face direction of the quad.
     * @arg number width - The width of the quad.
     * @arg number height - The height of the quad.
     * @arg Color color - The color of the quad. Uses the Color.
     * @arg number [rotation=0] - The rotation of the quad counter-clockwise in degrees around the normal axis. In other words, the quad will always face the same way but this will rotate its corners.
     */
    function DrawQuadEasy(
        position: Vector,
        normal: Vector,
        width: number,
        height: number,
        color: Color,
        rotation?: number
    ): void;

    /**
     * Draws the current material set by render.SetMaterial to the whole screen. The color cannot be customized.
     *
     * @arg boolean [applyPoster=false] - If set to true, when rendering a poster the quad will be properly drawn in parts in the poster. This is used internally by some Post Processing effects. Certain special textures (frame buffer like textures) do not need this adjustment.
     */
    function DrawScreenQuad(applyPoster?: boolean): void;

    /**
     * Draws the the current material set by render.SetMaterial to the area specified. Color cannot be customized.
     *
     * @arg number startX - X start position of the rect.
     * @arg number startY - Y start position of the rect.
     * @arg number width - Width of the rect.
     * @arg number height - Height of the rect.
     */
    function DrawScreenQuadEx(startX: number, startY: number, width: number, height: number): void;

    /**
     * Draws a sphere in 3D space. The material previously set with render.SetMaterial will be applied the sphere's surface.
     *
     * @arg Vector position - Position of the sphere.
     * @arg number radius - Radius of the sphere. Negative radius will make the sphere render inwards rather than outwards.
     * @arg number longitudeSteps - The number of longitude steps. This controls the quality of the sphere. Higher quality will lower performance significantly. 50 is a good number to start with.
     * @arg number latitudeSteps - The number of latitude steps. This controls the quality of the sphere. Higher quality will lower performance significantly. 50 is a good number to start with.
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the sphere. Uses the Color.
     */
    function DrawSphere(
        position: Vector,
        radius: number,
        longitudeSteps: number,
        latitudeSteps: number,
        color?: Color
    ): void;

    /**
     * Draws a sprite in 3D space.
     *
     * @arg Vector position - Position of the sprite.
     * @arg number width - Width of the sprite.
     * @arg number height - Height of the sprite.
     * @arg Color [color=Color( 255, 255, 255 )] - Color of the sprite. Uses the Color.
     */
    function DrawSprite(position: Vector, width: number, height: number, color?: Color): void;

    /**
     * Draws a texture over the whole screen.
     *
     * @arg ITexture tex - The texture to draw
     */
    function DrawTextureToScreen(tex: ITexture): void;

    /**
     * Draws a textured rectangle.
     *
     * @arg ITexture tex - The texture to draw
     * @arg number x - The x coordinate of the rectangle to draw.
     * @arg number y - The y coordinate of the rectangle to draw.
     * @arg number width - The width of the rectangle to draw.
     * @arg number height - The height of the rectangle to draw.
     */
    function DrawTextureToScreenRect(tex: ITexture, x: number, y: number, width: number, height: number): void;

    /**
     * Draws a wireframe box in 3D space.
     *
     * @arg Vector position - Position of the box.
     * @arg Angle angle - Angles of the box.
     * @arg Vector mins - The lowest corner of the box.
     * @arg Vector maxs - The highest corner of the box.
     * @arg Color [color=Color( 255, 255, 255 )] - The color of the box. Uses the Color.
     * @arg boolean [writeZ=false] - Sets whenever to write to the zBuffer.
     */
    function DrawWireframeBox(
        position: Vector,
        angle: Angle,
        mins: Vector,
        maxs: Vector,
        color?: Color,
        writeZ?: boolean
    ): void;

    /**
 * Draws a wireframe sphere in 3d space.
* 
* @arg Vector position - Position of the sphere.
* @arg number radius - The size of the sphere.
* @arg number longitudeSteps - The amount of longitude steps.
The larger this number is, the smoother the sphere is.
* @arg number latitudeSteps - The amount of latitude steps.
The larger this number is, the smoother the sphere is.
* @arg Color [color=Color( 255, 255, 255 )] - The color of the wireframe. Uses the Color.
* @arg boolean [writeZ=false] - Whether or not to consider the Z buffer. If false, the wireframe will be drawn over everything currently drawn. If true, it will be drawn with depth considered, as if it were a regular object in 3D space.
 */
    function DrawWireframeSphere(
        position: Vector,
        radius: number,
        longitudeSteps: number,
        latitudeSteps: number,
        color?: Color,
        writeZ?: boolean
    ): void;

    /**
     * Sets the status of the clip renderer, returning previous state.
     *
     * @arg boolean state - New clipping state.
     * @return {boolean} Previous clipping state.
     */
    function EnableClipping(state: boolean): boolean;

    /**
     * Ends the beam mesh of a beam started with render.StartBeam.
     */
    function EndBeam(): void;

    /**
     * Sets the color of the fog.
     *
     * @arg number red - Red channel of the fog color, 0 - 255.
     * @arg number green - Green channel of the fog color, 0 - 255.
     * @arg number blue - Blue channel of the fog color, 0 - 255.
     */
    function FogColor(red: number, green: number, blue: number): void;

    /**
 * Sets the at which the fog reaches its max density.
* 
* @arg number distance - The distance at which the fog reaches its max density.
If used in GM:SetupSkyboxFog, this value must be scaled by the first argument of the hook
 */
    function FogEnd(distance: number): void;

    /**
     * Sets the maximum density of the fog.
     *
     * @arg number maxDensity - The maximum density of the fog, 0-1.
     */
    function FogMaxDensity(maxDensity: number): void;

    /**
     * Sets the mode of fog.
     *
     * @arg number fogMode - Fog mode, see Enums/MATERIAL_FOG.
     */
    function FogMode(fogMode: number): void;

    /**
 * Sets the distance at which the fog starts showing up.
* 
* @arg number fogStart - The distance at which the fog starts showing up.
If used in GM:SetupSkyboxFog, this value must be scaled by the first argument of the hook
 */
    function FogStart(fogStart: number): void;

    /**
     * Returns the ambient color of the map.
     *
     * @return {Vector} color
     */
    function GetAmbientLightColor(): Vector;

    /**
     * Returns the current alpha blending.
     *
     * @return {number} Current alpha blending in range 0 to 1.
     */
    function GetBlend(): number;

    /**
     *
     *
     * @return {ITexture} The bloom texture
     */
    function GetBloomTex0(): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetBloomTex1(): ITexture;

    /**
     * Returns the current color modulation values as normals.
     *
     * @return {number} r
     * @return {number} g
     * @return {number} b
     */
    function GetColorModulation(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns the maximum available directX version.
     *
     * @return {number} dxLevel
     */
    function GetDXLevel(): number;

    /**
     * Returns the current fog color.
     *
     * @return {number} Red part of the color.
     * @return {number} Green part of the color
     * @return {number} Blue part of the color
     */
    function GetFogColor(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns the fog start and end distance.
     *
     * @return {number} Fog start distance set by render.FogStart
     * @return {number} For end distance set by render.FogEnd
     * @return {number} Fog Z distance set by render.SetFogZ
     */
    function GetFogDistances(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns the fog mode.
     *
     * @return {number} Fog mode, see Enums/MATERIAL_FOG
     */
    function GetFogMode(): number;

    /**
     * Returns the _rt_FullFrameDepth texture. Alias of _rt_PowerOfTwoFB
     *
     * @return {ITexture}
     */
    function GetFullScreenDepthTexture(): ITexture;

    /**
     * Returns whether HDR is currently enabled or not. This takes into account hardware support, current map and current client settings.
     *
     * @return {boolean} true if the player currently has HDR enabled.
     */
    function GetHDREnabled(): boolean;

    /**
     * Gets the light exposure on the specified position.
     *
     * @arg Vector position - The position of the surface to get the light from.
     * @return {Vector} lightColor
     */
    function GetLightColor(position: Vector): Vector;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetMoBlurTex0(): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetMoBlurTex1(): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetMorphTex0(): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetMorphTex1(): ITexture;

    /**
     * Returns the render target's power of two texture.
     *
     * @return {ITexture} The power of two texture, which is _rt_poweroftwofb by default.
     */
    function GetPowerOfTwoTexture(): ITexture;

    /**
     * Alias of render.GetPowerOfTwoTexture.
     *
     * @return {ITexture}
     */
    function GetRefractTexture(): ITexture;

    /**
     * Returns the currently active render target.
     *
     * @return {ITexture} The currently active Render Target
     */
    function GetRenderTarget(): ITexture;

    /**
     * Returns the _rt_ResolvedFullFrameDepth texture for SSAO depth. It will only be updated if GM:NeedsDepthPass returns true.
     *
     * @return {ITexture} The depth texture.
     */
    function GetResolvedFullFrameDepth(): ITexture;

    /**
     * Obtain an ITexture of the screen. You must call render.UpdateScreenEffectTexture in order to update this texture with the currently rendered scene.
     *
     * @arg number [textureIndex=0] - Max index is 3, but engine only creates the first two for you.
     * @return {ITexture}
     */
    function GetScreenEffectTexture(textureIndex?: number): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetSmallTex0(): ITexture;

    /**
     *
     *
     * @return {ITexture}
     */
    function GetSmallTex1(): ITexture;

    /**
     * Returns a floating point texture the same resolution as the screen.
     *
     * @return {ITexture} Render target named "__rt_supertexture1"
     */
    function GetSuperFPTex(): ITexture;

    /**
     * See render.GetSuperFPTex
     *
     * @return {ITexture} Render target named "__rt_supertexture2"
     */
    function GetSuperFPTex2(): ITexture;

    /**
     * Performs a render trace and returns the color of the surface hit, this uses a low res version of the texture.
     *
     * @arg Vector startPos - The start position to trace from.
     * @arg Vector endPos - The end position of the trace.
     * @return {Vector} color
     */
    function GetSurfaceColor(startPos: Vector, endPos: Vector): Vector;

    /**
     * Returns a vector representing linear tone mapping scale.
     *
     * @return {Vector} The vector representing linear tone mapping scale.
     */
    function GetToneMappingScaleLinear(): Vector;

    /**
     * Returns the current view setup.
     *
     * @arg boolean [noPlayer=false] - If true, returns the view->GetViewSetup, if false - returns view->GetPlayerViewSetup
     * @return {number} Current current view setup. See Structures/ViewSetup
     */
    function GetViewSetup(noPlayer?: boolean): number;

    /**
     * Sets the render material override for all next calls of Entity:DrawModel. Also overrides render.MaterialOverrideByIndex.
     *
     * @arg IMaterial material - The material to use as override, use nil to disable.
     */
    function MaterialOverride(material: IMaterial): void;

    /**
     * Similar to render.MaterialOverride, but overrides the materials per index.
     *
     * @arg number index - Starts with 0, the index of the material to override
     * @arg IMaterial material - The material to override with
     */
    function MaterialOverrideByIndex(index: number, material: IMaterial): void;

    /**
     * Returns the maximum texture height the renderer can handle.
     *
     * @return {number} maxTextureHeight
     */
    function MaxTextureHeight(): number;

    /**
     * Returns the maximum texture width the renderer can handle.
     *
     * @return {number} maxTextureWidth
     */
    function MaxTextureWidth(): number;

    /**
 * Creates a new ClientsideModel, renders it at the specified pos/ang, and removes it. Can also be given an existing CSEnt to reuse instead.
* 
* @arg object settings - Requires:

string model - The model to draw
Vector pos - The position to draw the model at
Angle angle - The angles to draw the model at
* @arg CSEnt [ent=nil] - If provided, this entity will be reused instead of creating a new one with ClientsideModel. Note that the ent's model, position and angles will be changed, and Entity:SetNoDraw will be set to true.
 */
    function Model(settings: object, ent?: CSEnt): void;

    /**
     * Sets a material to override a model's default material. Similar to Entity:SetMaterial except it uses an IMaterial argument and it can be used to change materials on models which are part of the world geometry.
     *
     * @arg IMaterial material - The material override.
     */
    function ModelMaterialOverride(material: IMaterial): void;

    /**
     * Overrides the write behaviour of all next rendering operations towards the alpha channel of the current render target.
     *
     * @arg boolean enable - Enable or disable the override.
     * @arg boolean shouldWrite - If the previous argument is true, sets whether the next rendering operations should write to the alpha channel or not. Has no effect if the previous argument is false.
     */
    function OverrideAlphaWriteEnable(enable: boolean, shouldWrite: boolean): void;

    /**
     * Overrides the internal graphical functions used to determine the final color and alpha of a rendered texture.
     *
     * @arg boolean enabled - true to enable, false to disable. No other arguments are required when disabling.
     * @arg number srcBlend - The source color blend function Enums/BLEND. Determines how a rendered texture's final color should be calculated.
     * @arg number destBlend - The destination color blend function Enums/BLEND.
     * @arg number blendFunc - The blend mode used for drawing the color layer Enums/BLENDFUNC.
     * @arg number [srcBlendAlpha=NaN] - The source alpha blend function Enums/BLEND. Determines how a rendered texture's final alpha should be calculated.
     * @arg number [destBlendAlpha=NaN] - The destination alpha blend function Enums/BLEND.
     * @arg number [blendFuncAlpha=NaN] - The blend mode used for drawing the alpha layer Enums/BLENDFUNC.
     */
    function OverrideBlend(
        enabled: boolean,
        srcBlend: number,
        destBlend: number,
        blendFunc: number,
        srcBlendAlpha?: number,
        destBlendAlpha?: number,
        blendFuncAlpha?: number
    ): void;

    /**
     * Overrides the internal graphical functions used to determine the final color and alpha of a rendered texture.
     *
     * @arg boolean enabled - true to enable, false to disable. No other arguments are required when disabling.
     * @arg number srcBlend - The source color blend function Enums/BLEND. Determines how a rendered texture's final color should be calculated.
     * @arg number destBlend
     * @arg number [srcBlendAlpha=NaN] - The source alpha blend function Enums/BLEND. Determines how a rendered texture's final alpha should be calculated.
     * @arg number [destBlendAlpha=NaN]
     */
    function OverrideBlendFunc(
        enabled: boolean,
        srcBlend: number,
        destBlend: number,
        srcBlendAlpha?: number,
        destBlendAlpha?: number
    ): void;

    /**
     * Overrides the write behaviour of all next rendering operations towards the color channel of the current render target.
     *
     * @arg boolean enable - Enable or disable the override.
     * @arg boolean shouldWrite - If the previous argument is true, sets whether the next rendering operations should write to the color channel or not. Has no effect if the previous argument is false.
     */
    function OverrideColorWriteEnable(enable: boolean, shouldWrite: boolean): void;

    /**
     * Overrides the write behaviour of all next rendering operations towards the depth buffer.
     *
     * @arg boolean enable - Enable or disable the override.
     * @arg boolean shouldWrite - If the previous argument is true, sets whether the next rendering operations should write to the depth buffer or not. Has no effect if the previous argument is false.
     */
    function OverrideDepthEnable(enable: boolean, shouldWrite: boolean): void;

    /**
     *
     */
    function PerformFullScreenStencilOperation(): void;

    /**
     * Removes the current active clipping plane from the clip plane stack.
     */
    function PopCustomClipPlane(): void;

    /**
     * Pops the current texture magnification filter from the filter stack.
     */
    function PopFilterMag(): void;

    /**
     * Pops the current texture minification filter from the filter stack.
     */
    function PopFilterMin(): void;

    /**
     * Pops the current flashlight mode from the flashlight mode stack.
     */
    function PopFlashlightMode(): void;

    /**
     * Pops the last render target and viewport from the RT stack and sets them as the current render target and viewport.
     */
    function PopRenderTarget(): void;

    /**
     * Pushes a new clipping plane of the clip plane stack and sets it as active.
     *
     * @arg Vector normal - The normal of the clipping plane.
     * @arg number distance - The distance of the plane from the world origin. You can use Vector:Dot between the normal and any point on the plane to find this.
     */
    function PushCustomClipPlane(normal: Vector, distance: number): void;

    /**
     * Pushes a texture filter onto the magnification texture filter stack.
     *
     * @arg number texFilterType - The texture filter type, see Enums/TEXFILTER
     */
    function PushFilterMag(texFilterType: number): void;

    /**
     * Pushes a texture filter onto the minification texture filter stack.
     *
     * @arg number texFilterType - The texture filter type, see Enums/TEXFILTER
     */
    function PushFilterMin(texFilterType: number): void;

    /**
     * Enables the flashlight projection for the upcoming rendering.
     *
     * @arg boolean [enable=false] - Whether the flashlight mode should be enabled or disabled.
     */
    function PushFlashlightMode(enable?: boolean): void;

    /**
     * Pushes the current render target and viewport to the RT stack then sets a new current render target and viewport. If the viewport is not specified, the dimensions of the render target are used instead.
     *
     * @arg ITexture texture - The new render target to be used.
     * @arg number [x=0] - X origin of the viewport.
     * @arg number [y=0] - Y origin of the viewport.
     * @arg number [w=NaN] - Width of the viewport.
     * @arg number [h=NaN] - Height of the viewport
     */
    function PushRenderTarget(texture: ITexture, x?: number, y?: number, w?: number, h?: number): void;

    /**
     * Reads the color of the specified pixel from the RenderTarget sent by render.CapturePixels
     *
     * @arg number x - The x coordinate.
     * @arg number y - The y coordinate.
     * @return {number} The red channel value.
     * @return {number} The green channel value.
     * @return {number} The blue channel value.
     * @return {number} The alpha channel value or no value if the render target has no alpha channel.
     */
    function ReadPixel(x: number, y: number): LuaMultiReturn<[number, number, number, number]>;

    /**
     * This applies the changes made to map lighting using engine.LightStyle.
     *
     * @arg boolean [DoStaticProps=false] - When true, this will also apply lighting changes to static props. This is really slow on large maps.
     * @arg boolean [UpdateStaticLighting=false] - Forces all props to update their static lighting. Can be slow.
     */
    function RedownloadAllLightmaps(DoStaticProps?: boolean, UpdateStaticLighting?: boolean): void;

    /**
     * Renders the HUD on the screen.
     *
     * @arg number x - X position for the HUD draw origin.
     * @arg number y - Y position for the HUD draw origin.
     * @arg number w - Width of the HUD draw.
     * @arg number h - Height of the HUD draw.
     */
    function RenderHUD(x: number, y: number, w: number, h: number): void;

    /**
     * Renders the scene with the specified viewData to the current active render target.
     *
     * @arg ViewDataStruct [view=nil] - The view data to be used in the rendering. See Structures/ViewData. Any missing value is assumed to be that of the current view. Similarly, you can make a normal render by simply not passing this table at all.
     */
    function RenderView(view?: ViewDataStruct): void;

    /**
     * Resets the model lighting to the specified color.
     *
     * @arg number r - The red part of the color, 0-1
     * @arg number g - The green part of the color, 0-1
     * @arg number b - The blue part of the color, 0-1
     */
    function ResetModelLighting(r: number, g: number, b: number): void;

    /**
     * Resets the HDR tone multiplier to the specified value.
     *
     * @arg number scale - The value which should be used as multiplier.
     */
    function ResetToneMappingScale(scale: number): void;

    /**
     * Sets the ambient lighting for any upcoming render operation.
     *
     * @arg number r - The red part of the color, 0-1.
     * @arg number g - The green part of the color, 0-1.
     * @arg number b - The blue part of the color, 0-1.
     */
    function SetAmbientLight(r: number, g: number, b: number): void;

    /**
     * Sets the alpha blending for every upcoming render operation.
     *
     * @arg number blending - Blending value from 0-1.
     */
    function SetBlend(blending: number): void;

    /**
     * Sets the current drawing material to "color".
     */
    function SetColorMaterial(): void;

    /**
     * Sets the current drawing material to color_ignorez.
     */
    function SetColorMaterialIgnoreZ(): void;

    /**
     * Sets the color modulation.
     *
     * @arg number r - The red channel multiplier normal ranging from 0-1.
     * @arg number g - The green channel multiplier normal ranging from 0-1.
     * @arg number b - The blue channel multiplier normal ranging from 0-1.
     */
    function SetColorModulation(r: number, g: number, b: number): void;

    /**
     * If the fog mode is set to MATERIAL_FOG_LINEAR_BELOW_FOG_Z, the fog will only be rendered below the specified height.
     *
     * @arg number fogZ - The fog Z.
     */
    function SetFogZ(fogZ: number): void;

    /**
     * Sets the goal HDR tone mapping scale.
     *
     * @arg number scale - The target scale.
     */
    function SetGoalToneMappingScale(scale: number): void;

    /**
 * Sets lighting mode when rendering something.
* 
* @arg number Mode - Lighting render mode
Possible values are:

0 - Default
1 - Total fullbright, similar to mat_fullbright 1 but excluding some weapon view models
2 - Increased brightness(?), models look fullbright
 */
    function SetLightingMode(Mode: number): void;

    /**
     * Sets the lighting origin.
     *
     * @arg Vector lightingOrigin - The position from which the light should be "emitted".
     */
    function SetLightingOrigin(lightingOrigin: Vector): void;

    /**
     * Sets the texture to be used as the lightmap in upcoming rendering operations. This is required when rendering meshes using a material with a lightmapped shader such as LightmappedGeneric.
     *
     * @arg ITexture tex - The texture to be used as the lightmap.
     */
    function SetLightmapTexture(tex: ITexture): void;

    /**
     * Sets up the local lighting for any upcoming render operation. Up to 4 local lights can be defined, with one of three different types (point, directional, spot).
     *
     * @arg LocalLightStruct [lights={}] - A table containing up to 4 tables for each light source that should be set up. Each of these tables should contain the properties of its associated light source, see Structures/LocalLight.
     */
    function SetLocalModelLights(lights?: LocalLightStruct): void;

    /**
     * Sets the material to be used in any upcoming render operation using the render.
     *
     * @arg IMaterial mat - The material to be used.
     */
    function SetMaterial(mat: IMaterial): void;

    /**
     * Sets up the ambient lighting for any upcoming render operation. Ambient lighting can be seen as a cube enclosing the object to be drawn, each of its faces representing a directional light source that shines towards the object. Thus, there is a total of six different light sources that can be configured separately.
     *
     * @arg number lightDirection - The light source to edit, see Enums/BOX.
     * @arg number red - The red component of the light color.
     * @arg number green - The green component of the light color.
     * @arg number blue - The blue component of the light color.
     */
    function SetModelLighting(lightDirection: number, red: number, green: number, blue: number): void;

    /**
     * Sets the render target to the specified rt.
     *
     * @arg ITexture texture - The new render target to be used.
     */
    function SetRenderTarget(texture: ITexture): void;

    /**
     * Sets the render target with the specified index to the specified rt.
     *
     * @arg number rtIndex - The index of the rt to set.
     * @arg ITexture texture - The new render target to be used.
     */
    function SetRenderTargetEx(rtIndex: number, texture: ITexture): void;

    /**
     * Sets a scissoring rect which limits the drawing area.
     *
     * @arg number startX - X start coordinate of the scissor rect.
     * @arg number startY - Y start coordinate of the scissor rect.
     * @arg number endX - X end coordinate of the scissor rect.
     * @arg number endY - Y end coordinate of the scissor rect.
     * @arg boolean enable - Enable or disable the scissor rect.
     */
    function SetScissorRect(startX: number, startY: number, endX: number, endY: number, enable: boolean): void;

    /**
     * Sets the shadow color.
     *
     * @arg number red - The red channel of the shadow color.
     * @arg number green - The green channel of the shadow color.
     * @arg number blue - The blue channel of the shadow color.
     */
    function SetShadowColor(red: number, green: number, blue: number): void;

    /**
     * Sets the shadow projection direction.
     *
     * @arg Vector shadowDirections - The new shadow direction.
     */
    function SetShadowDirection(shadowDirections: Vector): void;

    /**
     * Sets the maximum shadow projection range.
     *
     * @arg number shadowDistance - The new maximum shadow distance.
     */
    function SetShadowDistance(shadowDistance: number): void;

    /**
     * Sets whether any future render operations will ignore shadow drawing.
     *
     * @arg boolean newState
     */
    function SetShadowsDisabled(newState: boolean): void;

    /**
     * Sets the compare function of the stencil.
     *
     * @arg number compareFunction - Compare function, see Enums/STENCILCOMPARISONFUNCTION, and Enums/STENCIL for short.
     */
    function SetStencilCompareFunction(compareFunction: number): void;

    /**
     * Sets whether stencil tests are carried out for each rendered pixel.
     *
     * @arg boolean newState - The new state.
     */
    function SetStencilEnable(newState: boolean): void;

    /**
     * Sets the operation to be performed on the stencil buffer values if the compare function was not successful.
     * Note that this takes place before depth testing.
     *
     * @arg number failOperation - Fail operation function, see Enums/STENCILOPERATION.
     */
    function SetStencilFailOperation(failOperation: number): void;

    /**
     * Sets the operation to be performed on the stencil buffer values if the compare function was successful.
     *
     * @arg number passOperation - Pass operation function, see Enums/STENCILOPERATION.
     */
    function SetStencilPassOperation(passOperation: number): void;

    /**
     * Sets the reference value which will be used for all stencil operations. This is an unsigned integer.
     *
     * @arg number referenceValue - Reference value.
     */
    function SetStencilReferenceValue(referenceValue: number): void;

    /**
     * Sets the unsigned 8-bit test bitflag mask to be used for any stencil testing.
     *
     * @arg number mask - The mask bitflag.
     */
    function SetStencilTestMask(mask: number): void;

    /**
     * Sets the unsigned 8-bit write bitflag mask to be used for any writes to the stencil buffer.
     *
     * @arg number mask - The mask bitflag.
     */
    function SetStencilWriteMask(mask: number): void;

    /**
     * Sets the operation to be performed on the stencil buffer values if the stencil test is passed but the depth buffer test fails.
     *
     * @arg number zFailOperation - Z fail operation function, see Enums/STENCILOPERATION
     */
    function SetStencilZFailOperation(zFailOperation: number): void;

    /**
     *
     *
     * @arg Vector vec
     */
    function SetToneMappingScaleLinear(vec: Vector): void;

    /**
     * Changes the view port position and size. The values will be clamped to the game's screen resolution.
     *
     * @arg number x - X origin of the view port.
     * @arg number y - Y origin of the view port.
     * @arg number w - Width of the view port.
     * @arg number h - Height of the view port.
     */
    function SetViewPort(x: number, y: number, w: number, h: number): void;

    /**
     * Sets the internal parameter INT_RENDERPARM_WRITE_DEPTH_TO_DESTALPHA
     *
     * @arg boolean enable
     */
    function SetWriteDepthToDestAlpha(enable: boolean): void;

    /**
     * Swaps the frame buffers/cycles the frame. In other words, this updates the screen.
     */
    function Spin(): void;

    /**
     * Start a new beam draw operation.
     *
     * @arg number segmentCount - Amount of beam segments that are about to be drawn.
     */
    function StartBeam(segmentCount: number): void;

    /**
     * Returns whether the player's hardware supports HDR. (High Dynamic Range) HDR can still be disabled by the mat_hdr_level console variable or just not be supported by the map.
     *
     * @return {boolean} true if the player's hardware supports HDR.
     */
    function SupportsHDR(): boolean;

    /**
     * Returns if the current settings and the system allow the usage of pixel shaders 1.4.
     *
     * @return {boolean} Whether Pixel Shaders 1.4 are supported or not.
     */
    function SupportsPixelShaders_1_4(): boolean;

    /**
     * Returns if the current settings and the system allow the usage of pixel shaders 2.0.
     *
     * @return {boolean} Whether Pixel Shaders 2.0 are supported or not.
     */
    function SupportsPixelShaders_2_0(): boolean;

    /**
     * Returns if the current settings and the system allow the usage of vertex shaders 2.0.
     *
     * @return {boolean} Whether Vertex Shaders 2.0 are supported or not.
     */
    function SupportsVertexShaders_2_0(): boolean;

    /**
     * Suppresses or enables any engine lighting for any upcoming render operation.
     *
     * @arg boolean suppressLighting - True to suppress false to enable.
     */
    function SuppressEngineLighting(suppressLighting: boolean): void;

    /**
     * Enables HDR tone mapping which influences the brightness.
     */
    function TurnOnToneMapping(): void;

    /**
     * Updates the texture returned by render.GetFullScreenDepthTexture.
     */
    function UpdateFullScreenDepthTexture(): void;

    /**
     * Updates the power of two texture.
     *
     * @return {ITexture} Returns render.GetPowerOfTwoTexture.
     */
    function UpdatePowerOfTwoTexture(): ITexture;

    /**
     * Pretty much alias of render.UpdatePowerOfTwoTexture but does not return the texture.
     */
    function UpdateRefractTexture(): void;

    /**
     * Copies the entire screen to the screen effect texture, which can be acquired via render.GetScreenEffectTexture. This function is mainly intended to be used in GM:RenderScreenspaceEffects
     */
    function UpdateScreenEffectTexture(): void;
}
