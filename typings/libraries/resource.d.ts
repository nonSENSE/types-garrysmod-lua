/** @noSelfInFile */
declare namespace resource {
    /**
     * Adds the specified and all related files to the files the client should download.
     *
     * @arg string path - Virtual path of the file to be added, relative to garrysmod/. Do not add .bz2 to the filepath. Do not put gamemodes/[WILDCARD]gamemodename[WILDCARD]/content/ or addons/[WILDCARD]addonname[WILDCARD]/ into the path.
     */
    function AddFile(path: string): void;

    /**
     * Adds the specified file to the files the client should download.
     *
     * @arg string path - Path of the file to be added, relative to garrysmod/
     */
    function AddSingleFile(path: string): void;

    /**
     * Adds a workshop addon for the client to download before entering the server.
     *
     * @arg string workshopid - The workshop id of the file. This cannot be a collection.
     */
    function AddWorkshop(workshopid: string): void;
}
