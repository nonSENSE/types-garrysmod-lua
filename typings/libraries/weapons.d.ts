/** @noSelfInFile */
declare namespace weapons {
    /**
     * Get a copy of weapon table by name. This function also inherits fields from the weapon's base class, unlike weapons.GetStored.
     *
     * @arg string classname - Class name of weapon to retrieve
     * @return {object} The retrieved table or nil
     */
    function Get(classname: string): object;

    /**
     * Get a list of all the registered SWEPs. This does not include weapons added to spawnmenu manually.
     *
     * @return {object} List of all the registered SWEPs
     */
    function GetList(): object;

    /**
     * Gets the REAL weapon table, not a copy. The produced table does not inherit fields from the weapon's base class, unlike weapons.Get.
     *
     * @arg string weapon_class - Weapon class to retrieve weapon table of
     * @return {object} The weapon table
     */
    function GetStored(weapon_class: string): object;

    /**
     * Checks if name is based on base
     *
     * @arg string name - Entity's class name to be checked
     * @arg string base - Base class name to be checked
     * @return {boolean} Returns true if class name is based on base, else false.
     */
    function IsBasedOn(name: string, base: string): boolean;

    /**
     * Called after all SWEPS have been loaded and runs baseclass.Set on each one.
     */
    function OnLoaded(): void;

    /**
     * Registers a Scripted Weapon (SWEP) class manually. When the engine spawns an entity, weapons registered with this function will be created if the class names match.
     *
     * @arg object swep_table - The SWEP table
     * @arg string classname - Classname to assign to that swep
     */
    function Register(swep_table: object, classname: string): void;
}
