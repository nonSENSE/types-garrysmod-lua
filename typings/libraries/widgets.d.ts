/** @noSelfInFile */
declare namespace widgets {
    /**
     * Automatically called to update all widgets.
     *
     * @arg Player ply - The player
     * @arg CMoveData mv - Player move data
     */
    function PlayerTick(ply: Player, mv: CMoveData): void;

    /**
     * Renders a widget. Normally you won't need to call this.
     *
     * @arg Entity ent - Widget entity to render
     */
    function RenderMe(ent: Entity): void;
}
