/** @noSelfInFile */
declare namespace util {
    /**
 * Adds the specified string to a string table, which will cache it and network it to all clients automatically.
* Whenever you want to create a net message with net.Start, you must add the name of that message as a networked string via this function.
* 
* @arg string str - Adds the specified string to the string table.
* @return {number} The id of the string that was added to the string table.
Same as calling util.NetworkStringToID.
 */
    function AddNetworkString(str: string): number;

    /**
     * Function used to calculate aim vector from 2D screen position. It is used in SuperDOF calculate Distance.
     *
     * @arg Angle ViewAngles - View angles
     * @arg number ViewFOV - View Field of View
     * @arg number x - Mouse X position
     * @arg number y - Mouse Y position
     * @arg number scrWidth - Screen width
     * @arg number scrHeight - Screen height
     * @return {Vector} Calculated aim vector
     */
    function AimVector(
        ViewAngles: Angle,
        ViewFOV: number,
        x: number,
        y: number,
        scrWidth: number,
        scrHeight: number
    ): Vector;

    /**
     * Decodes the specified string from base64.
     *
     * @arg string str - String to decode.
     * @return {string} The raw bytes of the decoded string.
     */
    function Base64Decode(str: string): string;

    /**
     * Encodes the specified string to base64.
     *
     * @arg string str - String to encode.
     * @arg boolean [inline=false] - true to disable RFC 2045 compliance (newline every 76th character)
     * @return {string} Base 64 encoded string.
     */
    function Base64Encode(str: string, inline?: boolean): string;

    /**
     * Applies explosion damage to all entities in the specified radius.
     *
     * @arg Entity inflictor - The entity that caused the damage.
     * @arg Entity attacker - The entity that attacked.
     * @arg Vector damageOrigin - The center of the explosion
     * @arg number damageRadius - The radius in which entities will be damaged.
     * @arg number damage - The amount of damage to be applied.
     */
    function BlastDamage(
        inflictor: Entity,
        attacker: Entity,
        damageOrigin: Vector,
        damageRadius: number,
        damage: number
    ): void;

    /**
     * Applies spherical damage based on damage info to all entities in the specified radius.
     *
     * @arg CTakeDamageInfo dmg - The information about the damage
     * @arg Vector damageOrigin - Center of the spherical damage
     * @arg number damageRadius - The radius in which entities will be damaged.
     */
    function BlastDamageInfo(dmg: CTakeDamageInfo, damageOrigin: Vector, damageRadius: number): void;

    /**
     * Compresses the given string using the LZMA algorithm.
     *
     * @arg string str - String to compress.
     * @return {string} The compressed string, or nil if the input string was zero length ("").
     */
    function Compress(str: string): string;

    /**
     * Generates the CRC Checksum of the specified string.
     *
     * @arg string stringToHash - The string to calculate the checksum of.
     * @return {string} The unsigned 32 bit checksum.
     */
    function CRC(stringToHash: string): string;

    /**
     * Returns the current date formatted like '2012-10-31 18-00-00'
     *
     * @return {string} date
     */
    function DateStamp(): string;

    /**
     * Performs a trace and paints a decal to the surface hit.
     *
     * @arg string name - The name of the decal to paint.
     * @arg Vector start - The start of the trace.
     * @arg Vector end - The end of the trace.
     * @arg Entity [filter=NULL] - If set, the decal will not be able to be placed on given entity. Can also be a table of entities.
     */
    function Decal(name: string, start: Vector, end: Vector, filter?: Entity): void;

    /**
 * Performs a trace and paints a decal to the surface hit.
* 
* @arg IMaterial material - The name of the decal to paint. Can be retrieved with util.DecalMaterial.
* @arg Entity ent - The entity to apply the decal to
* @arg Vector position - The position of the decal.
* @arg Vector normal - The direction of the decal.
* @arg Color color - The color of the decal. Uses the Color.
This only works when used on a brush model and only if the decal material has set $vertexcolor to 1.
* @arg number w - The width scale of the decal.
* @arg number h - The height scale of the decal.
 */
    function DecalEx(
        material: IMaterial,
        ent: Entity,
        position: Vector,
        normal: Vector,
        color: Color,
        w: number,
        h: number
    ): void;

    /**
     * Gets the full material path by the decal name. Used with util.DecalEx.
     *
     * @arg string decalName - Name of the decal.
     * @return {string} Material path of the decal.
     */
    function DecalMaterial(decalName: string): string;

    /**
     * Decompresses the given string using LZMA algorithm. Used to decompress strings previously compressed with util.Compress.
     *
     * @arg string compressedString - The compressed string to decompress.
     * @arg number [maxSize=NaN] - The maximal size in bytes it will decompress.
     * @return {string} The original, decompressed string or an empty string on failure or invalid input.
     */
    function Decompress(compressedString: string, maxSize?: number): string;

    /**
     * Gets the distance between a line and a point in 3d space.
     *
     * @arg Vector lineStart - Start of the line.
     * @arg Vector lineEnd - End of the line.
     * @arg Vector pointPos - The position of the point.
     * @return {number} Distance from line.
     * @return {Vector} Nearest point on line.
     * @return {number} Distance along line from start.
     */
    function DistanceToLine(
        lineStart: Vector,
        lineEnd: Vector,
        pointPos: Vector
    ): LuaMultiReturn<[number, Vector, number]>;

    /**
 * Creates an effect with the specified data.
* 
* @arg string effectName - The name of the effect to create.
You can find a list of built-in engine effects here. You can create your own, example effects can be found here and here.
* @arg CEffectData effectData - The effect data describing the effect.
* @arg boolean [allowOverride=true] - Whether Lua-defined effects should override engine-defined effects with the same name for this/single function call.
* @arg CRecipientFilter [ignorePredictionOrRecipientFilter=nil] - Can either be a boolean to ignore the prediction filter or a CRecipientFilter.
Set this to true if you wish to call this function in multiplayer from server.
 */
    function Effect(
        effectName: string,
        effectData: CEffectData,
        allowOverride?: boolean,
        ignorePredictionOrRecipientFilter?: CRecipientFilter
    ): void;

    /**
 * Returns a table containing the info about the model.
* 
* @arg string mdl - Model path
* @return {number} The model info as a table with the following keys:

number SkinCount - Identical to Entity:SkinCount.
string KeyValues - Valve key-value formatted info about the model's physics (Constraint Info, etc). This is limited to 4096 characters.
string ModelKeyValues - Valve key-value formatted info about the model ($keyvalues command in the .qc of the model), if present
 */
    function GetModelInfo(mdl: string): number;

    /**
 * Returns a table of visual meshes of given model.
* 
* @arg string model - The full path to a model to get the visual meshes of.
* @arg number [lod=0]
* @arg number [bodygroupMask=0]
* @return {string} A table of tables with the following format:

string material - The material of the specific mesh
table triangles - A table of Structures/MeshVertexes ready to be fed into IMesh:BuildFromTriangles
table verticies - A table of Structures/MeshVertexes representing all the vertices of the mesh. This table is used internally to generate the "triangles" table.

Each Structures/MeshVertex returned also has an extra table of tables field called "weights" with the following data:

number boneID - The bone this vertex is attached to
number weight - How "strong" this vertex is attached to the bone. A vertex can be attached to multiple bones at once.
* @return {number} A table of tables containing the model bind pose (where the keys are the bone ID) with the following contents:

number parent - The ID of the parent bone.
VMatrix matrix - The bone's bind transform in model (not bone) space.
 */
    function GetModelMeshes(model: string, lod?: number, bodygroupMask?: number): LuaMultiReturn<[string, number]>;

    /**
     * Gets PData of an offline player using their SteamID
     *
     * @arg string steamID - SteamID of the player
     * @arg string name - Variable name to get the value of
     * @arg string def - The default value, in case there's nothing stored
     * @return {string} The stored value
     */
    function GetPData(steamID: string, name: string, def: string): string;

    /**
     * Creates a new PixVis handle. See util.PixelVisible.
     *
     * @return {any} PixVis
     */
    function GetPixelVisibleHandle(): any;

    /**
     * Utility function to quickly generate a trace table that starts at the players view position, and ends 32768 units along a specified direction.
     *
     * @arg Player ply - The player the trace should be based on
     * @arg Vector [dir=ply:GetAimVector()] - The direction of the trace
     * @return {TraceStruct} The trace data. See Structures/Trace
     */
    function GetPlayerTrace(ply: Player, dir?: Vector): TraceStruct;

    /**
     * Gets information about the sun position and obstruction or nil if there is no sun.
     *
     * @return {SunInfoStruct} The sun info. See Structures/SunInfo
     */
    function GetSunInfo(): SunInfoStruct;

    /**
 * Returns data of a surface property at given ID.
* 
* @arg number id - Surface property ID. You can get it from Structures/TraceResult.
* @return {SurfacePropertyDataStruct} The data or no value if there is no valid surface property at given index.
See Structures/SurfacePropertyData
 */
    function GetSurfaceData(id: number): SurfacePropertyDataStruct;

    /**
     * Returns the matching surface property index for the given surface property name.
     *
     * @arg string surfaceName - The name of the surface.
     * @return {number} The surface property index, or -1 if name doesn't correspond to a valid surface property.
     */
    function GetSurfaceIndex(surfaceName: string): number;

    /**
     * Returns the name of a surface property at given ID.
     *
     * @arg number id - Surface property ID. You can get it from Structures/TraceResult.
     * @return {string} The name or an empty string if there is no valid surface property at given index.
     */
    function GetSurfacePropName(id: number): string;

    /**
 * Returns a table of all SteamIDs that have a usergroup.
* 
* @return {string} The table of users. The table consists of SteamID-Table pairs, where the table has 2 fields:
string name - Players name
string group - The players user group
 */
    function GetUserGroups(): string;

    /**
 * Performs a "ray" box intersection and returns position, normal and the fraction.
* 
* @arg Vector rayStart - Origin/start position of the ray.
* @arg Vector rayDelta - The ray vector itself. This can be thought of as: the ray end point relative to the start point.
Note that in this implementation, the ray is not infinite - it's only a segment.
* @arg Vector boxOrigin - The center of the box.
* @arg Angle boxAngles - The angles of the box.
* @arg Vector boxMins - The min position of the box.
* @arg Vector boxMaxs - The max position of the box.
* @return {Vector} Hit position, nil if not hit.
* @return {Vector} Normal/direction vector, nil if not hit.
* @return {number} Fraction of trace used, nil if not hit.
 */
    function IntersectRayWithOBB(
        rayStart: Vector,
        rayDelta: Vector,
        boxOrigin: Vector,
        boxAngles: Angle,
        boxMins: Vector,
        boxMaxs: Vector
    ): LuaMultiReturn<[Vector, Vector, number]>;

    /**
     * Performs a ray-plane intersection and returns the hit position or nil.
     *
     * @arg Vector rayOrigin - Origin/start position of the ray.
     * @arg Vector rayDirection - The direction of the ray.
     * @arg Vector planePosition - Any position of the plane.
     * @arg Vector planeNormal - The normal vector of the plane.
     * @return {Vector} The position of intersection, nil if not hit.
     */
    function IntersectRayWithPlane(
        rayOrigin: Vector,
        rayDirection: Vector,
        planePosition: Vector,
        planeNormal: Vector
    ): Vector;

    /**
     * Checks if a certain position is within the world bounds.
     *
     * @arg Vector position - Position to check.
     * @return {boolean} Whether the vector is in world.
     */
    function IsInWorld(position: Vector): boolean;

    /**
     * Checks if the model is loaded in the game.
     *
     * @arg string modelName - Name/Path of the model to check.
     * @return {boolean} Returns true if the model is loaded in the game; otherwise false.
     */
    function IsModelLoaded(modelName: string): boolean;

    /**
     * Check whether the skybox is visible from the point specified.
     *
     * @arg Vector position - The position to check the skybox visibility from.
     * @return {boolean} Whether the skybox is visible from the position.
     */
    function IsSkyboxVisibleFromPoint(position: Vector): boolean;

    /**
     * Checks if the specified model is valid.
     *
     * @arg string modelName - Name/Path of the model to check.
     * @return {boolean} Whether the model is valid or not. Returns false clientside if the model is not precached by the server.
     */
    function IsValidModel(modelName: string): boolean;

    /**
     * Checks if given numbered physics object of given entity is valid or not. Most useful for ragdolls.
     *
     * @arg Entity ent - The entity
     * @arg number physobj - Number of the physics object to test
     * @return {boolean} true is valid, false otherwise
     */
    function IsValidPhysicsObject(ent: Entity, physobj: number): boolean;

    /**
     * Checks if the specified prop is valid.
     *
     * @arg string modelName - Name/Path of the model to check.
     * @return {boolean} Returns true if the specified prop is valid; otherwise false.
     */
    function IsValidProp(modelName: string): boolean;

    /**
     * Checks if the specified model name points to a valid ragdoll.
     *
     * @arg string ragdollName - Name/Path of the ragdoll model to check.
     * @return {boolean} Returns true if the specified model name points to a valid ragdoll; otherwise false.
     */
    function IsValidRagdoll(ragdollName: string): boolean;

    /**
     * Converts a JSON string to a Lua table.
     *
     * @arg string json - The JSON string to convert.
     * @return {object} The table containing converted information. Returns nothing on failure.
     */
    function JSONToTable(json: string): object;

    /**
     * Converts a KeyValue string to a Lua table.
     *
     * @arg string keyValues - The KeyValue string to convert.
     * @arg boolean [usesEscapeSequences=false] - If set to true, will replace \t, \n, " and \ in the input text with their escaped variants
     * @arg boolean [preserveKeyCase=false] - Whether we should preserve key case (may fail) or not (always lowercase)
     * @return {object} The converted table
     */
    function KeyValuesToTable(keyValues: string, usesEscapeSequences?: boolean, preserveKeyCase?: boolean): object;

    /**
     * Similar to util.KeyValuesToTable but it also preserves order of keys.
     *
     * @arg string keyvals - The key value string
     * @arg boolean [usesEscapeSequences=false] - If set to true, will replace \t, \n, " and \ in the input text with their escaped variants
     * @arg boolean [preserveKeyCase=false] - Whether we should preserve key case (may fail) or not (always lowercase)
     * @return {object} The output table
     */
    function KeyValuesToTablePreserveOrder(
        keyvals: string,
        usesEscapeSequences?: boolean,
        preserveKeyCase?: boolean
    ): object;

    /**
     * Returns a vector in world coordinates based on an entity and local coordinates
     *
     * @arg Entity ent - The entity lpos is local to
     * @arg Vector lpos - Coordinates local to the ent
     * @arg number bonenum - The bonenumber of the ent lpos is local to
     * @return {Vector} wpos
     */
    function LocalToWorld(ent: Entity, lpos: Vector, bonenum: number): Vector;

    /**
     * Generates the MD5 Checksum of the specified string.
     *
     * @arg string stringToHash - The string to calculate the checksum of.
     * @return {string} The MD5 hex string of the checksum.
     */
    function MD5(stringToHash: string): string;

    /**
     * Returns the networked string associated with the given ID from the string table.
     *
     * @arg number stringTableID - ID to get the associated string from.
     * @return {string} The networked string, or nil if it wasn't found.
     */
    function NetworkIDToString(stringTableID: number): string;

    /**
     * Returns the networked ID associated with the given string from the string table.
     *
     * @arg string networkString - String to get the associated networked ID from.
     * @return {number} The networked ID of the string, or 0 if it hasn't been networked with util.AddNetworkString.
     */
    function NetworkStringToID(networkString: string): number;

    /**
     * Formats a float by stripping off extra 0's and .'s.
     *
     * @arg number float - The float to format.
     * @return {string} Formatted float.
     */
    function NiceFloat(float: number): string;

    /**
     * Creates a tracer effect with the given parameters.
     *
     * @arg string name - The name of the tracer effect.
     * @arg Vector startPos - The start position of the tracer.
     * @arg Vector endPos - The end position of the tracer.
     * @arg boolean doWhiz - Play the hit miss(whiz) sound.
     */
    function ParticleTracer(name: string, startPos: Vector, endPos: Vector, doWhiz: boolean): void;

    /**
     * Creates a tracer effect with the given parameters.
     *
     * @arg string name - The name of the tracer effect.
     * @arg Vector startPos - The start position of the tracer.
     * @arg Vector endPos - The end position of the tracer.
     * @arg boolean doWhiz - Play the hit miss(whiz) sound.
     * @arg number entityIndex - Entity index of the emitting entity.
     * @arg number attachmentIndex - Attachment index to be used as origin.
     */
    function ParticleTracerEx(
        name: string,
        startPos: Vector,
        endPos: Vector,
        doWhiz: boolean,
        entityIndex: number,
        attachmentIndex: number
    ): void;

    /**
 * Returns the visibility of a sphere in the world.
* 
* @arg Vector position - The center of the visibility test.
* @arg number radius - The radius of the sphere to check for visibility.
* @arg any PixVis - The PixVis handle created with util.GetPixelVisibleHandle.
Don't use the same handle twice per tick or it will give unpredictable results.
* @return {number} Visibility, ranges from 0-1. 0 when none of the area is visible, 1 when all of it is visible.
 */
    function PixelVisible(position: Vector, radius: number, PixVis: any): number;

    /**
     * Returns the contents of the position specified.
     *
     * @arg Vector position - Position to get the contents sample from.
     * @return {number} Contents bitflag, see Enums/CONTENTS
     */
    function PointContents(position: Vector): number;

    /**
     * Precaches a model for later use. Model is cached after being loaded once.
     *
     * @arg string modelName - The model to precache.
     */
    function PrecacheModel(modelName: string): void;

    /**
     * Precaches a sound for later use. Sound is cached after being loaded once.
     *
     * @arg string soundName - The sound to precache.
     */
    function PrecacheSound(soundName: string): void;

    /**
     * Performs a trace with the given origin, direction, and filter.
     *
     * @arg Vector origin - The origin of the trace.
     * @arg Vector dir - The direction of the trace times the distance of the trace. This is added to the origin to determine the endpos.
     * @arg Entity [filter=nil] - Entity which should be ignored by the trace. Can also be a table of entities or a function - see Structures/Trace.
     * @return {TraceResultStruct} Trace result. See Structures/TraceResult.
     */
    function QuickTrace(origin: Vector, dir: Vector, filter?: Entity): TraceResultStruct;

    /**
     * Removes PData of offline player using his SteamID.
     *
     * @arg string steamID - SteamID of the player
     * @arg string name - Variable name to remove
     */
    function RemovePData(steamID: string, name: string): void;

    /**
 * Makes the screen shake.
* 
* @arg Vector pos - The origin of the effect.
Does nothing on client.
* @arg number amplitude - The strength of the effect.
* @arg number frequency - The frequency of the effect in hertz.
* @arg number duration - The duration of the effect in seconds.
* @arg number radius - The range from the origin within which views will be affected, in Hammer units.
Does nothing on client.
 */
    function ScreenShake(pos: Vector, amplitude: number, frequency: number, duration: number, radius: number): void;

    /**
     * Sets PData for offline player using their SteamID
     *
     * @arg string steamID - SteamID of the player
     * @arg string name - Variable name to store the value in
     * @arg any value - The value to store
     */
    function SetPData(steamID: string, name: string, value: any): void;

    /**
     * Generates the SHA-1 Checksum of the specified string.
     *
     * @arg string stringToHash - The string to calculate the checksum of.
     * @return {string} The SHA-1 hex string of the checksum.
     */
    function SHA1(stringToHash: string): string;

    /**
     * Generates the SHA-256 Checksum of the specified string.
     *
     * @arg string stringToHash - The string to calculate the checksum of.
     * @return {string} The SHA-256 hex string of the checksum.
     */
    function SHA256(stringToHash: string): string;

    /**
     * Generates a random float value that should be the same on client and server.
     *
     * @arg string uniqueName - The seed for the random value
     * @arg number min - The minimum value of the random range
     * @arg number max - The maximum value of the random range
     * @arg number [additionalSeed=0] - The additional seed
     * @return {number} The random float value
     */
    function SharedRandom(uniqueName: string, min: number, max: number, additionalSeed?: number): number;

    /**
     * Adds a trail to the specified entity.
     *
     * @arg Entity ent - Entity to attach trail to
     * @arg number attachmentID - Attachment ID of the entities model to attach trail to. If you are not sure, set this to 0
     * @arg any color - Color of the trail, use Color
     * @arg boolean additive - Should the trail be additive or not
     * @arg number startWidth - Start width of the trail
     * @arg number endWidth - End width of the trail
     * @arg number lifetime - How long it takes to transition from startWidth to endWidth
     * @arg number textureRes - The resolution of trails texture. A good value can be calculated using this formula: 1 / ( startWidth + endWidth ) * 0.5
     * @arg string texture - Path to the texture to use as a trail.
     * @return {Entity} Entity of created trail (env_spritetrail)
     */
    function SpriteTrail(
        ent: Entity,
        attachmentID: number,
        color: any,
        additive: boolean,
        startWidth: number,
        endWidth: number,
        lifetime: number,
        textureRes: number,
        texture: string
    ): Entity;

    /**
     * Returns a new Stack object.
     *
     * @return {Stack} A brand new stack object.
     */
    function Stack(): Stack;

    /**
     * Given a 64bit SteamID will return a STEAM_0: style Steam ID
     *
     * @arg string id - The 64 bit Steam ID
     * @return {string} STEAM_0 style Steam ID
     */
    function SteamIDFrom64(id: string): string;

    /**
     * Given a STEAM_0 style Steam ID will return a 64bit Steam ID
     *
     * @arg string id - The STEAM_0 style id
     * @return {string} 64bit Steam ID or 0 (as a string) on fail
     */
    function SteamIDTo64(id: string): string;

    /**
     * Converts a string to the specified type.
     *
     * @arg string str - The string to convert
     * @arg string typename - The type to attempt to convert the string to. This can be vector, angle, float, int, bool, or string (case insensitive).
     * @return {any} The result of the conversion, or nil if a bad type is specified.
     */
    function StringToType(str: string, typename: string): any;

    /**
     * Converts a table to a JSON string.
     *
     * @arg object table - Table to convert.
     * @arg boolean [prettyPrint=false] - Format and indent the JSON.
     * @return {string} A JSON formatted string containing the serialized data
     */
    function TableToJSON(table: object, prettyPrint?: boolean): string;

    /**
     * Converts the given table into a key value string.
     *
     * @arg object table - The table to convert.
     * @arg string [parentKey="TableToKeyValues"] - The parent key.
     * @return {string} KeyValueString
     */
    function TableToKeyValues(table: object, parentKey?: string): string;

    /**
 * Creates a timer object.
* 
* @arg number [startdelay=0] - How long you want the timer to be.
* @return {object} A timer object. It has next methods:
Reset() - Resets the timer to nothing
Start( time ) - Starts the timer, call with end time
Started() - Returns true if the timer has been started
Elapsed() - Returns true if the time has elapsed
 */
    function Timer(startdelay?: number): object;

    /**
     * Returns the time since this function has been last called
     *
     * @return {number} Time since this function has been last called in ms
     */
    function TimerCycle(): number;

    /**
     * Converts string or a number to a bool, if possible. Alias of tobool.
     *
     * @arg any input - A string or a number to convert.
     * @return {boolean} False if the input is equal to the string or boolean "false", if the input is equal to the string or number "0", or if the input is nil. Returns true otherwise.
     */
    function tobool(input: any): boolean;

    /**
     * Runs a trace using the entity's collisionmodel between two points. This does not take the entity's angles into account and will trace its unrotated collisionmodel.
     *
     * @arg TraceStruct tracedata - Trace data. See Structures/Trace
     * @arg Entity ent - The entity to use
     * @return {TraceResultStruct} Trace result. See Structures/TraceResult
     */
    function TraceEntity(tracedata: TraceStruct, ent: Entity): TraceResultStruct;

    /**
     * Traces from one entity to another.
     *
     * @arg Entity ent1 - The first entity to trace from
     * @arg Entity ent2 - The second entity to trace to
     * @return {TraceResultStruct} Trace result. See Structures/TraceResult
     */
    function TraceEntityHull(ent1: Entity, ent2: Entity): TraceResultStruct;

    /**
     * Performs an AABB hull (axis-aligned bounding box, aka not rotated) trace with the given trace data.
     *
     * @arg HullTraceStruct TraceData - The trace data to use. See Structures/HullTrace
     * @return {TraceResultStruct} Trace result. See Structures/TraceResult
     */
    function TraceHull(TraceData: HullTraceStruct): TraceResultStruct;

    /**
 * Performs a trace with the given trace data.
* 
* @arg TraceStruct TraceData - The trace data to use. See Structures/Trace
* @return {TraceResultStruct} Trace result. See Structures/TraceResult.
Can return nil if game.GetWorld or its physics object is invalid. This will be the case for any traces done before GM:InitPostEntity is called.
 */
    function TraceLine(TraceData: TraceStruct): TraceResultStruct;

    /**
     * Converts a type to a (nice, but still parsable) string
     *
     * @arg any input - What to convert
     * @return {string} Converted string
     */
    function TypeToString(input: any): string;
}
