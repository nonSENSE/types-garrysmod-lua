/** @noSelfInFile */
declare namespace menubar {
    /**
     * Creates the menu bar ( The bar at the top of the screen when holding C or Q in sandbox ) and docks it to the top of the screen. It will not appear.
     */
    function Init(): void;

    /**
     * Checks if the supplied panel is parent to the menubar
     *
     * @arg Panel pnl - The panel to check
     * @return {boolean} Is parent or not
     */
    function IsParent(pnl: Panel): boolean;

    /**
     * Parents the menubar to the panel and displays the menubar.
     *
     * @arg Panel pnl - The panel to parent to
     */
    function ParentTo(pnl: Panel): void;
}
