/** @noSelfInFile */
declare namespace team {
    /**
     * Increases the score of the given team
     *
     * @arg number index - Index of the team
     * @arg number increment - Amount to increase the team's score by
     */
    function AddScore(index: number, increment: number): void;

    /**
     * Returns the team index of the team with the least players. Falls back to TEAM_UNASSIGNED
     *
     * @return {number} Team index
     */
    function BestAutoJoinTeam(): number;

    /**
     * Returns a table consisting of information on every defined team
     *
     * @return {object} Team info
     */
    function GetAllTeams(): object;

    /**
     * Returns the selectable classes for the given team. This can be added to with team.SetClass
     *
     * @arg number index - Index of the team
     * @return {object} Selectable classes
     */
    function GetClass(index: number): object;

    /**
     * Returns the team's color.
     *
     * @arg number teamIndex - The team index.
     * @return {Color} The team's color as a Color.
     */
    function GetColor(teamIndex: number): Color;

    /**
     * Returns the name of the team.
     *
     * @arg number teamIndex - The team index.
     * @return {string} The team name. If the team is not defined, returns an empty string.
     */
    function GetName(teamIndex: number): string;

    /**
     * Returns a table with all player of the specified team.
     *
     * @arg number teamIndex - The team index.
     * @return {Player} A table of Players that belong to the requested team.
     */
    function GetPlayers(teamIndex: number): Player;

    /**
     * Returns the score of the team.
     *
     * @arg number teamIndex - The team index.
     * @return {number} score
     */
    function GetScore(teamIndex: number): number;

    /**
     * Returns a table of valid spawnpoint classes the team can use. These are set with team.SetSpawnPoint.
     *
     * @arg number index - Index of the team
     * @return {object} Valid spawnpoint classes
     */
    function GetSpawnPoint(index: number): object;

    /**
     * Returns a table of valid spawnpoint entities the team can use. These are set with  team.SetSpawnPoint.
     *
     * @arg number index - Index of the team
     * @return {object} Valid spawnpoint entities
     */
    function GetSpawnPoints(index: number): object;

    /**
     * Returns if a team is joinable or not. This is set in team.SetUp.
     *
     * @arg number index - The index of the team.
     * @return {boolean} True if the team is joinable. False otherwise.
     */
    function Joinable(index: number): boolean;

    /**
     * Returns the amount of players in a team.
     *
     * @arg number teamIndex - The team index.
     * @return {number} playerCount
     */
    function NumPlayers(teamIndex: number): number;

    /**
     * Sets valid classes for use by a team. Classes can be created using player_manager.RegisterClass
     *
     * @arg number index - Index of the team
     * @arg any classes - A class ID or table of class IDs
     */
    function SetClass(index: number, classes: any): void;

    /**
     * Sets the team's color.
     *
     * @arg number teamIndex - The team index.
     * @arg Color color - The team's new color as a Color.
     */
    function SetColor(teamIndex: number, color: Color): void;

    /**
     * Sets the score of the given team
     *
     * @arg number index - Index of the team
     * @arg number score - The team's new score
     */
    function SetScore(index: number, score: number): void;

    /**
     * Sets valid spawnpoint classes for use by a team.
     *
     * @arg number index - Index of the team
     * @arg any classes - A spawnpoint classname or table of spawnpoint classnames
     */
    function SetSpawnPoint(index: number, classes: any): void;

    /**
     * Creates a new team.
     *
     * @arg number teamIndex - The team index.
     * @arg string teamName - The team name.
     * @arg Color teamColor - The team color. Uses the Color.
     * @arg boolean [isJoinable=true] - Whether the team is joinable or not.
     */
    function SetUp(teamIndex: number, teamName: string, teamColor: Color, isJoinable?: boolean): void;

    /**
     * Returns the total number of deaths of all players in the team.
     *
     * @arg number index - The team index.
     * @return {number} Total deaths in team.
     */
    function TotalDeaths(index: number): number;

    /**
     * Get's the total frags in a team.
     *
     * @arg Entity Entity_or_number - Entity or number.
     * @return {number} index
     */
    function TotalFrags(Entity_or_number: Entity): number;

    /**
     * Returns true if the given team index is valid
     *
     * @arg number index - Index of the team
     * @return {boolean} Is valid
     */
    function Valid(index: number): boolean;
}
