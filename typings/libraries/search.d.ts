/** @noSelfInFile */
declare namespace search {
    /**
 * Adds a search result provider. For examples, see gamemodes/sandbox/gamemode/cl_search_models.lua
* 
* @arg GMLua.CallbackNoContext provider - Provider function. It has one argument: string searchQuery
You must return a list of tables structured like this:

string text - Text to "Copy to clipboard"
function func - Function to use/spawn the item
Panel icon - A panel to add to spawnmenu
table words - A table of words?
* @arg string [id="nil"] - If provided, ensures that only one provider exists with the given ID at a time.
 */
    function AddProvider(provider: GMLua.CallbackNoContext, id?: string): void;

    /**
     * Retrieves search results.
     *
     * @arg string query - Search query
     * @arg string [types="nil"] - If set, only searches given provider type(s), instead of everything. For example "tool" will only search tools in Sandbox. Can be a table for multiple types.
     * @arg number [maxResults=1024] - How many results to stop at
     * @return {object} A table of results
     */
    function GetResults(query: string, types?: string, maxResults?: number): object;
}
