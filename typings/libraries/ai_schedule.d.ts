/** @noSelfInFile */
declare namespace ai_schedule {
    /**
     * Creates a schedule for scripted NPC.
     *
     * @arg string name - Name of the schedule.
     * @return {any} A table containing schedule information to be used with ENTITY:StartSchedule.
     */
    function New(name: string): any;
}
