/** @noSelfInFile */
declare namespace presets {
    /**
     * Adds preset to a preset group.
     *
     * @arg string groupname - The preset group name, usually it's tool class name.
     * @arg string name - Preset name, must be unique.
     * @arg object values - A table of preset console commands.
     */
    function Add(groupname: string, name: string, values: object): void;

    /**
     * Used internally to tell the player that the name they tried to use in their preset is not acceptable.
     */
    function BadNameAlert(): void;

    /**
     * Returns whether a preset with given name exists or not
     *
     * @arg string type - The preset group name, usually it's tool class name.
     * @arg string name - Name of the preset to test
     * @return {boolean} true if the preset does exist
     */
    function Exists(type: string, name: string): boolean;

    /**
     * Returns a table with preset names and values from a single preset group.
     *
     * @arg string groupname - Preset group name.
     * @return {object} All presets in specified group.
     */
    function GetTable(groupname: string): object;

    /**
     * Used internally to ask the player if they want to override an already existing preset.
     *
     * @arg GMLua.CallbackNoContext callback
     */
    function OverwritePresetPrompt(callback: GMLua.CallbackNoContext): void;

    /**
     * Removes a preset entry from a preset group.
     *
     * @arg string groupname - Preset group to remove from
     * @arg string name - Name of preset to remove
     */
    function Remove(groupname: string, name: string): void;

    /**
     * Renames preset.
     *
     * @arg string groupname - Preset group name
     * @arg string oldname - Old preset name
     * @arg string newname - New preset name
     */
    function Rename(groupname: string, oldname: string, newname: string): void;
}
