/** @noSelfInFile */
declare namespace engine {
    /**
     * Returns the name of the currently running gamemode.
     *
     * @return {string} The active gamemode's name. This is the name of the gamemode's folder.
     */
    function ActiveGamemode(): string;

    /**
     * Closes the server and completely exits.
     */
    function CloseServer(): void;

    /**
     * Returns a list of addons the player have subscribed to on the workshop.
     *
     * @return {object} A table with 8 keys (downloaded, models, title, file, mounted, wsid, size, updated).
     */
    function GetAddons(): object;

    /**
     * When starting playing a demo, engine.GetDemoPlaybackTick will be reset and its old value will be added to this functions return value.
     *
     * @return {number}
     */
    function GetDemoPlaybackStartTick(): number;

    /**
     * Current tick of currently loaded demo.
     *
     * @return {number} The amount of ticks of currently loaded demo.
     */
    function GetDemoPlaybackTick(): number;

    /**
     * Returns time scale of demo playback.
     *
     * @return {number} The time scale of demo playback, value of demo_timescale console variable.
     */
    function GetDemoPlaybackTimeScale(): number;

    /**
     * Returns total amount of ticks of currently loaded demo.
     *
     * @return {number} Total amount of ticks of currently loaded demo.
     */
    function GetDemoPlaybackTotalTicks(): number;

    /**
     * Returns a table containing info for all installed gamemodes
     *
     * @return {object} gamemodes
     */
    function GetGamemodes(): object;

    /**
     * Returns an array of tables corresponding to all games from which Garry's Mod supports mounting content.
     *
     * @return {object} A table of tables containing all mountable games
     */
    function GetGames(): object;

    /**
     * Returns the UGC (demos, saves and dupes) the player have subscribed to on the workshop.
     *
     * @return {object} Returns a table with 5 keys (title, type, tags, wsid, timeadded)
     */
    function GetUserContent(): object;

    /**
     * Returns true if we're currently playing a demo.
     *
     * @return {boolean} Whether the game is currently playing a demo or not.
     */
    function IsPlayingDemo(): boolean;

    /**
     * Returns true if the game is currently recording a demo file (.dem) using gm_demo
     *
     * @return {boolean} Whether the game is currently recording a demo or not.
     */
    function IsRecordingDemo(): boolean;

    /**
     * This is a direct binding to the function engine->LightStyle. This function allows you to change the default light style of the map - so you can make lighting lighter or darker. You’ll need to call render.RedownloadAllLightmaps clientside to refresh the lightmaps to this new color.
     *
     * @arg number lightstyle - The lightstyle to edit. 0 to 63. If you want to edit map lighting, you want to set this to 0.
     * @arg string pattern - The pattern to change the lightstyle to. a is the darkest, z is the brightest. You can use stuff like "abcxyz" to make flashing patterns. The normal brightness for a map is m. Values over z are allowed, ~ for instance.
     */
    function LightStyle(lightstyle: number, pattern: string): void;

    /**
     * Loads a duplication from the local filesystem.
     *
     * @arg string dupeName - Name of the file. e.g, engine.OpenDupe("dupes/8b809dd7a1a9a375e75be01cdc12e61f.dupe")
     * @return {string} Compressed dupeData. Use util.JSONToTable to make it into a format useable by the duplicator tool.
     */
    function OpenDupe(dupeName: string): string;

    /**
     * Returns an estimate of the server's performance. Equivalent to calling FrameTime from the server, according to source code.
     *
     * @return {number} Frame time.
     * @return {number} Server Framerate Std Deviation.
     */
    function ServerFrameTime(): LuaMultiReturn<[number, number]>;

    /**
     * Sets the mounting options for mountable content.
     *
     * @arg string depotID - The depot id of the game to mount.
     * @arg boolean doMount - The mount state, true to mount, false to unmount
     */
    function SetMounted(depotID: string, doMount: boolean): void;

    /**
     * Returns the number of ticks since the game server started.
     *
     * @return {number} Number of ticks since the game server started.
     */
    function TickCount(): number;

    /**
     * Returns the number of seconds between each gametick.
     *
     * @return {number} Number of seconds between each gametick.
     */
    function TickInterval(): number;

    /**
     * Returns video recording settings set by video.Record. Used by Demo-To-Video feature.
     *
     * @return {VideoDataStruct} The video recording settings, see Structures/VideoData.
     */
    function VideoSettings(): VideoDataStruct;

    /**
     * Saves a duplication as a file.
     *
     * @arg string dupe - Dupe table, encoded by util.TableToJSON and compressed by util.Compress
     * @arg string jpeg - The dupe icon, created by render.Capture
     */
    function WriteDupe(dupe: string, jpeg: string): void;

    /**
     * Stores savedata into the game (can be loaded using the LoadGame menu)
     *
     * @arg string saveData - Data generated by gmsave.SaveMap
     * @arg string name - Name the save will have.
     * @arg number time - When the save was saved during the game (Put CurTime here)
     * @arg string map - The map the save is used for.
     */
    function WriteSave(saveData: string, name: string, time: number, map: string): void;
}
