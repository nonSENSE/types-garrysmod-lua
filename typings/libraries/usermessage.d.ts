/** @noSelfInFile */
declare namespace usermessage {
    /**
     * Returns a table of every usermessage hook
     *
     * @return {object} hooks
     */
    function GetTable(): object;

    /**
 * Sets a hook for the specified to be called when a usermessage with the specified name arrives.
* 
* @arg string name - The message name to hook to.
* @arg GMLua.CallbackNoContext callback - The function to be called if the specified message was received.
Parameters (Optional):

bf_read msg
vararg preArgs
* @arg args[] [preArgs=nil] - Arguments that are passed to the callback function when the hook is called. ring ring
 */
    function Hook(name: string, callback: GMLua.CallbackNoContext, ...preArgs: any[]): void;

    /**
     * Called by the engine when a usermessage arrives, this method calls the hook function specified by usermessage.Hook if any.
     *
     * @arg string name - The message name.
     * @arg bf_read msg - The message.
     */
    function IncomingMessage(name: string, msg: bf_read): void;
}
