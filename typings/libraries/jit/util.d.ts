/** @noSelfInFile */
declare namespace jit {
    export namespace util {
        /**
         * Returns bytecode of a function at a position.
         *
         * @arg GMLua.CallbackNoContext func - Function to retrieve bytecode from.
         * @arg number pos - Position of the bytecode to retrieve.
         * @return {number} bytecode instruction
         * @return {number} bytecode opcode
         */
        function funcbc(func: GMLua.CallbackNoContext, pos: number): LuaMultiReturn<[number, number]>;

        /**
         * Retrieves LuaJIT information about a given function, similarly to debug.getinfo. Possible table fields:
         *
         * @arg GMLua.CallbackNoContext func - Function or Proto to retrieve info about.
         * @arg number [pos=0]
         * @return {object} Information about the supplied function/proto.
         */
        function funcinfo(func: GMLua.CallbackNoContext, pos?: number): object;

        /**
 * Gets a constant at a certain index in a function.
* 
* @arg GMLua.CallbackNoContext func - Function to get constant from
* @arg number index - Constant index (counting down from the top of the function at -1)
* @return {any} The constant found.
This will return a proto for functions that are created inside the target function - see Example 2.
 */
        function funck(func: GMLua.CallbackNoContext, index: number): any;

        /**
         * Does the exact same thing as debug.getupvalue except it only returns the name, not the name and the object. The upvalue indexes also start at 0 rather than 1, so doing jit.util.funcuvname(func, 0) will get you the same name as debug.getupvalue(func, 1)
         *
         * @arg GMLua.CallbackNoContext func - Function to get the upvalue indexed from
         * @arg number index - The upvalue index, starting from 0
         * @return {string} The function returns nil if there is no upvalue with the given index, otherwise the name of the upvalue is returned
         */
        function funcuvname(func: GMLua.CallbackNoContext, index: number): string;

        /**
         * Gets the address of a function from a list of 20 functions, for the list see Ircalladdr Functions
         *
         * @arg number index - The index of the function address to get from the ircalladdr func array (starting from 0)
         * @return {number} The address of the function
         */
        function ircalladdr(index: number): number;

        /**
         *
         *
         * @arg number exitno - exit number to retrieve exit stub address from (gotten via jit.attach with the texit event)
         * @return {number} exitstub trace address
         */
        function traceexitstub(exitno: number): number;

        /**
         * Return table fields:
         *
         * @arg number trace - trace index to retrieve info for (gotten via jit.attach)
         * @return {object} trace info
         */
        function traceinfo(trace: number): object;

        /**
         *
         *
         * @arg number tr
         * @arg number index
         * @return {number} m
         * @return {number} ot
         * @return {number} op1
         * @return {number} op2
         * @return {number} prev
         */
        function traceir(tr: number, index: number): LuaMultiReturn<[number, number, number, number, number]>;

        /**
         *
         *
         * @arg number tr
         * @arg number index
         * @return {any} k
         * @return {number} t
         * @return {number} slot; optional
         */
        function tracek(tr: number, index: number): LuaMultiReturn<[any, number, number]>;

        /**
         *
         *
         * @arg number tr
         * @return {string} mcode
         * @return {number} address
         * @return {number} loop
         */
        function tracemc(tr: number): LuaMultiReturn<[string, number, number]>;

        /**
         * Return table fields:
         *
         * @arg number tr - trace index to retrieve snapshot for (gotten via jit.attach)
         * @arg number sn - snapshot index for trace (starting from 0 to nexit - 1, nexit gotten via jit.util.traceinfo)
         * @return {object} snapshot
         */
        function tracesnap(tr: number, sn: number): object;
    }
}
