/** @noSelfInFile */
declare namespace jit {
    export namespace opt {
        /**
         * JIT compiler optimization control. The opt sub-module provides the backend for the -O command line LuaJIT option.
         * You can also use it programmatically, e.g.:
         *
         * @arg args[] args
         */
        function start(...args: any[]): void;
    }
}
