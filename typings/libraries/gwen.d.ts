/** @noSelfInFile */
declare namespace GWEN {
    /**
 * Used in derma skins to create a bordered rectangle drawing function from an image. The texture is taken either from last argument or from SKIN.GwenTexture
* 
* @arg number x - The X coordinate on the texture
* @arg number y - The Y coordinate on the texture
* @arg number w - Width of the area on texture
* @arg number h - Height of the area on texture
* @arg number left - Left width of border
* @arg number top - Top width of border
* @arg number right - Right width of border
* @arg number bottom - Bottom width of border
* @arg IMaterial [source=nil] - Texture of source image to create a bordered rectangle from. Uses SKIN.GwenTexture if not set.
* @return {Function} The drawing function. Arguments are:

number x - X coordinate for the box
number y - Y coordinate for the box
number w - Width of the box
number h - Height of the box
table clr - Optional color, default is white. Uses the Color
 */
    function CreateTextureBorder(
        x: number,
        y: number,
        w: number,
        h: number,
        left: number,
        top: number,
        right: number,
        bottom: number,
        source?: IMaterial
    ): Function;

    /**
 * Used in derma skins to create a rectangle drawing function from an image. The rectangle will not be scaled, but instead it will be drawn in the center of the box. The texture is taken from SKIN.GwenTexture
* 
* @arg number x - The X coordinate on the texture
* @arg number y - The Y coordinate on the texture
* @arg number w - Width of the area on texture
* @arg number h - Height of the area on texture
* @return {Function} The drawing function. Arguments are:
number x - X coordinate for the box
number y - Y coordinate for the box
number w - Width of the box
number h - Height of the box
table clr - Optional color, default is white. Uses the Color
 */
    function CreateTextureCentered(x: number, y: number, w: number, h: number): Function;

    /**
 * Used in derma skins to create a rectangle drawing function from an image. The texture of the rectangle will be scaled. The texture is taken from SKIN.GwenTexture
* 
* @arg number x - The X coordinate on the texture
* @arg number y - The Y coordinate on the texture
* @arg number w - Width of the area on texture
* @arg number h - Height of the area on texture
* @return {Function} The drawing function. Arguments are:

number x - X coordinate for the box
number y - Y coordinate for the box
number w - Width of the box
number h - Height of the box
table clr - Optional color, default is white. Uses the Color
 */
    function CreateTextureNormal(x: number, y: number, w: number, h: number): Function;

    /**
     * When used in a material skin, it returns a color value from a point in the skin image.
     *
     * @arg number x - X position of the pixel to get the color from.
     * @arg number y - Y position of the pixel to get the color from.
     * @return {Color} The color of the point on the skin as a Color.
     */
    function TextureColor(x: number, y: number): Color;
}
