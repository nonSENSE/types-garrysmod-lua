/** @noSelfInFile */
declare namespace undo {
    /**
     * Adds an entity to the current undo block
     *
     * @arg Entity ent - The entity to add
     */
    function AddEntity(ent: Entity): void;

    /**
 * Adds a function to call when the current undo block is undone. Note that if an undo has a function, the player will always be notified when this undo is performed, even if the entity it is meant to undo no longer exists.
* 
* @arg GMLua.CallbackNoContext func - The function to call. First argument will be the Undo Structure, all subsequent arguments will be what was passed after this function in the argument below.
Returning false will mark execution of this function as "failed", meaning that the undo might be skipped if no other entities are removed by it. This is useful when for example an entity you want to access is removed therefore there's nothing to do.
* @arg args[] arguments - Arguments to pass to the function (after the undo info table)
 */
    function AddFunction(func: GMLua.CallbackNoContext, ...arguments: any[]): void;

    /**
     * Begins a new undo entry
     *
     * @arg string name - Name of the undo message to show to players
     */
    function Create(name: string): void;

    /**
     * Processes an undo block (in table form). This is used internally by the undo manager when a player presses Z.
     *
     * @arg UndoStruct tab - The undo block to process as an Structures/Undo
     * @return {number} Number of removed entities
     */
    function Do_Undo(tab: UndoStruct): number;

    /**
     * Completes an undo entry, and registers it with the player's client
     *
     * @arg string NiceText - Text that appears in the player's undo history
     */
    function Finish(NiceText: string): void;

    /**
     * Serverside, returns a table containing all undo blocks of all players. Clientside, returns a table of the local player's undo blocks.
     *
     * @return {object} The undo table.
     */
    function GetTable(): object;

    /**
     * Makes the UI dirty - it will re-create the controls the next time it is viewed.
     */
    function MakeUIDirty(): void;

    /**
     * Replaces any instance of the "from" reference with the "to" reference, in any existing undo block. Returns true if something was replaced
     *
     * @arg Entity from - The old entity
     * @arg Entity to - The new entity to replace the old one
     * @return {boolean} somethingReplaced
     */
    function ReplaceEntity(from: Entity, to: Entity): boolean;

    /**
     * Sets a custom undo text for the current undo block
     *
     * @arg string customText - The text to display when the undo block is undone
     */
    function SetCustomUndoText(customText: string): void;

    /**
     * Sets the player which the current undo block belongs to
     *
     * @arg Player ply - The player responsible for undoing the block
     */
    function SetPlayer(ply: Player): void;

    /**
     * Adds a hook (CPanelPaint) to the control panel paint function so we can determine when it is being drawn.
     */
    function SetupUI(): void;
}
