/** @noSelfInFile */
declare namespace sound {
    /**
     * Creates a sound script. It can also override sounds, which seems to only work when set on the server.
     *
     * @arg SoundDataStruct soundData - The sounds properties. See Structures/SoundData
     */
    function Add(soundData: SoundDataStruct): void;

    /**
     * Overrides sounds defined inside of a txt file; typically used for adding map-specific sounds.
     *
     * @arg string filepath - Path to the script file to load.
     */
    function AddSoundOverrides(filepath: string): void;

    /**
     * Emits a sound hint to the game elements to react to, for example to repel or attract antlions.
     *
     * @arg number hint - The hint to emit. See Enums/SOUND
     * @arg Vector pos - The position to emit the hint at
     * @arg number volume - The volume or radius of the hint
     * @arg number duration - The duration of the hint in seconds
     * @arg Entity [owner=NULL]
     */
    function EmitHint(hint: number, pos: Vector, volume: number, duration: number, owner?: Entity): void;

    /**
 * Creates a sound from a function.
* 
* @arg string indentifier - An unique identified for the sound.
You cannot override already existing ones.
* @arg number samplerate - The sample rate of the sound. Must be 11025, 22050 or 44100.
* @arg number length - The length in seconds of the sound to generate.
* @arg GMLua.CallbackNoContext callback - A function which will be called to generate every sample on the sound. This function gets the current sample number passed as the first argument. The return value must be between -1.0 and 1.0. Other values will wrap back to the -1 to 1 range and basically clip. There are 65535 possible quantifiable values between -1 and 1.
 */
    function Generate(indentifier: string, samplerate: number, length: number, callback: GMLua.CallbackNoContext): void;

    /**
     * Returns the most dangerous/closest sound hint based on given location and types of sounds to sense.
     *
     * @arg number types - The types of sounds to choose from. See SOUND_ enums.
     * @arg Vector pos - The position to sense sounds at.
     * @return {SoundHintDataStruct} A table with SoundHintData structure or nil if no sound hints are nearby.
     */
    function GetLoudestSoundHint(types: number, pos: Vector): SoundHintDataStruct;

    /**
     * Returns properties of the soundscript.
     *
     * @arg string name - The name of the sound script
     * @return {SoundDataStruct} The properties of the soundscript. See Structures/SoundData
     */
    function GetProperties(name: string): SoundDataStruct;

    /**
     * Returns a list of all registered sound scripts.
     *
     * @return {object} The list/array of all registered sound scripts ( No other information is provided )
     */
    function GetTable(): object;

    /**
     * Plays a sound from the specified position in the world.
     * If you want to play a sound without a position, such as a UI sound, use surface.PlaySound instead.
     *
     * @arg string Name - A string path to the sound.
     * @arg Vector Pos - A vector describing where the sound should play.
     * @arg number Level - Sound level in decibels. 75 is normal. Ranges from 20 to 180, where 180 is super loud. This affects how far away the sound will be heard.
     * @arg number Pitch - An integer describing the sound pitch. Range is from 0 to 255. 100 is normal pitch.
     * @arg number Volume - A float ranging from 0-1 describing the output volume of the sound.
     */
    function Play(Name: string, Pos: Vector, Level: number, Pitch: number, Volume: number): void;

    /**
 * Plays a file from GMod directory. You can find a list of all error codes here
* 
* @arg string path - The path to the file to play.
Unlike other sound functions and structures, the path is relative to garrysmod/ instead of garrysmod/sound/
* @arg string flags - Flags for the sound. Can be one or more of following, separated by a space (" "):

3d - Makes the sound 3D, so you can set its position
mono - Forces the sound to have only one channel
noplay - Forces the sound not to play as soon as this function is called
noblock - Disables streaming in blocks. It is more resource-intensive, but it is required for IGModAudioChannel:SetTime.

If you don't want to use any of the above, you can just leave it as "".
* @arg GMLua.CallbackNoContext callback - Callback function that is called as soon as the the stream is loaded. It has next arguments:

IGModAudioChannel soundchannel - The sound channel. Will be nil if an error occured.
number errorID - ID of an error if an error has occured. Will be nil, otherwise.
string errorName - Name of an error if an error has occured. Will be nil, otherwise.
 */
    function PlayFile(path: string, flags: string, callback: GMLua.CallbackNoContext): void;

    /**
 * Allows you to play external sound files, as well as online radio streams.
* You can find a list of all error codes here
* 
* @arg string url - The URL of the sound to play
* @arg string flags - Flags for the sound. Can be one or more of following, separated by a space (" "):

3d - Makes the sound 3D, so you can set its position
mono - Forces the sound to have only one channel
noplay - Forces the sound not to play as soon as this function is called
noblock - Disables streaming in blocks. It is more resource-intensive, but it is required for IGModAudioChannel:SetTime.

If you don't want to use any of the above, you can just leave it as "".
* @arg GMLua.CallbackNoContext callback - Callback function that is called as soon as the the stream is loaded. It has next arguments:
IGModAudioChannel soundchannel - The sound channel
number errorID - ID of an error, if an error has occured
string errorName - Name of an error, if an error has occured
 */
    function PlayURL(url: string, flags: string, callback: GMLua.CallbackNoContext): void;
}
