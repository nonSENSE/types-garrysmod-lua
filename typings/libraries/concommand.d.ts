/** @noSelfInFile */
declare namespace concommand {
    /**
 * Creates a console command that runs a function in lua with optional autocompletion function and help text.
* 
* @arg string name - The command name to be used in console.
This cannot be a name of existing console command or console variable. It will silently fail if it is.
* @arg GMLua.CallbackNoContext callback - The function to run when the concommand is executed. Arguments passed are:

Player ply - The player that ran the concommand. NULL entity if command was entered with the dedicated server console.
string cmd - The concommand string (if one callback is used for several concommands).
table args - A table of all string arguments.
string argStr - The arguments as a string.
* @arg GMLua.CallbackNoContext [autoComplete=nil] - The function to call which should return a table of options for autocompletion. (Autocompletion Tutorial)
This only properly works on the client since it is not networked. Arguments passed are:

string cmd - The concommand this autocompletion is for.
string args - The arguments typed so far.
* @arg string [helpText="nil"] - The text to display should a user run 'help cmdName'.
* @arg number [flags=0] - Concommand modifier flags. See Enums/FCVAR.
 */
    function Add(
        name: string,
        callback: GMLua.CallbackNoContext,
        autoComplete?: GMLua.CallbackNoContext,
        helpText?: string,
        flags?: number
    ): void;

    /**
     * Used by the engine to call the autocomplete function for a console command, and retrieve returned options.
     *
     * @arg string command - Name of command
     * @arg string arguments - Arguments given to the command
     * @return {object} Possibilities for auto-completion. This is the return value of the auto-complete callback.
     */
    function AutoComplete(command: string, arguments: string): object;

    /**
     * Returns the tables of all console command callbacks, and autocomplete functions, that were added to the game with concommand.Add.
     *
     * @return {object} Table of command callback functions.
     * @return {object} Table of command autocomplete functions.
     */
    function GetTable(): LuaMultiReturn<[object, object]>;

    /**
     * Removes a console command.
     *
     * @arg string name - The name of the command to be removed.
     */
    function Remove(name: string): void;

    /**
 * Used by the engine to run a console command's callback function. This will only be called for commands that were added with AddConsoleCommand, which concommand.Add calls internally. An error is sent to the player's chat if no callback is found.
* 
* @arg Player ply - Player to run concommand on
* @arg string cmd - Command name
* @arg any args - Command arguments.
Can be table or string
* @arg string argumentString - string of all arguments sent to the command
* @return {boolean} true if the console command with the given name exists, and false if it doesn't.
 */
    function Run(ply: Player, cmd: string, args: any, argumentString: string): boolean;
}
