/** @noSelfInFile */
declare namespace controlpanel {
    /**
     * Clears ALL the control panels ( for tools )
     */
    function Clear(): void;

    /**
 * Returns (or creates if not exists) a control panel.
* 
* @arg string name - The name of the panel. For normal tools this will be equal to TOOL.Mode (the tool's filename without the extension).
When you create a tool/option via spawnmenu.AddToolMenuOption, the internal control panel name is TOOL.Mode .. "_" .. tool_tab:lower() .. "_" .. tool_category:lower().
* @return {Panel} The ControlPanel panel.
 */
    function Get(name: string): Panel;
}
