/** @noSelfInFile */
declare namespace debug {
    /**
     * Enters an interactive mode with the user, running each string that the user enters. Using simple commands and other debug facilities, the user can inspect global and local variables, change their values, evaluate expressions, and so on. A line containing only the word cont finishes this function, so that the caller continues its execution.
     */
    function debug(): void;

    /**
     * Returns the environment of the passed object. This can be set with debug.setfenv
     *
     * @arg object object - Object to get environment of
     * @return {object} Environment
     */
    function getfenv(object: object): object;

    /**
     * Returns the current hook settings of the passed thread. The thread argument can be omitted. This is unrelated to . More information on hooks can be found at http://www.lua.org/pil/23.2.html
     *
     * @arg any [thread=nil] - Which thread to retrieve its hook from
     * @return {Function} Hook function
     * @return {string} Hook mask
     * @return {number} Hook count
     */
    function gethook(thread?: any): LuaMultiReturn<[Function, string, number]>;

    /**
 * Returns debug information about a function.
* 
* @arg GMLua.CallbackNoContext funcOrStackLevel - Takes either a function or a number representing the stack level as an argument. Stack level 0 always corresponds to the debug.getinfo call, 1 would be the function calling debug.getinfo, and so on.
Returns useful information about that function in a table.
* @arg string [fields="flnSu"] - A string whose characters specify the information to be retrieved.

f - Populates the func field.
l - Populates the currentline field.
L - Populates the activelines field.
n - Populates the name and namewhat fields - only works if stack level is passed rather than function pointer.
S - Populates the location fields (lastlinedefined, linedefined, short_src, source and what).
u - Populates the argument and upvalue fields (isvararg, nparams, nups)
* @return {DebugInfoStruct} A table as a Structures/DebugInfo containing information about the function you passed. Can return nil if the stack level didn't point to a valid stack frame.
 */
    function getinfo(funcOrStackLevel: GMLua.CallbackNoContext, fields?: string): DebugInfoStruct;

    /**
 * Gets the name and value of a local variable indexed from the level.
* 
* @arg any [thread=Current thread] - The thread
* @arg number level - The level above the thread.

0 = the function that was called (most always this function)'s arguments
1 = the thread that had called this function.
2 = the thread that had called the function that started the thread that called this function.

A function defined in Lua can also be passed as the level. The index will specify the parameter's name to be returned (a parameter will have a value of nil).
* @arg number index - The variable's index you want to get.

1 = the first local defined in the thread
2 = the second local defined in the thread
etc...
* @return {string} The name of the variable.
Sometimes this will be (*temporary) if the local variable had no name.
Variables with names starting with ( are internal variables.
* @return {any} The value of the local variable.
 */
    function getlocal(thread?: any, level?: number, index?: number): LuaMultiReturn<[string, any]>;

    /**
     * Returns the metatable of an object. This function ignores the metatable's __metatable field.
     *
     * @arg any object - The object to retrieve the metatable from.
     * @return {object} The metatable of the given object.
     */
    function getmetatable(object: any): object;

    /**
     * Returns the internal Lua registry table.
     *
     * @return {object} The Lua registry
     */
    function getregistry(): object;

    /**
     * Used for getting variable values in an index from the passed function. This does nothing for C functions.
     *
     * @arg GMLua.CallbackNoContext func - Function to get the upvalue indexed from.
     * @arg number index - The index in the upvalue array. The max number of entries can be found in debug.getinfo's "nups" key.
     * @return {string} Name of the upvalue. Will be nil if the index was out of range (< 1 or > debug.getinfo.nups), or the function was defined in C.
     * @return {any} Value of the upvalue.
     */
    function getupvalue(func: GMLua.CallbackNoContext, index: number): LuaMultiReturn<[string, any]>;

    /**
     * Sets the environment of the passed object.
     *
     * @arg object object - Object to set environment of
     * @arg object env - Environment to set
     * @return {object} The object
     */
    function setfenv(object: object, env: object): object;

    /**
     * Sets the given function as a Lua hook. This is completely different to gamemode hooks. The thread argument can be completely omitted and calling this function with no arguments will remove the current hook. This is used by default for infinite loop detection. More information on hooks can be found at http://www.lua.org/pil/23.2.html
     *
     * @arg any thread - Thread to set the hook on. This argument can be omited
     * @arg GMLua.CallbackNoContext hook - Function for the hook to call
     * @arg string mask - The hook's mask
     * @arg number count - How often to call the hook (in instructions). 0 for every instruction
     */
    function sethook(thread: any, hook: GMLua.CallbackNoContext, mask: string, count: number): void;

    /**
 * Sets a local variable's value.
* 
* @arg any [thread=Current Thread] - The thread
* @arg number level - The level above the thread.
0 is the function that was called (most always this function)'s arguments
1 is the thread that had called this function.
2 is the thread that had called the function that started the thread that called this function.
* @arg number index - The variable's index you want to get.
1 = the first local defined in the thread
2 = the second local defined in the thread
* @arg any [value=nil] - The value to set the local to
* @return {string} The name of the local variable if the local at the index exists, otherwise nil is returned.
 */
    function setlocal(thread?: any, level?: number, index?: number, value?: any): string;

    /**
 * Sets the object's metatable. Unlike setmetatable, this function works regardless of whether the first object passed is a valid table or not; this function even works on primitive datatypes such as numbers, functions, and even nil.
* 
* @arg any object - Object to set the metatable for.
* @arg object metatable - The metatable to set for the object.
If this argument is nil, then the object's metatable is removed.
* @return {boolean} true if the object's metatable was set successfully.
 */
    function setmetatable(object: any, metatable: object): boolean;

    /**
     * Sets the variable indexed from func
     *
     * @arg GMLua.CallbackNoContext func - The function to index the upvalue from
     * @arg number index - The index from func
     * @arg any [val=nil] - The value to set the upvalue to.
     * @return {string} Returns nil if there is no upvalue with the given index, otherwise it returns the upvalue's name.
     */
    function setupvalue(func: GMLua.CallbackNoContext, index: number, val?: any): string;

    /**
     * Prints out the lua function call stack to the console.
     */
    function Trace(): void;

    /**
     * Returns a full execution stack trace.
     *
     * @arg any [thread=current thread] - Thread (ie. error object from xpcall error handler) to build traceback for. If this argument is not set to a proper thread it will act as the next argument.
     * @arg string [message="nil"] - Appended at the beginning of the traceback.
     * @arg number [level=1] - Which level to start the traceback.
     * @return {string} A dump of the execution stack.
     */
    function traceback(thread?: any, message?: string, level?: number): string;

    /**
     * Returns an unique identifier for the upvalue indexed from func
     *
     * @arg GMLua.CallbackNoContext func - The function to index the upvalue from
     * @arg number index - The index from func
     * @return {number} A unique identifier
     */
    function upvalueid(func: GMLua.CallbackNoContext, index: number): number;

    /**
     * Make the n1-th upvalue of the Lua closure f1 refer to the n2-th upvalue of the Lua closure f2.
     *
     * @arg GMLua.CallbackNoContext f1
     * @arg number n1
     * @arg GMLua.CallbackNoContext f2
     * @arg number n2
     */
    function upvaluejoin(f1: GMLua.CallbackNoContext, n1: number, f2: GMLua.CallbackNoContext, n2: number): void;
}
