/** @noSelfInFile */
declare namespace language {
    /**
     * Adds a language item. Language placeholders preceded with "#" are replaced with full text in Garry's Mod once registered with this function.
     *
     * @arg string placeholder - The key for this phrase, without the preceding "#".
     * @arg string fulltext - The phrase that should be displayed whenever this key is used.
     */
    function Add(placeholder: string, fulltext: string): void;

    /**
     * Retrieves the translated version of inputted string. Useful for concentrating multiple translated strings.
     *
     * @arg string phrase - The phrase to translate
     * @return {string} The translated phrase, or the input string if no translation was found
     */
    function GetPhrase(phrase: string): string;
}
