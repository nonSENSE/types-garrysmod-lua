/** @noSelfInFile */
declare namespace cam {
    /**
     * Shakes the screen at a certain position.
     *
     * @arg Vector pos - Origin of the shake.
     * @arg Angle angles - Angles of the shake.
     * @arg number factor - The shake factor.
     */
    function ApplyShake(pos: Vector, angles: Angle, factor: number): void;

    /**
     * Switches the renderer back to the previous drawing mode from a 3D context.
     */
    function End(): void;

    /**
     * Switches the renderer back to the previous drawing mode from a 2D context.
     */
    function End2D(): void;

    /**
     * Switches the renderer back to the previous drawing mode from a 3D context.
     */
    function End3D(): void;

    /**
     * Switches the renderer back to the previous drawing mode from a 3D2D context.
     */
    function End3D2D(): void;

    /**
     * Switches the renderer back to the previous drawing mode from a 3D orthographic rendering context.
     */
    function EndOrthoView(): void;

    /**
     * Returns the currently active model matrix.
     *
     * @return {VMatrix} The currently active matrix.
     */
    function GetModelMatrix(): VMatrix;

    /**
     * Tells the renderer to ignore the depth buffer and draw any upcoming operation "ontop" of everything that was drawn yet.
     *
     * @arg boolean ignoreZ - Determines whenever to ignore the depth buffer or not.
     */
    function IgnoreZ(ignoreZ: boolean): void;

    /**
     * Pops the current active rendering matrix from the stack and reinstates the previous one.
     */
    function PopModelMatrix(): void;

    /**
     * Pushes the specified matrix onto the render matrix stack. Unlike opengl, this will replace the current model matrix.
     *
     * @arg VMatrix matrix - The matrix to push.
     * @arg boolean [multiply=false] - If set, multiplies given matrix with currently active matrix (cam.GetModelMatrix) before pushing.
     */
    function PushModelMatrix(matrix: VMatrix, multiply?: boolean): void;

    /**
     * Sets up a new rendering context. This is an extended version of cam.Start3D and cam.Start2D. Must be finished by cam.End3D or cam.End2D.
     *
     * @arg RenderCamDataStruct dataTbl - Render context config. See Structures/RenderCamData
     */
    function Start(dataTbl: RenderCamDataStruct): void;

    /**
     * Sets up a new 2D rendering context. Must be finished by cam.End2D.
     */
    function Start2D(): void;

    /**
     * Sets up a new 3D rendering context. Must be finished by cam.End3D.
     *
     * @arg Vector [pos=EyePos()] - Render cam position.
     * @arg Angle [angles=EyeAngles()] - Render cam angles.
     * @arg number [fov=NaN] - Field of view.
     * @arg number [x=0] - X coordinate of where to start the new view port.
     * @arg number [y=0] - Y coordinate of where to start the new view port.
     * @arg number [w=NaN] - Width of the new viewport.
     * @arg number [h=NaN] - Height of the new viewport.
     * @arg number [zNear=NaN] - Distance to near clipping plane.
     * @arg number [zFar=NaN] - Distance to far clipping plane.
     */
    function Start3D(
        pos?: Vector,
        angles?: Angle,
        fov?: number,
        x?: number,
        y?: number,
        w?: number,
        h?: number,
        zNear?: number,
        zFar?: number
    ): void;

    /**
 * Sets up a new 2D rendering context. Must be finished by cam.End3D2D. This function pushes a new matrix onto the stack. (cam.PushModelMatrix)
* 
* @arg Vector pos - Origin of the 3D2D context, ie. the top left corner, (0, 0).
* @arg Angle angles - Angles of the 3D2D context.
+x in the 2d context corresponds to +x of the angle (its forward direction).
+y in the 2d context corresponds to -y of the angle (its right direction).
If (dx, dy) are your desired (+x, +y) unit vectors, the angle you want is dx:AngleEx(dx:Cross(-dy)).
* @arg number scale - The scale of the render context.
If scale is 1 then 1 pixel in 2D context will equal to 1 unit in 3D context.
 */
    function Start3D2D(pos: Vector, angles: Angle, scale: number): void;

    /**
     * Sets up a new 3d context using orthographic projection.
     *
     * @arg number leftOffset - The left plane offset.
     * @arg number topOffset - The top plane offset.
     * @arg number rightOffset - The right plane offset.
     * @arg number bottomOffset - The bottom plane offset.
     */
    function StartOrthoView(leftOffset: number, topOffset: number, rightOffset: number, bottomOffset: number): void;
}
