/** @noSelfInFile */
declare namespace derma {
    /**
     * Gets the color from a Derma skin of a panel and returns default color if not found
     *
     * @arg string name
     * @arg Panel pnl
     * @arg object def - The default color in case of failure.
     */
    function Color(name: string, pnl: Panel, def: object): void;

    /**
     * Defines a new Derma control with an optional base.
     *
     * @arg string name - Name of the newly created control
     * @arg string description - Description of the control
     * @arg object tab - Table containing control methods and properties
     * @arg string base - Derma control to base the new control off of
     * @return {object} A table containing the new control's methods and properties
     */
    function DefineControl(name: string, description: string, tab: object, base: string): object;

    /**
     * Defines a new skin so that it is usable by Derma. The default skin can be found in garrysmod/lua/skins/default.lua
     *
     * @arg string name - Name of the skin
     * @arg string descriptions - Description of the skin
     * @arg object skin - Table containing skin data
     */
    function DefineSkin(name: string, descriptions: string, skin: object): void;

    /**
     * Returns the derma.Controls table, a list of all derma controls registered with derma.DefineControl.
     *
     * @return {any} A listing of all available derma-based controls. See derma.Controls for structure and contents.
     */
    function GetControlList(): any;

    /**
     * Returns the default skin table, which can be changed with the hook GM:ForceDermaSkin
     *
     * @return {object} Skin table
     */
    function GetDefaultSkin(): object;

    /**
     * Returns the skin table of the skin with the supplied name
     *
     * @arg string name - Name of skin
     * @return {object} Skin table
     */
    function GetNamedSkin(name: string): object;

    /**
     * Returns a copy of the table containing every Derma skin
     *
     * @return {object} Table of every Derma skin
     */
    function GetSkinTable(): object;

    /**
     * Clears all cached panels so that they reassess which skin they should be using.
     */
    function RefreshSkins(): void;

    /**
     * Returns how many times derma.RefreshSkins has been called.
     *
     * @return {number} Amount of times derma.RefreshSkins has been called.
     */
    function SkinChangeIndex(): number;

    /**
     * Calls the specified hook for the given panel
     *
     * @arg string type - The type of hook to run
     * @arg string name - The name of the hook to run
     * @arg Panel panel - The panel to call the hook for
     * @arg number w - The width of the panel
     * @arg number h - The height of the panel
     * @return {any} The returned variable from the skin hook
     */
    function SkinHook(type: string, name: string, panel: Panel, w: number, h: number): any;

    /**
     * Returns a function to draw a specified texture of panels skin.
     *
     * @arg string name - The identifier of the texture
     * @arg Panel pnl - Panel to get the skin of.
     * @arg any [fallback=nil] - What to return if we failed to retrieve the texture
     * @return {Function} A function that is created with the GWEN to draw a texture.
     */
    function SkinTexture(name: string, pnl: Panel, fallback?: any): Function;
}
