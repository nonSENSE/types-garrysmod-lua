/** @noSelfInFile */
declare namespace input {
    /**
     * Returns the last key captured by key trapping.
     *
     * @return {number} The key, see Enums/KEY
     */
    function CheckKeyTrapping(): number;

    /**
     * Returns the digital value of an analog stick on the current (set up via convars) controller.
     *
     * @arg number axis - The analog axis to poll. See Enums/ANALOG.
     * @return {number} The digital value, in range of 0-65535. (verify)
     */
    function GetAnalogValue(axis: number): number;

    /**
     * Returns the cursor's position on the screen.
     *
     * @return {number} The cursors position on the X axis.
     * @return {number} The cursors position on the Y axis.
     */
    function GetCursorPos(): LuaMultiReturn<[number, number]>;

    /**
     * Gets the button code from a button name. This is opposite of input.GetKeyName.
     *
     * @arg string button - The internal button name, such as e or ⇧ shift.
     * @return {number} The button code, see Enums/BUTTON_CODE.
     */
    function GetKeyCode(button: string): number;

    /**
     * Gets the button name from a numeric button code. The name needs to be translated with language.GetPhrase before being displayed.
     *
     * @arg number button - The button, see Enums/BUTTON_CODE.
     * @return {string} Button name.
     */
    function GetKeyName(button: number): string;

    /**
     * Gets whether the specified button code is down.
     *
     * @arg number button - The button, valid values are in the range of Enums/BUTTON_CODE.
     * @return {boolean} Is the button down
     */
    function IsButtonDown(button: number): boolean;

    /**
     * Returns whether a control key is being pressed
     *
     * @return {boolean} Is Ctrl key down or not
     */
    function IsControlDown(): boolean;

    /**
     * Gets whether a key is down.
     *
     * @arg number key - The key, see Enums/KEY.
     * @return {boolean} Is the key down
     */
    function IsKeyDown(key: number): boolean;

    /**
     * Returns whether key trapping is activate and the next key press will be captured.
     *
     * @return {boolean} Whether key trapping active or not
     */
    function IsKeyTrapping(): boolean;

    /**
     * Gets whether a mouse button is down
     *
     * @arg number mouseKey - The key, see Enums/MOUSE
     * @return {boolean} Is the key down
     */
    function IsMouseDown(mouseKey: number): boolean;

    /**
     * Gets whether a shift key is being pressed
     *
     * @return {boolean} isDown
     */
    function IsShiftDown(): boolean;

    /**
 * Returns the client's bound key for the specified console command. If the player has multiple keys bound to a single command, there is no defined behavior of which key will be returned.
* 
* @arg string binding - The binding name
* @arg boolean [exact=false] - True if the binding should match exactly
* @return {string} The first key found with that binding or no value if no key with given binding was found.
See also input.GetKeyCode.
 */
    function LookupBinding(binding: string, exact?: boolean): string;

    /**
     * Returns the bind string that the given key is bound to.
     *
     * @arg number key - Key from Enums/BUTTON_CODE
     * @return {string} The bind string of the given key.
     */
    function LookupKeyBinding(key: number): string;

    /**
     * Switches to the provided weapon on the next CUserCmd generation/CreateMove call. Direct binding to CInput::MakeWeaponSelection.
     *
     * @arg Weapon weapon - The weapon entity to switch to.
     */
    function SelectWeapon(weapon: Weapon): void;

    /**
     * Sets the cursor's position on the screen, relative to the topleft corner of the window
     *
     * @arg number mouseX - X coordinate for mouse position
     * @arg number mouseY - Y coordinate for mouse position
     */
    function SetCursorPos(mouseX: number, mouseY: number): void;

    /**
     * Begins waiting for a key to be pressed so we can save it for input.CheckKeyTrapping. Used by the DBinder.
     */
    function StartKeyTrapping(): void;

    /**
     * Translates a console command alias, basically reverse of the alias console command.
     *
     * @arg string command - The alias to lookup.
     * @return {string} The command(s) this alias will execute if ran, or nil if the alias doesn't exist.
     */
    function TranslateAlias(command: string): string;

    /**
     * Returns whether a key was initially pressed in the same frame this function was called.
     *
     * @arg number key - The key, see Enums/KEY.
     * @return {boolean} True if the key was initially pressed the same frame that this function was called, false otherwise.
     */
    function WasKeyPressed(key: number): boolean;

    /**
     * Returns whether a key was released in the same frame this function was called.
     *
     * @arg number key - The key, see Enums/KEY.
     * @return {boolean} True if the key was released the same frame that this function was called, false otherwise.
     */
    function WasKeyReleased(key: number): boolean;

    /**
     * Returns whether the key is being held down or not.
     *
     * @arg number key - The key to test, see Enums/KEY
     * @return {boolean} Whether the key is being held down or not.
     */
    function WasKeyTyped(key: number): boolean;

    /**
     * Returns whether a mouse key was double pressed in the same frame this function was called.
     *
     * @arg number button - The mouse button to test, see Enums/MOUSE
     * @return {boolean} Whether the mouse key was double pressed or not.
     */
    function WasMouseDoublePressed(button: number): boolean;

    /**
     * Returns whether a mouse key was initially pressed in the same frame this function was called.
     *
     * @arg number key - The key, see Enums/MOUSE
     * @return {boolean} True if the mouse key was initially pressed the same frame that this function was called, false otherwise.
     */
    function WasMousePressed(key: number): boolean;

    /**
     * Returns whether a mouse key was released in the same frame this function was called.
     *
     * @arg number key - The key to test, see Enums/MOUSE
     * @return {boolean} True if the mouse key was released the same frame that this function was called, false otherwise.
     */
    function WasMouseReleased(key: number): boolean;
}
