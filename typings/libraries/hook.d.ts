/** @noSelfInFile */
declare namespace hook {
    /**
 * Add a hook to be called upon the given event occurring.
* 
* @arg T eventName - The event to hook on to. This can be any GM hook, gameevent after using gameevent.Listen, or custom hook run with hook.Call or hook.Run.
* @arg any identifier - The unique identifier, usually a string. This can be used elsewhere in the code to replace or remove the hook. The identifier should be unique so that you do not accidentally override some other mods hook, unless that's what you are trying to do.
The identifier can be either a string, or a table/object with an IsValid function defined such as an Entity or Panel. numbers and booleans, for example, are not allowed.
If the identifier is a table/object, it will be inserted in front of the other arguments in the callback and the hook will be called as long as it's valid. However, as soon as IsValid( identifier ) returns false, the hook will be removed.
* @arg GMLua.CallbackNoContext<GM[T]> func - The function to be called, arguments given to it depend on the .
Returning any value besides nil from the hook's function will stop other hooks of the same event down the loop from being executed. Only return a value when absolutely necessary and when you know what you are doing.
It WILL break other addons.
 */
    function Add<T extends GM__HOOKS>(eventName: T, identifier: any, func: GMLua.CallbackNoContext<GM[T]>): void;

    /**
 * Add a hook to be called upon the given event occurring.
* 
* @arg string eventName - The event to hook on to. This can be any GM hook, gameevent after using gameevent.Listen, or custom hook run with hook.Call or hook.Run.
* @arg any identifier - The unique identifier, usually a string. This can be used elsewhere in the code to replace or remove the hook. The identifier should be unique so that you do not accidentally override some other mods hook, unless that's what you are trying to do.
The identifier can be either a string, or a table/object with an IsValid function defined such as an Entity or Panel. numbers and booleans, for example, are not allowed.
If the identifier is a table/object, it will be inserted in front of the other arguments in the callback and the hook will be called as long as it's valid. However, as soon as IsValid( identifier ) returns false, the hook will be removed.
* @arg GMLua.CallbackNoContext func - The function to be called, arguments given to it depend on the .
Returning any value besides nil from the hook's function will stop other hooks of the same event down the loop from being executed. Only return a value when absolutely necessary and when you know what you are doing.
It WILL break other addons.
 */
    function Add(eventName: string, identifier: any, func: GMLua.CallbackNoContext): void;

    /**
     * Calls all hooks associated with the given event until one returns something other than nil, and then returns that data.
     *
     * @arg string eventName - The event to call hooks for.
     * @arg object gamemodeTable - If the gamemode is specified, the gamemode hook within will be called, otherwise not.
     * @arg args[] args - The arguments to be passed to the hooks.
     * @return {args[]} Return data from called hooks. Limited to 6 return values.
     */
    function Call(eventName: string, gamemodeTable: object, ...args: any[]): any;

    /**
     * Returns a list of all the hooks registered with hook.Add.
     *
     * @return {object} A table of tables. See below for output example.
     */
    function GetTable(): object;

    /**
     * Removes the hook with the supplied identifier from the given event.
     *
     * @arg string eventName - The event name.
     * @arg any identifier - The unique identifier of the hook to remove, usually a string.
     */
    function Remove(eventName: string, identifier: any): void;

    /**
     * Calls all hooks associated with the given event until one returns something other than nil and then returns that data. If no hook returns any data, it will try to call the GAMEMODE:<eventName>; alternative, if one exists.
     *
     * @arg string eventName - The event to call hooks for.
     * @arg args[] args - The arguments to be passed to the hooks.
     * @return {any} Returned data from called hooks.
     */
    function Run(eventName: string, ...args: any[]): any;
}
