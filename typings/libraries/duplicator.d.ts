/** @noSelfInFile */
declare namespace duplicator {
    /**
     * Allow this entity to be duplicated
     *
     * @arg string classname - An entity's classname
     */
    function Allow(classname: string): void;

    /**
     * Calls every function registered with duplicator.RegisterBoneModifier on each bone the ent has.
     *
     * @arg Player ply - The player whose entity this is
     * @arg Entity ent - The entity in question
     */
    function ApplyBoneModifiers(ply: Player, ent: Entity): void;

    /**
     * Calls every function registered with duplicator.RegisterEntityModifier on the entity.
     *
     * @arg Player ply - The player whose entity this is
     * @arg Entity ent - The entity in question
     */
    function ApplyEntityModifiers(ply: Player, ent: Entity): void;

    /**
     * Clears/removes the chosen entity modifier from the entity.
     *
     * @arg Entity ent - The entity the modification is stored on
     * @arg any key - The key of the stored entity modifier
     */
    function ClearEntityModifier(ent: Entity, key: any): void;

    /**
 * Copies the entity, and all of its constraints and entities, then returns them in a table.
* 
* @arg Entity ent - The entity to duplicate. The function will automatically copy all constrained entities.
* @arg object [tableToAdd={}] - A preexisting table to add entities and constraints in from.
Uses the same table format as the table returned from this function.
* @return {object} A table containing duplication info which includes the following members:

table Entities
table Constraints
Vector Mins
Vector Maxs

The values of Mins & Maxs from the table are returned from duplicator.WorkoutSize
 */
    function Copy(ent: Entity, tableToAdd?: object): object;

    /**
 * Copies the passed table of entities to save for later.
* 
* @arg object ents - A table of entities to save/copy.
* @return {object} A table containing duplication info which includes the following members:

table Entities
table Constraints
Vector Mins
Vector Maxs
 */
    function CopyEnts(ents: object): object;

    /**
     * Returns a table with some entity data that can be used to create a new entity with duplicator.CreateEntityFromTable
     *
     * @arg Entity ent - The entity table to save
     * @return {EntityCopyDataStruct} See Structures/EntityCopyData
     */
    function CopyEntTable(ent: Entity): EntityCopyDataStruct;

    /**
     * Creates a constraint from a saved/copied constraint table.
     *
     * @arg object constraint - Saved/copied constraint table
     * @arg object entityList - The list of entities that are to be constrained
     * @return {Entity} The newly created constraint entity
     */
    function CreateConstraintFromTable(constraint: object, entityList: object): Entity;

    /**
     * "Create an entity from a table."
     *
     * @arg Player ply - The player who wants to create something
     * @arg EntityCopyDataStruct entTable - The duplication data to build the entity with. See Structures/EntityCopyData
     * @return {Entity} The newly created entity
     */
    function CreateEntityFromTable(ply: Player, entTable: EntityCopyDataStruct): Entity;

    /**
     * "Restores the bone's data."
     *
     * @arg Entity ent - The entity to be bone manipulated
     * @arg BoneManipulationDataStruct bones - Table with a Structures/BoneManipulationData for every bone (that has manipulations applied) using the bone ID as the table index.
     */
    function DoBoneManipulator(ent: Entity, bones: BoneManipulationDataStruct): void;

    /**
     * Restores the flex data using Entity:SetFlexWeight and Entity:SetFlexScale
     *
     * @arg Entity ent - The entity to restore the flexes on
     * @arg object flex - The flexes to restore
     * @arg number [scale=NaN] - The flex scale to apply. (Flex scale is unchanged if omitted)
     */
    function DoFlex(ent: Entity, flex: object, scale?: number): void;

    /**
     * "Applies generic every-day entity stuff for ent from table data."
     *
     * @arg Entity ent - The entity to be applied upon
     * @arg object data - The data to be applied onto the entity
     */
    function DoGeneric(ent: Entity, data: object): void;

    /**
     * "Applies bone data, generically."
     *
     * @arg Entity ent - The entity to be applied upon
     * @arg Player [ply=nil] - The player who owns the entity. Unused in function as of early 2013
     * @arg object data - The data to be applied onto the entity
     */
    function DoGenericPhysics(ent: Entity, ply?: Player, data?: object): void;

    /**
 * Returns the entity class factory registered with duplicator.RegisterEntityClass.
* 
* @arg string name - The name of the entity class factory
* @return {Function} Is compromised of the following members:

function Func - The function that creates the entity
table Args - Arguments to pass to the function
 */
    function FindEntityClass(name: string): Function;

    /**
     * "Generic function for duplicating stuff"
     *
     * @arg Player ply - The player who wants to create something
     * @arg object data - The duplication data to build the entity with
     * @return {Entity} The newly created entity
     */
    function GenericDuplicatorFunction(ply: Player, data: object): Entity;

    /**
     * Fills entStorageTable with all of the entities in a group connected with constraints. Fills constraintStorageTable with all of the constrains constraining the group.
     *
     * @arg Entity ent - The entity to start from
     * @arg object entStorageTable - The table the entities will be inserted into
     * @arg object constraintStorageTable - The table the constraints will be inserted into
     * @return {object} entStorageTable
     * @return {object} constraintStorageTable
     */
    function GetAllConstrainedEntitiesAndConstraints(
        ent: Entity,
        entStorageTable: object,
        constraintStorageTable: object
    ): LuaMultiReturn<[object, object]>;

    /**
     * Returns whether the entity can be duplicated or not
     *
     * @arg string classname - An entity's classname
     * @return {boolean} Returns true if the entity can be duplicated (nil otherwise)
     */
    function IsAllowed(classname: string): boolean;

    /**
     * "Given entity list and constraint list, create all entities and return their tables"
     *
     * @arg Player Player - The player who wants to create something
     * @arg object EntityList - A table of duplicator data to create the entities from
     * @arg object ConstraintList - A table of duplicator data to create the constraints from
     * @return {object} List of created entities
     * @return {object} List of created constraints
     */
    function Paste(Player: Player, EntityList: object, ConstraintList: object): LuaMultiReturn<[object, object]>;

    /**
 * Registers a function to be called on each of an entity's bones when duplicator.ApplyBoneModifiers is called.
* 
* @arg any key - The type of the key doesn't appear to matter, but it is preferable to use a string.
* @arg GMLua.CallbackNoContext boneModifier - Function called on each bone that an ent has. Called during duplicator.ApplyBoneModifiers.
Function parameters are:

Player ply
Entity ent
number boneID
PhysObj bone
table data

The data table that is passed to boneModifier is set with duplicator.StoreBoneModifier
 */
    function RegisterBoneModifier(key: any, boneModifier: GMLua.CallbackNoContext): void;

    /**
     * Register a function used for creating a duplicated constraint.
     *
     * @arg string name - The unique name of new constraint
     * @arg GMLua.CallbackNoContext callback - Function to be called when this constraint is created
     * @arg any args - Arguments passed to the callback function
     */
    function RegisterConstraint(name: string, callback: GMLua.CallbackNoContext, args: any): void;

    /**
     * This allows you to specify a specific function to be run when your SENT is pasted with the duplicator, instead of relying on the generic automatic functions.
     *
     * @arg string name - The ClassName of the entity you wish to register a factory for
     * @arg GMLua.CallbackNoContext func - The factory function you want to have called. It should have the arguments (Player, ...) where ... is whatever arguments you request to be passed. It also should return the entity created, otherwise duplicator.Paste result will not include it!
     * @arg args[] args - Strings of the names of arguments you want passed to function the from the Structures/EntityCopyData. As a special case, "Data" will pass the whole structure.
     */
    function RegisterEntityClass(name: string, func: GMLua.CallbackNoContext, ...args: any[]): void;

    /**
     * This allows you to register tweaks to entities. For instance, if you were making an "unbreakable" addon, you would use this to enable saving the "unbreakable" state of entities between duplications.
     *
     * @arg string name - An identifier for your modification. This is not limited, so be verbose. Person's 'Unbreakable' mod is far less likely to cause conflicts than unbreakable
     * @arg GMLua.CallbackNoContext func - The function to be called for your modification. It should have the arguments (Player, Entity, Data), where data is what you pass to duplicator.StoreEntityModifier.
     */
    function RegisterEntityModifier(name: string, func: GMLua.CallbackNoContext): void;

    /**
     * Help to remove certain map created entities before creating the saved entities
     * This is obviously so we don't get duplicate props everywhere.
     * It should be called before calling Paste.
     */
    function RemoveMapCreatedEntities(): void;

    /**
     * "When a copy is copied it will be translated according to these.
     * If you set them - make sure to set them back to 0 0 0!"
     *
     * @arg Angle v - The angle to offset all pastes from
     */
    function SetLocalAng(v: Angle): void;

    /**
     * "When a copy is copied it will be translated according to these.
     * If you set them - make sure to set them back to 0 0 0!"
     *
     * @arg Vector v - The position to offset all pastes from
     */
    function SetLocalPos(v: Vector): void;

    /**
 * Stores bone mod data for a registered bone modification function
* 
* @arg Entity ent - The entity to add bone mod data to
* @arg number boneID - The bone ID.
See Entity:GetPhysicsObjectNum
* @arg any key - The key for the bone modification
* @arg object data - The bone modification data that is passed to the bone modification function
 */
    function StoreBoneModifier(ent: Entity, boneID: number, key: any, data: object): void;

    /**
     * Stores an entity modifier into an entity for saving
     *
     * @arg Entity entity - The entity to store modifier in
     * @arg string name - Unique modifier name as defined in duplicator.RegisterEntityModifier
     * @arg object data - Modifier data
     */
    function StoreEntityModifier(entity: Entity, name: string, data: object): void;

    /**
     * "Work out the AABB size"
     *
     * @arg object Ents - A table of entity duplication datums.
     */
    function WorkoutSize(Ents: object): void;
}
