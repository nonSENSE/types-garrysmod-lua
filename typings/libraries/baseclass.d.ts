/** @noSelfInFile */
declare namespace baseclass {
    /**
     * Gets the base class of an an object.
     *
     * @arg string name - The child class.
     * @return {object} The base class's meta table.
     */
    function Get(name: string): object;

    /**
     * Add a new base class that can be derived by others. This is done automatically for:
     *
     * @arg string name - The name of this base class. Must be completely unique.
     * @arg object tab - The base class.
     */
    function Set(name: string, tab: object): void;
}
