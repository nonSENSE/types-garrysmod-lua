/** @noSelfInFile */
declare namespace file {
    /**
     * Appends a file relative to the data folder.
     *
     * @arg string name - The file's name.
     * @arg string content - The content which should be appended to the file.
     */
    function Append(name: string, content: string): void;

    /**
 * Returns the content of a file asynchronously.
* 
* @arg string fileName - The name of the file.
* @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
* @arg GMLua.CallbackNoContext callback - A callback function that will be called when the file read operation finishes. Arguments are:

string fileName - The fileName argument above.
string gamePath - The gamePath argument above.
number status - The status of the operation. The list can be found in Enums/FSASYNC.
string data - The entirety of the data of the file.
* @arg boolean [sync=false] - If true the file will be read synchronously.
* @return {number} FSASYNC_OK on success, FSASYNC_ERR_ on failure.
 */
    function AsyncRead(fileName: string, gamePath: string, callback: GMLua.CallbackNoContext, sync?: boolean): number;

    /**
     * Creates a directory that is relative to the data folder.
     *
     * @arg string name - The directory's name.
     */
    function CreateDir(name: string): void;

    /**
     * Deletes a file or empty folder that is relative to the data folder. You can't remove any files outside of data folder.
     *
     * @arg string name - The file name.
     */
    function Delete(name: string): void;

    /**
     * Returns a boolean of whether the file or directory exists or not.
     *
     * @arg string name - The file or directory's name.
     * @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
     * @return {boolean} Returns true if the file exists and false if it does not.
     */
    function Exists(name: string, gamePath: string): boolean;

    /**
 * Returns a list of files and directories inside a single folder.
* 
* @arg string name - The wildcard to search for. models/[WILDCARD].mdl will list .mdl files in the models/ folder.
* @arg string path - The path to look for the files and directories in. See this list for a list of valid paths.
* @arg string [sorting="nameasc"] - The sorting to be used, optional.

nameasc sort the files ascending by name.
namedesc sort the files descending by name.
dateasc sort the files ascending by date.
datedesc sort the files descending by date.
* @return {object} A table of found files, or nil if the path is invalid
* @return {object} A table of found directories, or nil if the path is invalid
 */
    function Find(name: string, path: string, sorting?: string): LuaMultiReturn<[object, object]>;

    /**
     * Returns whether the given file is a directory or not.
     *
     * @arg string fileName - The file or directory's name.
     * @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
     * @return {boolean} true if the given path is a directory or false if it's a file.
     */
    function IsDir(fileName: string, gamePath: string): boolean;

    /**
 * Attempts to open a file with the given mode.
* 
* @arg string fileName - The files name. See file.Write for details on filename restrictions when writing to files.
* @arg string fileMode - The mode to open the file in. Possible values are:

r - read mode
w - write mode
a - append mode
rb - binary read mode
wb - binary write mode
ab - binary append mode
* @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
* @return {any} The opened file object, or nil if it failed to open due to it not existing or being used by another process.
 */
    function Open(fileName: string, fileMode: string, gamePath: string): any;

    /**
     * Returns the content of a file.
     *
     * @arg string fileName - The name of the file.
     * @arg string [gamePath="GAME or DATA"] - The path to look for the files and directories in. If this argument is set to true then the path will be GAME, otherwise if the argument is false or nil then the path will be DATA. See this list for a list of valid paths.
     * @return {string} The data from the file as a string, or nil if the file isn't found.
     */
    function Read(fileName: string, gamePath?: string): string;

    /**
 * Attempts to rename a file with the given name to another given name.
* 
* @arg string orignalFileName - The original file or folder name. See file.Write for details on filename restrictions when writing to files.
This argument will be forced lowercase.
* @arg string targetFileName - The target file or folder name. See file.Write for details on filename restrictions when writing to files.
This argument will be forced lowercase.
* @return {boolean} true on success, false otherwise.
 */
    function Rename(orignalFileName: string, targetFileName: string): boolean;

    /**
     * Returns the file's size in bytes. If the file is not found, returns -1.
     *
     * @arg string fileName - The file's name.
     * @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
     */
    function Size(fileName: string, gamePath: string): void;

    /**
     * Returns when the file or folder was last modified in Unix time.
     *
     * @arg string path - The file or folder path.
     * @arg string gamePath - The path to look for the files and directories in. See this list for a list of valid paths.
     * @return {number} Seconds passed since Unix epoch.
     */
    function Time(path: string, gamePath: string): number;

    /**
 * Writes the given string to a file. Erases all previous data in the file. To add data without deleting previous data, use file.Append.
* 
* @arg string fileName - The name of the file being written into. The path is relative to the data/ folder.
This argument will be forced lowercase.
The filename must end with one of the following:

.txt
.dat
.json
.xml
.csv
.jpg
.jpeg
.png
.vtf
.vmt
.mp3
.wav
.ogg

Restricted symbols are: " :
* @arg string content - The content that will be written into the file.
 */
    function Write(fileName: string, content: string): void;
}
