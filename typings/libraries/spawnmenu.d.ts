/** @noSelfInFile */
declare namespace spawnmenu {
    /**
     * Activates a tool, opens context menu and brings up the tool gun.
     *
     * @arg string tool - Tool class/file name
     * @arg boolean menu_only - Should we activate this tool in the menu only or also the toolgun? true = menu only,false = toolgun aswell
     */
    function ActivateTool(tool: string, menu_only: boolean): void;

    /**
     * Activates tools context menu in specified tool tab.
     *
     * @arg number tab - The tabID of the tab to open the context menu in
     * @arg Panel cp - The control panel to open
     */
    function ActivateToolPanel(tab: number, cp: Panel): void;

    /**
     * Returns currently opened control panel of a tool, post process effect or some other menu in spawnmenu.
     *
     * @return {Panel} The currently opened control panel, if any.
     */
    function ActiveControlPanel(): Panel;

    /**
 * Registers a new content type that is saveable into spawnlists.
* Created/called by spawnmenu.CreateContentIcon.
* 
* @arg string name - An unique name of the content type.
* @arg GMLua.CallbackNoContext _constructor - A function that is called whenever we need create a new panel for this content type.
It has two arguments:
Panel container - The container/parent of the new panel
table data - Data for the content type passed from spawnmenu.CreateContentIcon
 */
    function AddContentType(name: string, _constructor: GMLua.CallbackNoContext): void;

    /**
     * Inserts a new tab into the CreationMenus table, which will be used by the creation menu to generate its tabs (Spawnlists, Weapons, Entities, etc.)
     *
     * @arg string name - What text will appear on the tab (I.E Spawnlists).
     * @arg GMLua.CallbackNoContext func - The function called to generate the content of the tab.
     * @arg string [material="icon16/exclamation.png"] - Path to the material that will be used as an icon on the tab.
     * @arg number [order=1000] - The order in which this tab should be shown relative to the other tabs on the creation menu.
     * @arg string [tooltip="nil"] - The tooltip to be shown for this tab.
     */
    function AddCreationTab(
        name: string,
        func: GMLua.CallbackNoContext,
        material?: string,
        order?: number,
        tooltip?: string
    ): void;

    /**
 * Used to add addon spawnlists to the spawnmenu tree. This function should be called within SANDBOX:PopulatePropMenu.
* 
* @arg string classname - A unique classname of the list.
* @arg string name - The name of the category displayed to the player, e.g. Comic Props.
* @arg object contents - A table of entries for the spawn menu. It must be numerically indexed.
Each member of the table is a sub-table containing a type member, and other members depending on the type.
New content types can be added via spawnmenu.AddContentType.



string type
Description
Other members




"header"
a simple header for organization
string text - The text that the header will display


"model"
spawns a model where the player is looking
string model - The path to the model file 	 number skin - The skin for the model to use (optional)  string body - The bodygroups for the model (optional)  number wide - The width of the spawnicon (optional)  number tall - The height of the spawnicon (optional)


"entity"
spawns an entity where the player is looking(appears in the Entities tab by default)
string spawnname - The filename of the entity, for example "sent_ball"  string nicename - The name of the entity to display  string material - The icon to display, this should be set to "entities/<sent_name>.png" 	boolean admin - Whether the entity is only spawnable by admins (optional)


"vehicle"
spawns a vehicle where the player is looking  (appears in the Vehicles tab by default)
string spawnname - The filename of the vehicle  string nicename - The name of the vehicle to display  string material - The icon to display  boolean admin - Whether the vehicle is only spawnable by admins (optional)


"npc"
spawns an NPC where the player is looking  (appears in the NPCs tab by default)
string spawnname - The spawn name of the NPC 	string nicename - The name to display  string material - The icon to display  table weapon - A table of potential weapons (each a string) to give to the NPC. When spawned, one of these will be chosen randomly each time.  boolean admin - Whether the NPC is only spawnable by admins (optional)


"weapon"
When clicked, gives the player a weapon;  When middle-clicked, spawns a weapon where the player is looking  (appears in the Weapons tab by default)
string spawnname - The spawn name of the weapon  string nicename - The name to display  string material - The icon to display  boolean admin - Whether the weapon is only spawnable by admins (optional)
* @arg string icon - The icon to use in the tree.
* @arg number [id=1000] - The unique ID number for the spawnlist category. Used to make sub categories. See "parentID" parameter below. If not set, it will be automatically set to ever increasing number, starting with 1000.
* @arg number [parentID=0] - The unique ID of the parent category. This will make the created category a subcategory of category with given unique ID. 0 makes this a base category (such as Builder).
* @arg string needsApp - The needed game for this prop category, if one is needed. If the specified game is not mounted, the category isn't shown. This uses the shortcut name, e.g. cstrike, and not the Steam AppID.
 */
    function AddPropCategory(
        classname: string,
        name: string,
        contents: object,
        icon: string,
        id?: number,
        parentID?: number,
        needsApp?: string
    ): void;

    /**
 * Used to create a new category in the list inside of a spawnmenu ToolTab.
* 
* @arg string tab - The ToolTab name, as created with spawnmenu.AddToolTab.
You can also use the default ToolTab names "Main" and "Utilities".
* @arg string RealName - The identifier name
* @arg string PrintName - The displayed name
 */
    function AddToolCategory(tab: string, RealName: string, PrintName: string): void;

    /**
 * Adds an option to the right side of the spawnmenu
* 
* @arg string tab - The spawnmenu tab to add into (for example "Utilities")
* @arg string category - The category to add into (for example "Admin")
* @arg string _class - Unique identifier of option to add
* @arg string name - The nice name of item
* @arg string cmd - Command to execute when the item is selected
* @arg string config - Config name, used in older versions to load tool settings UI from a file. No longer works.
We advise against using this. It may be changed or removed in a future update.

Category=No
* @arg GMLua.CallbackNoContext cpanel - A function to build the context panel. The function has one argument:

Panel pnl - A DForm that will be shown in the context menu
* @arg object [table={}] - Allows to override the table that will be added to the tool list. Some of the fields will be overwritten by this function.
 */
    function AddToolMenuOption(
        tab: string,
        category: string,
        _class: string,
        name: string,
        cmd: string,
        config: string,
        cpanel: GMLua.CallbackNoContext,
        table?: object
    ): void;

    /**
     * Adds a new tool tab to the right side of the spawnmenu via the SANDBOX:AddToolMenuTabs hook.
     *
     * @arg string name - The internal name of the tab. This is used for sorting.
     * @arg string [label="name"] - The 'nice' name of the tab (Tip: language.Add)
     * @arg string [icon="icon16/wrench.png"] - The filepath to the icon of the tab. Should be a .png
     */
    function AddToolTab(name: string, label?: string, icon?: string): void;

    /**
     * Clears all the tools from the different tool categories and the categories itself, if ran at the correct place.
     */
    function ClearToolMenus(): void;

    /**
     * Creates a new content icon.
     *
     * @arg string type - The type of the content icon.
     * @arg Panel parent - The parent to add the content icon to.
     * @arg any data - The data to send to the content icon in spawnmenu.AddContentType
     * @return {Panel} The created content icon, if it was returned by spawnmenu.AddContentType
     */
    function CreateContentIcon(type: string, parent: Panel, data: any): Panel;

    /**
     * Calls spawnmenu.SaveToTextFiles.
     *
     * @arg object spawnlists - A table containing spawnlists.
     */
    function DoSaveToTextFiles(spawnlists: object): void;

    /**
     * Returns the function to create an vgui element for a specified content type
     *
     * @arg string contentType
     * @return {Function} The panel creation function
     */
    function GetContentType(contentType: string): Function;

    /**
     * Returns the list of Creation tabs. Creation tabs are added via spawnmenu.AddCreationTab.
     *
     * @return {CreationMenusStruct} The list of Creation tabs. See the Structures/CreationMenus.
     */
    function GetCreationTabs(): CreationMenusStruct;

    /**
     * Similar to spawnmenu.GetPropTable, but only returns spawnlists created by addons via spawnmenu.AddPropCategory.
     *
     * @return {any} See spawnmenu.GetPropTable for table format.
     */
    function GetCustomPropTable(): any;

    /**
 * Returns a table of all prop categories and their props in the spawnmenu.
* 
* @return {object} Table of all the prop categories and props in the following format:
{
	["settings/spawnlist/001-construction props.txt"] = {
		name = "Construction Props",
		icon = "icon16/page.png",
		id = 1,
		parentid = 0,
		needsapp = "",
		contents = {
			{
				model = "models/Cranes/crane_frame.mdl",
				type = "model"
			}
			-- etc.
		},
	}
	-- etc.
}
 */
    function GetPropTable(): object;

    /**
     * Adds a new tool tab (or returns an existing one by name) to the right side of the spawnmenu via the SANDBOX:AddToolMenuTabs hook.
     *
     * @arg string name - The internal name of the tab. This is used for sorting.
     * @arg string [label="name"] - The 'nice' name of the tab
     * @arg string [icon="icon16/wrench.png"] - The filepath to the icon of the tab. Should be a .png
     * @return {object} A table of tables representing categories and items in the left part of the tab. See example below to example structure.
     */
    function GetToolMenu(name: string, label?: string, icon?: string): object;

    /**
     * Gets a table of tools on the client.
     *
     * @return {object} A table with groups of tools, along with information on each tool.
     */
    function GetTools(): object;

    /**
     * Calls spawnmenu.PopulateFromTextFiles.
     */
    function PopulateFromEngineTextFiles(): void;

    /**
     * Loads spawnlists from text files.
     *
     * @arg GMLua.CallbackNoContext callback - The function to call. Arguments are ( strFilename, strName, tabContents, icon, id, parentid, needsapp )
     */
    function PopulateFromTextFiles(callback: GMLua.CallbackNoContext): void;

    /**
     * Saves a table of spawnlists to files.
     *
     * @arg object spawnlists - A table containing spawnlists.
     */
    function SaveToTextFiles(spawnlists: object): void;

    /**
     * Sets currently active control panel to be returned by spawnmenu.ActiveControlPanel.
     *
     * @arg Panel pnl - The panel to set.
     */
    function SetActiveControlPanel(pnl: Panel): void;

    /**
     * Switches the creation tab (left side of the spawnmenu) on the spawnmenu to the given tab.
     *
     * @arg number id - The tab ID to open
     */
    function SwitchCreationTab(id: number): void;

    /**
     * Opens specified tool tab in spawnmenu.
     *
     * @arg number id - The tab ID to open
     */
    function SwitchToolTab(id: number): void;
}
