/** @noSelfInFile */
declare namespace saverestore {
    /**
 * Adds a restore/load hook for the Half-Life 2 save system.
* 
* @arg string identifier - The unique identifier for this hook.
* @arg GMLua.CallbackNoContext callback - The function to be called when an engine save is being loaded. It has one argument:
IRestore save - The restore object to be used to read data from save file that is being loaded
You can also use those functions to read data:
saverestore.ReadVar
saverestore.ReadTable
saverestore.LoadEntity
 */
    function AddRestoreHook(identifier: string, callback: GMLua.CallbackNoContext): void;

    /**
 * Adds a save hook for the Half-Life 2 save system. You can this to carry data through level transitions in Half-Life 2.
* 
* @arg string identifier - The unique identifier for this hook.
* @arg GMLua.CallbackNoContext callback - The function to be called when an engine save is being saved. It has one argument:
ISave save - The save object to be used to write data to the save file that is being saved
You can also use those functions to save data:
saverestore.WriteVar
saverestore.WriteTable
saverestore.SaveEntity
 */
    function AddSaveHook(identifier: string, callback: GMLua.CallbackNoContext): void;

    /**
     * Loads Entity:GetTable from the save game file that is being loaded and merges it with the given entitys Entity:GetTable.
     *
     * @arg Entity ent - The entity which will receive the loaded values from the save.
     * @arg IRestore save - The restore object to read the Entity:GetTable from.
     */
    function LoadEntity(ent: Entity, save: IRestore): void;

    /**
     * Called by engine when a save is being loaded.
     *
     * @arg IRestore save - The restore object to read data from the save file with.
     */
    function LoadGlobal(save: IRestore): void;

    /**
     * Called by the engine just before saverestore.LoadGlobal is.
     */
    function PreRestore(): void;

    /**
     * Called by the engine just before saverestore.SaveGlobal is.
     */
    function PreSave(): void;

    /**
     * Reads a table from the save game file that is being loaded.
     *
     * @arg IRestore save - The restore object to read the table from.
     * @return {object} The table that has been read, if any
     */
    function ReadTable(save: IRestore): object;

    /**
     * Loads a variable from the save game file that is being loaded.
     *
     * @arg IRestore save - The restore object to read variables from.
     * @return {any} The variable that was read, if any.
     */
    function ReadVar(save: IRestore): any;

    /**
     * Saves entitys Entity:GetTable to the save game file that is being saved.
     *
     * @arg Entity ent - The entity to save Entity:GetTable of.
     * @arg ISave save - The save object to save Entity:GetTable to.
     */
    function SaveEntity(ent: Entity, save: ISave): void;

    /**
     * Called by engine when a save is being saved.
     *
     * @arg ISave save - The save object to write data into the save file.
     */
    function SaveGlobal(save: ISave): void;

    /**
     * Returns how many writable keys are in the given table.
     *
     * @arg object table - The table to test.
     * @return {number} The number of keys that can be written with saverestore.WriteTable.
     */
    function WritableKeysInTable(table: object): number;

    /**
     * Write a table to a save game file that is being saved.
     *
     * @arg object table - The table to write
     * @arg ISave save - The save object to write the table to.
     */
    function WriteTable(table: object, save: ISave): void;

    /**
 * Writes a variable to the save game file that is being saved.
* 
* @arg any value - The value to save.
It can be one of the following types: number, boolean, string, Entity, Angle, Vector or table.
* @arg ISave save - The save object to write the variable to.
 */
    function WriteVar(value: any, save: ISave): void;
}
