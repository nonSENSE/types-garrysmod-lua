/** @noSelfInFile */
declare namespace ai_task {
    /**
     * Create a new empty task. Used by Schedule:AddTask and Schedule:EngTask.
     *
     * @return {Task} The new task object.
     */
    function New(): Task;
}
