/** @noSelfInFile */
declare namespace cookie {
    /**
     * Deletes a cookie on the client.
     *
     * @arg string name - The name of the cookie that you want to delete.
     */
    function Delete(name: string): void;

    /**
     * Gets the value of a cookie on the client as a number.
     *
     * @arg string name - The name of the cookie that you want to get.
     * @arg any [def=nil] - Value to return if the cookie does not exist.
     * @return {number} The cookie value
     */
    function GetNumber(name: string, def?: any): number;

    /**
     * Gets the value of a cookie on the client as a string.
     *
     * @arg string name - The name of the cookie that you want to get.
     * @arg any [def=nil] - Value to return if the cookie does not exist.
     * @return {string} The cookie value
     */
    function GetString(name: string, def?: any): string;

    /**
     * Sets the value of a cookie, which is saved automatically by the sql.
     *
     * @arg string key - The name of the cookie that you want to set.
     * @arg string value - Value to store in the cookie.
     */
    function Set(key: string, value: string): void;
}
