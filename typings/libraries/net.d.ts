/** @noSelfInFile */
declare namespace net {
    /**
     * Sends the currently built net message to all connected players.
     * More information can be found in Net Library Usage.
     */
    function Broadcast(): void;

    /**
 * Returns the amount of data left to read in the current message. Does nothing when sending data.
* 
* @return {number} The amount of data left to read in the current net message in bytes.
Returns nil if no net message has been started.
* @return {number} The amount of data left to read in the current net message in bits.
Returns nil if no net message has been started.
 */
    function BytesLeft(): LuaMultiReturn<[number, number]>;

    /**
 * Returns the size of the current message.
* 
* @return {number} The amount of bytes written to the current net message.
Returns nil if no net message has been started.
* @return {number} The amount of bits written to the current net message.
Returns nil if no net message has been started.
 */
    function BytesWritten(): LuaMultiReturn<[number, number]>;

    /**
     * Function called by the engine to tell the Lua state a message arrived.
     *
     * @arg number length - The message length, in bits.
     * @arg Player client - The player that sent the message. This will be nil in the client state.
     */
    function Incoming(length: number, client: Player): void;

    /**
     * Reads an angle from the received net message.
     *
     * @return {Angle} The read angle, or Angle( 0, 0, 0 ) if no angle could be read
     */
    function ReadAngle(): Angle;

    /**
     * Reads a bit from the received net message.
     *
     * @return {number} 0 or 1, or 0 if the bit could not be read.
     */
    function ReadBit(): number;

    /**
     * Reads a boolean from the received net message.
     *
     * @return {boolean} true or false, or false if the bool could not be read.
     */
    function ReadBool(): boolean;

    /**
     * Reads a Color from the current net message.
     *
     * @arg boolean [hasAlpha=true] - If the color has alpha written or not. Must match what was given to net.WriteColor.
     * @return {Color} The Color read from the current net message, or Color( 0, 0, 0, 0 ) if the color could not be read.
     */
    function ReadColor(hasAlpha?: boolean): Color;

    /**
     * Reads pure binary data from the message.
     *
     * @arg number length - The length of the data to be read, in bytes.
     * @return {string} The binary data read, or a string containing one character with a byte of 0 if no data could be read.
     */
    function ReadData(length: number): string;

    /**
     * Reads a double-precision number from the received net message.
     *
     * @return {number} The double-precision number, or 0 if no number could be read.
     */
    function ReadDouble(): number;

    /**
     * Reads an entity from the received net message. You should always check if the specified entity exists as it may have been removed and therefore NULL if it is outside of the players PVS or was already removed.
     *
     * @return {Entity} The entity, or Entity(0) if no entity could be read.
     */
    function ReadEntity(): Entity;

    /**
     * Reads a floating point number from the received net message.
     *
     * @return {number} The floating point number, or 0 if no number could be read.
     */
    function ReadFloat(): number;

    /**
     * Reads a word, basically unsigned short. This is used internally to read the "header" of the message which is an unsigned short which can be converted to the corresponding message name via util.NetworkIDToString.
     *
     * @return {number} The header number
     */
    function ReadHeader(): number;

    /**
 * Reads an integer from the received net message.
* 
* @arg number bitCount - The amount of bits to be read.
This must be set to what you set to net.WriteInt. Read more information at net.WriteInt.
* @return {number} The read integer number, or 0 if no integer could be read.
 */
    function ReadInt(bitCount: number): number;

    /**
     * Reads a VMatrix from the received net message.
     *
     * @return {VMatrix} The matrix, or an empty matrix if no matrix could be read.
     */
    function ReadMatrix(): VMatrix;

    /**
     * Reads a normal vector from the net message.
     *
     * @return {Vector} The normalized vector ( length = 1 ), or Vector( 0, 0, 1 ) if no normal could be read.
     */
    function ReadNormal(): Vector;

    /**
     * Reads a null-terminated string from the net stream. The size of the string is 8 bits plus 8 bits for every ASCII character in the string.
     *
     * @return {string} The read string, or a string with 0 length if no string could be read.
     */
    function ReadString(): string;

    /**
     * Reads a table from the received net message.
     *
     * @return {object} Table recieved via the net message, or a blank table if no table could be read.
     */
    function ReadTable(): object;

    /**
     * Reads a value from the net message with the specified type, written by net.WriteType.
     *
     * @arg number [typeID=NaN] - The type of value to be read, using Enums/TYPE.
     * @return {any} The value, or the respective blank value based on the type you're reading if the value could not be read.
     */
    function ReadType(typeID?: number): any;

    /**
     * Reads an unsigned integer with the specified number of bits from the received net message.
     *
     * @arg number numberOfBits - The size of the integer to be read, in bits.
     * @return {number} The unsigned integer read, or 0 if the integer could not be read.
     */
    function ReadUInt(numberOfBits: number): number;

    /**
     * Reads a vector from the received net message. Vectors sent by this function are compressed, which may result in precision loss. See net.WriteVector for more information.
     *
     * @return {Vector} The read vector, or Vector( 0, 0, 0 ) if no vector could be read.
     */
    function ReadVector(): Vector;

    /**
 * Adds a net message handler. Only one receiver can be used to receive the net message.
* 
* @arg string messageName - The message name to hook to.
* @arg GMLua.CallbackNoContext callback - The function to be called if the specified message was received. Arguments are:

number len - Length of the message, in bits.
Player ply - The player that sent the message, works only server-side.
 */
    function Receive(messageName: string, callback: GMLua.CallbackNoContext): void;

    /**
     * Sends the current message to the specified player, or to all players listed in the table.
     *
     * @arg Player ply - The player(s) to send the message to. Can be a table of players or a CRecipientFilter.
     */
    function Send(ply: Player): void;

    /**
     * Sends the current message to all except the specified, or to all except all players in the table.
     *
     * @arg Player ply - The player(s) to NOT send the message to. Can be a table of players.
     */
    function SendOmit(ply: Player): void;

    /**
     * Sends the message to all players that are in the same Potentially Audible Set (PAS) as the position, or simply said, it adds all players that can potentially hear sounds from this position.
     *
     * @arg Vector position - PAS position.
     */
    function SendPAS(position: Vector): void;

    /**
     * Sends the message to all players the position is in the PVS of or, more simply said, sends the message to players that can potentially see this position.
     *
     * @arg Vector position - Position that must be in players' visibility set.
     */
    function SendPVS(position: Vector): void;

    /**
     * Sends the current message to the server.
     */
    function SendToServer(): void;

    /**
     * Begins a new net message. If another net message is already started and hasn't been sent yet, it will be discarded.
     *
     * @arg string messageName - The name of the message to send
     * @arg boolean [unreliable=false] - If set to true, the message is not guaranteed to reach its destination
     * @return {boolean} true if the message has been started.
     */
    function Start(messageName: string, unreliable?: boolean): boolean;

    /**
     * Writes an angle to the current net message.
     *
     * @arg Angle angle - The angle to be sent.
     */
    function WriteAngle(angle: Angle): void;

    /**
     * Appends a boolean (as 1 or 0) to the current net message.
     *
     * @arg boolean boolean - Bit status (false = 0, true = 1).
     */
    function WriteBit(boolean: boolean): void;

    /**
     * Appends a boolean to the current net message. Alias of net.WriteBit.
     *
     * @arg boolean boolean - Boolean value to write.
     */
    function WriteBool(boolean: boolean): void;

    /**
     * Appends a Color to the current net message.
     *
     * @arg Color Color - The Color you want to append to the net message.
     * @arg boolean [writeAlpha=true] - If we should write the alpha of the color or not.
     */
    function WriteColor(Color: Color, writeAlpha?: boolean): void;

    /**
     * Writes a chunk of binary data to the message.
     *
     * @arg string binaryData - The binary data to be sent.
     * @arg number [length=NaN] - The length of the binary data to be sent, in bytes.
     */
    function WriteData(binaryData: string, length?: number): void;

    /**
     * Appends a double-precision number to the current net message.
     *
     * @arg number double - The double to be sent
     */
    function WriteDouble(double: number): void;

    /**
     * Appends an entity to the current net message.
     *
     * @arg Entity entity - The entity to be sent.
     */
    function WriteEntity(entity: Entity): void;

    /**
     * Appends a float (number with decimals) to the current net message.
     *
     * @arg number float - The float to be sent.
     */
    function WriteFloat(float: number): void;

    /**
 * Appends an integer - a whole number - to the current net message. Can be read back with net.ReadInt on the receiving end.
* 
* @arg number integer - The integer to be sent.
* @arg number bitCount - The amount of bits the number consists of. This must be 32 or less.
If you are unsure what to set, just set it to 32.
Consult the table below to determine the bit count you need:



Bit Count
Minimum value
Maximum value




3
-4
3


4
-8
7


5
-16
15


6
-32
31


7
-64
63


8
-128
127


9
-256
255


10
-512
511


11
-1024
1023


12
-2048
2047


13
-4096
4095


14
-8192
8191


15
-16384
16383


16
-32768
32767


17
-65536
65535


18
-131072
131071


19
-262144
262143


20
-524288
524287


21
-1048576
1048575


22
-2097152
2097151


23
-4194304
4194303


24
-8388608
8388607


25
-16777216
16777215


26
-33554432
33554431


27
-67108864
67108863


28
-134217728
134217727


29
-268435456
268435455


30
-536870912
536870911


31
-1073741824
1073741823


32
-2147483648
2147483647
 */
    function WriteInt(integer: number, bitCount: number): void;

    /**
     * Writes a VMatrix to the current net message.
     *
     * @arg VMatrix matrix - The matrix to be sent.
     */
    function WriteMatrix(matrix: VMatrix): void;

    /**
     * Writes a normalized/direction vector ( Vector with length of 1 ) to the net message.
     *
     * @arg Vector normal - The normalized/direction vector to be send.
     */
    function WriteNormal(normal: Vector): void;

    /**
     * Appends a string to the current net message. The size of the string is 8 bits plus 8 bits for every ASCII character in the string. The maximum allowed length of a single written string is 65532 characters.
     *
     * @arg string string - The string to be sent.
     */
    function WriteString(string: string): void;

    /**
 * Appends a table to the current net message. Adds 16 extra bits per key/value pair so you're better off writing each individual key/value as the exact type if possible.
* 
* @arg object table - The table to be sent.
If the table contains a nil key the table may not be read correctly.
Not all objects can be sent over the network. Things like functions, IMaterials, etc will cause errors when reading the table from a net message.
 */
    function WriteTable(table: object): void;

    /**
     * Appends any type of value to the current net message.
     *
     * @arg any Data - The data to be sent.
     */
    function WriteType(Data: any): void;

    /**
 * Appends an unsigned integer with the specified number of bits to the current net message.
* 
* @arg number unsignedInteger - The unsigned integer to be sent.
* @arg number numberOfBits - The size of the integer to be sent, in bits. Acceptable values range from any number 1 to 32 inclusive.
For reference: 1 = bit, 4 = nibble, 8 = byte, 16 = short, 32 = long.
Consult the table below to determine the bit count you need. The minimum value for all bit counts is 0.



Bit Count
Maximum value




1
1


2
3


3
7


4
15


5
31


6
63


7
127


8
255


9
511


10
1023


11
2047


12
4095


13
8191


14
16383


15
32767


16
65535


17
131071


18
262143


19
524287


20
1048575


21
2097151


22
4194303


23
8388607


24
16777215


25
33554431


26
67108863


27
134217727


28
268435455


29
536870911


30
1073741823


31
2147483647


32
4294967295
 */
    function WriteUInt(unsignedInteger: number, numberOfBits: number): void;

    /**
     * Appends a vector to the current net message.
     * Vectors sent by this function are compressed, which may result in precision loss. XYZ components greater than 16384 or less than -16384 are irrecoverably altered (most significant bits are trimmed) and precision after the decimal point is low.
     *
     * @arg Vector vector - The vector to be sent.
     */
    function WriteVector(vector: Vector): void;
}
