/** @noSelfInFile */
declare namespace scripted_ents {
    /**
     * Defines an alias string that can be used to refer to another classname
     *
     * @arg string alias - A new string which can be used to refer to another classname
     * @arg string classname - The classname the alias should refer to
     */
    function Alias(alias: string, classname: string): void;

    /**
     * Returns a copy of the ENT table for a class, including functions defined by the base class
     *
     * @arg string classname - The classname of the ENT table to return, can be an alias
     * @return {object} entTable
     */
    function Get(classname: string): object;

    /**
 * Returns a copy of the list of all ENT tables registered
* 
* @return {object} A table of all entities in the following format: (table keys are the classnames)

table t - The ENT table associated with the entity
boolean isBaseType - Always true
string Base - The entity base (note capital B in the key name)
string type - The entity type
 */
    function GetList(): object;

    /**
     * Retrieves a member of entity's table.
     *
     * @arg string _class - Entity's class name
     * @arg string name - Name of member to retrieve
     * @return {any} The member or nil if failed
     */
    function GetMember(_class: string, name: string): any;

    /**
     * Returns a list of all ENT tables which contain ENT.Spawnable
     *
     * @return {ENTStruct} A table of Structures/ENTs
     */
    function GetSpawnable(): ENTStruct;

    /**
     * Returns the actual ENT table for a class. Modifying functions/variables in this table will change newly spawned entities
     *
     * @arg string classname - The classname of the ENT table to return
     * @return {object} entTable
     */
    function GetStored(classname: string): object;

    /**
     * Returns the 'type' of a class, this will one of the following: 'anim', 'ai', 'brush', 'point'.
     *
     * @arg string classname - The classname to check
     * @return {string} type
     */
    function GetType(classname: string): string;

    /**
     * Checks if name is based on base
     *
     * @arg string name - Entity's class name to be checked
     * @arg string base - Base class name to be checked
     * @return {boolean} Returns true if class name is based on base, else false.
     */
    function IsBasedOn(name: string, base: string): boolean;

    /**
     * Called after all ENTS have been loaded and runs baseclass.Set on each one.
     */
    function OnLoaded(): void;

    /**
     * Registers an ENT table with a classname. Reregistering an existing classname will automatically update the functions of all existing entities of that class.
     *
     * @arg object ENT - The ENT table to register.
     * @arg string classname - The classname to register.
     */
    function Register(ENT: object, classname: string): void;
}
