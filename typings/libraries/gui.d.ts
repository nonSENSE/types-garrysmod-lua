/** @noSelfInFile */
declare namespace gui {
    /**
     * Opens the game menu overlay.
     */
    function ActivateGameUI(): void;

    /**
     * Enables the mouse cursor without restricting player movement, like using Sandbox's context menu.
     *
     * @arg boolean enabled - Whether the cursor should be enabled or not. (true = enable, false = disable)
     */
    function EnableScreenClicker(enabled: boolean): void;

    /**
     * Hides the game menu overlay.
     */
    function HideGameUI(): void;

    /**
     * Simulates a mouse move with the given deltas.
     *
     * @arg number deltaX - The movement delta on the x axis.
     * @arg number deltaY - The movement delta on the y axis.
     */
    function InternalCursorMoved(deltaX: number, deltaY: number): void;

    /**
     * Simulates a key press for the given key.
     *
     * @arg number key - The key, see Enums/KEY.
     */
    function InternalKeyCodePressed(key: number): void;

    /**
     * Simulates a key release for the given key.
     *
     * @arg number key - The key, see Enums/KEY.
     */
    function InternalKeyCodeReleased(key: number): void;

    /**
     * Simulates a key type typing to the specified key.
     *
     * @arg number key - The key, see Enums/KEY.
     */
    function InternalKeyCodeTyped(key: number): void;

    /**
     * Simulates an ASCII symbol writing.
     * Use to write text in the chat or in VGUI.
     * Doesn't work while the main menu is open!
     *
     * @arg number code - ASCII code of symbol, see http://www.mikroe.com/img/publication/spa/pic-books/programming-in-basic/chapter/04/fig4-24.gif
     */
    function InternalKeyTyped(code: number): void;

    /**
     * Simulates a double mouse key press for the given mouse key.
     *
     * @arg number key - The key, see Enums/MOUSE.
     */
    function InternalMouseDoublePressed(key: number): void;

    /**
     * Simulates a mouse key press for the given mouse key.
     *
     * @arg number key - The key, see Enums/MOUSE.
     */
    function InternalMousePressed(key: number): void;

    /**
     * Simulates a mouse key release for the given mouse key.
     *
     * @arg number key - The key, see Enums/MOUSE.
     */
    function InternalMouseReleased(key: number): void;

    /**
     * Simulates a mouse wheel scroll with the given delta.
     *
     * @arg number delta - The amount of scrolling to simulate.
     */
    function InternalMouseWheeled(delta: number): void;

    /**
     * Returns whether the console is visible or not.
     *
     * @return {boolean} Whether the console is visible or not.
     */
    function IsConsoleVisible(): boolean;

    /**
     * Returns whether the game menu overlay ( main menu ) is open or not.
     *
     * @return {boolean} Whether the game menu overlay ( main menu ) is open or not
     */
    function IsGameUIVisible(): boolean;

    /**
     * Returns the cursor's position on the screen, or 0, 0 if cursor is not visible.
     *
     * @return {number} mouseX
     * @return {number} mouseY
     */
    function MousePos(): LuaMultiReturn<[number, number]>;

    /**
     * Returns x component of the mouse position.
     *
     * @return {number} mouseX
     */
    function MouseX(): number;

    /**
     * Returns y component of the mouse position.
     *
     * @return {number} mouseY
     */
    function MouseY(): number;

    /**
     * Opens specified URL in the steam overlay browser.
     *
     * @arg string url - URL to open, it has to start with either http:// or https://.
     */
    function OpenURL(url: string): void;

    /**
     * Converts the specified screen position to a direction vector local to the player's view. A related function is Vector:ToScreen, which translates a 3D position to a screen coordinate.
     *
     * @arg number x - X coordinate on the screen.
     * @arg number y - Y coordinate on the screen.
     * @return {Vector} Direction
     */
    function ScreenToVector(x: number, y: number): Vector;

    /**
     * Sets the cursor's position on the screen, relative to the topleft corner of the window
     *
     * @arg number mouseX - The X coordinate to move the cursor to.
     * @arg number mouseY - The Y coordinate to move the cursor to.
     */
    function SetMousePos(mouseX: number, mouseY: number): void;

    /**
     * Shows console in the game UI.
     */
    function ShowConsole(): void;
}
