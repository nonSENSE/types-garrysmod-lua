/** @noSelfInFile */
declare namespace chat {
    /**
 * Adds text to the local player's chat box (which only they can read).
* 
* @arg args[] arguments - The arguments. Arguments can be:

table - Color. Will set the color for all following strings until the next Color argument.
string - Text to be added to the chat box.
Player - Adds the name of the player in the player's team color to the chat box.
any - Any other type, such as Entity will be converted to string and added as text.
 */
    function AddText(...arguments: any[]): void;

    /**
     * Closes the chat window.
     */
    function Close(): void;

    /**
     * Returns the chatbox position.
     *
     * @return {number} The X coordinate of the chatbox's position.
     * @return {number} The Y coordinate of the chatbox's position.
     */
    function GetChatBoxPos(): LuaMultiReturn<[number, number]>;

    /**
     * Returns the chatbox size.
     *
     * @return {number} The width of the chatbox.
     * @return {number} The height of the chatbox.
     */
    function GetChatBoxSize(): LuaMultiReturn<[number, number]>;

    /**
     * Opens the chat window.
     *
     * @arg number mode - If equals 1, opens public chat, otherwise opens team chat
     */
    function Open(mode: number): void;

    /**
     * Plays the chat "tick" sound.
     */
    function PlaySound(): void;
}
