/** @noSelfInFile */
declare namespace cvars {
    /**
 * Adds a callback to be called when the named convar changes.
* 
* @arg string name - The name of the convar to add the change callback to.
* @arg GMLua.CallbackNoContext callback - The function to be called when the convar changes. The arguments passed are:

string convar - The name of the convar.
string oldValue - The old value of the convar.
string newValue - The new value of the convar.
* @arg string [identifier="nil"] - If set, you will be able to remove the callback using cvars.RemoveChangeCallback.
 */
    function AddChangeCallback(name: string, callback: GMLua.CallbackNoContext, identifier?: string): void;

    /**
     * Retrieves console variable as a boolean.
     *
     * @arg string cvar - Name of console variable
     * @arg boolean [def=false] - The value to return if the console variable does not exist
     * @return {boolean} Retrieved value
     */
    function Bool(cvar: string, def?: boolean): boolean;

    /**
 * Returns a table of the given ConVars callbacks.
* 
* @arg string name - The name of the ConVar.
* @arg boolean [createIfNotFound=false] - Whether or not to create the internal callback table for given ConVar if there isn't one yet.
This argument is internal and should not be used.
* @return {object} A table of the convar's callbacks, or nil if the convar doesn't exist.
 */
    function GetConVarCallbacks(name: string, createIfNotFound?: boolean): object;

    /**
     * Retrieves console variable as a number.
     *
     * @arg string cvar - Name of console variable
     * @arg any [def=nil] - The value to return if the console variable does not exist
     * @return {number} Retrieved value
     */
    function Number(cvar: string, def?: any): number;

    /**
     * Called by the engine when a convar value changes.
     *
     * @arg string name - Convar name
     * @arg string oldVal - The old value of the convar
     * @arg string newVal - The new value of the convar
     */
    function OnConVarChanged(name: string, oldVal: string, newVal: string): void;

    /**
     * Removes a callback for a convar using the the callback's identifier. The identifier should be the third argument specified for cvars.AddChangeCallback.
     *
     * @arg string name - The name of the convar to remove the callback from.
     * @arg string indentifier - The callback's identifier.
     */
    function RemoveChangeCallback(name: string, indentifier: string): void;

    /**
     * Retrieves console variable as a string.
     *
     * @arg string cvar - Name of console variable
     * @arg any [def=nil] - The value to return if the console variable does not exist
     * @return {string} Retrieved value
     */
    function String(cvar: string, def?: any): string;
}
