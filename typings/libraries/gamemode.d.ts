/** @noSelfInFile */
declare namespace gamemode {
    /**
     * Called by the engine to call a hook within the loaded gamemode.
     *
     * @arg string name - The name of the hook to call.
     * @arg args[] args - The arguments
     * @return {any} The result of the hook function - can be up to 6 values. Returns false if the gamemode function doesn't exist (i.e. nothing happened), but remember - a hook can also return false.
     */
    function Call(name: string, ...args: any[]): any;

    /**
     * This returns the internally stored gamemode table.
     *
     * @arg string name - The name of the gamemode you want to get
     * @return {object} The gamemode's table
     */
    function Get(name: string): object;

    /**
     * Called by the engine when a gamemode is being loaded.
     *
     * @arg object gm - Your GM table
     * @arg string name - Name of your gamemode, lowercase, no spaces.
     * @arg string derived - The gamemode name that your gamemode is derived from
     */
    function Register(gm: object, name: string, derived: string): void;
}
