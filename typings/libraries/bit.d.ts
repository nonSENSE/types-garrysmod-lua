/** @noSelfInFile */
declare namespace bit {
    /**
     * Returns the arithmetically shifted value.
     *
     * @arg number value - The value to be manipulated.
     * @arg number shiftCount - Amounts of bits to shift.
     * @return {number} shiftedValue
     */
    function arshift(value: number, shiftCount: number): number;

    /**
     * Performs the bitwise and for all values specified.
     *
     * @arg number value - The value to be manipulated.
     * @arg number [otherValues=NaN] - Values bit to perform bitwise "and" with. Optional.
     * @return {number} Result of bitwise "and" operation.
     */
    function band(value: number, otherValues?: number): number;

    /**
     * Returns the bitwise not of the value.
     *
     * @arg number value - The value to be inverted.
     * @return {number} bitwiseNot
     */
    function bnot(value: number): number;

    /**
     * Returns the bitwise OR of all values specified.
     *
     * @arg number value1 - The first value.
     * @arg args[] args - Extra values to be evaluated. (must all be numbers)
     * @return {number} The bitwise OR result between all numbers.
     */
    function bor(value1: number, ...args: any[]): number;

    /**
     * Swaps the byte order.
     *
     * @arg number value - The value to be byte swapped.
     * @return {number} swapped
     */
    function bswap(value: number): number;

    /**
     * Returns the bitwise xor of all values specified.
     *
     * @arg number value - The value to be manipulated.
     * @arg number [otherValues=NaN] - Values bit xor with. Optional.
     * @return {number} bitwiseXOr
     */
    function bxor(value: number, otherValues?: number): number;

    /**
     * Returns the left shifted value.
     *
     * @arg number value - The value to be manipulated.
     * @arg number shiftCount - Amounts of bits to shift left by.
     * @return {number} shiftedValue
     */
    function lshift(value: number, shiftCount: number): number;

    /**
     * Returns the left rotated value.
     *
     * @arg number value - The value to be manipulated.
     * @arg number shiftCount - Amounts of bits to rotate left by.
     * @return {number} shiftedValue
     */
    function rol(value: number, shiftCount: number): number;

    /**
     * Returns the right rotated value.
     *
     * @arg number value - The value to be manipulated.
     * @arg number shiftCount - Amounts of bits to rotate right by.
     * @return {number} shiftedValue
     */
    function ror(value: number, shiftCount: number): number;

    /**
     * Returns the right shifted value.
     *
     * @arg number value - The value to be manipulated.
     * @arg number shiftCount - Amounts of bits to shift right by.
     * @return {number} shiftedValue
     */
    function rshift(value: number, shiftCount: number): number;

    /**
     * Normalizes the specified value and clamps it in the range of a signed 32bit integer.
     *
     * @arg number value - The value to be normalized.
     * @return {number} swapped
     */
    function tobit(value: number): number;

    /**
     * Returns the hexadecimal representation of the number with the specified digits.
     *
     * @arg number value - The value to be normalized.
     * @arg number [digits=8] - The number of digits. Optional
     * @return {string} hexString
     */
    function tohex(value: number, digits?: number): string;
}
