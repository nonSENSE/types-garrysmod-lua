/** @noSelfInFile */
declare namespace coroutine {
    /**
     * Creates a coroutine of the given function.
     *
     * @arg GMLua.CallbackNoContext func - The function for the coroutine to use.
     * @return {any} coroutine
     */
    function create(func: GMLua.CallbackNoContext): any;

    /**
     * Resumes the given coroutine and passes the given vararg to either the function arguments or the coroutine.yield that is inside that function and returns whatever yield is called with the next time or by the final return in the function.
     *
     * @arg any coroutine - Coroutine to resume.
     * @arg args[] args - Arguments to be returned by coroutine.yield.
     * @return {boolean} If the executed thread code had no errors occur within it.
     * @return {args[]} If an error occurred, this will be a string containing the error message. Otherwise, this will be arguments that were yielded.
     */
    function resume(coroutine: any, ...args: any[]): LuaMultiReturn<[boolean, ...any[]]>;

    /**
     * Returns the active coroutine or nil if we are not within a coroutine.
     *
     * @return {any} coroutine
     */
    function running(): any;

    /**
     * Returns the status of the coroutine passed to it, the possible statuses are "suspended", "running", and "dead".
     *
     * @arg any coroutine - Coroutine to check the status of.
     * @return {string} status
     */
    function status(coroutine: any): string;

    /**
     * Repeatedly yields the coroutine for the given duration before continuing.
     *
     * @arg number duration - The number of seconds to wait
     */
    function wait(duration: number): void;

    /**
     * Returns a function which calling is equivalent with calling coroutine.resume with the coroutine and all extra parameters.
     *
     * @arg GMLua.CallbackNoContext coroutine - Coroutine to resume.
     * @return {Function} func
     */
    function wrap(coroutine: GMLua.CallbackNoContext): Function;

    /**
     * Pauses the active coroutine and passes all additional variables to the call of coroutine.resume that resumed the coroutine last time, and returns all additional variables that were passed to the previous call of resume.
     *
     * @arg args[] returnValue - Arguments to be returned by the last call of coroutine.resume.
     * @return {args[]} Arguments that were set previously by coroutine.resume.
     */
    function yield(...returnValue: any[]): any;
}
