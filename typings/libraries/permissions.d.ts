/** @noSelfInFile */
declare namespace permissions {
    /**
     * Requests the player to connect to a specified server. The player will be prompted with a confirmation window.
     *
     * @arg string address - The address to ask to connect to. If a port is not given, the default :27015 port will be added.
     */
    function AskToConnect(address: string): void;

    /**
     * Connects player to the server. This is what permissions.AskToConnect uses internally.
     *
     * @arg string ip - IP address to connect.
     */
    function Connect(ip: string): void;

    /**
     * Activates player's microphone as if he pressed the speak button himself. The player will be prompted with a confirmation window which grants permission temporarily/permanently(depending on checkbox state) for the connected server (revokable).
     * This is used for TTT's traitor voice channel.
     *
     * @arg boolean enable - Enable or disable voice activity. true will run +voicerecord command, anything else -voicerecord.
     */
    function EnableVoiceChat(enable: boolean): void;

    /**
 * Returns all permissions per server. Permanent permissions are stored in settings/permissions.bin.
* 
* @return {object} A table of permanent and temporary permissions granted for servers.
Example structure:
permanent = {
	["123.123.123.123"] = "connect" -- this server has a permission to connect player to any server even after restarting the game
},
temporary = {
	["111.111.111.111"] = "voicerecord" -- this server can enable voice activity on player during this game session
}
 */
    function GetAll(): object;

    /**
     * Grants permission to the current connected server.
     *
     * @arg string permission - Permission to grant for the server the player is currently connected.
     * @arg boolean temporary - true if the permission should be granted temporary.
     */
    function Grant(permission: string, temporary: boolean): void;

    /**
 * Returns whether the player has granted the current server a specific permission.
* 
* @arg string permission - The permission to poll. Currently only 2 permission is valid:

"connect"
"voicerecord"
* @return {boolean} Whether the permission is granted or not.
 */
    function IsGranted(permission: string): boolean;

    /**
     * Revokes permission from the server.
     *
     * @arg string permission - Permission to revoke from the server.
     * @arg string ip - IP of the server.
     */
    function Revoke(permission: string, ip: string): void;
}
