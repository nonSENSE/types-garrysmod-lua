/** @noSelfInFile */
declare namespace matproxy {
    /**
     * Adds a material proxy.
     *
     * @arg MatProxyDataStruct MatProxyData - The information about the proxy. See Structures/MatProxyData
     */
    function Add(MatProxyData: MatProxyDataStruct): void;

    /**
     * Called by the engine from OnBind
     *
     * @arg string uname
     * @arg IMaterial mat
     * @arg Entity ent
     */
    function Call(uname: string, mat: IMaterial, ent: Entity): void;

    /**
     * Called by the engine from OnBind
     *
     * @arg string name
     * @arg string uname
     * @arg IMaterial mat
     * @arg object values
     */
    function Init(name: string, uname: string, mat: IMaterial, values: object): void;

    /**
     * Called by engine, returns true if we're overriding a proxy
     *
     * @arg string name - The name of proxy in question
     * @return {boolean} Are we overriding it?
     */
    function ShouldOverrideProxy(name: string): boolean;
}
