/** @noSelfInFile */
declare namespace killicon {
    /**
     * Creates new kill icon using a texture.
     *
     * @arg string _class - Weapon or entity class
     * @arg string texture - Path to the texture
     * @arg object color - Color of the kill icon
     */
    function Add(_class: string, texture: string, color: object): void;

    /**
     * Creates kill icon from existing one.
     *
     * @arg string new_class - New class of the kill icon
     * @arg string existing_class - Already existing kill icon class
     */
    function AddAlias(new_class: string, existing_class: string): void;

    /**
     * Adds kill icon for given weapon/entity class using special font.
     *
     * @arg string _class - Weapon or entity class
     * @arg string font - Font to be used
     * @arg string symbol - The symbol to be used
     * @arg object color - Color of the killicon
     */
    function AddFont(_class: string, font: string, symbol: string, color: object): void;

    /**
     * Draws a kill icon.
     *
     * @arg number x - X coordinate of the icon
     * @arg number y - Y coordinate of the icon
     * @arg string name - Classname of the kill icon
     * @arg number alpha - Alpha/transparency value ( 0 - 255 ) of the icon
     */
    function Draw(x: number, y: number, name: string, alpha: number): void;

    /**
     * Checks if kill icon exists for given class.
     *
     * @arg string _class - The class to test
     * @return {boolean} Returns true if kill icon exists
     */
    function Exists(_class: string): boolean;

    /**
     * Returns the size of a kill icon.
     *
     * @arg string name - Classname of the kill icon
     * @return {number} Width of the kill icon
     * @return {number} Height of the kill icon
     */
    function GetSize(name: string): LuaMultiReturn<[number, number]>;
}
