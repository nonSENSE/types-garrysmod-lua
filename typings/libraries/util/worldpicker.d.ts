/** @noSelfInFile */
declare namespace util {
    export namespace worldpicker {
        /**
         * Returns if the user is currently picking an entity.
         *
         * @return {boolean} Is world picking
         */
        function Active(): boolean;

        /**
         * Finishes the world picking. This is called when a user presses their mouse after calling util.worldpicker.Start.
         *
         * @arg TraceResultStruct tr - Structures/TraceResult from the mouse press
         */
        function Finish(tr: TraceResultStruct): void;

        /**
 * Starts picking an entity in the world. This will suppress the next mouse click, and instead use it as a direction in the trace sent to the callback.
* 
* @arg GMLua.CallbackNoContext callback - Function to call after an entity choice has been made. Argument is:
table tr - TraceResult from the mouse press. tr.Entity will return the entity clicked
 */
        function Start(callback: GMLua.CallbackNoContext): void;
    }
}
