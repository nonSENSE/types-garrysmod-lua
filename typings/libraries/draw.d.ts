/** @noSelfInFile */
declare namespace draw {
    /**
     * Simple draw text at position, but this will expand newlines and tabs.
     *
     * @arg string text - Text to be drawn.
     * @arg string [font="DermaDefault"] - Name of font to draw the text in. See surface.CreateFont to create your own, or Default Fonts for a list of default fonts.
     * @arg number [x=0] - The X Coordinate.
     * @arg number [y=0] - The Y Coordinate.
     * @arg Color [color=Color( 255, 255, 255, 255 )] - Color to draw the text in. Uses the Color.
     * @arg number [xAlign=NaN] - Where to align the text horizontally. Uses the Enums/TEXT_ALIGN.
     */
    function DrawText(text: string, font?: string, x?: number, y?: number, color?: Color, xAlign?: number): void;

    /**
     * Returns the height of the specified font in pixels. This is equivalent to the height of the character W. See surface.GetTextSize.
     *
     * @arg string font - Name of the font to get the height of.
     * @return {number} The font height
     */
    function GetFontHeight(font: string): number;

    /**
     * Sets drawing texture to a default white texture (vgui/white) via surface.SetMaterial. Useful for resetting the drawing texture.
     */
    function NoTexture(): void;

    /**
     * Draws a rounded rectangle.
     *
     * @arg number cornerRadius - Radius of the rounded corners, works best with a multiple of 2.
     * @arg number x - The x coordinate of the top left of the rectangle.
     * @arg number y - The y coordinate of the top left of the rectangle.
     * @arg number width - The width of the rectangle.
     * @arg number height - The height of the rectangle.
     * @arg Color color - The color to fill the rectangle with. Uses the Color.
     */
    function RoundedBox(cornerRadius: number, x: number, y: number, width: number, height: number, color: Color): void;

    /**
     * Draws a rounded rectangle. This function also lets you specify which corners are drawn rounded.
     *
     * @arg number cornerRadius - Radius of the rounded corners, works best with a power of 2 number.
     * @arg number x - The x coordinate of the top left of the rectangle.
     * @arg number y - The y coordinate of the top left of the rectangle.
     * @arg number width - The width of the rectangle.
     * @arg number height - The height of the rectangle.
     * @arg Color color - The color to fill the rectangle with. Uses the Color.
     * @arg boolean [roundTopLeft=false] - Whether the top left corner should be rounded.
     * @arg boolean [roundTopRight=false] - Whether the top right corner should be rounded.
     * @arg boolean [roundBottomLeft=false] - Whether the bottom left corner should be rounded.
     * @arg boolean [roundBottomRight=false] - Whether the bottom right corner should be rounded.
     */
    function RoundedBoxEx(
        cornerRadius: number,
        x: number,
        y: number,
        width: number,
        height: number,
        color: Color,
        roundTopLeft?: boolean,
        roundTopRight?: boolean,
        roundBottomLeft?: boolean,
        roundBottomRight?: boolean
    ): void;

    /**
     * Draws text on the screen.
     *
     * @arg string text - The text to be drawn.
     * @arg string [font="DermaDefault"] - The font. See surface.CreateFont to create your own, or see Default Fonts for a list of default fonts.
     * @arg number [x=0] - The X Coordinate.
     * @arg number [y=0] - The Y Coordinate.
     * @arg Color [color=Color( 255, 255, 255, 255 )] - The color of the text. Uses the Color.
     * @arg number [xAlign=NaN] - The alignment of the X coordinate using Enums/TEXT_ALIGN.
     * @arg number [yAlign=NaN] - The alignment of the Y coordinate using Enums/TEXT_ALIGN.
     * @return {number} The width of the text. Same value as if you were calling surface.GetTextSize.
     * @return {number} The height of the text. Same value as if you were calling surface.GetTextSize.
     */
    function SimpleText(
        text: string,
        font?: string,
        x?: number,
        y?: number,
        color?: Color,
        xAlign?: number,
        yAlign?: number
    ): LuaMultiReturn<[number, number]>;

    /**
     * Creates a simple line of text that is outlined.
     *
     * @arg string Text - The text to draw.
     * @arg string [font="DermaDefault"] - The font name to draw with. See surface.CreateFont to create your own, or here for a list of default fonts.
     * @arg number [x=0] - The X Coordinate.
     * @arg number [y=0] - The Y Coordinate.
     * @arg Color [color=Color( 255, 255, 255, 255 )] - The color of the text. Uses the Color.
     * @arg number [xAlign=NaN] - The alignment of the X Coordinate using Enums/TEXT_ALIGN.
     * @arg number [yAlign=NaN] - The alignment of the Y Coordinate using Enums/TEXT_ALIGN.
     * @arg number outlinewidth - Width of the outline.
     * @arg Color [outlinecolor=Color( 255, 255, 255, 255 )] - Color of the outline. Uses the Color.
     * @return {number} The width of the text. Same value as if you were calling surface.GetTextSize.
     * @return {number} The height of the text. Same value as if you were calling surface.GetTextSize.
     */
    function SimpleTextOutlined(
        Text: string,
        font?: string,
        x?: number,
        y?: number,
        color?: Color,
        xAlign?: number,
        yAlign?: number,
        outlinewidth?: number,
        outlinecolor?: Color
    ): LuaMultiReturn<[number, number]>;

    /**
     * Works like draw.SimpleText but uses a table structure instead.
     *
     * @arg TextDataStruct textdata - The text properties. See the Structures/TextData
     * @return {number} Width of drawn text
     * @return {number} Height of drawn text
     */
    function Text(textdata: TextDataStruct): LuaMultiReturn<[number, number]>;

    /**
     * Works like draw.Text, but draws the text as a shadow.
     *
     * @arg TextDataStruct textdata - The text properties. See Structures/TextData
     * @arg number distance - How far away the shadow appears.
     * @arg number [alpha=200] - How visible the shadow is (0-255).
     */
    function TextShadow(textdata: TextDataStruct, distance: number, alpha?: number): void;

    /**
     * Draws a texture with a table structure.
     *
     * @arg TextureDataStruct texturedata - The texture properties. See Structures/TextureData.
     */
    function TexturedQuad(texturedata: TextureDataStruct): void;

    /**
     * Draws a rounded box with text in it.
     *
     * @arg number bordersize - Size of border, should be multiple of 2. Ideally this will be 8 or 16.
     * @arg number x - The X Coordinate.
     * @arg number y - The Y Coordinate.
     * @arg string text - Text to draw.
     * @arg string font - Font to draw in. See surface.CreateFont to create your own, or here for a list of default fonts.
     * @arg Color boxcolor - The box color. Uses the Color.
     * @arg Color textcolor - The text color. Uses the Color.
     * @arg number [xalign=NaN] - The alignment of the X coordinate using Enums/TEXT_ALIGN.
     * @arg number [yalign=NaN] - The alignment of the Y coordinate using Enums/TEXT_ALIGN.
     * @return {number} The width of the word box.
     * @return {number} The height of the word box.
     */
    function WordBox(
        bordersize: number,
        x: number,
        y: number,
        text: string,
        font: string,
        boxcolor: Color,
        textcolor: Color,
        xalign?: number,
        yalign?: number
    ): LuaMultiReturn<[number, number]>;
}
