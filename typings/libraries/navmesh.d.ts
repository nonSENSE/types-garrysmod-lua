/** @noSelfInFile */
declare namespace navmesh {
    /**
     * Add this position and normal to the list of walkable positions, used before map generation with navmesh.BeginGeneration
     *
     * @arg Vector pos - The terrain position.
     * @arg Vector dir - The normal of this terrain position.
     */
    function AddWalkableSeed(pos: Vector, dir: Vector): void;

    /**
     * Starts the generation of a new navmesh.
     */
    function BeginGeneration(): void;

    /**
     * Clears all the walkable positions, used before calling navmesh.BeginGeneration.
     */
    function ClearWalkableSeeds(): void;

    /**
     * Creates a new CNavArea.
     *
     * @arg Vector corner - The first corner of the new CNavArea
     * @arg Vector opposite_corner - The opposite (diagonally) corner of the new CNavArea
     * @return {CNavArea} The new CNavArea or nil if we failed for some reason.
     */
    function CreateNavArea(corner: Vector, opposite_corner: Vector): CNavArea;

    /**
     * Returns a bunch of areas within distance, used to find hiding spots by NextBots for example.
     *
     * @arg Vector pos - The position to search around
     * @arg number radius - Radius to search within
     * @arg number stepdown - Maximum stepdown( fall distance ) allowed
     * @arg number stepup - Maximum stepup( jump height ) allowed
     * @return {CNavArea} A table of CNavAreas
     */
    function Find(pos: Vector, radius: number, stepdown: number, stepup: number): CNavArea;

    /**
     * Returns an integer indexed table of all CNavAreas on the current map. If the map doesn't have a navmesh generated then this will return an empty table.
     *
     * @return {CNavArea} A table of all the CNavAreas on the current map.
     */
    function GetAllNavAreas(): CNavArea;

    /**
     * Returns the position of the edit cursor when nav_edit is set to 1.
     *
     * @return {Vector} The position of the edit cursor.
     */
    function GetEditCursorPosition(): Vector;

    /**
     * Finds the closest standable ground at, above, or below the provided position.
     *
     * @arg Vector pos - Position to find the closest ground for.
     * @return {number} The height of the ground layer.
     * @return {Vector} The normal of the ground layer.
     */
    function GetGroundHeight(pos: Vector): LuaMultiReturn<[number, Vector]>;

    /**
     * Returns the currently marked CNavArea, for use with editing console commands.
     *
     * @return {CNavArea} The currently marked CNavArea.
     */
    function GetMarkedArea(): CNavArea;

    /**
     * Returns the currently marked CNavLadder, for use with editing console commands.
     *
     * @return {CNavLadder} The currently marked CNavLadder.
     */
    function GetMarkedLadder(): CNavLadder;

    /**
     * Returns the Nav Area contained in this position that also satisfies the elevation limit.
     *
     * @arg Vector pos - The position to search for.
     * @arg number beneathLimit - The elevation limit at which the Nav Area will be searched.
     * @return {CNavArea} The nav area.
     */
    function GetNavArea(pos: Vector, beneathLimit: number): CNavArea;

    /**
     * Returns a CNavArea by the given ID.
     *
     * @arg number id - ID of the CNavArea to get. Starts with 1.
     * @return {CNavArea} The CNavArea with given ID.
     */
    function GetNavAreaByID(id: number): CNavArea;

    /**
     * Returns the highest ID of all nav areas on the map. While this can be used to get all nav areas, this number may not actually be the actual number of nav areas on the map.
     *
     * @return {number} The highest ID of all nav areas on the map.
     */
    function GetNavAreaCount(): number;

    /**
     * Returns a CNavLadder by the given ID.
     *
     * @arg number id - ID of the CNavLadder to get. Starts with 1.
     * @return {CNavLadder} The CNavLadder with given ID.
     */
    function GetNavLadderByID(id: number): CNavLadder;

    /**
 * Returns the closest CNavArea to given position at the same height, or beneath it.
* 
* @arg Vector pos - The position to look from
* @arg boolean [anyZ=false] - This argument is ignored and has no effect
* @arg number [maxDist=10000] - This is the maximum distance from the given position that the function will look for a CNavArea
* @arg boolean [checkLOS=false] - If this is set to true then the function will internally do a util.TraceLine from the starting position to each potential CNavArea with a MASK_NPCSOLID_BRUSHONLY. If the trace fails then the CNavArea is ignored.
If this is set to false then the function will find the closest CNavArea through anything, including the world.
* @arg boolean [checkGround=true] - If checkGround is true then this function will internally call navmesh.GetNavArea to check if there is a CNavArea directly below the position, and return it if so, before checking anywhere else.
* @arg number [team=NaN] - This will internally call CNavArea:IsBlocked to check if the target CNavArea is not to be navigated by the given team. Currently this appears to do nothing.
* @return {CNavArea} The closest CNavArea found with the given parameters, or a NULL CNavArea if one was not found.
 */
    function GetNearestNavArea(
        pos: Vector,
        anyZ?: boolean,
        maxDist?: number,
        checkLOS?: boolean,
        checkGround?: boolean,
        team?: number
    ): CNavArea;

    /**
     * Returns the classname of the player spawn entity.
     *
     * @return {string} The classname of the spawn point entity. By default returns "info_player_start"
     */
    function GetPlayerSpawnName(): string;

    /**
     * Whether we're currently generating a new navmesh with navmesh.BeginGeneration.
     *
     * @return {boolean} Whether we're generating a nav mesh or not.
     */
    function IsGenerating(): boolean;

    /**
     * Returns true if a navmesh has been loaded when loading the map.
     *
     * @return {boolean} Whether a navmesh has been loaded when loading the map.
     */
    function IsLoaded(): boolean;

    /**
     * Loads a new navmesh from the .nav file for current map discarding any changes made to the navmesh previously.
     */
    function Load(): void;

    /**
     * Deletes every CNavArea and CNavLadder on the map without saving the changes.
     */
    function Reset(): void;

    /**
     * Saves any changes made to navmesh to the .nav file.
     */
    function Save(): void;

    /**
     * Sets the CNavArea as marked, so it can be used with editing console commands.
     *
     * @arg CNavArea area - The CNavArea to set as the marked area.
     */
    function SetMarkedArea(area: CNavArea): void;

    /**
     * Sets the CNavLadder as marked, so it can be used with editing console commands.
     *
     * @arg CNavLadder area - The CNavLadder to set as the marked ladder.
     */
    function SetMarkedLadder(area: CNavLadder): void;

    /**
     * Sets the classname of the default spawn point entity, used before generating a new navmesh with navmesh.BeginGeneration.
     *
     * @arg string spawnPointClass - The classname of what the player uses to spawn, automatically adds it to the walkable positions during map generation.
     */
    function SetPlayerSpawnName(spawnPointClass: string): void;
}
