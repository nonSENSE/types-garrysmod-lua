/** @noSelfInFile */
declare namespace list {
    /**
     * Adds an item to a named list
     *
     * @arg string identifier - The list identifier
     * @arg any item - The item to add to the list
     * @return {number} The index at which the item was added.
     */
    function Add(identifier: string, item: any): number;

    /**
     * Returns true if the list contains the value. (as a value - not a key)
     *
     * @arg string list - List to search through
     * @arg any value - The value to test
     * @return {boolean} Returns true if the list contains the value, false otherwise
     */
    function Contains(list: string, value: any): boolean;

    /**
     * Returns a copy of the list stored at identifier
     *
     * @arg string identifier - The list identifier
     * @return {object} The copy of the list
     */
    function Get(identifier: string): object;

    /**
     * Returns the actual table of the list stored at identifier. Modifying this will affect the stored list
     *
     * @arg string identifier - The list identifier
     * @return {object} The actual list
     */
    function GetForEdit(identifier: string): object;

    /**
     * Returns a a list of all lists currently in use.
     *
     * @return {object} The list of all lists, i.e. a table containing names of all lists.
     */
    function GetTable(): object;

    /**
     * Returns true if the list contains the given key.
     *
     * @arg string list - List to search through
     * @arg any key - The key to test
     * @return {boolean} Returns true if the list contains the key, false otherwise
     */
    function HasEntry(list: string, key: any): boolean;

    /**
     * Sets a specific position in the named list to a value.
     *
     * @arg string identifier - The list identifier
     * @arg any key - The key in the list to set
     * @arg any item - The item to set to the list as key
     */
    function Set(identifier: string, key: any, item: any): void;
}
