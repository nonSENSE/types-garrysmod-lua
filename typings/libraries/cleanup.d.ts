/** @noSelfInFile */
declare namespace cleanup {
    /**
     * Adds an entity to a player's cleanup list.
     *
     * @arg Player pl - Who's cleanup list to add the entity to.
     * @arg string type - The type of cleanup.
     * @arg Entity ent - The entity to add to the player's cleanup list.
     */
    function Add(pl: Player, type: string, ent: Entity): void;

    /**
     * Called by the gmod_admin_cleanup console command. Allows admins to clean up the server.
     *
     * @arg Player pl - The player that called the console command.
     * @arg string command - The console command that called this function.
     * @arg object args - First and only arg is the cleanup type.
     */
    function CC_AdminCleanup(pl: Player, command: string, args: object): void;

    /**
     * Called by the gmod_cleanup console command. Allows players to cleanup their own props.
     *
     * @arg Player pl - The player that called the console command.
     * @arg string command - The console command that called this function.
     * @arg object args - First and only argument is the cleanup type.
     */
    function CC_Cleanup(pl: Player, command: string, args: object): void;

    /**
     * Gets the cleanup list.
     */
    function GetList(): void;

    /**
     * Gets the table of cleanup types.
     *
     * @return {object} A list of cleanup types.
     */
    function GetTable(): object;

    /**
     * Registers a new cleanup type.
     *
     * @arg string type - Name of type.
     */
    function Register(type: string): void;

    /**
     * Replaces one entity in the cleanup module with another
     *
     * @arg Entity from - Old entity
     * @arg Entity to - New entity
     * @return {boolean} Whether any action was taken.
     */
    function ReplaceEntity(from: Entity, to: Entity): boolean;

    /**
     * Repopulates the clients cleanup menu
     */
    function UpdateUI(): void;
}
