/** @noSelfInFile */
declare namespace achievements {
    /**
     * Adds one to the count of balloons burst. Once this count reaches 1000, the 'Popper' achievement is unlocked.
     */
    function BalloonPopped(): void;

    /**
     * Returns the amount of achievements in Garry's Mod.
     *
     * @return {number} The amount of achievements available.
     */
    function Count(): number;

    /**
     * Adds one to the count of balls eaten. Once this count reaches 200, the 'Ball Eater' achievement is unlocked.
     */
    function EatBall(): void;

    /**
     * Retrieves progress of given achievement
     *
     * @arg number achievementID - The ID of achievement to retrieve progress of. Note: IDs start from 0, not 1.
     */
    function GetCount(achievementID: number): void;

    /**
     * Retrieves description of given achievement
     *
     * @arg number achievementID - The ID of achievement to retrieve description of. Note: IDs start from 0, not 1.
     * @return {string} Description of an achievement
     */
    function GetDesc(achievementID: number): string;

    /**
     * Retrieves progress goal of given achievement
     *
     * @arg number achievementID - The ID of achievement to retrieve goal of. Note: IDs start from 0, not 1.
     * @return {number} Progress goal of an achievement
     */
    function GetGoal(achievementID: number): number;

    /**
     * Retrieves name of given achievement
     *
     * @arg number achievementID - The ID of achievement to retrieve name of. Note: IDs start from 0, not 1.
     * @return {string} Name of an achievement
     */
    function GetName(achievementID: number): string;

    /**
     * Adds one to the count of baddies killed. Once this count reaches 1000, the 'War Zone' achievement is unlocked.
     */
    function IncBaddies(): void;

    /**
     * Adds one to the count of innocent animals killed. Once this count reaches 1000, the 'Innocent Bystander' achievement is unlocked.
     */
    function IncBystander(): void;

    /**
     * Adds one to the count of friendly NPCs killed. Once this count reaches 1000, the 'Bad Friend' achievement is unlocked.
     */
    function IncGoodies(): void;

    /**
     * Used in GMod 12 in the achievements menu to show the user if they have unlocked certain achievements.
     *
     * @arg number AchievementID - Internal Achievement ID number
     * @return {boolean} Returns true if the given achievementID is achieved.
     */
    function IsAchieved(AchievementID: number): boolean;

    /**
     * Adds one to the count of things removed. Once this count reaches 5000, the 'Destroyer' achievement is unlocked.
     */
    function Remover(): void;

    /**
     * Adds one to the count of NPCs spawned. Once this count reaches 1000, the 'Procreator' achievement is unlocked.
     */
    function SpawnedNPC(): void;

    /**
     * Adds one to the count of props spawned. Once this count reaches 5000, the 'Creator' achievement is unlocked.
     */
    function SpawnedProp(): void;

    /**
     * Adds one to the count of ragdolls spawned. Once this count reaches 2000, the 'Dollhouse' achievement is unlocked.
     */
    function SpawnedRagdoll(): void;

    /**
     * Adds one to the count of how many times the spawnmenu has been opened. Once this count reaches 100,000, the 'Menu User' achievement is unlocked.
     */
    function SpawnMenuOpen(): void;
}
