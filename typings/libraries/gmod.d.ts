/** @noSelfInFile */
declare namespace gmod {
    /**
     * Returns GAMEMODE.
     *
     * @return {object} GAMEMODE
     */
    function GetGamemode(): object;
}
