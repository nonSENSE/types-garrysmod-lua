/** @noSelfInFile */
declare namespace markup {
    /**
     * A convenience function that converts a Color into its markup ready string representation.
     *
     * @arg Color clr - The Color to convert.
     * @return {string} The markup color, for example 255,255,255.
     */
    function Color(clr: Color): string;

    /**
     * Converts a string to its escaped, markup-safe equivalent.
     *
     * @arg string text - The string to sanitize.
     * @return {string} The parsed markup object ready to be drawn.
     */
    function Escape(text: string): string;

    /**
     * Parses markup into a MarkupObject. Currently, this only supports fonts and colors as demonstrated in the example.
     *
     * @arg string markup - The markup to be parsed.
     * @arg number [maxWidth=NaN] - The max width of the output
     * @return {MarkupObject} The parsed markup object ready to be drawn.
     */
    function Parse(markup: string, maxWidth?: number): MarkupObject;
}
