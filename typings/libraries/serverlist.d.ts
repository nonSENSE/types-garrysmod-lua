/** @noSelfInFile */
declare namespace serverlist {
    /**
     * Adds current server the player is on to their favorites.
     */
    function AddCurrentServerToFavorites(): void;

    /**
     * Queries a server for its information/ping.
     *
     * @arg string ip - The IP address of the server, including the port.
     * @arg GMLua.CallbackNoContext callback - The function to be called if and when the request finishes. Function has the same arguments as the callback of serverlist.Query.
     */
    function PingServer(ip: string, callback: GMLua.CallbackNoContext): void;

    /**
 * Queries a server for its player list.
* 
* @arg string ip - The IP address of the server, including the port.
* @arg GMLua.CallbackNoContext callback - The function to be called if and when the request finishes. Function has one argument, a table containing tables with player info.
Each table with player info has next fields:
number time - The amount of time the player is playing on the server, in seconds
string name - The player name
number score - The players score
 */
    function PlayerList(ip: string, callback: GMLua.CallbackNoContext): void;

    /**
     * Queries the master server for server list.
     *
     * @arg ServerQueryDataStruct data - The information about what kind of servers we want. See Structures/ServerQueryData.
     */
    function Query(data: ServerQueryDataStruct): void;
}
