/** @noSelfInFile */
declare namespace constraint {
    /**
     * Stores information about constraints in an entity's table.
     *
     * @arg Entity ent1 - The entity to store the information on.
     * @arg Entity constrt - The constraint to store in the entity's table.
     * @arg Entity [ent2=nil] - Optional. If different from ent1, the info will also be stored in the table for this entity.
     * @arg Entity [ent3=nil] - Optional. Same as ent2.
     * @arg Entity [ent4=nil] - Optional. Same as ent2.
     */
    function AddConstraintTable(ent1: Entity, constrt: Entity, ent2?: Entity, ent3?: Entity, ent4?: Entity): void;

    /**
     * Stores info about the constraints on the entity's table. The only difference between this and constraint.AddConstraintTable is that the constraint does not get deleted when the entity is removed.
     *
     * @arg Entity ent1 - The entity to store the information on.
     * @arg Entity constrt - The constraint to store in the entity's table.
     * @arg Entity [ent2=nil] - Optional. If different from ent1, the info will also be stored in the table for this entity.
     * @arg Entity [ent3=nil] - Optional. Same as ent2.
     * @arg Entity [ent4=nil] - Optional. Same as ent2.
     */
    function AddConstraintTableNoDelete(
        ent1: Entity,
        constrt: Entity,
        ent2?: Entity,
        ent3?: Entity,
        ent4?: Entity
    ): void;

    /**
     * Creates an advanced ballsocket (ragdoll) constraint.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1 - Position on the first entity, in its local space coordinates.
     * @arg Vector LPos2 - Position on the second entity, in its local space coordinates.
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable)
     * @arg number torquelimit - Amount of torque (rotation speed) until it breaks (0 = unbreakable)
     * @arg number xmin - Minimum angle in rotations around the X axis local to the constraint.
     * @arg number ymin - Minimum angle in rotations around the Y axis local to the constraint.
     * @arg number zmin - Minimum angle in rotations around the Z axis local to the constraint.
     * @arg number xmax - Maximum angle in rotations around the X axis local to the constraint.
     * @arg number ymax - Maximum angle in rotations around the Y axis local to the constraint.
     * @arg number zmax - Maximum angle in rotations around the Z axis local to the constraint.
     * @arg number xfric - Rotational friction in the X axis local to the constraint.
     * @arg number yfric - Rotational friction in the Y axis local to the constraint.
     * @arg number zfric - Rotational friction in the Z axis local to the constraint.
     * @arg number onlyrotation - Only limit rotation, free movement.
     * @arg number nocollide - Whether the entities should be no-collided.
     * @return {Entity} A phys_ragdollconstraint entity. Will return false if the constraint could not be created.
     */
    function AdvBallsocket(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        forcelimit: number,
        torquelimit: number,
        xmin: number,
        ymin: number,
        zmin: number,
        xmax: number,
        ymax: number,
        zmax: number,
        xfric: number,
        yfric: number,
        zfric: number,
        onlyrotation: number,
        nocollide: number
    ): Entity;

    /**
     * Creates an axis constraint.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1 - Position on the first entity, in its local space coordinates.
     * @arg Vector LPos2 - Position on the second entity, in its local space coordinates.
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable)
     * @arg number torquelimit - Amount of torque (rotational force) until it breaks (0 = unbreakable)
     * @arg number friction - Constraint friction.
     * @arg number nocollide - Whether the entities should be no-collided.
     * @arg Vector LocalAxis - If you include the LocalAxis then LPos2 will not be used in the final constraint. However, LPos2 is still a required argument.
     * @arg boolean DontAddTable - Whether or not to add the constraint info on the entity table. See constraint.AddConstraintTable.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     */
    function Axis(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        forcelimit: number,
        torquelimit: number,
        friction: number,
        nocollide: number,
        LocalAxis: Vector,
        DontAddTable: boolean
    ): Entity;

    /**
     * Creates a ballsocket joint.
     *
     * @arg Entity Ent1 - First entity
     * @arg Entity Ent2 - Second entity
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LocalPos - Centerposition of the joint, relative to the second entity.
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable)
     * @arg number torquelimit - Amount of torque (rotation speed) until it breaks (0 = unbreakable)
     * @arg number nocollide - Whether the entities should be nocollided
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     */
    function Ballsocket(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LocalPos: Vector,
        forcelimit: number,
        torquelimit: number,
        nocollide: number
    ): Entity;

    /**
     * Basic checks to make sure that the specified entity and bone are valid. Returns false if we should not be constraining the entity.
     *
     * @arg Entity ent - The entity to check
     * @arg number bone - The bone of the entity to check (use 0 for mono boned ents)
     * @return {boolean} shouldConstrain
     */
    function CanConstrain(ent: Entity, bone: number): boolean;

    /**
     * Creates a rope without any constraint.
     *
     * @arg Vector pos - Starting position of the rope.
     * @arg number width - Width of the rope.
     * @arg string material - Material of the rope.
     * @arg Entity Constraint - Constraint for the rope.
     * @arg Entity Ent1 - First entity.
     * @arg Vector LPos1 - Position of first end of the rope. Local to Ent1.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg Entity Ent2 - Second entity.
     * @arg Vector LPos2 - Position of second end of the rope. Local to Ent2.
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg object kv - (Optional) Any additional key/values to be set on the rope.
     * @return {Entity} rope
     */
    function CreateKeyframeRope(
        pos: Vector,
        width: number,
        material: string,
        Constraint: Entity,
        Ent1: Entity,
        LPos1: Vector,
        Bone1: number,
        Ent2: Entity,
        LPos2: Vector,
        Bone2: number,
        kv: object
    ): Entity;

    /**
     * Creates an invisible, non-moveable anchor point in the world to which things can be attached.
     *
     * @arg Vector pos - The position to spawn the anchor at
     * @return {Entity} anchor
     * @return {PhysObj} physicsObject,
     * @return {number} bone
     * @return {Vector} LPos
     */
    function CreateStaticAnchorPoint(pos: Vector): LuaMultiReturn<[Entity, PhysObj, number, Vector]>;

    /**
     * Creates an elastic constraint.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1 - Position of first end of the rope. Local to Ent1.
     * @arg Vector LPos2 - Position of second end of the rope. Local to Ent2.
     * @arg number constant
     * @arg number damping
     * @arg number rdamping
     * @arg string material - The material of the rope.
     * @arg number width - Width of rope.
     * @arg boolean stretchonly
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     * @return {Entity} rope.  Will return nil if the constraint could not be created.
     */
    function Elastic(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        constant: number,
        damping: number,
        rdamping: number,
        material: string,
        width: number,
        stretchonly: boolean,
        color: any
    ): LuaMultiReturn<[Entity, Entity]>;

    /**
 * Returns the constraint of a specified type between two entities, if it exists
* 
* @arg Entity ent1 - The first entity to check
* @arg Entity ent2 - The second entity to check
* @arg string type - The type of constraint, case sensitive. List of default constrains is as follows:

Weld
Axis
AdvBallsocket
Rope
Elastic
NoCollide
Motor
Pulley
Ballsocket
Winch
Hydraulic
Muscle
Keepupright
Slider
* @arg number bone1 - The bone number for the first entity (0 for monoboned entities)
* @arg number bone2 - The bone number for the second entity
* @return {Entity} constraint
 */
    function Find(ent1: Entity, ent2: Entity, type: string, bone1: number, bone2: number): Entity;

    /**
 * Returns the first constraint of a specific type directly connected to the entity found
* 
* @arg Entity ent - The entity to check
* @arg string type - The type of constraint, case sensitive. List of default constrains is as follows:

Weld
Axis
AdvBallsocket
Rope
Elastic
NoCollide
Motor
Pulley
Ballsocket
Winch
Hydraulic
Muscle
Keepupright
Slider
* @return {any} The constraint table, set with constraint.AddConstraintTable
 */
    function FindConstraint(ent: Entity, type: string): any;

    /**
 * Returns the other entity involved in the first constraint of a specific type directly connected to the entity
* 
* @arg Entity ent - The entity to check
* @arg string type - The type of constraint, case sensitive. List of default constrains is as follows:

Weld
Axis
AdvBallsocket
Rope
Elastic
NoCollide
Motor
Pulley
Ballsocket
Winch
Hydraulic
Muscle
Keepupright
Slider
* @return {Entity} The other entity.
 */
    function FindConstraintEntity(ent: Entity, type: string): Entity;

    /**
 * Returns a table of all constraints of a specific type directly connected to the entity
* 
* @arg Entity ent - The entity to check
* @arg string type - The type of constraint, case sensitive. List of default constrains is as follows:

Weld
Axis
AdvBallsocket
Rope
Elastic
NoCollide
Motor
Pulley
Ballsocket
Winch
Hydraulic
Muscle
Keepupright
Slider
* @return {object} All the constraints of this entity.
 */
    function FindConstraints(ent: Entity, type: string): object;

    /**
     * Make this entity forget any constraints it knows about. Note that this will not actually remove the constraints.
     *
     * @arg Entity ent - The entity that will forget its constraints.
     */
    function ForgetConstraints(ent: Entity): void;

    /**
     * Returns a table of all entities recursively constrained to an entitiy.
     *
     * @arg Entity ent - The entity to check
     * @arg object [ResultTable=nil] - Table used to return result. Optional.
     * @return {object} A table containing all of the constrained entities. This includes all entities constrained to entities constrained to the supplied entity, etc.
     */
    function GetAllConstrainedEntities(ent: Entity, ResultTable?: object): object;

    /**
     * Returns a table of all constraints directly connected to the entity
     *
     * @arg Entity ent - The entity to check
     * @return {object} A list of all constraints connected to the entity.
     */
    function GetTable(ent: Entity): object;

    /**
     * Returns true if the entity has constraints attached to it
     *
     * @arg Entity ent - The entity to check
     * @return {boolean} Whether the entity has any constraints or not.
     */
    function HasConstraints(ent: Entity): boolean;

    /**
     * Creates a Hydraulic constraint.
     *
     * @arg Player pl - The player that will be used to call numpad.OnDown.
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls),
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls).
     * @arg Vector LPos1
     * @arg Vector LPos2
     * @arg number Length1
     * @arg number Length2
     * @arg number width - The width of the rope.
     * @arg number key - The key binding, corresponding to an Enums/KEY
     * @arg number fixed - Whether the hydraulic is fixed.
     * @arg number speed
     * @arg string material - The material of the rope.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     * @return {Entity} rope. Will return nil if the constraint could not be created.
     * @return {Entity} controller. Can return nil depending on how the constraint was created. Will return nil if the constraint could not be created.
     * @return {Entity} slider. Can return nil depending on how the constraint was created. Will return nil if the constraint could not be created.
     */
    function Hydraulic(
        pl: Player,
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        Length1: number,
        Length2: number,
        width: number,
        key: number,
        fixed: number,
        speed: number,
        material: string,
        color: any
    ): LuaMultiReturn<[Entity, Entity, Entity, Entity]>;

    /**
     * Creates a keep upright constraint.
     *
     * @arg Entity ent - The entity to keep upright
     * @arg Angle ang - The angle defined as "upright"
     * @arg number bone - The bone of the entity to constrain (0 for boneless)
     * @arg number angularLimit - Basically, the strength of the constraint
     * @return {Entity} The created constraint, if any or false if the constraint failed to set
     */
    function Keepupright(ent: Entity, ang: Angle, bone: number, angularLimit: number): Entity;

    /**
     * Creates a motor constraint.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1
     * @arg Vector LPos2
     * @arg number friction
     * @arg number torque
     * @arg number forcetime
     * @arg number nocollide - Whether the entities should be no-collided.
     * @arg number toggle - Whether the constraint is on toggle.
     * @arg Player pl - The player that will be used to call numpad.OnDown and numpad.OnUp.
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable)
     * @arg number numpadkey_fwd - The key binding for "forward", corresponding to an Enums/KEY
     * @arg number numpadkey_bwd - The key binding for "backwards", corresponding to an Enums/KEY
     * @arg number direction
     * @arg Vector LocalAxis
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     * @return {Entity} axis. Will return nil if the constraint could not be created.
     */
    function Motor(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        friction: number,
        torque: number,
        forcetime: number,
        nocollide: number,
        toggle: number,
        pl: Player,
        forcelimit: number,
        numpadkey_fwd: number,
        numpadkey_bwd: number,
        direction: number,
        LocalAxis: Vector
    ): LuaMultiReturn<[Entity, Entity]>;

    /**
     * Creates a muscle constraint.
     *
     * @arg Player pl - The player that will be used to call numpad.OnDown.
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1
     * @arg Vector LPos2
     * @arg number Length1
     * @arg number Length2
     * @arg number width - Width of the rope.
     * @arg number key - The key binding, corresponding to an Enums/KEY
     * @arg number fixed - Whether the constraint is fixed.
     * @arg number period
     * @arg number amplitude
     * @arg boolean starton
     * @arg string material - Material of the rope.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     * @return {Entity} rope. Will return nil if the constraint could not be created.
     * @return {Entity} controller. Will return nil if the constraint could not be created.
     * @return {Entity} slider. Will return nil if the fixed argument is not 1 or if the constraint could not be created.
     */
    function Muscle(
        pl: Player,
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        Length1: number,
        Length2: number,
        width: number,
        key: number,
        fixed: number,
        period: number,
        amplitude: number,
        starton: boolean,
        material: string,
        color: any
    ): LuaMultiReturn<[Entity, Entity, Entity, Entity]>;

    /**
     * Creates an no-collide "constraint". Disables collision between two entities.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls).
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls).
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     */
    function NoCollide(Ent1: Entity, Ent2: Entity, Bone1: number, Bone2: number): Entity;

    /**
     * Creates a pulley constraint.
     *
     * @arg Entity Ent1
     * @arg Entity Ent4
     * @arg number Bone1
     * @arg number Bone4
     * @arg Vector LPos1
     * @arg Vector LPos4
     * @arg Vector WPos2
     * @arg Vector WPos3
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable)
     * @arg boolean rigid - Whether the constraint is rigid.
     * @arg number width - Width of the rope.
     * @arg string material - Material of the rope.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     */
    function Pulley(
        Ent1: Entity,
        Ent4: Entity,
        Bone1: number,
        Bone4: number,
        LPos1: Vector,
        LPos4: Vector,
        WPos2: Vector,
        WPos3: Vector,
        forcelimit: number,
        rigid: boolean,
        width: number,
        material: string,
        color: any
    ): Entity;

    /**
     * Attempts to remove all constraints associated with an entity
     *
     * @arg Entity ent - The entity to remove constraints from
     * @return {boolean} Whether any constraints were removed
     * @return {number} Number of constraints removed
     */
    function RemoveAll(ent: Entity): LuaMultiReturn<[boolean, number]>;

    /**
     * Attempts to remove all constraints of a specified type associated with an entity
     *
     * @arg Entity ent - The entity to check
     * @arg string type - The constraint type to remove (eg. "Weld", "Elastic", "NoCollide")
     * @return {boolean} Whether we removed any constraints or not
     * @return {number} The amount of constraints removed
     */
    function RemoveConstraints(ent: Entity, type: string): LuaMultiReturn<[boolean, number]>;

    /**
     * Creates a rope constraint - with rope!
     *
     * @arg Entity Ent1 - First entity
     * @arg Entity Ent2 - Second entity
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls)
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls)
     * @arg Vector LPos1 - Position of first end of the rope. Local to Ent1.
     * @arg Vector LPos2 - Position of second end of the rope. Local to Ent2.
     * @arg number length - Length of the rope.
     * @arg number addlength - Amount to add to the length of the rope. Works as it does in the Rope tool.
     * @arg number forcelimit - Amount of force until it breaks (0 = unbreakable).
     * @arg number width - Width of the rope.
     * @arg string material - Material of the rope.
     * @arg boolean rigid - Whether the constraint is rigid.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will be a keyframe_rope if you roping to the same bone on the same entity. Will return false if the constraint could not be created.
     * @return {Entity} rope. Will return nil if "Constraint" is a keyframe_rope or if the constraint could not be created.
     */
    function Rope(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        length: number,
        addlength: number,
        forcelimit: number,
        width: number,
        material: string,
        rigid: boolean,
        color: any
    ): LuaMultiReturn<[Entity, Entity]>;

    /**
     * Creates a slider constraint.
     *
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls),
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls).
     * @arg Vector LPos1
     * @arg Vector LPos2
     * @arg number width - The width of the rope.
     * @arg string material - The material of the rope.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Will return false if the constraint could not be created.
     * @return {Entity} rope. Will return nil if the constraint could not be created.
     */
    function Slider(
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        width: number,
        material: string,
        color: any
    ): LuaMultiReturn<[Entity, Entity]>;

    /**
 * Creates a weld constraint
* 
* @arg Entity ent1 - The first entity
* @arg Entity ent2 - The second entity
* @arg number bone1 - The bonenumber of the first entity (0 for monoboned entities)
PhysObj number for ragdolls, see: Entity:TranslateBoneToPhysBone.
* @arg number bone2 - The bonenumber of the second entity
* @arg number forcelimit - The amount of force appliable to the constraint before it will break (0 is never)
* @arg boolean nocollide - Should ent1 be nocollided to ent2 via this constraint
* @arg boolean deleteent1onbreak - If true, when ent2 is removed, ent1 will also be removed
* @return {Entity} constraint
 */
    function Weld(
        ent1: Entity,
        ent2: Entity,
        bone1: number,
        bone2: number,
        forcelimit: number,
        nocollide: boolean,
        deleteent1onbreak: boolean
    ): Entity;

    /**
     * Creates a Winch constraint.
     *
     * @arg Player pl - The player that will be used to call numpad.OnDown and numpad.OnUp.
     * @arg Entity Ent1 - First entity.
     * @arg Entity Ent2 - Second entity.
     * @arg number Bone1 - Bone of first entity (0 for non-ragdolls),
     * @arg number Bone2 - Bone of second entity (0 for non-ragdolls).
     * @arg Vector LPos1
     * @arg Vector LPos2
     * @arg number width - The width of the rope.
     * @arg number fwd_bind - The key binding for "forward", corresponding to an Enums/KEY
     * @arg number bwd_bind - The key binding for "backwards", corresponding to an Enums/KEY
     * @arg number fwd_speed - Forward speed.
     * @arg number bwd_speed - Backwards speed.
     * @arg string material - The material of the rope.
     * @arg boolean toggle - Whether the winch should be on toggle.
     * @arg any color - The color of the rope. See Color.
     * @return {Entity} Constraint. Can return nil. Will return false if the constraint could not be created.
     * @return {Entity} rope. Will return nil if the constraint could not be created.
     * @return {Entity} controller. Can return nil.
     */
    function Winch(
        pl: Player,
        Ent1: Entity,
        Ent2: Entity,
        Bone1: number,
        Bone2: number,
        LPos1: Vector,
        LPos2: Vector,
        width: number,
        fwd_bind: number,
        bwd_bind: number,
        fwd_speed: number,
        bwd_speed: number,
        material: string,
        toggle: boolean,
        color: any
    ): LuaMultiReturn<[Entity, Entity, Entity]>;
}
