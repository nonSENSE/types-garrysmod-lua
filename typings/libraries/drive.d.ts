/** @noSelfInFile */
declare namespace drive {
    /**
     * Optionally alter the view.
     *
     * @arg Player ply - The player
     * @arg ViewDataStruct view - The view, see Structures/ViewData
     * @return {boolean} true if succeeded
     */
    function CalcView(ply: Player, view: ViewDataStruct): boolean;

    /**
     * Clientside, the client creates the cmd (usercommand) from their input device (mouse, keyboard) and then it's sent to the server. Restrict view angles here.
     *
     * @arg CUserCmd cmd - The user command
     * @return {boolean} true if succeeded
     */
    function CreateMove(cmd: CUserCmd): boolean;

    /**
     * Destroys players current driving method.
     *
     * @arg Player ply - The player to affect
     */
    function DestroyMethod(ply: Player): void;

    /**
     * Player has stopped driving the entity.
     *
     * @arg Player ply - The player
     * @arg Entity ent - The entity
     */
    function End(ply: Player, ent: Entity): void;

    /**
     * The move is finished. Copy mv back into the target.
     *
     * @arg Player ply - The player
     * @arg CMoveData mv - The move data
     * @return {boolean} true if succeeded
     */
    function FinishMove(ply: Player, mv: CMoveData): boolean;

    /**
     * Returns ( or creates if inexistent ) a driving method.
     *
     * @arg Player ply - The player
     * @return {object} A method object.
     */
    function GetMethod(ply: Player): object;

    /**
     * The move is executed here.
     *
     * @arg Player ply - The player
     * @arg CMoveData mv - The move data
     * @return {boolean} true if succeeded
     */
    function Move(ply: Player, mv: CMoveData): boolean;

    /**
     * Starts driving for the player.
     *
     * @arg Player ply - The player to affect
     * @arg Entity ent - The entity to drive
     * @arg string mode - The driving mode
     */
    function PlayerStartDriving(ply: Player, ent: Entity, mode: string): void;

    /**
     * Stops the player from driving anything. ( For example a prop in sandbox )
     *
     * @arg Player ply - The player to affect
     */
    function PlayerStopDriving(ply: Player): void;

    /**
     * Registers a new entity drive.
     *
     * @arg string name - The name of the drive.
     * @arg object data - The data required to create the drive. This includes the functions used by the drive.
     * @arg string base - The base of the drive.
     */
    function Register(name: string, data: object, base: string): void;

    /**
     * Called when the player first starts driving this entity
     *
     * @arg Player ply - The player
     * @arg Entity ent - The entity
     */
    function Start(ply: Player, ent: Entity): void;

    /**
     * The user command is received by the server and then converted into a move. This is also run clientside when in multiplayer, for prediction to work.
     *
     * @arg Player ply - The player
     * @arg CMoveData mv - The move data
     * @arg CUserCmd cmd - The user command
     * @return {boolean} true if succeeded
     */
    function StartMove(ply: Player, mv: CMoveData, cmd: CUserCmd): boolean;
}
