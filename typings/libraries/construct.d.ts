/** @noSelfInFile */
declare namespace construct {
    /**
     * Creates a magnet.
     *
     * @arg Player ply - Player that will have the numpad control over the magnet
     * @arg Vector pos - The position of the magnet
     * @arg Angle ang - The angles of the magnet
     * @arg string model - The model of the maget
     * @arg string material - Material of the magnet ( texture )
     * @arg number key - The key to toggle the magnet, see Enums/KEY
     * @arg number maxObjects - Maximum amount of objects the magnet can hold
     * @arg number strength - Strength of the magnet
     * @arg number [nopull=0] - If > 0, disallows the magnet to pull objects towards it
     * @arg number [allowrot=0] - If > 0, allows rotation of the objects attached
     * @arg number [startOn=0] - If > 0, enabled from spawn
     * @arg number toggle - If != 0, pressing the key toggles the magnet, otherwise you'll have to hold the key to keep it enabled
     * @arg Vector [vel=Vector( 0, 0, 0 )] - Velocity to set on spawn
     * @arg Angle [aVel=Angle( 0, 0, 0 )] - Angular velocity to set on spawn
     * @arg boolean [frozen=false] - Freeze the magnet on start
     * @return {Entity} The magnet
     */
    function Magnet(
        ply: Player,
        pos: Vector,
        ang: Angle,
        model: string,
        material: string,
        key: number,
        maxObjects: number,
        strength: number,
        nopull?: number,
        allowrot?: number,
        startOn?: number,
        toggle?: number,
        vel?: Vector,
        aVel?: Angle,
        frozen?: boolean
    ): Entity;

    /**
     * Sets props physical properties.
     *
     * @arg Player ply - The player. This variable is not used and can be left out.
     * @arg Entity ent - The entity to apply properties to
     * @arg number physObjID - You can use this or the argument below. This will be used in case you don't provide argument below.
     * @arg PhysObj physObj - The physics object to apply the properties to
     * @arg PhysPropertiesStruct data - The table containing properties to apply. See Structures/PhysProperties
     */
    function SetPhysProp(
        ply: Player,
        ent: Entity,
        physObjID: number,
        physObj: PhysObj,
        data: PhysPropertiesStruct
    ): void;
}
