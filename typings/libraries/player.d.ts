/** @noSelfInFile */
declare namespace player {
    /**
 * Similar to the serverside command "bot", this function creates a new Player bot with the given name. This bot will not obey to the usual "bot_*" commands, and it's the same bot base used in TF2 and CS:S.
* 
* @arg string botName - The name of the bot, using an already existing name will append brackets at the end of it with a number pertaining it.
Example: "Bot name test", "Bot name test(1)".
* @return {Player} The newly created Player bot. Returns NULL if there's no Player slots available to host it.
 */
    function CreateNextBot(botName: string): Player;

    /**
     * Gets all the current players in the server (not including connecting clients).
     *
     * @return {Player} All Players currently in the server.
     */
    function GetAll(): Player;

    /**
     * Returns a table of all bots on the server.
     *
     * @return {object} A table only containing bots ( AI / non human players )
     */
    function GetBots(): object;

    /**
     * Gets the player with the specified AccountID.
     *
     * @arg number accountID - The Player:AccountID to find the player by.
     * @return {Player} Player if one is found, false otherwise.
     */
    function GetByAccountID(accountID: number): Player;

    /**
     * Gets the player with the specified connection ID.
     *
     * @arg number connectionID - The connection ID to find the player by.
     * @return {Player} Player if one is found, nil otherwise
     */
    function GetByID(connectionID: number): Player;

    /**
     * Gets the player with the specified SteamID.
     *
     * @arg string steamID - The Player:SteamID to find the player by.
     * @return {Player} Player if one is found, false otherwise.
     */
    function GetBySteamID(steamID: string): Player;

    /**
     * Gets the player with the specified SteamID64.
     *
     * @arg string steamID64 - The Player:SteamID64 to find the player by.
     * @return {Player} Player if one is found, false otherwise.
     */
    function GetBySteamID64(steamID64: string): Player;

    /**
     * Gets the player with the specified uniqueID (not recommended way to identify players).
     *
     * @arg string uniqueID - The Player:UniqueID to find the player by.
     * @return {Player} Player if one is found, false otherwise.
     */
    function GetByUniqueID(uniqueID: string): Player;

    /**
     * Gives you the player count.
     *
     * @return {number} Number of players
     */
    function GetCount(): number;

    /**
     * Returns a table of all human ( non bot/AI ) players.
     *
     * @return {object} A table of all human ( non bot/AI ) players.
     */
    function GetHumans(): object;
}
