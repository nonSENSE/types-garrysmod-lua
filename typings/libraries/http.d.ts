/** @noSelfInFile */
declare namespace http {
    /**
 * Launches an asynchronous GET request to a HTTP server.
* 
* @arg string url - The URL of the website to fetch.
* @arg GMLua.CallbackNoContext [onSuccess=nil] - Function to be called on success. Arguments are

string body
string size - equal to string.len(body).
table headers
number code - The HTTP success code.
* @arg GMLua.CallbackNoContext [onFailure=nil] - Function to be called on failure. Arguments are

string error - The error message.
* @arg object [headers={}] - KeyValue table for headers.
 */
    function Fetch(
        url: string,
        onSuccess?: GMLua.CallbackNoContext,
        onFailure?: GMLua.CallbackNoContext,
        headers?: object
    ): void;

    /**
 * Sends an asynchronous POST request to a HTTP server.
* 
* @arg string url - The url to of the website to post.
* @arg object parameters - The post parameters (x-www-form-urlencoded) to be send to the server. Keys and values must be strings.
* @arg GMLua.CallbackNoContext [onSuccess=nil] - Function to be called on success. Arguments are

string body
string size - equal to string.len(body).
table headers
number code - The HTTP success code.
* @arg GMLua.CallbackNoContext [onFailure=nil] - Function to be called on failure. Arguments are

string error - The error message.
* @arg object [headers={}] - KeyValue table for headers.
 */
    function Post(
        url: string,
        parameters: object,
        onSuccess?: GMLua.CallbackNoContext,
        onFailure?: GMLua.CallbackNoContext,
        headers?: object
    ): void;
}
