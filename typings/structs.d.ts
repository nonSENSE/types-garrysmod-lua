declare interface AmmoDataStruct {
    dmgtype?: number;
    force?: number;
    maxsplash?: number;
    minsplash?: number;
    name: string;
    npcdmg?: number;
    plydmg?: number;
    tracer?: number;
    maxcarry?: number;
    flags?: number;
}

declare interface AngPosStruct {
    Ang: Angle;
    Pos: Vector;
}

declare interface AnimationDataStruct {
    StartTime: number;
    EndTime: number;
    Ease?: number;
    OnEnd: Function;
    Think: Function;
    Pos: Vector;
    StartPos: Vector;
    SizeX: boolean;
    SizeY: boolean;
    Size?: Vector;
    StartSize: Vector;
    Color: any;
    StartColor: any;
    Alpha: number;
    StartAlpha: number;
    Speed: number;
    UseGravity: boolean;
}

declare interface AttachmentDataStruct {
    id: number;
    name: string;
}

declare interface BodyGroupDataStruct {
    id: number;
    name: string;
    num: number;
    submodels: object;
}

declare interface BoneManipulationDataStruct {
    s?: Vector;
    a?: Angle;
    p?: Vector;
}

declare interface BulletStruct {
    Attacker?: Entity;
    Callback?: Function;
    Damage?: number;
    Force?: number;
    Distance?: number;
    HullSize?: number;
    Num?: number;
    Tracer?: number;
    AmmoType?: string;
    TracerName?: string;
    Dir?: Vector;
    Spread?: Vector;
    Src?: Vector;
    IgnoreEntity?: Entity;
}

declare interface CamDataStruct {
    origin: Vector;
    angles: Angle;
    fov: number;
    znear: number;
    zfar: number;
    drawviewer?: boolean;
    ortho?: object;
}

declare interface CollisionDataStruct {
    HitPos: Vector;
    HitEntity: Entity;
    OurOldVelocity: Vector;
    HitObject: PhysObj;
    DeltaTime: number;
    TheirOldVelocity: Vector;
    Speed: number;
    HitNormal: Vector;
    PhysObject: PhysObj;
    OurSurfaceProps: number;
    TheirSurfaceProps: number;
    HitSpeed: Vector;
    OurNewVelocity: Vector;
    TheirNewVelocity: Vector;
    OurOldAngularVelocity: Vector;
    TheirOldAngularVelocity: Vector;
}

declare interface ColorStruct {
    r: number;
    g: number;
    b: number;
    a: number;
}

declare interface CreationMenusStruct {
    Function: Function;
    Icon: string;
    Order: number;
    Tooltip: string;
}

declare interface DateDataStruct {
    day: number;
    hour: number;
    isdst: boolean;
    min: number;
    month: number;
    sec: number;
    wday: number;
    yday: number;
    year: number;
}

declare interface DebugInfoStruct {
    func: Function;
    linedefined: number;
    lastlinedefined: number;
    source: string;
    short_src: string;
    what: string;
    currentline: number;
    name: string;
    namewhat: string;
    isvararg: boolean;
    nparams: number;
    nups: number;
    activelines: object;
}

declare interface DynamicLightStruct {
    brightness: number;
    decay: number;
    dietime: number;
    dir?: Vector;
    innerangle?: number;
    outerangle?: number;
    key: number;
    minlight?: number;
    noworld?: boolean;
    nomodel?: boolean;
    pos: Vector;
    size: number;
    style: number;
    b: number;
    g: number;
    r: number;
}

declare interface EmitSoundInfoStruct {
    SoundName: string;
    OriginalSoundName: string;
    SoundTime?: number;
    DSP?: number;
    SoundLevel?: number;
    Pitch?: number;
    Flags?: number;
    Channel: number;
    Volume: number;
    Entity: Entity;
    Pos: Vector;
}

declare interface ENTStruct {
    Base: string;
    Type: string;
    ClassName: string;
    Folder: string;
    AutomaticFrameAdvance?: boolean;
    Category?: string;
    Spawnable?: boolean;
    Editable?: boolean;
    AdminOnly?: boolean;
    PrintName: string;
    Author: string;
    Contact: string;
    Purpose: string;
    Instructions: string;
    RenderGroup?: number;
    DisableDuplicator?: boolean;
    ScriptedEntityType: string;
    DoNotDuplicate?: boolean;
    IconOverride?: string;
}

declare interface EntityCopyDataStruct {
    Name: string;
    Class: string;
    Pos: Vector;
    Angle: Angle;
    DT: object;
    Model: string;
    ModelScale: number;
    Skin: number;
    ColGroup: number;
    Mins: Vector;
    Maxs: Vector;
    PhysicsObjects: object;
    FlexScale: number;
    Flex: any;
    BodyG: any;
    BoneManip: object;
    MapCreationID: number;
    WorkshopID: number;
}

declare interface FontDataStruct {
    font?: string;
    extended?: boolean;
    size?: number;
    weight?: number;
    blursize?: number;
    scanlines?: number;
    antialias?: boolean;
    underline?: boolean;
    italic?: boolean;
    strikeout?: boolean;
    symbol?: boolean;
    rotary?: boolean;
    shadow?: boolean;
    additive?: boolean;
    outline?: boolean;
}

declare interface GMStruct {
    Name: string;
    Author: string;
    Email: string;
    Website: string;
    FolderName: string;
    Folder: string;
    TeamBased: boolean;
    IsSandboxDerived: boolean;
    ThisClass: string;
    BaseClass: object;
}

declare interface HTTPRequestStruct {
    failed: Function;
    success: Function;
    method: string;
    url: string;
    parameters: object;
    headers: object;
    body: string;
    type?: string;
    timeout?: number;
}

declare interface HullTraceStruct {
    start: Vector;
    endpos: Vector;
    maxs: Vector;
    mins: Vector;
    filter?: Entity;
    mask?: number;
    collisiongroup?: number;
    ignoreworld?: boolean;
    output?: object;
}

declare interface LocalLightStruct {
    type?: number;
    color?: Vector;
    pos?: Vector;
    dir?: Vector;
    range?: number;
    angularFalloff?: number;
    innerAngle?: number;
    outerAngle?: number;
    fiftyPercentDistance: number;
    zeroPercentDistance: number;
    quadraticFalloff?: number;
    linearFalloff?: number;
    constantFalloff?: number;
}

declare interface MatProxyDataStruct {
    name: string;
    init: Function;
    bind: Function;
}

declare interface MeshVertexStruct {
    color: Color;
    normal: Vector;
    tangent: Vector;
    binormal: Vector;
    pos: Vector;
    u: number;
    v: number;
    userdata: object;
}

declare interface OperatingParamsStruct {
    RPM: number;
    gear: number;
    isTorqueBoosting: boolean;
    speed: number;
    steeringAngle: number;
    wheelsInContact: number;
}

declare interface PathSegmentStruct {
    area: CNavArea;
    curvature: number;
    distanceFromStart: number;
    forward: Vector;
    how: number;
    ladder: CNavLadder;
    length: number;
    m_portalCenter: Vector;
    m_portalHalfWidth: number;
    pos: Vector;
    type: number;
}

declare interface PhysEnvPerformanceSettingsStruct {
    LookAheadTimeObjectsVsObject: number;
    LookAheadTimeObjectsVsWorld: number;
    MaxAngularVelocity: number;
    MaxCollisionChecksPerTimestep: number;
    MaxCollisionsPerObjectPerTimestep: number;
    MaxFrictionMass: number;
    MaxVelocity: number;
    MinFrictionMass: number;
}

declare interface PhysicsObjectSaveStruct {
    Pos: Vector;
    Angle: Angle;
    Frozen: boolean;
    NoGrav: boolean;
    Sleep: boolean;
}

declare interface PhysPropertiesStruct {
    GravityToggle?: boolean;
    Material?: string;
}

declare interface PLAYERStruct {
    DisplayName: string;
    WalkSpeed?: number;
    RunSpeed?: number;
    CrouchedWalkSpeed?: number;
    DuckSpeed?: number;
    UnDuckSpeed?: number;
    JumpPower?: number;
    CanUseFlashlight?: boolean;
    MaxHealth?: number;
    MaxArmor?: number;
    StartHealth?: number;
    StartArmor?: number;
    DropWeaponOnDie?: boolean;
    TeammateNoCollide?: boolean;
    AvoidPlayers?: boolean;
    UseVMHands?: boolean;
}

declare interface PolygonVertexStruct {
    x: number;
    y: number;
    u: number;
    v: number;
}

declare interface PropertyAddStruct {
    Type?: string;
    MenuLabel: string;
    MenuIcon: string;
    StructureField(Order): number;
    PrependSpacer?: boolean;
    Filter: Function;
    Checked: Function;
    Action: Function;
    Receive: Function;
    MenuOpen: Function;
    OnCreate: Function;
}

declare interface RenderCamDataStruct {
    x: number;
    y: number;
    w: number;
    h: number;
    type?: string;
    origin: Vector;
    angles: Angle;
    fov: number;
    aspect: number;
    zfar: number;
    znear: number;
    subrect?: boolean;
    bloomtone?: boolean;
    offcenter: object;
    ortho: object;
}

declare interface RenderCaptureDataStruct {
    format: string;
    x: number;
    y: number;
    w: number;
    h: number;
    quality: number;
    alpha?: boolean;
}

declare interface SequenceInfoStruct {
    label: string;
    activityname: string;
    activity: number;
    actweight: number;
    flags: number;
    numevents: number;
    numblends: number;
    bbmin: number;
    bbmax: number;
    fadeintime: number;
    fadeouttime: number;
    localentrynode: number;
    localexitnode: number;
    nodeflags: number;
    lastframe: number;
    nextseq: number;
    pose: number;
    cycleposeindex: number;
    anims: any;
}

declare interface ServerQueryDataStruct {
    GameDir?: string;
    Type: string;
    AppID?: number;
    Callback: Function;
    CallbackFailed: Function;
    Finished: Function;
}

declare interface SoundDataStruct {
    channel: number;
    name: string;
    pitchend: number;
    pitchstart: number;
    level: number;
    sound: string;
    volume?: number;
    pitch?: number;
}

declare interface SoundHintDataStruct {
    origin: Vector;
    owner: Entity;
    target: Entity;
    volume: number;
    type: number;
    expiration: number;
    expires: boolean;
    channel: number;
}

declare interface SunInfoStruct {
    direction: Vector;
    obstruction: number;
}

declare interface SurfacePropertyDataStruct {
    name: string;
    hardnessFactor: number;
    hardThreshold: number;
    hardVelocityThreshold: number;
    reflectivity: number;
    roughnessFactor: number;
    roughThreshold: number;
    climbable: number;
    jumpFactor: number;
    material: number;
    maxSpeedFactor: number;
    dampening: number;
    density: number;
    elasticity: number;
    friction: number;
    thickness: number;
    breakSound: string;
    bulletImpactSound: string;
    impactHardSound: string;
    impactSoftSound: string;
    rollingSound: string;
    scrapeRoughSound: string;
    scrapeSmoothSound: string;
    stepLeftSound: string;
    stepRightSound: string;
    strainSound: string;
}

declare interface SWEPStruct {
    ClassName: string;
    Category?: string;
    Spawnable?: boolean;
    AdminOnly?: boolean;
    PrintName?: string;
    Base?: string;
    m_WeaponDeploySpeed?: number;
    Owner: Entity;
    Weapon: Weapon;
    Author?: string;
    Contact?: string;
    Purpose?: string;
    Instructions?: string;
    ViewModel?: string;
    ViewModelFlip?: boolean;
    ViewModelFlip1?: boolean;
    ViewModelFlip2?: boolean;
    ViewModelFOV?: number;
    WorldModel?: string;
    AutoSwitchFrom?: boolean;
    AutoSwitchTo?: boolean;
    Weight?: number;
    BobScale?: number;
    SwayScale?: number;
    BounceWeaponIcon?: boolean;
    DrawWeaponInfoBox?: boolean;
    DrawAmmo?: boolean;
    DrawCrosshair?: boolean;
    RenderGroup?: number;
    Slot?: number;
    SlotPos?: number;
    SpeechBubbleLid?: number;
    WepSelectIcon?: number;
    CSMuzzleFlashes?: boolean;
    CSMuzzleX?: boolean;
    Primary: object;
    Secondary: object;
    UseHands?: boolean;
    Folder: string;
    AccurateCrosshair?: boolean;
    DisableDuplicator?: boolean;
    ScriptedEntityType?: string;
    m_bPlayPickupSound?: boolean;
    IconOverride?: string;
}

declare interface TeamDataStruct {
    Color: object;
    Joinable: boolean;
    Name: string;
    Score: number;
}

declare interface TextDataStruct {
    text: string;
    font?: string;
    pos: object;
    xalign?: number;
    yalign?: number;
    color?: object;
}

declare interface TextureDataStruct {
    texture: number;
    x: number;
    y: number;
    w: number;
    h: number;
    color?: Color;
}

declare interface TOOLStruct {
    AddToMenu?: boolean;
    Category?: string;
    Command?: string;
    Name?: string;
    ClientConVar: object;
    ServerConVar: object;
    BuildCPanel: Function;
    Information: any;
    Mode: string;
    Tab: string;
    LeftClickAutomatic?: boolean;
    RightClickAutomatic?: boolean;
}

declare interface ToScreenDataStruct {
    x: number;
    y: number;
    visible: boolean;
}

declare interface TraceStruct {
    start?: Vector;
    endpos?: Vector;
    filter?: Entity;
    mask?: number;
    collisiongroup?: number;
    ignoreworld?: boolean;
    output?: object;
}

declare interface TraceResultStruct {
    Entity?: Entity;
    Fraction?: number;
    FractionLeftSolid?: number;
    Hit?: boolean;
    HitBox?: number;
    HitGroup?: number;
    HitNoDraw?: boolean;
    HitNonWorld?: boolean;
    HitNormal?: Vector;
    HitPos: Vector;
    HitSky?: boolean;
    HitTexture?: string;
    HitWorld?: boolean;
    MatType?: number;
    Normal: Vector;
    PhysicsBone?: number;
    StartPos: Vector;
    SurfaceProps?: number;
    StartSolid?: boolean;
    AllSolid?: boolean;
    SurfaceFlags?: number;
    DispFlags?: number;
    Contents: number;
}

declare interface UGCFileInfoStruct {
    id: number;
    title: string;
    description: string;
    fileid: number;
    previewid: number;
    previewurl: string;
    owner: number;
    created: number;
    updated: number;
    banned: boolean;
    tags: string;
    size: number;
    previewsize: number;
    installed: boolean;
    disabled: boolean;
    children: object;
    ownername: string;
    error: number;
}

declare interface UndoStruct {
    Owner: Player;
    Name: string;
    Entities: object;
    Functions: object;
    CustomUndoText: string;
    NiceName: string;
}

declare interface VehicleParamsStruct {
    wheelsPerAxle: number;
    axleCount: number;
    axles: VehicleParamsAxleStruct;
    body: VehicleParamsBodyStruct;
    engine: VehicleParamsEngineStruct;
    steering: VehicleParamsSteeringStruct;
}

declare interface VehicleParamsAxleStruct {
    brakeFactor: number;
    offset: Vector;
    raytraceCenterOffset: Vector;
    raytraceOffset: Vector;
    suspension_maxBodyForce: number;
    suspension_springConstant: number;
    suspension_springDamping: number;
    suspension_springDampingCompression: number;
    suspension_stabilizerConstant: number;
    torqueFactor: number;
    wheelOffset: Vector;
    wheels_brakeMaterialIndex: number;
    wheels_damping: number;
    wheels_frictionScale: number;
    wheels_inertia: number;
    wheels_mass: number;
    wheels_materialIndex: number;
    wheels_radius: number;
    wheels_rotdamping: number;
    wheels_skidMaterialIndex: number;
    wheels_springAdditionalLength: number;
}

declare interface VehicleParamsBodyStruct {
    addGravity: number;
    counterTorqueFactor: number;
    keepUprightTorque: number;
    massCenterOverride: Vector;
    massOverride: number;
    maxAngularVelocity: number;
    tiltForce: number;
    tiltForceHeight: number;
}

declare interface VehicleParamsEngineStruct {
    autobrakeSpeedFactor: number;
    autobrakeSpeedGain: number;
    axleRatio: number;
    boostDelay: number;
    boostDuration: number;
    boostForce: number;
    boostMaxSpeed: number;
    gearCount: number;
    gearRatio: object;
    horsepower: number;
    isAutoTransmission: boolean;
    maxRPM: number;
    maxRevSpeed: number;
    maxSpeed: number;
    shiftDownRPM: number;
    shiftUpRPM: number;
    throttleTime: number;
    torqueBoost: boolean;
}

declare interface VehicleParamsSteeringStruct {
    boostSteeringRateFactor: number;
    boostSteeringRestRateFactor: number;
    brakeSteeringRateFactor: number;
    degreesBoost: number;
    degreesFast: number;
    degreesSlow: number;
    dustCloud: boolean;
    isSkidAllowed: boolean;
    powerSlideAccel: number;
    speedFast: number;
    speedSlow: number;
    steeringExponent: number;
    steeringRateFast: number;
    steeringRateSlow: number;
    steeringRestRateFast: number;
    steeringRestRateSlow: number;
    throttleSteeringRestRateFactor: number;
    turnThrottleReduceFast: number;
    turnThrottleReduceSlow: number;
}

declare interface VideoDataStruct {
    container: string;
    video: string;
    audio: string;
    quality: number;
    bitrate: number;
    fps: number;
    lockfps: boolean;
    name: string;
    width: number;
    height: number;
}

declare interface ViewDataStruct {
    origin: Vector;
    angles: Angle;
    aspect: number;
    x: number;
    y: number;
    w: number;
    h: number;
    drawhud?: boolean;
    drawmonitors?: boolean;
    drawviewmodel?: boolean;
    viewmodelfov: number;
    fov: number;
    ortho: object;
    ortholeft: number;
    orthoright: number;
    orthotop: number;
    orthobottom: number;
    znear: number;
    zfar: number;
    znearviewmodel: number;
    zfarviewmodel: number;
    dopostprocess?: boolean;
    bloomtone?: boolean;
    offcenter: any;
}

declare interface ViewSetupStruct {
    origin: Vector;
    angles: Angle;
    aspect: number;
    x: number;
    y: number;
    width: number;
    height: number;
    fov: number;
    fov_unscaled: number;
    fovviewmodel: number;
    fovviewmodel_unscaled: number;
    ortho: object;
    znear: number;
    zfar: number;
    znearviewmodel: number;
    zfarviewmodel: number;
    bloomtone: boolean;
    subrect: boolean;
    offcenter: ViewDataStruct;
}
