/** @noSelfInFile */
declare namespace DLabel {
    /**
     * Called when the label is left clicked (on key release) by the player.
     */
    function DoClick(): void;

    /**
     * Called when the label is double clicked by the player with left clicks.
     */
    function DoDoubleClick(): void;

    /**
     * Called when the label is middle mouse (Mouse wheel, also known as mouse 3) clicked (on key release) by the player.
     */
    function DoMiddleClick(): void;

    /**
     * Called when the label is right clicked (on key release) by the player.
     */
    function DoRightClick(): void;

    /**
     * Called when the player presses the label with any mouse button.
     */
    function OnDepressed(): void;

    /**
     * Called when the player releases any mouse button on the label. This is always called after DLabel:OnDepressed.
     */
    function OnReleased(): void;

    /**
     * Called when the toggle state of the label is changed by DLabel:Toggle.
     *
     * @arg boolean toggleState - The new toggle state.
     */
    function OnToggled(toggleState: boolean): void;

    /**
     * Called just before DLabel:DoClick.
     */
    function DoClickInternal(): void;

    /**
     * Called just before DLabel:DoDoubleClick. In DLabel does nothing and is safe to override.
     */
    function DoDoubleClickInternal(): void;

    /**
     * Returns whether the label stretches vertically or not.
     *
     * @return {boolean} Whether the label stretches vertically or not.
     */
    function GetAutoStretchVertical(): boolean;

    /**
     * Returns whether the DLabel should set its text color to the current skin's bright text color.
     *
     * @return {boolean}
     */
    function GetBright(): boolean;

    /**
     * Returns the actual color of the text.
     *
     * @return {object} The the actual color of the text.
     */
    function GetColor(): object;

    /**
     * Returns whether the DLabel should set its text color to the current skin's dark text color.
     *
     * @return {boolean}
     */
    function GetDark(): boolean;

    /**
     * Gets the disabled state of the DLabel. This is set with DLabel:SetDisabled.
     *
     * @return {boolean} The disabled state of the label.
     */
    function GetDisabled(): boolean;

    /**
     * Returns whether or not double clicking will call DLabel:DoDoubleClick.
     *
     * @return {boolean} true = enabled, false means disabled
     */
    function GetDoubleClickingEnabled(): boolean;

    /**
     * Returns the current font of the DLabel. This is set with DLabel:SetFont.
     *
     * @return {string} The name of the font in use.
     */
    function GetFont(): string;

    /**
     * Returns whether the DLabel should set its text color to the current skin's highlighted text color.
     *
     * @return {boolean}
     */
    function GetHighlight(): boolean;

    /**
     * Returns whether the toggle functionality is enabled for a label. Set with DLabel:SetIsToggle.
     *
     * @return {boolean} Whether or not toggle functionality is enabled.
     */
    function GetIsToggle(): boolean;

    /**
     * Returns the "override" text color, set by DLabel:SetTextColor.
     *
     * @return {object} The color of the text, or nil.
     */
    function GetTextColor(): object;

    /**
     * Returns the "internal" or fallback color of the text.
     *
     * @return {object} The "internal" color of the text
     */
    function GetTextStyleColor(): object;

    /**
     * Returns the current toggle state of the label. This can be set with DLabel:SetToggle and toggled with DLabel:Toggle.
     *
     * @return {boolean} The current toggle state.
     */
    function GetToggle(): boolean;

    /**
     * Automatically adjusts the height of the label dependent of the height of the text inside of it.
     *
     * @arg boolean stretch - Whenever to stretch the label vertically or not.
     */
    function SetAutoStretchVertical(stretch: boolean): void;

    /**
     * Sets the color of the text to the bright text color defined in the skin.
     *
     * @arg boolean bright - Whenever to set the text to bright or not.
     */
    function SetBright(bright: boolean): void;

    /**
     * Changes color of label. Alias of DLabel:SetTextColor.
     *
     * @arg object color - The color to set. Uses the Color structure.
     */
    function SetColor(color: object): void;

    /**
     * Sets the color of the text to the dark text color defined in the skin.
     *
     * @arg boolean dark - Whenever to set the text to dark or not.
     */
    function SetDark(dark: boolean): void;

    /**
     * Sets the disabled state of the DLabel.
     *
     * @arg boolean disable - true to disable the DLabel, false to enable it.
     */
    function SetDisabled(disable: boolean): void;

    /**
     * Sets whether or not double clicking should call DLabel:DoDoubleClick.
     *
     * @arg boolean enable - true to enable, false to disable
     */
    function SetDoubleClickingEnabled(enable: boolean): void;

    /**
 * Sets the font of the label.
* 
* @arg string fontName - The name of the font.
See here for a list of existing fonts.
Alternatively, use surface.CreateFont to create your own custom font.
 */
    function SetFont(fontName: string): void;

    /**
     * Sets the color of the text to the highlight text color defined in the skin.
     *
     * @arg boolean highlight - true to set the label's color to skins's text highlight color, false otherwise.
     */
    function SetHighlight(highlight: boolean): void;

    /**
     * Enables or disables toggle functionality for a label. Retrieved with DLabel:GetIsToggle.
     *
     * @arg boolean allowToggle - Whether or not to enable toggle functionality.
     */
    function SetIsToggle(allowToggle: boolean): void;

    /**
     * Sets the text color of the DLabel. This will take precedence over DLabel:SetTextStyleColor.
     *
     * @arg Color color - The text color. Uses the Color.
     */
    function SetTextColor(color: Color): void;

    /**
     * Used by DLabel:SetDark, DLabel:SetBright and DLabel:SetHighlight to set the text color without affecting DLabel:SetTextColor calls.
     *
     * @arg Color color - The text color. Uses the Color.
     */
    function SetTextStyleColor(color: Color): void;

    /**
     * Sets the toggle state of the label. This can be retrieved with DLabel:GetToggle and toggled with DLabel:Toggle.
     *
     * @arg boolean toggleState - The toggle state to be set.
     */
    function SetToggle(toggleState: boolean): void;

    /**
     * Toggles the label's state. This can be set and retrieved with DLabel:SetToggle and DLabel:GetToggle.
     */
    function Toggle(): void;

    /**
     * A hook called from within PANEL:ApplySchemeSettings to determine the color of the text on display.
     *
     * @arg object skin - A table supposed to contain the color values listed above.
     */
    function UpdateColours(skin: object): void;

    /**
     * Called internally to update the color of the text.
     */
    function UpdateFGColor(): void;
}
