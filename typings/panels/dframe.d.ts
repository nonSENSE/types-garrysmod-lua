/** @noSelfInFile */
declare namespace DFrame {
    /**
     * Called when the DFrame is closed with DFrame:Close. This applies when the close button in the DFrame's control box is clicked.
     */
    function OnClose(): void;

    /**
     * Centers the frame relative to the whole screen and invalidates its layout. This overrides Panel:Center.
     */
    function Center(): void;

    /**
     * Hides or removes the DFrame, and calls DFrame:OnClose.
     */
    function Close(): void;

    /**
     * Gets whether the background behind the frame is being blurred.
     *
     * @return {boolean} Whether or not background blur is enabled.
     */
    function GetBackgroundBlur(): boolean;

    /**
     * Determines whether or not the DFrame will be removed when it is closed. This is set with DFrame:SetDeleteOnClose.
     *
     * @return {boolean} Whether or not the frame will be removed on close.
     */
    function GetDeleteOnClose(): boolean;

    /**
     * Gets whether or not the frame is draggable by the user.
     *
     * @return {boolean} Whether the frame is draggable or not.
     */
    function GetDraggable(): boolean;

    /**
     * Gets whether or not the frame is part of a derma menu. This is set with DFrame:SetIsMenu.
     *
     * @return {boolean} Whether or not this frame is a menu component.
     */
    function GetIsMenu(): boolean;

    /**
     * Gets the minimum height the DFrame can be resized to by the user.
     *
     * @return {number} The minimum height the user can resize the frame to.
     */
    function GetMinHeight(): number;

    /**
     * Gets the minimum width the DFrame can be resized to by the user.
     *
     * @return {number} The minimum width the user can resize the frame to.
     */
    function GetMinWidth(): number;

    /**
     * Gets whether or not the shadow effect bordering the DFrame is being drawn.
     *
     * @return {boolean} Whether or not the shadow is being drawn.
     */
    function GetPaintShadow(): boolean;

    /**
     * Gets whether or not the DFrame is restricted to the boundaries of the screen resolution.
     *
     * @return {boolean} Whether or not the frame is restricted.
     */
    function GetScreenLock(): boolean;

    /**
     * Gets whether or not the DFrame can be resized by the user.
     *
     * @return {boolean} Whether the frame can be resized or not.
     */
    function GetSizable(): boolean;

    /**
     * Returns the title of the frame.
     *
     * @return {string} Title of the frame.
     */
    function GetTitle(): string;

    /**
     * Determines if the frame or one of its children has the screen focus.
     *
     * @return {boolean} Whether or not the frame has focus.
     */
    function IsActive(): boolean;

    /**
     * Blurs background behind the frame.
     *
     * @arg boolean blur - Whether or not to create background blur or not.
     */
    function SetBackgroundBlur(blur: boolean): void;

    /**
     * Determines whether or not the DFrame is removed when it is closed with DFrame:Close.
     *
     * @arg boolean shouldDelete - Whether or not to delete the frame on close. This is true by default.
     */
    function SetDeleteOnClose(shouldDelete: boolean): void;

    /**
     * Sets whether the frame should be draggable by the user. The DFrame can only be dragged from its title bar.
     *
     * @arg boolean draggable - Whether to be draggable or not.
     */
    function SetDraggable(draggable: boolean): void;

    /**
 * Adds or removes an icon on the left of the DFrame's title.
* 
* @arg string path - Set to nil to remove the icon.
Otherwise, set to file path to create the icon.
 */
    function SetIcon(path: string): void;

    /**
     * Sets whether the frame is part of a derma menu or not.
     *
     * @arg boolean isMenu - Whether or not this frame is a menu component.
     */
    function SetIsMenu(isMenu: boolean): void;

    /**
     * Sets the minimum height the DFrame can be resized to by the user.
     *
     * @arg number minH - The minimum height the user can resize the frame to.
     */
    function SetMinHeight(minH: number): void;

    /**
     * Sets the minimum width the DFrame can be resized to by the user.
     *
     * @arg number minW - The minimum width the user can resize the frame to.
     */
    function SetMinWidth(minW: number): void;

    /**
     * Sets whether or not the shadow effect bordering the DFrame should be drawn.
     *
     * @arg boolean shouldPaint - Whether or not to draw the shadow. This is true by default.
     */
    function SetPaintShadow(shouldPaint: boolean): void;

    /**
     * Sets whether the DFrame is restricted to the boundaries of the screen resolution.
     *
     * @arg boolean lock - If true, the frame cannot be dragged outside of the screen bounds
     */
    function SetScreenLock(lock: boolean): void;

    /**
     * Sets whether or not the DFrame can be resized by the user.
     *
     * @arg boolean sizeable - Whether the frame should be resizeable or not.
     */
    function SetSizable(sizeable: boolean): void;

    /**
     * Sets the title of the frame.
     *
     * @arg string title - New title of the frame.
     */
    function SetTitle(title: string): void;

    /**
     * Determines whether the DFrame's control box (close, minimise and maximise buttons) is displayed.
     *
     * @arg boolean show - false hides the control box; this is true by default.
     */
    function ShowCloseButton(show: boolean): void;
}
