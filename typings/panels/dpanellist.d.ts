/** @noSelfInFile */
declare namespace DPanelList {
    /**
     * Adds a existing panel to the end of DPanelList.
     *
     * @arg Panel pnl - Panel to be used as element of list
     * @arg string [state="nil"] - If set to "ownline", the item will take its own entire line.
     */
    function AddItem(pnl: Panel, state?: string): void;

    /**
     * Removes all items.
     */
    function CleanList(): void;

    /**
     * Enables/creates the vertical scroll bar so that the panel list can be scrolled through.
     */
    function EnableVerticalScrollbar(): void;

    /**
     * Returns all panels has added by DPanelList:AddItem
     *
     * @return {any} A table of panels used as items of DPanelList.
     */
    function GetItems(): any;

    /**
     * Returns offset of list items from the panel borders set by DPanelList:SetPadding
     *
     * @return {number} Offset from panel borders
     */
    function GetPadding(): number;

    /**
     * Returns distance between list items set by DPanelList:SetSpacing
     *
     * @return {number} Distance between panels
     */
    function GetSpacing(): number;

    /**
     *
     *
     * @arg Panel insert - The panel to insert
     * @arg string strLineState - If set to "ownline", no other panels will be placed to the left or right of the panel we are inserting
     */
    function InsertAtTop(insert: Panel, strLineState: string): void;

    /**
     * Sets the offset of the lists items from the panel borders
     *
     * @arg number Offset - Offset from panel borders
     */
    function SetPadding(Offset: number): void;

    /**
     * Sets distance between list items
     *
     * @arg number Distance - Distance between panels
     */
    function SetSpacing(Distance: number): void;
}
