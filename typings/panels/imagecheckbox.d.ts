/** @noSelfInFile */
declare namespace ImageCheckBox {
    /**
     * Returns the checked state of the ImageCheckBox
     *
     * @return {boolean} true for checked, false otherwise
     */
    function GetChecked(): boolean;

    /**
     * Sets the checked state of the checkbox.
     *
     * @arg boolean OnOff - true for checked, false otherwise
     */
    function Set(OnOff: boolean): void;

    /**
     * Sets the checked state of the checkbox.
     *
     * @arg boolean bOn - true for checked, false otherwise
     */
    function SetChecked(bOn: boolean): void;

    /**
     * Sets the material that will be visible when the ImageCheckBox is checked.
     *
     * @arg string mat - The file path of the material to set (relative to "garrysmod/materials/").
     */
    function SetMaterial(mat: string): void;
}
