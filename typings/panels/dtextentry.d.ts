/** @noSelfInFile */
declare namespace DTextEntry {
    /**
     * Called whenever the value of the panel has been updated (whether by user input or otherwise).
     *
     * @arg string char - The last character entered into the panel.
     * @return {boolean} Return true to prevent the value from changing, false to allow it.
     */
    function AllowInput(char: string): boolean;

    /**
     * Returns whether a string is numeric or not.
     * Always returns false if the DTextEntry:SetNumeric is set to false.
     *
     * @arg string strValue - The string to check.
     * @return {boolean} Whether the string is numeric or not.
     */
    function CheckNumeric(strValue: string): boolean;

    /**
     * Called by the DTextEntry when a list of autocompletion options is requested. Meant to be overridden.
     *
     * @arg string inputText - Player's current input.
     * @return {object} If a table is returned, the values of the table will show up as autocomplete suggestions for the user.
     */
    function GetAutoComplete(inputText: string): object;

    /**
     * Returns the cursor color of a DTextEntry.
     *
     * @return {Color} The color of the cursor as a Color.
     */
    function GetCursorColor(): Color;

    /**
     * Returns whether pressing Enter can cause the panel to lose focus. Note that a multiline DTextEntry cannot be escaped using the Enter key even when this function returns true.
     *
     * @return {boolean} Whether pressing the Enter key can cause the panel to lose focus.
     */
    function GetEnterAllowed(): boolean;

    /**
     * Returns the contents of the DTextEntry as a number.
     *
     * @return {number} Text of the DTextEntry as a float, or nil if it cannot be converted to a number using tonumber.
     */
    function GetFloat(): number;

    /**
     * Similar to DTextEntry:GetFloat, but rounds the value to the nearest integer.
     *
     * @return {number} Text of the DTextEntry as a round number, or nil if it cannot be converted to a number.
     */
    function GetInt(): number;

    /**
     * Returns whether only numeric characters (123456789.-) can be entered into the DTextEntry.
     *
     * @return {boolean} Whether the DTextEntry is numeric or not.
     */
    function GetNumeric(): boolean;

    /**
     * Whether the background is displayed or not
     *
     * @return {boolean} false hides the background; this is true by default.
     */
    function GetPaintBackground(): boolean;

    /**
     * Return current color of panel placeholder
     *
     * @return {object} Current placeholder color
     */
    function GetPlaceholderColor(): object;

    /**
     * Returns the placeholder text set with DTextEntry:SetPlaceholderText.
     *
     * @return {string}
     */
    function GetPlaceholderText(): string;

    /**
     * Returns the text color of a DTextEntry.
     *
     * @return {Color} The color of the text as a Color.
     */
    function GetTextColor(): Color;

    /**
     * Returns whether the DTextEntry is set to run DTextEntry:OnValueChange every time a character is typed or deleted or only when Enter is pressed.
     *
     * @return {boolean}
     */
    function GetUpdateOnType(): boolean;

    /**
     * Returns whether this DTextEntry is being edited or not. (i.e. has focus)
     *
     * @return {boolean} Whether this DTextEntry is being edited or not
     */
    function IsEditing(): boolean;

    /**
     * Determines whether or not DTextEntry is in multi-line mode. This is set with DTextEntry:SetMultiline.
     *
     * @return {boolean} Whether the object is in multi-line mode or not.
     */
    function IsMultiline(): boolean;

    /**
     * Called internally by DTextEntry:OnTextChanged when the user modifies the text in the DTextEntry.
     */
    function OnChange(): void;

    /**
     * Called whenever enter is pressed on a DTextEntry.
     *
     * @arg string value - The current text of the DTextEntry
     */
    function OnEnter(value: string): void;

    /**
     * Called whenever the DTextEntry gains focus.
     */
    function OnGetFocus(): void;

    /**
     * Called from DTextEntry's TextEntry:OnKeyCodeTyped override whenever a valid character is typed while the text entry is focused.
     *
     * @arg number keyCode - They key code of the key pressed, see Enums/KEY.
     */
    function OnKeyCode(keyCode: number): void;

    /**
     * Called whenever the DTextEntry lose focus.
     */
    function OnLoseFocus(): void;

    /**
     * Called internally when the text inside the DTextEntry changes.
     *
     * @arg boolean noMenuRemoval - Determines whether to remove the autocomplete menu (false) or not (true).
     */
    function OnTextChanged(noMenuRemoval: boolean): void;

    /**
     * Called internally when the text changes of the DTextEntry are applied. (And set to the attached console variable, if one is given)
     *
     * @arg string value - The DTextEntry text.
     */
    function OnValueChange(value: string): void;

    /**
     * Builds a DMenu for the DTextEntry based on the input table.
     *
     * @arg any tab - Table containing results from DTextEntry:GetAutoComplete.
     */
    function OpenAutoComplete(tab: any): void;

    /**
     * Sets the cursor's color in  DTextEntry (the blinking line).
     *
     * @arg object color - The color to set the cursor to.
     */
    function SetCursorColor(color: object): void;

    /**
     * Disables input on a DTextEntry and greys it out visually. This differs from DTextEntry:SetEditable which doesn't visually change the textbox.
     *
     * @arg boolean disabled - Whether the textbox should be disabled
     */
    function SetDisabled(disabled: boolean): void;

    /**
     * Disables Input on a DTextEntry. This differs from DTextEntry:SetDisabled - SetEditable will not affect the appearance of the textbox.
     *
     * @arg boolean enabled - Whether the DTextEntry should be editable
     */
    function SetEditable(enabled: boolean): void;

    /**
     * Sets whether pressing the Enter key will cause the DTextEntry to lose focus or not, provided it is not multiline. This is true by default.
     *
     * @arg boolean allowEnter - If set to false, pressing Enter will not cause the panel to lose focus and will never call DTextEntry:OnEnter.
     */
    function SetEnterAllowed(allowEnter: boolean): void;

    /**
     * Changes the font of the DTextEntry.
     *
     * @arg string font - The name of the font to be changed to.
     */
    function SetFont(font: string): void;

    /**
     * Enables or disables the history functionality of  DTextEntry.
     *
     * @arg boolean enable - Whether to enable history or not.
     */
    function SetHistoryEnabled(enable: boolean): void;

    /**
     * Enables or disables the multi-line functionality of DTextEntry.
     *
     * @arg boolean multiline - Whether to enable multiline or not.
     */
    function SetMultiline(multiline: boolean): void;

    /**
     * Sets whether or not to decline non-numeric characters as input.
     *
     * @arg boolean numericOnly - Whether to accept only numeric characters.
     */
    function SetNumeric(numericOnly: boolean): void;

    /**
     * Sets whether to show background.
     *
     * @arg boolean show - false hides the background; this is true by default.
     */
    function SetPaintBackground(show: boolean): void;

    /**
     * Allow you to set placeholder color.
     *
     * @arg object [color=Color(128, 128, 128)] - The color of the placeholder.
     */
    function SetPlaceholderColor(color?: object): void;

    /**
     * Sets the placeholder text that will be shown while the text entry has no user text. The player will not need to delete the placeholder text if they decide to start typing.
     *
     * @arg string [text="nil"]
     */
    function SetPlaceholderText(text?: string): void;

    /**
     * Sets whether or not the panel accepts tab key.
     *
     * @arg boolean enabled - Whether the DTextEntry should ignore tab
     */
    function SetTabbingDisabled(enabled: boolean): void;

    /**
     * Sets the text color of the DTextEntry.
     *
     * @arg Color color - The text color. Uses the Color.
     */
    function SetTextColor(color: Color): void;

    /**
     * Sets whether we should fire DTextEntry:OnValueChange every time we type or delete a character or only when Enter is pressed.
     *
     * @arg boolean updateOnType
     */
    function SetUpdateOnType(updateOnType: boolean): void;

    /**
     * Sets the text of the DTextEntry and calls DTextEntry:OnValueChange.
     *
     * @arg string text - The value to set.
     */
    function SetValue(text: string): void;
}
