/** @noSelfInFile */
declare namespace DIconLayout {
    /**
     * Called when the panel is modified.
     */
    function OnModified(): void;

    /**
     * Creates a replica of the DIconLayout it is called on.
     *
     * @return {Panel} The replica.
     */
    function Copy(): Panel;

    /**
     * Copies the contents (Child elements) of another DIconLayout to itself.
     *
     * @arg Panel from - DIconLayout to copy from.
     */
    function CopyContents(from: Panel): void;

    /**
     * Returns the size of the border.
     *
     * @return {number}
     */
    function GetBorder(): number;

    /**
     * Returns the direction that it will be layed out, using the DOCK enumerations.
     *
     * @return {number} Layout direction.
     */
    function GetLayoutDir(): number;

    /**
     * Returns the distance between two 'icons' on the X axis.
     *
     * @return {number} Distance between two 'icons' on the X axis.
     */
    function GetSpaceX(): number;

    /**
     * Returns distance between two "Icons" on the Y axis.
     *
     * @return {number} distance between two "Icons" on the Y axis.
     */
    function GetSpaceY(): number;

    /**
     * Returns whether the icon layout will stretch its height to fit all the children.
     *
     * @return {boolean}
     */
    function GetStretchHeight(): boolean;

    /**
     * Returns whether the icon layout will stretch its width to fit all the children.
     *
     * @return {boolean}
     */
    function GetStretchWidth(): boolean;

    /**
     * Resets layout vars before calling Panel:InvalidateLayout. This is called when children are added or removed, and must be called when the spacing, border or layout direction is changed.
     */
    function Layout(): void;

    /**
     * Used internally to layout the child elements if the DIconLayout:SetLayoutDir is set to LEFT (See Enums/DOCK).
     */
    function LayoutIcons_LEFT(): void;

    /**
     * Used internally to layout the child elements if the DIconLayout:SetLayoutDir is set to TOP (See Enums/DOCK).
     */
    function LayoutIcons_TOP(): void;

    /**
     * Sets the internal border (padding) within the DIconLayout. This will not change its size, only the positioning of children. You must call DIconLayout:Layout in order for the changes to take effect.
     *
     * @arg number width - The border (padding) inside the DIconLayout.
     */
    function SetBorder(width: number): void;

    /**
     * Sets the direction that it will be layed out, using the Enums/DOCK.
     *
     * @arg number direction - Enums/DOCK
     */
    function SetLayoutDir(direction: number): void;

    /**
     * Sets the horizontal (x) spacing between children within the DIconLayout. You must call DIconLayout:Layout in order for the changes to take effect.
     *
     * @arg number xSpacing - The width of the gap between child objects.
     */
    function SetSpaceX(xSpacing: number): void;

    /**
     * Sets the vertical (y) spacing between children within the DIconLayout. You must call DIconLayout:Layout in order for the changes to take effect.
     *
     * @arg number ySpacing - The vertical gap between rows in the DIconLayout.
     */
    function SetSpaceY(ySpacing: number): void;

    /**
     * If set to true, the icon layout will stretch its height to fit all the children.
     *
     * @arg boolean do_stretch
     */
    function SetStretchHeight(do_stretch: boolean): void;

    /**
     * If set to true, the icon layout will stretch its width to fit all the children.
     *
     * @arg boolean stretchW
     */
    function SetStretchWidth(stretchW: boolean): void;
}
