/** @noSelfInFile */
declare namespace DTooltip {
    /**
     * Forces the tooltip to close. This will remove the panel.
     */
    function Close(): void;

    /**
     * Used to draw a triangle beneath the DTooltip
     *
     * @arg number x - arrow location on the x axis
     * @arg number y - arrow location on the y axis
     */
    function DrawArrow(x: number, y: number): void;

    /**
     * Sets up the tooltip for display for given panel and starts the timer.
     *
     * @arg Panel pnl - The panel to open the tooltip for.
     */
    function OpenForPanel(pnl: Panel): void;

    /**
     * Positions the DTooltip so it doesn't stay in the same draw position.
     */
    function PositionTooltip(): void;

    /**
     * What Panel you want put inside of the DTooltip
     *
     * @arg Panel panel - Contents
     * @arg boolean [bDelete=false] - If set to true, the panel in the first argument will be automatically removed when DTooltip is closed via DTooltip:Close.
     */
    function SetContents(panel: Panel, bDelete?: boolean): void;
}
