/** @noSelfInFile */
declare namespace DNotify {
    /**
     * Adds a panel to the notification
     *
     * @arg Panel pnl - The panel to add
     * @arg number [lifeLength=NaN] - If set, overrides DNotify:SetLife.
     */
    function AddItem(pnl: Panel, lifeLength?: number): void;

    /**
     * Returns the current alignment of this notification panel. Set by DNotify:SetAlignment.
     *
     * @return {number} The numpad alignment
     */
    function GetAlignment(): number;

    /**
     * Returns all the items added with DNotify:AddItem.
     *
     * @return {Panel} A table of Panels.
     */
    function GetItems(): Panel;

    /**
     * Returns the display time in seconds of the DNotify. This is set with
     * DNotify:SetLife.
     *
     * @return {number} The display time in seconds.
     */
    function GetLife(): number;

    /**
     * Returns the spacing between items set by DNotify:SetSpacing.
     *
     * @return {number}
     */
    function GetSpacing(): number;

    /**
     * Sets the alignment of the child panels in the notification
     *
     * @arg number alignment - It's the Numpad alignment, 6 is right, 9 is top left, etc.
     */
    function SetAlignment(alignment: number): void;

    /**
     * Sets the display time in seconds for the DNotify.
     *
     * @arg number time - The time in seconds.
     */
    function SetLife(time: number): void;

    /**
     * Sets the spacing between child elements of the notification panel.
     *
     * @arg number spacing
     */
    function SetSpacing(spacing: number): void;

    /**
     *
     */
    function Shuffle(): void;
}
