/** @noSelfInFile */
declare namespace DKillIcon {
    /**
     * Gets the killicon being shown.
     *
     * @return {string} The name of the killicon currently being displayed.
     */
    function GetName(): string;

    /**
     * Sets the killicon to be displayed. You should call Panel:SizeToContents following this.
     *
     * @arg string iconName - The name of the killicon to be displayed.
     */
    function SetName(iconName: string): void;
}
