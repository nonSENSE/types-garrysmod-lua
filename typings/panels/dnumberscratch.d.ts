/** @noSelfInFile */
declare namespace DNumberScratch {
    /**
     * Used by DNumberScratch:DrawScreen.
     *
     * @arg number level
     * @arg number x
     * @arg number y
     * @arg number w
     * @arg number h
     * @arg number range
     * @arg number value
     * @arg number min
     * @arg number max
     */
    function DrawNotches(
        level: number,
        x: number,
        y: number,
        w: number,
        h: number,
        range: number,
        value: number,
        min: number,
        max: number
    ): void;

    /**
     * Used by DNumberScratch:PaintScratchWindow.
     *
     * @arg number x
     * @arg number y
     * @arg number w
     * @arg number h
     */
    function DrawScreen(x: number, y: number, w: number, h: number): void;

    /**
     * Returns whether this panel is active or not, i.e. if the player is currently changing its value.
     *
     * @return {boolean}
     */
    function GetActive(): boolean;

    /**
     * Returns the desired amount of numbers after the decimal point.
     *
     * @return {number} 0 for whole numbers only, 1 for one number after the decimal point, etc.
     */
    function GetDecimals(): number;

    /**
     * Returns the real value of the DNumberScratch as a number.
     *
     * @return {number} The real value of the DNumberScratch
     */
    function GetFloatValue(): number;

    /**
     * Returns the value of the DNumberScratch as a fraction, a value between 0 and 1 where 0 is the minimum and 1 is the maximum value of the DNumberScratch.
     *
     * @return {number} A value between 0 and 1
     */
    function GetFraction(): number;

    /**
     * Returns the maximum value that can be selected on the number scratch
     *
     * @return {number} The maximum value that can be selected on the number scratch
     */
    function GetMax(): number;

    /**
     * Returns the minimum value that can be selected on the number scratch
     *
     * @return {number} The minimum value that can be selected on the number scratch
     */
    function GetMin(): number;

    /**
     * Returns the range of the DNumberScratch. Basically max value - min value.
     *
     * @return {number} The range of the DNumberScratch
     */
    function GetRange(): number;

    /**
     * Returns whether the scratch window should be visible or not.
     *
     * @return {boolean}
     */
    function GetShouldDrawScreen(): boolean;

    /**
     * Returns the real value of the DNumberScratch as a string.
     *
     * @return {string} The real value of the DNumberScratch
     */
    function GetTextValue(): string;

    /**
     * Returns the zoom level of the scratch window
     *
     * @return {number}
     */
    function GetZoom(): number;

    /**
     * Returns the ideal zoom level for the panel based on the DNumberScratch:GetRange.
     *
     * @return {number} The ideal zoom level for the panel based on the panels range.
     */
    function IdealZoom(): number;

    /**
     * Returns whether the player is currently editing the value of the DNumberScratch.
     *
     * @return {boolean}
     */
    function IsEditing(): boolean;

    /**
     * Used to lock the cursor in place.
     */
    function LockCursor(): void;

    /**
     * Called when the value of the DNumberScratch is changed.
     *
     * @arg number newValue - The new value
     */
    function OnValueChanged(newValue: number): void;

    /**
     * Used to paint the 'scratch' window.
     */
    function PaintScratchWindow(): void;

    /**
     * Sets whether or not the panel is 'active'.
     *
     * @arg boolean active - true to activate, false to deactivate.
     */
    function SetActive(active: boolean): void;

    /**
     * Sets the desired amount of numbers after the decimal point.
     *
     * @arg number decimals - 0 for whole numbers only, 1 for one number after the decimal point, etc.
     */
    function SetDecimals(decimals: number): void;

    /**
     * Does not trigger DNumberScratch:OnValueChanged
     *
     * @arg number val - The value to set
     */
    function SetFloatValue(val: number): void;

    /**
     * Sets the value of the DNumberScratch as a fraction, a value between 0 and 1 where 0 is the minimum and 1 is the maximum value of the DNumberScratch
     *
     * @arg number frac - A value between 0 and 1
     */
    function SetFraction(frac: number): void;

    /**
     * Sets the max value that can be selected on the number scratch
     *
     * @arg number max - The maximum number
     */
    function SetMax(max: number): void;

    /**
     * Sets the minimum value that can be selected on the number scratch.
     *
     * @arg number min - The minimum number
     */
    function SetMin(min: number): void;

    /**
     * Sets if the scratch window should be drawn or not. Cannot be used to force it to draw, only to hide it, which will not stop the panel from working with invisible window.
     *
     * @arg boolean shouldDraw
     */
    function SetShouldDrawScreen(shouldDraw: boolean): void;

    /**
     * Sets the value of the DNumberScratch and triggers DNumberScratch:OnValueChanged
     *
     * @arg number val - The value to set.
     */
    function SetValue(val: number): void;

    /**
     * Sets the zoom level of the scratch panel.
     *
     * @arg number zoom
     */
    function SetZoom(zoom: number): void;

    /**
     * Forces the assigned ConVar to be updated to the value of this DNumberScratch
     */
    function UpdateConVar(): void;
}
