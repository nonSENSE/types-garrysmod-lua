/** @noSelfInFile */
declare namespace DPropertySheet {
    /**
 * Adds a new tab.
* 
* @arg string name - Name of the tab
* @arg Panel pnl - Panel to be used as contents of the tab. This normally should be a DPanel
* @arg string [icon="nil"] - Icon for the tab. This will ideally be a silkicon, but any material name can be used.
* @arg boolean [noStretchX=false] - Should DPropertySheet try to fill itself with given panel horizontally.
* @arg boolean [noStretchY=false] - Should DPropertySheet try to fill itself with given panel vertically.
* @arg string [tooltip="nil"] - Tooltip for the tab when user hovers over it with his cursor
* @return {Panel} A table containing the following keys:

Panel Tab - The created DTab.
string Name - Name of the created tab
Panel Panel - The contents panel of the tab
 */
    function AddSheet(
        name: string,
        pnl: Panel,
        icon?: string,
        noStretchX?: boolean,
        noStretchY?: boolean,
        tooltip?: string
    ): Panel;

    /**
 * Removes tab and/or panel from the parent DPropertySheet.
* 
* @arg Panel tab - The DTab of the sheet from DPropertySheet.
See DPropertySheet:GetItems.
* @arg boolean removePanel - Set to true to remove the associated panel object as well.
* @return {Panel} The panel of the tab.
 */
    function CloseTab(tab: Panel, removePanel: boolean): Panel;

    /**
     * Internal function that handles the cross fade animation when the player switches tabs.
     *
     * @arg object anim
     * @arg number delta
     * @arg object data
     */
    function CrossFade(anim: object, delta: number, data: object): void;

    /**
     * Returns the active DTab of this DPropertySheet.
     *
     * @return {Panel} The DTab
     */
    function GetActiveTab(): Panel;

    /**
     * Returns the amount of time (in seconds) it takes to fade between tabs.
     *
     * @return {number} The amount of time (in seconds) it takes to fade between tabs.
     */
    function GetFadeTime(): number;

    /**
 * Returns a list of all tabs of this DPropertySheet.
* 
* @return {string} A table of tables.
Each table contains 3 key-value pairs:

string Name - The name of the tab.
Panel Tab - The DTab associated with the tab.
Panel Panel - The Panel associated with the tab.
 */
    function GetItems(): string;

    /**
     * Gets the padding from the parent panel to child panels.
     *
     * @return {number} Padding
     */
    function GetPadding(): number;

    /**
     * Returns whatever value was set by DPropertySheet:SetShowIcons.
     *
     * @return {boolean}
     */
    function GetShowIcons(): boolean;

    /**
     * Called when a player switches the tabs.
     *
     * @arg Panel old - The previously active DTab
     * @arg Panel _new - The newly active DTab
     */
    function OnActiveTabChanged(old: Panel, _new: Panel): void;

    /**
 * Sets the active tab of the DPropertySheet.
* 
* @arg Panel tab - The DTab to set active.
See DPropertySheet:GetItems
 */
    function SetActiveTab(tab: Panel): void;

    /**
     * Sets the amount of time (in seconds) it takes to fade between tabs.
     *
     * @arg number [time=0.1] - The amount of time it takes (in seconds) to fade between tabs.
     */
    function SetFadeTime(time?: number): void;

    /**
     * Sets the padding from parent panel to children panel.
     *
     * @arg number [padding=8] - Amount of padding
     */
    function SetPadding(padding?: number): void;

    /**
     * Does nothing.
     *
     * @arg boolean show
     */
    function SetShowIcons(show: boolean): void;

    /**
     * Creates a close button on the right side of the DPropertySheet that will run the given callback function when pressed.
     *
     * @arg GMLua.CallbackNoContext func - Callback function to be called when the close button is pressed.
     */
    function SetupCloseButton(func: GMLua.CallbackNoContext): void;

    /**
     * Sets the width of the DPropertySheet to fit the contents of all of the tabs.
     */
    function SizeToContentWidth(): void;

    /**
     * Switches the active tab to a tab with given name.
     *
     * @arg string name - Case sensitive name of the tab.
     */
    function SwitchToName(name: string): void;
}
