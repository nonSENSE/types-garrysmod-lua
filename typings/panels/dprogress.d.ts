/** @noSelfInFile */
declare namespace DProgress {
    /**
     * Returns the progress bar's fraction. 0 is 0% and 1 is 100%.
     *
     * @return {number} Current fraction of the progress bar.
     */
    function GetFraction(): number;

    /**
     * Sets the fraction of the progress bar. 0 is 0% and 1 is 100%.
     *
     * @arg number fraction - Fraction of the progress bar. Range is 0 to 1 (0% to 100%).
     */
    function SetFraction(fraction: number): void;
}
