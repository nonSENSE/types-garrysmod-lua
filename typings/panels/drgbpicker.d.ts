/** @noSelfInFile */
declare namespace DRGBPicker {
    /**
     * Returns the color at given position on the internal texture.
     *
     * @arg number x - The X coordinate on the texture to get the color from
     * @arg number y - The Y coordinate on the texture to get the color from
     * @return {Color} Color
     * @return {number} The X-coordinate clamped to the texture's width.
     * @return {number} The Y-coordinate clamped to the texture's height.
     */
    function GetPosColor(x: number, y: number): LuaMultiReturn<[Color, number, number]>;

    /**
     * Returns the color currently set on the color picker.
     *
     * @return {Color} The color set on the color picker, see Color.
     */
    function GetRGB(): Color;

    /**
     * Function which is called when the cursor is clicked and/or moved on the color picker. Meant to be overridden.
     *
     * @arg Color col - The color that is selected on the color picker (Color form).
     */
    function OnChange(col: Color): void;

    /**
     * Sets the color stored in the color picker.
     *
     * @arg Color color - The color to set, see Color.
     */
    function SetRGB(color: Color): void;
}
