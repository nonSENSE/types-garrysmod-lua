/** @noSelfInFile */
declare namespace DProperties {
    /**
     * Creates a row in the properties panel.
     *
     * @arg string category - The category to list this row under
     * @arg string name - The label of this row
     * @return {Panel} An internal Row panel.
     */
    function CreateRow(category: string, name: string): Panel;

    /**
     * Returns the DScrollPanel all the properties panels are attached to.
     *
     * @return {Panel} A DScrollPanel canvas
     */
    function GetCanvas(): Panel;

    /**
     * Returns (or creates) a category of properties.
     *
     * @arg string name - Name of the category
     * @arg boolean create - Create a new category if it doesn't exist.
     * @return {Panel} An internal panel.
     */
    function GetCategory(name: string, create: boolean): Panel;
}
