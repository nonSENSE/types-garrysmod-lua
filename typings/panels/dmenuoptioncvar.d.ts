/** @noSelfInFile */
declare namespace DMenuOptionCVar {
    /**
     * Returns the console variable used by the DMenuOptionCVar.
     *
     * @return {string} The console variable used
     */
    function GetConVar(): string;

    /**
     * Returns the value of the console variable when the DMenuOptionCVar is not checked.
     *
     * @return {string} The value
     */
    function GetValueOff(): string;

    /**
     * Return the value of the console variable when the DMenuOptionCVar is checked.
     *
     * @return {string} The value
     */
    function GetValueOn(): string;

    /**
     * Sets the console variable to be used by DMenuOptionCVar.
     *
     * @arg string cvar - The console variable name to set
     */
    function SetConVar(cvar: string): void;

    /**
     * Sets the value of the console variable when the DMenuOptionCVar is not checked.
     *
     * @arg string value - The value
     */
    function SetValueOff(value: string): void;

    /**
     * Sets the value of the console variable when the DMenuOptionCVar is checked.
     *
     * @arg string value - The value
     */
    function SetValueOn(value: string): void;
}
