/** @noSelfInFile */
declare namespace DListView {
    /**
     * Called when a line in the DListView is double clicked.
     *
     * @arg number lineID - The line number of the double clicked line.
     * @arg Panel line - The double clicked DListView_Line.
     */
    function DoDoubleClick(lineID: number, line: Panel): void;

    /**
     * Called when a row is right-clicked
     *
     * @arg number lineID - The line ID of the right clicked line
     * @arg Panel line - The line panel itself, a DListView_Line.
     */
    function OnRowRightClick(lineID: number, line: Panel): void;

    /**
     * Called internally by DListView:OnClickLine when a line is selected. This is the function you should override to define the behavior when a line is selected.
     *
     * @arg number rowIndex - The index of the row/line that the user clicked on.
     * @arg Panel row - The DListView_Line that the user clicked on.
     */
    function OnRowSelected(rowIndex: number, row: Panel): void;

    /**
     * Adds a column to the listview.
     *
     * @arg string column - The name of the column to add.
     * @arg number position - Sets the ordering of this column compared to other columns. Starting index is 1.
     * @return {Panel} The newly created DListView_Column.
     */
    function AddColumn(column: string, position: number): Panel;

    /**
     * Adds a line to the list view.
     *
     * @arg args[] text - Values for a new row in the DListView, If several arguments are supplied, each argument will correspond to a respective column in the DListView.
     * @return {Panel} The newly created DListView_Line.
     */
    function AddLine(...text: any[]): Panel;

    /**
     * Removes all lines that have been added to the DListView.
     */
    function Clear(): void;

    /**
     * Clears the current selection in the DListView.
     */
    function ClearSelection(): void;

    /**
     * Gets the width of a column.
     *
     * @arg number column - The column to get the width of.
     * @return {number} Width of the column.
     */
    function ColumnWidth(column: number): number;

    /**
     * Creates the lines and gets the height of the contents, in a DListView.
     *
     * @return {number} The height of the contents
     */
    function DataLayout(): number;

    /**
     * Removes the scrollbar.
     */
    function DisableScrollbar(): void;

    /**
     * Internal helper function called from the PANEL:PerformLayout of DListView.
     */
    function FixColumnsLayout(): void;

    /**
     * Gets the canvas.
     *
     * @return {Panel} The canvas.
     */
    function GetCanvas(): Panel;

    /**
     * Returns the height of the data of the DListView.
     *
     * @return {number} The height of the data of the DListView.
     */
    function GetDataHeight(): number;

    /**
     * See DListView:SetDirty.
     *
     * @return {boolean}
     */
    function GetDirty(): boolean;

    /**
     * Returns the height of the header of the DListView.
     *
     * @return {number} The height of the header of the DListView.
     */
    function GetHeaderHeight(): number;

    /**
     * Returns whether the header line should be visible on not.
     *
     * @return {boolean} Whether the header line should be visible on not.
     */
    function GetHideHeaders(): boolean;

    /**
     * Returns the height of DListView:GetCanvas.
     *
     * @return {number} The height of DListView:GetCanvas.
     */
    function GetInnerTall(): number;

    /**
     * Gets the DListView_Line at the given index.
     *
     * @arg number id - The index of the line to get.
     * @return {Panel} The DListView_Line at the given index.
     */
    function GetLine(id: number): Panel;

    /**
     * Gets all of the lines added to the DListView.
     *
     * @return {object} The lines added to the DListView.
     */
    function GetLines(): object;

    /**
     * Returns whether multiple lines can be selected or not.
     *
     * @return {boolean} Whether multiple lines can be selected or not.
     */
    function GetMultiSelect(): boolean;

    /**
     * Gets all of the lines that are currently selected.
     *
     * @return {any} A table of DListView_Lines.
     */
    function GetSelected(): any;

    /**
     * Gets the currently selected DListView_Line index.
     *
     * @return {number} The index of the currently selected line.
     * @return {Panel} The currently selected DListView_Line.
     */
    function GetSelectedLine(): LuaMultiReturn<[number, Panel]>;

    /**
     * Returns whether sorting of columns by clicking their headers is allowed or not.
     *
     * @return {boolean} Whether sorting of columns by clicking their headers is allowed or not
     */
    function GetSortable(): boolean;

    /**
     * Converts LineID to SortedID
     *
     * @arg number lineId - The DListView_Line:GetID of a line to look up
     * @return {number}
     */
    function GetSortedID(lineId: number): number;

    /**
     * Called whenever a line is clicked.
     *
     * @arg Panel line - The selected line.
     * @arg boolean isSelected - Boolean indicating whether the line is selected.
     */
    function OnClickLine(line: Panel, isSelected: boolean): void;

    /**
     * Called from DListView_Column.
     *
     * @arg Panel column - The column which initialized the resize
     * @arg number size
     */
    function OnRequestResize(column: Panel, size: number): void;

    /**
     * Removes a line from the list view.
     *
     * @arg number line - Removes the given row, by row id (same number as DListView:GetLine).
     */
    function RemoveLine(line: number): void;

    /**
     * Selects the line at the first index of the DListView if one has been added.
     */
    function SelectFirstItem(): void;

    /**
     * Selects a line in the listview.
     *
     * @arg Panel Line - The line to select.
     */
    function SelectItem(Line: Panel): void;

    /**
     * Sets the height of all lines of the DListView except for the header line.
     *
     * @arg number height - The new height to set. Default value is 17.
     */
    function SetDataHeight(height: number): void;

    /**
     * Used internally to signify if the DListView needs a rebuild.
     *
     * @arg boolean isDirty
     */
    function SetDirty(isDirty: boolean): void;

    /**
     * Sets the height of the header line of the DListView.
     *
     * @arg number height - The new height to set. Default value is 16.
     */
    function SetHeaderHeight(height: number): void;

    /**
     * Sets whether the header line should be visible on not.
     *
     * @arg boolean hide - Whether the header line should be visible on not.
     */
    function SetHideHeaders(hide: boolean): void;

    /**
     * Sets whether multiple lines can be selected by the user by using the ctrl or ⇧ shift keys. When set to false, only one line can be selected.
     *
     * @arg boolean allowMultiSelect - Whether multiple lines can be selected or not
     */
    function SetMultiSelect(allowMultiSelect: boolean): void;

    /**
     * Enables/disables the sorting of columns by clicking.
     *
     * @arg boolean isSortable - Whether sorting columns with clicking is allowed or not.
     */
    function SetSortable(isSortable: boolean): void;

    /**
     * Sorts the items in the specified column.
     *
     * @arg number columnIndex - The index of the column that should be sorted.
     * @arg boolean [descending=false] - Whether the items should be sorted in descending order or not.
     */
    function SortByColumn(columnIndex: number, descending?: boolean): void;

    /**
     * Sorts the list based on given columns.
     *
     * @arg number [column1=NaN]
     * @arg boolean [descrending1=false]
     * @arg number [column2=NaN]
     * @arg boolean [descrending2=false]
     * @arg number [column3=NaN]
     * @arg boolean [descrending3=false]
     * @arg number [column4=NaN]
     * @arg boolean [descrending4=false]
     */
    function SortByColumns(
        column1?: number,
        descrending1?: boolean,
        column2?: number,
        descrending2?: boolean,
        column3?: number,
        descrending3?: boolean,
        column4?: number,
        descrending4?: boolean
    ): void;
}
