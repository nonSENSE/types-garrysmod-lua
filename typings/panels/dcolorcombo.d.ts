/** @noSelfInFile */
declare namespace DColorCombo {
    /**
     * Called internally to create panels necessary for this panel to work.
     */
    function BuildControls(): void;

    /**
     * Returns the color of the DColorCombo.
     *
     * @return {Color} A Color
     */
    function GetColor(): Color;

    /**
     * Returns true if the panel is currently being edited
     *
     * @return {boolean}
     */
    function IsEditing(): boolean;

    /**
     * Called when the value (color) of this panel was changed.
     *
     * @arg object newcol
     */
    function OnValueChanged(newcol: object): void;

    /**
     * Sets the color of this panel.
     *
     * @arg Color clr - A Color.
     */
    function SetColor(clr: Color): void;
}
