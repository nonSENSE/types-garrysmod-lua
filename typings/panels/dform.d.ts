/** @noSelfInFile */
declare namespace DForm {
    /**
     * Adds one or two items to the DForm.
     * If this method is called with only one argument, it is added to the bottom of the form. If two arguments are passed, they are placed side-by-side at the bottom of the form.
     *
     * @arg Panel left - Left-hand element to add to the DForm.
     * @arg Panel [right=nil] - Right-hand element to add to the DForm.
     */
    function AddItem(left: Panel, right?: Panel): void;

    /**
     * Adds a DButton onto the DForm
     *
     * @arg string text - The text on the button
     * @arg string concommand - The concommand to run when the button is clicked
     * @arg args[] args - The arguments to pass on to the concommand when the button is clicked
     * @return {Panel} The created DButton
     */
    function Button(text: string, concommand: string, ...args: any[]): Panel;

    /**
     * Adds a DCheckBoxLabel onto the DForm.
     *
     * @arg string label - The label to be set next to the check box
     * @arg string convar - The console variable to change when this is changed
     * @return {Panel} The created DCheckBoxLabel
     */
    function CheckBox(label: string, convar: string): Panel;

    /**
     * Adds a DComboBox onto the DForm
     *
     * @arg string title - Text to the left of the combo box
     * @arg string convar - Console variable to change when the user selects something from the dropdown.
     * @return {Panel} The created DComboBox
     * @return {Panel} The created DLabel
     */
    function ComboBox(title: string, convar: string): LuaMultiReturn<[Panel, Panel]>;

    /**
     * Adds a DLabel onto the DForm. Unlike DForm:Help, this is indented and is colored blue, depending on the derma skin.
     *
     * @arg string help - The help message to be displayed.
     * @return {Panel} The created DLabel
     */
    function ControlHelp(help: string): Panel;

    /**
     * Adds a DLabel onto the DForm as a helper
     *
     * @arg string help - The help message to be displayed
     * @return {Panel} The created DLabel
     */
    function Help(help: string): Panel;

    /**
     * Adds a DListBox onto the DForm
     *
     * @arg string label - The label to set on the DListBox
     * @return {Panel} The created DListBox
     * @return {Panel} The created DLabel
     */
    function ListBox(label: string): LuaMultiReturn<[Panel, Panel]>;

    /**
     * Adds a DNumberWang onto the DForm
     *
     * @arg string label - The label to be placed next to the DNumberWang
     * @arg string convar - The console variable to change when the slider is changed
     * @arg number min - The minimum value of the slider
     * @arg number max - The maximum value of the slider
     * @arg number [decimals=NaN] - The number of decimals to allow in the slider (Optional)
     * @return {Panel} The created DNumberWang
     * @return {Panel} The created DLabel
     */
    function NumberWang(
        label: string,
        convar: string,
        min: number,
        max: number,
        decimals?: number
    ): LuaMultiReturn<[Panel, Panel]>;

    /**
     * Adds a DNumSlider onto the DForm
     *
     * @arg string label - The label of the DNumSlider
     * @arg string convar - The console variable to change when the slider is changed
     * @arg number min - The minimum value of the slider
     * @arg number max - The maximum value of the slider
     * @arg number [decimals=NaN] - The number of decimals to allow on the slider. (Optional)
     * @return {Panel} The created DNumSlider
     */
    function NumSlider(label: string, convar: string, min: number, max: number, decimals?: number): Panel;

    /**
     * Creates a DPanelSelect and docks it to the top of the DForm.
     *
     * @return {Panel} The created DPanelSelect.
     */
    function PanelSelect(): Panel;

    /**
 * Creates a PropSelect panel and docks it to the top of the DForm.
* 
* @arg string label - The label to display above the prop select.
* @arg string convar - The convar to set the selected model to.
* @arg object models - A table of models to display for selection. Supports 2 formats:

Key is the model and value are the convars to set when that model is selected in format convar=value
An table of tables where each table must have the following keys:


string model - The model.
number skin - Model's skin. Defaults to 0
string tooltip - Displayed when user hovers over the model. Defaults to the model path.
The key of the table is the value of the convar.
* @arg number [height=2] - The height of the prop select panel, in 64px icon increments.
* @return {Panel} The created PropSelect panel.
 */
    function PropSelect(label: string, convar: string, models: object, height?: number): Panel;

    /**
     * Does nothing.
     */
    function Rebuild(): void;

    /**
     * Sets the title (header) name of the DForm. This is Label until set.
     *
     * @arg string name - The new header name.
     */
    function SetName(name: string): void;

    /**
     * Adds a DTextEntry to a DForm
     *
     * @arg string label - The label to be next to the text entry
     * @arg string convar - The console variable to be changed when the text entry is changed
     * @return {Panel} The created DTextEntry
     * @return {Panel} The created DLabel
     */
    function TextEntry(label: string, convar: string): LuaMultiReturn<[Panel, Panel]>;
}
