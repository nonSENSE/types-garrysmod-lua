/** @noSelfInFile */
declare namespace DVerticalDivider {
    /**
     * Returns the bottom content panel of the DVerticalDivider.
     *
     * @return {Panel} The bottom content panel.
     */
    function GetBottom(): Panel;

    /**
     * Returns the minimum height of the bottom content panel.
     *
     * @return {number} The minimum height of the bottom content panel.
     */
    function GetBottomMin(): number;

    /**
     * Returns the height of the divider bar between the top and bottom content panels of the DVerticalDivider.
     *
     * @return {number} The height of the divider bar.
     */
    function GetDividerHeight(): number;

    /**
     * Returns whether the divider is being dragged or not.
     *
     * @return {boolean} If true, mouse movement will alter the size of the divider.
     */
    function GetDragging(): boolean;

    /**
     * Returns the local Y position of where the user starts dragging the divider.
     *
     * @return {number} The local Y position where divider dragging has started.
     */
    function GetHoldPos(): number;

    /**
     * Returns the middle content panel of the DVerticalDivider.
     *
     * @return {Panel} The middle content panel.
     */
    function GetMiddle(): Panel;

    /**
     * Returns the top content panel of the DVerticalDivider.
     *
     * @return {Panel} The top content panel.
     */
    function GetTop(): Panel;

    /**
     * Returns the current height of the top content panel set by DVerticalDivider:SetTopHeight or by the user.
     *
     * @return {number} The current height of the DVerticalDivider.
     */
    function GetTopHeight(): number;

    /**
     * Returns the maximum height of the top content panel. See DVerticalDivider:SetTopMax for more information.
     *
     * @return {number} The maximum height of the top content panel.
     */
    function GetTopMax(): number;

    /**
     * Returns the minimum height of the top content panel.
     *
     * @return {number} The minimum height of the top content panel.
     */
    function GetTopMin(): number;

    /**
     * Sets the passed panel as the bottom content of the DVerticalDivider.
     *
     * @arg Panel pnl - The panel to set as the bottom content.
     */
    function SetBottom(pnl: Panel): void;

    /**
     * Sets the minimum height of the bottom content panel.
     *
     * @arg number height - The minimum height of the bottom content panel. Default is 50.
     */
    function SetBottomMin(height: number): void;

    /**
     * Sets the height of the divider bar between the top and bottom content panels of the DVerticalDivider.
     *
     * @arg number height - The height of the divider bar.
     */
    function SetDividerHeight(height: number): void;

    /**
     * Sets whether the divider is being dragged or not.
     *
     * @arg boolean isDragging - Setting this to true causes cursor movement to alter the position of the divider.
     */
    function SetDragging(isDragging: boolean): void;

    /**
     * Sets the local Y position of where the user starts dragging the divider.
     *
     * @arg number y - The local Y position where divider dragging has started.
     */
    function SetHoldPos(y: number): void;

    /**
     * Places the passed panel in between the top and bottom content panels of the DVerticalDivider.
     *
     * @arg Panel pnl - The panel to set as the middle content.
     */
    function SetMiddle(pnl: Panel): void;

    /**
     * Sets the passed panel as the top content of the DVerticalDivider.
     *
     * @arg Panel pnl - The panel to set as the top content.
     */
    function SetTop(pnl: Panel): void;

    /**
     * Sets the height of the top content panel.
     *
     * @arg number height - The height of the top content panel.
     */
    function SetTopHeight(height: number): void;

    /**
     * Sets the maximum height of the top content panel. This is ignored if the panel would exceed the minimum bottom content panel height set from DVerticalDivider:SetBottomMin.
     *
     * @arg number height - The maximum height of the top content panel. Default is 4096.
     */
    function SetTopMax(height: number): void;

    /**
     * Sets the minimum height of the top content panel.
     *
     * @arg number height - The minimum height of the top content panel. Default is 50.
     */
    function SetTopMin(height: number): void;

    /**
     * Causes the user to start dragging the divider.
     */
    function StartGrab(): void;
}
