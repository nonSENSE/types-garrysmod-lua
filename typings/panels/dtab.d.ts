/** @noSelfInFile */
declare namespace DTab {
    /**
     * Returns the panel that the tab represents.
     *
     * @return {Panel} Panel added to the sheet using DPropertySheet:AddSheet.
     */
    function GetPanel(): Panel;

    /**
     * Returns whether the tab is the currently selected tab of the associated DPropertySheet.
     *
     * @return {any} Currently selected tab.
     */
    function IsActive(): any;
}
