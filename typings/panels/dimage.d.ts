/** @noSelfInFile */
declare namespace DImage {
    /**
     * Actually loads the IMaterial to render it. Called from DImage:LoadMaterial.
     */
    function DoLoadMaterial(): void;

    /**
     * "Fixes" the current material of the DImage if it has VertexLit shader by creating a new one with the same name and a prefix of "_DImage" and automatically calling DImage:SetMaterial with the new material.
     */
    function FixVertexLitMaterial(): void;

    /**
     * Returns the texture path set by DImage:SetFailsafeMatName.
     *
     * @return {string}
     */
    function GetFailsafeMatName(): string;

    /**
     * Returns the image loaded in the image panel.
     *
     * @return {string} The path to the image that is loaded.
     */
    function GetImage(): string;

    /**
     * Returns the color override of the image panel.
     *
     * @return {Color} The color override of the image. Uses the Color.
     */
    function GetImageColor(): Color;

    /**
     * Returns whether the DImage should keep the aspect ratio of its image when being resized.
     *
     * @return {boolean} Whether the DImage should keep the aspect ratio of its image when being resized.
     */
    function GetKeepAspect(): boolean;

    /**
     * Returns the current Material of the DImage.
     *
     * @return {IMaterial}
     */
    function GetMaterial(): IMaterial;

    /**
     * Returns the texture path set by DImage:SetMatName.
     *
     * @return {string}
     */
    function GetMatName(): string;

    /**
     * Initializes the loading process of the material to render if it is not loaded yet.
     */
    function LoadMaterial(): void;

    /**
     * Paints a ghost copy of the DImage panel at the given position and dimensions. This function overrides Panel:PaintAt.
     *
     * @arg number posX - The x coordinate to draw the panel from.
     * @arg number posY - The y coordinate to draw the panel from.
     * @arg number width - The width of the panel image to be drawn.
     * @arg number height - The height of the panel image to be drawn.
     */
    function PaintAt(posX: number, posY: number, width: number, height: number): void;

    /**
     * Sets the backup material to be loaded when the image is first rendered. Used by DImage:SetOnViewMaterial.
     *
     * @arg string backupMat
     */
    function SetFailsafeMatName(backupMat: string): void;

    /**
     * Sets the image to load into the frame. If the first image can't be loaded and strBackup is set, that image will be loaded instead.
     *
     * @arg string strImage - The path of the image to load. When no file extension is supplied the VMT file extension is used.
     * @arg string [strBackup="nil"] - The path of the backup image.
     */
    function SetImage(strImage: string, strBackup?: string): void;

    /**
     * Sets the image's color override.
     *
     * @arg Color col - The color override of the image. Uses the Color.
     */
    function SetImageColor(col: Color): void;

    /**
     * Sets whether the DImage should keep the aspect ratio of its image when being resized.
     *
     * @arg boolean keep - true to keep the aspect ratio, false not to
     */
    function SetKeepAspect(keep: boolean): void;

    /**
     * Sets a Material directly as an image.
     *
     * @arg IMaterial mat - The material to set
     */
    function SetMaterial(mat: IMaterial): void;

    /**
     * Sets the material to be loaded when the image is first rendered. Used by DImage:SetOnViewMaterial.
     *
     * @arg string mat
     */
    function SetMatName(mat: string): void;

    /**
     * Similar to DImage:SetImage, but will only do the expensive part of actually loading the textures/material if the material is about to be rendered/viewed.
     *
     * @arg string mat
     * @arg string backupMat
     */
    function SetOnViewMaterial(mat: string, backupMat: string): void;

    /**
     * Returns true if the image is not yet loaded.
     *
     * @return {boolean}
     */
    function Unloaded(): boolean;
}
