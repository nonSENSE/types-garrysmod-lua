/** @noSelfInFile */
declare namespace DTileLayout {
    /**
     * Clears the panel's tile table. Used by DTileLayout:LayoutTiles.
     */
    function ClearTiles(): void;

    /**
     * Called to designate a range of tiles as occupied by a panel.
     *
     * @arg number x - The x coordinate of the top-left corner of the panel.
     * @arg number y - The y coordinate of the top-left corner of the panel.
     * @arg number w - The panel's width.
     * @arg number h - The panel's height.
     */
    function ConsumeTiles(x: number, y: number, w: number, h: number): void;

    /**
     * Creates and returns an exact copy of the DTileLayout.
     *
     * @return {Panel} The created copy.
     */
    function Copy(): Panel;

    /**
     * Creates copies of all the children from the given panel object and parents them to this one.
     *
     * @arg Panel source - The source panel from which to copy all children.
     */
    function CopyContents(source: Panel): void;

    /**
     * Finds the coordinates of the first group of free tiles that fit the given size.
     *
     * @arg number x - The x coordinate to start looking from.
     * @arg number y - The y coordinate to start looking from.
     * @arg number w - The needed width.
     * @arg number h - The needed height.
     * @return {number} The x coordinate of the found available space.
     * @return {number} The y coordinate of the found available space.
     */
    function FindFreeTile(x: number, y: number, w: number, h: number): LuaMultiReturn<[number, number]>;

    /**
     * Determines if a group of tiles is vacant.
     *
     * @arg number x - The x coordinate of the first tile.
     * @arg number y - The y coordinate of the first tile.
     * @arg number w - The width needed.
     * @arg number h - The height needed.
     * @return {boolean} Whether or not this group is available for occupation.
     */
    function FitsInTile(x: number, y: number, w: number, h: number): boolean;

    /**
     * Returns the size of each single tile, set with DTileLayout:SetBaseSize.
     *
     * @return {number} Base tile size.
     */
    function GetBaseSize(): number;

    /**
     * Returns the border spacing set by DTileLayout:SetBorder.
     *
     * @return {number}
     */
    function GetBorder(): number;

    /**
     * Returns the minimum height the DTileLayout can resize to.
     *
     * @return {number} The minimum height the panel can shrink to.
     */
    function GetMinHeight(): number;

    /**
     * Returns the X axis spacing between 2 elements set by DTileLayout:SetSpaceX.
     *
     * @return {number}
     */
    function GetSpaceX(): number;

    /**
     * Returns the Y axis spacing between 2 elements set by DTileLayout:SetSpaceY.
     *
     * @return {number}
     */
    function GetSpaceY(): number;

    /**
     * Gets the occupied state of a tile.
     *
     * @arg number x - The x coordinate of the tile.
     * @arg number y - The y coordinate of the tile.
     * @return {any} The occupied state of the tile, normally 1 or nil.
     */
    function GetTile(x: number, y: number): any;

    /**
     * Resets the last width/height info, and invalidates the panel's layout, causing it to recalculate all child positions. It is called whenever a child is added or removed, and can be called to refresh the panel.
     */
    function Layout(): void;

    /**
     * Called by PANEL:PerformLayout to arrange and lay out the child panels, if it has changed in size.
     */
    function LayoutTiles(): void;

    /**
     * Called when anything is dropped on or rearranged within the DTileLayout.
     */
    function OnModified(): void;

    /**
     * Sets the size of a single tile. If a child panel is larger than this size, it will occupy several tiles.
     *
     * @arg number size - The size of each tile. It is recommended you use 2ⁿ (16, 32, 64...) numbers, and those above 4, as numbers lower than this will result in many tiles being processed and therefore slow operation.
     */
    function SetBaseSize(size: number): void;

    /**
     * Sets the spacing between the border/edge of the DTileLayout and all the elements inside.
     *
     * @arg number border
     */
    function SetBorder(border: number): void;

    /**
     * Determines the minimum height the DTileLayout will resize to. This is useful if child panels will be added/removed often.
     *
     * @arg number minH - The minimum height the panel can shrink to.
     */
    function SetMinHeight(minH: number): void;

    /**
     * Sets the spacing between 2 elements in the DTileLayout on the X axis.
     *
     * @arg number spacingX
     */
    function SetSpaceX(spacingX: number): void;

    /**
     * Sets the spacing between 2 elements in the DTileLayout on the Y axis.
     *
     * @arg number spaceY
     */
    function SetSpaceY(spaceY: number): void;

    /**
     * Called to set the occupied state of a tile.
     *
     * @arg number x - The x coordinate of the tile.
     * @arg number y - The y coordinate of the tile.
     * @arg any state - The new state of the tile, normally 1 or nil.
     */
    function SetTile(x: number, y: number, state: any): void;
}
