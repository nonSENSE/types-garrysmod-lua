/** @noSelfInFile */
declare namespace DSlider {
    /**
     * Identical to DSlider:IsEditing
     *
     * @return {boolean}
     */
    function GetDragging(): boolean;

    /**
     * Returns the draggable panel's lock on the X axis.
     *
     * @return {number}
     */
    function GetLockX(): number;

    /**
     * Returns the draggable panel's lock on the Y axis.
     *
     * @return {number}
     */
    function GetLockY(): number;

    /**
     * Returns the current notch color, set by DSlider:SetNotchColor
     *
     * @return {Color} The current color
     */
    function GetNotchColor(): Color;

    /**
     * Appears to be non functioning, however is still used by panels such as DNumSlider.
     *
     * @return {number}
     */
    function GetNotches(): number;

    /**
     * Does nothing.
     *
     * @return {any}
     */
    function GetNumSlider(): any;

    /**
     * Returns the target position of the draggable "knob" panel of the slider on the X axis.
     *
     * @return {number} The value range seems to be from 0 to 1
     */
    function GetSlideX(): number;

    /**
     * Returns the target position of the draggable "knob" panel of the slider on the Y axis.
     *
     * @return {number} The value range seems to be from 0 to 1
     */
    function GetSlideY(): number;

    /**
     * Appears to be non functioning, however is still used by panels such as DNumSlider.
     *
     * @return {boolean}
     */
    function GetTrapInside(): boolean;

    /**
     * Returns true if this element is being edited by the player.
     *
     * @return {boolean}
     */
    function IsEditing(): boolean;

    /**
     * Sets the background for the slider.
     *
     * @arg string path - Path to the image.
     */
    function SetBackground(path: string): void;

    /**
     * Sets whether or not the slider is being dragged.
     *
     * @arg boolean dragging
     */
    function SetDragging(dragging: boolean): void;

    /**
     * Does nothing.
     */
    function SetImage(): void;

    /**
     * Does nothing.
     */
    function SetImageColor(): void;

    /**
 * Sets the lock on the X axis.
* 
* @arg number [lockX=NaN] - Set to nil to reset lock.
The value range is from 0 to 1.
 */
    function SetLockX(lockX?: number): void;

    /**
 * Sets the lock on the Y axis.
* 
* @arg number [lockY=NaN] - Set to nil to reset lock.
The value range is from 0 to 1.
 */
    function SetLockY(lockY?: number): void;

    /**
     * Sets the current notch color, overriding the color set by the derma skin.
     *
     * @arg Color clr - The new color to set
     */
    function SetNotchColor(clr: Color): void;

    /**
     * Appears to be non functioning, however is still used by panels such as DNumSlider.
     *
     * @arg number notches
     */
    function SetNotches(notches: number): void;

    /**
     * Does nothing.
     *
     * @arg any slider
     */
    function SetNumSlider(slider: any): void;

    /**
     * Used to position the draggable panel of the slider on the X axis.
     *
     * @arg number x - The value range seems to be from 0 to 1
     */
    function SetSlideX(x: number): void;

    /**
     * Used to position the draggable panel of the slider on the Y axis.
     *
     * @arg number y - The value range seems to be from 0 to 1
     */
    function SetSlideY(y: number): void;

    /**
     * Appears to be non functioning, however is still used by panels such as DNumSlider.
     *
     * @arg boolean trap
     */
    function SetTrapInside(trap: boolean): void;

    /**
     * For override by child panels, such as DNumSlider.
     *
     * @arg number x
     * @arg number y
     * @return {number} x
     * @return {number} y
     */
    function TranslateValues(x: number, y: number): LuaMultiReturn<[number, number]>;
}
