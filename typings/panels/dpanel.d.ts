/** @noSelfInFile */
declare namespace DPanel {
    /**
     * Returns the panel's background color.
     *
     * @return {object} Color of the panel's background.
     */
    function GetBackgroundColor(): object;

    /**
     * Returns whether or not the panel is disabled.
     *
     * @return {boolean} True if the panel is disabled (mouse input disabled and background alpha set to 75), false if its enabled (mouse input enabled and background alpha set to 255).
     */
    function GetDisabled(): boolean;

    /**
     * Returns whether or not the panel background is being drawn. Alias of DPanel:GetPaintBackground.
     *
     * @return {boolean} True if the panel background is drawn, false otherwise.
     */
    function GetDrawBackground(): boolean;

    /**
     * Returns whether or not the panel background is being drawn.
     *
     * @return {boolean} True if the panel background is drawn, false otherwise.
     */
    function GetPaintBackground(): boolean;

    /**
     * Sets the background color of the panel.
     *
     * @arg object color - The background color.
     */
    function SetBackgroundColor(color: object): void;

    /**
     * Sets whether or not to disable the panel.
     *
     * @arg boolean disabled - True to disable the panel (mouse input disabled and background alpha set to 75), false to enable it (mouse input enabled and background alpha set to 255).
     */
    function SetDisabled(disabled: boolean): void;

    /**
     * Sets whether or not to draw the panel background. Alias of DPanel:SetPaintBackground.
     *
     * @arg boolean draw - True to show the panel's background, false to hide it.
     */
    function SetDrawBackground(draw: boolean): void;

    /**
     * Sets whether or not to paint/draw the panel background.
     *
     * @arg boolean paint - True to show the panel's background, false to hide it.
     */
    function SetPaintBackground(paint: boolean): void;
}
