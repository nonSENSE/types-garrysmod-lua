/** @noSelfInFile */
declare namespace ContextBase {
    /**
 * Called by spawnmenu functions (when creating a context menu) to fill this control with data.
* 
* @arg object contextData - A two-membered table:

string convar - The console variable to use. Calls ContextBase:SetConVar.
string label - The text to display inside the control's label.
 */
    function ControlValues(contextData: object): void;

    /**
     * Returns the ConVar for the panel to change/handle, set by ContextBase:SetConVar
     *
     * @return {string} The ConVar for the panel to change.
     */
    function ConVar(): string;

    /**
     * Sets the ConVar for the panel to change/handle.
     *
     * @arg string cvar - The ConVar for the panel to change.
     */
    function SetConVar(cvar: string): void;

    /**
     * You should override this function and use it to check whether your convar value changed.
     */
    function TestForChanges(): void;
}
