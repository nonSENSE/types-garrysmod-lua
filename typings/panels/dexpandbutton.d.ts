/** @noSelfInFile */
declare namespace DExpandButton {
    /**
     * Returns whether this DExpandButton is expanded or not.
     *
     * @return {boolean} True if expanded, false otherwise
     */
    function GetExpanded(): boolean;

    /**
     * Sets whether this DExpandButton should be expanded or not. Only changes appearance.
     *
     * @arg boolean expanded - True to expand ( visually will show a "-" )
     */
    function SetExpanded(expanded: boolean): void;
}
