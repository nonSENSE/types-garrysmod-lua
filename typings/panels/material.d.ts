/** @noSelfInFile */
declare namespace Material {
    /**
     * Sets the alpha value of the Material panel.
     *
     * @arg number alpha - The alpha value, from 0 to 255.
     */
    function SetAlpha(alpha: number): void;

    /**
     * Sets the material used by the panel.
     *
     * @arg string matname - The file path of the material to set (relative to "garrysmod/materials/").
     */
    function SetMaterial(matname: string): void;
}
