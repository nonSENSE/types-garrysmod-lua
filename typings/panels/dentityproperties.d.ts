/** @noSelfInFile */
declare namespace DEntityProperties {
    /**
     * Called internally by DEntityProperties:RebuildControls.
     *
     * @arg string varname
     * @arg object editdata
     */
    function EditVariable(varname: string, editdata: object): void;

    /**
     * Called internally when an entity being edited became invalid.
     */
    function EntityLost(): void;

    /**
     * Called when we were editing an entity and then it became invalid (probably removed)
     */
    function OnEntityLost(): void;

    /**
     * Called internally by DEntityProperties:SetEntity to rebuild the controls.
     */
    function RebuildControls(): void;

    /**
     * Sets the entity to be edited by this panel. The entity must support the Editable Entities system or nothing will happen.
     *
     * @arg Entity ent - The entity to edit
     */
    function SetEntity(ent: Entity): void;
}
