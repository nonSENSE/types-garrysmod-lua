/** @noSelfInFile */
declare namespace DTree_Node {
    /**
     * A helper function that adds a new node and calls to DTree_Node:MakeFolder on it.
     *
     * @arg string name - The name of the new node
     * @arg string folder - The folder in the filesystem to use, relative to the garrysmod/ folder.
     * @arg string path - The path to search in. See File Search Paths
     * @arg boolean [showFiles=false] - Should files be added as nodes (true) or folders only (false)
     * @arg string [wildcard="*"] - The wildcard to use when searching for files.
     * @arg boolean [bDontForceExpandable=false]
     * @return {Panel} The created DTree_Node
     */
    function AddFolder(
        name: string,
        folder: string,
        path: string,
        showFiles?: boolean,
        wildcard?: string,
        bDontForceExpandable?: boolean
    ): Panel;

    /**
     * Add a child node to the DTree_Node
     *
     * @arg string name - Name of the node.
     * @arg string [icon="icon16/folder.png"] - The icon that will show next to the node in the DTree.
     * @return {Panel} Returns the created DTree_Node panel.
     */
    function AddNode(name: string, icon?: string): Panel;

    /**
     * Adds the given panel to the child nodes list, a DListLayout.
     *
     * @arg Panel pnl - The panel to add.
     */
    function AddPanel(pnl: Panel): void;

    /**
     * Internal function that handles the expand/collapse animations.
     *
     * @arg object anim
     * @arg number delta
     * @arg object data
     */
    function AnimSlide(anim: object, delta: number, data: object): void;

    /**
     * Called when a child node is expanded or collapsed to propagate this event to parent nodes to update layout.
     *
     * @arg boolean expanded
     */
    function ChildExpanded(expanded: boolean): void;

    /**
     * Cleans up the internal table of items (sub-nodes) of this node from invalid panels or sub-nodes that were moved from this node to another.
     */
    function CleanList(): void;

    /**
     * Create and returns a copy of this node, including all the sub-nodes.
     *
     * @return {Panel} The copied DTree_Node.
     */
    function Copy(): Panel;

    /**
     * Creates the container DListLayout for the DTree_Nodes.
     */
    function CreateChildNodes(): void;

    /**
     * Called automatically to update the status of DTree_Node:GetLastChild on children of this node.
     */
    function DoChildrenOrder(): void;

    /**
     * Called when the node is clicked.
     *
     * @return {boolean} Return true to prevent DoClick from being called on parent nodes or the DTree itself.
     */
    function DoClick(): boolean;

    /**
     * Called when the node is right clicked.
     *
     * @return {boolean} Return true to prevent DoRightClick from being called on parent nodes or the DTree itself.
     */
    function DoRightClick(): boolean;

    /**
     * Expands or collapses this node, as well as ALL child nodes of this node.
     *
     * @arg boolean expand - Whether to expand (true) or collapse (false)
     */
    function ExpandRecurse(expand: boolean): void;

    /**
     * Collapses or expands all nodes from the topmost-level node to this one.
     *
     * @arg boolean expand - Whether to expand (true) or collapse (false)
     */
    function ExpandTo(expand: boolean): void;

    /**
     * Called automatically from DTree_Node:PopulateChildrenAndSelf and DTree_Node:PopulateChildren to populate this node with child nodes of files and folders.
     *
     * @arg boolean bAndChildren - Does nothing. Set to true if called from DTree_Node:PopulateChildren.
     * @arg boolean bExpand - Expand self once population process is finished.
     */
    function FilePopulate(bAndChildren: boolean, bExpand: boolean): void;

    /**
     * Called automatically from DTree_Node:FilePopulate to actually fill the node with sub-nodes based on set preferences like should files be added, etc.
     *
     * @arg object files - A list of files in this folder
     * @arg object folders - A list of folder in this folder.
     * @arg string foldername - The folder name/path this node represents
     * @arg string path - The Path ID search was performed with. See File Search Paths
     * @arg boolean bAndChildren - Inherited from the FilePopulate call. Does nothing
     * @arg string wildcard - The wildcard that was given
     */
    function FilePopulateCallback(
        files: object,
        folders: object,
        foldername: string,
        path: string,
        bAndChildren: boolean,
        wildcard: string
    ): void;

    /**
     * Returns n-th child node.
     *
     * @arg number num - The number of the child to get, starting with 0
     * @return {Panel} The child panel, if valid ID is given
     */
    function GetChildNode(num: number): Panel;

    /**
     * Returns the number of child nodes this node has. For use with DTree_Node:GetChildNode
     *
     * @return {number} Number of child nodes.
     */
    function GetChildNodeCount(): number;

    /**
     * Returns a table containing all child nodes of this node.
     *
     * @return {object} A list of all child nodes.
     */
    function GetChildNodes(): object;

    /**
     * Returns value set by DTree_Node:SetDirty.
     *
     * @return {boolean}
     */
    function GetDirty(): boolean;

    /**
     * Returns whether the double clock to collapse/expand functionality is enabled on this node.
     *
     * @return {boolean}
     */
    function GetDoubleClickToOpen(): boolean;

    /**
     * Returns what is set by DTree_Node:SetDraggableName.
     *
     * @return {string}
     */
    function GetDraggableName(): string;

    /**
     * Returns whether or not this node is drawing lines
     *
     * @return {boolean}
     */
    function GetDrawLines(): boolean;

    /**
     * Returns whether the node is expanded or not.
     *
     * @return {boolean} Expanded or not.
     */
    function GetExpanded(): boolean;

    /**
     * Returns the filepath of the file attached to this node.
     *
     * @return {string}
     */
    function GetFileName(): string;

    /**
     * Returns the folder path to search in, set by DTree_Node:MakeFolder.
     *
     * @return {string} The folder path.
     */
    function GetFolder(): string;

    /**
     * Returns whether the expand/collapse button is shown on this node regardless of whether or not it has sub-nodes.
     *
     * @return {boolean}
     */
    function GetForceShowExpander(): boolean;

    /**
     * Returns whether the expand button (little + button) should be shown or hidden.
     *
     * @return {boolean}
     */
    function GetHideExpander(): boolean;

    /**
     * Returns the image path to the icon of this node.
     *
     * @return {string} The path to the image
     */
    function GetIcon(): string;

    /**
     * Returns the indentation level of the DTree this node belongs to.
     *
     * @return {number} The indentation level.
     */
    function GetIndentSize(): number;

    /**
     * Returns whether this node is the last child on this level or not.
     *
     * @return {boolean}
     */
    function GetLastChild(): boolean;

    /**
     * The height of a single DTree_Node of the DTree this node belongs to.
     *
     * @return {number} The height of a single DTree_Node.
     */
    function GetLineHeight(): number;

    /**
     *
     *
     * @return {boolean}
     */
    function GetNeedsChildSearch(): boolean;

    /**
     * Returns whether or not the node is set to be populated from the filesystem.
     *
     * @return {boolean}
     */
    function GetNeedsPopulating(): boolean;

    /**
     * Returns the parent DTree_Node. Note that Panel:GetParent will not be the same!
     *
     * @return {Panel} The parent node.
     */
    function GetParentNode(): Panel;

    /**
     * Returns the path ID (File Search Paths) used in populating the DTree from the filesystem.
     *
     * @return {string} The Path ID
     */
    function GetPathID(): string;

    /**
     * Returns the root node, the DTree this node is under.
     *
     * @return {Panel} The root node
     */
    function GetRoot(): Panel;

    /**
     * Returns whether or not nodes for files should/will be added when populating the node from filesystem.
     *
     * @return {boolean}
     */
    function GetShowFiles(): boolean;

    /**
     * Returns the wildcard set by DTree_Node:MakeFolder.
     *
     * @return {string} The search wildcard
     */
    function GetWildCard(): string;

    /**
     * Inserts a sub-node into this node before or after the given node.
     *
     * @arg Panel node - The DTree_Node to insert.
     * @arg Panel nodeNextTo - The node to insert the node above before or after.
     * @arg boolean before - true to insert before, false to insert after.
     */
    function Insert(node: Panel, nodeNextTo: Panel, before: boolean): void;

    /**
     * Inserts an existing node as a "child" or a sub-node of this node.
     * Used internally by the drag'n'drop functionality.
     *
     * @arg Panel node - Has to be DTree_Node
     */
    function InsertNode(node: Panel): void;

    /**
     * Called automatically internally.
     *
     * @arg Panel node - The DTree_Node.
     */
    function InstallDraggable(node: Panel): void;

    /**
     * See DTree_Node:DoClick
     */
    function InternalDoClick(): void;

    /**
     * See DTree_Node:DoRightClick.
     */
    function InternalDoRightClick(): void;

    /**
     * Returns true if DTree_Node:GetRoot is the same as DTree_Node:GetParentNode of this node.
     *
     * @return {boolean} If this is a root node.
     */
    function IsRootNode(): boolean;

    /**
     * Removes given node as a sub-node of this node.
     *
     * @arg Panel pnl - The node to remove
     */
    function LeaveTree(pnl: Panel): void;

    /**
     * Makes this node a folder in the filesystem. This will make it automatically populated.
     *
     * @arg string folder - The folder in the filesystem to use, relative to the garrysmod/ folder.
     * @arg string path - The path to search in. See File Search Paths
     * @arg boolean [showFiles=false] - Should files be added as nodes (true) or folders only (false)
     * @arg string [wildcard="*"] - The wildcard to use when searching for files.
     * @arg boolean [dontForceExpandable=false] - If set to true, don't show the expand buttons on empty nodes.
     */
    function MakeFolder(
        folder: string,
        path: string,
        showFiles?: boolean,
        wildcard?: string,
        dontForceExpandable?: boolean
    ): void;

    /**
     * Moves given panel to the top of the children of this node.
     *
     * @arg Panel node - The node to move.
     */
    function MoveChildTo(node: Panel): void;

    /**
     * Moves this node to the top of the level.
     */
    function MoveToTop(): void;

    /**
     * Called when sub-nodes of this DTree_Node were changed, such as being rearranged if that functionality is enabled.
     */
    function OnModified(): void;

    /**
     * Called when a new sub-node is added this node.
     *
     * @arg any newNode - The newly added sub node.
     */
    function OnNodeAdded(newNode: any): void;

    /**
     * Called when this or a sub node is selected. Do not use this, it is not for override.
     *
     * @arg Panel node
     */
    function OnNodeSelected(node: Panel): void;

    /**
     * Called automatically to perform layout on this node if this node DTree_Node:IsRootNode.
     */
    function PerformRootNodeLayout(): void;

    /**
     * Called automatically from DTree_Node:PopulateChildrenAndSelf.
     */
    function PopulateChildren(): void;

    /**
     * Called automatically from DTree_Node:SetExpanded (or when user manually expands the node) to populate the node with sub-nodes from the filesystem if this was enabled via DTree_Node:MakeFolder.
     *
     * @arg boolean expand - Expand self once population process is finished.
     */
    function PopulateChildrenAndSelf(expand: boolean): void;

    /**
     * Appears to have no effect on the DTree_Node.
     *
     * @arg boolean dirty
     */
    function SetDirty(dirty: boolean): void;

    /**
     * Sets whether double clicking the node should expand/collapse it or not.
     *
     * @arg boolean enable - true to enable, false to disable this functionality.
     */
    function SetDoubleClickToOpen(enable: boolean): void;

    /**
     * Used to store name for sub elements for a Panel:Droppable call.
     *
     * @arg string name
     */
    function SetDraggableName(name: string): void;

    /**
     * Sets whether or not this node should draw visual lines.
     *
     * @arg boolean draw
     */
    function SetDrawLines(draw: boolean): void;

    /**
     * Expands or collapses this node.
     *
     * @arg boolean expand - Whether to expand (true) or collapse (false)
     * @arg boolean [surpressAnimation=false] - Whether to play animation (false) or not (true)
     */
    function SetExpanded(expand: boolean, surpressAnimation?: boolean): void;

    /**
     * Sets the file full filepath to the file attached to this node
     *
     * @arg string filename
     */
    function SetFileName(filename: string): void;

    /**
     * Sets the folder to search files and folders in.
     *
     * @arg string folder
     */
    function SetFolder(folder: string): void;

    /**
     * Sets whether or not the expand/collapse button (+/- button) should be shown on this node regardless of whether it has sub-elements or not.
     *
     * @arg boolean forceShow
     */
    function SetForceShowExpander(forceShow: boolean): void;

    /**
     * Sets whether the expand button (little + button) should be shown or hidden.
     *
     * @arg boolean hide
     */
    function SetHideExpander(hide: boolean): void;

    /**
     * Sets the material for the icon of the DTree_Node.
     *
     * @arg string path - The path to the material to be used. Do not include "materials/". .pngs are supported.
     */
    function SetIcon(path: string): void;

    /**
     * Called automatically to set whether this node is the last child on this level or not.
     *
     * @arg boolean last
     */
    function SetLastChild(last: boolean): void;

    /**
     *
     *
     * @arg boolean newState
     */
    function SetNeedsChildSearch(newState: boolean): void;

    /**
     * Sets whether or not the node needs populating from the filesystem.
     *
     * @arg boolean needs - Whether or not the node needs populating
     */
    function SetNeedsPopulating(needs: boolean): void;

    /**
     * Sets the parent node of this node. Not the same as Panel:SetParent.
     *
     * @arg Panel parent - The panel to set as a parent node for this node
     */
    function SetParentNode(parent: Panel): void;

    /**
     * Sets the path ID (File Search Paths) for populating the tree from the filesystem.
     *
     * @arg string path - The path ID to set.
     */
    function SetPathID(path: string): void;

    /**
     * Sets the root node (the DTree) of this node.
     *
     * @arg Panel root - The panel to set as root node.
     */
    function SetRoot(root: Panel): void;

    /**
     * Called automatically to update the "selected" status of this node.
     *
     * @arg boolean selected - Whether this node is currently selected or not.
     */
    function SetSelected(selected: boolean): void;

    /**
     * Sets whether or not nodes for files should be added when populating the node from filesystem.
     *
     * @arg boolean showFiles
     */
    function SetShowFiles(showFiles: boolean): void;

    /**
     * Currently does nothing, not implemented.
     */
    function SetupCopy(): void;

    /**
     * Sets the search wildcard.
     *
     * @arg string wildcard - The wildcard to set
     */
    function SetWildCard(wildcard: string): void;

    /**
     * Returns whether or not the DTree this node is in has icons enabled.
     *
     * @return {number} Whether the icons are shown or not
     */
    function ShowIcons(): number;
}
