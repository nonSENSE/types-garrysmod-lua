/** @noSelfInFile */
declare namespace DLabelURL {
    /**
     * Gets the current text color of the DLabelURL. Alias as DLabelURL:GetTextColor.
     *
     * @return {any} The current text Color.
     */
    function GetColor(): any;

    /**
     * Gets the current text color of the DLabelURL set by DLabelURL:SetTextColor.
     *
     * @return {any} The current text Color.
     */
    function GetTextColor(): any;

    /**
     * Returns the color set by DLabelURL:SetTextStyleColor.
     *
     * @return {Color} The Color
     */
    function GetTextStyleColor(): Color;

    /**
     * Alias of DLabelURL:SetTextColor.
     *
     * @arg any col - The Color to use.
     */
    function SetColor(col: any): void;

    /**
     * Sets the text color of the DLabelURL. Overrides DLabelURL:SetTextStyleColor.
     *
     * @arg any col - The Color to use.
     */
    function SetTextColor(col: any): void;

    /**
     * Sets the base text color of the DLabelURL. This is overridden by DLabelURL:SetTextColor.
     *
     * @arg any color - The Color to set
     */
    function SetTextStyleColor(color: any): void;
}
