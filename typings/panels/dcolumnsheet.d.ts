/** @noSelfInFile */
declare namespace DColumnSheet {
    /**
 * Adds a new column/tab.
* 
* @arg string name - Name of the column/tab
* @arg Panel pnl - Panel to be used as contents of the tab. This normally would be a DPanel
* @arg string [icon="nil"] - Icon for the tab. This will ideally be a silkicon, but any material name can be used.
* @return {Panel} A table containing the following keys:

Panel Button - The created tab button that will switch to the given panel
Panel Panel - The given panel to switch to when the button is pressed
 */
    function AddSheet(name: string, pnl: Panel, icon?: string): Panel;

    /**
     * Returns the active button of this DColumnSheet.
     *
     * @return {Panel} The active button
     */
    function GetActiveButton(): Panel;

    /**
     * Makes a button an active button for this DColumnSheet.
     *
     * @arg Panel active - The button to make active button
     */
    function SetActiveButton(active: Panel): void;

    /**
     * Makes the tabs/buttons show only the image and no text.
     */
    function UseButtonOnlyStyle(): void;
}
