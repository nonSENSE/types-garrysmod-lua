/** @noSelfInFile */
declare namespace DListView_Line {
    /**
     * Called when the player right clicks this line.
     */
    function OnRightClick(): void;

    /**
     * Called when the player selects this line.
     */
    function OnSelect(): void;

    /**
     * Called by DListView:DataLayout
     *
     * @arg any pnl - The list view.
     */
    function DataLayout(pnl: any): void;

    /**
     * Returns whether this line is odd or even in the list. This is internally used (and set) to change the looks of every other line.
     *
     * @return {boolean} Whether this line is 'alternative'.
     */
    function GetAltLine(): boolean;

    /**
     * Gets the string held in the specified column of a DListView_Line panel.
     *
     * @arg number column - The number of the column to retrieve the text from, starts with 1.
     * @return {string} The contents of the specified column.
     */
    function GetColumnText(column: number): string;

    /**
     * Returns the ID of this line, set automatically in DListView:AddLine.
     *
     * @return {number} The ID of this line.
     */
    function GetID(): number;

    /**
     * Returns the parent DListView of this line.
     *
     * @return {any} The parent DListView of this line.
     */
    function GetListView(): any;

    /**
     * Returns the data stored on given cell of this line.
     *
     * @arg number column - The number of the column to write the text from, starts with 1.
     * @return {any} The data that is set for given column of this line, if any.
     */
    function GetSortValue(column: number): any;

    /**
     * Returns whether this line is selected.
     *
     * @return {boolean} Whether this line is selected.
     */
    function IsLineSelected(): boolean;

    /**
     * Sets whether this line is odd or even in the list. This is internally used (and set automatically) to change the looks of every other line.
     *
     * @arg boolean alt - Whether this line is 'alternative'.
     */
    function SetAltLine(alt: boolean): void;

    /**
     * Sets the string held in the specified column of a DListView_Line panel.
     *
     * @arg number column - The number of the column to write the text from, starts with 1.
     * @arg string value - Column text you want to set
     * @return {any} The DLabel in which the text was set.
     */
    function SetColumnText(column: number, value: string): any;

    /**
     * Sets whether this line is selected or not.
     *
     * @arg boolean selected - Whether this line is selected.
     */
    function SetSelected(selected: boolean): void;

    /**
     * Allows you to store data per column.
     *
     * @arg number column - The number of the column to write the text from, starts with 1.
     * @arg any data - Data for given column on the line you wish to set.
     */
    function SetSortValue(column: number, data: any): void;
}
