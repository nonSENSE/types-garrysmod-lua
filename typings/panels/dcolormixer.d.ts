/** @noSelfInFile */
declare namespace DColorMixer {
    /**
     *
     */
    function ConVarThink(): void;

    /**
     *
     *
     * @arg string cvar
     */
    function DoConVarThink(cvar: string): void;

    /**
     * Return true if alpha bar is shown, false if not.
     *
     * @return {boolean} Return true if shown, false if not.
     */
    function GetAlphaBar(): boolean;

    /**
     * Returns the current selected color.
     *
     * @return {Color} The current selected color as a Color.
     */
    function GetColor(): Color;

    /**
     * Returns the ConVar name for the alpha channel of the color.
     *
     * @return {string} The ConVar name for the alpha channel of the color
     */
    function GetConVarA(): string;

    /**
     * Returns the ConVar name for the blue channel of the color.
     *
     * @return {string} The ConVar name for the blue channel of the color
     */
    function GetConVarB(): string;

    /**
     * Returns the ConVar name for the green channel of the color.
     *
     * @return {string} The ConVar name for the green channel of the color
     */
    function GetConVarG(): string;

    /**
     * Returns the ConVar name for the red channel of the color.
     *
     * @return {string} The ConVar name for the red channel of the color
     */
    function GetConVarR(): string;

    /**
     * Return true if palette is shown, false if not.
     *
     * @return {boolean} Return true if shown, false if not.
     */
    function GetPalette(): boolean;

    /**
     * Returns the color as a normalized Vector.
     *
     * @return {Vector} A vector representing the color of the DColorMixer, each value being in range of 0 to 1. Alpha is not included.
     */
    function GetVector(): Vector;

    /**
     * Return true if the wangs are shown, false if not.
     *
     * @return {boolean} Return true if shown, false if not.
     */
    function GetWangs(): boolean;

    /**
     * Show/Hide the alpha bar in DColorMixer
     *
     * @arg boolean show - Show / Hide the alpha bar
     */
    function SetAlphaBar(show: boolean): void;

    /**
     * Sets the base color of the DColorCube part of the DColorMixer.
     *
     * @arg Color clr - Color
     */
    function SetBaseColor(clr: Color): void;

    /**
     * Sets the color of the DColorMixer
     *
     * @arg any color - The color to set. See Color
     */
    function SetColor(color: any): void;

    /**
     * Sets the ConVar name for the alpha channel of the color.
     *
     * @arg string convar - The ConVar name for the alpha channel of the color
     */
    function SetConVarA(convar: string): void;

    /**
     * Sets the ConVar name for the blue channel of the color.
     *
     * @arg string convar - The ConVar name for the blue channel of the color
     */
    function SetConVarB(convar: string): void;

    /**
     * Sets the ConVar name for the green channel of the color.
     *
     * @arg string convar - The ConVar name for the green channel of the color
     */
    function SetConVarG(convar: string): void;

    /**
     * Sets the ConVar name for the red channel of the color.
     *
     * @arg string convar - The ConVar name for the red channel of the color
     */
    function SetConVarR(convar: string): void;

    /**
 * Sets the label's text to show.
* 
* @arg string [text="nil"] - Set to non empty string to show the label and its text.
Give it an empty string or nothing and the label will be hidden.
 */
    function SetLabel(text?: string): void;

    /**
     * Show or hide the palette panel
     *
     * @arg boolean enabled - Show or hide the palette panel?
     */
    function SetPalette(enabled: boolean): void;

    /**
     * Sets the color of DColorMixer from a Vector. Alpha is not included.
     *
     * @arg Vector vec - The color to set. It is expected that the vector will have values be from 0 to 1. (i.e. be normalized)
     */
    function SetVector(vec: Vector): void;

    /**
     * Show / Hide the colors indicators in DColorMixer
     *
     * @arg boolean show - Show / Hide the colors indicators
     */
    function SetWangs(show: boolean): void;

    /**
     * Does nothing.
     */
    function TranslateValues(): void;

    /**
     * Use DColorMixer:SetColor instead!
     *
     * @arg object clr
     */
    function UpdateColor(clr: object): void;

    /**
     *
     *
     * @arg string cvar - The ConVar name
     * @arg string part - The color part to set the cvar to. "r", "g", "b" or "a".
     * @arg Color clr - The Color
     */
    function UpdateConVar(cvar: string, part: string, clr: Color): void;

    /**
     *
     *
     * @arg Color clr - The Color
     */
    function UpdateConVars(clr: Color): void;

    /**
     * Called when the player changes the color of the DColorMixer.
     *
     * @arg Color col - The new color. See Color
     */
    function ValueChanged(col: Color): void;
}
