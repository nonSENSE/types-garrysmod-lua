/** @noSelfInFile */
declare namespace DColorButton {
    /**
     * Returns the color of the button
     *
     * @return {Color} The Color of the button
     */
    function GetColor(): Color;

    /**
     * Returns the unique ID set by DColorButton:SetID.
     *
     * @return {number} The unique ID of the button
     */
    function GetID(): number;

    /**
     * Returns whether the DColorButton is currently being pressed (the user is holding it down).
     *
     * @return {boolean}
     */
    function IsDown(): boolean;

    /**
     * Sets the color of the DColorButton.
     *
     * @arg Color color - A Color to set the color as
     * @arg boolean [noTooltip=false] - If true, the tooltip will not be reset to display the selected color.
     */
    function SetColor(color: Color, noTooltip?: boolean): void;

    /**
     * Used internally by DColorPalette to detect which button is which.
     *
     * @arg number id - A unique ID to give this button
     */
    function SetID(id: number): void;
}
