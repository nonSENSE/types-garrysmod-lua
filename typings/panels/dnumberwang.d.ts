/** @noSelfInFile */
declare namespace DNumberWang {
    /**
     * Returns the amount of decimal places allowed in the number selector, set by DNumberWang:SetDecimals
     *
     * @return {number} The amount of decimal places allowed in the number selector.
     */
    function GetDecimals(): number;

    /**
     * Returns whatever is set by DNumberWang:SetFloatValue or 0.
     *
     * @return {number}
     */
    function GetFloatValue(): number;

    /**
     * Returns a fraction representing the current number selector value to number selector min/max range ratio. If argument val is supplied, that number will be computed instead.
     *
     * @arg number val - The fraction numerator.
     */
    function GetFraction(val: number): void;

    /**
     * Returns interval at which the up and down buttons change the current value.
     *
     * @return {number} The current interval.
     */
    function GetInterval(): number;

    /**
     * Returns the maximum numeric value allowed by the number selector.
     *
     * @return {number} The maximum value.
     */
    function GetMax(): number;

    /**
     * Returns the minimum numeric value allowed by the number selector.
     *
     * @return {number} The minimum number.
     */
    function GetMin(): number;

    /**
     * This function returns the panel it is used on.
     *
     * @return {Panel} self
     */
    function GetTextArea(): Panel;

    /**
     * Returns the numeric value inside the number selector.
     *
     * @return {number} The numeric value.
     */
    function GetValue(): number;

    /**
     * Hides the number selector arrows.
     */
    function HideWang(): void;

    /**
     * Internal function which is called when the number selector value is changed. This function is empty by default so it needs to be overridden in order to provide functionality.
     *
     * @arg number val - The new value of the number selector.
     */
    function OnValueChanged(val: number): void;

    /**
     * Sets the amount of decimal places allowed in the number selector.
     *
     * @arg number num - The amount of decimal places.
     */
    function SetDecimals(num: number): void;

    /**
     * Appears to do nothing.
     *
     * @arg number val
     */
    function SetFloatValue(val: number): void;

    /**
     * Sets the value of the number selector based on the given fraction number.
     *
     * @arg number val - The fraction of the number selector's range.
     */
    function SetFraction(val: number): void;

    /**
     * Sets interval at which the up and down buttons change the current value.
     *
     * @arg number min - The new interval.
     */
    function SetInterval(min: number): void;

    /**
     * Sets the maximum numeric value allowed by the number selector.
     *
     * @arg number max - The maximum value.
     */
    function SetMax(max: number): void;

    /**
     * Sets the minimum numeric value allowed by the number selector.
     *
     * @arg number min - The minimum value.
     */
    function SetMin(min: number): void;

    /**
     * Sets the minimum and maximum value allowed by the number selector.
     *
     * @arg number min - The minimum value.
     * @arg number max - The maximum value.
     */
    function SetMinMax(min: number, max: number): void;

    /**
     * Sets the value of the DNumberWang and triggers DNumberWang:OnValueChanged
     *
     * @arg number val - The value to set.
     */
    function SetValue(val: number): void;
}
