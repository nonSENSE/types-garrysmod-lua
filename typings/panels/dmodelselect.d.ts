/** @noSelfInFile */
declare namespace DModelSelect {
    /**
     * Sets the height of the panel in the amount of 64px spawnicons.
     *
     * @arg number [num=2] - Basically how many rows of 64x64 px spawnicons should fit in this DModelSelect
     */
    function SetHeight(num?: number): void;

    /**
     * Called to set the list of models within the panel element.
     *
     * @arg object models - Each key is a model path, the value is a kay-value table where they key is a convar name and value is the value to set to that convar.
     * @arg string convar
     * @arg boolean dontSort
     * @arg boolean DontCallListConVars
     */
    function SetModelList(models: object, convar: string, dontSort: boolean, DontCallListConVars: boolean): void;
}
