/** @noSelfInFile */
declare namespace DMenuOption {
    /**
     * Creates a sub DMenu and returns it. Has no duplicate call protection.
     *
     * @return {Panel} The created DMenu to add options to.
     */
    function AddSubMenu(): Panel;

    /**
     * Returns the checked state of DMenuOption.
     *
     * @return {boolean} Are we checked or not
     */
    function GetChecked(): boolean;

    /**
     * Returns whether the DMenuOption is a checkbox option or a normal button option.
     *
     * @return {boolean}
     */
    function GetIsCheckable(): boolean;

    /**
     * No Description
     *
     * @return {Panel} A DMenu
     */
    function GetMenu(): Panel;

    /**
     * Called whenever the DMenuOption's checked state changes.
     *
     * @arg boolean checked - The new checked state
     */
    function OnChecked(checked: boolean): void;

    /**
     * Sets the checked state of the DMenuOption. Does not invoke DMenuOption:OnChecked.
     *
     * @arg boolean checked
     */
    function SetChecked(checked: boolean): void;

    /**
     * Sets whether the DMenuOption is a checkbox option or a normal button option.
     *
     * @arg boolean checkable
     */
    function SetIsCheckable(checkable: boolean): void;

    /**
     * No Description
     *
     * @arg Panel pnl
     */
    function SetMenu(pnl: Panel): void;

    /**
     * Used internally by DMenuOption:AddSubMenu to create the submenu arrow and assign the created submenu to be opened this this option is hovered.
     *
     * @arg Panel menu
     */
    function SetSubMenu(menu: Panel): void;

    /**
     * Toggles the checked state of DMenuOption. Does not respect DMenuOption:GetIsCheckable.
     */
    function ToggleCheck(): void;
}
