/** @noSelfInFile */
declare namespace ControlPresets {
    /**
     * Adds a convar to be managed by this control.
     *
     * @arg string convar - The convar to add.
     */
    function AddConVar(convar: string): void;

    /**
     * Get a list of all Console Variables being managed by this panel.
     *
     * @return {object} numbered table of convars
     */
    function GetConVars(): object;

    /**
     * Set the name label text.
     *
     * @arg string name - The text to put in the label
     */
    function SetLabel(name: string): void;
}
