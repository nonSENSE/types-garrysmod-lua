/** @noSelfInFile */
declare namespace DFileBrowser {
    /**
     * Clears the file tree and list, and resets all values.
     */
    function Clear(): void;

    /**
     * Returns the root directory/folder of the file tree.
     *
     * @return {string} The path to the root folder.
     */
    function GetBaseFolder(): string;

    /**
     * Returns the current directory/folder being displayed.
     *
     * @return {string} The directory the file list is currently displaying.
     */
    function GetCurrentFolder(): string;

    /**
     * Returns the current file type filter on the file list.
     *
     * @return {string} The current filter applied to the file list.
     */
    function GetFileTypes(): string;

    /**
     * Returns the DTree Node that the file tree stems from.
     *
     * @return {Panel} The DTree_Node used for the tree.
     */
    function GetFolderNode(): Panel;

    /**
     * Returns whether or not the model viewer mode is enabled. In this mode, files are displayed as SpawnIcons instead of a list.
     *
     * @return {boolean} Whether or not files will be displayed using SpawnIcons.
     */
    function GetModels(): boolean;

    /**
     * Returns the name being used for the file tree.
     *
     * @return {string} The name used for the root of the file tree.
     */
    function GetName(): string;

    /**
     * Returns whether or not the file tree is open.
     *
     * @return {boolean} Whether or not the file tree is open.
     */
    function GetOpen(): boolean;

    /**
     * Returns the access path of the file tree. This is GAME unless changed with DFileBrowser:SetPath.
     *
     * @return {string} The current access path i.e. "GAME", "LUA", "DATA" etc.
     */
    function GetPath(): string;

    /**
     * Returns the current search filter on the file tree.
     *
     * @return {string} The filter in use on the file tree.
     */
    function GetSearch(): string;

    /**
     * Called when a file is double-clicked.
     *
     * @arg string filePath - The path to the file that was double-clicked.
     * @arg Panel selectedPanel - The panel that was double-clicked to select this file.This will either be a DListView_Line or SpawnIcon depending on whether the model viewer mode is enabled. See DFileBrowser:SetModels.
     */
    function OnDoubleClick(filePath: string, selectedPanel: Panel): void;

    /**
 * Called when a file is right-clicked.
* 
* @arg string filePath - The path to the file that was right-clicked.
* @arg Panel selectedPanel - The panel that was right-clicked to select this file.
This will either be a DListView_Line or SpawnIcon depending on whether the model viewer mode is enabled. See DFileBrowser:SetModels.
 */
    function OnRightClick(filePath: string, selectedPanel: Panel): void;

    /**
     * Called when a file is selected.
     *
     * @arg string filePath - The path to the file that was selected.
     * @arg Panel selectedPanel - The panel that was clicked to select this file.This will either be a DListView_Line or SpawnIcon depending on whether the model viewer mode is enabled. See DFileBrowser:SetModels.
     */
    function OnSelect(filePath: string, selectedPanel: Panel): void;

    /**
     * Sets the root directory/folder of the file tree.
     *
     * @arg string baseDir - The path to the folder to use as the root.
     */
    function SetBaseFolder(baseDir: string): void;

    /**
     * Sets the directory/folder from which to display the file list.
     *
     * @arg string currentDir - The directory to display files from.
     */
    function SetCurrentFolder(currentDir: string): void;

    /**
 * Sets the file type filter for the file list.
* 
* @arg string [fileTypes="*.*"] - A list of file types to display, separated by spaces e.g.
"*.lua *.txt *.mdl"
 */
    function SetFileTypes(fileTypes?: string): void;

    /**
     * Enables or disables the model viewer mode. In this mode, files are displayed as SpawnIcons instead of a list.
     *
     * @arg boolean [showModels=false] - Whether or not to display files using SpawnIcons.
     */
    function SetModels(showModels?: boolean): void;

    /**
     * Sets the name to use for the file tree.
     *
     * @arg string [treeName="`baseFolder`"] - The name for the root of the file tree. Passing no value causes this to be the base folder name. See DFileBrowser:SetBaseFolder.
     */
    function SetName(treeName?: string): void;

    /**
     * Opens or closes the file tree.
     *
     * @arg boolean [open=false] - true to open the tree, false to close it.
     * @arg boolean [useAnim=false] - If true, the DTree's open/close animation is used.
     */
    function SetOpen(open?: boolean, useAnim?: boolean): void;

    /**
     * Sets the access path for the file tree. This is set to GAME by default.
     *
     * @arg string path - The access path i.e. "GAME", "LUA", "DATA" etc.
     */
    function SetPath(path: string): void;

    /**
     * Sets the search filter for the file tree.
     *
     * @arg string [filter="*"] - The filter to use on the file tree.
     */
    function SetSearch(filter?: string): void;

    /**
     * Called to set up the DTree and file viewer when a base path has been set.
     *
     * @return {boolean} Whether or not the variables needed to set up have been defined.
     */
    function Setup(): boolean;

    /**
     * Called to set up the DListView or DIconBrowser by DFileBrowser:Setup.
     *
     * @return {boolean} Whether or not the files pane was set up successfully.
     */
    function SetupFiles(): boolean;

    /**
     * Called to set up the DTree by DFileBrowser:Setup.
     *
     * @return {boolean} Whether or not the tree was set up successfully.
     */
    function SetupTree(): boolean;

    /**
     * Builds the file or icon list for the current directory.
     *
     * @arg string currentDir - The directory to populate the list from.
     */
    function ShowFolder(currentDir: string): void;

    /**
     * Sorts the file list.
     *
     * @arg boolean [descending=false] - The sort order. true for descending (z-a), false for ascending (a-z).
     */
    function SortFiles(descending?: boolean): void;
}
