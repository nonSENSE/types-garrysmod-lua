/** @noSelfInFile */
declare namespace DDragBase {
    /**
     * Internal function used in DDragBase:MakeDroppable
     *
     * @arg object drops
     * @arg boolean bDoDrop
     * @arg string command
     * @arg number y
     * @arg number x
     */
    function DropAction_Copy(drops: object, bDoDrop: boolean, command: string, y: number, x: number): void;

    /**
     * Internal function used in DDragBase:DropAction_Copy
     *
     * @arg object drops
     * @arg boolean bDoDrop
     * @arg string command
     * @arg number y
     * @arg number x
     */
    function DropAction_Normal(drops: object, bDoDrop: boolean, command: string, y: number, x: number): void;

    /**
     * Internal function used in DDragBase:DropAction_Normal
     *
     * @arg object drops
     * @arg boolean bDoDrop
     * @arg string command
     * @arg number y
     * @arg number x
     */
    function DropAction_Simple(drops: object, bDoDrop: boolean, command: string, y: number, x: number): void;

    /**
     * No Description
     *
     * @return {string} Name of the DnD family.
     */
    function GetDnD(): string;

    /**
     * No Description
     *
     * @return {boolean}
     */
    function GetUseLiveDrag(): boolean;

    /**
     * Makes the panel a receiver for any droppable panel with the same DnD name. Internally calls Panel:Receiver.
     *
     * @arg string name - The unique name for the receiver slot. Only droppable panels with the same DnD name as this can be dropped on the panel.
     * @arg boolean allowCopy - Whether or not to allow droppable panels to be copied when the ctrl key is held down.
     */
    function MakeDroppable(name: string, allowCopy: boolean): void;

    /**
     * Called when anything is dropped on or rearranged within the DDragBase.
     */
    function OnModified(): void;

    /**
     * No Description
     *
     * @arg string name - Name of the DnD family.
     */
    function SetDnD(name: string): void;

    /**
     * Determines where you can drop stuff.
     * "4" for left
     * "5" for center
     * "6" for right
     * "8" for top
     * "2" for bottom
     *
     * @arg string [pos="5"] - Where you're allowed to drop things.
     */
    function SetDropPos(pos?: string): void;

    /**
     * No Description
     *
     * @arg boolean newState
     */
    function SetUseLiveDrag(newState: boolean): void;

    /**
     * Internal function used in DDragBase:DropAction_Normal
     *
     * @arg number drop
     * @arg Panel pnl
     */
    function UpdateDropTarget(drop: number, pnl: Panel): void;
}
