/** @noSelfInFile */
declare namespace DMenu {
    /**
 * Creates a DMenuOptionCVar and adds it as an option into the menu. Checking and unchecking the option will alter the given console variable's value.
* 
* @arg string strText - The text of the button
* @arg string convar - The console variable to change
* @arg string on - The value of the console variable to set when the option is checked
* @arg string off - The value of the console variable to set when the option is unchecked
* @arg GMLua.CallbackNoContext [funcFunction=nil] - If set, the function will be called every time the option is pressed/clicked/selected.
It will have only one argument:
Panel pnl - The created DMenuOptionCVar
* @return {Panel} The created DMenuOptionCVar
 */
    function AddCVar(
        strText: string,
        convar: string,
        on: string,
        off: string,
        funcFunction?: GMLua.CallbackNoContext
    ): Panel;

    /**
     * Add an option to the DMenu
     *
     * @arg string name - Name of the option.
     * @arg GMLua.CallbackNoContext [func=nil] - Function to execute when this option is clicked.
     * @return {Panel} Returns the created DMenuOption panel.
     */
    function AddOption(name: string, func?: GMLua.CallbackNoContext): Panel;

    /**
     * Adds a panel to the DMenu as if it were an option.
     *
     * @arg Panel pnl - The panel that you want to add.
     */
    function AddPanel(pnl: Panel): void;

    /**
     * Adds a horizontal line spacer.
     */
    function AddSpacer(): void;

    /**
     * Add a sub menu to the DMenu
     *
     * @arg string Name - Name of the sub menu.
     * @arg GMLua.CallbackNoContext [func=nil] - Function to execute when this sub menu is clicked.
     * @return {Panel} The created sub DMenu
     * @return {Panel} The created DMenuOption
     */
    function AddSubMenu(Name: string, func?: GMLua.CallbackNoContext): LuaMultiReturn<[Panel, Panel]>;

    /**
     * Returns the number of child elements of DMenu's DScrollPanel:GetCanvas.
     *
     * @return {number} The number of child elements
     */
    function ChildCount(): number;

    /**
     * Clears all highlights made by DMenu:HighlightItem.
     */
    function ClearHighlights(): void;

    /**
     * Used internally by DMenu:OpenSubMenu.
     *
     * @arg Panel menu - The menu to close
     */
    function CloseSubMenu(menu: Panel): void;

    /**
 * Gets a child by its index.
* 
* @arg number childIndex - The index of the child to get.
Unlike Panel:GetChild, this index starts at 1.
 */
    function GetChild(childIndex: number): void;

    /**
     * Set by DMenu:SetDeleteSelf
     *
     * @return {boolean}
     */
    function GetDeleteSelf(): boolean;

    /**
     * Returns the value set by DMenu:SetDrawBorder.
     *
     * @return {boolean}
     */
    function GetDrawBorder(): boolean;

    /**
     * Returns whether the DMenu should draw the icon column with a different color or not.
     *
     * @return {boolean} Whether to draw the column or not
     */
    function GetDrawColumn(): boolean;

    /**
     * Returns the maximum height of the DMenu.
     *
     * @return {number} The maximum height in pixels
     */
    function GetMaxHeight(): number;

    /**
     * Returns the minimum width of the DMenu in pixels
     *
     * @return {number} the minimum width of the DMenu
     */
    function GetMinimumWidth(): number;

    /**
     * Returns the currently opened submenu.
     *
     * @return {Panel} The currently opened submenu, if any.
     */
    function GetOpenSubMenu(): Panel;

    /**
     * Used to safely hide (not remove) the menu. This will also hide any opened submenues recursively.
     */
    function Hide(): void;

    /**
     * Highlights selected item in the DMenu by setting the item's key "Highlight" to true.
     *
     * @arg Panel item - The item to highlight.
     */
    function HighlightItem(item: Panel): void;

    /**
     * Opens the DMenu at the current mouse position
     *
     * @arg number [x=NaN] - Position (X coordinate) to open the menu at.
     * @arg number [y=NaN] - Position (Y coordinate) to open the menu at.
     * @arg any [skipanimation=nil] - This argument does nothing.
     * @arg Panel [ownerpanel=nil] - If x and y are not set manually, setting this argument will offset the y position of the opened menu by the height of given panel.
     */
    function Open(x?: number, y?: number, skipanimation?: any, ownerpanel?: Panel): void;

    /**
     * Closes any active sub menus, and opens a new one.
     *
     * @arg Panel item - The DMenuOption to open the submenu at
     * @arg Panel [menu=nil] - The submenu to open. If set to nil, the function just closes existing submenus.
     */
    function OpenSubMenu(item: Panel, menu?: Panel): void;

    /**
     * Called when a option has been selected
     *
     * @arg Panel option - The DMenuOption that was selected
     * @arg string optionText - The options text
     */
    function OptionSelected(option: Panel, optionText: string): void;

    /**
     * Called by DMenuOption. Calls DMenu:OptionSelected.
     *
     * @arg Panel option - The DMenuOption that called this function
     */
    function OptionSelectedInternal(option: Panel): void;

    /**
     * Set to true by default. IF set to true, the menu will be deleted when it is closed, not simply hidden.
     *
     * @arg boolean newState - true to delete menu on close, false to simply hide.
     */
    function SetDeleteSelf(newState: boolean): void;

    /**
     * Does nothing.
     *
     * @arg boolean bool
     */
    function SetDrawBorder(bool: boolean): void;

    /**
     * Sets whether the DMenu should draw the icon column with a different color.
     *
     * @arg boolean draw - Whether to draw the column or not
     */
    function SetDrawColumn(draw: boolean): void;

    /**
     * Sets the maximum height the DMenu can have. If the height of all menu items exceed this value, a scroll bar will be automatically added.
     *
     * @arg number maxHeight - The maximum height of the DMenu to set, in pixels
     */
    function SetMaxHeight(maxHeight: number): void;

    /**
     * Sets the minimum width of the DMenu. The menu will be stretched to match the given value.
     *
     * @arg number minWidth - The minimum width of the DMenu in pixels
     */
    function SetMinimumWidth(minWidth: number): void;

    /**
     * Used internally to store the open submenu by DMenu:Hide, DMenu:OpenSubMenu, DMenu:CloseSubMenu
     *
     * @arg Panel item - The menu to store
     */
    function SetOpenSubMenu(item: Panel): void;
}
