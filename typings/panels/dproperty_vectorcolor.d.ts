/** @noSelfInFile */
declare namespace DProperty_VectorColor {
    /**
     * Called by a property row to setup a color selection control.
     *
     * @arg string [prop="VectorColor"]
     * @arg any settings - A table of settings. None of the values are used for this property. See Editable Entities.
     */
    function Setup(prop?: string, settings?: any): void;

    /**
     * Sets the color value of the property.
     *
     * @arg Vector color - Sets the color to use in a DProperty_VectorColor.
     */
    function SetValue(color: Vector): void;
}
