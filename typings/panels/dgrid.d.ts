/** @noSelfInFile */
declare namespace DGrid {
    /**
     * Adds a new item to the grid.
     *
     * @arg Panel item - The item to add. It will be forced visible and parented to the DGrid.
     */
    function AddItem(item: Panel): void;

    /**
     * Returns the number of columns of this DGrid. Set by DGrid:SetCols.
     *
     * @return {number} The number of columns of this DGrid
     */
    function GetCols(): number;

    /**
     * Returns the width of each column of the DGrid, which is set by DGrid:SetColWide.
     *
     * @return {number} The width of each column
     */
    function GetColWide(): number;

    /**
     * Returns a list of panels in the grid.
     *
     * @return {Panel} A list of Panels.
     */
    function GetItems(): Panel;

    /**
     * Returns the height of each row of the DGrid, which is set by DGrid:SetRowHeight.
     *
     * @return {number} The height of each row
     */
    function GetRowHeight(): number;

    /**
     * Removes given panel from the DGrid:GetItems.
     *
     * @arg Panel item - Item to remove from the grid
     * @arg boolean [bDontDelete=false] - If set to true, the actual panel will not be removed via Panel:Remove.
     */
    function RemoveItem(item: Panel, bDontDelete?: boolean): void;

    /**
     * Sets the number of columns this panel should have.
     *
     * @arg number cols - The desired number of columns
     */
    function SetCols(cols: number): void;

    /**
     * Sets the width of each column.
     *
     * @arg number colWidth - The width of each column.
     */
    function SetColWide(colWidth: number): void;

    /**
     * Sets the height of each row.
     *
     * @arg number rowHeight - The height of each row
     */
    function SetRowHeight(rowHeight: number): void;

    /**
     * Sorts the items in the grid. Does not visually update the grid, use Panel:InvalidateLayout for that.
     *
     * @arg string key - A key in the panel from DGrid:GetItems. The key's value must be numeric.
     * @arg boolean [desc=true] - True for descending order, false for ascending.
     */
    function SortByMember(key: string, desc?: boolean): void;
}
