/** @noSelfInFile */
declare namespace DColorCube {
    /**
     * Returns the base Color set by DColorCube:SetBaseRGB.
     *
     * @return {Color} A Color
     */
    function GetBaseRGB(): Color;

    /**
     * Returns the value set by DColorCube:SetHue.
     *
     * @return {number}
     */
    function GetHue(): number;

    /**
     * Returns the color cube's current set color.
     *
     * @return {Color} The set color, uses Color.
     */
    function GetRGB(): Color;

    /**
     * Function which is called when the color cube slider is moved (through user input). Meant to be overridden.
     *
     * @arg Color color - The new color, uses Color.
     */
    function OnUserChanged(color: Color): void;

    /**
     * Sets the base color and the color used to draw the color cube panel itself.
     *
     * @arg Color color - The base color to set, uses Color.
     */
    function SetBaseRGB(color: Color): void;

    /**
     * Sets the base color of the color cube and updates the slider position.
     *
     * @arg Color color - The color to set, uses Color.
     */
    function SetColor(color: Color): void;

    /**
     * Appears to do nothing and unused.
     *
     * @arg number hue
     */
    function SetHue(hue: number): void;

    /**
     * Used internally to set the real "output" color of the panel.
     *
     * @arg Color clr - A Color
     */
    function SetRGB(clr: Color): void;

    /**
     * Updates the color cube RGB based on the given x and y position and returns its arguments. Similar to DColorCube:UpdateColor.
     *
     * @arg number x - The x position to sample color from/the percentage of saturation to remove from the color (ranges from 0.0 to 1.0).
     * @arg number y - The y position to sample color from/the percentage of brightness or value to remove from the color (ranges from 0.0 to 1.0).
     * @return {number} The given x position.
     * @return {number} The given y position.
     */
    function TranslateValues(x: number, y: number): LuaMultiReturn<[number, number]>;

    /**
     * Updates the color cube RGB based on the given x and y position. Similar to DColorCube:TranslateValues.
     *
     * @arg number [x=NaN] - The x position to set color to/the percentage of saturation to remove from the color (ranges from 0.0 to 1.0).
     * @arg number [y=NaN] - The y position to set color to/the percentage of brightness or value to remove from the color (ranges from 0.0 to 1.0).
     */
    function UpdateColor(x?: number, y?: number): void;
}
