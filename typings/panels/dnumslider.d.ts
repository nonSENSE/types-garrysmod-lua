/** @noSelfInFile */
declare namespace DNumSlider {
    /**
     * Returns the amount of numbers after the decimal point.
     *
     * @return {number} 0 for whole numbers only, 1 for one number after the decimal point, etc.
     */
    function GetDecimals(): number;

    /**
     * Returns the default value of the slider, if one was set by DNumSlider:SetDefaultValue
     *
     * @return {number} The default value of the slider
     */
    function GetDefaultValue(): number;

    /**
     * Returns the maximum value of the slider
     *
     * @return {number} The maximum value of the slider
     */
    function GetMax(): number;

    /**
     * Returns the minimum value of the slider
     *
     * @return {number} The minimum value of the slider
     */
    function GetMin(): number;

    /**
     * Returns the range of the slider, basically maximum value - minimum value.
     *
     * @return {number} The range of the slider
     */
    function GetRange(): number;

    /**
     * Returns the DTextEntry component of the slider.
     *
     * @return {Panel} The DTextEntry.
     */
    function GetTextArea(): Panel;

    /**
     * Returns the value of the DNumSlider
     *
     * @return {number} The value of the slider.
     */
    function GetValue(): number;

    /**
     * Returns true if either the DTextEntry, the DSlider or the DNumberScratch are being edited.
     *
     * @return {boolean} Whether or not the DNumSlider is being edited by the player.
     */
    function IsEditing(): boolean;

    /**
     * Called when the value of the slider is changed, through code or changing the slider.
     *
     * @arg number value - The new value of the DNumSlider.
     */
    function OnValueChanged(value: number): void;

    /**
     * Resets the slider to the default value, if one was set by DNumSlider:SetDefaultValue.
     */
    function ResetToDefaultValue(): void;

    /**
     * Sets the console variable to be updated when the value of the slider is changed.
     *
     * @arg string cvar - The name of the ConVar to be updated.
     */
    function SetConVar(cvar: string): void;

    /**
     * Calls DLabel:SetDark on the DLabel part of the DNumSlider.
     *
     * @arg boolean dark
     */
    function SetDark(dark: boolean): void;

    /**
     * Sets the desired amount of numbers after the decimal point.
     *
     * @arg number decimals - 0 for whole numbers only, 1 for one number after the decimal point, etc.
     */
    function SetDecimals(decimals: number): void;

    /**
     * Sets the default value of the slider, to be used by DNumSlider:ResetToDefaultValue or by middle mouse clicking the draggable knob of the slider.
     *
     * @arg number def - The new default value of the slider to set
     */
    function SetDefaultValue(def: number): void;

    /**
     * Sets the maximum value for the slider.
     *
     * @arg number max - The value to set as maximum for the slider.
     */
    function SetMax(max: number): void;

    /**
     * Sets the minimum value for the slider
     *
     * @arg number min - The value to set as minimum for the slider.
     */
    function SetMin(min: number): void;

    /**
     * Sets the minimum and the maximum value of the slider.
     *
     * @arg number min - The minimum value of the slider.
     * @arg number max - The maximum value of the slider.
     */
    function SetMinMax(min: number, max: number): void;

    /**
     * Sets the value of the DNumSlider.
     *
     * @arg number val - The value to set.
     */
    function SetValue(val: number): void;

    /**
     *
     *
     * @arg number x
     * @arg number y
     * @return {number}
     * @return {number} The second passed argument.
     */
    function TranslateSliderValues(x: number, y: number): LuaMultiReturn<[number, number]>;

    /**
     *
     */
    function UpdateNotches(): void;

    /**
     * Called when the value has been changed. This will also be called when the user manually changes the value through the text panel.
     *
     * @arg number value - The value the slider has been changed to.
     */
    function ValueChanged(value: number): void;
}
