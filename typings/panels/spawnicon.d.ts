/** @noSelfInFile */
declare namespace SpawnIcon {
    /**
     * Returns the currently set body groups of the spawn icon. This is set by SpawnIcon:SetBodyGroup.
     *
     * @return {string} The bodygroups of the spawnicon
     */
    function GetBodyGroup(): string;

    /**
     * Returns the currently set model name. This is set by SpawnIcon:SetModelName.
     *
     * @return {string} The model name
     */
    function GetModelName(): string;

    /**
     * Returns the currently set skin of the spawnicon. This is set by SpawnIcon:SetSkinID.
     *
     * @return {number} Current skin ID
     */
    function GetSkinID(): number;

    /**
     * Called when right clicked on the SpawnIcon. It will not be called if there is a selection (Panel:GetSelectionCanvas), in which case SANDBOX:SpawnlistOpenGenericMenu is called.
     */
    function OpenMenu(): void;

    /**
     * Sets the bodygroups, so it can be retrieved with SpawnIcon:GetBodyGroup. Use Panel:SetModel instead.
     *
     * @arg number bodygroup - The id of the bodygroup you're setting. Starts from 0.
     * @arg number value - The value you're setting the bodygroup to. Starts from 0.
     */
    function SetBodyGroup(bodygroup: number, value: number): void;

    /**
     * Sets the model name, so it can be retrieved with SpawnIcon:GetModelName. Use Panel:SetModel instead.
     *
     * @arg string mdl - The model name to set
     */
    function SetModelName(mdl: string): void;

    /**
     * Sets the skin id of the spawn icon, so it can be retrieved with SpawnIcon:GetSkinID. Use Panel:SetModel instead.
     *
     * @arg number skin - Skin ID to set
     */
    function SetSkinID(skin: number): void;
}
