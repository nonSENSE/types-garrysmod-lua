/** @noSelfInFile */
declare namespace DTree {
    /**
     * Add a node to the DTree
     *
     * @arg string name - Name of the option.
     * @arg string [icon="icon16/folder.png"] - The icon that will show nexto the node in the DTree.
     * @return {Panel} Returns the created DTree_Node panel.
     */
    function AddNode(name: string, icon?: string): Panel;

    /**
     * Calls directly to Panel:InvalidateLayout.
     * Called by DTree_Nodes when a sub element has been expanded or collapsed.
     *
     * @arg boolean bExpand
     */
    function ChildExpanded(bExpand: boolean): void;

    /**
     * Called when the any node is clicked. Called by DTree_Node:DoClick.
     */
    function DoClick(): void;

    /**
     * Called when the any node is right clicked. Called by DTree_Node:DoRightClick.
     *
     * @arg any node - The right clicked node.
     */
    function DoRightClick(node: any): void;

    /**
     * Does nothing. Used as a placeholder empty function alongside DTree:MoveChildTo, DTree:SetExpanded and DTree:ChildExpanded.
     *
     * @arg boolean bExpand
     */
    function ExpandTo(bExpand: boolean): void;

    /**
     * Returns the status of DTree:SetClickOnDragHover. See that for more info.
     *
     * @return {boolean}
     */
    function GetClickOnDragHover(): boolean;

    /**
     * Returns the indentation size of the DTree, the distance between each "level" of the tree is offset on the left from the previous level.
     *
     * @return {number} The indentation size.
     */
    function GetIndentSize(): number;

    /**
     * Returns the height of each DTree_Node in the tree.
     *
     * @return {number} The height of each DTree_Node in the tree.
     */
    function GetLineHeight(): number;

    /**
     * Returns the currently selected node.
     *
     * @return {Panel} Curently selected node.
     */
    function GetSelectedItem(): Panel;

    /**
     * Returns whether or not the Silkicons next to each node of the DTree will be displayed.
     *
     * @return {boolean} Whether or not the silkicons next to each node will be displayed.
     */
    function GetShowIcons(): boolean;

    /**
     * Does nothing.
     */
    function LayoutTree(): void;

    /**
 * Moves given node to the top of DTrees children. (Makes it the topmost mode)
* 
* @arg Panel child - The node to move
* @arg number pos - We advise against using this. It may be changed or removed in a future update.
Unused, does nothing.
 */
    function MoveChildTo(child: Panel, pos: number): void;

    /**
     * This function is called when a node within a tree is selected.
     *
     * @arg Panel node - The node that was selected.
     */
    function OnNodeSelected(node: Panel): void;

    /**
     * Returns the root DTree_Node, the node that is the parent to all other nodes of the DTree.
     *
     * @return {Panel} Root node.
     */
    function Root(): Panel;

    /**
     * Enables the "click when drag-hovering" functionality.
     *
     * @arg boolean enable
     */
    function SetClickOnDragHover(enable: boolean): void;

    /**
     * Does nothing. Is not called by the DTree itself.
     *
     * @arg boolean bExpand
     */
    function SetExpanded(bExpand: boolean): void;

    /**
     * Sets the indentation size of the DTree, the distance between each "level" of the tree is offset on the left from the previous level.
     *
     * @arg number size - The new indentation size.
     */
    function SetIndentSize(size: number): void;

    /**
     * Sets the height of each DTree_Node in the tree.
     *
     * @arg number h - The height to set.
     */
    function SetLineHeight(h: number): void;

    /**
     * Set the currently selected top-level node.
     *
     * @arg Panel node - DTree_Node to select.
     */
    function SetSelectedItem(node: Panel): void;

    /**
     * Sets whether or not the Silkicons next to each node of the DTree will be displayed.
     *
     * @arg boolean show - Whether or not to show icons.
     */
    function SetShowIcons(show: boolean): void;

    /**
     * Returns whether or not the Silkicons next to each node of the DTree will be displayed.
     *
     * @return {boolean} Whether or not the silkicons next to each node will be displayed.
     */
    function ShowIcons(): boolean;
}
