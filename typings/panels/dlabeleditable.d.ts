/** @noSelfInFile */
declare namespace DLabelEditable {
    /**
     * Returns whether the editable label will stretch to the text entered or not.
     *
     * @return {boolean} Whether the editable label will stretch to the text entered or not.
     */
    function GetAutoStretch(): boolean;

    /**
     * A hook called when the player presses Enter (i.e. the finished editing the label) and the text has changed.
     *
     * @arg string txt - The original user input text
     * @return {string} If provided, will override the text that will be applied to the label itself.
     */
    function OnLabelTextChanged(txt: string): string;

    /**
     * Sets whether the editable label should stretch to the text entered or not.
     *
     * @arg boolean stretch - Whether the editable label should stretch to the text entered or not.
     */
    function SetAutoStretch(stretch: boolean): void;
}
