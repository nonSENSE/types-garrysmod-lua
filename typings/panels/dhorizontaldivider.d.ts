/** @noSelfInFile */
declare namespace DHorizontalDivider {
    /**
     * Returns the width of the horizontal divider bar, set by DHorizontalDivider:SetDividerWidth.
     *
     * @return {number} The width of the horizontal divider bar
     */
    function GetDividerWidth(): number;

    /**
     * Returns whether or not the player is currently dragging the middle divider bar.
     *
     * @return {boolean} Whether or not the player is currently dragging the middle divider bar.
     */
    function GetDragging(): boolean;

    /**
     * Returns the local X coordinate of where the player started dragging the thing
     *
     * @return {number}
     */
    function GetHoldPos(): number;

    /**
     * Returns the left side content of the DHorizontalDivider
     *
     * @return {Panel} The content on the left side
     */
    function GetLeft(): Panel;

    /**
     * Returns the minimum width of the left side, set by DHorizontalDivider:SetLeftMin.
     *
     * @return {number} The minimum width of the left side
     */
    function GetLeftMin(): number;

    /**
     * Returns the current width of the left side, set by DHorizontalDivider:SetLeftWidth or by the user.
     *
     * @return {number} The current width of the left side
     */
    function GetLeftWidth(): number;

    /**
     * Returns the middle content, set by DHorizontalDivider:SetMiddle.
     *
     * @return {Panel} The middle content
     */
    function GetMiddle(): Panel;

    /**
     * Returns the right side content
     *
     * @return {Panel} The right side content
     */
    function GetRight(): Panel;

    /**
     * Returns the minimum width of the right side, set by DHorizontalDivider:SetRightMin.
     *
     * @return {number} The minimum width of the right side
     */
    function GetRightMin(): number;

    /**
     * Sets the width of the horizontal divider bar.
     *
     * @arg number width - The width of the horizontal divider bar.
     */
    function SetDividerWidth(width: number): void;

    /**
     * Sets whether the player is dragging the divider or not
     *
     * @arg boolean dragonot
     */
    function SetDragging(dragonot: boolean): void;

    /**
     * Sets the local X coordinate of where the player started dragging the thing
     *
     * @arg number x
     */
    function SetHoldPos(x: number): void;

    /**
     * Sets the left side content of the DHorizontalDivider.
     *
     * @arg Panel pnl - The panel to set as the left side
     */
    function SetLeft(pnl: Panel): void;

    /**
     * Sets the minimum width of the left side
     *
     * @arg number minWidth - The minimum width of the left side
     */
    function SetLeftMin(minWidth: number): void;

    /**
     * Sets the current/starting width of the left side.
     *
     * @arg number width - The current/starting width of the left side
     */
    function SetLeftWidth(width: number): void;

    /**
     * Sets the middle content, over the draggable divider bar panel.
     *
     * @arg Panel middle - The middle content
     */
    function SetMiddle(middle: Panel): void;

    /**
     * Sets the right side content
     *
     * @arg Panel pnl - The right side content
     */
    function SetRight(pnl: Panel): void;

    /**
     * Sets the minimum width of the right side
     *
     * @arg number minWidth - The minimum width of the right side
     */
    function SetRightMin(minWidth: number): void;

    /**
     *
     */
    function StartGrab(): void;
}
