/** @noSelfInFile */
declare namespace DHorizontalScroller {
    /**
     * Adds a panel to the DHorizontalScroller.
     *
     * @arg Panel pnl - The panel to add. It will be automatically parented.
     */
    function AddPanel(pnl: Panel): void;

    /**
     * Returns the internal canvas panel where the content of DHorizontalScroller are placed on.
     *
     * @return {Panel} The DDragBase panel.
     */
    function GetCanvas(): Panel;

    /**
     * No Description
     *
     * @return {number}
     */
    function GetOverlap(): number;

    /**
     * No Description
     *
     * @return {boolean}
     */
    function GetShowDropTargets(): boolean;

    /**
     * Same as DDragBase:MakeDroppable.
     * TODO: Transclude or whatever to here?
     *
     * @arg string name
     */
    function MakeDroppable(name: string): void;

    /**
     * Called when the panel is scrolled.
     */
    function OnDragModified(): void;

    /**
     * Scrolls the DHorizontalScroller to given child panel.
     *
     * @arg Panel target - The target child panel. Must be a child of DHorizontalScroller:GetCanvas
     */
    function ScrollToChild(target: Panel): void;

    /**
     * Controls the spacing between elements of the horizontal scroller.
     *
     * @arg number overlap - Overlap in pixels. Positive numbers will make elements overlap each other, negative will add spacing.
     */
    function SetOverlap(overlap: number): void;

    /**
     * Sets the scroll amount, automatically clamping the value.
     *
     * @arg number scroll - The new scroll amount
     */
    function SetScroll(scroll: number): void;

    /**
     * No Description
     *
     * @arg boolean newState
     */
    function SetShowDropTargets(newState: boolean): void;

    /**
     * Same as DDragBase:SetUseLiveDrag
     *
     * @arg boolean newState
     */
    function SetUseLiveDrag(newState: boolean): void;
}
