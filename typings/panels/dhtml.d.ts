/** @noSelfInFile */
declare namespace DHTML {
    /**
     * Defines a Javascript function that when called will call a Lua callback.
     *
     * @arg string library - Library name of the JS function you are defining.
     * @arg string name - Name of the JS function you are defining.
     * @arg GMLua.CallbackNoContext callback - Function called when the JS function is called. Arguments passed to the JS function will be passed here.
     */
    function AddFunction(library: string, name: string, callback: GMLua.CallbackNoContext): void;

    /**
     * Runs/Executes a string as JavaScript code in a panel.
     *
     * @arg string js - Specify JavaScript code to be executed.
     */
    function Call(js: string): void;

    /**
     * Called when the page inside the DHTML window runs console.log. This can also be called within the Lua environment to emulate console.log. If the contained message begins with RUNLUA: the following text will be executed as code within the Lua environment (this is how Lua is called from DHTML windows).
     *
     * @arg string msg - The message to be logged (or Lua code to be executed; see above).
     */
    function ConsoleMessage(msg: string): void;

    /**
     * Returns if the loaded page can run Lua code, set by DHTML:SetAllowLua
     *
     * @return {boolean} Whether or not Lua code can be called from the loaded page.
     */
    function GetAllowLua(): boolean;

    /**
     * Runs/Executes a string as JavaScript code in a panel.
     *
     * @arg string js - Specify JavaScript code to be executed.
     */
    function QueueJavascript(js: string): void;

    /**
     * Determines whether the loaded page can run Lua code or not. See DHTML for how to run Lua from a DHTML window.
     *
     * @arg boolean [allow=false] - Whether or not to allow Lua.
     */
    function SetAllowLua(allow?: boolean): void;

    /**
     * Sets if the loaded window should display scrollbars when the webpage is larger than the viewing window. This is similar to the CSS overflow rule.
     *
     * @arg boolean show - True if scrollbars should be visible.
     */
    function SetScrollbars(show: boolean): void;

    /**
     * Stops the loading of the HTML panel's current page.
     */
    function StopLoading(): void;
}
