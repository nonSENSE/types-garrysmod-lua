/** @noSelfInFile */
declare namespace DListView_Column {
    /**
     * Sets the fixed width of the column.
     *
     * @arg number width - The number value which will determine a fixed width.
     */
    function SetFixedWidth(width: number): void;

    /**
     * Sets the maximum width of a column.
     *
     * @arg number width - The number value which will determine a maximum width.
     */
    function SetMaxWidth(width: number): void;

    /**
     * Sets the minimum width of a column.
     *
     * @arg number width - The number value which will determine a minimum width.
     */
    function SetMinWidth(width: number): void;

    /**
 * Sets the text alignment for the column
* 
* @arg number alignment - The direction of the content, based on the number pad.










7: top-left
8: top-center
9: top-right


4: middle-left
5: center
6: middle-right


1: bottom-left
2: bottom-center
3: bottom-right
 */
    function SetTextAlign(alignment: number): void;

    /**
     * Sets the width of the panel.
     *
     * @arg number width - The number value which will determine panel width.
     */
    function SetWidth(width: number): void;
}
