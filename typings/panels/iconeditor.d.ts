/** @noSelfInFile */
declare namespace IconEditor {
    /**
     * Applies the top-down view camera settings for the model in the DAdjustableModelPanel.
     */
    function AboveLayout(): void;

    /**
     * Applies the best camera settings for the model in the DAdjustableModelPanel, using the values returned by PositionSpawnIcon.
     */
    function BestGuessLayout(): void;

    /**
     * Fills the DListView on the left of the editor with the model entity's animation list. Called by IconEditor:Refresh.
     *
     * @arg Entity ent - The entity being rendered within the model panel.
     */
    function FillAnimations(ent: Entity): void;

    /**
     * Applies the front view camera settings for the model in the DAdjustableModelPanel.
     */
    function FullFrontalLayout(): void;

    /**
     * Places the camera at the origin (0,0,0), relative to the entity, in the DAdjustableModelPanel.
     */
    function OriginLayout(): void;

    /**
     * Updates the internal DAdjustableModelPanel and SpawnIcon.
     */
    function Refresh(): void;

    /**
     * Re-renders the SpawnIcon.
     */
    function RenderIcon(): void;

    /**
     * Applies the right side view camera settings for the model in the DAdjustableModelPanel.
     */
    function RightLayout(): void;

    /**
     * Sets up the default ambient and directional lighting for the DAdjustableModelPanel. Called by IconEditor:Refresh.
     */
    function SetDefaultLighting(): void;

    /**
     * Sets the editor's model and icon from an entity. Alternative to IconEditor:SetIcon, with uses a SpawnIcon.
     *
     * @arg Entity ent - The entity to retrieve the model and skin from.
     */
    function SetFromEntity(ent: Entity): void;

    /**
     * Sets the SpawnIcon to modify. You should call Panel:Refresh immediately after this, as the user will not be able to make changes to the icon beforehand.
     *
     * @arg Panel icon - The SpawnIcon object to be modified.
     */
    function SetIcon(icon: Panel): void;

    /**
     * Updates the entity being rendered in the internal DAdjustableModelPanel. Called by the model panel's DModelPanel:LayoutEntity method.
     *
     * @arg Entity ent - The entity being rendered within the model panel.
     */
    function UpdateEntity(ent: Entity): void;
}
