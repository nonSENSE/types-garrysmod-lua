/** @noSelfInFile */
declare namespace DAlphaBar {
    /**
     * Called when user changes the desired alpha value with the control.
     *
     * @arg number alpha - The new alpha value
     */
    function OnChange(alpha: number): void;

    /**
     * Returns the base color of the alpha bar. This is the color for which the alpha channel is being modified.
     *
     * @return {object} The current base color.
     */
    function GetBarColor(): object;

    /**
     * Returns the alpha value of the alpha bar.
     *
     * @return {number} The current alpha value.
     */
    function GetValue(): number;

    /**
     * Sets the base color of the alpha bar. This is the color for which the alpha channel is being modified.
     *
     * @arg object clr - The new Color to set. See Color.
     */
    function SetBarColor(clr: object): void;

    /**
     * Sets the alpha value or the alpha bar.
     *
     * @arg number alpha - The new alpha value to set
     */
    function SetValue(alpha: number): void;
}
