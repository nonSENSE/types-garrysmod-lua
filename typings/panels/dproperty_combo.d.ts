/** @noSelfInFile */
declare namespace DProperty_Combo {
    /**
     * Add a choice to your combo control.
     *
     * @arg string Text - Shown text.
     * @arg any data - Stored Data.
     * @arg boolean [select=false] - Select this element?
     */
    function AddChoice(Text: string, data: any, select?: boolean): void;

    /**
     * Called after the user selects a new value.
     *
     * @arg any data - The new data that was selected.
     */
    function DataChanged(data: any): void;

    /**
     * Set the selected option.
     *
     * @arg number Id - Id of the choice to be selected.
     */
    function SetSelected(Id: number): void;

    /**
 * Sets up a combo control.
* 
* @arg string [prop="Combo"] - The name of DProperty sub control to add.
* @arg object [data={ text = 'Select...' }] - Data to use to set up the combo box control.
Structure:

string text - The default label for this combo box
table values - The values to add to the combo box
 */
    function Setup(prop?: string, data?: object): void;
}
