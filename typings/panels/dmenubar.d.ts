/** @noSelfInFile */
declare namespace DMenuBar {
    /**
     * Creates a new DMenu object tied to a DButton with the given label on the menu bar.
     *
     * @arg string label - The name (label) of the derma menu to create.
     * @return {Panel} The new DMenu which will be opened when the button is clicked.
     */
    function AddMenu(label: string): Panel;

    /**
     * Retrieves a DMenu object from the menu bar. If one with the given label doesn't exist, a new one is created.
     *
     * @arg string label - The name (label) of the derma menu to get or create.
     * @return {Panel} The DMenu with the given label.
     */
    function AddOrGetMenu(label: string): Panel;

    /**
     * Returns the DMenuBar's background color
     *
     * @return {Color} The background's color. See Color
     */
    function GetBackgroundColor(): Color;

    /**
     * Returns whether or not the DMenuBar is disabled
     *
     * @return {boolean} Is disabled
     */
    function GetDisabled(): boolean;

    /**
     * Returns whether or not the background should be painted. Is the same as DMenuBar:GetPaintBackground
     *
     * @return {boolean} Should the background be painted
     */
    function GetDrawBackground(): boolean;

    /**
     * Returns whether or not the panel is a menu. Used for closing menus when another panel is selected.
     *
     * @return {boolean} Is a menu
     */
    function GetIsMenu(): boolean;

    /**
     * If a menu is visible/opened, then the menu is returned.
     *
     * @return {Panel} Returns the visible/open menu or nil.
     */
    function GetOpenMenu(): Panel;

    /**
     * Returns whether or not the background should be painted. Is the same as DMenuBar:GetDrawBackground
     *
     * @return {boolean} Should the background be painted
     */
    function GetPaintBackground(): boolean;

    /**
     * Sets the background color
     *
     * @arg Color color - See Color
     */
    function SetBackgroundColor(color: Color): void;

    /**
     * Sets whether or not the panel is disabled
     *
     * @arg boolean disable - Should be disabled or not
     */
    function SetDisabled(disable: boolean): void;

    /**
     * Sets whether or not the background should be painted. Is the same as DMenuBar:SetPaintBackground
     *
     * @arg boolean shouldPaint - Should the background be painted
     */
    function SetDrawBackground(shouldPaint: boolean): void;

    /**
     * Sets whether or not the panel is a menu. Used for closing menus when another panel is selected.
     *
     * @arg boolean isMenu - Is this a menu
     */
    function SetIsMenu(isMenu: boolean): void;

    /**
     * Sets whether or not the background should be painted. Is the same as DMenuBar:SetDrawBackground
     *
     * @arg boolean shouldPaint - Should the background be painted
     */
    function SetPaintBackground(shouldPaint: boolean): void;
}
