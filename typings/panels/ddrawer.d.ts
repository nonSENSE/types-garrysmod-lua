/** @noSelfInFile */
declare namespace DDrawer {
    /**
     * Closes the DDrawer.
     */
    function Close(): void;

    /**
     * Return the Open Size of DDrawer.
     *
     * @return {number} Open size.
     */
    function GetOpenSize(): number;

    /**
     * Return the Open Time of DDrawer.
     *
     * @return {number} Time in seconds.
     */
    function GetOpenTime(): number;

    /**
     * Opens the DDrawer.
     */
    function Open(): void;

    /**
     * Set the height of DDrawer
     *
     * @arg number Value - Height of DDrawer. Default is 100.
     */
    function SetOpenSize(Value: number): void;

    /**
     * Set the time (in seconds) for DDrawer to open.
     *
     * @arg number value - Length in seconds. Default is 0.3
     */
    function SetOpenTime(value: number): void;

    /**
     * Toggles the DDrawer.
     */
    function Toggle(): void;
}
