/** @noSelfInFile */
declare namespace DBinder {
    /**
     * Gets the code of the key currently bound by the DBinder. Same as DBinder:GetValue.
     *
     * @return {number} The key code of the bound key. See Enums/KEY.
     */
    function GetSelectedNumber(): number;

    /**
     * Gets the code of the key currently bound by the DBinder. Same as DBinder:GetSelectedNumber.
     *
     * @return {number} The key code of the bound key. See Enums/KEY.
     */
    function GetValue(): number;

    /**
     * Called when the player selects a new bind.
     *
     * @arg number iNum - The new bound key. See input.GetKeyName.
     */
    function OnChange(iNum: number): void;

    /**
     * Sets the current key bound by the DBinder, and updates the button's text as well as the ConVar.
     *
     * @arg number keyCode - The key code of the key to bind. See Enums/KEY.
     */
    function SetSelectedNumber(keyCode: number): void;

    /**
     * Alias of DBinder:SetSelectedNumber.
     *
     * @arg number keyCode - The key code of the key to bind. See Enums/KEY.
     */
    function SetValue(keyCode: number): void;

    /**
     * Used to set the text of the DBinder to the current key binding, or NONE.
     */
    function UpdateText(): void;
}
