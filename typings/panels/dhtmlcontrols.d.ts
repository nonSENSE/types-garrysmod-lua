/** @noSelfInFile */
declare namespace DHTMLControls {
    /**
     *
     */
    function FinishedLoading(): void;

    /**
     * Sets the color of the navigation buttons.
     *
     * @arg Color clr - A Color
     */
    function SetButtonColor(clr: Color): void;

    /**
     * Sets the DHTML element to control with these DHTMLControls.
     *
     * @arg Panel dhtml - The HTML panel
     */
    function SetHTML(dhtml: Panel): void;

    /**
     *
     */
    function StartedLoading(): void;

    /**
     * Basically adds an URL to the history.
     *
     * @arg string url
     */
    function UpdateHistory(url: string): void;

    /**
     *
     */
    function UpdateNavButtonStatus(): void;
}
