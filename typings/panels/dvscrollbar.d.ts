/** @noSelfInFile */
declare namespace DVScrollBar {
    /**
     * Adds specified amount of scroll in pixels.
     *
     * @arg number add - How much to scroll downwards. Can be negative for upwards scroll
     * @return {boolean} True if the scroll level was changed (i.e. if we did or did not scroll)
     */
    function AddScroll(add: number): boolean;

    /**
     * Smoothly scrolls to given level.
     *
     * @arg number scroll - The scroll level to animate to. In pixels from the top ( from 0 )
     * @arg number length - Length of the animation in seconds
     * @arg number [delay=0] - Delay of the animation in seconds
     * @arg number [ease=-1] - See Panel:NewAnimation for explanation.
     */
    function AnimateTo(scroll: number, length: number, delay?: number, ease?: number): void;

    /**
     * No Description
     *
     * @return {number}
     */
    function BarScale(): number;

    /**
     * Returns whether or not the manual up/down scroll buttons are visible or not. Set by DVScrollBar:SetHideButtons.
     *
     * @return {boolean} Whether or not the manual up/down scroll buttons are visible or not.
     */
    function GetHideButtons(): boolean;

    /**
     * Returns the negative of DVScrollBar:GetScroll.
     *
     * @return {number}
     */
    function GetOffset(): number;

    /**
     * Returns the amount of scroll level from the top in pixels
     *
     * @return {number} The amount of scroll level from the top
     */
    function GetScroll(): number;

    /**
     * Called from within DScrollBarGrip
     */
    function Grip(): void;

    /**
     * Allows hiding the up and down buttons for better visual stylisation.
     *
     * @arg boolean hide - True to hide
     */
    function SetHideButtons(hide: boolean): void;

    /**
     * Sets the scroll level in pixels.
     *
     * @arg number scroll
     */
    function SetScroll(scroll: number): void;

    /**
     * Sets up the scrollbar for use.
     *
     * @arg number barSize - The size of the panel that holds the canvas, basically size of "1 page".
     * @arg number canvasSize - The total size of the canvas, this typically is the bigger number.
     */
    function SetUp(barSize: number, canvasSize: number): void;

    /**
     * Should return nil in all cases. See DVScrollBar:GetScroll.
     *
     * @return {any}
     */
    function Value(): any;
}
