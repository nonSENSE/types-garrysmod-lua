/** @noSelfInFile */
declare namespace TextEntry {
    /**
     * Called from engine whenever a valid character is typed while the text entry is focused.
     *
     * @arg number keyCode - They key code of the key pressed, see Enums/KEY.
     * @return {boolean} Whether you've handled the key press. Returning true prevents the default text entry behavior from occurring.
     */
    function OnKeyCodeTyped(keyCode: number): boolean;
}
