/** @noSelfInFile */
declare namespace DCheckBoxLabel {
    /**
     * Gets the checked state of the checkbox. This calls the checkbox's DCheckBox:GetChecked function.
     *
     * @return {boolean} Whether the box is checked or not.
     */
    function GetChecked(): boolean;

    /**
     * Gets the indentation of the element on the X axis.
     *
     * @return {number} How much the content is moved to the right in pixels
     */
    function GetIndent(): number;

    /**
     * Called when the "checked" state is changed.
     *
     * @arg boolean bVal - Whether the checkbox is checked or unchecked.
     */
    function OnChange(bVal: boolean): void;

    /**
     * Sets the color of the DCheckBoxLabel's text to the bright text color defined in the skin.
     *
     * @arg boolean bright - true makes the text bright.
     */
    function SetBright(bright: boolean): void;

    /**
     * Sets the checked state of the checkbox. Does not call DCheckBoxLabel:OnChange or Panel:ConVarChanged, unlike DCheckBoxLabel:SetValue.
     *
     * @arg boolean checked - Whether the box should be checked or not.
     */
    function SetChecked(checked: boolean): void;

    /**
     * Sets the console variable to be set when the checked state of the DCheckBoxLabel changes.
     *
     * @arg string convar - The name of the convar to set
     */
    function SetConVar(convar: string): void;

    /**
     * Sets the text of the DCheckBoxLabel to be dark colored.
     *
     * @arg boolean darkify - True to be dark, false to be default
     */
    function SetDark(darkify: boolean): void;

    /**
     * Sets the font of the text part of the DCheckBoxLabel.
     *
     * @arg string font - Font name
     */
    function SetFont(font: string): void;

    /**
     * Sets the indentation of the element on the X axis.
     *
     * @arg number ident - How much in pixels to move the content to the right
     */
    function SetIndent(ident: number): void;

    /**
     * Sets the text color for the DCheckBoxLabel.
     *
     * @arg Color color - The text color. Uses the Color.
     */
    function SetTextColor(color: Color): void;

    /**
     * Sets the checked state of the checkbox, and calls DCheckBoxLabel:OnChange and the checkbox's Panel:ConVarChanged methods.
     *
     * @arg boolean checked - Whether the box should be checked or not (1 or 0 can also be used).
     */
    function SetValue(checked: boolean): void;

    /**
     * Toggles the checked state of the DCheckBoxLabel.
     */
    function Toggle(): void;
}
