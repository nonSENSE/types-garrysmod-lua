/** @noSelfInFile */
declare namespace DAdjustableModelPanel {
    /**
     * Used by the panel to perform mouse capture operations when adjusting the model.
     */
    function CaptureMouse(): void;

    /**
     * Used to adjust the perspective in the model panel via the keyboard, when the right mouse button is used.
     */
    function FirstPersonControls(): void;

    /**
     * Gets whether mouse and keyboard-based adjustment of the perspective has been enabled. See DAdjustableModelPanel:SetFirstPerson for more information.
     *
     * @return {boolean} Whether first person controls are enabled. See DAdjustableModelPanel:FirstPersonControls.
     */
    function GetFirstPerson(): boolean;

    /**
     * Enables mouse and keyboard-based adjustment of the perspective.
     *
     * @arg boolean enable - Whether to enable/disable first person controls. See DAdjustableModelPanel:FirstPersonControls.
     */
    function SetFirstPerson(enable: boolean): void;
}
