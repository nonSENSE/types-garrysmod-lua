/** @noSelfInFile */
declare namespace DScrollPanel {
    /**
     * Parents the passed panel to the DScrollPanel's canvas.
     *
     * @arg Panel pnl - The panel to add.
     */
    function AddItem(pnl: Panel): void;

    /**
     * Clears the DScrollPanel's canvas, removing all added items.
     */
    function Clear(): void;

    /**
     * Returns the canvas ( The panel all child panels are parented to ) of the DScrollPanel.
     *
     * @return {Panel} The canvas
     */
    function GetCanvas(): Panel;

    /**
     * Gets the DScrollPanels padding
     *
     * @return {number} DScrollPanels padding
     */
    function GetPadding(): number;

    /**
     * Returns the vertical scroll bar of the panel.
     *
     * @return {Panel} The DVScrollBar.
     */
    function GetVBar(): Panel;

    /**
     * Return the width of the DScrollPanel's canvas.
     *
     * @return {number} The width of the DScrollPanel's canvas
     */
    function InnerWidth(): number;

    /**
     *
     */
    function Rebuild(): void;

    /**
     * Scrolls to the given child
     *
     * @arg Panel panel - The panel to scroll to, must be a child of the DScrollPanel.
     */
    function ScrollToChild(panel: Panel): void;

    /**
     * Sets the canvas of the DScrollPanel.
     *
     * @arg Panel canvas - The new canvas
     */
    function SetCanvas(canvas: Panel): void;

    /**
     * Sets the DScrollPanel's padding. This function appears to be unused.
     *
     * @arg number padding - The padding of the DScrollPanel.
     */
    function SetPadding(padding: number): void;
}
