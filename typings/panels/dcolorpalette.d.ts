/** @noSelfInFile */
declare namespace DColorPalette {
    /**
     * Basically the same functionality as DColorPalette:OnValueChanged, you should use that instead!
     *
     * @arg Color clr - The new color via the Color
     * @arg Panel btn - The DColorButton that was pressed.
     */
    function DoClick(clr: Color, btn: Panel): void;

    /**
     * Returns the size of each palette button. Set by DColorPalette:SetButtonSize.
     *
     * @return {number} The size of each palette button
     */
    function GetButtonSize(): number;

    /**
     * Returns the ConVar name for the alpha channel of the color.
     *
     * @return {string} The ConVar name for the alpha channel of the color
     */
    function GetConVarA(): string;

    /**
     * Returns the ConVar name for the blue channel of the color.
     *
     * @return {string} The ConVar name for the blue channel of the color
     */
    function GetConVarB(): string;

    /**
     * Returns the ConVar name for the green channel of the color.
     *
     * @return {string} The ConVar name for the green channel of the color
     */
    function GetConVarG(): string;

    /**
     * Returns the ConVar name for the red channel of the color.
     *
     * @return {string} The ConVar name for the red channel of the color
     */
    function GetConVarR(): string;

    /**
     * Returns the number of rows of the palette, provided 6 colors fill each row. This value is equal to the number of colors in the DColorPalette divided by 6.
     *
     * @return {number} Number of rows of the DColorPalette.
     */
    function GetNumRows(): number;

    /**
     * Used internally to make sure changes on one palette affect other palettes with same name.
     */
    function NetworkColorChange(): void;

    /**
     * Called when a palette button has been pressed
     *
     * @arg Panel pnl - The DColorButton that was pressed.
     */
    function OnRightClickButton(pnl: Panel): void;

    /**
     * Called when the color is changed after clicking a new value.
     *
     * @arg any newcol - The new color of the DColorPalette
     */
    function OnValueChanged(newcol: any): void;

    /**
     * Resets this entire color palette to a default preset one, without saving.
     */
    function Reset(): void;

    /**
     * Resets this entire color palette to a default preset one and saves the changes.
     */
    function ResetSavedColors(): void;

    /**
     * Saves the color of given button across sessions.
     *
     * @arg Panel btn - The button to save the color of. Used to get the ID of the button.
     * @arg object clr - The color to save to this button's index
     */
    function SaveColor(btn: Panel, clr: object): void;

    /**
     * Sets the size of each palette button.
     *
     * @arg number size - Sets the new size
     */
    function SetButtonSize(size: number): void;

    /**
     * Currently does nothing. Intended to "select" the color.
     *
     * @arg object clr
     */
    function SetColor(clr: object): void;

    /**
     * Clears the palette and adds new buttons with given colors.
     *
     * @arg Color tab - A number indexed table where each value is a Color
     */
    function SetColorButtons(tab: Color): void;

    /**
     * Sets the ConVar name for the alpha channel of the color.
     *
     * @arg string convar - The ConVar name for the alpha channel of the color
     */
    function SetConVarA(convar: string): void;

    /**
     * Sets the ConVar name for the blue channel of the color.
     *
     * @arg string convar - The ConVar name for the blue channel of the color
     */
    function SetConVarB(convar: string): void;

    /**
     * Sets the ConVar name for the green channel of the color.
     *
     * @arg string convar - The ConVar name for the green channel of the color
     */
    function SetConVarG(convar: string): void;

    /**
     * Sets the ConVar name for the red channel of the color.
     *
     * @arg string convar - The ConVar name for the red channel of the color
     */
    function SetConVarR(convar: string): void;

    /**
     * Roughly sets the number of colors that can be picked by the user. If the DColorPalette is exactly 6 rows tall, this function will set the number of colors shown per row in the palette.
     *
     * @arg number rows - Scale for the range of colors that the user can pick. Default is 8.
     */
    function SetNumRows(rows: number): void;

    /**
 * Internal helper function for DColorPalette:UpdateConVars.
* 
* @arg string name - The name of the console variable to set
* @arg string key - The key of the 3rd argument to set the convar to
Possible values: "r", "g", "b", "a"
* @arg Color clr - The Color to retrieve the info from.
 */
    function UpdateConVar(name: string, key: string, clr: Color): void;

    /**
     * Updates all the console variables set by DColorPalette:SetConVarR and so on with given color.
     *
     * @arg Color clr - A Color
     */
    function UpdateConVars(clr: Color): void;
}
