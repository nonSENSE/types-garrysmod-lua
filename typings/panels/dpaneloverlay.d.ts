/** @noSelfInFile */
declare namespace DPanelOverlay {
    /**
     * Returns the border color of the DPanelOverlay set by DPanelOverlay:SetColor.
     *
     * @return {Color} The set color. Uses the Color.
     */
    function GetColor(): Color;

    /**
     * Returns the type of the DPanelOverlay set by DPanelOverlay:SetType.
     *
     * @return {number} The set type.
     */
    function GetType(): number;

    /**
     * Used internally by the panel for type 3.
     *
     * @arg object cola
     * @arg object colb
     * @arg object colc
     * @arg object cold
     * @arg number size
     */
    function PaintDifferentColours(cola: object, colb: object, colc: object, cold: object, size: number): void;

    /**
     * Used internally by the panel for types 1 and 2.
     *
     * @arg number size
     */
    function PaintInnerCorners(size: number): void;

    /**
     * Sets the border color of the DPanelOverlay.
     *
     * @arg Color color - The color to set. Uses the Color.
     */
    function SetColor(color: Color): void;

    /**
 * Sets the type of the DPanelOverlay.
* 
* @arg number type - The type to set.
Possible value are:

1 - 8px corners of given color
2 - 4px corners of given type
3 - 2 top? corners of hardcoded color, 2 other corners of given color
 */
    function SetType(type: number): void;
}
