/** @noSelfInFile */
declare namespace DCollapsibleCategory {
    /**
     * Adds a new text button to the collapsible category, like the tool menu in Spawnmenu.
     *
     * @arg string name - The name of the button
     * @return {Panel} The DButton
     */
    function Add(name: string): Panel;

    /**
     * Internal function that handles the open/close animations.
     *
     * @arg object anim
     * @arg number delta
     * @arg object data
     */
    function AnimSlide(anim: object, delta: number, data: object): void;

    /**
     * Forces the category to open or collapse
     *
     * @arg boolean expand - True to open, false to collapse
     */
    function DoExpansion(expand: boolean): void;

    /**
     * Returns the expand/collapse animation time set by DCollapsibleCategory:SetAnimTime.
     *
     * @return {number} The animation time in seconds
     */
    function GetAnimTime(): number;

    /**
     * Returns whether the DCollapsibleCategory is expanded or not.
     *
     * @return {boolean} If expanded it will return true.
     */
    function GetExpanded(): boolean;

    /**
     * Returns the header height of the DCollapsibleCategory.
     *
     * @return {number} The current height of the header.
     */
    function GetHeaderHeight(): number;

    /**
     * No Description
     *
     * @return {Panel}
     */
    function GetList(): Panel;

    /**
     * Doesn't actually do anything.
     *
     * @return {number}
     */
    function GetPadding(): number;

    /**
     * Returns whether or not the background should be painted.
     *
     * @return {boolean} If the background is painted or not
     */
    function GetPaintBackground(): boolean;

    /**
     * No Description
     *
     * @return {number}
     */
    function GetStartHeight(): number;

    /**
     * Called by DCollapsibleCategory:Toggle. This function does nothing by itself, as you're supposed to overwrite it.
     *
     * @arg boolean expanded - If it was expanded or not
     */
    function OnToggle(expanded: boolean): void;

    /**
     * Sets the time in seconds it takes to expand the DCollapsibleCategory
     *
     * @arg number time - The time in seconds it takes to expand
     */
    function SetAnimTime(time: number): void;

    /**
     * Sets the contents of the DCollapsibleCategory.
     *
     * @arg Panel pnl - The panel, containing the contents for the DCollapsibleCategory, mostly an DScrollPanel
     */
    function SetContents(pnl: Panel): void;

    /**
     * Sets whether the DCollapsibleCategory is expanded or not upon opening the container.
     *
     * @arg boolean [expanded=true] - Whether it shall be expanded or not by default
     */
    function SetExpanded(expanded?: boolean): void;

    /**
     * Sets the header height of the DCollapsibleCategory.
     *
     * @arg number height - The new height to set. Default height is 20.
     */
    function SetHeaderHeight(height: number): void;

    /**
     * Sets the name of the DCollapsibleCategory.
     *
     * @arg string label - The label/name of the DCollapsibleCategory.
     */
    function SetLabel(label: string): void;

    /**
     * No Description
     *
     * @arg Panel pnl
     */
    function SetList(pnl: Panel): void;

    /**
     * Doesn't actually do anything.
     *
     * @arg number padding
     */
    function SetPadding(padding: number): void;

    /**
     * Sets whether or not the background should be painted.
     *
     * @arg boolean paint
     */
    function SetPaintBackground(paint: boolean): void;

    /**
     * No Description
     *
     * @arg number height
     */
    function SetStartHeight(height: number): void;

    /**
     * Toggles the expanded state of the DCollapsibleCategory.
     */
    function Toggle(): void;

    /**
     * No Description
     */
    function UnselectAll(): void;

    /**
     * Used internally to update the "AltLine" property on all "child" panels.
     */
    function UpdateAltLines(): void;
}
