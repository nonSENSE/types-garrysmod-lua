/** @noSelfInFile */
declare namespace DCheckBox {
    /**
     * Gets the checked state of the checkbox.
     *
     * @return {boolean} Whether the box is checked or not.
     */
    function GetChecked(): boolean;

    /**
     * Returns whether the state of the checkbox is being edited. This means whether the user is currently clicking (mouse-down) on the checkbox, and applies to both the left and right mouse buttons.
     *
     * @return {boolean} Whether the checkbox is being clicked.
     */
    function IsEditing(): boolean;

    /**
     * Called when the "checked" state is changed.
     *
     * @arg boolean bVal - Whether the CheckBox is checked or not.
     */
    function OnChange(bVal: boolean): void;

    /**
     * Sets the checked state of the checkbox. Does not call the checkbox's DCheckBox:OnChange and Panel:ConVarChanged methods, unlike DCheckBox:SetValue.
     *
     * @arg boolean checked - Whether the box should be checked or not.
     */
    function SetChecked(checked: boolean): void;

    /**
     * Sets the checked state of the checkbox, and calls the checkbox's DCheckBox:OnChange and Panel:ConVarChanged methods.
     *
     * @arg boolean checked - Whether the box should be checked or not.
     */
    function SetValue(checked: boolean): void;

    /**
     * Toggles the checked state of the checkbox, and calls the checkbox's DCheckBox:OnChange and Panel:ConVarChanged methods. This is called by DCheckBox:DoClick.
     */
    function Toggle(): void;
}
