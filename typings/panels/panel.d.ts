/** @noSelfInFile */
declare namespace Panel {
    /**
 * Sets a new image to be loaded by a TGAImage.
* 
* @arg string imageName - The file path.
* @arg string strPath - The PATH to search in. See File Search Paths.
This isn't used internally.
 */
    function LoadTGAImage(imageName: string, strPath: string): void;
}
