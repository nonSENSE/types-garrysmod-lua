/** @noSelfInFile */
declare namespace DImageButton {
    /**
     * Returns the "image" of the DImageButton. Equivalent of DImage:GetImage.
     *
     * @return {string} The path to the image that is loaded.
     */
    function GetImage(): string;

    /**
     * Returns whether the image inside the button should be stretched to fit it or not
     *
     * @return {boolean}
     */
    function GetStretchToFit(): boolean;

    /**
     * Sets the color of the image. Equivalent of DImage:SetImageColor
     *
     * @arg any color - The Color to set
     */
    function SetColor(color: any): void;

    /**
     * Alias of DImageButton:SetImage.
     */
    function SetIcon(): void;

    /**
     * Sets the "image" of the DImageButton. Equivalent of DImage:SetImage.
     *
     * @arg string strImage - The path of the image to load. When no file extension is supplied the VMT file extension is used.
     * @arg string strBackup - The path of the backup image.
     */
    function SetImage(strImage: string, strBackup: string): void;

    /**
     * Hides or shows the image of the image button. Internally this calls Panel:SetVisible on the internal DImage.
     *
     * @arg boolean visible - Set true to make it visible ( default ), or false to hide the image
     */
    function SetImageVisible(visible: boolean): void;

    /**
     * Sets whether the DImageButton should keep the aspect ratio of its image. Equivalent of DImage:SetKeepAspect.
     *
     * @arg boolean keep - true to keep the aspect ratio, false not to
     */
    function SetKeepAspect(keep: boolean): void;

    /**
     * Sets a Material directly as an image. Equivalent of DImage:SetMaterial.
     *
     * @arg IMaterial mat - The material to set
     */
    function SetMaterial(mat: IMaterial): void;

    /**
     * See DImage:SetOnViewMaterial
     *
     * @arg string mat
     * @arg string backup
     */
    function SetOnViewMaterial(mat: string, backup: string): void;

    /**
     * Sets whether the image inside the DImageButton should be stretched to fill the entire size of the button, without preserving aspect ratio.
     *
     * @arg boolean stretch - True to stretch, false to not to stretch
     */
    function SetStretchToFit(stretch: boolean): void;
}
