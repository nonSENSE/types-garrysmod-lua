/** @noSelfInFile */
declare namespace ContentIcon {
    /**
     * Returns the color set by ContentIcon:SetColor
     *
     * @return {Color} See Color
     */
    function GetColor(): Color;

    /**
     * Returns the content type used to save and restore the content icon in a spawnlist.
     *
     * @return {string} The content type, for example "entity" or "weapon".
     */
    function GetContentType(): string;

    /**
     * Returns a table of weapon classes for the content icon with "NPC" content type to be randomly chosen from when user tries to spawn the NPC.
     *
     * @return {object} A table of weapon classes to be chosen from when user tries to spawn the NPC.
     */
    function GetNPCWeapon(): object;

    /**
     * Returns the internal "name" for the content icon, usually a class name for an entity.
     *
     * @return {string} Internal "name" to be used when user left clicks the icon.
     */
    function GetSpawnName(): string;

    /**
     * A hook for override, by default does nothing. Called when user right clicks on the content icon, you are supposed to open a DermaMenu here with additional options.
     */
    function OpenMenu(): void;

    /**
     * Sets whether the content item is admin only. This makes the icon to display a admin icon in the top left corner of the icon.
     *
     * @arg boolean adminOnly - Whether this content should be admin only or not
     */
    function SetAdminOnly(adminOnly: boolean): void;

    /**
     * Sets the color for the content icon. Currently is not used by the content icon panel.
     *
     * @arg Color clr - The color to set. See Color
     */
    function SetColor(clr: Color): void;

    /**
     * Sets the content type used to save and restore the content icon in a spawnlist.
     *
     * @arg string type - The content type, for example "entity" or "weapon"
     */
    function SetContentType(type: string): void;

    /**
     * Sets the material to be displayed as the content icon.
     *
     * @arg string path - Path to the icon to use.
     */
    function SetMaterial(path: string): void;

    /**
     * Sets the tool tip and the "nice" name to be displayed by the content icon.
     *
     * @arg string name - "Nice" name to display.
     */
    function SetName(name: string): void;

    /**
     * Sets a table of weapon classes for the content icon with "NPC" content type to be randomly chosen from when user tries to spawn the NPC.
     *
     * @arg object weapons - A table of weapon classes to be chosen from when user tries to spawn the NPC.
     */
    function SetNPCWeapon(weapons: object): void;

    /**
     * Sets the internal "name" for the content icon, usually a class name for an entity.
     *
     * @arg string name - Internal "name" to be used when user left clicks the icon.
     */
    function SetSpawnName(name: string): void;
}
