/** @noSelfInFile */
declare namespace ControlPanel {
    /**
 * Adds a control to the control panel.
* 
* @arg string type - The control type to add. The complete list is:

header
textbox
label
checkbox/toggle
slider
propselect
matselect
ropematerial
button
numpad
color
combobox
listbox
materialgallery
* @arg object controlinfo - Each control takes their own table structure. You may search "AddControl" on GitHub for examples.
Here is a full list of each type and the table members it requires:

header

description


textbox:

label (def: "Untitled")
command


label:

text


checkbox, toggle (same thing):

label (def: "Untitled")
command
help (boolean, if true assumes label is a language string (#tool.toolname.stuff) and adds .help at the end)


slider: (DForm:NumSlider)

type (optional string, if equals float then 2 digits after the decimal will be used, otherwise 0)
label (def: Untitled)
command
min (def: 0)
max (def: 100)
help (boolean, see above)


propselect:

(data goes directly to PropSelect's :ControlValues(data))


matselect:

(data goes directly to MatSelect's :ControlValues(data))


ropematerial:

convar (notice: NOT called command this time!)


button:

label / text (if label is missing will use text. Def: No Label)
command


numpad:

command
command2
label
label2


color:

label
red (convar)
green (convar)
blue (convar)
alpha (convar)


combobox:

menubutton (if doesn't equal "1", becomes a listbox)
folder
options (optional, ha)
cvars (optional)


listbox:

height (if set, becomes DListView, otherwise is CtrlListBox)
label (def: unknown)
options (optional)


materialgallery:

width (def: 32)
height (def: 32)
rows (def: 4)
convar
options
 */
    function AddControl(type: string, controlinfo: object): void;

    /**
     * Adds an item by calling DForm:AddItem.
     *
     * @arg Panel panel - Panel to add as an item to the control panel.
     */
    function AddPanel(panel: Panel): void;

    /**
     * Creates a CtrlColor (a color picker) panel and adds it as an item.
     *
     * @arg string label - The label for this color picker.
     * @arg string convarR - Name of the convar that will store the R component of the selected color.
     * @arg string convarG - Name of the convar that will store the G component of the selected color.
     * @arg string convarB - Name of the convar that will store the B component of the selected color.
     * @arg string convarA - Name of the convar that will store the A component of the selected color.
     * @return {Panel} The created CtrlColor panel.
     */
    function ColorPicker(label: string, convarR: string, convarG: string, convarB: string, convarA: string): Panel;

    /**
 * Sets control values of the control panel.
* 
* @arg object data - A two-membered table:

boolean closed - Sets if the control panel should be unexpanded.
string label - The text to display inside the control's label.
 */
    function ControlValues(data: object): void;

    /**
 * Calls the given function with this panel as the only argument. Used by the spawnmenu to populate the control panel.
* 
* @arg GMLua.CallbackNoContext func - A function that takes one argument:

ControlPanel panelToPopulate
 */
    function FillViaFunction(func: GMLua.CallbackNoContext): void;

    /**
     * Returns this control panel.
     *
     * @return {any} The same control panel the function is being called on.
     */
    function GetEmbeddedPanel(): any;

    /**
     * Creates a CtrlNumPad (a Sandbox key binder) panel and adds it as an item.
     *
     * @arg string label1 - The label for the left key binder.
     * @arg string convar1 - The name of the convar that will store the key code for player selected key of the left key binder.
     * @arg string [label2="nil"] - If set and convar2 is set, the label for the right key binder.
     * @arg string [convar2="nil"] - If set and label2 is set, the name of the convar that will store the key code for player selected key of the right key binder.
     * @return {Panel} The created CtrlNumPad panel.
     */
    function KeyBinder(label1: string, convar1: string, label2?: string, convar2?: string): Panel;

    /**
     * Creates a MatSelect panel and adds it as an item.
     *
     * @arg string convar - Calls MatSelect:SetConVar with this value.
     * @arg object [options=nil] - If specified, calls MatSelect:AddMaterial(key, value) for each table entry. If the table key is a number, the function will instead be called with the value as both arguments.
     * @arg boolean [autostretch=false] - If specified, calls MatSelect:SetAutoHeight with this value.
     * @arg number [width=NaN] - If specified, calls MatSelect:SetItemWidth with this value.
     * @arg number [height=NaN] - If specified, calls MatSelect:SetItemHeight with this value.
     * @return {any} The created MatSelect panel.
     */
    function MatSelect(convar: string, options?: object, autostretch?: boolean, width?: number, height?: number): any;

    /**
     * Creates a ControlPresets panel and adds it as an item.
     *
     * @arg string group - The preset group. Must be unique.
     * @arg object cvarList - A table of convar names as keys and their defaults as the values. Typically the output of Tool:BuildConVarList.
     * @return {Panel} The created ControlPresets panel.
     */
    function ToolPresets(group: string, cvarList: object): Panel;
}
