/** @noSelfInFile */
declare namespace DButton {
    /**
     * Returns value set by DButton:SetDrawBorder. See that page for more info.
     *
     * @return {boolean} value set by DButton:SetDrawBorder.
     */
    function GetDrawBorder(): boolean;

    /**
     * Returns true if the DButton is currently depressed (a user is clicking on it).
     *
     * @return {boolean} Whether or not the button is depressed.
     */
    function IsDown(): boolean;

    /**
     * Sets a console command to be called when the button is clicked.
     *
     * @arg string command - The console command to be called.
     * @arg string args - The arguments for the command.
     */
    function SetConsoleCommand(command: string, args: string): void;

    /**
     * Sets whether or not the DButton is disabled.
     *
     * @arg boolean disable - When true Enable the button, when false Disable the button.
     */
    function SetDisabled(disable: boolean): void;

    /**
     * Does absolutely nothing at all. Default value is automatically set to true.
     *
     * @arg boolean draw - Does nothing.
     */
    function SetDrawBorder(draw: boolean): void;

    /**
     * Sets an image to be displayed as the button's background. Alias of DButton:SetImage
     *
     * @arg string [img="nil"] - The image file to use, relative to /materials. If this is nil, the image background is removed.
     */
    function SetIcon(img?: string): void;

    /**
     * Sets an image to be displayed as the button's background.
     *
     * @arg string [img="nil"] - The image file to use, relative to /materials. If this is nil, the image background is removed.
     */
    function SetImage(img?: string): void;

    /**
     * Sets an image to be displayed as the button's background.
     *
     * @arg IMaterial [img=nil] - The material to use. If this is nil, the image background is removed.
     */
    function SetMaterial(img?: IMaterial): void;

    /**
     * A hook called from within DLabel's PANEL:ApplySchemeSettings to determine the color of the text on display.
     *
     * @arg object skin - A table supposed to contain the color values listed above.
     */
    function UpdateColours(skin: object): void;
}
