/** @noSelfInFile */
declare namespace DShape {
    /**
 * Returns the current type of shape this panel is set to display.
* 
* @return {Color} The border color
See Color
 */
    function GetBorderColor(): Color;

    /**
     * Returns the color set to display the shape with.
     *
     * @return {Color} The Color
     */
    function GetColor(): Color;

    /**
     * Returns the current type of shape this panel is set to display.
     *
     * @return {string}
     */
    function GetType(): string;

    /**
 * Sets the border color of the shape.
* 
* @arg Color clr - The desired border color.
See Color
 */
    function SetBorderColor(clr: Color): void;

    /**
     * Sets the color to display the shape with.
     *
     * @arg Color clr - The Color
     */
    function SetColor(clr: Color): void;

    /**
     * Sets the shape to be drawn.
     *
     * @arg string type - The render type of the DShape. Only rectangles (Rect) work currently. If you don't define a type immediately, the PANEL:Paint method will generate errors until you do.
     */
    function SetType(type: string): void;
}
