/** @noSelfInFile */
declare namespace DBubbleContainer {
    /**
     * Sets the speech bubble position and size along with the dialog point position.
     *
     * @arg number x - The x position of the dialog point. If this is set to a value greater than half of the set width, the entire bubble container will be moved in addition to the dialog point.
     * @arg number y - The y position of the bubble container. Has no effect unless set to a value greater than the set height + 64 pixels.
     * @arg number w - The width of the bubble container.
     * @arg number h - The height of the bubble container.
     */
    function OpenForPos(x: number, y: number, w: number, h: number): void;
}
