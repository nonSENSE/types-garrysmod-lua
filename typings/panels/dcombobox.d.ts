/** @noSelfInFile */
declare namespace DComboBox {
    /**
     * Called when the player opens the dropdown menu.
     *
     * @arg Panel menu - The DMenu menu panel.
     */
    function OnMenuOpened(menu: Panel): void;

    /**
     * Called when an option in the combo box is selected. This function does nothing by itself, you're supposed to overwrite it.
     *
     * @arg number index - The index of the option for use with other DComboBox functions.
     * @arg string value - The name of the option.
     * @arg any data - The data assigned to the option.
     */
    function OnSelect(index: number, value: string, data: any): void;

    /**
 * Adds a choice to the combo box.
* 
* @arg string value - The text show to the user.
* @arg any [data=nil] - The data accompanying this string. If left empty, the value argument is used instead.
Can be accessed with the second argument of DComboBox:GetSelected, DComboBox:GetOptionData and as an argument of DComboBox:OnSelect.
* @arg boolean [select=false] - Should this be the default selected text show to the user or not.
* @arg string [icon="nil"] - Adds an icon for this choice.
* @return {number} The index of the new option.
 */
    function AddChoice(value: string, data?: any, select?: boolean, icon?: string): number;

    /**
     * Adds a spacer below the currently last item in the drop down. Recommended to use with DComboBox:SetSortItems set to false.
     */
    function AddSpacer(): void;

    /**
     * Selects a combo box option by its index and changes the text displayed at the top of the combo box.
     *
     * @arg string value - The text to display at the top of the combo box.
     * @arg number index - The option index.
     */
    function ChooseOption(value: string, index: number): void;

    /**
     * Selects an option within a combo box based on its table index.
     *
     * @arg number index - Selects the option with given index.
     */
    function ChooseOptionID(index: number): void;

    /**
     * Clears the combo box's text value, choices, and data values.
     */
    function Clear(): void;

    /**
     * Closes the combo box menu. Called when the combo box is clicked while open.
     */
    function CloseMenu(): void;

    /**
     * Returns an option's data based on the given index.
     *
     * @arg number index - The option index.
     * @return {any} The option's data value.
     */
    function GetOptionData(index: number): any;

    /**
     * Returns an option's text based on the given index.
     *
     * @arg number index - The option index.
     * @return {string} The option's text value.
     */
    function GetOptionText(index: number): string;

    /**
 * Returns an option's text based on the given data.
* 
* @arg string data - The data to look up the name of.
If given a number and no matching data was found, the function will test given data against each tonumber'd data entry.
* @return {string} The option's text value.
If no matching data was found, the data itself will be returned. If multiple identical data entries exist, the first instance will be returned.
 */
    function GetOptionTextByData(data: string): string;

    /**
     * Returns the currently selected option's text and data
     *
     * @return {string} The option's text value.
     * @return {any} The option's stored data.
     */
    function GetSelected(): LuaMultiReturn<[string, any]>;

    /**
     * Returns the index (ID) of the currently selected option.
     *
     * @return {number} The ID of the currently selected option.
     */
    function GetSelectedID(): number;

    /**
     * Returns an whether the items in the dropdown will be alphabetically sorted or not.
     *
     * @return {boolean} True if enabled, false otherwise.
     */
    function GetSortItems(): boolean;

    /**
     * Returns whether or not the combo box's menu is opened.
     *
     * @return {boolean} True if the menu is open, false otherwise.
     */
    function IsMenuOpen(): boolean;

    /**
     * Opens the combo box drop down menu. Called when the combo box is clicked.
     */
    function OpenMenu(): void;

    /**
     * Sets whether or not the items should be sorted alphabetically in the dropdown menu of the DComboBox. If set to false, items will appear in the order they were added by DComboBox:AddChoice calls.
     *
     * @arg boolean sort - true to enable, false to disable
     */
    function SetSortItems(sort: boolean): void;

    /**
     * Sets the text shown in the combo box when the menu is not collapsed.
     *
     * @arg string value - The text in the DComboBox.
     */
    function SetValue(value: string): void;
}
