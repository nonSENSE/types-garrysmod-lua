/** @noSelfInFile */
declare namespace DSprite {
    /**
     * Gets the color the sprite is using as a modifier.
     *
     * @return {any} The Color being used.
     */
    function GetColor(): any;

    /**
     * No Description
     *
     * @return {Vector}
     */
    function GetHandle(): Vector;

    /**
     * Gets the material the sprite is using.
     *
     * @return {IMaterial} The material in use.
     */
    function GetMaterial(): IMaterial;

    /**
     * Gets the 2D rotation angle of the sprite, in the plane of the screen.
     *
     * @return {number} The anti-clockwise rotation in degrees.
     */
    function GetRotation(): number;

    /**
     * Sets the color modifier for the sprite.
     *
     * @arg any color - The Color to use.
     */
    function SetColor(color: any): void;

    /**
     * Seems to be an unused feature. Does nothing.
     *
     * @arg Vector vec
     */
    function SetHandle(vec: Vector): void;

    /**
     * Sets the source material for the sprite.
     *
     * @arg IMaterial material - The material to use. This will ideally be an UnlitGeneric.
     */
    function SetMaterial(material: IMaterial): void;

    /**
     * Sets the 2D rotation angle of the sprite, in the plane of the screen.
     *
     * @arg number ang - The anti-clockwise rotation in degrees.
     */
    function SetRotation(ang: number): void;
}
