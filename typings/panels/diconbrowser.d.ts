/** @noSelfInFile */
declare namespace DIconBrowser {
    /**
     * Automatically called to fill the browser with icons. Will not work if DIconBrowser:SetManual is set to true.
     */
    function Fill(): void;

    /**
     * A simple unused search feature, hides all icons that do not contain given text in their file path.
     *
     * @arg string text - The text to search for
     */
    function FilterByText(text: string): void;

    /**
     * Returns whether or not the browser should fill itself with icons.
     *
     * @return {boolean}
     */
    function GetManual(): boolean;

    /**
     * Returns the currently selected icon's file path.
     *
     * @return {string} The currently selected icon's file path.
     */
    function GetSelectedIcon(): string;

    /**
     * Called when the selected icon was changed. Use DIconBrowser:GetSelectedIcon to get the selected icon's filepath.
     */
    function OnChange(): void;

    /**
     * Use DIconBrowser:OnChange instead
     */
    function OnChangeInternal(): void;

    /**
     * Scrolls the browser to the selected icon
     */
    function ScrollToSelected(): void;

    /**
     * Selects an icon from file path
     *
     * @arg string icon - The file path of the icon to select. Do not include the "materials/" part.
     */
    function SelectIcon(icon: string): void;

    /**
     * Sets whether or not the browser should automatically fill itself with icons.
     *
     * @arg boolean manual
     */
    function SetManual(manual: boolean): void;

    /**
     * Set the currently selected file path. Do not use. Use DIconBrowser:SelectIcon instead.
     *
     * @arg string str
     */
    function SetSelectedIcon(str: string): void;
}
