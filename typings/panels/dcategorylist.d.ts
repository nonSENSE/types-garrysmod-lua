/** @noSelfInFile */
declare namespace DCategoryList {
    /**
     * Adds a DCollapsibleCategory to the list.
     *
     * @arg string categoryName - The name of the category to add.
     * @return {Panel} The created DCollapsibleCategory
     */
    function Add(categoryName: string): Panel;

    /**
     * Adds an element to the list.
     *
     * @arg Panel element - VGUI element to add to the list.
     */
    function AddItem(element: Panel): void;

    /**
     * Calls Panel:UnselectAll on all child elements, if they have it.
     */
    function UnselectAll(): void;
}
