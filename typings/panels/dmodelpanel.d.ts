/** @noSelfInFile */
declare namespace DModelPanel {
    /**
     * Called when the entity of the DModelPanel was drawn.
     *
     * @arg Entity ent - The clientside entity of the DModelPanel that has been drawn.
     */
    function PostDrawModel(ent: Entity): void;

    /**
     * Called before the entity of the DModelPanel is drawn.
     *
     * @arg Entity ent - The clientside entity of the DModelPanel that has been drawn.
     * @return {boolean} Return false to stop the entity from being drawn. This will also cause DModelPanel:PostDrawModel to stop being called.
     */
    function PreDrawModel(ent: Entity): boolean;

    /**
     * Used by the DModelPanel's paint hook to draw the model and background.
     */
    function DrawModel(): void;

    /**
     * Returns the ambient lighting used on the rendered entity.
     *
     * @return {object} The color of the ambient lighting.
     */
    function GetAmbientLight(): object;

    /**
     * Returns whether or not the panel entity should be animated when the default DModelPanel:LayoutEntity function is called.
     *
     * @return {boolean} True if the panel entity can be animated with Entity:SetSequence directly, false otherwise.
     */
    function GetAnimated(): boolean;

    /**
     * Returns the animation speed of the panel entity, see DModelPanel:SetAnimSpeed.
     *
     * @return {number} The animation speed.
     */
    function GetAnimSpeed(): number;

    /**
     * Returns the position of the model viewing camera.
     *
     * @return {Vector} The position of the camera.
     */
    function GetCamPos(): Vector;

    /**
     * Returns the color of the rendered entity.
     *
     * @return {Color} The color of the entity, see Color.
     */
    function GetColor(): Color;

    /**
     * Returns the entity being rendered by the model panel.
     *
     * @return {CSEnt} The rendered entity (client-side)
     */
    function GetEntity(): CSEnt;

    /**
     * Returns the FOV (field of view) the camera is using.
     *
     * @return {number} The FOV of the camera.
     */
    function GetFOV(): number;

    /**
     * Returns the angles of the model viewing camera. Is nil until changed with DModelPanel:SetLookAng.
     *
     * @return {Angle} The angles of the camera.
     */
    function GetLookAng(): Angle;

    /**
     * Returns the position the viewing camera is pointing toward.
     *
     * @return {Vector} The position the camera is pointing toward.
     */
    function GetLookAt(): Vector;

    /**
     * Gets the model of the rendered entity.
     *
     * @return {string} The model of the rendered entity.
     */
    function GetModel(): string;

    /**
     * By default, this function slowly rotates and animates the entity being rendered.
     *
     * @arg Entity entity - The entity that is being rendered.
     */
    function LayoutEntity(entity: Entity): void;

    /**
     * This function is used in the DModelPanel:LayoutEntity. It will set the active model to the last set animation using Entity:SetSequence. By default, it is the walking animation.
     */
    function RunAnimation(): void;

    /**
     * Sets the ambient lighting used on the rendered entity.
     *
     * @arg object color - The color of the ambient lighting.
     */
    function SetAmbientLight(color: object): void;

    /**
     * Sets whether or not to animate the entity when the default DModelPanel:LayoutEntity is called.
     *
     * @arg boolean animated - True to animate, false otherwise.
     */
    function SetAnimated(animated: boolean): void;

    /**
     * Sets the speed used by DModelPanel:RunAnimation to advance frame on an entity sequence.
     *
     * @arg number animSpeed - The animation speed.
     */
    function SetAnimSpeed(animSpeed: number): void;

    /**
     * Sets the position of the camera.
     *
     * @arg Vector pos - The position to set the camera at.
     */
    function SetCamPos(pos: Vector): void;

    /**
     * Sets the color of the rendered entity.
     *
     * @arg object color - The render color of the entity.
     */
    function SetColor(color: object): void;

    /**
     * Sets the directional lighting used on the rendered entity.
     *
     * @arg number direction - The light direction, see Enums/BOX.
     * @arg object color - The color of the directional lighting.
     */
    function SetDirectionalLight(direction: number, color: object): void;

    /**
     * Sets the entity to be rendered by the model panel.
     *
     * @arg Entity ent - The new panel entity.
     */
    function SetEntity(ent: Entity): void;

    /**
     * Sets the panel camera's FOV (field of view).
     *
     * @arg number fov - The field of view value.
     */
    function SetFOV(fov: number): void;

    /**
     * Sets the angles of the camera.
     *
     * @arg Angle ang - The angles to set the camera to.
     */
    function SetLookAng(ang: Angle): void;

    /**
     * Makes the panel's camera face the given position. Basically sets the camera's angles (DModelPanel:SetLookAng) after doing some math.
     *
     * @arg Vector pos - The position to orient the camera toward.
     */
    function SetLookAt(pos: Vector): void;

    /**
     * Sets the model of the rendered entity.
     *
     * @arg string model - The model to apply to the entity.
     */
    function SetModel(model: string): void;

    /**
     * No Description
     *
     * @arg string path - The path to the scene file. (.vcd)
     */
    function StartScene(path: string): void;
}
