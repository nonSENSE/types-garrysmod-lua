declare class __CNavLadderClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Connects this ladder to a CNavArea with a one way connection. ( From this ladder to the target area ).
     *
     * @arg CNavArea area - The area this ladder leads to.
     */
    public ConnectTo(area: CNavArea): void;

    /**
     * Disconnects this ladder from given area in a single direction.
     *
     * @arg CNavArea area - The CNavArea this to disconnect from.
     */
    public Disconnect(area: CNavArea): void;

    /**
     * Returns the bottom most position of the ladder.
     *
     * @return {Vector} The bottom most position of the ladder.
     */
    public GetBottom(): Vector;

    /**
     * Returns the bottom area of the CNavLadder.
     *
     * @return {CNavArea}
     */
    public GetBottomArea(): CNavArea;

    /**
     * Returns this CNavLadders unique ID.
     *
     * @return {number} The unique ID.
     */
    public GetID(): number;

    /**
     * Returns the length of the ladder.
     *
     * @return {number} The length of the ladder.
     */
    public GetLength(): number;

    /**
     * Returns the direction of this CNavLadder. ( The direction in which players back will be facing if they are looking directly at the ladder )
     *
     * @return {Vector} The direction of this CNavLadder.
     */
    public GetNormal(): Vector;

    /**
     * Returns the world position based on given height relative to the ladder.
     *
     * @arg number height - The Z position in world space coordinates.
     * @return {Vector} The closest point on the ladder to that height.
     */
    public GetPosAtHeight(height: number): Vector;

    /**
     * Returns the topmost position of the ladder.
     *
     * @return {Vector} The topmost position of the ladder.
     */
    public GetTop(): Vector;

    /**
     * Returns the top behind CNavArea of the CNavLadder.
     *
     * @return {CNavArea} The top behind CNavArea of the CNavLadder.
     */
    public GetTopBehindArea(): CNavArea;

    /**
     * Returns the top forward CNavArea of the CNavLadder.
     *
     * @return {CNavArea} The top forward CNavArea of the CNavLadder.
     */
    public GetTopForwardArea(): CNavArea;

    /**
     * Returns the top left CNavArea of the CNavLadder.
     *
     * @return {CNavArea} The top left CNavArea of the CNavLadder.
     */
    public GetTopLeftArea(): CNavArea;

    /**
     * Returns the top right CNavArea of the CNavLadder.
     *
     * @return {CNavArea} The top right CNavArea of the CNavLadder.
     */
    public GetTopRightArea(): CNavArea;

    /**
     * Returns the width of the ladder in Hammer Units.
     *
     * @return {number} The width of the ladder in Hammer Units.
     */
    public GetWidth(): number;

    /**
     * Returns whether this CNavLadder has an outgoing ( one or two way ) connection to given CNavArea in given direction.
     *
     * @arg CNavArea navArea - The CNavArea to test against.
     * @arg number navDirType - The direction, in which to look for the connection. See Enums/NavDir
     * @return {boolean} Whether this CNavLadder has an outgoing ( one or two way ) connection to given CNavArea in given direction.
     */
    public IsConnectedAtSide(navArea: CNavArea, navDirType: number): boolean;

    /**
     * Returns whether this CNavLadder is valid or not.
     *
     * @return {boolean} Whether this CNavLadder is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Removes the given nav ladder.
     */
    public Remove(): void;

    /**
     * Sets the bottom area of the CNavLadder.
     *
     * @arg CNavArea area
     */
    public SetBottomArea(area: CNavArea): void;

    /**
     * Sets the top behind area of the CNavLadder.
     *
     * @arg CNavArea area
     */
    public SetTopBehindArea(area: CNavArea): void;

    /**
     * Sets the top forward area of the CNavLadder.
     *
     * @arg CNavArea area
     */
    public SetTopForwardArea(area: CNavArea): void;

    /**
     * Sets the top left area of the CNavLadder.
     *
     * @arg CNavArea area
     */
    public SetTopLeftArea(area: CNavArea): void;

    /**
     * Sets the top right area of the CNavLadder.
     *
     * @arg CNavArea area
     */
    public SetTopRightArea(area: CNavArea): void;
}

declare type CNavLadder = __CNavLadderClass;
