declare class __CMoveDataClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds keys to the move data, as if player pressed them.
     *
     * @arg number keys - Keys to add, see Enums/IN
     */
    public AddKey(keys: number): void;

    /**
     * Gets the aim angle. Seems to be same as CMoveData:GetAngles.
     *
     * @return {Angle} Aiming angle
     */
    public GetAbsMoveAngles(): Angle;

    /**
     * Gets the aim angle. On client is the same as Entity:GetAngles.
     *
     * @return {Angle} Aiming angle
     */
    public GetAngles(): Angle;

    /**
     * Gets which buttons are down
     *
     * @return {number} An integer representing which buttons are down, see Enums/IN
     */
    public GetButtons(): number;

    /**
     * Returns the center of the player movement constraint. See CMoveData:SetConstraintCenter.
     *
     * @return {Vector} The constraint origin.
     */
    public GetConstraintCenter(): Vector;

    /**
     * Returns the radius that constrains the players movement. See CMoveData:SetConstraintRadius.
     *
     * @return {number} The constraint radius
     */
    public GetConstraintRadius(): number;

    /**
     * Returns the player movement constraint speed scale. See CMoveData:SetConstraintSpeedScale.
     *
     * @return {number} The constraint speed scale
     */
    public GetConstraintSpeedScale(): number;

    /**
     * Returns the width (distance from the edge of the radius, inward) where the actual movement constraint functions.
     *
     * @return {number} The constraint width
     */
    public GetConstraintWidth(): number;

    /**
     * Returns an internal player movement variable m_outWishVel.
     *
     * @return {Vector}
     */
    public GetFinalIdealVelocity(): Vector;

    /**
     * Returns an internal player movement variable m_outJumpVel.
     *
     * @return {Vector}
     */
    public GetFinalJumpVelocity(): Vector;

    /**
     * Returns an internal player movement variable m_outStepHeight.
     *
     * @return {number}
     */
    public GetFinalStepHeight(): number;

    /**
     * Returns the players forward speed.
     *
     * @return {number} speed
     */
    public GetForwardSpeed(): number;

    /**
     * Gets the number passed to "impulse" console command
     *
     * @return {number} The impulse
     */
    public GetImpulseCommand(): number;

    /**
     * Returns the maximum client speed of the player
     *
     * @return {number} The maximum client speed
     */
    public GetMaxClientSpeed(): number;

    /**
     * Returns the maximum speed of the player.
     *
     * @return {number} The maximum speed
     */
    public GetMaxSpeed(): number;

    /**
     * Returns the angle the player is moving at. For more info, see CMoveData:SetMoveAngles.
     *
     * @return {Angle} The move direction
     */
    public GetMoveAngles(): Angle;

    /**
     * Gets the aim angle. Only works clientside, server returns same as CMoveData:GetAngles.
     *
     * @return {Angle} The aim angle
     */
    public GetOldAngles(): Angle;

    /**
     * Get which buttons were down last frame
     *
     * @return {number} An integer representing which buttons were down, see Enums/IN
     */
    public GetOldButtons(): number;

    /**
     * Gets the player's position.
     *
     * @return {Vector} The player's position.
     */
    public GetOrigin(): Vector;

    /**
     * Returns the strafe speed of the player.
     *
     * @return {number} speed
     */
    public GetSideSpeed(): number;

    /**
     * Returns the vertical speed of the player. ( Z axis of CMoveData:GetVelocity )
     *
     * @return {number} Vertical speed
     */
    public GetUpSpeed(): number;

    /**
     * Gets the players velocity.
     *
     * @return {Vector} The players velocity
     */
    public GetVelocity(): Vector;

    /**
     * Returns whether the key is down or not
     *
     * @arg number key - The key to test, see Enums/IN
     * @return {boolean} Is the key down or not
     */
    public KeyDown(key: number): boolean;

    /**
     * Returns whether the key was pressed. If you want to check if the key is held down, try CMoveData:KeyDown
     *
     * @arg number key - The key to test, see Enums/IN
     * @return {boolean} Was the key pressed or not.
     */
    public KeyPressed(key: number): boolean;

    /**
     * Returns whether the key was released
     *
     * @arg number key - A key to test, see Enums/IN
     * @return {boolean} Was the key released or not.
     */
    public KeyReleased(key: number): boolean;

    /**
     * Returns whether the key was down or not.
     *
     * @arg number key - The key to test, see Enums/IN
     * @return {boolean} Was the key down or not
     */
    public KeyWasDown(key: number): boolean;

    /**
     * Sets absolute move angles.( ? ) Doesn't seem to do anything.
     *
     * @arg Angle ang - New absolute move angles
     */
    public SetAbsMoveAngles(ang: Angle): void;

    /**
     * Sets angles.
     *
     * @arg Angle ang - The angles.
     */
    public SetAngles(ang: Angle): void;

    /**
     * Sets the pressed buttons on the move data
     *
     * @arg number buttons - A number representing which buttons are down, see Enums/IN
     */
    public SetButtons(buttons: number): void;

    /**
     * Sets the center of the player movement constraint. See CMoveData:SetConstraintRadius.
     *
     * @arg Vector pos - The constraint origin.
     */
    public SetConstraintCenter(pos: Vector): void;

    /**
     * Sets the radius that constrains the players movement.
     *
     * @arg number radius - The new constraint radius
     */
    public SetConstraintRadius(radius: number): void;

    /**
     * Sets the player movement constraint speed scale. This will be applied to the player within the constraint radius when approaching its edge.
     *
     * @arg number arg1 - The constraint speed scale
     */
    public SetConstraintSpeedScale(arg1: number): void;

    /**
     * Sets  the width (distance from the edge of the radius, inward) where the actual movement constraint functions.
     *
     * @arg number arg1 - The constraint width
     */
    public SetConstraintWidth(arg1: number): void;

    /**
     * Sets an internal player movement variable m_outWishVel.
     *
     * @arg Vector idealVel
     */
    public SetFinalIdealVelocity(idealVel: Vector): void;

    /**
     * Sets an internal player movement variable m_outJumpVel.
     *
     * @arg Vector jumpVel
     */
    public SetFinalJumpVelocity(jumpVel: Vector): void;

    /**
     * Sets an internal player movement variable m_outStepHeight.
     *
     * @arg number stepHeight
     */
    public SetFinalStepHeight(stepHeight: number): void;

    /**
     * Sets players forward speed.
     *
     * @arg number speed - New forward speed
     */
    public SetForwardSpeed(speed: number): void;

    /**
     * Sets the impulse command. This isn't actually utilised in the engine anywhere.
     *
     * @arg number impulse - The impulse to set
     */
    public SetImpulseCommand(impulse: number): void;

    /**
     * Sets the maximum player speed. Player won't be able to run or sprint faster then this value.
     *
     * @arg number maxSpeed - The new maximum speed
     */
    public SetMaxClientSpeed(maxSpeed: number): void;

    /**
     * Sets the maximum speed of the player. This must match with CMoveData:SetMaxClientSpeed both, on server and client.
     *
     * @arg number maxSpeed - The new maximum speed
     */
    public SetMaxSpeed(maxSpeed: number): void;

    /**
     * Sets the serverside move angles, making the movement keys act as if player was facing that direction.
     *
     * @arg Angle dir - The aim direction.
     */
    public SetMoveAngles(dir: Angle): void;

    /**
     * Sets old aim angles. ( ? ) Doesn't seem to be doing anything.
     *
     * @arg Angle aimAng - The old angles
     */
    public SetOldAngles(aimAng: Angle): void;

    /**
     * Sets the 'old' pressed buttons on the move data. These buttons are used to work out which buttons have been released, which have just been pressed and which are being held down.
     *
     * @arg number buttons - A number representing which buttons were down, see Enums/IN
     */
    public SetOldButtons(buttons: number): void;

    /**
     * Sets the players position.
     *
     * @arg Vector pos - The position
     */
    public SetOrigin(pos: Vector): void;

    /**
     * Sets players strafe speed.
     *
     * @arg number speed - Strafe speed
     */
    public SetSideSpeed(speed: number): void;

    /**
     * Sets vertical speed of the player. ( Z axis of CMoveData:SetVelocity )
     *
     * @arg number speed - Vertical speed to set
     */
    public SetUpSpeed(speed: number): void;

    /**
     * Sets the player's velocity
     *
     * @arg Vector velocity - The velocity to set
     */
    public SetVelocity(velocity: Vector): void;
}

declare type CMoveData = __CMoveDataClass;
