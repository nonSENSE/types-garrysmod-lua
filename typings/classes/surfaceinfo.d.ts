declare class __SurfaceInfoClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the brush surface's material.
     *
     * @return {IMaterial} Material of one portion of a brush model.
     */
    public GetMaterial(): IMaterial;

    /**
     * Returns a list of vertices the brush surface is built from.
     *
     * @return {Vector} A list of Vector points. This will usually be 4 corners of a quadrilateral in counter-clockwise order.
     */
    public GetVertices(): Vector;

    /**
     * Checks if the brush surface is a nodraw surface, meaning it will not be drawn by the engine.
     *
     * @return {boolean} Returns true if this surface won't be drawn.
     */
    public IsNoDraw(): boolean;

    /**
     * Checks if the brush surface is displaying the skybox.
     *
     * @return {boolean} Returns true if the surface is the sky.
     */
    public IsSky(): boolean;

    /**
     * Checks if the brush surface is water.
     *
     * @return {boolean} Returns true if the surface is water.
     */
    public IsWater(): boolean;
}

declare type SurfaceInfo = __SurfaceInfoClass;
