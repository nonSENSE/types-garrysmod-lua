declare class __VectorClass {
    [1]: number;
    [2]: number;
    [3]: number;
    ["x"]: number;
    ["y"]: number;
    ["z"]: number;

    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds the values of the argument vector to the orignal vector. This functions the same as vector1 + vector2 without creating a new vector object, skipping object construction and garbage collection.
     *
     * @arg Vector vector - The vector to add.
     */
    public Add(vector: Vector): void;

    /**
     * Returns an angle representing the normal of the vector.
     *
     * @return {Angle} The angle/direction of the vector.
     */
    public Angle(): Angle;

    /**
     * Returns the angle of the vector, but instead of assuming that up is Vector( 0, 0, 1 ) (Like Vector:Angle does) you can specify which direction is 'up' for the angle.
     *
     * @arg Vector up - The up direction vector
     * @return {Angle} The angle
     */
    public AngleEx(up: Vector): Angle;

    /**
     * Calculates the cross product of this vector and the passed one.
     *
     * @arg Vector otherVector - Vector to calculate the cross product with.
     * @return {Vector} The cross product of the two vectors.
     */
    public Cross(otherVector: Vector): Vector;

    /**
     * Returns the euclidean distance between the vector and the other vector.
     *
     * @arg Vector otherVector - The vector to get the distance to.
     * @return {number} Distance between the vectors.
     */
    public Distance(otherVector: Vector): number;

    /**
     * Returns the squared distance of 2 vectors, this is faster than Vector:Distance as calculating the square root is an expensive process.
     *
     * @arg Vector otherVec - The vector to calculate the distance to.
     * @return {number} Squared distance to the vector.
     */
    public DistToSqr(otherVec: Vector): number;

    /**
     * Divide the vector by the given number, that means x, y and z are divided by that value. This will change the value of the original vector, see example 2 for division without changing the value.
     *
     * @arg number divisor - The value to divide the vector with.
     */
    public Div(divisor: number): void;

    /**
     * Returns the dot product  of this vector and the passed one.
     *
     * @arg Vector otherVector - The vector to calculate the dot product with
     * @return {number} The dot product between the two vectors
     */
    public Dot(otherVector: Vector): number;

    /**
     * Returns the dot product of the two vectors.
     *
     * @arg Vector Vector - The other vector.
     * @return {number} Dot Product
     */
    public DotProduct(Vector: Vector): number;

    /**
     * Returns the negative version of this vector, i.e. a vector with every component to the negative value of itself.
     *
     * @return {Vector} The negative of this vector.
     */
    public GetNegated(): Vector;

    /**
     * Returns a normalized version of the vector. This is a alias of Vector:GetNormalized.
     *
     * @return {Vector} Normalized version of the vector.
     */
    public GetNormal(): Vector;

    /**
     * Returns a normalized version of the vector. Normalized means vector with same direction but with length of 1.
     *
     * @return {Vector} Normalized version of the vector.
     */
    public GetNormalized(): Vector;

    /**
     * Returns if the vector is equal to another vector with the given tolerance.
     *
     * @arg Vector compare - The vector to compare to.
     * @arg number tolerance - The tolerance range.
     * @return {boolean} Are the vectors equal or not.
     */
    public IsEqualTol(compare: Vector, tolerance: number): boolean;

    /**
     * Checks whenever all fields of the vector are 0.
     *
     * @return {boolean} Do all fields of the vector equal 0 or not
     */
    public IsZero(): boolean;

    /**
     * Returns the Euclidean length of the vector: √(x² + y² + z²).
     *
     * @return {number} Length of the vector.
     */
    public Length(): number;

    /**
     * Returns the length of the vector in two dimensions, without the Z axis.
     *
     * @return {number} Length of the vector in two dimensions, √(x² + y²)
     */
    public Length2D(): number;

    /**
     * Returns the squared length of the vectors x and y value, x² + y².
     *
     * @return {number} Squared length of the vector in two dimensions
     */
    public Length2DSqr(): number;

    /**
     * Returns the squared length of the vector, x² + y² + z².
     *
     * @return {number} Squared length of the vector
     */
    public LengthSqr(): number;

    /**
     * Scales the vector by the given number (that means x, y and z are multiplied by that value) or Vector.
     *
     * @arg number multiplier - The value to scale the vector with.
     */
    public Mul(multiplier: number): void;

    /**
     * Negates this vector, i.e. sets every component to the negative value of itself. Same as Vector( -vec.x, -vec.y, -vec.z )
     */
    public Negate(): void;

    /**
     * Normalizes the given vector. This changes the vector you call it on, if you want to return a normalized copy without affecting the original, use Vector:GetNormalized.
     */
    public Normalize(): void;

    /**
     * Randomizes each element of this Vector object.
     *
     * @arg number [min=-1] - The minimum value for each component.
     * @arg number [max=1] - The maximum value for each component.
     */
    public Random(min?: number, max?: number): void;

    /**
     * Rotates a vector by the given angle.
     * Doesn't return anything, but rather changes the original vector.
     *
     * @arg Angle rotation - The angle to rotate the vector by.
     */
    public Rotate(rotation: Angle): void;

    /**
     * Copies the values from the second vector to the first vector.
     *
     * @arg Vector vector - The vector to copy from.
     */
    public Set(vector: Vector): void;

    /**
     * Sets the x, y, and z of the vector.
     *
     * @arg number x - The x component
     * @arg number y - The y component
     * @arg number z - The z component
     */
    public SetUnpacked(x: number, y: number, z: number): void;

    /**
     * Substracts the values of the second vector from the orignal vector, this function can be used to avoid garbage collection.
     *
     * @arg Vector vector - The other vector.
     */
    public Sub(vector: Vector): void;

    /**
     * Translates the Vector (values ranging from 0 to 1) into a Color. This will also range the values from 0 - 1 to 0 - 255.
     *
     * @return {Color} The created Color.
     */
    public ToColor(): Color;

    /**
     * Returns where on the screen the specified position vector would appear. A related function is gui.ScreenToVector, which converts a 2D coordinate to a 3D direction.
     *
     * @return {ToScreenDataStruct} The created Structures/ToScreenData.
     */
    public ToScreen(): ToScreenDataStruct;

    /**
     * Returns the vector as a table with three elements.
     *
     * @return {object} The table with elements 1 = x, 2 = y, 3 = z.
     */
    public ToTable(): object;

    /**
     * Returns the x, y, and z of the vector.
     *
     * @return {number} x or Vector[1].
     * @return {number} y or Vector[2].
     * @return {number} z or Vector[3].
     */
    public Unpack(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns whenever the given vector is in a box created by the 2 other vectors.
     *
     * @arg Vector boxStart - The first vector.
     * @arg Vector boxEnd - The second vector.
     * @return {boolean} Is the vector in the box or not
     */
    public WithinAABox(boxStart: Vector, boxEnd: Vector): boolean;

    /**
     * Sets x, y and z to 0.
     */
    public Zero(): void;
}

// Allow the class to be instantiated with a function call:

/**
 * Creates a Vector object.
* 
* @arg number [x=0] - The x component of the vector.
If this is a Vector, this function will return a copy of the given vector.
If this is a string, this function will try to parse the string as a vector. If it fails, it returns a 0 vector.
(See examples)
* @arg number [y=0] - The y component of the vector.
* @arg number [z=0] - The z component of the vector.
* @return {Vector} The created vector object.
 */
declare function Vector(x?: number, y?: number, z?: number): Vector;

declare type Vector = __VectorClass & number;
