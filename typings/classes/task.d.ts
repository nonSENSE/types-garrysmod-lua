declare class __TaskClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Initialises the AI task. Called by ai_task.New.
     */
    public Init(): void;

    /**
     * Initialises the AI task as an engine task.
     *
     * @arg string taskname - The name of the task.
     * @arg number taskdata
     */
    public InitEngine(taskname: string, taskdata: number): void;

    /**
     * Initialises the AI task as NPC method-based.
     *
     * @arg string startname - The name of the NPC method to call on task start.
     * @arg string runname - The name of the NPC method to call on task run.
     * @arg number taskdata
     */
    public InitFunctionName(startname: string, runname: string, taskdata: number): void;

    /**
     * Determines if the task is an engine task (TYPE_ENGINE, 1).
     */
    public IsEngineType(): void;

    /**
     * Determines if the task is an NPC method-based task (TYPE_FNAME, 2).
     */
    public IsFNameType(): void;

    /**
     * Runs the AI task.
     *
     * @arg NPC target - The NPC to run the task on.
     */
    public Run(target: NPC): void;

    /**
     * Runs the AI task as an NPC method. This requires the task to be of type TYPE_FNAME.
     *
     * @arg NPC target - The NPC to run the task on.
     */
    public Run_FName(target: NPC): void;

    /**
     * Starts the AI task.
     *
     * @arg NPC target - The NPC to start the task on.
     */
    public Start(target: NPC): void;

    /**
     * Starts the AI task as an NPC method.
     *
     * @arg NPC target - The NPC to start the task on.
     */
    public Start_FName(target: NPC): void;
}

declare type Task = __TaskClass;
