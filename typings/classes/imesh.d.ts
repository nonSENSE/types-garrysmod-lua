declare class __IMeshClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Builds the mesh from a table mesh vertexes.
     *
     * @arg MeshVertexStruct vertexes - A table consisting of Structures/MeshVertexs.
     */
    public BuildFromTriangles(vertexes: MeshVertexStruct): void;

    /**
     * Deletes the mesh and frees the memory used by it.
     */
    public Destroy(): void;

    /**
     * Renders the mesh with the active matrix.
     */
    public Draw(): void;

    /**
     * Returns whether this IMesh is valid or not.
     *
     * @return {boolean} Whether this IMesh is valid or not.
     */
    public IsValid(): boolean;
}

declare type IMesh = __IMeshClass;
