declare class __AngleClass {
    [1]: number;
    [2]: number;
    [3]: number;
    ["p"]: number;
    ["pitch"]: number;
    ["x"]: number;
    ["y"]: number;
    ["yaw"]: number;
    ["r"]: number;
    ["roll"]: number;
    ["z"]: number;

    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds the values of the argument angle to the orignal angle.
     *
     * @arg Angle angle - The angle to add.
     */
    public Add(angle: Angle): void;

    /**
     * Divides all values of the original angle by a scalar. This functions the same as angle1 / num without creating a new angle object, skipping object construction and garbage collection.
     *
     * @arg number scalar - The number to divide by.
     */
    public Div(scalar: number): void;

    /**
     * Returns a normal vector facing in the direction that the angle points.
     *
     * @return {Vector} The forward direction of the angle
     */
    public Forward(): Vector;

    /**
     * Returns whether the pitch, yaw and roll are 0 or not.
     *
     * @return {boolean} Whether the pitch, yaw and roll are 0 or not.
     */
    public IsZero(): boolean;

    /**
     * Multiplies a scalar to all the values of the orignal angle. This functions the same as num * angle without creating a new angle object, skipping object construction and garbage collection.
     *
     * @arg number scalar - The number to multiply.
     */
    public Mul(scalar: number): void;

    /**
     * Normalizes the angles by applying a module with 360 to pitch, yaw and roll.
     */
    public Normalize(): void;

    /**
     * Randomizes each element of this Angle object.
     *
     * @arg number [min=-360] - The minimum value for each component.
     * @arg number [max=360] - The maximum value for each component.
     */
    public Random(min?: number, max?: number): void;

    /**
     * Returns a normal vector facing in the direction that points right relative to the angle's direction.
     *
     * @return {Vector} The right direction of the angle
     */
    public Right(): Vector;

    /**
     * Rotates the angle around the specified axis by the specified degrees.
     *
     * @arg Vector axis - The axis to rotate around as a normalized unit vector. When argument is not a unit vector, you will experience numerical offset errors in the rotated angle.
     * @arg number rotation - The degrees to rotate around the specified axis.
     */
    public RotateAroundAxis(axis: Vector, rotation: number): void;

    /**
     * Copies pitch, yaw and roll from the second angle to the first.
     *
     * @arg Angle originalAngle - The angle to copy the values from.
     */
    public Set(originalAngle: Angle): void;

    /**
     * Sets the p, y, and r of the angle.
     *
     * @arg number p - The pitch component of the Angle
     * @arg number y - The yaw component of the Angle
     * @arg number r - The roll component of the Angle
     */
    public SetUnpacked(p: number, y: number, r: number): void;

    /**
     * Snaps the angle to nearest interval of degrees.
     *
     * @arg string axis - The component/axis to snap. Can be either "p"/"pitch", "y"/"yaw" or "r"/"roll".
     * @arg number target - The target angle snap interval
     * @return {Angle} The snapped angle.
     */
    public SnapTo(axis: string, target: number): Angle;

    /**
     * Subtracts the values of the argument angle to the orignal angle. This functions the same as angle1 - angle2 without creating a new angle object, skipping object construction and garbage collection.
     *
     * @arg Angle angle - The angle to subtract.
     */
    public Sub(angle: Angle): void;

    /**
     * Returns the angle as a table with three elements.
     *
     * @return {object} The table with elements 1 = p, 2 = y, 3 = r.
     */
    public ToTable(): object;

    /**
     * Returns the pitch, yaw, and roll components of the angle.
     *
     * @return {number} p, pitch, x, or Angle[1].
     * @return {number} y, yaw, or Angle[2].
     * @return {number} r, roll, r, or Angle[3].
     */
    public Unpack(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns a normal vector facing in the direction that points up relative to the angle's direction.
     *
     * @return {Vector} The up direction of the angle.
     */
    public Up(): Vector;

    /**
     * Sets pitch, yaw and roll to 0.
     * This function is faster than doing it manually.
     */
    public Zero(): void;
}

// Allow the class to be instantiated with a function call:

/**
 * Creates an Angle object.
* 
* @arg number [pitch=0] - The pitch value of the angle.
If this is an Angle, this function will return a copy of the given angle.
If this is a string, this function will try to parse the string as a angle. If it fails, it returns a 0 angle.
(See examples)
* @arg number [yaw=0] - The yaw value of the angle.
* @arg number [roll=0] - The roll value of the angle.
* @return {Angle} Created angle
 */
declare function Angle(pitch?: number, yaw?: number, roll?: number): Angle;

declare type Angle = __AngleClass & number;
