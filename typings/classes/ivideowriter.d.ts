declare class __IVideoWriterClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds the current framebuffer to the video stream.
     *
     * @arg number frameTime - Usually set to what FrameTime is, or simply 1/fps.
     * @arg boolean downsample - If true it will downsample the whole screenspace to the videos width and height, otherwise it will just record from the top left corner to the given width and height and therefor not the whole screen.
     */
    public AddFrame(frameTime: number, downsample: boolean): void;

    /**
     * Ends the video recording and dumps it to disk.
     */
    public Finish(): void;

    /**
     * Returns the height of the video stream.
     *
     * @return {number} height
     */
    public Height(): number;

    /**
     * Sets whether to record sound or not.
     *
     * @arg boolean record - Record.
     */
    public SetRecordSound(record: boolean): void;

    /**
     * Returns the width of the video stream.
     *
     * @return {number} width
     */
    public Width(): number;
}

declare type IVideoWriter = __IVideoWriterClass;
