declare class __NPCClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Makes the NPC like, hate, feel neutral towards, or fear the entity in question. If you want to setup relationship towards a certain entity class, use NPC:AddRelationship.
     *
     * @arg Entity target - The entity for the relationship to be applied to.
     * @arg number disposition - A Enums/D representing the relationship type.
     * @arg number priority - How strong the relationship is.
     */
    public AddEntityRelationship(target: Entity, disposition: number, priority: number): void;

    /**
 * Changes how an NPC feels towards another NPC.  If you want to setup relationship towards a certain entity, use NPC:AddEntityRelationship.
* 
* @arg string relationstring - A string representing how the relationship should be set up.
Should be formatted as "npc_class Enums/D numberPriority".
 */
    public AddRelationship(relationstring: string): void;

    /**
     * Force an NPC to play his Alert sound.
     */
    public AlertSound(): void;

    /**
     * Executes any movement the current sequence may have.
     *
     * @arg number interval - This is a good place to use Entity:GetAnimTimeInterval.
     * @arg Entity [target=NULL]
     * @return {boolean} true if any movement was performed.
     */
    public AutoMovement(interval: number, target?: Entity): boolean;

    /**
     * Adds a capability to the NPC.
     *
     * @arg number capabilities - Capabilities to add, see Enums/CAP.
     */
    public CapabilitiesAdd(capabilities: number): void;

    /**
     * Removes all of Capabilities the NPC has.
     */
    public CapabilitiesClear(): void;

    /**
 * Returns the NPC's capabilities along the ones defined on its weapon.
* 
* @return {number} The capabilities as a bitflag.
See Enums/CAP
 */
    public CapabilitiesGet(): number;

    /**
     * Remove a certain capability.
     *
     * @arg number capabilities - Capabilities to remove, see Enums/CAP
     */
    public CapabilitiesRemove(capabilities: number): void;

    /**
     * Returns the NPC class. Do not confuse with Entity:GetClass!
     *
     * @return {number} See Enums/CLASS
     */
    public Classify(): number;

    /**
     * Resets the NPC:GetBlockingEntity.
     */
    public ClearBlockingEntity(): void;

    /**
     * Clears out the specified Enums/COND on this NPC.
     *
     * @arg number condition - The Enums/COND to clear out.
     */
    public ClearCondition(condition: number): void;

    /**
     * Clears the Enemy from the NPC's memory, effectively forgetting it until met again with either the NPC vision or with NPC:UpdateEnemyMemory.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to mark
     */
    public ClearEnemyMemory(enemy?: Entity): void;

    /**
     * Clears the NPC's current expression which can be set with NPC:SetExpression.
     */
    public ClearExpression(): void;

    /**
     * Clears the current NPC goal or target.
     */
    public ClearGoal(): void;

    /**
     * Stops the current schedule that the NPC is doing.
     */
    public ClearSchedule(): void;

    /**
     * Translates condition ID to a string.
     *
     * @arg number cond - The NPCs condition ID, see Enums/COND
     * @return {string} A human understandable string equivalent of that condition.
     */
    public ConditionName(cond: number): string;

    /**
     * Returns the way the NPC "feels" about the entity.
     *
     * @arg Entity ent - The entity to get the disposition from.
     * @return {number} The NPCs disposition, see Enums/D.
     */
    public Disposition(ent: Entity): number;

    /**
     * Forces the NPC to drop the specified weapon.
     *
     * @arg Weapon [weapon=nil] - Weapon to be dropped. If unset, will default to the currently equipped weapon.
     * @arg Vector [target=nil] - If set, launches the weapon at given position. There is a limit to how far it is willing to throw the weapon. Overrides velocity argument.
     * @arg Vector [velocity=nil] - If set and previous argument is unset, launches the weapon with given velocity. If the velocity is higher than 400, it will be clamped to 400.
     */
    public DropWeapon(weapon?: Weapon, target?: Vector, velocity?: Vector): void;

    /**
     * Makes an NPC exit a scripted sequence, if one is playing.
     */
    public ExitScriptedSequence(): void;

    /**
     * Force an NPC to play its Fear sound.
     */
    public FearSound(): void;

    /**
     * Force an NPC to play its FoundEnemy sound.
     */
    public FoundEnemySound(): void;

    /**
     * Returns the weapon the NPC is currently carrying, or NULL.
     *
     * @return {Entity} The NPCs current weapon
     */
    public GetActiveWeapon(): Entity;

    /**
     * Returns the NPC's current activity.
     *
     * @return {number} Current activity, see Enums/ACT.
     */
    public GetActivity(): number;

    /**
     * Returns the aim vector of the NPC. NPC alternative of Player:GetAimVector.
     *
     * @return {Vector} The aim direction of the NPC.
     */
    public GetAimVector(): Vector;

    /**
     * Returns the activity to be played when the NPC arrives at its goal
     *
     * @return {number}
     */
    public GetArrivalActivity(): number;

    /**
     * Returns the sequence to be played when the NPC arrives at its goal.
     *
     * @return {number} Sequence ID to be played, or -1 if there's no sequence.
     */
    public GetArrivalSequence(): number;

    /**
     * Returns the most dangerous/closest sound hint based on the NPCs location and the types of sounds it can sense.
     *
     * @arg number types - The types of sounds to choose from. See SOUND_ enums
     * @return {SoundHintDataStruct} A table with SoundHintData structure or nil if no sound hints are nearby.
     */
    public GetBestSoundHint(types: number): SoundHintDataStruct;

    /**
     * Returns the entity blocking the NPC along its path.
     *
     * @return {Entity} Blocking entity
     */
    public GetBlockingEntity(): Entity;

    /**
     * Returns the NPC's current schedule.
     *
     * @return {number} The NPCs schedule, see Enums/SCHED or -1 if we failed for some reason
     */
    public GetCurrentSchedule(): number;

    /**
     * Returns how proficient (skilled) an NPC is with its current weapon.
     *
     * @return {number} NPC's proficiency for current weapon. See Enums/WEAPON_PROFICIENCY.
     */
    public GetCurrentWeaponProficiency(): number;

    /**
     * Gets the NPC's current waypoint position (where NPC is currently moving towards), if any is available.
     *
     * @return {Vector} The position of the current NPC waypoint.
     */
    public GetCurWaypointPos(): Vector;

    /**
     * Returns the entity that this NPC is trying to fight.
     *
     * @return {NPC} Enemy NPC.
     */
    public GetEnemy(): NPC;

    /**
     * Returns the first time an NPC's enemy was seen by the NPC.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to check.
     * @return {number} First time the given enemy was seen.
     */
    public GetEnemyFirstTimeSeen(enemy?: Entity): number;

    /**
     * Returns the last known position of an NPC's enemy.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to check.
     * @return {Vector} The last known position.
     */
    public GetEnemyLastKnownPos(enemy?: Entity): Vector;

    /**
     * Returns the last seen position of an NPC's enemy.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to check.
     * @return {Vector} The last seen position.
     */
    public GetEnemyLastSeenPos(enemy?: Entity): Vector;

    /**
     * Returns the last time an NPC's enemy was seen by the NPC.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to check.
     * @return {number} Last time the given enemy was seen.
     */
    public GetEnemyLastTimeSeen(enemy?: Entity): number;

    /**
     * Returns the expression file the NPC is currently playing.
     *
     * @return {string} The file path of the expression.
     */
    public GetExpression(): string;

    /**
     * Returns NPCs hull type set by NPC:SetHullType.
     *
     * @return {number} Hull type, see Enums/HULL
     */
    public GetHullType(): number;

    /**
     * Returns the ideal activity the NPC currently wants to achieve.
     *
     * @return {number} The ideal activity. Enums/ACT.
     */
    public GetIdealActivity(): number;

    /**
     * Returns the ideal move acceleration of the NPC.
     *
     * @return {number} The ideal move acceleration.
     */
    public GetIdealMoveAcceleration(): number;

    /**
     * Returns the ideal move speed of the NPC.
     *
     * @return {number} The ideal move speed.
     */
    public GetIdealMoveSpeed(): number;

    /**
     * Returns how far should the NPC look ahead in its route.
     *
     * @return {number} How far the NPC checks ahead of its route.
     */
    public GetMinMoveCheckDist(): number;

    /**
     * Returns how far before the NPC can come to a complete stop.
     *
     * @arg number [minResult=10] - The minimum value that will be returned by this function.
     * @return {number} The minimum stop distance.
     */
    public GetMinMoveStopDist(minResult?: number): number;

    /**
     * Returns the current timestep the internal NPC motor is working on.
     *
     * @return {number} The current timestep.
     */
    public GetMoveInterval(): number;

    /**
     * Returns the NPC's current movement activity.
     *
     * @return {number} Current NPC movement activity, see Enums/ACT.
     */
    public GetMovementActivity(): number;

    /**
     * Returns the index of the sequence the NPC uses to move.
     *
     * @return {number} The movement sequence index
     */
    public GetMovementSequence(): number;

    /**
     * Returns the current move velocity of the NPC.
     *
     * @return {Vector} The current move velocity of the NPC.
     */
    public GetMoveVelocity(): Vector;

    /**
     * Returns the NPC's navigation type.
     *
     * @return {number} The nav type. See Enums/NAV.
     */
    public GetNavType(): number;

    /**
     * Returns the nearest member of the squad the NPC is in.
     *
     * @return {NPC} The nearest member of the squad the NPC is in.
     */
    public GetNearestSquadMember(): NPC;

    /**
     * Gets the NPC's next waypoint position, where NPC will be moving after reaching current waypoint, if any is available.
     *
     * @return {Vector} The position of the next NPC waypoint.
     */
    public GetNextWaypointPos(): Vector;

    /**
     * Returns the NPC's state.
     *
     * @return {number} The NPC's current state, see Enums/NPC_STATE.
     */
    public GetNPCState(): number;

    /**
     * Returns the distance the NPC is from Target Goal.
     *
     * @return {number} The number of hammer units the NPC is away from the Goal.
     */
    public GetPathDistanceToGoal(): number;

    /**
     * Returns the amount of time it will take for the NPC to get to its Target Goal.
     *
     * @return {number} The amount of time to get to the target goal.
     */
    public GetPathTimeToGoal(): number;

    /**
     * Returns the shooting position of the NPC.
     *
     * @return {Vector} The NPC's shooting position.
     */
    public GetShootPos(): Vector;

    /**
     * Returns the current squad name of the NPC.
     *
     * @return {string} The new squad name to set.
     */
    public GetSquad(): string;

    /**
     * Returns the NPC's current target set by NPC:SetTarget.
     *
     * @return {Entity} Target entity
     */
    public GetTarget(): Entity;

    /**
     * Returns the status of the current task.
     *
     * @return {number} The status. See Enums/TASKSTATUS.
     */
    public GetTaskStatus(): number;

    /**
     * Returns a specific weapon the NPC owns.
     *
     * @arg string _class - A classname of the weapon to try to get.
     * @return {Weapon} The weapon for the specified class, or NULL of the NPC doesn't have given weapon.
     */
    public GetWeapon(_class: string): Weapon;

    /**
     * Returns a table of the NPC's weapons.
     *
     * @return {object} A list of the weapons the NPC currently has.
     */
    public GetWeapons(): object;

    /**
     * Used to give a weapon to an already spawned NPC.
     *
     * @arg string weapon - Class name of the weapon to equip to the NPC.
     * @return {Weapon} The weapon entity given to the NPC.
     */
    public Give(weapon: string): Weapon;

    /**
     * Returns whether or not the NPC has the given condition.
     *
     * @arg number condition - The condition index, see Enums/COND.
     * @return {boolean} True if the NPC has the given condition, false otherwise.
     */
    public HasCondition(condition: number): boolean;

    /**
     * Polls the enemy memory to check if the given entity has eluded us or not.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to test.
     * @return {boolean} If the enemy has eluded us.
     */
    public HasEnemyEluded(enemy?: Entity): boolean;

    /**
     * Polls the enemy memory to check if the NPC has any memory of given enemy.
     *
     * @arg Entity [enemy=GetEnemy()] - The entity to test.
     * @return {boolean} If we have any memory on given enemy.
     */
    public HasEnemyMemory(enemy?: Entity): boolean;

    /**
     * Returns true if the current navigation has a obstacle, this is different from NPC:GetBlockingEntity, this includes obstacles that it can steer around.
     *
     * @return {boolean} true if the current navigation has a obstacle.
     */
    public HasObstacles(): boolean;

    /**
     * Force an NPC to play his Idle sound.
     */
    public IdleSound(): void;

    /**
     * Returns whether or not the NPC is performing the given schedule.
     *
     * @arg number schedule - The schedule number, see Enums/SCHED.
     * @return {boolean} True if the NPC is performing the given schedule, false otherwise.
     */
    public IsCurrentSchedule(schedule: number): boolean;

    /**
     * Returns whether the NPC has an active goal.
     *
     * @return {boolean} Whether the NPC has an active goal or not.
     */
    public IsGoalActive(): boolean;

    /**
     * Returns if the current movement is locked on the Yaw axis.
     *
     * @return {boolean} Whether the movement is yaw locked or not.
     */
    public IsMoveYawLocked(): boolean;

    /**
     * Returns whether the NPC is moving or not.
     *
     * @return {boolean} Whether the NPC is moving or not.
     */
    public IsMoving(): boolean;

    /**
     * Checks if the NPC is running an ai_goal. ( e.g. An npc_citizen NPC following the Player. )
     *
     * @return {boolean} Returns true if running an ai_goal, otherwise returns false.
     */
    public IsRunningBehavior(): boolean;

    /**
     * Returns whether the current NPC is the leader of the squad it is in.
     *
     * @return {boolean} Whether the NPC is the leader of the squad or not.
     */
    public IsSquadLeader(): boolean;

    /**
     * Returns true if the entity was remembered as unreachable. The memory is updated automatically from following engine tasks if they failed:
     *
     * @arg Entity testEntity - The entity to test.
     * @return {boolean} If the entity is reachable or not.
     */
    public IsUnreachable(testEntity: Entity): boolean;

    /**
     * Force an NPC to play his LostEnemy sound.
     */
    public LostEnemySound(): void;

    /**
     * Tries to achieve our ideal animation state, playing any transition sequences that we need to play to get there.
     */
    public MaintainActivity(): void;

    /**
     * Causes the NPC to temporarily forget the current enemy and switch on to a better one.
     *
     * @arg Entity [enemy=GetEnemy()] - The enemy to mark
     */
    public MarkEnemyAsEluded(enemy?: Entity): void;

    /**
     * Executes a climb move.
     *
     * @arg Vector destination - The destination of the climb.
     * @arg Vector dir - The direction of the climb.
     * @arg number distance - The distance.
     * @arg number yaw - The yaw angle.
     * @arg number left - Amount of climb nodes left?
     * @return {number} The result. See Enums/AIMR.
     */
    public MoveClimbExec(destination: Vector, dir: Vector, distance: number, yaw: number, left: number): number;

    /**
     * Starts a climb move.
     *
     * @arg Vector destination - The destination of the climb.
     * @arg Vector dir - The direction of the climb.
     * @arg number distance - The distance.
     * @arg number yaw - The yaw angle.
     */
    public MoveClimbStart(destination: Vector, dir: Vector, distance: number, yaw: number): void;

    /**
     * Stops a climb move.
     */
    public MoveClimbStop(): void;

    /**
     * Executes a jump move.
     *
     * @return {number} The result. See Enums/AIMR.
     */
    public MoveJumpExec(): number;

    /**
     * Starts a jump move.
     *
     * @arg Vector vel - The jump velocity.
     */
    public MoveJumpStart(vel: Vector): void;

    /**
     * Stops a jump move.
     *
     * @return {number} The result. See Enums/AIMR.
     */
    public MoveJumpStop(): number;

    /**
     * Makes the NPC walk toward the given position. The NPC will return to the player after amount of time set by player_squad_autosummon_time ConVar.
     *
     * @arg Vector position - The target position for the NPC to walk to.
     */
    public MoveOrder(position: Vector): void;

    /**
     * Pauses the NPC movement?
     */
    public MovePause(): void;

    /**
     * Starts NPC movement?
     */
    public MoveStart(): void;

    /**
     * Stops the NPC movement?
     */
    public MoveStop(): void;

    /**
     * Works similarly to NPC:NavSetRandomGoal.
     *
     * @arg Vector pos - The origin to calculate a path from.
     * @arg number length - The target length of the path to calculate.
     * @arg Vector dir - The direction in which to look for a new path end goal.
     * @return {boolean} Whether path generation was successful or not.
     */
    public NavSetGoal(pos: Vector, length: number, dir: Vector): boolean;

    /**
     * Set the goal target for an NPC.
     *
     * @arg Entity target - The targeted entity to set the goal to.
     * @arg Vector offset - The offset to apply to the targeted entity's position.
     * @return {boolean} Whether path generation was successful or not
     */
    public NavSetGoalTarget(target: Entity, offset: Vector): boolean;

    /**
     * Creates a random path of specified minimum length between a closest start node and random node in the specified direction. This won't actually force the NPC to move.
     *
     * @arg number minPathLength - Minimum length of path in units
     * @arg Vector dir - Unit vector pointing in the direction of the target random node
     * @return {boolean} Whether path generation was successful or not
     */
    public NavSetRandomGoal(minPathLength: number, dir: Vector): boolean;

    /**
     * Sets a goal in x, y offsets for the NPC to wander to
     *
     * @arg number xOffset - X offset
     * @arg number yOffset - Y offset
     * @return {boolean} Whether path generation was successful or not
     */
    public NavSetWanderGoal(xOffset: number, yOffset: number): boolean;

    /**
     * Forces the NPC to pickup an existing weapon entity. The NPC will not pick up the weapon if they already own a weapon of given type, or if the NPC could not normally have this weapon in their inventory.
     *
     * @arg Weapon wep - The weapon to try to pick up.
     * @return {boolean} Whether the NPC succeeded in picking up the weapon or not.
     */
    public PickupWeapon(wep: Weapon): boolean;

    /**
     * Forces the NPC to play a sentence from scripts/sentences.txt
     *
     * @arg string sentence - The sentence string to speak.
     * @arg number delay - Delay in seconds until the sentence starts playing.
     * @arg number volume - The volume of the sentence, from 0 to 1.
     * @return {number} Returns the sentence index, -1 if the sentence couldn't be played.
     */
    public PlaySentence(sentence: string, delay: number, volume: number): number;

    /**
     * Makes the NPC remember an entity or an enemy as unreachable, for a specified amount of time. Use NPC:IsUnreachable to check if an entity is still unreachable.
     *
     * @arg Entity ent - The entity to mark as unreachable.
     * @arg number [time=3] - For how long to remember the entity as unreachable. Negative values will act as 3 seconds.
     */
    public RememberUnreachable(ent: Entity, time?: number): void;

    /**
     * This function crashes the game no matter how it is used and will be removed in a future update.
     */
    public RemoveMemory(): void;

    /**
     * Resets the ideal activity of the NPC. See also NPC:SetIdealActivity.
     *
     * @arg number act - The new activity. See Enums/ACT.
     */
    public ResetIdealActivity(act: number): void;

    /**
     * Resets all the movement calculations.
     */
    public ResetMoveCalc(): void;

    /**
     * Starts an engine task.
     *
     * @arg number taskID - The task ID, see ai_task.h
     * @arg number taskData - The task data.
     */
    public RunEngineTask(taskID: number, taskData: number): void;

    /**
     * Forces the NPC to switch to a specific weapon the NPC owns. See NPC:GetWeapons.
     *
     * @arg string _class - A classname of the weapon or a Weapon entity to switch to.
     */
    public SelectWeapon(_class: string): void;

    /**
     * Stops any sounds (speech) the NPC is currently palying.
     */
    public SentenceStop(): void;

    /**
     * Sets the NPC's current activity.
     *
     * @arg number act - The new activity to set, see Enums/ACT.
     */
    public SetActivity(act: number): void;

    /**
     *
     *
     * @arg number act
     */
    public SetArrivalActivity(act: number): void;

    /**
     *
     */
    public SetArrivalDirection(): void;

    /**
     * Sets the distance to goal at which the NPC should stop moving and continue to other business such as doing the rest of their tasks in a schedule.
     *
     * @arg number dist - The distance to goal that is close enough for the NPC
     */
    public SetArrivalDistance(dist: number): void;

    /**
     *
     */
    public SetArrivalSequence(): void;

    /**
     * Sets the arrival speed? of the NPC
     *
     * @arg number speed - The new arrival speed
     */
    public SetArrivalSpeed(speed: number): void;

    /**
     * Sets an NPC condition.
     *
     * @arg number condition - The condition index, see Enums/COND.
     */
    public SetCondition(condition: number): void;

    /**
     * Sets the weapon proficiency of an NPC (how skilled an NPC is with its current weapon).
     *
     * @arg number proficiency - The proficiency for the NPC's current weapon. See Enums/WEAPON_PROFICIENCY.
     */
    public SetCurrentWeaponProficiency(proficiency: number): void;

    /**
     * Sets the target for an NPC.
     *
     * @arg Entity enemy - The enemy that the NPC should target
     * @arg boolean [newenemy=true] - Calls NPC:SetCondition(COND_NEW_ENEMY) if the new enemy is valid and not equal to the last enemy.
     */
    public SetEnemy(enemy: Entity, newenemy?: boolean): void;

    /**
     * Sets the NPC's .vcd expression. Similar to Entity:PlayScene except the scene is looped until it's interrupted by default NPC behavior or NPC:ClearExpression.
     *
     * @arg string expression - The expression filepath.
     * @return {number}
     */
    public SetExpression(expression: string): number;

    /**
     * Updates the NPC's hull and physics hull in order to match its model scale. Entity:SetModelScale seems to take care of this regardless.
     */
    public SetHullSizeNormal(): void;

    /**
     * Sets the hull type for the NPC.
     *
     * @arg number hullType - Hull type. See Enums/HULL
     */
    public SetHullType(hullType: number): void;

    /**
     * Sets the ideal activity the NPC currently wants to achieve. This is most useful for custom SNPCs.
     *
     * @arg number arg1 - The ideal activity to set. Enums/ACT.
     */
    public SetIdealActivity(arg1: number): void;

    /**
 * Sets the ideal yaw angle (left-right rotation) for the NPC and forces them to turn to that angle.
* 
* @arg number angle - The aim direction to set, the yaw component.
* @arg number [speed=-1] - The turn speed. Special values are:

-1 - Calculate automatically
-2 - Keep the previous yaw speed
 */
    public SetIdealYawAndUpdate(angle: number, speed?: number): void;

    /**
     * Sets the last registered or memorized position for an npc. When using scheduling, the NPC will focus on navigating to the last position via nodes.
     *
     * @arg Vector Position - Where the NPC's last position will be set.
     */
    public SetLastPosition(Position: Vector): void;

    /**
     * Sets how long to try rebuilding path before failing task.
     *
     * @arg number time - How long to try rebuilding path before failing task
     */
    public SetMaxRouteRebuildTime(time: number): void;

    /**
     * Sets the timestep the internal NPC motor is working on.
     *
     * @arg number time - The new timestep.
     */
    public SetMoveInterval(time: number): void;

    /**
     * Sets the activity the NPC uses when it moves.
     *
     * @arg number activity - The movement activity, see Enums/ACT.
     */
    public SetMovementActivity(activity: number): void;

    /**
     * Sets the sequence the NPC navigation path uses for speed calculation. Doesn't seem to have any visible effect on NPC movement.
     *
     * @arg number sequenceId - The movement sequence index
     */
    public SetMovementSequence(sequenceId: number): void;

    /**
     * Sets the move velocity of the NPC
     *
     * @arg Vector vel - The new movement velocity.
     */
    public SetMoveVelocity(vel: Vector): void;

    /**
     * Sets whether the current movement should locked on the Yaw axis or not.
     *
     * @arg boolean lock - Whether the movement should yaw locked or not.
     */
    public SetMoveYawLocked(lock: boolean): void;

    /**
     * Sets the navigation type of the NPC.
     *
     * @arg number navtype - The new nav type. See Enums/NAV.
     */
    public SetNavType(navtype: number): void;

    /**
     * Sets the state the NPC is in to help it decide on a ideal schedule.
     *
     * @arg number state - New NPC state, see Enums/NPC_STATE
     */
    public SetNPCState(state: number): void;

    /**
     * Sets the NPC's current schedule.
     *
     * @arg number schedule - The NPC schedule, see Enums/SCHED.
     */
    public SetSchedule(schedule: number): void;

    /**
     * Assigns the NPC to a new squad. A squad can have up to 16 NPCs. NPCs in a single squad should be friendly to each other.
     *
     * @arg string name - The new squad name to set.
     */
    public SetSquad(name: string): void;

    /**
     * Sets the NPC's target. This is used in some engine schedules.
     *
     * @arg Entity entity - The target of the NPC.
     */
    public SetTarget(entity: Entity): void;

    /**
     * Sets the status of the current task.
     *
     * @arg number status - The status. See Enums/TASKSTATUS.
     */
    public SetTaskStatus(status: number): void;

    /**
     * Forces the NPC to start an engine task, this has different results for every NPC.
     *
     * @arg number task - The id of the task to start, see ai_task.h
     * @arg number taskData - The task data as a float, not all tasks make use of it.
     */
    public StartEngineTask(task: number, taskData: number): void;

    /**
     * Resets the NPC's movement animation and velocity. Does not actually stop the NPC from moving.
     */
    public StopMoving(): void;

    /**
     * Cancels NPC:MoveOrder basically.
     *
     * @arg Entity target - Must be a player, does nothing otherwise.
     */
    public TargetOrder(target: Entity): void;

    /**
     * Marks the current NPC task as completed.
     */
    public TaskComplete(): void;

    /**
     * Marks the current NPC task as failed.
     *
     * @arg string task - A string most likely defined as a Source Task, for more information on Tasks go to https://developer.valvesoftware.com/wiki/Task
     */
    public TaskFail(task: string): void;

    /**
     * Force the NPC to update information on the supplied enemy, as if it had line of sight to it.
     *
     * @arg Entity enemy - The enemy to update.
     * @arg Vector pos - The last known position of the enemy.
     */
    public UpdateEnemyMemory(enemy: Entity, pos: Vector): void;

    /**
     * Updates the turn activity. Basically applies the turn animations depending on the current turn yaw.
     */
    public UpdateTurnActivity(): void;

    /**
     * Only usable on "ai" base entities.
     *
     * @return {boolean} If we succeeded setting the behavior.
     */
    public UseActBusyBehavior(): boolean;

    /**
     *
     *
     * @return {boolean}
     */
    public UseAssaultBehavior(): boolean;

    /**
     * Only usable on "ai" base entities.
     *
     * @return {boolean} If we succeeded setting the behavior.
     */
    public UseFollowBehavior(): boolean;

    /**
     *
     *
     * @return {boolean}
     */
    public UseFuncTankBehavior(): boolean;

    /**
     *
     *
     * @return {boolean}
     */
    public UseLeadBehavior(): boolean;

    /**
     * Undoes the other Use*Behavior functions.
     */
    public UseNoBehavior(): void;
}

declare type NPC = __NPCClass & Entity;
