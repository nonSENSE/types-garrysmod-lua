declare class __CNewParticleEffectClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds a control point to the particle system.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Entity ent - The entity to attach the control point to.
     * @arg number partAttachment - See Enums/PATTACH.
     * @arg number [entAttachment=0] - The attachment ID on the entity to attach the particle system to
     * @arg Vector [offset=Vector( 0, 0, 0 )] - The offset from the Entity:GetPos of the entity we are attaching this CP to.
     */
    public AddControlPoint(
        cpID: number,
        ent: Entity,
        partAttachment: number,
        entAttachment?: number,
        offset?: Vector
    ): void;

    /**
     *
     *
     * @return {boolean}
     */
    public GetAutoUpdateBBox(): boolean;

    /**
     * Returns the name of the particle effect this system is set to emit.
     *
     * @return {string} The name of the particle effect.
     */
    public GetEffectName(): string;

    /**
     * Returns the highest control point number for given particle system.
     *
     * @return {boolean} The highest control point number for given particle system, 0 to 63.
     */
    public GetHighestControlPoint(): boolean;

    /**
     * Returns the owner of the particle system, the entity the particle system is attached to.
     *
     * @return {Entity} The owner of the particle system.
     */
    public GetOwner(): Entity;

    /**
     * Returns whether the particle system has finished emitting particles or not.
     *
     * @return {boolean} Whether the particle system has finished emitting particles or not.
     */
    public IsFinished(): boolean;

    /**
     * Returns whether the particle system is valid or not.
     *
     * @return {boolean} Whether the particle system is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Returns whether the particle system is intended to be used on a view model?
     *
     * @return {boolean}
     */
    public IsViewModelEffect(): boolean;

    /**
     * Forces the particle system to render using current rendering context.
     */
    public Render(): void;

    /**
     * Forces the particle system to restart emitting particles.
     */
    public Restart(): void;

    /**
     * Sets a value for given control point.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Vector value - The value to set for given control point.
     */
    public SetControlPoint(cpID: number, value: Vector): void;

    /**
     * Essentially makes child control point follow the parent entity.
     *
     * @arg number child - The child control point ID, 0 to 63.
     * @arg Entity parent - The parent entity to follow.
     */
    public SetControlPointEntity(child: number, parent: Entity): void;

    /**
     * Sets the forward direction for given control point.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Vector forward - The forward direction for given control point
     */
    public SetControlPointForwardVector(cpID: number, forward: Vector): void;

    /**
     * Sets the orientation for given control point.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Vector forward - The forward direction for given control point
     * @arg Vector right - The right direction for given control point
     * @arg Vector up - The up direction for given control point
     */
    public SetControlPointOrientation(cpID: number, forward: Vector, right: Vector, up: Vector): void;

    /**
     * Essentially makes child control point follow the parent control point.
     *
     * @arg number child - The child control point ID, 0 to 63.
     * @arg number parent - The parent control point ID, 0 to 63.
     */
    public SetControlPointParent(child: number, parent: number): void;

    /**
     * Sets the right direction for given control point.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Vector right - The right direction for given control point.
     */
    public SetControlPointRightVector(cpID: number, right: Vector): void;

    /**
     * Sets the upward direction for given control point.
     *
     * @arg number cpID - The control point ID, 0 to 63.
     * @arg Vector upward - The upward direction for given control point
     */
    public SetControlPointUpVector(cpID: number, upward: Vector): void;

    /**
     *
     *
     * @arg boolean isViewModel
     */
    public SetIsViewModelEffect(isViewModel: boolean): void;

    /**
     * Forces the particle system to stop automatically rendering.
     *
     * @arg boolean should - Whether to automatically draw the particle effect or not.
     */
    public SetShouldDraw(should: boolean): void;

    /**
     * Sets the sort origin for given particle system. This is used as a helper to determine which particles are in front of which.
     *
     * @arg Vector origin - The new sort origin.
     */
    public SetSortOrigin(origin: Vector): void;

    /**
     * Starts the particle emission.
     *
     * @arg boolean [infiniteOnly=false]
     */
    public StartEmission(infiniteOnly?: boolean): void;

    /**
     * Stops the particle emission.
     *
     * @arg boolean [infiniteOnly=false]
     * @arg boolean [removeAllParticles=false]
     * @arg boolean [wakeOnStop=false]
     */
    public StopEmission(infiniteOnly?: boolean, removeAllParticles?: boolean, wakeOnStop?: boolean): void;

    /**
     * Stops particle emission and destroys all particles instantly. Also detaches the particle effect from the entity it was attached to.
     */
    public StopEmissionAndDestroyImmediately(): void;
}

declare type CNewParticleEffect = __CNewParticleEffectClass;
