declare class __VMatrixClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds given matrix to this matrix.
     *
     * @arg VMatrix input - The input matrix to add.
     */
    public Add(input: VMatrix): void;

    /**
     * Returns the absolute rotation of the matrix.
     *
     * @return {Angle} Absolute rotation of the matrix
     */
    public GetAngles(): Angle;

    /**
     * Returns a specific field in the matrix.
     *
     * @arg number row - Row of the field whose value is to be retrieved, from 1 to 4
     * @arg number column - Column of the field whose value is to be retrieved, from 1 to 4
     * @return {number} The value of the specified field
     */
    public GetField(row: number, column: number): number;

    /**
     * Gets the forward direction of the matrix.
     *
     * @return {Vector} The forward direction of the matrix.
     */
    public GetForward(): Vector;

    /**
     * Returns an inverted matrix without modifying the original matrix.
     *
     * @return {VMatrix} The inverted matrix if possible, nil otherwise
     */
    public GetInverse(): VMatrix;

    /**
     * Returns an inverted matrix without modifying the original matrix. This function will not fail, but only works correctly on matrices that contain only translation and/or rotation.
     *
     * @return {VMatrix} The inverted matrix.
     */
    public GetInverseTR(): VMatrix;

    /**
     * Gets the right direction of the matrix.
     *
     * @return {Vector} The right direction of the matrix.
     */
    public GetRight(): Vector;

    /**
     * Returns the absolute scale of the matrix.
     *
     * @return {Vector} Absolute scale of the matrix
     */
    public GetScale(): Vector;

    /**
     * Returns the absolute translation of the matrix.
     *
     * @return {Vector} Absolute translation of the matrix
     */
    public GetTranslation(): Vector;

    /**
     * Returns the transpose (each row becomes a column) of this matrix.
     *
     * @return {VMatrix} The transposed matrix.
     */
    public GetTransposed(): VMatrix;

    /**
     * Gets the up direction of the matrix.
     *
     * @return {Vector} The up direction of the matrix.
     */
    public GetUp(): Vector;

    /**
     * Initializes the matrix as Identity matrix.
     */
    public Identity(): void;

    /**
     * Inverts the matrix.
     *
     * @return {boolean} Whether the matrix was inverted or not
     */
    public Invert(): boolean;

    /**
     * Inverts the matrix. This function will not fail, but only works correctly on matrices that contain only translation and/or rotation.
     */
    public InvertTR(): void;

    /**
     * Returns whether the matrix is equal to Identity matrix or not.
     *
     * @return {boolean} Is the matrix an Identity matrix or not
     */
    public IsIdentity(): boolean;

    /**
     * Returns whether the matrix is a rotation matrix or not.
     *
     * @return {boolean} Is the matrix a rotation matrix or not
     */
    public IsRotationMatrix(): boolean;

    /**
     * Checks whenever all fields of the matrix are 0, aka if this is a null matrix.
     *
     * @return {boolean} If the matrix is a null matrix.
     */
    public IsZero(): boolean;

    /**
     * Multiplies this matrix by given matrix.
     *
     * @arg VMatrix input - The input matrix to multiply by.
     */
    public Mul(input: VMatrix): void;

    /**
     * Rotates the matrix by the given angle.
     *
     * @arg Angle rotation - Rotation.
     */
    public Rotate(rotation: Angle): void;

    /**
     * Scales the matrix by the given vector.
     *
     * @arg Vector scale - Vector to scale with matrix with.
     */
    public Scale(scale: Vector): void;

    /**
     * Scales the absolute translation with the given value.
     *
     * @arg number scale - Value to scale the translation with.
     */
    public ScaleTranslation(scale: number): void;

    /**
     * Copies values from the given matrix object.
     *
     * @arg VMatrix src - The matrix to copy values from.
     */
    public Set(src: VMatrix): void;

    /**
     * Sets the absolute rotation of the matrix.
     *
     * @arg Angle angle - New angles.
     */
    public SetAngles(angle: Angle): void;

    /**
     * Sets a specific field in the matrix.
     *
     * @arg number row - Row of the field to be set, from 1 to 4
     * @arg number column - Column of the field to be set, from 1 to 4
     * @arg number value - The value to set in that field
     */
    public SetField(row: number, column: number, value: number): void;

    /**
     * Sets the forward direction of the matrix.
     *
     * @arg Vector forward - The forward direction of the matrix.
     */
    public SetForward(forward: Vector): void;

    /**
     * Sets the right direction of the matrix.
     *
     * @arg Vector forward - The right direction of the matrix.
     */
    public SetRight(forward: Vector): void;

    /**
     * Modifies the scale of the matrix while preserving the rotation and translation.
     *
     * @arg Vector scale - The scale to set.
     */
    public SetScale(scale: Vector): void;

    /**
     * Sets the absolute translation of the matrix.
     *
     * @arg Vector translation - New translation.
     */
    public SetTranslation(translation: Vector): void;

    /**
     * Sets each component of the matrix.
     *
     * @arg number e11
     * @arg number e12
     * @arg number e13
     * @arg number e14
     * @arg number e21
     * @arg number e22
     * @arg number e23
     * @arg number e24
     * @arg number e31
     * @arg number e32
     * @arg number e33
     * @arg number e34
     * @arg number e41
     * @arg number e42
     * @arg number e43
     * @arg number e44
     */
    public SetUnpacked(
        e11: number,
        e12: number,
        e13: number,
        e14: number,
        e21: number,
        e22: number,
        e23: number,
        e24: number,
        e31: number,
        e32: number,
        e33: number,
        e34: number,
        e41: number,
        e42: number,
        e43: number,
        e44: number
    ): void;

    /**
     * Sets the up direction of the matrix.
     *
     * @arg Vector forward - The up direction of the matrix.
     */
    public SetUp(forward: Vector): void;

    /**
     * Subtracts given matrix from this matrix.
     *
     * @arg VMatrix input - The input matrix to subtract.
     */
    public Sub(input: VMatrix): void;

    /**
     * Converts the matrix to a 4x4 table. See Matrix function.
     *
     * @return {object} The 4x4 table.
     */
    public ToTable(): object;

    /**
     * Translates the matrix by the given vector aka. adds the vector to the translation.
     *
     * @arg Vector translation - Vector to translate the matrix by.
     */
    public Translate(translation: Vector): void;

    /**
     * Returns each component of the matrix, expanding rows before columns.
     *
     * @return {number} VMatrix:GetField(1, 1)
     * @return {number} VMatrix:GetField(1, 2)
     * @return {number} VMatrix:GetField(1, 3)
     * @return {number} VMatrix:GetField(1, 4)
     * @return {number} VMatrix:GetField(2, 1)
     * @return {number} VMatrix:GetField(2, 2)
     * @return {number} VMatrix:GetField(2, 3)
     * @return {number} VMatrix:GetField(2, 4)
     * @return {number} VMatrix:GetField(3, 1)
     * @return {number} VMatrix:GetField(3, 2)
     * @return {number} VMatrix:GetField(3, 3)
     * @return {number} VMatrix:GetField(3, 4)
     * @return {number} VMatrix:GetField(4, 1)
     * @return {number} VMatrix:GetField(4, 2)
     * @return {number} VMatrix:GetField(4, 3)
     * @return {number} VMatrix:GetField(4, 4)
     */
    public Unpack(): LuaMultiReturn<
        [
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number,
            number
        ]
    >;

    /**
     * Sets all components of the matrix to 0, also known as a null matrix.
     */
    public Zero(): void;
}

declare type VMatrix = __VMatrixClass & number;
