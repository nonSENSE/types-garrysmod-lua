declare class __NextBotClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Become a ragdoll and remove the entity.
     *
     * @arg CTakeDamageInfo info - Damage info passed from an onkilled event
     * @return {Entity} The created ragdoll, if any.
     */
    public BecomeRagdoll(info: CTakeDamageInfo): Entity;

    /**
     * Should only be called in NEXTBOT:BodyUpdate. This sets the move_x and move_y pose parameters of the bot to fit how they're currently moving, sets the animation speed (Entity:GetPlaybackRate) to suit the ground speed, and calls Entity:FrameAdvance.
     */
    public BodyMoveXY(): void;

    /**
 * Like NextBot:FindSpots but only returns a vector.
* 
* @arg string type - Either "random", "near", "far"
* @arg object options - This table should contain the search info.
string type - The type (Only'hiding' for now)
Vector pos - the position to search.
number radius - the radius to search.
number stepup - the highest step to step up.
number stepdown - the highest we can step down without being hurt.
* @return {Vector} If it finds a spot it will return a vector. If not it will return nil.
 */
    public FindSpot(type: string, options: object): Vector;

    /**
 * Returns a table of hiding spots.
* 
* @arg object specs - This table should contain the search info.
string type - The type (optional, only 'hiding' supported)
Vector pos - the position to search.
number radius - the radius to search.
number stepup - the highest step to step up.
number stepdown - the highest we can step down without being hurt.
* @return {Vector} An unsorted table of tables containing:
Vector vector - The position of the hiding spot
number distance - the distance to that position
 */
    public FindSpots(specs: object): Vector;

    /**
     * Returns the currently running activity
     *
     * @return {number} The current activity
     */
    public GetActivity(): number;

    /**
     * Returns the Field of View of the Nextbot NPC, used for its vision functionality, such as NextBot:IsAbleToSee.
     *
     * @return {number} The current FOV of the nextbot
     */
    public GetFOV(): number;

    /**
     * Returns the maximum range the nextbot can see other nextbots/players at. See NextBot:IsAbleToSee.
     *
     * @return {number} The current vision range
     */
    public GetMaxVisionRange(): number;

    /**
     * Returns squared distance to an entity or a position.
     *
     * @arg Vector to - The position to measure distance to. Can be an entity.
     * @return {number} The squared distance
     */
    public GetRangeSquaredTo(to: Vector): number;

    /**
     * Returns the distance to an entity or position.
     *
     * @arg Vector to - The position to measure distance to. Can be an entity.
     * @return {number} The distance
     */
    public GetRangeTo(to: Vector): number;

    /**
     * Returns the solid mask for given NextBot.
     *
     * @return {number} The solid mask, see Enums/CONTENTS and Enums/MASK
     */
    public GetSolidMask(): number;

    /**
     * Called from Lua when the NPC is stuck. This should only be called from the behaviour coroutine - so if you want to override this function and do something special that yields - then go for it.
     */
    public HandleStuck(): void;

    /**
     * Returns if the Nextbot NPC can see the give entity or not.
     *
     * @arg Entity ent - The entity to test if we can see
     * @arg number [useFOV=NaN] - Whether to use the Field of View of the Nextbot
     * @return {boolean} If the nextbot can see or not
     */
    public IsAbleToSee(ent: Entity, useFOV?: number): boolean;

    /**
 * To be called in the behaviour coroutine only! Will yield until the bot has reached the goal or is stuck
* 
* @arg Vector pos - The position we want to get to
* @arg object options - A table containing a bunch of tweakable options.
number lookahead - Minimum look ahead distance.
number tolerance - How close we must be to the goal before it can be considered complete.
boolean draw - Draw the path. Only visible on listen servers and single player.
number maxage - Maximum age of the path before it times out.
number repath - Rebuilds the path after this number of seconds.
* @return {string} Either "failed", "stuck", "timeout" or "ok" - depending on how the NPC got on
 */
    public MoveToPos(pos: Vector, options: object): string;

    /**
     * To be called in the behaviour coroutine only! Plays an animation sequence and waits for it to end before returning.
     *
     * @arg string name - The sequence name
     * @arg number [speed=1] - Playback Rate of that sequence
     */
    public PlaySequenceAndWait(name: string, speed?: number): void;

    /**
     * Sets the Field of View for the Nextbot NPC, used for its vision functionality, such as NextBot:IsAbleToSee.
     *
     * @arg number fov - The new FOV
     */
    public SetFOV(fov: number): void;

    /**
     * Sets the maximum range the nextbot can see other nextbots/players at. See NextBot:IsAbleToSee.
     *
     * @arg number range - The new vision range to set.
     */
    public SetMaxVisionRange(range: number): void;

    /**
     * Sets the solid mask for given NextBot.
     *
     * @arg number mask - The new mask, see Enums/CONTENTS and Enums/MASK
     */
    public SetSolidMask(mask: number): void;

    /**
     * Start doing an activity (animation)
     *
     * @arg number activity - One of the Enums/ACT
     */
    public StartActivity(activity: number): void;
}

declare type NextBot = __NextBotClass;
