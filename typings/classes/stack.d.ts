declare class __StackClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Pop an item from the stack
     *
     * @arg number [amount=1] - Amount of items you want to pop.
     */
    public Pop(amount?: number): void;

    /**
     * Push an item onto the stack
     *
     * @arg any object - The item you want to push
     */
    public Push(object: any): void;

    /**
     * Returns the size of the stack
     *
     * @return {number} The size of the stack
     */
    public Size(): number;

    /**
     * Get the item at the top of the stack
     *
     * @return {any} The item at the top of the stack
     */
    public Top(): any;
}

declare type Stack = __StackClass;
