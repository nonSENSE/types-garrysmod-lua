declare class __ISaveClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Ends current data block started with ISave:StartBlock and returns to the parent block.
     */
    public EndBlock(): void;

    /**
     * Starts a new block of data that you can write to inside current block. Blocks must be ended with ISave:EndBlock.
     *
     * @arg string name - Name of the new block. Used for determining which block is which, returned by IRestore:StartBlock during game load.
     */
    public StartBlock(name: string): void;

    /**
     * Writes an Angle to the save object.
     *
     * @arg Angle ang - The angle to write.
     */
    public WriteAngle(ang: Angle): void;

    /**
     * Writes a boolean to the save object.
     *
     * @arg boolean bool - The boolean to write.
     */
    public WriteBool(bool: boolean): void;

    /**
     * Writes an Entity to the save object.
     *
     * @arg Entity ent - The entity to write.
     */
    public WriteEntity(ent: Entity): void;

    /**
     * Writes a floating point number to the save object.
     *
     * @arg number float - The floating point number to write.
     */
    public WriteFloat(float: number): void;

    /**
     * Writes an integer number to the save object.
     *
     * @arg number int - The integer number to write.
     */
    public WriteInt(int: number): void;

    /**
     * Writes a string to the save object.
     *
     * @arg string str - The string to write. Maximum length is 1024.
     */
    public WriteString(str: string): void;

    /**
     * Writes a Vector to the save object.
     *
     * @arg Vector vec - The vector to write.
     */
    public WriteVector(vec: Vector): void;
}

declare type ISave = __ISaveClass;
