declare class __ColorClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Sets the red, green, blue, and alpha of the color.
     *
     * @arg number r - The red component
     * @arg number g - The green component
     * @arg number b - The blue component
     * @arg number a - The alpha component
     */
    public SetUnpacked(r: number, g: number, b: number, a: number): void;

    /**
     * Converts a Color into HSL color space. This calls ColorToHSL internally.
     *
     * @return {number} The hue in degrees [0, 360).
     * @return {number} The saturation in the range [0, 1].
     * @return {number} The lightness in the range [0, 1].
     */
    public ToHSL(): LuaMultiReturn<[number, number, number]>;

    /**
     * Converts a Color into HSV color space. This calls ColorToHSV internally.
     *
     * @return {number} The hue in degrees [0, 360).
     * @return {number} The saturation in range [0, 1].
     * @return {number} The value in range [0, 1].
     */
    public ToHSV(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns the color as a table with four elements.
     *
     * @return {object} The table with elements 1 = r, 2 = g, 3 = b, 4 = a.
     */
    public ToTable(): object;

    /**
     * Translates the Color into a Vector, losing the alpha channel.
     * This will also range the values from 0 - 255 to 0 - 1
     *
     * @return {Vector} The created Vector
     */
    public ToVector(): Vector;

    /**
     * Returns the red, green, blue, and alpha of the color.
     *
     * @return {number} Red
     * @return {number} Green
     * @return {number} Blue
     * @return {number} Alpha
     */
    public Unpack(): LuaMultiReturn<[number, number, number, number]>;
}

// Allow the class to be instantiated with a function call:

/**
 * Creates a Color.
 *
 * @arg number r - An integer from 0-255 describing the red value of the color.
 * @arg number g - An integer from 0-255 describing the green value of the color.
 * @arg number b - An integer from 0-255 describing the blue value of the color.
 * @arg number [a=255] - An integer from 0-255 describing the alpha (transparency) of the color.
 * @return {Color} The created Color.
 */
declare function Color(r: number, g: number, b: number, a?: number): Color;

declare type Color = __ColorClass;
