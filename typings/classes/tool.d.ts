declare class __ToolClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns whether the tool is allowed to be used or not. This function ignores the SANDBOX:CanTool hook.
     *
     * @return {boolean} Returns true if the tool is allowed.
     */
    public Allowed(): boolean;

    /**
     * Builds a list of all ConVars set via the ClientConVar variable on the Structures/TOOL and their default values. This is used for the preset system.
     *
     * @return {object} A list of all convars and their default values.
     */
    public BuildConVarList(): object;

    /**
     * Checks all added objects to see if they're still valid, if not, clears the list of objects.
     */
    public CheckObjects(): void;

    /**
     * Clears all objects previously set with Tool:SetObject.
     */
    public ClearObjects(): void;

    /**
     * Initializes the tool object
     *
     * @return {Tool} The created tool object.
     */
    public Create(): Tool;

    /**
     * Creates clientside ConVars based on the ClientConVar table specified in the tool structure. Also creates the 'toolmode_allow_X' ConVar.
     */
    public CreateConVars(): void;

    /**
     * Retrieves a physics bone number previously stored using Tool:SetObject.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {number} Associated physics bone with given id.
     */
    public GetBone(id: number): number;

    /**
     * Attempts to grab a clientside tool ConVar.
     *
     * @arg string name - Name of the convar to retrieve. The function will automatically add the "mytoolfilename_" part to it.
     * @return {string} The value of the requested ConVar.
     */
    public GetClientInfo(name: string): string;

    /**
     * Attempts to grab a clientside tool ConVar.
     *
     * @arg string name - Name of the convar to retrieve. The function will automatically add the "mytoolfilename_" part to it.
     * @arg number [def=0] - The default value to return in case the lookup fails.
     * @return {number} The value of the requested ConVar.
     */
    public GetClientNumber(name: string, def?: number): number;

    /**
     * Retrieves an Entity previously stored using Tool:SetObject.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {Entity} Associated Entity with given id.
     */
    public GetEnt(id: number): Entity;

    /**
     * Returns a language key based on this tool's name and the current stage it is on.
     *
     * @return {string} The returned language key, for example "#tool.weld.1"
     */
    public GetHelpText(): string;

    /**
     * Retrieves an local vector previously stored using Tool:SetObject.
     * See also Tool:GetPos.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {Vector} Associated local vector with given id.
     */
    public GetLocalPos(id: number): Vector;

    /**
     * Returns the name of the current tool mode.
     *
     * @return {string} The current tool mode.
     */
    public GetMode(): string;

    /**
     * Retrieves an normal vector previously stored using Tool:SetObject.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {Vector} Associated normal vector with given id.
     */
    public GetNormal(id: number): Vector;

    /**
     * Returns the current operation of the tool set by Tool:SetOperation.
     *
     * @return {number} The current operation the tool is at.
     */
    public GetOperation(): number;

    /**
     * Returns the owner of this tool.
     *
     * @return {Entity} Player using the tool
     */
    public GetOwner(): Entity;

    /**
     * Retrieves an PhysObj previously stored using Tool:SetObject.
     * See also Tool:GetEnt.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {PhysObj} Associated PhysObj with given id. If it wasn't specified, returns current PhysObj of associated Entity.
     */
    public GetPhys(id: number): PhysObj;

    /**
     * Retrieves an vector previously stored using Tool:SetObject. See also Tool:GetLocalPos.
     *
     * @arg number id - The id of the object which was set in Tool:SetObject.
     * @return {Vector} Associated vector with given id. The vector is converted from Tool:GetLocalPos.
     */
    public GetPos(id: number): Vector;

    /**
     * Attempts to grab a serverside tool ConVar.
     * This will not do anything on client, despite the function being defined shared.
     *
     * @arg string name - Name of the convar to retrieve. The function will automatically add the "mytoolfilename_" part to it.
     * @return {string} The value of the requested ConVar.
     */
    public GetServerInfo(name: string): string;

    /**
     * Returns the current stage of the tool set by Tool:SetStage.
     *
     * @return {number} The current stage of the current operation the tool is at.
     */
    public GetStage(): number;

    /**
     * Initializes the ghost entity with the given model. Removes any old ghost entity if called multiple times.
     *
     * @arg string model - The model of the new ghost entity
     * @arg Vector pos - Position to initialize the ghost entity at, usually not needed since this is updated in Tool:UpdateGhostEntity.
     * @arg Angle angle - Angle to initialize the ghost entity at, usually not needed since this is updated in Tool:UpdateGhostEntity.
     */
    public MakeGhostEntity(model: string, pos: Vector, angle: Angle): void;

    /**
     * Returns the amount of stored objects ( Entitys ) the tool has.
     *
     * @return {number} The amount of stored objects, or Tool:GetStage clientide.
     */
    public NumObjects(): number;

    /**
     * Removes any ghost entity created for this tool.
     */
    public ReleaseGhostEntity(): void;

    /**
 * Stores an Entity for later use in the tool.
* 
* @arg number id - The id of the object to store.
* @arg Entity ent - The entity to store.
* @arg Vector pos - The position to store.
this position is in global space and is internally converted to local space relative to the object, so when you retrieve it later it will be corrected to the object's new position
* @arg PhysObj phys - The physics object to store.
* @arg number bone - The hit bone to store.
* @arg Vector normal - The hit normal to store.
 */
    public SetObject(id: number, ent: Entity, pos: Vector, phys: PhysObj, bone: number, normal: Vector): void;

    /**
     * Sets the current operation of the tool. Does nothing clientside. See also Tool:SetStage.
     *
     * @arg number operation - The new operation ID to set.
     */
    public SetOperation(operation: number): void;

    /**
     * Sets the current stage of the tool. Does nothing clientside.
     *
     * @arg number stage - The new stage to set.
     */
    public SetStage(stage: number): void;

    /**
     * Initializes the ghost entity based on the supplied entity.
     *
     * @arg Entity ent - The entity to copy ghost parameters off
     */
    public StartGhostEntity(ent: Entity): void;

    /**
     * Sets the tool's stage to how many stored objects the tool has.
     */
    public UpdateData(): void;

    /**
     * Updates the position and orientation of the ghost entity based on where the toolgun owner is looking along with data from object with id 1 set by Tool:SetObject.
     */
    public UpdateGhostEntity(): void;
}

declare type Tool = __ToolClass;
