declare class __CSEntClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Removes the clientside entity
     */
    public Remove(): void;
}

declare type CSEnt = __CSEntClass;
