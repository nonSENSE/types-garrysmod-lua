declare class __bf_readClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Reads an returns an angle object from the bitstream.
     *
     * @return {Angle} The read angle
     */
    public ReadAngle(): Angle;

    /**
     * Reads 1 bit an returns a bool representing the bit.
     *
     * @return {boolean} bit
     */
    public ReadBool(): boolean;

    /**
     * Reads a signed char and returns a number from -127 to 127 representing the ascii value of that char.
     *
     * @return {number} asciiVal
     */
    public ReadChar(): number;

    /**
     * Reads a short representing an entity index and returns the matching entity handle.
     *
     * @return {Entity} ent
     */
    public ReadEntity(): Entity;

    /**
     * Reads a 4 byte float from the bitstream and returns it.
     *
     * @return {number} float
     */
    public ReadFloat(): number;

    /**
     * Reads a 4 byte long from the bitstream and returns it.
     *
     * @return {number} int
     */
    public ReadLong(): number;

    /**
     * Reads a 2 byte short from the bitstream and returns it.
     *
     * @return {number} short
     */
    public ReadShort(): number;

    /**
     * Reads a null terminated string from the bitstream.
     *
     * @return {string} str
     */
    public ReadString(): string;

    /**
     * Reads a special encoded vector from the bitstream and returns it, this function is not suitable to send normals.
     *
     * @return {Vector} vec
     */
    public ReadVector(): Vector;

    /**
     * Reads a special encoded vector normal from the bitstream and returns it, this function is not suitable to send vectors that represent a position.
     *
     * @return {Vector} normal
     */
    public ReadVectorNormal(): Vector;

    /**
     * Rewinds the bitstream so it can be read again.
     */
    public Reset(): void;
}

declare type bf_read = __bf_readClass;
