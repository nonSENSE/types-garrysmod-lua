declare class __CSoundPatchClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adjust the pitch, alias the speed at which the sound is being played.
     *
     * @arg number pitch - The pitch can range from 0-255. Where 100 is the original pitch.
     * @arg number [deltaTime=0] - The time to fade from previous to the new pitch.
     */
    public ChangePitch(pitch: number, deltaTime?: number): void;

    /**
     * Adjusts the volume of the sound played.
     * Appears to only work while the sound is being played.
     *
     * @arg number volume - The volume ranges from 0 to 1.
     * @arg number [deltaTime=0] - Time to fade the volume from previous to new value from.
     */
    public ChangeVolume(volume: number, deltaTime?: number): void;

    /**
     * Fades out the volume of the sound from the current volume to 0 in the given amount of seconds.
     *
     * @arg number seconds - Fade time.
     */
    public FadeOut(seconds: number): void;

    /**
 * Returns the DSP ( Digital Signal Processor ) effect for the sound.
* 
* @return {number} The DSP effects of the sound
List of DSP's are Pick from the here.
 */
    public GetDSP(): number;

    /**
     * Returns the current pitch.
     *
     * @return {number} The current pitch, can range from 0-255.
     */
    public GetPitch(): number;

    /**
     * Returns the current sound level.
     *
     * @return {number} The current sound level, see Enums/SNDLVL.
     */
    public GetSoundLevel(): number;

    /**
     * Returns the current volume.
     *
     * @return {number} The current volume, ranging from 0 to 1.
     */
    public GetVolume(): number;

    /**
     * Returns whenever the sound is being played.
     *
     * @return {boolean} Is playing or not
     */
    public IsPlaying(): boolean;

    /**
     * Starts to play the sound. This will reset the sound's volume and pitch to their default values. See CSoundPatch:PlayEx
     */
    public Play(): void;

    /**
     * Same as CSoundPatch:Play but with 2 extra arguments allowing to set volume and pitch directly.
     *
     * @arg number volume - The volume ranges from 0 to 1.
     * @arg number pitch - The pitch can range from 0-255.
     */
    public PlayEx(volume: number, pitch: number): void;

    /**
 * Sets the DSP (Digital Signal Processor) effect for the sound. Similar to Player:SetDSP but for individual sounds.
* 
* @arg number dsp - The DSP effect to set.
Pick from the list of DSP's
 */
    public SetDSP(dsp: number): void;

    /**
     * Sets the sound level in decibel.
     *
     * @arg number level - The sound level in decibel. See Enums/SNDLVL
     */
    public SetSoundLevel(level: number): void;

    /**
     * Stops the sound from being played.
     */
    public Stop(): void;
}

declare type CSoundPatch = __CSoundPatchClass;
