declare class __ITextureClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Invokes the generator of the texture. Reloads file based textures from disk and clears render target textures.
     */
    public Download(): void;

    /**
     * Returns the color of the specified pixel, only works for textures created from PNG files.
     *
     * @arg number x - The X coordinate.
     * @arg number y - The Y coordinate.
     * @return {Color} The color of the pixel as a Color.
     */
    public GetColor(x: number, y: number): Color;

    /**
     * Returns the true unmodified height of the texture.
     *
     * @return {number} height
     */
    public GetMappingHeight(): number;

    /**
     * Returns the true unmodified width of the texture.
     *
     * @return {number} width
     */
    public GetMappingWidth(): number;

    /**
     * Returns the name of the texture, in most cases the path.
     *
     * @return {string} name
     */
    public GetName(): string;

    /**
     * Returns the number of animation frames in this texture.
     *
     * @return {number} The number of animation frames in this texture.
     */
    public GetNumAnimationFrames(): number;

    /**
     * Returns the modified height of the texture, this value may be affected by mipmapping and other factors.
     *
     * @return {number} height
     */
    public Height(): number;

    /**
     * Returns whenever the texture is valid. (i.e. was loaded successfully or not)
     *
     * @return {boolean} Whether the texture was loaded successfully or not.
     */
    public IsError(): boolean;

    /**
     * Returns whenever the texture is the error texture (pink and black checkerboard pattern).
     *
     * @return {boolean} Whether the texture is the error texture or not.
     */
    public IsErrorTexture(): boolean;

    /**
     * Returns the modified width of the texture, this value may be affected by mipmapping and other factors.
     *
     * @return {number} width
     */
    public Width(): number;
}

declare type ITexture = __ITextureClass;
