declare class __CLuaLocomotionClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Sets the location we want to get to
     *
     * @arg Vector goal - The vector we want to get to
     * @arg number goalweight - If unsure then set this to 1
     */
    public Approach(goal: Vector, goalweight: number): void;

    /**
     * Removes the stuck status from the bot
     */
    public ClearStuck(): void;

    /**
     * Sets the direction we want to face
     *
     * @arg Vector goal - The vector we want to face
     */
    public FaceTowards(goal: Vector): void;

    /**
     * Returns the acceleration speed
     *
     * @return {number} Current acceleration speed
     */
    public GetAcceleration(): number;

    /**
     * Returns whether the Nextbot is allowed to avoid obstacles or not.
     *
     * @return {boolean} Whether this bot is allowed to try to avoid obstacles.
     */
    public GetAvoidAllowed(): boolean;

    /**
     * Returns whether the Nextbot is allowed to climb or not.
     *
     * @return {boolean} Whether this bot is allowed to climb.
     */
    public GetClimbAllowed(): boolean;

    /**
     * Returns the current acceleration as a vector
     *
     * @return {Vector} Current acceleration
     */
    public GetCurrentAcceleration(): Vector;

    /**
     * Gets the height the bot is scared to fall from
     *
     * @return {number} Current death drop height
     */
    public GetDeathDropHeight(): number;

    /**
     * Gets the deceleration speed
     *
     * @return {number} Current deceleration speed
     */
    public GetDeceleration(): number;

    /**
     * Returns the locomotion's gravity.
     *
     * @return {number} The gravity.
     */
    public GetGravity(): number;

    /**
     * Return unit vector in XY plane describing our direction of motion - even if we are currently not moving
     *
     * @return {Vector} A vector representing the X and Y movement
     */
    public GetGroundMotionVector(): Vector;

    /**
     * Returns whether the Nextbot is allowed to jump gaps or not.
     *
     * @return {boolean} Whether this bot is allowed to jump gaps.
     */
    public GetJumpGapsAllowed(): boolean;

    /**
     * Gets the height of the bot's jump
     *
     * @return {number} Current jump height
     */
    public GetJumpHeight(): number;

    /**
     * Returns maximum jump height of this CLuaLocomotion.
     *
     * @return {number} The maximum jump height.
     */
    public GetMaxJumpHeight(): number;

    /**
     * Returns the max rate at which the NextBot can visually rotate.
     *
     * @return {number} Maximum yaw rate
     */
    public GetMaxYawRate(): number;

    /**
     * Returns the NextBot this locomotion is associated with.
     *
     * @return {NextBot} The nextbot
     */
    public GetNextBot(): NextBot;

    /**
     * Gets the max height the bot can step up
     *
     * @return {number} Current step height
     */
    public GetStepHeight(): number;

    /**
     * Returns the current movement velocity as a vector
     *
     * @return {Vector} Current velocity
     */
    public GetVelocity(): Vector;

    /**
     * Returns whether this CLuaLocomotion can reach and/or traverse/move in given CNavArea.
     *
     * @arg CNavArea area - The area to test
     * @return {boolean} Whether this CLuaLocomotion can traverse given CNavArea.
     */
    public IsAreaTraversable(area: CNavArea): boolean;

    /**
     * Returns true if we're trying to move.
     *
     * @return {boolean} Whether we're trying to move or not.
     */
    public IsAttemptingToMove(): boolean;

    /**
     * Returns true of the locomotion engine is jumping or climbing
     *
     * @return {boolean} Whether we're climbing or jumping or not
     */
    public IsClimbingOrJumping(): boolean;

    /**
     * Returns whether the nextbot this locomotion is attached to is on ground or not.
     *
     * @return {boolean} Whether the nextbot is on ground or not.
     */
    public IsOnGround(): boolean;

    /**
     * Returns true if we're stuck
     *
     * @return {boolean} Whether we're stuck or not
     */
    public IsStuck(): boolean;

    /**
     * Returns whether or not the target in question is on a ladder or not.
     *
     * @return {boolean} If the target is on a ladder or not.
     */
    public IsUsingLadder(): boolean;

    /**
     * Makes the bot jump. It must be on ground (Entity:IsOnGround) and its model must have ACT_JUMP activity.
     */
    public Jump(): void;

    /**
     * Makes the bot jump across a gap. The bot must be on ground (Entity:IsOnGround) and its model must have ACT_JUMP activity.
     *
     * @arg Vector landingGoal
     * @arg Vector landingForward
     */
    public JumpAcrossGap(landingGoal: Vector, landingForward: Vector): void;

    /**
     * Sets the acceleration speed
     *
     * @arg number speed - Speed acceleration (default is 400)
     */
    public SetAcceleration(speed: number): void;

    /**
     * Sets whether the Nextbot is allowed try to to avoid obstacles or not. This is used during path generation. Works similarly to nb_allow_avoiding convar. By default bots are allowed to try to avoid obstacles.
     *
     * @arg boolean allowed - Whether this bot should be allowed to try to avoid obstacles.
     */
    public SetAvoidAllowed(allowed: boolean): void;

    /**
     * Sets whether the Nextbot is allowed to climb or not. This is used during path generation. Works similarly to nb_allow_climbing convar. By default bots are allowed to climb.
     *
     * @arg boolean allowed - Whether this bot should be allowed to climb.
     */
    public SetClimbAllowed(allowed: boolean): void;

    /**
     * Sets the height the bot is scared to fall from.
     *
     * @arg number height - Height (default is 200)
     */
    public SetDeathDropHeight(height: number): void;

    /**
     * Sets the deceleration speed.
     *
     * @arg number deceleration - New deceleration speed (default is 400)
     */
    public SetDeceleration(deceleration: number): void;

    /**
     * Sets movement speed.
     *
     * @arg number speed - The new desired speed
     */
    public SetDesiredSpeed(speed: number): void;

    /**
     * Sets the locomotion's gravity.
     *
     * @arg number gravity - New gravity to set. Default is 1000.
     */
    public SetGravity(gravity: number): void;

    /**
     * Sets whether the Nextbot is allowed to jump gaps or not. This is used during path generation. Works similarly to nb_allow_gap_jumping convar. By default bots are allowed to jump gaps.
     *
     * @arg boolean allowed - Whether this bot should be allowed to jump gaps.
     */
    public SetJumpGapsAllowed(allowed: boolean): void;

    /**
     * Sets the height of the bot's jump
     *
     * @arg number height - Height (default is 58)
     */
    public SetJumpHeight(height: number): void;

    /**
     * Sets the max rate at which the NextBot can visually rotate. This will not affect moving or pathing.
     *
     * @arg number yawRate - Desired new maximum yaw rate
     */
    public SetMaxYawRate(yawRate: number): void;

    /**
     * Sets the max height the bot can step up
     *
     * @arg number height - Height (default is 18)
     */
    public SetStepHeight(height: number): void;

    /**
     * Sets the current movement velocity
     *
     * @arg Vector velocity
     */
    public SetVelocity(velocity: Vector): void;
}

declare type CLuaLocomotion = __CLuaLocomotionClass;
