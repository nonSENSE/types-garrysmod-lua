declare class __CLuaEmitterClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Creates a new CLuaParticle with the given material and position.
     *
     * @arg string material - The particles material. Can also be an IMaterial.
     * @arg Vector position - The position to spawn the particle on.
     * @return {CLuaParticle} The created particle, if any.
     */
    public Add(material: string, position: Vector): CLuaParticle;

    /**
     * Manually renders all particles the emitter has created.
     */
    public Draw(): void;

    /**
     * Removes the emitter, making it no longer usable from Lua. If particles remain, the emitter will be removed when all particles die.
     */
    public Finish(): void;

    /**
     * Returns the amount of active particles of this emitter.
     *
     * @return {number} The amount of active particles of this emitter
     */
    public GetNumActiveParticles(): number;

    /**
     * Returns the position of this emitter. This is set when creating the emitter with ParticleEmitter.
     *
     * @return {Vector} Position of this particle emitter.
     */
    public GetPos(): Vector;

    /**
     * Returns whether this emitter is 3D or not. This is set when creating the emitter with ParticleEmitter.
     *
     * @return {boolean} Whether this emitter is 3D or not.
     */
    public Is3D(): boolean;

    /**
     * Returns whether this CLuaEmitter is valid or not.
     *
     * @return {boolean} Whether this CLuaEmitter is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Sets the bounding box for this emitter.
     *
     * @arg Vector mins - The minimum position of the box
     * @arg Vector maxs - The maximum position of the box
     */
    public SetBBox(mins: Vector, maxs: Vector): void;

    /**
     * This function sets the the distance between the render camera and the emitter at which the particles should start fading and at which distance fade ends ( alpha becomes 0 ).
     *
     * @arg number distanceMin - Min distance where the alpha becomes 0.
     * @arg number distanceMax - Max distance where the alpha starts fading.
     */
    public SetNearClip(distanceMin: number, distanceMax: number): void;

    /**
     * Prevents all particles of the emitter from automatically drawing.
     *
     * @arg boolean noDraw - Whether we should draw the particles ( false ) or not ( true )
     */
    public SetNoDraw(noDraw: boolean): void;

    /**
     * The function name has not much in common with its actual function, it applies a radius to every particles that affects the building of the bounding box, as it, usually is constructed by the particle that has the lowest x, y and z and the highest x, y and z, this function just adds/subtracts the radius and inflates the bounding box.
     *
     * @arg number radius - Particle radius.
     */
    public SetParticleCullRadius(radius: number): void;

    /**
     * Sets the position of the particle emitter.
     *
     * @arg Vector position - New position.
     */
    public SetPos(position: Vector): void;
}

declare type CLuaEmitter = __CLuaEmitterClass;
