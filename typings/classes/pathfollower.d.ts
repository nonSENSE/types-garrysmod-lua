declare class __PathFollowerClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * If you created your path with type "Chase" this functions should be used in place of PathFollower:Update to cause the bot to chase the specified entity.
     *
     * @arg NextBot bot - The bot to update along the path. This can also be a nextbot player (player.CreateNextbot)
     * @arg Entity ent - The entity we want to chase
     */
    public Chase(bot: NextBot, ent: Entity): void;

    /**
 * Compute shortest path from bot to 'goal' via A* algorithm.
* 
* @arg NextBot from - The nextbot we're generating for.  This can also be a nextbot player (player.CreateNextbot).
* @arg Vector to - To point
* @arg GMLua.CallbackNoContext [generator=nil] - A funtion that allows you to alter the path generation. See example below for the default function.
* @return {boolean} If returns true, path was found to the goal position.
If returns false, path may either be invalid (use IsValid() to check), or valid but doesn't reach all the way to the goal.
 */
    public Compute(from: NextBot, to: Vector, generator?: GMLua.CallbackNoContext): boolean;

    /**
     * Draws the path. This is meant for debugging - and uses debug overlay.
     */
    public Draw(): void;

    /**
     * Returns the first segment of the path.
     *
     * @return {PathSegmentStruct} A table with Structures/PathSegment.
     */
    public FirstSegment(): PathSegmentStruct;

    /**
     * Returns the age since the path was built
     *
     * @return {number} Path age
     */
    public GetAge(): number;

    /**
     * Returns all of the segments of the given path.
     *
     * @return {PathSegmentStruct} A table of tables with Structures/PathSegment.
     */
    public GetAllSegments(): PathSegmentStruct;

    /**
     * The closest position along the path to a position
     *
     * @arg Vector position - The point we're querying for
     * @return {Vector} The closest position on the path
     */
    public GetClosestPosition(position: Vector): Vector;

    /**
     * Returns the current goal data. Can return nil if the current goal is invalid, for example immediately after PathFollower:Update.
     *
     * @return {PathSegmentStruct} A table with Structures/PathSegment.
     */
    public GetCurrentGoal(): PathSegmentStruct;

    /**
 * Returns the cursor data
* 
* @return {number} A table with 3 keys:
number curvature
Vector forward
Vector pos
 */
    public GetCursorData(): number;

    /**
     * Returns the current progress along the path
     *
     * @return {number} The current progress
     */
    public GetCursorPosition(): number;

    /**
     * Returns the path end position
     *
     * @return {Vector} The end position
     */
    public GetEnd(): Vector;

    /**
     * Returns how close we can get to the goal to call it done.
     *
     * @return {number} The distance we're setting it to
     */
    public GetGoalTolerance(): number;

    /**
     *
     *
     * @return {Entity}
     */
    public GetHindrance(): Entity;

    /**
     * Returns the total length of the path
     *
     * @return {number} The length of the path
     */
    public GetLength(): number;

    /**
     * Returns the minimum range movement goal must be along path.
     *
     * @return {number} The minimum look ahead distance
     */
    public GetMinLookAheadDistance(): number;

    /**
     * Returns the vector position of distance along path
     *
     * @arg number distance - The distance along the path to query
     * @return {Vector} The position
     */
    public GetPositionOnPath(distance: number): Vector;

    /**
     * Returns the path start position
     *
     * @return {Vector} The start position
     */
    public GetStart(): Vector;

    /**
     * Invalidates the current path
     */
    public Invalidate(): void;

    /**
     * Returns true if the path is valid
     *
     * @return {boolean} Wether the path is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Returns the last segment of the path.
     *
     * @return {PathSegmentStruct} A table with Structures/PathSegment.
     */
    public LastSegment(): PathSegmentStruct;

    /**
     * Moves the cursor by give distance.
     *
     * @arg number distance - The distance to move the cursor (in relative world units)
     */
    public MoveCursor(distance: number): void;

    /**
     * Sets the cursor position to given distance.
     *
     * @arg number distance - The distance to move the cursor (in world units)
     */
    public MoveCursorTo(distance: number): void;

    /**
 * Moves the cursor of the path to the closest position compared to given vector.
* 
* @arg Vector pos
* @arg number [type=0] - Seek type
0 = SEEK_ENTIRE_PATH - Search the entire path length
1 = SEEK_AHEAD - Search from current cursor position forward toward end of path
2 = SEEK_BEHIND - Search from current cursor position backward toward path start
* @arg number [alongLimit=0]
 */
    public MoveCursorToClosestPosition(pos: Vector, type?: number, alongLimit?: number): void;

    /**
     * Moves the cursor to the end of the path
     */
    public MoveCursorToEnd(): void;

    /**
     * Moves the cursor to the end of the path
     */
    public MoveCursorToStart(): void;

    /**
     * Resets the age which is retrieved by PathFollower:GetAge to 0.
     */
    public ResetAge(): void;

    /**
     * How close we can get to the goal to call it done
     *
     * @arg number distance - The distance we're setting it to
     */
    public SetGoalTolerance(distance: number): void;

    /**
     * Sets minimum range movement goal must be along path
     *
     * @arg number mindist - The minimum look ahead distance
     */
    public SetMinLookAheadDistance(mindist: number): void;

    /**
     * Move the bot along the path.
     *
     * @arg NextBot bot - The bot to update along the path
     */
    public Update(bot: NextBot): void;
}

declare type PathFollower = __PathFollowerClass;
