declare class __ConVarClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Tries to convert the current string value of a ConVar to a boolean.
     *
     * @return {boolean} The boolean value of the console variable. If the variable is numeric and not 0, the result will be true. Otherwise the result will be false.
     */
    public GetBool(): boolean;

    /**
     * Returns the default value of the ConVar
     *
     * @return {string} The default value of the console variable.
     */
    public GetDefault(): string;

    /**
     * Returns the Enums/FCVAR flags of the ConVar
     *
     * @return {number} The bitflag. See Enums/FCVAR
     */
    public GetFlags(): number;

    /**
 * Attempts to convert the ConVar value to a float
* 
* @return {number} The float value of the console variable.
If the value cannot be converted to a float, it will return 0.
 */
    public GetFloat(): number;

    /**
     * Returns the help text assigned to that convar.
     *
     * @return {string} The help text
     */
    public GetHelpText(): string;

    /**
 * Attempts to convert the ConVar value to a integer.
* 
* @return {number} The integer value of the console variable.
If it fails to convert to an integer, it will return 0.
All float/decimal values will be rounded down. ( With math.floor )
 */
    public GetInt(): number;

    /**
     * Returns the maximum value of the ConVar
     *
     * @return {number} The maximum value of the ConVar
     */
    public GetMax(): number;

    /**
     * Returns the minimum value of the ConVar
     *
     * @return {number} The minimum value of the ConVar
     */
    public GetMin(): number;

    /**
     * Returns the name of the ConVar.
     *
     * @return {string} The name of the console variable.
     */
    public GetName(): string;

    /**
     * Returns the current ConVar value as a string.
     *
     * @return {string} The current console variable value as a string.
     */
    public GetString(): string;

    /**
     * Returns whether the specified flag is set on the ConVar
     *
     * @arg number flag - The Enums/FCVAR flag to test
     * @return {boolean} Whether the flag is set or not
     */
    public IsFlagSet(flag: number): boolean;

    /**
     * Reverts ConVar to its default value
     */
    public Revert(): void;

    /**
     * Sets a ConVar's value to 1 or 0 based on the input boolean. This can only be ran on ConVars created from within Lua.
     *
     * @arg boolean value - Value to set the ConVar to.
     */
    public SetBool(value: boolean): void;

    /**
     * Sets a ConVar's value to the input number.
     *
     * @arg number value - Value to set the ConVar to.
     */
    public SetFloat(value: number): void;

    /**
     * Sets a ConVar's value to the input number after converting it to an integer.
     *
     * @arg number value - Value to set the ConVar to.
     */
    public SetInt(value: number): void;

    /**
     * Sets a ConVar's value to the input string. This can only be ran on ConVars created from within Lua.
     *
     * @arg string value - Value to set the ConVar to.
     */
    public SetString(value: string): void;
}

// Allow the class to be instantiated with a function call:

/**
 * Returns whether a ConVar with the given name exists or not
 *
 * @arg string name - Name of the ConVar.
 * @return {boolean} True if the ConVar exists, false otherwise.
 */
declare function ConVarExists(name: string): boolean;

declare type ConVar = __ConVarClass;
