declare class __EntityClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Activates the entity. This needs to be used on some entities (like constraints) after being spawned.
     */
    public Activate(): void;

    /**
 * Add a callback function to a specific event. This is used instead of hooks to avoid calling empty functions unnecessarily.
* 
* @arg string hook - The hook name to hook onto. See Entity Callbacks
* @arg GMLua.CallbackNoContext func - The function to call
* @return {number} The callback ID that was just added, which can later be used in Entity:RemoveCallback.
Returns nothing if the passed callback function was invalid or when asking for a non-existent hook.
 */
    public AddCallback(hook: string, func: GMLua.CallbackNoContext): number;

    /**
     * Applies an engine effect to an entity.
     *
     * @arg number effect - The effect to apply, see Enums/EF.
     */
    public AddEffects(effect: number): void;

    /**
     * Adds engine flags.
     *
     * @arg number flag - Engine flag to add, see Enums/EFL
     */
    public AddEFlags(flag: number): void;

    /**
     * Adds flags to the entity.
     *
     * @arg number flag - Flag to add, see Enums/FL
     */
    public AddFlags(flag: number): void;

    /**
     * Adds a gesture animation to the entity and plays it.
     *
     * @arg number activity - The activity to play as the gesture. See Enums/ACT.
     * @arg boolean [autokill=true]
     * @return {number} Layer ID of the started gesture, used to manipulate the played gesture by other functions.
     */
    public AddGesture(activity: number, autokill?: boolean): number;

    /**
     * Adds a gesture animation to the entity and plays it.
     *
     * @arg number sequence - The sequence ID to play as the gesture. See Entity:LookupSequence.
     * @arg boolean [autokill=true]
     * @return {number} Layer ID of the started gesture, used to manipulate the played gesture by other functions.
     */
    public AddGestureSequence(sequence: number, autokill?: boolean): number;

    /**
     * Adds a gesture animation to the entity and plays it.
     *
     * @arg number sequence - The sequence ID to play as the gesture. See Entity:LookupSequence.
     * @arg number priority
     * @return {number} Layer ID of created layer
     */
    public AddLayeredSequence(sequence: number, priority: number): number;

    /**
     * Adds solid flag(s) to the entity.
     *
     * @arg number flags - The flag(s) to apply, see Enums/FSOLID.
     */
    public AddSolidFlags(flags: number): void;

    /**
     * Adds a PhysObject to the entity's motion controller so that ENTITY:PhysicsSimulate will be called for given PhysObject as well.
     *
     * @arg PhysObj physObj - The PhysObj to add to the motion controller.
     */
    public AddToMotionController(physObj: PhysObj): void;

    /**
     * Returns an angle based on the ones inputted that you can use to align an object.
     *
     * @arg Angle from - The angle you want to align from
     * @arg Angle to - The angle you want to align to
     * @return {Angle} The resulting aligned angle
     */
    public AlignAngles(from: Angle, to: Angle): Angle;

    /**
     * Spawns a clientside ragdoll for the entity, positioning it in place of the original entity, and makes the entity invisible. It doesn't preserve flex values (face posing) as CSRagdolls don't support flex.
     *
     * @return {Entity} The created ragdoll. (class C_ClientRagdoll])
     */
    public BecomeRagdollOnClient(): Entity;

    /**
     * Returns true if the entity is being looked at by the local player and is within 256 units of distance.
     *
     * @return {boolean} Is the entity being looked at by the local player and within 256 units.
     */
    public BeingLookedAtByLocalPlayer(): boolean;

    /**
     * Dispatches blocked events to this entity's blocked handler. This function is only useful when interacting with entities like func_movelinear.
     *
     * @arg Entity entity - The entity that is blocking us
     */
    public Blocked(entity: Entity): void;

    /**
     * Returns a centered vector of this entity, NPCs use this internally to aim at their targets.
     *
     * @arg Vector origin - The vector of where the the attack comes from.
     * @arg boolean [noisy=false] - Decides if it should return the centered vector with a random offset to it.
     * @return {Vector} The centered vector.
     */
    public BodyTarget(origin: Vector, noisy?: boolean): Vector;

    /**
     * Returns whether the entity's bone has the flag or not.
     *
     * @arg number boneID - Bone ID to test flag of.
     * @arg number flag - The flag to test, see Enums/BONE
     * @return {boolean} Whether the bone has that flag or not
     */
    public BoneHasFlag(boneID: number, flag: number): boolean;

    /**
     * Returns the length between given bone's position and the position of given bone's parent.
     *
     * @arg number boneID - The ID of the bone you want the length of. You may want to get the length of the next bone ( boneID + 1 ) for decent results
     * @return {number} The length of the bone
     */
    public BoneLength(boneID: number): number;

    /**
     * Returns the distance between the center of the bounding box and the furthest bounding box corner.
     *
     * @return {number} The radius of the bounding box.
     */
    public BoundingRadius(): number;

    /**
     * Causes a specified function to be run if the entity is removed by any means. This can later be undone by Entity:RemoveCallOnRemove if you need it to not run.
     *
     * @arg string identifier - Identifier of the function within CallOnRemove
     * @arg GMLua.CallbackNoContext removeFunc - Function to be called on remove
     * @arg args[] args - Optional arguments to pass to removeFunc. Do note that the first argument passed to the function will always be the entity being removed, and the arguments passed on here start after that.
     */
    public CallOnRemove(identifier: string, removeFunc: GMLua.CallbackNoContext, ...args: any[]): void;

    /**
     * Resets all pose parameters such as aim_yaw, aim_pitch and rotation.
     */
    public ClearPoseParameters(): void;

    /**
     * Declares that the collision rules of the entity have changed, and subsequent calls for GM:ShouldCollide with this entity may return a different value than they did previously.
     */
    public CollisionRulesChanged(): void;

    /**
     * Creates bone followers based on the current entity model.
     */
    public CreateBoneFollowers(): void;

    /**
     * Returns whether the entity was created by map or not.
     *
     * @return {boolean} Is created by map?
     */
    public CreatedByMap(): boolean;

    /**
 * Creates a clientside particle system attached to the entity. See also CreateParticleSystem
* 
* @arg string particle - The particle name to create
* @arg number attachment - Attachment ID to attach the particle to
* @arg object [options=nil] - A table of tables ( IDs 1 to 64 ) having the following structure:

number attachtype - The particle attach type. See PATTACH. Default: PATTACH_ABSORIGIN
Entity entity - The parent entity? Default: NULL
Vector position - The offset position for given control point. Default:  nil

This only affects the control points of the particle effects and will do nothing if the effect doesn't use control points.
* @return {CNewParticleEffect} The created particle system.
 */
    public CreateParticleEffect(particle: string, attachment: number, options?: object): CNewParticleEffect;

    /**
     * Draws the shadow of an entity.
     */
    public CreateShadow(): void;

    /**
     * Whenever the entity is removed, entityToRemove will be removed also.
     *
     * @arg Entity entityToRemove - The entity to be removed
     */
    public DeleteOnRemove(entityToRemove: Entity): void;

    /**
     * Destroys bone followers created by Entity:CreateBoneFollowers.
     */
    public DestroyBoneFollowers(): void;

    /**
     * Removes the shadow for the entity.
     */
    public DestroyShadow(): void;

    /**
 * Disables an active matrix.
* 
* @arg string matrixType - The name of the matrix type to disable.
The only known matrix type is "RenderMultiply".
 */
    public DisableMatrix(matrixType: string): void;

    /**
     * Performs a trace attack.
     *
     * @arg CTakeDamageInfo damageInfo - The damage to apply.
     * @arg TraceResultStruct traceRes - Trace result to use to deal damage. See Structures/TraceResult
     * @arg Vector [dir=traceRes.HitNormal] - Direction of the attack.
     */
    public DispatchTraceAttack(damageInfo: CTakeDamageInfo, traceRes: TraceResultStruct, dir?: Vector): void;

    /**
     * This removes the argument entity from an ent's list of entities to 'delete on remove'
     *
     * @arg Entity entityToUnremove - The entity to be removed from the list of entities to delete
     */
    public DontDeleteOnRemove(entityToUnremove: Entity): void;

    /**
     * Draws the entity or model.
     *
     * @arg number [flags=NaN] - The optional STUDIO_ flags, usually taken from ENTITY:Draw and similar hooks.
     */
    public DrawModel(flags?: number): void;

    /**
     * Sets whether an entity's shadow should be drawn.
     *
     * @arg boolean shouldDraw - True to enable, false to disable shadow drawing.
     */
    public DrawShadow(shouldDraw: boolean): void;

    /**
     * Move an entity down until it collides with something.
     */
    public DropToFloor(): void;

    /**
     * Sets up a self.dt.NAME alias for a Data Table variable.
     *
     * @arg string Type - The type of the DTVar being set up. It can be one of the following: 'Int', 'Float', 'Vector', 'Angle', 'Bool', 'Entity' or 'String'
     * @arg number ID - The ID of the DTVar. Can be between 0 and 3 for strings, 0 and 31 for everything else.
     * @arg string Name - Name by which you will refer to DTVar. It must be a valid variable name. (No spaces!)
     */
    public DTVar(Type: string, ID: number, Name: string): void;

    /**
 * Plays a sound on an entity. If run clientside, the sound will only be heard locally.
* 
* @arg string soundName - The name of the sound to be played.
The string cannot have whitespace at the start or end. You can remove this with string.Trim.
* @arg number [soundLevel=75] - A modifier for the distance this sound will reach, acceptable range is 0 to 511. 100 means no adjustment to the level. See Enums/SNDLVL
Will not work if a sound script is used.
* @arg number [pitchPercent=100] - The pitch applied to the sound. The acceptable range is from 0 to 255. 100 means the pitch is not changed.
* @arg number [volume=1] - The volume, from 0 to 1.
* @arg number [channel=NaN] - The sound channel, see Enums/CHAN.
Will not work if a sound script is used.
* @arg number [soundFlags=0] - The flags of the sound, see Enums/SND
* @arg number [dsp=0] - The DSP preset for this sound. List of DSP presets
 */
    public EmitSound(
        soundName: string,
        soundLevel?: number,
        pitchPercent?: number,
        volume?: number,
        channel?: number,
        soundFlags?: number,
        dsp?: number
    ): void;

    /**
 * Toggles the constraints of this ragdoll entity on and off.
* 
* @arg boolean toggleConstraints - Set to true to enable the constraints and false to disable them.
Disabling constraints will delete the constraint entities.
 */
    public EnableConstraints(toggleConstraints: boolean): void;

    /**
     * Flags an entity as using custom lua defined collisions. Fixes entities having spongy player collisions or not hitting traces, such as after Entity:PhysicsFromMesh
     *
     * @arg boolean useCustom - True to flag this entity
     */
    public EnableCustomCollisions(useCustom: boolean): void;

    /**
 * Can be used to apply a custom VMatrix to the entity, mostly used for scaling the model by a Vector.
* 
* @arg string matrixType - The name of the matrix type. 
The only implemented matrix type is "RenderMultiply".
* @arg VMatrix matrix - The matrix to apply before drawing the entity.
 */
    public EnableMatrix(matrixType: string, matrix: VMatrix): void;

    /**
 * Gets the unique entity index of an entity.
* 
* @return {number} The index of the entity.
-1 for clientside-only or serverside-only entities.
 */
    public EntIndex(): number;

    /**
     * Extinguishes the entity if it is on fire.
     */
    public Extinguish(): void;

    /**
     * Returns the direction a player, npc or ragdoll is looking as a world-oriented angle.
     *
     * @return {Angle} Player's eye angle.
     */
    public EyeAngles(): Angle;

    /**
     * Returns the position of an Player/NPC's view.
     *
     * @return {Vector} View position of the entity.
     */
    public EyePos(): Vector;

    /**
     * Searches for bodygroup with given name.
     *
     * @arg string name - The bodygroup name to search for.
     * @return {number} Bodygroup ID, -1 if not found
     */
    public FindBodygroupByName(name: string): number;

    /**
     * Returns a transition from the given start and end sequence.
     *
     * @arg number currentSequence - The currently playing sequence
     * @arg number goalSequence - The goal sequence.
     * @return {number} The transition sequence, -1 if not available.
     */
    public FindTransitionSequence(currentSequence: number, goalSequence: number): number;

    /**
     * Fires an entity's input, conforming to the map IO event queue system. You can find inputs for most entities on the Valve Developer Wiki
     *
     * @arg string input - The name of the input to fire
     * @arg string [param="nil"] - The value to give to the input, can also be a number or a boolean.
     * @arg number [delay=0] - Delay in seconds before firing
     * @arg Entity [activator=nil] - The entity that caused this input (i.e. the player who pushed a button)
     * @arg Entity [caller=nil] - The entity that is triggering this input (i.e. the button that was pushed)
     */
    public Fire(input: string, param?: string, delay?: number, activator?: Entity, caller?: Entity): void;

    /**
     * Fires a bullet.
     *
     * @arg BulletStruct bulletInfo - The bullet data to be used. See the Structures/Bullet.
     * @arg boolean [suppressHostEvents=false] - Has the effect of encasing the FireBullets call in SuppressHostEvents, only works in multiplayer.
     */
    public FireBullets(bulletInfo: BulletStruct, suppressHostEvents?: boolean): void;

    /**
     * Makes an entity follow another entity's bone.
     *
     * @arg Entity [parent=NULL] - The entity to follow the bone of. If unset, removes the FollowBone effect.
     * @arg number boneid - The bone to follow
     */
    public FollowBone(parent?: Entity, boneid?: number): void;

    /**
     * Forces the Entity to be dropped, when it is being held by a player's gravitygun or physgun.
     */
    public ForcePlayerDrop(): void;

    /**
     * Advances the cycle of an animated entity.
     */
    public FrameAdvance(): void;

    /**
     * Returns the entity's velocity.
     *
     * @return {Vector} The velocity of the entity.
     */
    public GetAbsVelocity(): Vector;

    /**
     * Gets the angles of given entity.
     *
     * @return {Angle} The angles of the entity.
     */
    public GetAngles(): Angle;

    /**
     * Returns a table containing the number of frames, flags, name, and FPS of an entity's animation ID.
     *
     * @arg number animIndex - The animation ID to look up
     * @return {object} Information about the animation, or nil if the index is out of bounds
     */
    public GetAnimInfo(animIndex: number): object;

    /**
     * Returns the last time the entity had an animation update. Returns 0 if the entity doesn't animate.
     *
     * @return {number} The last time the entity had an animation update.
     */
    public GetAnimTime(): number;

    /**
     * Returns the amount of time since last animation.
     *
     * @return {number} The amount of time since last animation.
     */
    public GetAnimTimeInterval(): number;

    /**
     * Gets the orientation and position of the attachment by its ID, returns nothing if the attachment does not exist.
     *
     * @arg number attachmentId - The internal ID of the attachment.
     * @return {AngPosStruct} The angle and position of the attachment. See the Structures/AngPos. Most notably, the table contains the keys "Ang" and "Pos".
     */
    public GetAttachment(attachmentId: number): AngPosStruct;

    /**
     * Returns a table containing all attachments of the given entity's model.
     *
     * @return {AttachmentDataStruct} Attachment data. See Structures/AttachmentData.
     */
    public GetAttachments(): AttachmentDataStruct;

    /**
     * Returns the entity's base velocity which is their velocity due to forces applied by other entities. This includes entity-on-entity collision or riding a treadmill.
     *
     * @return {Vector} The base velocity of the entity.
     */
    public GetBaseVelocity(): Vector;

    /**
     * Returns the blood color of this entity. This can be set with Entity:SetBloodColor.
     *
     * @return {number} Color from Enums/BLOOD_COLOR
     */
    public GetBloodColor(): number;

    /**
     * Gets the exact value for specific bodygroup of given entity.
     *
     * @arg number id - The id of bodygroup to get value of. Starts from 0.
     * @return {number} Current bodygroup. Starts from 0.
     */
    public GetBodygroup(id: number): number;

    /**
     * Returns the count of possible values for this bodygroup.
     *
     * @arg number bodygroup - The ID of bodygroup to retrieve count of.
     * @return {number} Count of values of passed bodygroup.
     */
    public GetBodygroupCount(bodygroup: number): number;

    /**
     * Gets the name of specific bodygroup for given entity.
     *
     * @arg number id - The id of bodygroup to get the name of.
     * @return {string} The name of the bodygroup
     */
    public GetBodygroupName(id: number): string;

    /**
     * Returns a list of all body groups of the entity.
     *
     * @return {BodyGroupDataStruct} Bodygroups as a table of Structures/BodyGroupDatas if the entity can have bodygroups.
     */
    public GetBodyGroups(): BodyGroupDataStruct;

    /**
     * Returns the contents of the specified bone.
     *
     * @arg number bone - The bone id, starting at index 0. See Entity:LookupBone.
     * @return {number} The contents as a Enums/CONTENTS or 0 on failure.
     */
    public GetBoneContents(bone: number): number;

    /**
     * Returns the value of the bone controller with the specified ID.
     *
     * @arg number boneID - ID of the bone controller. Goes from 0 to 3.
     * @return {number} The value set on the bone controller.
     */
    public GetBoneController(boneID: number): number;

    /**
     * Returns the amount of bones in the entity.
     *
     * @return {number} The amount of bones in given entity; -1 on failure.
     */
    public GetBoneCount(): number;

    /**
 * Returns the transformation matrix of a given bone on the entity's model. The matrix contains the transformation used to position the bone in the world. It is not relative to the parent bone.
* 
* @arg number boneID - The bone ID to retrieve matrix of, starting at index 0.

Bones clientside and serverside will differ
* @return {VMatrix} The matrix

Some entities don't update animation every frame such as prop_physics and won't have accurate bone matrix.
 */
    public GetBoneMatrix(boneID: number): VMatrix;

    /**
 * Returns name of given bone id.
* 
* @arg number index - ID of bone to lookup name of, starting at index 0.
* @return {string} The name of given bone.

nil in case we failed or entity doesn't have a model.
__INVALIDBONE__ in case the name cannot be read or the index is out of range.
 */
    public GetBoneName(index: number): string;

    /**
     * Returns parent bone of given bone.
     *
     * @arg number bone - The bode ID of the bone to get parent of, starting at index 0.
     * @return {number} Parent bone ID or -1 if we failed for some reason.
     */
    public GetBoneParent(bone: number): number;

    /**
     * Returns the position and angle of the given attachment, relative to the world.
     *
     * @arg number boneIndex - The bone index of the bone to get the position of, starting at index 0. See Entity:LookupBone.
     * @return {Vector} The bone's position relative to the world. It can return nothing if the requested bone is out of bounds, or the entity has no model.
     * @return {Angle} The bone's angle relative to the world.
     */
    public GetBonePosition(boneIndex: number): LuaMultiReturn<[Vector, Angle]>;

    /**
     * Returns the surface property of the specified bone.
     *
     * @arg number bone - The bone id, starting at index 0. See Entity:LookupBone.
     * @return {string} The surface property of the bone to be used with util.GetSurfaceIndex or an empty string on failure.
     */
    public GetBoneSurfaceProp(bone: number): string;

    /**
 * Returns info about given plane of non-nodraw brush model surfaces of the entity's model. Works on worldspawn as well.
* 
* @arg number id - The index of the plane to get info of. Starts from 0.
* @return {Vector} The origin of the plane.
This will be either the first vertex's position (if available) or the plane's normal multiplied by the plane's distance.
* @return {Vector} The normal of the plane.
* @return {number} The "distance" of the plane.
The distance is the dot product of the plane's normal and the point it was initialized with.
 */
    public GetBrushPlane(id: number): LuaMultiReturn<[Vector, Vector, number]>;

    /**
     * Returns the amount of planes of non-nodraw brush model surfaces of the entity's model.
     *
     * @return {number} The amount of brush model planes of the entity's model. This will be 0 for any non-brush model.
     */
    public GetBrushPlaneCount(): number;

    /**
     * Returns a table of brushes surfaces for brush model entities.
     *
     * @return {SurfaceInfo} Table of SurfaceInfos if the entity has a brush model, or no value otherwise.
     */
    public GetBrushSurfaces(): SurfaceInfo;

    /**
     * Returns the specified hook callbacks for this entity added with Entity:AddCallback
     *
     * @arg string hook - The hook to retrieve the callbacks from, see Entity Callbacks for the possible hooks.
     * @return {object} A table containing the callbackid and function of all the callbacks for the specified hook
     */
    public GetCallbacks(hook: string): object;

    /**
     * Returns ids of child bones of given bone.
     *
     * @arg number boneid - Bone id to lookup children of
     * @return {object} A table of bone ids
     */
    public GetChildBones(boneid: number): object;

    /**
     * Gets the children of the entity - that is, every entity whose move parent is this entity.
     *
     * @return {object} A list of movement children entities
     */
    public GetChildren(): object;

    /**
     * Returns the classname of a entity. This is often the name of the Lua file or folder containing the files for the entity
     *
     * @return {string} The entity's classname
     */
    public GetClass(): string;

    /**
     * Returns an entity's collision bounding box. In most cases, this will return the same bounding box as Entity:GetModelBounds unless the entity does not have a physics mesh or it has a PhysObj different from the default.
     *
     * @return {Vector} The minimum vector of the collision bounds
     * @return {Vector} The maximum vector of the collision bounds
     */
    public GetCollisionBounds(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Returns the entity's collision group
     *
     * @return {number} The collision group. See Enums/COLLISION_GROUP
     */
    public GetCollisionGroup(): number;

    /**
     * Returns the color the entity is set to.
     *
     * @return {Color} The color of the entity as a Color.
     */
    public GetColor(): Color;

    /**
     * Returns the two entities involved in a constraint ent, or nil if the entity is not a constraint.
     *
     * @return {Entity} ent1
     * @return {Entity} ent2
     */
    public GetConstrainedEntities(): LuaMultiReturn<[Entity, Entity]>;

    /**
     * Returns the two entities physobjects involved in a constraint ent, or no value if the entity is not a constraint.
     *
     * @return {PhysObj} phys1
     * @return {PhysObj} phys2
     */
    public GetConstrainedPhysObjects(): LuaMultiReturn<[PhysObj, PhysObj]>;

    /**
     * Returns entity's creation ID. Unlike Entity:EntIndex or  Entity:MapCreationID, it will always increase and old values won't be reused.
     *
     * @return {number} The creation ID
     */
    public GetCreationID(): number;

    /**
     * Returns the time the entity was created on, relative to CurTime.
     *
     * @return {number} The time the entity was created on.
     */
    public GetCreationTime(): number;

    /**
     * Gets the creator of the SENT.
     *
     * @return {Player} The creator, NULL for no creator.
     */
    public GetCreator(): Player;

    /**
     * Returns whether this entity uses custom collision check set by Entity:SetCustomCollisionCheck.
     *
     * @return {boolean} Whether this entity uses custom collision check or not
     */
    public GetCustomCollisionCheck(): boolean;

    /**
     * Returns the frame of the currently played sequence. This will be a number between 0 and 1 as a representation of sequence progress.
     *
     * @return {number} The frame of the currently played sequence
     */
    public GetCycle(): number;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {Angle} Requested angle.
 */
    public GetDTAngle(key: number): Angle;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {boolean} Requested boolean.
 */
    public GetDTBool(key: number): boolean;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {Entity} Requested entity.
 */
    public GetDTEntity(key: number): Entity;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {number} Requested float.
 */
    public GetDTFloat(key: number): number;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {number} 32-bit signed integer
 */
    public GetDTInt(key: number): number;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 3.
Specifies what key to grab from datatable.
* @return {string} Requested string.
 */
    public GetDTString(key: number): string;

    /**
 * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
* 
* @arg number key - Goes from 0 to 31.
Specifies what key to grab from datatable.
* @return {Vector} Requested vector.
 */
    public GetDTVector(key: number): Vector;

    /**
     * Returns internal data about editable Entity:NetworkVars.
     *
     * @return {object} The internal data
     */
    public GetEditingData(): object;

    /**
     * Returns a bit flag of all engine effect flags of the entity.
     *
     * @return {number} Engine effect flags, see Enums/EF
     */
    public GetEffects(): number;

    /**
     * Returns a bit flag of all engine flags of the entity.
     *
     * @return {number} Engine flags, see Enums/EFL
     */
    public GetEFlags(): number;

    /**
     * Returns the elasticity of this entity, used by some flying entities such as the Helicopter NPC to determine how much it should bounce around when colliding.
     *
     * @return {number} elasticity
     */
    public GetElasticity(): number;

    /**
     * Returns all flags of given entity.
     *
     * @return {number} Flags of given entity as a bitflag, see Enums/FL
     */
    public GetFlags(): number;

    /**
     * Returns acceptable value range for the flex.
     *
     * @arg number flex - The ID of the flex to look up bounds of
     * @return {number} The minimum value for this flex
     * @return {number} The maximum value for this flex
     */
    public GetFlexBounds(flex: number): LuaMultiReturn<[number, number]>;

    /**
 * Returns the ID of the flex based on given name.
* 
* @arg string name - The name of the flex to get the ID of. Case sensitive.
* @return {number} The ID of flex

nil if no flex with given name was found
 */
    public GetFlexIDByName(name: string): number;

    /**
     * Returns flex name.
     *
     * @arg number id - The flex id to look up name of
     * @return {string} The flex name
     */
    public GetFlexName(id: number): string;

    /**
     * Returns the number of flexes this entity has.
     *
     * @return {number} The number of flexes.
     */
    public GetFlexNum(): number;

    /**
     * Returns the flex scale of the entity.
     *
     * @return {number} The flex scale
     */
    public GetFlexScale(): number;

    /**
     * Returns current weight ( value ) of the flex.
     *
     * @arg number flex - The ID of the flex to get weight of
     * @return {number} The current weight of the flex
     */
    public GetFlexWeight(flex: number): number;

    /**
     * Returns the forward vector of the entity, as a normalized direction vector
     *
     * @return {Vector} forwardDir
     */
    public GetForward(): Vector;

    /**
     * Returns how much friction an entity has. Entities default to 1 (100%) and can be higher or even negative.
     *
     * @return {number} friction
     */
    public GetFriction(): number;

    /**
     * Gets the gravity multiplier of the entity.
     *
     * @return {number} gravityMultiplier
     */
    public GetGravity(): number;

    /**
     * Returns the object the entity is standing on.
     *
     * @return {Entity} The ground entity.
     */
    public GetGroundEntity(): Entity;

    /**
     * Returns the entity's ground speed velocity, which is based on the entity's walk/run speed and/or the ground speed of their sequence ( Entity:GetSequenceGroundSpeed ). Will return an empty Vector if the entity isn't moving on the ground.
     *
     * @return {Vector} The ground speed velocity.
     */
    public GetGroundSpeedVelocity(): Vector;

    /**
 * Gets the bone the hit box is attached to.
* 
* @arg number hitbox - The number of the hit box.
* @arg number hboxset - The number of the hit box set. This should be 0 in most cases.
Numbering for these sets start from 0. The total amount of sets can be found with Entity:GetHitBoxSetCount.
* @return {number} The number of the bone. Will be nil if the hit box index was out of range.
 */
    public GetHitBoxBone(hitbox: number, hboxset: number): number;

    /**
     * Gets the bounds (min and max corners) of a hit box.
     *
     * @arg number hitbox - The number of the hit box.
     * @arg number group - The group of the hit box. This should be 0 in most cases.
     * @return {Vector} Hit box mins. Will be nil if the hit box index was out of range.
     * @return {Vector} Hit box maxs. Will be nil if the hit box index was out of range.
     */
    public GetHitBoxBounds(hitbox: number, group: number): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Gets how many hit boxes are in a given hit box group.
     *
     * @arg number group - The number of the hit box group.
     * @return {number} The number of hit boxes.
     */
    public GetHitBoxCount(group: number): number;

    /**
     * Returns the number of hit box sets that an entity has. Functionally identical to Entity:GetHitboxSetCount
     *
     * @return {number} number of hit box sets
     */
    public GetHitBoxGroupCount(): number;

    /**
 * Gets the hit group of a given hitbox in a given hitbox set.
* 
* @arg number hitbox - The number of the hit box.
* @arg number hitboxset - The number of the hit box set. This should be 0 in most cases.
Numbering for these sets start from 0. The total group count can be found with Entity:GetHitBoxSetCount.
* @return {number} The hitbox group of given hitbox. See Enums/HITGROUP
 */
    public GetHitBoxHitGroup(hitbox: number, hitboxset: number): number;

    /**
     * Returns entity's current hit box set
     *
     * @return {number} The current hit box set id, or no value if the entity doesn't have hit boxes
     * @return {string} The current hit box set name, or no value if the entity doesn't have hit boxes
     */
    public GetHitboxSet(): LuaMultiReturn<[number, string]>;

    /**
     * Returns the amount of hitbox sets in the entity.
     *
     * @return {number} The amount of hitbox sets in the entity.
     */
    public GetHitboxSetCount(): number;

    /**
     * An interface for accessing internal key values on entities.
     *
     * @arg string variableName - Name of variable corresponding to an entity save value.
     * @return {any} The internal variable value.
     */
    public GetInternalVariable(variableName: string): any;

    /**
     * Returns a table containing all key values the entity has.
     *
     * @return {object} A table of key values.
     */
    public GetKeyValues(): object;

    /**
     * Returns the animation cycle/frame for given layer.
     *
     * @arg number layerID - The Layer ID
     * @return {number} The animation cycle/frame for given layer.
     */
    public GetLayerCycle(layerID: number): number;

    /**
     * Returns the duration of given layer.
     *
     * @arg number layerID - The Layer ID
     * @return {number} The duration of the layer
     */
    public GetLayerDuration(layerID: number): number;

    /**
     * Returns the layer playback rate. See also Entity:GetLayerDuration.
     *
     * @arg number layerID - The Layer ID
     * @return {number} The current playback rate.
     */
    public GetLayerPlaybackRate(layerID: number): number;

    /**
     * Returns the sequence id of given layer.
     *
     * @arg number layerID - The Layer ID.
     * @return {number} The sequenceID of the layer.
     */
    public GetLayerSequence(layerID: number): number;

    /**
     * Returns the current weight of the layer. See Entity:SetLayerWeight for more information.
     *
     * @arg number layerID - The Layer ID
     * @return {number} The current weight of the layer
     */
    public GetLayerWeight(layerID: number): number;

    /**
     * Returns the entity that is being used as the light origin position for this entity.
     *
     * @return {Entity} The lighting entity. This will usually be NULL.
     */
    public GetLightingOriginEntity(): Entity;

    /**
     * Returns the rotation of the entity relative to its parent entity.
     *
     * @return {Angle} Relative angle
     */
    public GetLocalAngles(): Angle;

    /**
     * Returns the non-VPhysics angular velocity of the entity relative to its parent entity.
     *
     * @return {Angle} The velocity
     */
    public GetLocalAngularVelocity(): Angle;

    /**
     * Returns entity's position relative to it's parent.
     *
     * @return {Vector} Relative position
     */
    public GetLocalPos(): Vector;

    /**
     * Gets the entity's angle manipulation of the given bone. This is relative to the default angle, so the angle is zero when unmodified.
     *
     * @arg number boneID - The bone's ID
     * @return {Angle} The entity's angle manipulation of the given bone.
     */
    public GetManipulateBoneAngles(boneID: number): Angle;

    /**
     * Returns the jiggle amount of the entity's bone.
     *
     * @arg number boneID - The bone ID
     * @return {number} Returns a value ranging from 0 to 255 depending on the value set with Entity:ManipulateBoneJiggle.
     */
    public GetManipulateBoneJiggle(boneID: number): number;

    /**
     * Gets the entity's position manipulation of the given bone. This is relative to the default position, so it is zero when unmodified.
     *
     * @arg number boneId - The bone's ID
     * @return {Vector} The entity's position manipulation of the given bone.
     */
    public GetManipulateBonePosition(boneId: number): Vector;

    /**
     * Gets the entity's scale manipulation of the given bone. Normal scale is Vector( 1, 1, 1 )
     *
     * @arg number boneID - The bone's ID
     * @return {Vector} The entity's scale manipulation of the given bone
     */
    public GetManipulateBoneScale(boneID: number): Vector;

    /**
     * Returns the material override for this entity.
     *
     * @return {string} material
     */
    public GetMaterial(): string;

    /**
 * Returns all materials of the entity's model.
* 
* @return {object} A table containing full paths to the materials of the model.
For models, it's limited to 128 materials.
 */
    public GetMaterials(): object;

    /**
     * Returns the surface material of this entity.
     *
     * @return {number} Surface material. See Enums/MAT
     */
    public GetMaterialType(): number;

    /**
     * Returns the max health that the entity was given. It can be set via Entity:SetMaxHealth.
     *
     * @return {number} Max health.
     */
    public GetMaxHealth(): number;

    /**
 * Gets the model of given entity.
* 
* @return {string} The entity's model. Will be a filesystem path for most models.
This will be nil for entities which cannot have models, such as point entities.
 */
    public GetModel(): string;

    /**
     * Returns the entity's model bounds. This is different than the collision bounds/hull. This is not scaled with Entity:SetModelScale, and will return the model's original, unmodified mins and maxs. This can be used to get world bounds.
     *
     * @return {Vector} The minimum vector of the bounds
     * @return {Vector} The maximum vector of the bounds
     */
    public GetModelBounds(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Returns the contents of the entity's current model.
     *
     * @return {number} The contents of the entity's model. See Enums/CONTENTS.
     */
    public GetModelContents(): number;

    /**
     * Gets the physics bone count of the entity's model. This is only applicable to anim type Scripted Entities with ragdoll models.
     *
     * @return {number} How many physics bones exist on the model.
     */
    public GetModelPhysBoneCount(): number;

    /**
     * Gets the models radius.
     *
     * @return {number} The radius of the model
     */
    public GetModelRadius(): number;

    /**
     * Returns the entity's model render bounds. By default this will return the same bounds as Entity:GetModelBounds.
     *
     * @return {Vector} The minimum vector of the bounds
     * @return {Vector} The maximum vector of the bounds
     */
    public GetModelRenderBounds(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Gets the selected entity's model scale.
     *
     * @return {number} Scale of that entity's model.
     */
    public GetModelScale(): number;

    /**
     * Returns the amount a momentary_rot_button entity is turned based on the given angle. 0 meaning completely turned closed, 1 meaning completely turned open.
     *
     * @arg Angle turnAngle - The angle of rotation to compare - usually should be Entity:GetAngles.
     * @return {number} The amount the momentary_rot_button is turned, ranging from 0 to 1, or nil if the entity is not a momentary_rot_button.
     */
    public GetMomentaryRotButtonPos(turnAngle: Angle): number;

    /**
     * Returns the move collide type of the entity. The move collide is the way a physics object reacts to hitting an object - will it bounce, slide?
     *
     * @return {number} The move collide type, see Enums/MOVECOLLIDE
     */
    public GetMoveCollide(): number;

    /**
     * Returns the movement parent of this entity.
     *
     * @return {Entity} The movement parent of this entity.
     */
    public GetMoveParent(): Entity;

    /**
     * Returns the entity's movetype
     *
     * @return {number} Move type. See Enums/MOVETYPE
     */
    public GetMoveType(): number;

    /**
     * Returns the mapping name of this entity.
     *
     * @return {string} The name of the Entity
     */
    public GetName(): string;

    /**
     * Gets networked angles for entity.
     *
     * @return {Angle} angle
     */
    public GetNetworkAngles(): Angle;

    /**
     * Retrieves a networked angle value at specified index on the entity that is set by Entity:SetNetworkedAngle.
     *
     * @arg string key - The key that is associated with the value
     * @arg Angle [fallback=Angle( 0, 0, 0 )] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {Angle} The retrieved value
     */
    public GetNetworkedAngle(key: string, fallback?: Angle): Angle;

    /**
     * Retrieves a networked boolean value at specified index on the entity that is set by Entity:SetNetworkedBool.
     *
     * @arg string key - The key that is associated with the value
     * @arg boolean [fallback=false] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {boolean} The retrieved value
     */
    public GetNetworkedBool(key: string, fallback?: boolean): boolean;

    /**
     * Retrieves a networked float value at specified index on the entity that is set by Entity:SetNetworkedEntity.
     *
     * @arg string key - The key that is associated with the value
     * @arg Entity [fallback=NULL] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {Entity} The retrieved value
     */
    public GetNetworkedEntity(key: string, fallback?: Entity): Entity;

    /**
     * Retrieves a networked float value at specified index on the entity that is set by Entity:SetNetworkedFloat.
     *
     * @arg string key - The key that is associated with the value
     * @arg number [fallback=0] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {number} The retrieved value
     */
    public GetNetworkedFloat(key: string, fallback?: number): number;

    /**
     * Retrieves a networked integer value at specified index on the entity that is set by Entity:SetNetworkedInt.
     *
     * @arg string key - The key that is associated with the value
     * @arg number [fallback=0] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {number} The retrieved value
     */
    public GetNetworkedInt(key: string, fallback?: number): number;

    /**
     * Retrieves a networked string value at specified index on the entity that is set by Entity:SetNetworkedString.
     *
     * @arg string key - The key that is associated with the value
     * @arg string fallback - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {string} The retrieved value
     */
    public GetNetworkedString(key: string, fallback: string): string;

    /**
     * Returns callback function for given NWVar of this entity.
     *
     * @arg string name - The name of the NWVar to get callback of.
     * @return {Function} The callback of given NWVar, if any.
     */
    public GetNetworkedVarProxy(name: string): Function;

    /**
     * Returns all the networked variables in an entity.
     *
     * @return {object} Key-Value table of all networked variables.
     */
    public GetNetworkedVarTable(): object;

    /**
     * Retrieves a networked vector value at specified index on the entity that is set by Entity:SetNetworkedVector.
     *
     * @arg string key - The key that is associated with the value
     * @arg Vector [fallback=Vector( 0, 0, 0 )] - The value to return if we failed to retrieve the value. ( If it isn't set )
     * @return {Vector} The retrieved value
     */
    public GetNetworkedVector(key: string, fallback?: Vector): Vector;

    /**
     * Gets networked origin for entity.
     *
     * @return {Vector} origin
     */
    public GetNetworkOrigin(): Vector;

    /**
     * Returns all network vars created by Entity:NetworkVar and Entity:NetworkVarElement and their current values.
     *
     * @return {object} The Key-Value formatted table of network var names and their current values
     */
    public GetNetworkVars(): object;

    /**
     * Returns if the entity's rendering and transmitting has been disabled.
     *
     * @return {boolean} Whether the entity's rendering and transmitting has been disabled.
     */
    public GetNoDraw(): boolean;

    /**
     * Returns the body group count of the entity.
     *
     * @return {number} Amount of bodygroups the entitys model has
     */
    public GetNumBodyGroups(): number;

    /**
     * Returns the number of pose parameters this entity has.
     *
     * @return {number} Amount of pose parameters the entity has
     */
    public GetNumPoseParameters(): number;

    /**
     * Retrieves a networked angle value at specified index on the entity that is set by Entity:SetNWAngle.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=Angle( 0, 0, 0 )] - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWAngle(key: string, fallback?: any): any;

    /**
     * Retrieves a networked boolean value at specified index on the entity that is set by Entity:SetNWBool.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=false] - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWBool(key: string, fallback?: any): any;

    /**
     * Retrieves a networked entity value at specified index on the entity that is set by Entity:SetNWEntity.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=NULL] - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWEntity(key: string, fallback?: any): any;

    /**
     * Retrieves a networked float value at specified index on the entity that is set by Entity:SetNWFloat.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=0] - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWFloat(key: string, fallback?: any): any;

    /**
     * Retrieves a networked integer (whole number) value that was previously set by Entity:SetNWInt.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=0] - The value to return if we failed to retrieve the value (If it isn't set).
     * @return {any} The value associated with the key
     */
    public GetNWInt(key: string, fallback?: any): any;

    /**
     * Retrieves a networked string value at specified index on the entity that is set by Entity:SetNWString.
     *
     * @arg string key - The key that is associated with the value
     * @arg any fallback - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWString(key: string, fallback: any): any;

    /**
     * Returns callback function for given NWVar of this entity.
     *
     * @arg any key - The key of the NWVar to get callback of.
     * @return {Function} The callback of given NWVar, or nil if not found.
     */
    public GetNWVarProxy(key: any): Function;

    /**
     * Returns all the networked variables in an entity.
     *
     * @return {object} Key-Value table of all networked variables.
     */
    public GetNWVarTable(): object;

    /**
     * Retrieves a networked vector value at specified index on the entity that is set by Entity:SetNWVector.
     *
     * @arg string key - The key that is associated with the value
     * @arg any [fallback=Vector( 0, 0, 0 )] - The value to return if we failed to retrieve the value. (If it isn't set)
     * @return {any} The value associated with the key
     */
    public GetNWVector(key: string, fallback?: any): any;

    /**
     * Returns the owner entity of this entity. See Entity:SetOwner for more info.
     *
     * @return {Entity} The owner entity of this entity.
     */
    public GetOwner(): Entity;

    /**
     * Returns the parent entity of this entity.
     *
     * @return {Entity} parentEntity
     */
    public GetParent(): Entity;

    /**
     * Returns the attachment index of the entity's parent. Returns 0 if the entity is not parented to a specific attachment or if it isn't parented at all.
     *
     * @return {number} The parented attachment index
     */
    public GetParentAttachment(): number;

    /**
     * If the entity is parented to an entity that has a model with multiple physics objects (like a ragdoll), this is used to retrieve what physics object number the entity is parented to on it's parent.
     *
     * @return {number} The physics object id, or nil if the entity has no parent
     */
    public GetParentPhysNum(): number;

    /**
 * Returns the position and angle of the entity's move parent as a 3x4 matrix (VMatrix is 4x4 so the fourth row goes unused). The first three columns store the angle as a rotation matrix, and the fourth column stores the position vector.
* 
* @return {VMatrix} The position and angle matrix.
If the entity has no move parent, an identity matrix will be returned.
If the entity is parented to attachment 0 or the parent isn't a BaseAnimating entity, the equivalent of Entity:GetMoveParent():GetWorldTransformMatrix() will be returned.
 */
    public GetParentWorldTransformMatrix(): VMatrix;

    /**
     * Returns whether the entity is persistent or not.
     *
     * @return {boolean} True if the entity is set to be persistent.
     */
    public GetPersistent(): boolean;

    /**
 * Returns player who is claiming kills of physics damage the entity deals.
* 
* @arg number [timeLimit=1] - The time to check if the entity was still a proper physics attacker.
Some entities such as the Combine Ball disregard the time limit and always return the physics attacker.
* @return {Player} The player. If entity that was set is not a player, it will return NULL entity.
 */
    public GetPhysicsAttacker(timeLimit?: number): Player;

    /**
     * Returns the entity's physics object, if the entity has physics.
     *
     * @return {PhysObj} The entity's physics object.
     */
    public GetPhysicsObject(): PhysObj;

    /**
     * Returns the number of physics objects an entity has (usually 1 for non-ragdolls)
     *
     * @return {number} numObjects
     */
    public GetPhysicsObjectCount(): number;

    /**
     * Returns a specific physics object from an entity with multiple PhysObjects (like ragdolls)
     *
     * @arg number physNum - The number corresponding to the PhysObj to grab. Starts at 0.
     * @return {PhysObj} The physics object
     */
    public GetPhysicsObjectNum(physNum: number): PhysObj;

    /**
     * Returns the playback rate of the main sequence on this entity, with 1.0 being the default speed.
     *
     * @return {number} The playback rate.
     */
    public GetPlaybackRate(): number;

    /**
     * Gets the position of entity in world.
     *
     * @return {Vector} The position of the entity.
     */
    public GetPos(): Vector;

    /**
 * Returns the pose parameter value
* 
* @arg string name - Pose parameter name to look up
* @return {number} Value of given pose parameter.
This value will be from 0 - 1 on the client and from minimum range to maximum range on the server! You'll have to remap this value clientside to Entity:GetPoseParameterRange's returns if you want get the actual pose parameter value. See Entity:SetPoseParameter's example.
 */
    public GetPoseParameter(name: string): number;

    /**
     * Returns name of given pose parameter
     *
     * @arg number id - Id of the pose paremeter
     * @return {string} Name of given pose parameter
     */
    public GetPoseParameterName(id: number): string;

    /**
     * Returns pose parameter range
     *
     * @arg number id - Pose parameter ID to look up
     * @return {number} The minimum value
     * @return {number} The maximum value
     */
    public GetPoseParameterRange(id: number): LuaMultiReturn<[number, number]>;

    /**
     * Returns whether this entity is predictable or not.
     *
     * @return {boolean} Whether this entity is predictable or not.
     */
    public GetPredictable(): boolean;

    /**
     * Called to override the preferred carry angles of this object.
     *
     * @arg Player ply - The player who is holding the object.
     * @return {Angle} Return an angle to override the carry angles.
     */
    public GetPreferredCarryAngles(ply: Player): Angle;

    /**
     * Returns the entity which the ragdoll came from. The opposite of Player:GetRagdollEntity.
     *
     * @return {Entity} The entity who owns the ragdoll.
     */
    public GetRagdollOwner(): Entity;

    /**
     * Returns the entity's render angles, set by Entity:SetRenderAngles in a drawing hook.
     *
     * @return {Angle} The entitys render angles
     */
    public GetRenderAngles(): Angle;

    /**
     * Returns render bounds of the entity. Can be overridden by Entity:SetRenderBounds.
     *
     * @return {Vector} The minimum vector of the bounds
     * @return {Vector} The maximum vector of the bounds.
     */
    public GetRenderBounds(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Returns current render FX of the entity.
     *
     * @return {number} The current render FX of the entity. See Enums/kRenderFx
     */
    public GetRenderFX(): number;

    /**
     * Returns the render group of the entity.
     *
     * @return {number} The render group. See Enums/RENDERGROUP
     */
    public GetRenderGroup(): number;

    /**
     * Returns the render mode of the entity.
     *
     * @return {number} The render Mode. See Enums/RENDERMODE
     */
    public GetRenderMode(): number;

    /**
     * Returns the entity's render origin, set by Entity:SetRenderOrigin in a drawing hook.
     *
     * @return {Vector} The entitys render origin
     */
    public GetRenderOrigin(): Vector;

    /**
     * Returns the rightward vector of the entity, as a normalized direction vector
     *
     * @return {Vector} rightDir
     */
    public GetRight(): Vector;

    /**
     * Returns the min and max of the entity's axis-aligned bounding box.
     *
     * @arg Vector min - Minimum extent of the bounding box.
     * @arg Vector max - Maximum extent of the bounding box.
     * @return {Vector} Minimum extent of the AABB
     * @return {Vector} Maximum extent of the AABB
     */
    public GetRotatedAABB(min: Vector, max: Vector): LuaMultiReturn<[Vector, Vector]>;

    /**
 * Returns a table of save values for an entity.
* 
* @arg boolean showAll - If set, shows all variables, not just the ones for save.
* @return {object} A table containing all save values in key/value format.
The value may be a sequential table (starting to 1) if the field in question is an array in engine.
 */
    public GetSaveTable(showAll: boolean): object;

    /**
     * Return the index of the model sequence that is currently active for the entity.
     *
     * @return {number} The index of the model sequence.
     */
    public GetSequence(): number;

    /**
     * Return activity id out of sequence id. Opposite of Entity:SelectWeightedSequence.
     *
     * @arg number seq - The sequence ID
     * @return {number} The activity ID, ie Enums/ACT
     */
    public GetSequenceActivity(seq: number): number;

    /**
     * Returns the activity name for the given sequence id.
     *
     * @arg number sequenceId - The sequence id.
     * @return {string} The Enums/ACT as a string, returns "Not Found!" with an invalid sequence and "No model!" when no model is set.
     */
    public GetSequenceActivityName(sequenceId: number): string;

    /**
     * Returns the amount of sequences ( animations ) the entity's model has.
     *
     * @return {number} The amount of sequences ( animations ) the entity's model has.
     */
    public GetSequenceCount(): number;

    /**
     * Returns the ground speed of the entity's sequence.
     *
     * @arg number sequenceId - The sequence ID.
     * @return {number} The ground speed of this sequence.
     */
    public GetSequenceGroundSpeed(sequenceId: number): number;

    /**
     * Returns a table of information about an entity's sequence.
     *
     * @arg number sequenceId - The sequence id of the entity.
     * @return {SequenceInfoStruct} Table of information about the entity's sequence, or nil is ID is out of range. See Structures/SequenceInfo
     */
    public GetSequenceInfo(sequenceId: number): SequenceInfoStruct;

    /**
     * Returns a list of all sequences ( animations ) the model has.
     *
     * @return {object} The list of all sequences ( animations ) the model has. The indices start with 0!
     */
    public GetSequenceList(): object;

    /**
     * Returns an entity's sequence move distance (the change in position over the course of the entire sequence).
     *
     * @arg number sequenceId - The sequence index.
     * @return {number} The move distance of the sequence.
     */
    public GetSequenceMoveDist(sequenceId: number): number;

    /**
     * Returns the delta movement and angles of a sequence of the entity's model.
     *
     * @arg number sequenceId - The sequence index. See Entity:GetSequenceName.
     * @arg number [startCycle=0] - The sequence start cycle. 0 is the start of the animation, 1 is the end.
     * @arg number [endCyclnde=1] - The sequence end cycle. 0 is the start of the animation, 1 is the end. Values like 2, etc are allowed.
     * @return {boolean} Whether the operation was successful
     * @return {Vector} The delta vector of the animation, how much the model's origin point moved.
     * @return {Angle} The delta angle of the animation.
     */
    public GetSequenceMovement(
        sequenceId: number,
        startCycle?: number,
        endCyclnde?: number
    ): LuaMultiReturn<[boolean, Vector, Angle]>;

    /**
     * Returns the change in heading direction in between the start and the end of the sequence.
     *
     * @arg number seq - The sequence index. See Entity:LookupSequence.
     * @return {number} The yaw delta. Returns 99999 for no movement.
     */
    public GetSequenceMoveYaw(seq: number): number;

    /**
     * Return the name of the sequence for the index provided.
     * Refer to Entity:GetSequence to find the current active sequence on this entity.
     *
     * @arg number index - The index of the sequence to look up.
     * @return {string} Name of the sequence.
     */
    public GetSequenceName(index: number): string;

    /**
     * Checks if the entity plays a sound when picked up by a player.
     *
     * @return {boolean} True if it plays the pickup sound, false otherwise.
     */
    public GetShouldPlayPickupSound(): boolean;

    /**
     * Returns if entity should create a server ragdoll on death or a client one.
     *
     * @return {boolean} Returns true if ragdoll will be created on server, false if on client
     */
    public GetShouldServerRagdoll(): boolean;

    /**
     * Returns the skin index of the current skin.
     *
     * @return {number} skinIndex
     */
    public GetSkin(): number;

    /**
     * Returns solid type of an entity.
     *
     * @return {number} The solid type. See the Enums/SOLID.
     */
    public GetSolid(): number;

    /**
     * Returns solid flag(s) of an entity.
     *
     * @return {number} The flag(s) of the entity, see Enums/FSOLID.
     */
    public GetSolidFlags(): number;

    /**
     * Returns if we should show a spawn effect on spawn on this entity.
     *
     * @return {boolean} The flag to allow or disallow the spawn effect.
     */
    public GetSpawnEffect(): boolean;

    /**
     * Returns the bitwise spawn flags used by the entity.
     *
     * @return {number} The spawn flags of the entity, see SF_Enums.
     */
    public GetSpawnFlags(): number;

    /**
     * Returns the material override for the given index.
     *
     * @arg number index - The index of the sub material. Acceptable values are from 0 to 31.
     * @return {string} The material that overrides this index, if any.
     */
    public GetSubMaterial(index: number): string;

    /**
     * Returns a list of models included into the entity's model in the .qc file.
     *
     * @return {object} The list of models included into the entity's model in the .qc file.
     */
    public GetSubModels(): object;

    /**
     * Returns the table that contains all values saved within the entity.
     *
     * @return {object} entTable
     */
    public GetTable(): object;

    /**
     * Returns the last trace used in the collision callbacks such as ENTITY:StartTouch, ENTITY:Touch and ENTITY:EndTouch.
     *
     * @return {TraceResultStruct} The Structures/TraceResult
     */
    public GetTouchTrace(): TraceResultStruct;

    /**
     * Returns true if the TransmitWithParent flag is set or not.
     *
     * @return {boolean} Is the TransmitWithParent flag is set or not
     */
    public GetTransmitWithParent(): boolean;

    /**
     * Returns if the entity is unfreezable, meaning it can't be frozen with the physgun. By default props are freezable, so this function will typically return false.
     *
     * @return {boolean} True if the entity is unfreezable, false otherwise.
     */
    public GetUnFreezable(): boolean;

    /**
     * Returns the upward vector of the entity, as a normalized direction vector
     *
     * @return {Vector} upDir
     */
    public GetUp(): Vector;

    /**
     * Retrieves a value from entity's Entity:GetTable. Set by Entity:SetVar.
     *
     * @arg any key - Key of the value to retrieve
     * @arg any [def=nil] - A default value to fallback to if we couldn't retrieve the value from entity
     * @return {any} Retrieved value
     */
    public GetVar(key: any, def?: any): any;

    /**
     * Returns the entity's velocity.
     *
     * @return {Vector} The velocity of the entity.
     */
    public GetVelocity(): Vector;

    /**
     * Returns ID of workshop addon that the entity is from.
     *
     * @return {number} The workshop ID
     */
    public GetWorkshopID(): number;

    /**
     * Returns the position and angle of the entity as a 3x4 matrix (VMatrix is 4x4 so the fourth row goes unused). The first three columns store the angle as a rotation matrix, and the fourth column stores the position vector.
     *
     * @return {VMatrix} The position and angle matrix.
     */
    public GetWorldTransformMatrix(): VMatrix;

    /**
     * Causes the entity to break into its current models gibs, if it has any.
     *
     * @arg Vector force - The force to apply to the created gibs.
     * @arg object [clr=nil] - If set, this will be color of the broken gibs instead of the entity's color.
     */
    public GibBreakClient(force: Vector, clr?: object): void;

    /**
     * Causes the entity to break into its current models gibs, if it has any.
     *
     * @arg Vector force - The force to apply to the created gibs
     */
    public GibBreakServer(force: Vector): void;

    /**
     * Returns whether or not the bone manipulation functions have ever been called on given  entity.
     *
     * @return {boolean} True if the entity has been bone manipulated, false otherwise.
     */
    public HasBoneManipulations(): boolean;

    /**
     * Returns whether or not the the entity has had flex manipulations performed with Entity:SetFlexWeight or Entity:SetFlexScale.
     *
     * @return {boolean} True if the entity has flex manipulations, false otherwise.
     */
    public HasFlexManipulatior(): boolean;

    /**
     * Returns whether this entity has the specified spawnflags bits set.
     *
     * @arg number spawnFlags - The spawnflag bits to check, see Enums/SF.
     * @return {boolean} Whether the entity has that spawnflag set or not.
     */
    public HasSpawnFlags(spawnFlags: number): boolean;

    /**
     * Returns the position of the head of this entity, NPCs use this internally to aim at their targets.
     *
     * @arg Vector origin - The vector of where the attack comes from.
     * @return {Vector} The head position.
     */
    public HeadTarget(origin: Vector): Vector;

    /**
     * Returns the health of the entity.
     *
     * @return {number} health
     */
    public Health(): number;

    /**
     * Sets the entity on fire.
     *
     * @arg number length - How long to keep the entity ignited, in seconds.
     * @arg number [radius=0] - The radius of the ignition, will ignite everything around the entity that is in this radius.
     */
    public Ignite(length: number, radius?: number): void;

    /**
     * Initializes this entity as being clientside only.
     */
    public InitializeAsClientEntity(): void;

    /**
     * Fires input to the entity with the ability to make another entity responsible, bypassing the event queue system.
     *
     * @arg string input - The name of the input to fire
     * @arg Entity [activator=nil] - The entity that caused this input (i.e. the player who pushed a button)
     * @arg Entity [caller=nil] - The entity that is triggering this input (i.e. the button that was pushed)
     * @arg any [param=nil] - The value to give to the input. Can be either a string, a number or a boolean.
     */
    public Input(input: string, activator?: Entity, caller?: Entity, param?: any): void;

    /**
     * Sets up Data Tables from entity to use with Entity:NetworkVar.
     */
    public InstallDataTable(): void;

    /**
     * Resets the entity's bone cache values in order to prepare for a model change.
     */
    public InvalidateBoneCache(): void;

    /**
     * Returns true if the entity has constraints attached to it
     *
     * @return {boolean} Whether the entity is constrained or not.
     */
    public IsConstrained(): boolean;

    /**
     * Returns if entity is constraint or not
     *
     * @return {boolean} Is the entity a constraint or not
     */
    public IsConstraint(): boolean;

    /**
     * Returns whether the entity is dormant or not. Client/server entities become dormant when they leave the PVS on the server. Client side entities can decide for themselves whether to become dormant. This mainly applies to PVS.
     *
     * @return {boolean} Whether the entity is dormant or not.
     */
    public IsDormant(): boolean;

    /**
     * Returns whether an entity has engine effect applied or not.
     *
     * @arg number effect - The effect to check for, see Enums/EF.
     * @return {boolean} Whether the entity has the engine effect applied or not.
     */
    public IsEffectActive(effect: number): boolean;

    /**
     * Checks if given flag is set or not.
     *
     * @arg number flag - The engine flag to test, see Enums/EFL
     * @return {boolean} Is set or not
     */
    public IsEFlagSet(flag: number): boolean;

    /**
     * Checks if given flag(s) is set or not.
     *
     * @arg number flag - The engine flag(s) to test, see Enums/FL
     * @return {boolean} Is set or not
     */
    public IsFlagSet(flag: number): boolean;

    /**
     * Returns whether the entity is inside a wall or outside of the map.
     *
     * @return {boolean} Is the entity in world
     */
    public IsInWorld(): boolean;

    /**
     * Returns whether the entity is lag compensated or not.
     *
     * @return {boolean} Whether the entity is lag compensated or not.
     */
    public IsLagCompensated(): boolean;

    /**
     * Returns true if the target is in line of sight.
     *
     * @arg Vector target - The target to test. You can also supply an Entity instead of a Vector
     * @return {boolean} Returns true if the line of sight is clear
     */
    public IsLineOfSightClear(target: Vector): boolean;

    /**
     * Returns if the entity is going to be deleted in the next frame.
     *
     * @return {boolean} If the entity is going to be deleted.
     */
    public IsMarkedForDeletion(): boolean;

    /**
     * Checks if the entity is a NextBot or not.
     *
     * @return {boolean} Whether the entity is an NextBot entity or not.
     */
    public IsNextBot(): boolean;

    /**
     * Checks if the entity is an NPC or not.
     *
     * @return {boolean} Whether the entity is an NPC.
     */
    public IsNPC(): boolean;

    /**
     * Returns whether the entity is on fire.
     *
     * @return {boolean} Whether the entity is on fire or not.
     */
    public IsOnFire(): boolean;

    /**
     * Returns whether the entity is on ground or not.
     *
     * @return {boolean} Whether the entity is on ground or not.
     */
    public IsOnGround(): boolean;

    /**
     * Checks if the entity is a player or not.
     *
     * @return {boolean} Whether the entity is a player.
     */
    public IsPlayer(): boolean;

    /**
     * Returns true if the entity is being held by a player. Either by physics gun, gravity gun or use-key (+use).
     *
     * @return {boolean} IsBeingHeld
     */
    public IsPlayerHolding(): boolean;

    /**
     * Returns whether there's a gesture is given activity being played.
     *
     * @arg number activity - The activity to test. See Enums/ACT.
     * @return {boolean} Whether there's a gesture is given activity being played.
     */
    public IsPlayingGesture(activity: number): boolean;

    /**
     * Checks if the entity is a ragdoll.
     *
     * @return {boolean} Is ragdoll or not
     */
    public IsRagdoll(): boolean;

    /**
     * Checks if the entity is a SENT or a built-in entity.
     *
     * @return {boolean} Returns true if entity is scripted ( SENT ), false if not ( A built-in engine entity )
     */
    public IsScripted(): boolean;

    /**
     * Returns whether the entity's current sequence is finished or not.
     *
     * @return {boolean} Whether the entity's sequence is finished or not.
     */
    public IsSequenceFinished(): boolean;

    /**
     * Returns if the entity is solid or not.
     * Very useful for determining if the entity is a trigger or not.
     *
     * @return {boolean} Whether the entity is solid or not.
     */
    public IsSolid(): boolean;

    /**
     * Returns whether the entity is a valid entity or not.
     *
     * @return {boolean} true if the entity is valid, false otherwise
     */
    public IsValid(): boolean;

    /**
     * Returns whether the given layer ID is valid and exists on this entity.
     *
     * @arg number layerID - The Layer ID
     * @return {boolean} Whether the given layer ID is valid and exists on this entity.
     */
    public IsValidLayer(layerID: number): boolean;

    /**
     * Checks if the entity is a vehicle or not.
     *
     * @return {boolean} Whether the entity is a vehicle.
     */
    public IsVehicle(): boolean;

    /**
     * Checks if the entity is a weapon or not.
     *
     * @return {boolean} Whether the entity is a weapon
     */
    public IsWeapon(): boolean;

    /**
     * Returns whether the entity is a widget or not.
     *
     * @return {boolean} Whether the entity is a widget or not.
     */
    public IsWidget(): boolean;

    /**
     * Returns if the entity is the map's Entity[0] worldspawn
     *
     * @return {boolean} isWorld
     */
    public IsWorld(): boolean;

    /**
     * Converts a vector local to an entity into a worldspace vector
     *
     * @arg Vector lpos - The local vector
     * @return {Vector} The translated to world coordinates vector
     */
    public LocalToWorld(lpos: Vector): Vector;

    /**
     * Converts a local angle (local to the entity) to a world angle.
     *
     * @arg Angle ang - The local angle
     * @return {Angle} The world angle
     */
    public LocalToWorldAngles(ang: Angle): Angle;

    /**
     * Returns the attachment index of the given attachment name.
     *
     * @arg string attachmentName - The name of the attachment.
     * @return {number} The attachment index, or 0 if the attachment does not exist and -1 if the model is invalid.
     */
    public LookupAttachment(attachmentName: string): number;

    /**
 * Gets the bone index of the given bone name, returns nothing if the bone does not exist.
* 
* @arg string boneName - The name of the bone.
Common generic bones ( for player models and some HL2 models ):

ValveBiped.Bip01_Head1
ValveBiped.Bip01_Spine
ValveBiped.Anim_Attachment_RH

Common hand bones (left hand equivalents also available, replace R with L)

ValveBiped.Bip01_R_Hand
ValveBiped.Bip01_R_Forearm
ValveBiped.Bip01_R_Foot
ValveBiped.Bip01_R_Thigh
ValveBiped.Bip01_R_Calf
ValveBiped.Bip01_R_Shoulder
ValveBiped.Bip01_R_Elbow
* @return {number} Index of the given bone name
 */
    public LookupBone(boneName: string): number;

    /**
     * Returns pose parameter ID from its name.
     *
     * @arg string name - Pose parameter name
     * @return {number} The ID of the given pose parameter name, if it exists, -1 otherwise
     */
    public LookupPoseParameter(name: string): number;

    /**
 * Returns sequence ID from its name.
* 
* @arg string name - Sequence name
* @return {number} Sequence ID for that name. This will differ for models with same sequence names. Will be -1 whether the sequence is invalid.
* @return {number} The sequence duration

0 if the sequence is invalid
 */
    public LookupSequence(name: string): LuaMultiReturn<[number, number]>;

    /**
     * Turns the Entity:GetPhysicsObject into a physics shadow.
     * It's used internally for the Player's and NPC's physics object, and certain HL2 entities such as the crane.
     *
     * @arg boolean [allowPhysicsMovement=true] - Whether to allow the physics shadow to move under stress.
     * @arg boolean [allowPhysicsRotation=true] - Whether to allow the physics shadow to rotate under stress.
     */
    public MakePhysicsObjectAShadow(allowPhysicsMovement?: boolean, allowPhysicsRotation?: boolean): void;

    /**
 * Sets custom bone angles.
* 
* @arg number boneID - Index of the bone you want to manipulate
* @arg Angle ang - Angle to apply.
The angle is relative to the original bone angle, not relative to the world or the entity.
 */
    public ManipulateBoneAngles(boneID: number, ang: Angle): void;

    /**
 * Manipulates the bone's jiggle status. This allows non jiggly bones to become jiggly.
* 
* @arg number boneID - Index of the bone you want to manipulate.
* @arg number enabled - 0 = No Jiggle
1 = Jiggle
 */
    public ManipulateBoneJiggle(boneID: number, enabled: number): void;

    /**
 * Sets custom bone offsets.
* 
* @arg number boneID - Index of the bone you want to manipulate
* @arg Vector pos - Position vector to apply
Note that the position is relative to the original bone position, not relative to the world or the entity.
 */
    public ManipulateBonePosition(boneID: number, pos: Vector): void;

    /**
 * Sets custom bone scale.
* 
* @arg number boneID - Index of the bone you want to manipulate
* @arg Vector scale - Scale vector to apply. Note that the scale is relative to the original bone scale, not relative to the world or the entity.
The vector will be normalised if its longer than 32 units.Issue Tracker: 1249
 */
    public ManipulateBoneScale(boneID: number, scale: Vector): void;

    /**
     * Returns entity's map creation ID. Unlike Entity:EntIndex or Entity:GetCreationID, it will always be the same on same map, no matter how much you clean up or restart it.
     *
     * @return {number} The map creation ID or -1 if the entity is not compiled into the map.
     */
    public MapCreationID(): number;

    /**
     * Refreshes the shadow of the entity.
     */
    public MarkShadowAsDirty(): void;

    /**
     * Fires the muzzle flash effect of the weapon the entity is carrying. This only creates a light effect and is often called alongside Weapon:SendWeaponAnim
     */
    public MuzzleFlash(): void;

    /**
     * Performs a Ray-Orientated Bounding Box intersection from the given position to the origin of the OBBox with the entity and returns the hit position on the OBBox.
     *
     * @arg Vector position - The vector to start the intersection from.
     * @return {Vector} The nearest hit point of the entity's bounding box in world coordinates.
     */
    public NearestPoint(position: Vector): Vector;

    /**
 * Creates a network variable on the entity and adds Set/Get functions for it. This function should only be called in ENTITY:SetupDataTables.
* 
* @arg string type - Supported choices:

String
Bool
Float
Int (32-bit signed integer)
Vector
Angle
Entity
* @arg number slot - Each network variable has to have a unique slot. The slot is per type - so you can have an int in slot 0, a bool in slot 0 and a float in slot 0 etc. You can't have two ints in slot 0, instead you would do a int in slot 0 and another int in slot 1.
The max slots right now are 32 - so you should pick a number between 0 and 31. An exception to this is strings which has a max slots of 4.
* @arg string name - The name will affect how you access it. If you call it Foo you would add two new functions on your entity - SetFoo() and GetFoo(). So be careful that what you call it won't collide with any existing functions (don't call it Pos for example).
* @arg object [extended=nil] - A table of extended information.
KeyName

Allows the NetworkVar to be set using Entity:SetKeyValue. This is useful if you're making an entity that you want to be loaded in a map. The sky entity uses this.

Edit

The edit key lets you mark this variable as editable. See Editable Entities for more information.
 */
    public NetworkVar(type: string, slot: number, name: string, extended?: object): void;

    /**
 * Similarly to Entity:NetworkVar, creates a network variable on the entity and adds Set/Get functions for it. This method stores it's value as a member value of a vector or an angle. This allows to go beyond the normal variable limit of Entity:NetworkVar for Int and Float types, at the expense of Vector and Angle limit.
* 
* @arg string type - Supported choices:

Vector
Angle
* @arg number slot - The slot for this Vector or Angle, from 0 to 31. See Entity:NetworkVar for more detailed explanation.
* @arg string element - Which element of a Vector or an Angle to store the value on. This can be p, y, r for Angles, and x, y, z for Vectors
* @arg string name - The name will affect how you access it. If you call it Foo you would add two new functions on your entity - SetFoo() and GetFoo(). So be careful that what you call it won't collide with any existing functions (don't call it "Pos" for example).
* @arg any [extended=nil] - A table of extra information. See Entity:NetworkVar for details.
 */
    public NetworkVarElement(type: string, slot: number, element: string, name: string, extended?: any): void;

    /**
 * Creates a callback that will execute when the given network variable changes - that is, when the Set<name>() function is run.
* 
* @arg string name - Name of variable to track changes of.
* @arg GMLua.CallbackNoContext callback - The function to call when the variable changes. It is passed 4 arguments:

Entity entity - Entity whos variable changed.
string name - Name of changed variable.
any old - Old/current variable value.
any new - New variable value that it was set to.
 */
    public NetworkVarNotify(name: string, callback: GMLua.CallbackNoContext): void;

    /**
     * In the case of a scripted entity, this will cause the next ENTITY:Think event to be run at the given time.
     *
     * @arg number timestamp - The relative to CurTime timestamp, at which the next think should occur.
     */
    public NextThink(timestamp: number): void;

    /**
     * Returns the center of an entity's bounding box as a local vector.
     *
     * @return {Vector} OBBCenter
     */
    public OBBCenter(): Vector;

    /**
     * Returns the highest corner of an entity's bounding box as a local vector.
     *
     * @return {Vector} The local position of the highest corner of the entity's oriented bounding box.
     */
    public OBBMaxs(): Vector;

    /**
     * Returns the lowest corner of an entity's bounding box as a local vector.
     *
     * @return {Vector} The local position of the lowest corner of the entity's oriented bounding box.
     */
    public OBBMins(): Vector;

    /**
     * Returns the entity's capabilities as a bitfield.
     *
     * @return {number} The bitfield, a combination of the FCAP_ flags.
     */
    public ObjectCaps(): number;

    /**
     * Returns true if the entity is on the ground, and false if it isn't.
     *
     * @return {boolean} Whether the entity is on the ground or not.
     */
    public OnGround(): boolean;

    /**
     * Tests whether the damage passes the entity filter.
     *
     * @arg CTakeDamageInfo dmg - The damage info to test
     * @return {boolean} Whether the damage info passes the entity filter.
     */
    public PassesDamageFilter(dmg: CTakeDamageInfo): boolean;

    /**
 * Tests whether the entity passes the entity filter.
* 
* @arg Entity caller - The initiator of the test.
For example the trigger this filter entity is used in.
* @arg Entity ent - The entity to test against the entity filter.
* @return {boolean} Whether the entity info passes the entity filter.
 */
    public PassesFilter(caller: Entity, ent: Entity): boolean;

    /**
     * Destroys the current physics object of an entity.
     */
    public PhysicsDestroy(): void;

    /**
     * Initializes the physics mesh of the entity from a triangle soup defined by a table of vertices. The resulting mesh is hollow, may contain holes, and always has a volume of 0.
     *
     * @arg MeshVertexStruct vertices - A table consisting of Structures/MeshVertex (only the pos element is taken into account). Every 3 vertices define a triangle in the physics mesh.
     * @return {boolean} Returns true on success, nil otherwise.
     */
    public PhysicsFromMesh(vertices: MeshVertexStruct): boolean;

    /**
 * Initializes the physics object of the entity using its current model. Deletes the previous physics object if it existed and the new object creation was successful.
* 
* @arg number solidType - The solid type of the physics object to create, see Enums/SOLID. Should be SOLID_VPHYSICS in most cases.
Using SOLID_NONE will only delete the current physics object - it does not create a new one.
* @return {boolean} Returns true on success, false otherwise.
 */
    public PhysicsInit(solidType: number): boolean;

    /**
     * Makes the physics object of the entity a AABB.
     *
     * @arg Vector mins - The minimum position of the box. This is automatically ordered with the maxs.
     * @arg Vector maxs - The maximum position of the box. This is automatically ordered with the mins.
     * @return {boolean} Returns true on success, nil otherwise. This fails when the game cannot create any more PhysCollides.
     */
    public PhysicsInitBox(mins: Vector, maxs: Vector): boolean;

    /**
     * Initializes the physics mesh of the entity with a convex mesh defined by a table of points. The resulting mesh is the  of all the input points. If successful, the previous physics object will be removed.
     *
     * @arg Vector points - A table of eight Vectors, in local coordinates, to be used in the computation of the convex mesh. Order does not matter.
     * @return {boolean} Returns true on success, false otherwise.
     */
    public PhysicsInitConvex(points: Vector): boolean;

    /**
     * An advanced version of Entity:PhysicsInitConvex which initializes a physics object from multiple convex meshes. This should be used for physics objects with a custom shape which cannot be represented by a single convex mesh.
     *
     * @arg Vector vertices - A table consisting of tables of Vectors. Each sub-table defines a set of points to be used in the computation of one convex mesh.
     * @return {boolean} Returns true on success, nil otherwise.
     */
    public PhysicsInitMultiConvex(vertices: Vector): boolean;

    /**
     * Initializes the entity's physics object as a physics shadow. Removes the previous physics object if successful. This is used internally for the Player's and NPC's physics object, and certain HL2 entities such as the crane.
     *
     * @arg boolean [allowPhysicsMovement=true] - Whether to allow the physics shadow to move under stress.
     * @arg boolean [allowPhysicsRotation=true] - Whether to allow the physics shadow to rotate under stress.
     * @return {boolean} Return true on success, nil otherwise.
     */
    public PhysicsInitShadow(allowPhysicsMovement?: boolean, allowPhysicsRotation?: boolean): boolean;

    /**
     * Makes the physics object of the entity a sphere.
     *
     * @arg number radius - The radius of the sphere.
     * @arg string physmat - Physical material from surfaceproperties.txt or added with physenv.AddSurfaceData.
     * @return {boolean} Returns true on success, false otherwise
     */
    public PhysicsInitSphere(radius: number, physmat: string): boolean;

    /**
     * Initializes a static physics object of the entity using its current model. If successful, the previous physics object is removed.
     *
     * @arg number solidType - The solid type of the physics object to create, see Enums/SOLID. Should be SOLID_VPHYSICS in most cases.
     * @return {boolean} Returns true on success, false otherwise. This will fail if the entity's current model has no associated physics mesh.
     */
    public PhysicsInitStatic(solidType: number): boolean;

    /**
     * Wakes up the entity's physics object
     */
    public PhysWake(): void;

    /**
     * Makes the entity play a .vcd scene. All scenes from Half-Life 2.
     *
     * @arg string scene - Filepath to scene.
     * @arg number [delay=0] - Delay in seconds until the scene starts playing.
     * @return {number} Estimated length of the scene.
     * @return {Entity} The scene entity, removing which will stop the scene from continuing to play.
     */
    public PlayScene(scene: string, delay?: number): LuaMultiReturn<[number, Entity]>;

    /**
     * Changes an entities angles so that it faces the target entity.
     *
     * @arg Entity target - The entity to face.
     */
    public PointAtEntity(target: Entity): void;

    /**
     * Precaches gibs for the entity's model.
     *
     * @return {number} The amount of gibs the prop has
     */
    public PrecacheGibs(): number;

    /**
     * Normalizes the ragdoll. This is used alongside Kinect in Entity:SetRagdollBuildFunction, for more info see ragdoll_motion entity.
     */
    public RagdollSolve(): void;

    /**
     * Sets the function to build the ragdoll. This is used alongside Kinect in Entity:SetRagdollBuildFunction, for more info see ragdoll_motion entity.
     */
    public RagdollStopControlling(): void;

    /**
     * Makes the physics objects follow the set bone positions. This is used alongside Kinect in Entity:SetRagdollBuildFunction, for more info see ragdoll_motion entity.
     */
    public RagdollUpdatePhysics(): void;

    /**
     * Removes the entity it is used on. The entity will be removed at the start of next tick.
     */
    public Remove(): void;

    /**
     * Removes all decals from the entities surface.
     */
    public RemoveAllDecals(): void;

    /**
     * Removes and stops all gestures.
     */
    public RemoveAllGestures(): void;

    /**
     * Removes a callback previously added with Entity:AddCallback
     *
     * @arg string hook - The hook name to remove. See Entity Callbacks
     * @arg number callbackid - The callback id previously retrieved with the return of Entity:AddCallback or Entity:GetCallbacks
     */
    public RemoveCallback(hook: string, callbackid: number): void;

    /**
     * Removes a function previously added via Entity:CallOnRemove.
     *
     * @arg string identifier - Identifier of the function within CallOnRemove
     */
    public RemoveCallOnRemove(identifier: string): void;

    /**
     * Removes an engine effect applied to an entity.
     *
     * @arg number effect - The effect to remove, see Enums/EF.
     */
    public RemoveEffects(effect: number): void;

    /**
     * Removes specified engine flag
     *
     * @arg number flag - The flag to remove, see Enums/EFL
     */
    public RemoveEFlags(flag: number): void;

    /**
     * Removes specified flag(s) from the entity
     *
     * @arg number flag - The flag(s) to remove, see Enums/FL
     */
    public RemoveFlags(flag: number): void;

    /**
     * Removes a PhysObject from the entity's motion controller so that ENTITY:PhysicsSimulate will no longer be called for given PhysObject.
     *
     * @arg PhysObj physObj - The PhysObj to remove from the motion controller.
     */
    public RemoveFromMotionController(physObj: PhysObj): void;

    /**
     * Removes and stops the gesture with given activity.
     *
     * @arg number activity - The activity remove. See Enums/ACT.
     */
    public RemoveGesture(activity: number): void;

    /**
     * Breaks internal Ragdoll constrains, so you can for example separate an arm from the body of a ragdoll and preserve all physics.
     *
     * @arg number [num=-1] - Which constraint to break, values below 0 mean break them all
     */
    public RemoveInternalConstraint(num?: number): void;

    /**
     * Removes solid flag(s) from the entity.
     *
     * @arg number flags - The flag(s) to remove, see Enums/FSOLID.
     */
    public RemoveSolidFlags(flags: number): void;

    /**
 * Plays an animation on the entity. This may not always work on engine entities.
* 
* @arg number sequence - The sequence to play. Also accepts strings.
If set to a string, the function will automatically call Entity:LookupSequence to retrieve the sequence ID as a number.
 */
    public ResetSequence(sequence: number): void;

    /**
     * Reset entity sequence info such as playback rate, ground speed, last event check, etc.
     */
    public ResetSequenceInfo(): void;

    /**
     * Makes the entity/weapon respawn.
     */
    public Respawn(): void;

    /**
     * Restarts the entity's animation gesture. If the given gesture is already playing, it will reset it and play it from the beginning.
     *
     * @arg number activity - The activity number to send to the entity. See Enums/ACT and Entity:GetSequenceActivity
     * @arg boolean [addIfMissing=true] - Add/start the gesture to if it has not been yet started.
     * @arg boolean [autokill=true]
     */
    public RestartGesture(activity: number, addIfMissing?: boolean, autokill?: boolean): void;

    /**
     * Returns sequence ID corresponding to given activity ID.
     *
     * @arg number act - The activity ID, see Enums/ACT.
     * @return {number} The sequence ID
     */
    public SelectWeightedSequence(act: number): number;

    /**
     * Returns the sequence ID corresponding to given activity ID, and uses the provided seed for random selection. The seed should be the same server-side and client-side if used in a predicted environment.
     *
     * @arg number act - The activity ID, see Enums/ACT.
     * @arg number seed - The seed to use for randomly selecting a sequence in the case the activity ID has multiple sequences bound to it. Entity:SelectWeightedSequence uses the same seed as util.SharedRandom internally for this.
     * @return {number} The sequence ID
     */
    public SelectWeightedSequenceSeeded(act: number, seed: number): number;

    /**
     * Sends sequence animation to the view model. It is recommended to use this for view model animations, instead of Entity:ResetSequence.
     *
     * @arg number seq - The sequence ID returned by Entity:LookupSequence or  Entity:SelectWeightedSequence.
     */
    public SendViewModelMatchingSequence(seq: number): void;

    /**
     * Returns length of currently played sequence.
     *
     * @arg number [seqid=NaN] - A sequence ID to return the length specific sequence of instead of the entity's main/currently playing sequence.
     * @return {number} The length of the sequence
     */
    public SequenceDuration(seqid?: number): number;

    /**
     * Sets the entity's velocity.
     *
     * @arg Vector velocity - The new velocity to set.
     */
    public SetAbsVelocity(velocity: Vector): void;

    /**
     * Sets the angles of the entity.
     *
     * @arg Angle angles - The new angles.
     */
    public SetAngles(angles: Angle): void;

    /**
     * Sets a player's third-person animation. Mainly used by Weapons to start the player's weapon attack and reload animations.
     *
     * @arg number playerAnim - Player animation, see Enums/PLAYER.
     */
    public SetAnimation(playerAnim: number): void;

    /**
     * Sets the start time (relative to CurTime) of the current animation, which is used to determine Entity:GetCycle. Should be less than CurTime to play an animation from the middle.
     *
     * @arg number time - The time the animation was supposed to begin.
     */
    public SetAnimTime(time: number): void;

    /**
     * Parents the sprite to an attachment on another model.
     *
     * @arg Entity ent - The entity to attach/parent to
     * @arg number attachment - The attachment ID to parent to
     */
    public SetAttachment(ent: Entity, attachment: number): void;

    /**
     * Sets the blood color this entity uses.
     *
     * @arg number bloodColor - An integer corresponding to Enums/BLOOD_COLOR.
     */
    public SetBloodColor(bloodColor: number): void;

    /**
     * Sets an entities' bodygroup.
     *
     * @arg number bodygroup - The id of the bodygroup you're setting. Starts from 0.
     * @arg number value - The value you're setting the bodygroup to. Starts from 0.
     */
    public SetBodygroup(bodygroup: number, value: number): void;

    /**
     * Sets the bodygroups from a string. A convenience function for Entity:SetBodygroup.
     *
     * @arg string bodygroups - Body groups to set. Each character in the string represents a separate bodygroup. (0 to 9, a to z being (10 to 35))
     */
    public SetBodyGroups(bodygroups: string): void;

    /**
 * Sets the specified value on the bone controller with the given ID of this entity, it's used in HL1 to change the head rotation of NPCs, turret aiming and so on.
* 
* @arg number boneControllerID - The ID of the bone controller to set the value to.
Goes from 0 to 3.
* @arg number value - The value to set on the specified bone controller.
 */
    public SetBoneController(boneControllerID: number, value: number): void;

    /**
     * Sets the bone matrix of given bone to given matrix. See also Entity:GetBoneMatrix.
     *
     * @arg number boneid - The ID of the bone
     * @arg VMatrix matrix - The matrix to set.
     */
    public SetBoneMatrix(boneid: number, matrix: VMatrix): void;

    /**
     * Sets the bone position and angles.
     *
     * @arg number bone - The bone ID to manipulate
     * @arg Vector pos - The position to set
     * @arg Angle ang - The angles to set
     */
    public SetBonePosition(bone: number, pos: Vector, ang: Angle): void;

    /**
     * Sets the collision bounds for the entity, which are used for triggers ( Entity:SetTrigger, ENTITY:Touch ), and collision ( If Entity:SetSolid set as SOLID_BBOX ).
     *
     * @arg Vector mins - The minimum vector of the bounds. The vector must be smaller than second argument on all axises.
     * @arg Vector maxs - The maximum vector of the bounds. The vector must be bigger than first argument on all axises.
     */
    public SetCollisionBounds(mins: Vector, maxs: Vector): void;

    /**
     * Sets the collision bounds for the entity, which are used for triggers ( Entity:SetTrigger, ENTITY:Touch ), determining if rendering is necessary clientside, and collision ( If Entity:SetSolid set as SOLID_BBOX ).
     *
     * @arg Vector vec1 - The first vector of the bounds.
     * @arg Vector vec2 - The second vector of the bounds.
     */
    public SetCollisionBoundsWS(vec1: Vector, vec2: Vector): void;

    /**
     * Sets the entity's collision group.
     *
     * @arg number group - Collision group of the entity, see Enums/COLLISION_GROUP
     */
    public SetCollisionGroup(group: number): void;

    /**
     * Sets the color of an entity.
     *
     * @arg Color [color=Color(255, 0, 255, 255)] - The color to set. Uses the Color.
     */
    public SetColor(color?: Color): void;

    /**
     * Sets the creator of the Entity. This is set automatically in Sandbox gamemode when spawning SENTs, but is never used/read by default.
     *
     * @arg Player ply - The creator
     */
    public SetCreator(ply: Player): void;

    /**
     * Marks the entity to call GM:ShouldCollide. Not to be confused with Entity:EnableCustomCollisions.
     *
     * @arg boolean enable - Enable or disable the custom collision check
     */
    public SetCustomCollisionCheck(enable: boolean): void;

    /**
     * Sets the progress of the current animation to a specific value between 0 and 1.
     *
     * @arg number value - The desired cycle value
     */
    public SetCycle(value: number): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg Angle ang - The angle to write on the entity's datatable.
     */
    public SetDTAngle(key: number, ang: Angle): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg boolean bool - The boolean to write on the entity's metatable.
     */
    public SetDTBool(key: number, bool: boolean): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg Entity ent - The entity to write on this entity's datatable.
     */
    public SetDTEntity(key: number, ent: Entity): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg number float - The float to write on the entity's datatable.
     */
    public SetDTFloat(key: number, float: number): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg number integer - The integer to write on the entity's datatable. This will be cast to a 32-bit signed integer internally.
     */
    public SetDTInt(key: number, integer: number): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 3.
     * @arg string str - The string to write on the entity's datatable, can't be more than 512 characters per string.
     */
    public SetDTString(key: number, str: string): void;

    /**
     * This is called internally by the Entity:NetworkVar system, you can use this in cases where using NetworkVar is not possible.
     *
     * @arg number key - Goes from 0 to 31.
     * @arg Vector vec - The vector to write on the entity's datatable.
     */
    public SetDTVector(key: number, vec: Vector): void;

    /**
     * Sets the elasticity of this entity, used by some flying entities such as the Helicopter NPC to determine how much it should bounce around when colliding.
     *
     * @arg number elasticity - The elasticity to set.
     */
    public SetElasticity(elasticity: number): void;

    /**
 * Allows you to set the Start or End entity attachment for the rope.
* 
* @arg string name - The name of the variable to modify.
Accepted names are StartEntity and EndEntity.
* @arg Entity entity - The entity to apply to the specific attachment.
 */
    public SetEntity(name: string, entity: Entity): void;

    /**
     * Sets the position an entity's eyes look toward.
     *
     * @arg Vector pos - The world position the entity is looking toward.
     */
    public SetEyeTarget(pos: Vector): void;

    /**
     * Sets the flex scale of the entity.
     *
     * @arg number scale - The new flex scale to set to
     */
    public SetFlexScale(scale: number): void;

    /**
     * Sets the flex weight.
     *
     * @arg number flex - The ID of the flex to modify weight of
     * @arg number weight - The new weight to set
     */
    public SetFlexWeight(flex: number, weight: number): void;

    /**
     * Sets how much friction an entity has when sliding against a surface. Entities default to 1 (100%) and can be higher or even negative.
     *
     * @arg number friction - Friction multiplier
     */
    public SetFriction(friction: number): void;

    /**
     * Sets the gravity multiplier of the entity.
     *
     * @arg number gravityMultiplier - Value which specifies the gravity multiplier.
     */
    public SetGravity(gravityMultiplier: number): void;

    /**
     * Sets the ground the entity is standing on.
     *
     * @arg Entity ground - The ground entity.
     */
    public SetGroundEntity(ground: Entity): void;

    /**
     * Sets the health of the entity.
     *
     * @arg number newHealth - New health value.
     */
    public SetHealth(newHealth: number): void;

    /**
 * Sets the current Hitbox set for the entity.
* 
* @arg number id - The new hitbox set to set. Can be a name as a string, or the ID as a number.
If the operation failed, the function will silently fail.
 */
    public SetHitboxSet(id: number): void;

    /**
     * Enables or disable the inverse kinematic usage of this entity.
     *
     * @arg boolean [useIK=false] - The state of the IK.
     */
    public SetIK(useIK?: boolean): void;

    /**
     * Sets Hammer key values on an entity.
     *
     * @arg string key - The internal key name
     * @arg string value - The value to set
     */
    public SetKeyValue(key: string, value: string): void;

    /**
     * This allows the entity to be lag compensated during Player:LagCompensation.
     *
     * @arg boolean enable - Whether the entity should be lag compensated or not.
     */
    public SetLagCompensated(enable: boolean): void;

    /**
     *
     *
     * @arg number layerID - The Layer ID
     * @arg number blendIn
     */
    public SetLayerBlendIn(layerID: number, blendIn: number): void;

    /**
     *
     *
     * @arg number layerID - The Layer ID
     * @arg number blendOut
     */
    public SetLayerBlendOut(layerID: number, blendOut: number): void;

    /**
     * Sets the animation cycle/frame of given layer.
     *
     * @arg number layerID - The Layer ID
     * @arg number cycle - The new animation cycle/frame for given layer.
     */
    public SetLayerCycle(layerID: number, cycle: number): void;

    /**
     * Sets the duration of given layer. This internally overrides the Entity:SetLayerPlaybackRate.
     *
     * @arg number layerID - The Layer ID
     * @arg number duration - The new duration of the layer in seconds.
     */
    public SetLayerDuration(layerID: number, duration: number): void;

    /**
     * Sets whether the layer should loop or not.
     *
     * @arg number layerID - The Layer ID
     * @arg boolean loop - Whether the layer should loop or not.
     */
    public SetLayerLooping(layerID: number, loop: boolean): void;

    /**
     * Sets the layer playback rate. See also Entity:SetLayerDuration.
     *
     * @arg number layerID - The Layer ID
     * @arg number rate - The new playback rate.
     */
    public SetLayerPlaybackRate(layerID: number, rate: number): void;

    /**
     * Sets the priority of given layer.
     *
     * @arg number layerID - The Layer ID
     * @arg number priority - The new priority of the layer.
     */
    public SetLayerPriority(layerID: number, priority: number): void;

    /**
     * Sets the sequence of given layer.
     *
     * @arg number layerID - The Layer ID.
     * @arg number seq - The sequenceID to set. See Entity:LookupSequence.
     */
    public SetLayerSequence(layerID: number, seq: number): void;

    /**
     * Sets the layer weight. This influences how strongly the animation should be overriding the normal animations of the entity.
     *
     * @arg number layerID - The Layer ID
     * @arg number weight - The new layer weight.
     */
    public SetLayerWeight(layerID: number, weight: number): void;

    /**
     * This forces an entity to use the bone transformation behaviour from versions prior to 8 July 2014.
     *
     * @arg boolean enabled - Whether the entity should use the old bone transformation behaviour or not.
     */
    public SetLegacyTransform(enabled: boolean): void;

    /**
     * Sets the entity to be used as the light origin position for this entity.
     *
     * @arg Entity lightOrigin - The lighting entity.
     */
    public SetLightingOriginEntity(lightOrigin: Entity): void;

    /**
     * Sets angles relative to angles of Entity:GetParent
     *
     * @arg Angle ang - The local angle
     */
    public SetLocalAngles(ang: Angle): void;

    /**
     * Sets the entity's angular velocity (rotation speed).
     *
     * @arg Angle angVel - The angular velocity to set.
     */
    public SetLocalAngularVelocity(angVel: Angle): void;

    /**
     * Sets local position relative to the parented position. This is for use with Entity:SetParent to offset position.
     *
     * @arg Vector pos - The local position
     */
    public SetLocalPos(pos: Vector): void;

    /**
     * Sets the entity's local velocity which is their velocity due to movement in the world from forces such as gravity. Does not include velocity from entity-on-entity collision or other world movement.
     *
     * @arg Vector velocity - The new velocity to set.
     */
    public SetLocalVelocity(velocity: Vector): void;

    /**
 * Sets the Level Of Detail model to use with this entity. This may not work for all models if the model doesn't include any LOD sub models.
* 
* @arg number [lod=-1] - The Level Of Detail model ID to use. -1 leaves the engine to automatically set the Level of Detail.
The Level Of Detail may range from 0 to 8, with 0 being the highest quality and 8 the lowest.
 */
    public SetLOD(lod?: number): void;

    /**
     * Sets the rendering material override of the entity.
     *
     * @arg string materialName - New material name. Use an empty string ("") to reset to the default materials.
     */
    public SetMaterial(materialName: string): void;

    /**
     * Sets the maximum health for entity. Note, that you can still set entity's health above this amount with Entity:SetHealth.
     *
     * @arg number maxhealth - What the max health should be
     */
    public SetMaxHealth(maxhealth: number): void;

    /**
     * Sets the NPC max yaw speed. Internally sets the m_fMaxYawSpeed variable which is polled by the engine.
     *
     * @arg number maxyaw - The new max yaw value to set
     */
    public SetMaxYawSpeed(maxyaw: number): void;

    /**
     * Sets the model of the entity.
     *
     * @arg string modelName - New model value.
     */
    public SetModel(modelName: string): void;

    /**
     * Alter the model name returned by Entity:GetModel. Does not affect the entity's actual model.
     *
     * @arg string modelname - The new model name.
     */
    public SetModelName(modelname: string): void;

    /**
     * Scales the model of the entity, if the entity is a Player or an NPC the hitboxes will be scaled as well.
     *
     * @arg number scale - A float to scale the model by. 0 will not draw anything. A number less than 0 will draw the model inverted.
     * @arg number [deltaTime=0] - Transition time of the scale change, set to 0 to modify the scale right away. To avoid issues with client-side trace detection this must be set, and can be an extremely low number to mimic a value of 0 such as .000001.
     */
    public SetModelScale(scale: number, deltaTime?: number): void;

    /**
     * Sets the move collide type of the entity. The move collide is the way a physics object reacts to hitting an object - will it bounce, slide?
     *
     * @arg number moveCollideType - The move collide type, see Enums/MOVECOLLIDE
     */
    public SetMoveCollide(moveCollideType: number): void;

    /**
     * Sets the Movement Parent of an entity to another entity.
     *
     * @arg Entity Parent - The entity to change this entity's Movement Parent to.
     */
    public SetMoveParent(Parent: Entity): void;

    /**
     * Sets the entity's move type. This should be called before initializing the physics object on the entity, unless it will override SetMoveType such as Entity:PhysicsInitBox.
     *
     * @arg number movetype - The new movetype, see Enums/MOVETYPE
     */
    public SetMoveType(movetype: number): void;

    /**
     * Sets the mapping name of the entity.
     *
     * @arg string mappingName - The name to set for the entity.
     */
    public SetName(mappingName: string): void;

    /**
     * Alters the entity's perceived serverside angle on the client.
     *
     * @arg Angle angle - Networked angle.
     */
    public SetNetworkAngles(angle: Angle): void;

    /**
     * Sets a networked angle value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Angle [value=Angle( 0, 0, 0 )] - The value to set
     */
    public SetNetworkedAngle(key: string, value?: Angle): void;

    /**
     * Sets a networked boolean value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg boolean [value=false] - The value to set
     */
    public SetNetworkedBool(key: string, value?: boolean): void;

    /**
     * Sets a networked entity value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Entity [value=NULL] - The value to set
     */
    public SetNetworkedEntity(key: string, value?: Entity): void;

    /**
     * Sets a networked float value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg number [value=0] - The value to set
     */
    public SetNetworkedFloat(key: string, value?: number): void;

    /**
     * Sets a networked integer value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg number [value=0] - The value to set
     */
    public SetNetworkedInt(key: string, value?: number): void;

    /**
     * Sets a networked number at the specified index on the entity.
     *
     * @arg any index - The index that the value is stored in.
     * @arg number number - The value to network.
     */
    public SetNetworkedNumber(index: any, number: number): void;

    /**
     * Sets a networked string value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg string value - The value to set
     */
    public SetNetworkedString(key: string, value: string): void;

    /**
     * Sets callback function to be called when given NWVar changes.
     *
     * @arg string name - The name of the NWVar to add callback for.
     * @arg GMLua.CallbackNoContext callback - The function to be called when the NWVar changes.
     */
    public SetNetworkedVarProxy(name: string, callback: GMLua.CallbackNoContext): void;

    /**
     * Sets a networked vector value at specified index on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Vector [value=Vector( 0, 0, 0 )] - The value to set
     */
    public SetNetworkedVector(key: string, value?: Vector): void;

    /**
     * Virtually changes entity position for clients. Does the same thing as Entity:SetPos when used serverside.
     *
     * @arg Vector origin - The position to make clients think this entity is at.
     */
    public SetNetworkOrigin(origin: Vector): void;

    /**
     * Sets the next time the clientside ENTITY:Think is called.
     *
     * @arg number nextthink - The next time, relative to CurTime, to execute the ENTITY:Think clientside.
     */
    public SetNextClientThink(nextthink: number): void;

    /**
     * Sets if the entity's model should render at all.
     *
     * @arg boolean shouldNotDraw - true disables drawing
     */
    public SetNoDraw(shouldNotDraw: boolean): void;

    /**
     * Sets whether the entity is solid or not.
     *
     * @arg boolean IsNotSolid - True will make the entity not solid, false will make it solid.
     */
    public SetNotSolid(IsNotSolid: boolean): void;

    /**
     * Sets the NPC classification. Internally sets the m_iClass variable which is polled by the engine.
     *
     * @arg number classification - The CLASS Enum
     */
    public SetNPCClass(classification: number): void;

    /**
     * Sets a networked angle value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Angle value - The value to set
     */
    public SetNWAngle(key: string, value: Angle): void;

    /**
     * Sets a networked boolean value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg boolean value - The value to set
     */
    public SetNWBool(key: string, value: boolean): void;

    /**
     * Sets a networked entity value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Entity value - The value to set
     */
    public SetNWEntity(key: string, value: Entity): void;

    /**
     * Sets a networked float (number) value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg number value - The value to set
     */
    public SetNWFloat(key: string, value: number): void;

    /**
     * Sets a networked integer (whole number) value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg number value - The value to set
     */
    public SetNWInt(key: string, value: number): void;

    /**
     * Sets a networked string value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg string value - The value to set, up to 199 characters.
     */
    public SetNWString(key: string, value: string): void;

    /**
 * Sets a function to be called when the NWVar changes.
* 
* @arg any key - The key of the NWVar to add callback for.
* @arg GMLua.CallbackNoContext callback - The function to be called when the NWVar changes. It has 4 arguments:

Entity ent - The entity
string name - Name of the NWVar that has changed
any oldval - The old value
any newval - The new value
 */
    public SetNWVarProxy(key: any, callback: GMLua.CallbackNoContext): void;

    /**
     * Sets a networked vector value on the entity.
     *
     * @arg string key - The key to associate the value with
     * @arg Vector value - The value to set
     */
    public SetNWVector(key: string, value: Vector): void;

    /**
     * Sets the owner of this entity, disabling all physics interaction with it.
     *
     * @arg Entity [owner=NULL] - The entity to be set as owner.
     */
    public SetOwner(owner?: Entity): void;

    /**
 * Sets the parent of this entity, making it move with its parent. This will make the child entity non solid, nothing can interact with them, including traces.
* 
* @arg Entity [parent=NULL] - The entity to parent to. Setting this to nil will clear the parent.
* @arg number [attachmentId=-1] - The attachment id to use when parenting, defaults to -1 or whatever the parent had set previously.
You must call Entity:SetMoveType( MOVETYPE_NONE ) on the child for this argument to have any effect!
 */
    public SetParent(parent?: Entity, attachmentId?: number): void;

    /**
     * Sets the parent of an entity to another entity with the given physics bone number. Similar to Entity:SetParent, except it is parented to a physbone. This function is useful mainly for ragdolls.
     *
     * @arg number bone - Physics bone number to attach to. Use 0 for objects with only one physics bone. (See Entity:GetPhysicsObjectNum)
     */
    public SetParentPhysNum(bone: number): void;

    /**
     * Sets whether or not the given entity is persistent. A persistent entity will be saved on server shutdown and loaded back when the server starts up. Additionally, by default persistent entities cannot be grabbed with the physgun and tools cannot be used on them.
     *
     * @arg boolean persist - Whether or not the entity should be persistent.
     */
    public SetPersistent(persist: boolean): void;

    /**
     * When called on a constraint entity, sets the two physics objects to be constrained.
     *
     * @arg PhysObj Phys1 - The first physics object to be constrained.
     * @arg PhysObj Phys2 - The second physics object to be constrained.
     */
    public SetPhysConstraintObjects(Phys1: PhysObj, Phys2: PhysObj): void;

    /**
     * Sets the player who gets credit if this entity kills something with physics damage within the time limit.
     *
     * @arg Player ent - Player who gets the kills. Setting this to a non-player entity will not work.
     * @arg number [timeLimit=5] - Time in seconds until the entity forgets its physics attacker and prevents it from getting the kill credit.
     */
    public SetPhysicsAttacker(ent: Player, timeLimit?: number): void;

    /**
     * Allows you to set how fast an entity's animation will play, with 1.0 being the default speed.
     *
     * @arg number fSpeed - How fast the animation will play.
     */
    public SetPlaybackRate(fSpeed: number): void;

    /**
     * Moves the entity to the specified position.
     *
     * @arg Vector position - The position to move the entity to.
     */
    public SetPos(position: Vector): void;

    /**
     * Sets the specified pose parameter to the specified value.
     *
     * @arg string poseName - Name of the pose parameter. Entity:GetPoseParameterName might come in handy here.
     * @arg number poseValue - The value to set the pose to.
     */
    public SetPoseParameter(poseName: string, poseValue: number): void;

    /**
     * Sets whether an entity should be predictable or not.
     * When an entity is set as predictable, its DT vars can be changed during predicted hooks. This is useful for entities which can be controlled by player input.
     *
     * @arg boolean setPredictable - whether to make this entity predictable or not.
     */
    public SetPredictable(setPredictable: boolean): void;

    /**
     * Prevents the server from sending any further information about the entity to a player.
     *
     * @arg Player player - The player to stop networking the entity to.
     * @arg boolean stopTransmitting - true to stop the entity from networking, false to make it network again.
     */
    public SetPreventTransmit(player: Player, stopTransmitting: boolean): void;

    /**
     * Sets the bone angles. This is used alongside Kinect in Entity:SetRagdollBuildFunction, for more info see ragdoll_motion entity.
     *
     * @arg number boneid - Bone ID
     * @arg Angle pos - Angle to set
     */
    public SetRagdollAng(boneid: number, pos: Angle): void;

    /**
 * Sets the function to build the ragdoll. This is used alongside Kinect, for more info see ragdoll_motion entity.
* 
* @arg GMLua.CallbackNoContext func - The build function. This function has one argument:

Entity ragdoll - The ragdoll to build
 */
    public SetRagdollBuildFunction(func: GMLua.CallbackNoContext): void;

    /**
     * Sets the bone position. This is used alongside Kinect in Entity:SetRagdollBuildFunction, for more info see ragdoll_motion entity.
     *
     * @arg number boneid - Bone ID
     * @arg Vector pos - Position to set
     */
    public SetRagdollPos(boneid: number, pos: Vector): void;

    /**
     * Sets the render angles of the Entity.
     *
     * @arg Angle newAngles - The new render angles to be set to.
     */
    public SetRenderAngles(newAngles: Angle): void;

    /**
     * Sets the render bounds for the entity. For world space coordinates see Entity:SetRenderBoundsWS.
     *
     * @arg Vector mins - The minimum corner of the bounds, relative to origin of the entity.
     * @arg Vector maxs - The maximum corner of the bounds, relative to origin of the entity.
     * @arg Vector [add=Vector( 0, 0, 0 )] - If defined, adds this vector to maxs and subtracts this vector from mins.
     */
    public SetRenderBounds(mins: Vector, maxs: Vector, add?: Vector): void;

    /**
     * Sets the render bounds for the entity in world space coordinates. For relative coordinates see Entity:SetRenderBounds.
     *
     * @arg Vector mins - The minimum corner of the bounds, relative to origin of the world/map.
     * @arg Vector maxs - The maximum corner of the bounds, relative to origin of the world/map.
     * @arg Vector [add=Vector( 0, 0, 0 )] - If defined, adds this vector to maxs and subtracts this vector from mins.
     */
    public SetRenderBoundsWS(mins: Vector, maxs: Vector, add?: Vector): void;

    /**
     * Used to specify a plane, past which an object will be visually clipped.
     *
     * @arg Vector planeNormal - The normal of the plane. Anything behind the normal will be clipped.
     * @arg number planePosition - The position of the plane.
     */
    public SetRenderClipPlane(planeNormal: Vector, planePosition: number): void;

    /**
     * Enables the use of clipping planes to "cut" objects.
     *
     * @arg boolean enabled - Enable or disable clipping planes
     */
    public SetRenderClipPlaneEnabled(enabled: boolean): void;

    /**
     * Sets entity's render FX.
     *
     * @arg number renderFX - The new render FX to set, see Enums/kRenderFx
     */
    public SetRenderFX(renderFX: number): void;

    /**
     * Sets the render mode of the entity.
     *
     * @arg number renderMode - New render mode to set, see Enums/RENDERMODE.
     */
    public SetRenderMode(renderMode: number): void;

    /**
     * Set the origin in which the Entity will be drawn from.
     *
     * @arg Vector newOrigin - The new origin in world coordinates where the Entity's model will now be rendered from.
     */
    public SetRenderOrigin(newOrigin: Vector): void;

    /**
     * Sets a save value for an entity. You can see a full list of an entity's save values by creating it and printing Entity:GetSaveTable().
     *
     * @arg string name - Name of the save value to set
     * @arg any value - Value to set
     * @return {boolean} Key successfully set
     */
    public SetSaveValue(name: string, value: any): boolean;

    /**
 * Sets the entity's model sequence.
* 
* @arg number sequenceId - The sequence to play. Also accepts strings.
If set to a string, the function will automatically call Entity:LookupSequence to retrieve the sequence ID as a number.
 */
    public SetSequence(sequenceId: number): void;

    /**
     * Sets whether or not the entity should make a physics contact sound when it's been picked up by a player.
     *
     * @arg boolean [playsound=false] - True to play the pickup sound, false otherwise.
     */
    public SetShouldPlayPickupSound(playsound?: boolean): void;

    /**
     * Sets if entity should create a server ragdoll on death or a client one.
     *
     * @arg boolean serverragdoll - Set true if ragdoll should be created on server, false if on client.
     */
    public SetShouldServerRagdoll(serverragdoll: boolean): void;

    /**
     * Sets the skin of the entity.
     *
     * @arg number skinIndex - 0-based index of the skin to use.
     */
    public SetSkin(skinIndex: number): void;

    /**
     * Sets the solidity of an entity.
     *
     * @arg number solid_type - The solid type. See the Enums/SOLID.
     */
    public SetSolid(solid_type: number): void;

    /**
     * Sets solid flag(s) for the entity.
     *
     * @arg number flags - The flag(s) to set, see Enums/FSOLID.
     */
    public SetSolidFlags(flags: number): void;

    /**
     * Sets whether the entity should use a spawn effect when it is created on the client.
     *
     * @arg boolean spawnEffect - Sets if we should show a spawn effect.
     */
    public SetSpawnEffect(spawnEffect: boolean): void;

    /**
 * Overrides a single material on the model of this entity.
* 
* @arg number [index=NaN] - Index of the material to override, acceptable values are from 0 to 31.
Indexes are by Entity:GetMaterials, but you have to subtract 1 from them.
If called with no arguments, all sub materials will be reset.
* @arg string [material="nil"] - The material to override the default one with. Set to nil to revert to default material.
 */
    public SetSubMaterial(index?: number, material?: string): void;

    /**
     * Changes the table that can be accessed by indexing an entity. Each entity starts with its own table by default.
     *
     * @arg object tab - Table for the entity to use
     */
    public SetTable(tab: object): void;

    /**
     * When this flag is set the entity will only transmit to the player when its parent is transmitted. This is useful for things like viewmodel attachments since without this flag they will transmit to everyone (and cause the viewmodels to transmit to everyone too).
     *
     * @arg boolean onoff - Will set the TransmitWithParent flag on or off
     */
    public SetTransmitWithParent(onoff: boolean): void;

    /**
     * Marks the entity as a trigger, so it will generate ENTITY:StartTouch, ENTITY:Touch and ENTITY:EndTouch callbacks.
     *
     * @arg boolean maketrigger - Make the entity trigger or not
     */
    public SetTrigger(maketrigger: boolean): void;

    /**
     * Sets whether an entity can be unfrozen, meaning that it cannot be unfrozen using the physgun.
     *
     * @arg boolean [freezable=false] - True to make the entity unfreezable, false otherwise.
     */
    public SetUnFreezable(freezable?: boolean): void;

    /**
     * Forces the entity to reconfigure its bones. You might need to call this after changing your model's scales or when manually drawing the entity multiple times at different positions.
     */
    public SetupBones(): void;

    /**
     * Initializes the class names of an entity's phoneme mappings (mouth movement data). This is called by default with argument "phonemes" when a flex-based entity (such as an NPC) is created.
     *
     * @arg string fileRoot - The file prefix of the phoneme mappings (relative to "garrysmod/expressions/").
     */
    public SetupPhonemeMappings(fileRoot: string): void;

    /**
     * Sets the use type of an entity, affecting how often ENTITY:Use will be called for Lua entities.
     *
     * @arg number useType - The use type to apply to the entity. Uses Enums/_USE.
     */
    public SetUseType(useType: number): void;

    /**
     * Allows to quickly set variable to entity's Entity:GetTable.
     *
     * @arg any key - Key of the value to set
     * @arg any value - Value to set the variable to
     */
    public SetVar(key: any, value: any): void;

    /**
     * Sets the entity's velocity. For entities with physics, consider using PhysObj:SetVelocity on the PhysObj of the entity.
     *
     * @arg Vector velocity - The new velocity to set.
     */
    public SetVelocity(velocity: Vector): void;

    /**
 * Sets the model and associated weapon to this viewmodel entity.
* 
* @arg string viewModel - The model string to give to this viewmodel.
Example: "models/weapons/c_smg1.mdl"
* @arg Weapon [weapon=NULL] - The weapon entity to associate this viewmodel to.
 */
    public SetWeaponModel(viewModel: string, weapon?: Weapon): void;

    /**
     * Returns the amount of skins the entity has.
     *
     * @return {number} The amount of skins the entity's model has.
     */
    public SkinCount(): number;

    /**
     * Moves the model instance from the source entity to this entity. This can be used to transfer decals that have been applied on one entity to another.
     *
     * @arg Entity srcEntity - Entity to move the model instance from.
     * @return {boolean} Whether the operation was successful or not
     */
    public SnatchModelInstance(srcEntity: Entity): boolean;

    /**
     * Initializes the entity and starts its networking. If called on a player, it will respawn them.
     */
    public Spawn(): void;

    /**
     * Starts a "looping" sound. As with any other sound playing methods, this function expects the sound file to be looping itself and will not automatically loop a non looping sound file as one might expect.
     *
     * @arg string sound - Sound to play. Can be either a sound script or a filepath.
     * @return {number} The ID number of started sound starting with 0, or -1 if we failed for some reason.
     */
    public StartLoopingSound(sound: string): number;

    /**
     * Starts a motion controller in the physics engine tied to this entity's PhysObj, which enables the use of ENTITY:PhysicsSimulate.
     */
    public StartMotionController(): void;

    /**
     * Stops all particle effects parented to the entity and immediately destroys them.
     */
    public StopAndDestroyParticles(): void;

    /**
     * Stops a sound created by Entity:StartLoopingSound.
     *
     * @arg number id - The sound ID returned by Entity:StartLoopingSound
     */
    public StopLoopingSound(id: number): void;

    /**
     * Stops the motion controller created with Entity:StartMotionController.
     */
    public StopMotionController(): void;

    /**
     * Stops all particle effects parented to the entity.
     */
    public StopParticleEmission(): void;

    /**
     * Stops any attached to the entity .pcf particles using ParticleEffectAttach.
     */
    public StopParticles(): void;

    /**
     * Stops all particle effects parented to the entity with given name.
     *
     * @arg string name - The name of the particle to stop.
     */
    public StopParticlesNamed(name: string): void;

    /**
     * Stops all particle effects parented to the entity with given name on given attachment.
     *
     * @arg string name - The name of the particle to stop.
     * @arg number attachment - The attachment of the entity to stop particles on.
     */
    public StopParticlesWithNameAndAttachment(name: string, attachment: number): void;

    /**
     * Stops emitting the given sound from the entity, especially useful for looping sounds.
     *
     * @arg string sound - The name of the sound script or the filepath to stop playback of.
     */
    public StopSound(sound: string): void;

    /**
     * Applies the specified amount of damage to the entity with DMG_GENERIC flag.
     *
     * @arg number damageAmount - The amount of damage to be applied.
     * @arg Entity attacker - The entity that initiated the attack that caused the damage.
     * @arg Entity inflictor - The entity that applied the damage, eg. a weapon.
     */
    public TakeDamage(damageAmount: number, attacker: Entity, inflictor: Entity): void;

    /**
     * Applies the damage specified by the damage info to the entity.
     *
     * @arg CTakeDamageInfo damageInfo - The damage to apply.
     */
    public TakeDamageInfo(damageInfo: CTakeDamageInfo): void;

    /**
     * Applies forces to our physics object in response to damage.
     *
     * @arg CTakeDamageInfo dmginfo - The damageinfo to apply. Only CTakeDamageInfo:GetDamageForce and CTakeDamageInfo:GetDamagePosition are used.
     */
    public TakePhysicsDamage(dmginfo: CTakeDamageInfo): void;

    /**
     * Check if the given position or entity is within this entity's PVS.
     *
     * @arg any testPoint - Entity or Vector to test against. If an entity is given, this function will test using its bounding box.
     * @return {boolean} True if the testPoint is within our PVS.
     */
    public TestPVS(testPoint: any): boolean;

    /**
 * Returns the ID of a PhysObj attached to the given bone. To be used with Entity:GetPhysicsObjectNum.
* 
* @arg number boneID - The ID of a bone to look up the "physics root" bone of.
* @return {number} The PhysObj ID of the given bone.

-1 if we somehow cannot translate
 */
    public TranslateBoneToPhysBone(boneID: number): number;

    /**
     * Returns the boneID of the bone the given PhysObj is attached to.
     *
     * @arg number physNum - The PhysObj number on the entity
     * @return {number} The boneID of the bone the PhysObj is attached to.
     */
    public TranslatePhysBoneToBone(physNum: number): number;

    /**
     * Updates positions of bone followers created by Entity:CreateBoneFollowers.
     */
    public UpdateBoneFollowers(): void;

    /**
     * Simulates a +use action on an entity.
     *
     * @arg Entity activator - The entity that caused this input. This will usually be the player who pressed their use key
     * @arg Entity [caller=NULL] - The entity responsible for the input. This will typically be the same as activator unless some other entity is acting as a proxy
     * @arg number [useType=NaN] - Use type, see Enums/USE.
     * @arg number [value=0] - Any value.
     */
    public Use(activator: Entity, caller?: Entity, useType?: number, value?: number): void;

    /**
     * Animations will be handled purely clientside instead of a fixed animtime, enabling interpolation. This does not affect layers and gestures.
     */
    public UseClientSideAnimation(): void;

    /**
     * Enables or disables trigger bounds.
     *
     * @arg boolean enable - Enable or disable the bounds.
     * @arg number [boundSize=0] - The distance/size of the trigger bounds.
     */
    public UseTriggerBounds(enable: boolean, boundSize?: number): void;

    /**
     * Returns the index of this view model, it can be used to identify which one of the player's view models this entity is.
     *
     * @return {number} View model index, ranges from 0 to 2, nil if the entity is not a view model
     */
    public ViewModelIndex(): number;

    /**
     * Returns whether the target/given entity is visible from the this entity.
     *
     * @arg Entity target - Entity to check for visibility to.
     * @return {boolean} If the entities can see each other.
     */
    public Visible(target: Entity): boolean;

    /**
     * Returns true if supplied vector is visible from the entity's line of sight.
     *
     * @arg Vector pos - The position to check for visibility
     * @return {boolean} Within line of sight
     */
    public VisibleVec(pos: Vector): boolean;

    /**
     * Returns an integer that represents how deep in water the entity is.
     *
     * @return {number} The water level.
     */
    public WaterLevel(): number;

    /**
     * Sets the activity of the entity's active weapon.
     *
     * @arg number act - Activity number. See Enums/ACT.
     * @arg number duration - How long the animation should take in seconds.
     */
    public Weapon_SetActivity(act: number, duration: number): void;

    /**
     * Calls and returns WEAPON:TranslateActivity on the weapon the entity ( player or NPC ) carries.
     *
     * @arg number act - The activity to translate
     * @return {number} The translated activity
     */
    public Weapon_TranslateActivity(act: number): number;

    /**
     * Returns two vectors representing the minimum and maximum extent of the entity's bounding box.
     *
     * @return {Vector} The minimum vector for the entity's bounding box.
     * @return {Vector} The maximum vector for the entity's bounding box.
     */
    public WorldSpaceAABB(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Returns the center of the entity according to its collision model.
     *
     * @return {Vector} The center of the entity
     */
    public WorldSpaceCenter(): Vector;

    /**
     * Converts a worldspace vector into a vector local to an entity
     *
     * @arg Vector wpos - The world vector
     * @return {Vector} The local vector
     */
    public WorldToLocal(wpos: Vector): Vector;

    /**
     * Converts world angles to local angles ( local to the entity )
     *
     * @arg Angle ang - The world angles
     * @return {Angle} The local angles
     */
    public WorldToLocalAngles(ang: Angle): Angle;
}

// Allow the class to be instantiated with a function call:

/**
 * Returns the entity with the matching Entity:EntIndex.
 *
 * @arg number entityIndex - The entity index.
 * @return {Entity} The entity if it exists, or NULL if it doesn't.
 */
declare function Entity(entityIndex: number): Entity;

declare type Entity = __EntityClass;
