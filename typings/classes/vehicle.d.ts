declare class __VehicleClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the remaining boosting time left.
     *
     * @return {number} The remaining boosting time left
     */
    public BoostTimeLeft(): number;

    /**
     * Tries to find an Exit Point for leaving vehicle, if one is unobstructed in the direction given.
     *
     * @arg number yaw - Yaw/roll from vehicle angle to check for exit
     * @arg number distance - Distance from origin to drop player
     * @return {Vector} Returns the vector for exit position or nil if cannot exit that way.
     */
    public CheckExitPoint(yaw: number, distance: number): Vector;

    /**
     * Sets whether the engine is enabled or disabled, i.e. can be started or not.
     *
     * @arg boolean enable - Enable or disable the engine
     */
    public EnableEngine(enable: boolean): void;

    /**
     * Returns information about the ammo of the vehicle
     *
     * @return {number} Ammo type of the vehicle ammo
     * @return {number} Clip size
     * @return {number} Count
     */
    public GetAmmo(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns third person camera distance.
     *
     * @return {number} Camera distance
     */
    public GetCameraDistance(): number;

    /**
     * Gets the driver of the vehicle, returns NULL if no driver is present.
     *
     * @return {Entity} The driver of the vehicle.
     */
    public GetDriver(): Entity;

    /**
     * Returns the current speed of the vehicle in Half-Life Hammer Units (in/s). Same as Entity:GetVelocity + Vector:Length.
     *
     * @return {number} The speed of the vehicle
     */
    public GetHLSpeed(): number;

    /**
     * Returns the max speed of the vehicle in MPH.
     *
     * @return {number} The max speed of the vehicle in MPH
     */
    public GetMaxSpeed(): number;

    /**
     * Returns some info about the vehicle.
     *
     * @return {OperatingParamsStruct} The operating params. See Structures/OperatingParams.
     */
    public GetOperatingParams(): OperatingParamsStruct;

    /**
     * Gets the passenger of the vehicle, returns NULL if no drivers is present.
     *
     * @arg number passenger - The index of the passenger
     * @return {Entity} The passenger
     */
    public GetPassenger(passenger: number): Entity;

    /**
     * Returns the seat position and angle of a given passenger seat.
     *
     * @arg number role - The passenger role. ( 1 is the driver )
     * @return {Vector} The seat position
     * @return {Angle} The seat angle
     */
    public GetPassengerSeatPoint(role: number): LuaMultiReturn<[Vector, Angle]>;

    /**
     * Returns the current RPM of the vehicle. This value is fake and doesn't actually affect the vehicle movement.
     *
     * @return {number} The RPM.
     */
    public GetRPM(): number;

    /**
     * Returns the current speed of the vehicle in MPH.
     *
     * @return {number} The speed of the vehicle in MPH
     */
    public GetSpeed(): number;

    /**
     * Returns the current steering of the vehicle.
     *
     * @return {number} The current steering of the vehicle.
     */
    public GetSteering(): number;

    /**
     * Returns the maximum steering degree of the vehicle
     *
     * @return {number} The maximum steering degree of the vehicle
     */
    public GetSteeringDegrees(): number;

    /**
     * Returns if vehicle has thirdperson mode enabled or not.
     *
     * @return {boolean} Returns true if third person mode enabled, false otherwise
     */
    public GetThirdPersonMode(): boolean;

    /**
     * Returns the current throttle of the vehicle.
     *
     * @return {number} The current throttle of the vehicle
     */
    public GetThrottle(): number;

    /**
     * Returns the vehicle class name. This is only useful for Sandbox spawned vehicles or any vehicle that properly sets the vehicle class with Vehicle:SetVehicleClass.
     *
     * @return {string} The class name of the vehicle.
     */
    public GetVehicleClass(): string;

    /**
     * Returns the vehicle parameters of given vehicle.
     *
     * @return {VehicleParamsStruct} The vehicle parameters. See Structures/VehicleParams
     */
    public GetVehicleParams(): VehicleParamsStruct;

    /**
     * Returns the view position and forward angle of a given passenger seat.
     *
     * @arg number [role=0] - The passenger role. 0 is the driver. This parameter seems to be ignored by the game engine and is therefore optional.
     * @return {Vector} The view position, will be 0, 0, 0 on failure
     * @return {Angle} The view angles, will be 0, 0, 0 on failure
     * @return {number} The field of view, will be 0 on failure
     */
    public GetVehicleViewPosition(role?: number): LuaMultiReturn<[Vector, Angle, number]>;

    /**
     * Returns the PhysObj of given wheel.
     *
     * @arg number wheel - The wheel to retrieve
     * @return {PhysObj} The wheel
     */
    public GetWheel(wheel: number): PhysObj;

    /**
     * Returns the base wheel height.
     *
     * @arg number wheel - The wheel to get the base wheel height of.
     * @return {number} The base wheel height.
     */
    public GetWheelBaseHeight(wheel: number): number;

    /**
     * Returns the wheel contact point.
     *
     * @arg number wheel - The wheel to check
     * @return {Vector} The contact position
     * @return {number} The Surface Properties ID of hit surface.
     * @return {boolean} Whether the wheel is on ground or not
     */
    public GetWheelContactPoint(wheel: number): LuaMultiReturn<[Vector, number, boolean]>;

    /**
     * Returns the wheel count of the vehicle
     *
     * @return {number} The amount of wheels
     */
    public GetWheelCount(): number;

    /**
     * Returns the total wheel height.
     *
     * @arg number wheel - The wheel to get the base wheel height of.
     * @return {number} The total wheel height.
     */
    public GetWheelTotalHeight(wheel: number): number;

    /**
     * Returns whether this vehicle has boost at all.
     *
     * @return {boolean} Whether this vehicle has boost at all.
     */
    public HasBoost(): boolean;

    /**
     * Returns whether this vehicle has a brake pedal. See Vehicle:SetHasBrakePedal.
     *
     * @return {boolean} Whether this vehicle has a brake pedal or not.
     */
    public HasBrakePedal(): boolean;

    /**
     * Returns whether this vehicle is currently boosting or not.
     *
     * @return {boolean} Whether this vehicle is currently boosting or not.
     */
    public IsBoosting(): boolean;

    /**
     * Returns whether the engine is enabled or not, i.e. whether it can be started.
     *
     * @return {boolean} Whether the engine is enabled
     */
    public IsEngineEnabled(): boolean;

    /**
     * Returns whether the engine is started or not.
     *
     * @return {boolean} Whether the engine is started or not.
     */
    public IsEngineStarted(): boolean;

    /**
     * Returns true if the vehicle object is a valid or not. This will return false when Vehicle functions are not usable on the vehicle.
     *
     * @return {boolean} Is the vehicle a valid vehicle or not
     */
    public IsValidVehicle(): boolean;

    /**
     * Returns whether this vehicle's engine is underwater or not. ( Internally the attachment point "engine" or "vehicle_engine" is checked )
     *
     * @return {boolean} Whether this vehicle's engine is underwater or not.
     */
    public IsVehicleBodyInWater(): boolean;

    /**
     * Releases the vehicle's handbrake (Jeep) so it can roll without any passengers.
     */
    public ReleaseHandbrake(): void;

    /**
     * Sets the boost. It is possible that this function does not work while the vehicle has a valid driver in it.
     *
     * @arg number boost - The new boost value
     */
    public SetBoost(boost: number): void;

    /**
     * Sets the third person camera distance of the vehicle.
     *
     * @arg number distance - Camera distance to set to
     */
    public SetCameraDistance(distance: number): void;

    /**
     * Turns on or off the Jeep handbrake so it can roll without a driver inside.
     *
     * @arg boolean handbrake - true to turn on, false to turn off.
     */
    public SetHandbrake(handbrake: boolean): void;

    /**
     * Sets whether this vehicle has a brake pedal.
     *
     * @arg boolean brakePedal - Whether this vehicle has a brake pedal
     */
    public SetHasBrakePedal(brakePedal: boolean): void;

    /**
     * Sets maximum reverse throttle
     *
     * @arg number maxRevThrottle - The new maximum throttle. This number must be negative.
     */
    public SetMaxReverseThrottle(maxRevThrottle: number): void;

    /**
     * Sets maximum forward throttle
     *
     * @arg number maxThrottle - The new maximum throttle.
     */
    public SetMaxThrottle(maxThrottle: number): void;

    /**
     * Sets spring length of given wheel
     *
     * @arg number wheel - The wheel to change spring length of
     * @arg number length - The new spring length
     */
    public SetSpringLength(wheel: number, length: number): void;

    /**
     * Sets the steering of the vehicle.
     *
     * @arg number front - Angle of the front wheels (-1 to 1)
     * @arg number rear - Angle of the rear wheels (-1 to 1)
     */
    public SetSteering(front: number, rear: number): void;

    /**
     * Sets the maximum steering degrees of the vehicle
     *
     * @arg number steeringDegrees - The new maximum steering degree
     */
    public SetSteeringDegrees(steeringDegrees: number): void;

    /**
     * Sets the third person mode state.
     *
     * @arg boolean enable - Enable or disable the third person mode for this vehicle
     */
    public SetThirdPersonMode(enable: boolean): void;

    /**
     * Sets the throttle of the vehicle. It is possible that this function does not work with a valid driver in it.
     *
     * @arg number throttle - The new throttle.
     */
    public SetThrottle(throttle: number): void;

    /**
     * Sets the vehicle class name.
     *
     * @arg string _class - The vehicle class name to set
     */
    public SetVehicleClass(_class: string): void;

    /**
     * Sets whether the entry or exit camera animation should be played or not.
     *
     * @arg boolean bOn - Whether the entry or exit camera animation should be played or not.
     */
    public SetVehicleEntryAnim(bOn: boolean): void;

    /**
     * Sets the vehicle parameters for given vehicle.
     *
     * @arg VehicleParamsStruct params - The new new vehicle parameters. See Structures/VehicleParams.
     */
    public SetVehicleParams(params: VehicleParamsStruct): void;

    /**
     * Sets friction of given wheel.  This function may be broken.
     *
     * @arg number wheel - The wheel to change the friction of
     * @arg number friction - The new friction to set
     */
    public SetWheelFriction(wheel: number, friction: number): void;

    /**
     * Starts or stops the engine.
     *
     * @arg boolean start - True to start, false to stop.
     */
    public StartEngine(start: boolean): void;
}

declare type Vehicle = __VehicleClass & Entity;
