declare class __CNavAreaClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
 * Adds a hiding spot onto this nav area.
* 
* @arg Vector pos - The position on the nav area
* @arg number [flags=7] - Flags describing what kind of hiding spot this is.

0 = None (Not recommended)
1 = In Cover/basically a hiding spot, in a corner with good hard cover nearby
2 = good sniper spot, had at least one decent sniping corridor
4 = perfect sniper spot, can see either very far, or a large area, or both
8 = exposed, spot in the open, usually on a ledge or cliff

Values over 255 will be clamped.
 */
    public AddHidingSpot(pos: Vector, flags?: number): void;

    /**
     * Adds this CNavArea to the closed list, a list of areas that have been checked by A* pathfinding algorithm.
     */
    public AddToClosedList(): void;

    /**
     * Adds this CNavArea to the Open List.
     */
    public AddToOpenList(): void;

    /**
     * Clears the open and closed lists for a new search.
     */
    public ClearSearchLists(): void;

    /**
     * Returns the height difference between the edges of two connected navareas.
     *
     * @arg CNavArea navarea
     * @return {number} The height change
     */
    public ComputeAdjacentConnectionHeightChange(navarea: CNavArea): number;

    /**
     * Returns the Enums/NavDir direction that the given vector faces on this CNavArea.
     *
     * @arg Vector pos - The position to compute direction towards.
     * @return {number} The direction the vector is in relation to this CNavArea. See Enums/NavDir.
     */
    public ComputeDirection(pos: Vector): number;

    /**
     * Returns the height difference on the Z axis of the two CNavAreas. This is calculated from the center most point on both CNavAreas.
     *
     * @arg CNavArea navArea - The nav area to test against.
     * @return {number} The ground height change.
     */
    public ComputeGroundHeightChange(navArea: CNavArea): number;

    /**
     * Connects this CNavArea to another CNavArea or CNavLadder with a one way connection. ( From this area to the target )
     *
     * @arg CNavArea area - The CNavArea or CNavLadder this area leads to.
     */
    public ConnectTo(area: CNavArea): void;

    /**
     * Returns true if this CNavArea contains the given vector.
     *
     * @arg Vector pos - The position to test.
     * @return {boolean} True if the vector was inside and false otherwise.
     */
    public Contains(pos: Vector): boolean;

    /**
     * Disconnects this nav area from given area or ladder. (Only disconnects one way)
     *
     * @arg CNavArea area - The CNavArea or CNavLadder this to disconnect from.
     */
    public Disconnect(area: CNavArea): void;

    /**
     * Draws this navarea on debug overlay.
     */
    public Draw(): void;

    /**
     * Draws the hiding spots on debug overlay. This includes sniper/exposed spots too!
     */
    public DrawSpots(): void;

    /**
 * Returns a table of all the CNavAreas that have a  ( one and two way ) connection from this CNavArea.
* 
* @return {CNavArea} A table of all CNavArea that have a ( one and two way ) connection from this CNavArea.
Returns an empty table if this area has no outgoing connections to any other areas.
 */
    public GetAdjacentAreas(): CNavArea;

    /**
 * Returns a table of all the CNavAreas that have a ( one and two way ) connection from this CNavArea in given direction.
* 
* @arg number navDir - The direction, in which to look for CNavAreas, see Enums/NavDir.
* @return {CNavArea} A table of all CNavArea that have a ( one and two way ) connection from this CNavArea in given direction.
Returns an empty table if this area has no outgoing connections to any other areas in given direction.
 */
    public GetAdjacentAreasAtSide(navDir: number): CNavArea;

    /**
     * Returns the amount of CNavAreas that have a connection ( one and two way ) from this CNavArea.
     *
     * @return {number} The amount of CNavAreas that have a connection ( one and two way ) from this CNavArea.
     */
    public GetAdjacentCount(): number;

    /**
     * Returns the amount of CNavAreas that have a connection ( one or two way ) from this CNavArea in given direction.
     *
     * @arg number navDir - The direction, in which to look for CNavAreas, see Enums/NavDir.
     * @return {number} The amount of CNavAreas that have a connection ( one or two way ) from this CNavArea in given direction.
     */
    public GetAdjacentCountAtSide(navDir: number): number;

    /**
 * Returns the attribute mask for the given CNavArea.
* 
* @return {number} Attribute mask for this CNavArea, see Enums/NAV_MESH for the specific flags.
A navmesh that was generated with nav_quicksave set to 1 will have all CNavAreas attribute masks set to 0
 */
    public GetAttributes(): number;

    /**
     * Returns the center most vector point for the given CNavArea.
     *
     * @return {Vector} The center vector.
     */
    public GetCenter(): Vector;

    /**
     * Returns the closest point of this Nav Area from the given position.
     *
     * @arg Vector pos - The given position, can be outside of the Nav Area bounds.
     * @return {Vector} The closest position on this Nav Area.
     */
    public GetClosestPointOnArea(pos: Vector): Vector;

    /**
     * Returns the vector position of the corner for the given CNavArea.
     *
     * @arg number cornerid - The target corner to get the position of, takes Enums/NavCorner.
     * @return {Vector} The corner position.
     */
    public GetCorner(cornerid: number): Vector;

    /**
     * Returns the cost from starting area this area when pathfinding. Set by CNavArea:SetCostSoFar.
     *
     * @return {number} The cost so far.
     */
    public GetCostSoFar(): number;

    /**
     * Returns a table of very bad hiding spots in this area.
     *
     * @return {Vector} A table of Vectors
     */
    public GetExposedSpots(): Vector;

    /**
 * Returns size info about the nav area.
* 
* @return {Vector} Returns a table containing the following keys:

Vector hi|
Vector lo|
number SizeX|
number SizeY|
number SizeZ|
 */
    public GetExtentInfo(): Vector;

    /**
 * Returns a table of good hiding spots in this area.
* 
* @arg number [type=1] - The type of spots to include.

0 = None (Not recommended)
1 = In Cover/basically a hiding spot, in a corner with good hard cover nearby
2 = good sniper spot, had at least one decent sniping corridor
4 = perfect sniper spot, can see either very far, or a large area, or both
8 = exposed, spot in the open, usually on a ledge or cliff, same as GetExposedSpots
Values over 255 and below 0 will be clamped.
* @return {Vector} A table of Vectors
 */
    public GetHidingSpots(type?: number): Vector;

    /**
     * Returns this CNavAreas unique ID.
     *
     * @return {number} The unique ID.
     */
    public GetID(): number;

    /**
 * Returns a table of all the CNavAreas that have a one-way connection to this CNavArea.
* 
* @return {CNavArea} A table of all CNavAreas with one-way connection to this CNavArea.
Returns an empty table if there are no one-way incoming connections to this CNavArea.
 */
    public GetIncomingConnections(): CNavArea;

    /**
 * Returns a table of all the CNavAreas that have a one-way connection to this CNavArea from given direction.
* 
* @arg number navDir - The direction, from which to look for CNavAreas, see Enums/NavDir.
* @return {CNavArea} A table of all CNavAreas with one-way connection to this CNavArea from given direction.
Returns an empty table if there are no one-way incoming connections to this CNavArea from given direction.
 */
    public GetIncomingConnectionsAtSide(navDir: number): CNavArea;

    /**
     * Returns all CNavLadders that have a ( one or two way ) connection from this CNavArea.
     *
     * @return {CNavLadder} The CNavLadders that have a ( one or two way ) connection from this CNavArea.
     */
    public GetLadders(): CNavLadder;

    /**
 * Returns all CNavLadders that have a ( one or two way ) connection from ( one and two way ) this CNavArea in given direction.
* 
* @arg number navDir - The direction, in which to look for CNavLadders.
0 = Up ( LadderDirectionType::LADDER_UP )
1 = Down ( LadderDirectionType::LADDER_DOWN )
* @return {CNavLadder} The CNavLadders that have a ( one or two way ) connection from this CNavArea in given direction.
 */
    public GetLaddersAtSide(navDir: number): CNavLadder;

    /**
     * Returns the parent CNavArea
     *
     * @return {CNavArea} The parent CNavArea
     */
    public GetParent(): CNavArea;

    /**
     * Returns how this CNavArea is connected to its parent.
     *
     * @return {number} See Enums/NavTraverseType
     */
    public GetParentHow(): number;

    /**
     * Returns the Place of the nav area.
     *
     * @return {string} The place of the nav area, or no value if it doesn't have a place set.
     */
    public GetPlace(): string;

    /**
     * Returns a random CNavArea that has an outgoing ( one or two way ) connection from this CNavArea in given direction.
     *
     * @arg number navDir - The direction, in which to look for CNavAreas, see Enums/NavDir.
     * @return {CNavArea} The random CNavArea that has an outgoing ( one or two way ) connection from this CNavArea in given direction, if any.
     */
    public GetRandomAdjacentAreaAtSide(navDir: number): CNavArea;

    /**
     * Returns a random point on the nav area.
     *
     * @return {Vector} The random point on the nav area.
     */
    public GetRandomPoint(): Vector;

    /**
     * Returns the width this Nav Area.
     *
     * @return {number}
     */
    public GetSizeX(): number;

    /**
     * Returns the height of this Nav Area.
     *
     * @return {number}
     */
    public GetSizeY(): number;

    /**
     * Returns the total cost when passing from starting area to the goal area through this node. Set by CNavArea:SetTotalCost.
     *
     * @return {number} The total cost
     */
    public GetTotalCost(): number;

    /**
     * Returns the elevation of this Nav Area at the given position.
     *
     * @arg Vector pos - The position to get the elevation from, the z value from this position is ignored and only the X and Y values are used to this task.
     * @return {number} The elevation.
     */
    public GetZ(pos: Vector): number;

    /**
     * Returns true if the given CNavArea has this attribute flag set.
     *
     * @arg number attribs - Attribute mask to check for, see Enums/NAV_MESH
     * @return {boolean} True if the CNavArea matches the given mask. False otherwise.
     */
    public HasAttributes(attribs: number): boolean;

    /**
 * Returns whether the nav area is blocked or not, i.e. whether it can be walked through or not.
* 
* @arg number [teamID=-2] - The team ID to test, -2 = any team.
Only 2 actual teams are available, 0 and 1.
* @arg boolean [ignoreNavBlockers=false] - Whether to ignore func_nav_blocker entities.
* @return {boolean} Whether the area is blocked or not
 */
    public IsBlocked(teamID?: number, ignoreNavBlockers?: boolean): boolean;

    /**
     * Returns whether this node is in the Closed List.
     *
     * @return {boolean} Whether this node is in the Closed List.
     */
    public IsClosed(): boolean;

    /**
     * Returns whether this CNavArea can completely (i.e. all corners of this area can see all corners of the given area) see the given CNavArea.
     *
     * @arg CNavArea area - The CNavArea to test.
     * @return {boolean} Whether the given area is visible from this area
     */
    public IsCompletelyVisible(area: CNavArea): boolean;

    /**
     * Returns whether this CNavArea has an outgoing ( one or two way ) connection to given CNavArea.
     *
     * @arg CNavArea navArea - The CNavArea to test against.
     * @return {boolean} Whether this CNavArea has an outgoing ( one or two way ) connection to given CNavArea.
     */
    public IsConnected(navArea: CNavArea): boolean;

    /**
     * Returns whether this CNavArea has an outgoing ( one or two way ) connection to given CNavArea in given direction.
     *
     * @arg CNavArea navArea - The CNavArea to test against.
     * @arg number navDirType - The direction, in which to look for the connection. See Enums/NavDir
     * @return {boolean} Whether this CNavArea has an outgoing ( one or two way ) connection to given CNavArea in given direction.
     */
    public IsConnectedAtSide(navArea: CNavArea, navDirType: number): boolean;

    /**
     * Returns whether this Nav Area is in the same plane as the given one.
     *
     * @arg CNavArea navArea - The Nav Area to test.
     * @return {boolean} Whether we're coplanar or not.
     */
    public IsCoplanar(navArea: CNavArea): boolean;

    /**
     * Returns whether this Nav Area is flat within the tolerance of the nav_coplanar_slope_limit_displacement and nav_coplanar_slope_limit convars.
     *
     * @return {boolean} Whether this CNavArea is mostly flat.
     */
    public IsFlat(): boolean;

    /**
     * Returns whether this area is in the Open List.
     *
     * @return {boolean} Whether this area is in the Open List.
     */
    public IsOpen(): boolean;

    /**
     * Returns whether the Open List is empty or not.
     *
     * @return {boolean} Whether the Open List is empty or not.
     */
    public IsOpenListEmpty(): boolean;

    /**
     * Returns if this position overlaps the Nav Area within the given tolerance.
     *
     * @arg Vector pos - The overlapping position to test.
     * @arg number [tolerance=0] - The tolerance of the overlapping, set to 0 for no tolerance.
     * @return {boolean} Whether the given position overlaps the Nav Area or not.
     */
    public IsOverlapping(pos: Vector, tolerance?: number): boolean;

    /**
     * Returns true if this CNavArea is overlapping the given CNavArea.
     *
     * @arg CNavArea navArea - The CNavArea to test against.
     * @return {boolean} True if the given CNavArea overlaps this CNavArea at any point.
     */
    public IsOverlappingArea(navArea: CNavArea): boolean;

    /**
     * Returns whether this CNavArea can see given position.
     *
     * @arg Vector pos - The position to test.
     * @arg Entity [ignoreEnt=NULL] - If set, the given entity will be ignored when doing LOS tests
     * @return {boolean} Whether the given position is visible from this area
     */
    public IsPartiallyVisible(pos: Vector, ignoreEnt?: Entity): boolean;

    /**
     * Returns whether this CNavArea can potentially see the given CNavArea.
     *
     * @arg CNavArea area - The CNavArea to test.
     * @return {boolean} Whether the given area is visible from this area
     */
    public IsPotentiallyVisible(area: CNavArea): boolean;

    /**
     * Returns if we're shaped like a square.
     *
     * @return {boolean} If we're a square or not.
     */
    public IsRoughlySquare(): boolean;

    /**
     * Whether this Nav Area is placed underwater.
     *
     * @return {boolean} Whether we're underwater or not.
     */
    public IsUnderwater(): boolean;

    /**
     * Returns whether this CNavArea is valid or not.
     *
     * @return {boolean} Whether this CNavArea is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Returns whether we can be seen from the given position.
     *
     * @arg Vector pos - The position to check.
     * @return {boolean} Whether we can be seen or not.
     * @return {Vector} If we can be seen, this is returned with either the center or one of the corners of the Nav Area.
     */
    public IsVisible(pos: Vector): LuaMultiReturn<[boolean, Vector]>;

    /**
     * Drops a corner or all corners of a CNavArea to the ground below it.
     *
     * @arg number corner - The corner(s) to drop, uses Enums/NavCorner
     */
    public PlaceOnGround(corner: number): void;

    /**
     * Removes a CNavArea from the Open List with the lowest cost to traverse to from the starting node, and returns it.
     *
     * @return {CNavArea} The CNavArea from the Open List with the lowest cost to traverse to from the starting node.
     */
    public PopOpenList(): CNavArea;

    /**
     * Removes the given nav area.
     */
    public Remove(): void;

    /**
     * Removes this node from the Closed List.
     */
    public RemoveFromClosedList(): void;

    /**
     * Sets the attributes for given CNavArea.
     *
     * @arg number attribs - The attribute bitflag. See Enums/NAV_MESH
     */
    public SetAttributes(attribs: number): void;

    /**
     * Sets the position of a corner of a nav area.
     *
     * @arg number corner - The corner to set, uses Enums/NavCorner
     * @arg Vector position - The new position to set.
     */
    public SetCorner(corner: number, position: Vector): void;

    /**
     * Sets the cost from starting area this area when pathfinding.
     *
     * @arg number cost - The cost so far
     */
    public SetCostSoFar(cost: number): void;

    /**
     * Sets the new parent of this CNavArea.
     *
     * @arg CNavArea parent - The new parent to set
     * @arg number how - How we get from parent to us using Enums/NavTraverseType
     */
    public SetParent(parent: CNavArea, how: number): void;

    /**
     * Sets the Place of the nav area.
     *
     * @arg string place - Set to "" to remove place from the nav area.
     * @return {boolean} Returns true of operation succeeded, false otherwise.
     */
    public SetPlace(place: string): boolean;

    /**
 * Sets the total cost when passing from starting area to the goal area through this node.
* 
* @arg number cost - The total cost of the path to set.
Must be above or equal 0.
 */
    public SetTotalCost(cost: number): void;

    /**
     * Moves this open list to appropriate position based on its CNavArea:GetTotalCost compared to the total cost of other areas in the open list.
     */
    public UpdateOnOpenList(): void;
}

declare type CNavArea = __CNavAreaClass;
