declare class __MarkupObjectClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Draws the computed markupobject to the screen.
     *
     * @arg number xOffset - The X coordinate on the screen.
     * @arg number yOffset - The Y coordinate on the screen.
     * @arg number [xAlign=NaN] - The alignment of the x coordinate within the text using Enums/TEXT_ALIGN
     * @arg number [yAlign=NaN] - The alignment of the y coordinate within the text using Enums/TEXT_ALIGN
     * @arg number [alphaoverride=255] - Sets the alpha of all drawn objects to this value.
     * @arg number [textAlign=NaN] - The alignment of the text horizontally using Enums/TEXT_ALIGN
     */
    public Draw(
        xOffset: number,
        yOffset: number,
        xAlign?: number,
        yAlign?: number,
        alphaoverride?: number,
        textAlign?: number
    ): void;

    /**
     * Gets computed the height of the markupobject.
     *
     * @return {number} The computed height.
     */
    public GetHeight(): number;

    /**
     * Gets maximum width for this markup object as defined in markup.Parse.
     *
     * @return {number} The max width.
     */
    public GetMaxWidth(): number;

    /**
     * Gets computed the width of the markupobject.
     *
     * @return {number} The computed width.
     */
    public GetWidth(): number;

    /**
     * Gets computed the width and height of the markupobject.
     *
     * @return {number} The computed width.
     * @return {number} The computed height.
     */
    public Size(): LuaMultiReturn<[number, number]>;
}

declare type MarkupObject = __MarkupObjectClass;
