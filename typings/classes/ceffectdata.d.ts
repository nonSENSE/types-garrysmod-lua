declare class __CEffectDataClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the angles of the effect.
     *
     * @return {Angle} The angles of the effect
     */
    public GetAngles(): Angle;

    /**
     * Returns the attachment ID for the effect.
     *
     * @return {number} The attachment ID of the effect.
     */
    public GetAttachment(): number;

    /**
     * Returns byte which represents the color of the effect.
     *
     * @return {number} The color of the effect
     */
    public GetColor(): number;

    /**
     * Returns the damage type of the effect
     *
     * @return {number} Damage type of the effect, see Enums/DMG
     */
    public GetDamageType(): number;

    /**
     * Returns the entity index of the entity set for the effect.
     *
     * @return {number} The entity index of the entity set for the effect.
     */
    public GetEntIndex(): number;

    /**
     * Returns the entity assigned to the effect.
     *
     * @return {Entity} The entity assigned to the effect
     */
    public GetEntity(): Entity;

    /**
     * Returns the flags of the effect.
     *
     * @return {number} The flags of the effect.
     */
    public GetFlags(): number;

    /**
     * Returns the hit box ID of the effect.
     *
     * @return {number} The hit box ID of the effect.
     */
    public GetHitBox(): number;

    /**
     * Returns the magnitude of the effect.
     *
     * @return {number} The magnitude of the effect.
     */
    public GetMagnitude(): number;

    /**
     * Returns the material ID of the effect.
     *
     * @return {number} The material ID of the effect.
     */
    public GetMaterialIndex(): number;

    /**
     * Returns the normalized direction vector of the effect.
     *
     * @return {Vector} The normalized direction vector of the effect.
     */
    public GetNormal(): Vector;

    /**
     * Returns the origin position of the effect.
     *
     * @return {Vector} The origin position of the effect.
     */
    public GetOrigin(): Vector;

    /**
     * Returns the radius of the effect.
     *
     * @return {number} The radius of the effect.
     */
    public GetRadius(): number;

    /**
     * Returns the scale of the effect.
     *
     * @return {number} The scale of the effect
     */
    public GetScale(): number;

    /**
     * Returns the start position of the effect.
     *
     * @return {Vector} The start position of the effect
     */
    public GetStart(): Vector;

    /**
     * Returns the surface property index of the effect.
     *
     * @return {number} The surface property index of the effect
     */
    public GetSurfaceProp(): number;

    /**
     * Sets the angles of the effect.
     *
     * @arg Angle ang - The new angles to be set.
     */
    public SetAngles(ang: Angle): void;

    /**
     * Sets the attachment id of the effect to be created with this effect data.
     *
     * @arg number attachment - New attachment ID of the effect.
     */
    public SetAttachment(attachment: number): void;

    /**
     * Sets the "color" of the effect.
     *
     * @arg number color - Color represented by a byte.
     */
    public SetColor(color: number): void;

    /**
     * Sets the damage type of the effect to be created with this effect data.
     *
     * @arg number damageType - Damage type, see Enums/DMG.
     */
    public SetDamageType(damageType: number): void;

    /**
     * Sets the entity of the effect via its index.
     *
     * @arg number entIndex - The entity index to be set.
     */
    public SetEntIndex(entIndex: number): void;

    /**
     * Sets the entity of the effect to be created with this effect data.
     *
     * @arg Entity entity - Entity of the effect, mostly used for parenting.
     */
    public SetEntity(entity: Entity): void;

    /**
     * Sets the flags of the effect. Can be used to change the appearance of a MuzzleFlash effect.
     *
     * @arg number flags - The flags of the effect. Each effect has their own flags.
     */
    public SetFlags(flags: number): void;

    /**
     * Sets the hit box index of the effect.
     *
     * @arg number hitBoxIndex - The hit box index of the effect.
     */
    public SetHitBox(hitBoxIndex: number): void;

    /**
     * Sets the magnitude of the effect.
     *
     * @arg number magnitude - The magnitude of the effect.
     */
    public SetMagnitude(magnitude: number): void;

    /**
     * Sets the material index of the effect.
     *
     * @arg number materialIndex - The material index of the effect.
     */
    public SetMaterialIndex(materialIndex: number): void;

    /**
     * Sets the normalized (length=1) direction vector of the effect to be created with this effect data. This must be a normalized vector for networking purposes.
     *
     * @arg Vector normal - The normalized direction vector of the effect.
     */
    public SetNormal(normal: Vector): void;

    /**
     * Sets the origin of the effect to be created with this effect data.
     *
     * @arg Vector origin - Origin of the effect.
     */
    public SetOrigin(origin: Vector): void;

    /**
     * Sets the radius of the effect to be created with this effect data.
     *
     * @arg number radius - Radius of the effect.
     */
    public SetRadius(radius: number): void;

    /**
     * Sets the scale of the effect to be created with this effect data.
     *
     * @arg number scale - Scale of the effect.
     */
    public SetScale(scale: number): void;

    /**
     * Sets the start of the effect to be created with this effect data.
     *
     * @arg Vector start - Start of the effect.
     */
    public SetStart(start: Vector): void;

    /**
     * Sets the surface property index of the effect.
     *
     * @arg number surfaceProperties - The surface property index of the effect.
     */
    public SetSurfaceProp(surfaceProperties: number): void;
}

declare type CEffectData = __CEffectDataClass;
