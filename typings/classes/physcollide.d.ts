declare class __PhysCollideClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Destroys the PhysCollide object.
     */
    public Destroy(): void;

    /**
     * Checks whether this PhysCollide object is valid or not.
     *
     * @return {boolean} Is valid or not.
     */
    public IsValid(): boolean;

    /**
     * Performs a trace against this PhysCollide with the given parameters. This can be used for both line traces and box traces.
     *
     * @arg Vector origin - The origin for the PhysCollide during the trace
     * @arg Angle angles - The angles for the PhysCollide during the trace
     * @arg Vector rayStart - The start position of the trace
     * @arg Vector rayEnd - The end position of the trace
     * @arg Vector rayMins - The mins of the trace's bounds
     * @arg Vector rayMaxs - The maxs of the trace's bounds
     * @return {Vector} Hit position of the trace. This is false if the trace did not hit.
     * @return {Vector} Hit normal of the trace
     * @return {number} Fraction of the trace. This is calculated from the distance between startPos, hitPos, and endPos.
     */
    public TraceBox(
        origin: Vector,
        angles: Angle,
        rayStart: Vector,
        rayEnd: Vector,
        rayMins: Vector,
        rayMaxs: Vector
    ): LuaMultiReturn<[Vector, Vector, number]>;
}

declare type PhysCollide = __PhysCollideClass;
