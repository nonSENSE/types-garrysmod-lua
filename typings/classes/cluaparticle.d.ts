declare class __CLuaParticleClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the air resistance of the particle.
     *
     * @return {number} The air resistance of the particle
     */
    public GetAirResistance(): number;

    /**
     * Returns the current orientation of the particle.
     *
     * @return {Angle} The angles of the particle
     */
    public GetAngles(): Angle;

    /**
     * Returns the angular velocity of the particle
     *
     * @return {Angle} The angular velocity of the particle
     */
    public GetAngleVelocity(): Angle;

    /**
 * Returns the 'bounciness' of the particle.
* 
* @return {number} The 'bounciness' of the particle
2 means it will gain 100% of its previous velocity,
1 means it will not lose velocity,
0.5 means it will lose half of its velocity with each bounce.
 */
    public GetBounce(): number;

    /**
     * Returns the color of the particle.
     *
     * @return {number} Red part of the color
     * @return {number} Green part of the color
     * @return {number} Blue part of the color
     */
    public GetColor(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns the amount of time in seconds after which the particle will be destroyed.
     *
     * @return {number} The amount of time in seconds after which the particle will be destroyed
     */
    public GetDieTime(): number;

    /**
     * Returns the alpha value that the particle will reach on its death.
     *
     * @return {number} The alpha value the particle will fade to
     */
    public GetEndAlpha(): number;

    /**
     * Returns the length that the particle will reach on its death.
     *
     * @return {number} The length the particle will reach
     */
    public GetEndLength(): number;

    /**
     * Returns the size that the particle will reach on its death.
     *
     * @return {number} The size the particle will reach
     */
    public GetEndSize(): number;

    /**
     * Returns the gravity of the particle.
     *
     * @return {Vector} The gravity of the particle.
     */
    public GetGravity(): Vector;

    /**
     * Returns the 'life time' of the particle, how long the particle existed since its creation.
     *
     * @return {number} How long the particle existed, in seconds.
     */
    public GetLifeTime(): number;

    /**
     * Returns the current material of the particle.
     *
     * @return {IMaterial} The material.
     */
    public GetMaterial(): IMaterial;

    /**
     * Returns the absolute position of the particle.
     *
     * @return {Vector} The absolute position of the particle.
     */
    public GetPos(): Vector;

    /**
     * Returns the current rotation of the particle in radians, this should only be used for 2D particles.
     *
     * @return {number} The current rotation of the particle in radians
     */
    public GetRoll(): number;

    /**
     * Returns the current rotation speed of the particle in radians, this should only be used for 2D particles.
     *
     * @return {number} The current rotation speed of the particle in radians
     */
    public GetRollDelta(): number;

    /**
     * Returns the alpha value which the particle has when it's created.
     *
     * @return {number} The alpha value which the particle has when it's created.
     */
    public GetStartAlpha(): number;

    /**
     * Returns the length which the particle has when it's created.
     *
     * @return {number} The length which the particle has when it's created.
     */
    public GetStartLength(): number;

    /**
     * Returns the size which the particle has when it's created.
     *
     * @return {number} The size which the particle has when it's created.
     */
    public GetStartSize(): number;

    /**
     * Returns the current velocity of the particle.
     *
     * @return {Vector} The current velocity of the particle.
     */
    public GetVelocity(): Vector;

    /**
     * Sets the air resistance of the the particle.
     *
     * @arg number airResistance - New air resistance.
     */
    public SetAirResistance(airResistance: number): void;

    /**
     * Sets the angles of the particle.
     *
     * @arg Angle ang - New angle.
     */
    public SetAngles(ang: Angle): void;

    /**
     * Sets the angular velocity of the the particle.
     *
     * @arg Angle angVel - New angular velocity.
     */
    public SetAngleVelocity(angVel: Angle): void;

    /**
 * Sets the 'bounciness' of the the particle.
* 
* @arg number bounce - New 'bounciness' of the particle
2 means it will gain 100% of its previous velocity,
1 means it will not lose velocity,
0.5 means it will lose half of its velocity with each bounce.
 */
    public SetBounce(bounce: number): void;

    /**
     * Sets the whether the particle should collide with the world or not.
     *
     * @arg boolean shouldCollide - Whether the particle should collide with the world or not
     */
    public SetCollide(shouldCollide: boolean): void;

    /**
 * Sets the function that gets called whenever the particle collides with the world.
* 
* @arg GMLua.CallbackNoContext collideFunc - Collide callback, the arguments are:
CLuaParticle particle - The particle itself
Vector hitPos - Position of the collision
Vector hitNormal - Direction of the collision, perpendicular to the hit surface
 */
    public SetCollideCallback(collideFunc: GMLua.CallbackNoContext): void;

    /**
     * Sets the color of the particle.
     *
     * @arg number r - The red component.
     * @arg number g - The green component.
     * @arg number b - The blue component.
     */
    public SetColor(r: number, g: number, b: number): void;

    /**
     * Sets the time where the particle will be removed.
     *
     * @arg number dieTime - The new die time.
     */
    public SetDieTime(dieTime: number): void;

    /**
     * Sets the alpha value of the particle that it will reach when it dies.
     *
     * @arg number endAlpha - The new alpha value of the particle that it will reach when it dies.
     */
    public SetEndAlpha(endAlpha: number): void;

    /**
     * Sets the length of the particle that it will reach when it dies.
     *
     * @arg number endLength - The new length of the particle that it will reach when it dies.
     */
    public SetEndLength(endLength: number): void;

    /**
     * Sets the size of the particle that it will reach when it dies.
     *
     * @arg number endSize - The new size of the particle that it will reach when it dies.
     */
    public SetEndSize(endSize: number): void;

    /**
     * Sets the directional gravity aka. acceleration of the particle.
     *
     * @arg Vector gravity - The directional gravity.
     */
    public SetGravity(gravity: Vector): void;

    /**
     * Sets the 'life time' of the particle, how long the particle existed since its creation.
     *
     * @arg number lifeTime - The new life time of the particle.
     */
    public SetLifeTime(lifeTime: number): void;

    /**
     * Sets whether the particle should be affected by lighting.
     *
     * @arg boolean useLighting - Whether the particle should be affected by lighting.
     */
    public SetLighting(useLighting: boolean): void;

    /**
     * Sets the material of the particle.
     *
     * @arg IMaterial mat - The new material to set. Can also be a string.
     */
    public SetMaterial(mat: IMaterial): void;

    /**
     * Sets when the particles think function should be called next, this uses the synchronized server time returned by CurTime.
     *
     * @arg number nextThink - Next think time.
     */
    public SetNextThink(nextThink: number): void;

    /**
     * Sets the absolute position of the particle.
     *
     * @arg Vector pos - The new particle position.
     */
    public SetPos(pos: Vector): void;

    /**
     * Sets the roll of the particle in radians. This should only be used for 2D particles.
     *
     * @arg number roll - The new rotation of the particle in radians.
     */
    public SetRoll(roll: number): void;

    /**
     * Sets the rotation speed of the particle in radians. This should only be used for 2D particles.
     *
     * @arg number rollDelta - The new rotation speed of the particle in radians.
     */
    public SetRollDelta(rollDelta: number): void;

    /**
     * Sets the initial alpha value of the particle.
     *
     * @arg number startAlpha - Initial alpha.
     */
    public SetStartAlpha(startAlpha: number): void;

    /**
     * Sets the initial length value of the particle.
     *
     * @arg number startLength - Initial length.
     */
    public SetStartLength(startLength: number): void;

    /**
     * Sets the initial size value of the particle.
     *
     * @arg number startSize - Initial size.
     */
    public SetStartSize(startSize: number): void;

    /**
 * Sets the think function of the particle.
* 
* @arg GMLua.CallbackNoContext thinkFunc - Think function. It has only one argument:
CLuaParticle particle - The particle the think hook is set on
 */
    public SetThinkFunction(thinkFunc: GMLua.CallbackNoContext): void;

    /**
     * Sets the velocity of the particle.
     *
     * @arg Vector vel - The new velocity of the particle.
     */
    public SetVelocity(vel: Vector): void;

    /**
     * Scales the velocity based on the particle speed.
     *
     * @arg boolean [doScale=false] - Use velocity scaling.
     */
    public SetVelocityScale(doScale?: boolean): void;
}

declare type CLuaParticle = __CLuaParticleClass;
