declare class __CUserCmdClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds a single key to the active buttons bitflag. See also CUserCmd:SetButtons.
     *
     * @arg number key - Key to add, see Enums/IN.
     */
    public AddKey(key: number): void;

    /**
     * Removes all keys from the command.
     */
    public ClearButtons(): void;

    /**
     * Clears the movement from the command.
     */
    public ClearMovement(): void;

    /**
     * Returns an increasing number representing the index of the user cmd.
     *
     * @return {number} The command number
     */
    public CommandNumber(): number;

    /**
     * Returns a bitflag indicating which buttons are pressed.
     *
     * @return {number} Pressed buttons, see Enums/IN
     */
    public GetButtons(): number;

    /**
     * The speed the client wishes to move forward with, negative if the clients wants to move backwards.
     *
     * @return {number} The desired speed
     */
    public GetForwardMove(): number;

    /**
     * Gets the current impulse from the client, usually 0. See impulses list and some GMod specific impulses.
     *
     * @return {number} The impulse
     */
    public GetImpulse(): number;

    /**
     * Returns the scroll delta as whole number.
     *
     * @return {number} Scroll delta
     */
    public GetMouseWheel(): number;

    /**
     * Returns the delta of the angular horizontal mouse movement of the player.
     *
     * @return {number} xDelta
     */
    public GetMouseX(): number;

    /**
     * Returns the delta of the angular vertical mouse movement of the player.
     *
     * @return {number} yDelta
     */
    public GetMouseY(): number;

    /**
     * The speed the client wishes to move sideways with, positive if it wants to move right, negative if it wants to move left.
     *
     * @return {number} requestSpeed
     */
    public GetSideMove(): number;

    /**
     * The speed the client wishes to move up with, negative if the clients wants to move down.
     *
     * @return {number} requestSpeed
     */
    public GetUpMove(): number;

    /**
     * Gets the direction the player is looking in.
     *
     * @return {Angle} The direction the player is looking in.
     */
    public GetViewAngles(): Angle;

    /**
     * When players are not sending usercommands to the server (often due to lag), their last usercommand will be executed multiple times as a backup. This function returns true if that is happening.
     *
     * @return {boolean} isForced
     */
    public IsForced(): boolean;

    /**
     * Returns true if the specified button(s) is pressed.
     *
     * @arg number key - Bitflag representing which button to check, see Enums/IN.
     * @return {boolean} Is key down or not
     */
    public KeyDown(key: number): boolean;

    /**
     * Removes a key bit from the current key bitflag.
     *
     * @arg number button - Bitflag to be removed from the key bitflag, see Enums/IN.
     */
    public RemoveKey(button: number): void;

    /**
     * Forces the associated player to select a weapon. This is used internally in the default HL2 weapon selection HUD.
     *
     * @arg Weapon weapon - The weapon entity to select.
     */
    public SelectWeapon(weapon: Weapon): void;

    /**
     * Sets the buttons as a bitflag. See also CUserCmd:GetButtons.
     *
     * @arg number buttons - Bitflag representing which buttons are "down", see Enums/IN.
     */
    public SetButtons(buttons: number): void;

    /**
     * Sets speed the client wishes to move forward with, negative if the clients wants to move backwards.
     *
     * @arg number speed - The new speed to request. The client will not be able to move faster than their set walk/sprint speed.
     */
    public SetForwardMove(speed: number): void;

    /**
     * Sets the impulse command to be sent to the server.
     *
     * @arg number impulse - The impulse to send.
     */
    public SetImpulse(impulse: number): void;

    /**
     * Sets the scroll delta.
     *
     * @arg number speed - The scroll delta.
     */
    public SetMouseWheel(speed: number): void;

    /**
     * Sets the delta of the angular horizontal mouse movement of the player.
     *
     * @arg number speed - Angular horizontal move delta.
     */
    public SetMouseX(speed: number): void;

    /**
     * Sets the delta of the angular vertical mouse movement of the player.
     *
     * @arg number speed - Angular vertical move delta.
     */
    public SetMouseY(speed: number): void;

    /**
     * Sets speed the client wishes to move sidewards with, positive to move right, negative to move left.
     *
     * @arg number speed - The new speed to request.
     */
    public SetSideMove(speed: number): void;

    /**
     * Sets speed the client wishes to move upwards with, negative to move down.
     *
     * @arg number speed - The new speed to request.
     */
    public SetUpMove(speed: number): void;

    /**
     * Sets the direction the client wants to move in.
     *
     * @arg Angle viewAngle - New view angles.
     */
    public SetViewAngles(viewAngle: Angle): void;

    /**
     * Returns tick count since joining the server.
     *
     * @return {number} The amount of ticks passed since joining the server.
     */
    public TickCount(): number;
}

declare type CUserCmd = __CUserCmdClass;
