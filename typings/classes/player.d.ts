declare class __PlayerClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the player's AccountID aka SteamID3. ([U:1:AccountID])
     *
     * @return {number} The AccountID of Player's SteamID.
     */
    public AccountID(): number;

    /**
     * Adds an entity to the players clean up list.
     *
     * @arg string type - Cleanup type
     * @arg Entity ent - Entity to add
     */
    public AddCleanup(type: string, ent: Entity): void;

    /**
     * Adds an entity to the total count of entities of same class.
     *
     * @arg string str - Entity type
     * @arg Entity ent - Entity
     */
    public AddCount(str: string, ent: Entity): void;

    /**
     * Add a certain amount to the player's death count
     *
     * @arg number count - number of deaths to add
     */
    public AddDeaths(count: number): void;

    /**
     * Add a certain amount to the player's frag count (or kills count)
     *
     * @arg number count - number of frags to add
     */
    public AddFrags(count: number): void;

    /**
     * Adds a entity to the players list of frozen objects.
     *
     * @arg Entity ent - Entity
     * @arg PhysObj physobj - Physics object belonging to ent
     */
    public AddFrozenPhysicsObject(ent: Entity, physobj: PhysObj): void;

    /**
     * Sets up the voting system for the player.
     * This is a really barebone system. By calling this a vote gets started, when the player presses 0-9 the callback function gets called along with the key the player pressed. Use the draw callback to draw the vote panel.
     *
     * @arg string name - Name of the vote
     * @arg number timeout - Time until the vote expires
     * @arg GMLua.CallbackNoContext vote_callback - The function to be run when the player presses 0-9 while a vote is active.
     * @arg GMLua.CallbackNoContext draw_callback - Used to draw the vote panel.
     */
    public AddPlayerOption(
        name: string,
        timeout: number,
        vote_callback: GMLua.CallbackNoContext,
        draw_callback: GMLua.CallbackNoContext
    ): void;

    /**
     * Plays a sequence directly from a sequence number, similar to Player:AnimRestartGesture. This function has the advantage to play sequences that haven't been bound to an existing Enums/ACT
     *
     * @arg number slot - Gesture slot using Enums/GESTURE_SLOT
     * @arg number sequenceId - The sequence ID to play, can be retrieved with Entity:LookupSequence.
     * @arg number cycle - The cycle to start the animation at, ranges from 0 to 1.
     * @arg boolean [autokill=false] - If the animation should not loop. true = stops the animation, false = the animation keeps playing.
     */
    public AddVCDSequenceToGestureSlot(slot: number, sequenceId: number, cycle: number, autokill?: boolean): void;

    /**
     * Checks if the player is alive.
     *
     * @return {boolean} Whether the player is alive
     */
    public Alive(): boolean;

    /**
     * Sets if the player can toggle his flashlight. Function exists on both the server and client but has no effect when ran on the client.
     *
     * @arg boolean canFlashlight - True allows flashlight toggling
     */
    public AllowFlashlight(canFlashlight: boolean): void;

    /**
     * Lets the player spray their decal without delay
     *
     * @arg boolean allow - Allow or disallow
     */
    public AllowImmediateDecalPainting(allow: boolean): void;

    /**
     * Resets player gesture in selected slot.
     *
     * @arg number slot - Slot to reset. See the Enums/GESTURE_SLOT.
     */
    public AnimResetGestureSlot(slot: number): void;

    /**
     * Restart a gesture on a player, within a gesture slot.
     *
     * @arg number slot - Gesture slot using Enums/GESTURE_SLOT
     * @arg number activity - The activity ( see Enums/ACT ) or sequence that should be played
     * @arg boolean [autokill=false] - Whether the animation should be automatically stopped. true = stops the animation, false = the animation keeps playing/looping
     */
    public AnimRestartGesture(slot: number, activity: number, autokill?: boolean): void;

    /**
     * Restarts the main animation on the player, has the same effect as calling Entity:SetCycle( 0 ).
     */
    public AnimRestartMainSequence(): void;

    /**
     * Sets the sequence of the animation playing in the given gesture slot.
     *
     * @arg number slot - The gesture slot. See Enums/GESTURE_SLOT
     * @arg number sequenceID - Sequence ID to set.
     */
    public AnimSetGestureSequence(slot: number, sequenceID: number): void;

    /**
     * Sets the weight of the animation playing in the given gesture slot.
     *
     * @arg number slot - The gesture slot. See Enums/GESTURE_SLOT
     * @arg number weight - The weight this slot should be set to. Value must be ranging from 0 to 1.
     */
    public AnimSetGestureWeight(slot: number, weight: number): void;

    /**
     * Returns the player's armor.
     *
     * @return {number} The player's armor.
     */
    public Armor(): number;

    /**
     * Bans the player from the server for a certain amount of minutes.
     *
     * @arg number minutes - Duration of the ban in minutes (0 is permanent)
     * @arg boolean [kick=false] - Whether to kick the player after banning them or not
     */
    public Ban(minutes: number, kick?: boolean): void;

    /**
     * Returns true if the player's flashlight hasn't been disabled by Player:AllowFlashlight.
     *
     * @return {boolean} Whether the player can use flashlight.
     */
    public CanUseFlashlight(): boolean;

    /**
     * Prints a string to the chatbox of the client.
     *
     * @arg string message - String to be printed
     */
    public ChatPrint(message: string): void;

    /**
 * Checks if the limit is hit or not. If it is, it will throw a notification saying so.
* 
* @arg string limitType - Limit type. In unmodified Sandbox possible values are:

"props"
"ragdolls"
"vehicles"
"effects"
"balloons"
"cameras"
"npcs"
"sents"
"dynamite"
"lamps"
"lights"
"wheels"
"thrusters"
"hoverballs"
"buttons"
"emitters"
* @return {boolean} Returns true if limit is not hit, false if it is hit
 */
    public CheckLimit(limitType: string): boolean;

    /**
     * Runs the concommand on the player. This does not work on bots.
     *
     * @arg string command - command to run
     */
    public ConCommand(command: string): void;

    /**
     * Creates the player's death ragdoll entity and deletes the old one.
     */
    public CreateRagdoll(): void;

    /**
     * Disables the default player's crosshair. Can be reenabled with Player:CrosshairEnable. This will affect WEAPON:DoDrawCrosshair.
     */
    public CrosshairDisable(): void;

    /**
     * Enables the player's crosshair, if it was previously disabled via Player:CrosshairDisable.
     */
    public CrosshairEnable(): void;

    /**
     * Returns whether the player is crouching or not (FL_DUCKING flag).
     *
     * @return {boolean} Whether the player is crouching.
     */
    public Crouching(): boolean;

    /**
     * Returns the player's death count
     *
     * @return {number} The number of deaths the player has had.
     */
    public Deaths(): number;

    /**
     * Prints the players' name and position to the console.
     */
    public DebugInfo(): void;

    /**
     * Detonates all tripmines belonging to the player.
     */
    public DetonateTripmines(): void;

    /**
     * Sends a third person animation event to the player.
     *
     * @arg number data - The data to send.
     */
    public DoAnimationEvent(data: number): void;

    /**
     * Starts the player's attack animation. The attack animation is determined by the weapon's HoldType.
     */
    public DoAttackEvent(): void;

    /**
     * Sends a specified third person animation event to the player.
     *
     * @arg number event - The event to send. See Enums/PLAYERANIMEVENT.
     * @arg number data - The data to send alongside the event.
     */
    public DoCustomAnimEvent(event: number, data: number): void;

    /**
     * Sends a third person reload animation event to the player.
     */
    public DoReloadEvent(): void;

    /**
     * Sends a third person secondary fire animation event to the player.
     */
    public DoSecondaryAttack(): void;

    /**
     * Show/Hide the player's weapon's viewmodel.
     *
     * @arg boolean draw - Should draw
     * @arg number [vm=0] - Which view model to show/hide, 0-2.
     */
    public DrawViewModel(draw: boolean, vm?: number): void;

    /**
     * Show/Hide the player's weapon's worldmodel.
     *
     * @arg boolean draw - Should draw
     */
    public DrawWorldModel(draw: boolean): void;

    /**
     * Drops the players' weapon of a specific class.
     *
     * @arg string _class - The class to drop.
     * @arg Vector [target=nil] - If set, launches the weapon at given position. There is a limit to how far it is willing to throw the weapon. Overrides velocity argument.
     * @arg Vector [velocity=nil] - If set and previous argument is unset, launches the weapon with given velocity. If the velocity is higher than 400, it will be clamped to 400.
     */
    public DropNamedWeapon(_class: string, target?: Vector, velocity?: Vector): void;

    /**
     * Drops any object the player is currently holding with either gravitygun or +Use (E key)
     */
    public DropObject(): void;

    /**
     * Forces the player to drop the specified weapon
     *
     * @arg Weapon [weapon=nil] - Weapon to be dropped. If unset, will default to the currently equipped weapon.
     * @arg Vector [target=nil] - If set, launches the weapon at given position. There is a limit to how far it is willing to throw the weapon. Overrides velocity argument.
     * @arg Vector [velocity=nil] - If set and previous argument is unset, launches the weapon with given velocity. If the velocity is higher than 400, it will be clamped to 400.
     */
    public DropWeapon(weapon?: Weapon, target?: Vector, velocity?: Vector): void;

    /**
     * Enters the player into specified vehicle
     *
     * @arg Vehicle vehicle - Vehicle the player will enter
     */
    public EnterVehicle(vehicle: Vehicle): void;

    /**
     * Equips the player with the HEV suit.
     */
    public EquipSuit(): void;

    /**
     * Makes the player exit the vehicle if they're in one.
     */
    public ExitVehicle(): void;

    /**
     * Enables/Disables the player's flashlight.Player:CanUseFlashlight must be true in order for the player's flashlight to be changed.
     *
     * @arg boolean isOn - Turns the flashlight on/off
     */
    public Flashlight(isOn: boolean): void;

    /**
     * Returns true if the player's flashlight is on.
     *
     * @return {boolean} Whether the player's flashlight is on.
     */
    public FlashlightIsOn(): boolean;

    /**
     * Returns the amount of kills a player has.
     *
     * @return {number} kills
     */
    public Frags(): number;

    /**
     * Freeze the player. Frozen players cannot move, look around, or attack. Key bindings are still called. Similar to Player:Lock but the player can still take damage.
     *
     * @arg boolean [frozen=false] - Whether the player should be frozen.
     */
    public Freeze(frozen?: boolean): void;

    /**
     * Returns the player's active weapon.
     *
     * @return {Weapon} The weapon the player currently has equipped.
     */
    public GetActiveWeapon(): Weapon;

    /**
     * Returns the player's current activity.
     *
     * @return {number} The player's current activity. See Enums/ACT.
     */
    public GetActivity(): number;

    /**
     * Returns the direction that the player is aiming.
     *
     * @return {Vector} The direction vector of players aim
     */
    public GetAimVector(): Vector;

    /**
     * Returns true if the players' model is allowed to rotate around the pitch and roll axis.
     *
     * @return {boolean} Allowed
     */
    public GetAllowFullRotation(): boolean;

    /**
     * Returns whether the player is allowed to use his weapons in a vehicle or not.
     *
     * @return {boolean} Whether the player is allowed to use his weapons in a vehicle or not.
     */
    public GetAllowWeaponsInVehicle(): boolean;

    /**
 * Returns a table of all ammo the player has.
* 
* @return {number} A table with the following format

number Key - AmmoID to be used with functions like game.GetAmmoName.
number Value - Amount of ammo the player has of this kind.
 */
    public GetAmmo(): number;

    /**
     * Gets the amount of ammo the player has.
     *
     * @arg any ammotype - The ammunition type. Can be either number ammo ID or string ammo name.
     * @return {number} The amount of ammo player has in reserve.
     */
    public GetAmmoCount(ammotype: any): number;

    /**
     * Gets if the player will be pushed out of nocollided players.
     *
     * @return {boolean} pushed
     */
    public GetAvoidPlayers(): boolean;

    /**
     * Returns true if the player is able to walk using the (default) alt key.
     *
     * @return {boolean} AbleToWalk
     */
    public GetCanWalk(): boolean;

    /**
     * Determines whenever the player is allowed to use the zoom functionality.
     *
     * @return {boolean} canZoom
     */
    public GetCanZoom(): boolean;

    /**
     * Returns the player's class id.
     *
     * @return {number} The player's class id.
     */
    public GetClassID(): number;

    /**
     * Gets total count of entities of same class.
     *
     * @arg string _class - Entity class to get count of.
     * @arg number [minus=0] - If specified, it will reduce the counter by this value. Works only serverside.
     * @return {number} The returned count.
     */
    public GetCount(_class: string, minus?: number): number;

    /**
     * Returns the crouched walk speed multiplier.
     *
     * @return {number} The crouched walk speed multiplier.
     */
    public GetCrouchedWalkSpeed(): number;

    /**
     * Returns the last command which was sent by the specified player. This can only be called on the player which GetPredictionPlayer() returns.
     *
     * @return {CUserCmd} Last user commands
     */
    public GetCurrentCommand(): CUserCmd;

    /**
     * Gets the actual view offset which equals the difference between the players actual position and their view when standing.
     *
     * @return {Vector} The actual view offset.
     */
    public GetCurrentViewOffset(): Vector;

    /**
     * Gets the entity the player is currently driving via the drive library.
     *
     * @return {Entity} The currently driven entity, or NULL entity
     */
    public GetDrivingEntity(): Entity;

    /**
     * Returns driving mode of the player. See Entity Driving.
     *
     * @return {number} The drive mode ID or 0 if player doesn't use the drive system.
     */
    public GetDrivingMode(): number;

    /**
     * Returns a player's duck speed (in seconds)
     *
     * @return {number} duckspeed
     */
    public GetDuckSpeed(): number;

    /**
     * Returns the entity the player is currently using, like func_tank mounted turrets or +use prop pickups.
     *
     * @return {Entity} Entity in use, or NULL entity otherwise. For +use prop pickups, this will be NULL clientside.
     */
    public GetEntityInUse(): Entity;

    /**
     * Returns a table with information of what the player is looking at.
     *
     * @return {TraceResultStruct} Trace information, see Structures/TraceResult.
     */
    public GetEyeTrace(): TraceResultStruct;

    /**
     * Returns the trace according to the players view direction, ignoring their mouse (holding c and moving the mouse in Sandbox).
     *
     * @return {TraceResultStruct} Trace result. See Structures/TraceResult.
     */
    public GetEyeTraceNoCursor(): TraceResultStruct;

    /**
     * Returns the FOV of the player.
     *
     * @return {number} Field of view as a float
     */
    public GetFOV(): number;

    /**
     * Returns the steam "relationship" towards the player.
     *
     * @return {string} Should return one of four different things depending on their status on your friends list: "friend", "blocked", "none" or "requested".
     */
    public GetFriendStatus(): string;

    /**
     * Gets the hands entity of a player
     *
     * @return {Entity} The hands entity if players has one
     */
    public GetHands(): Entity;

    /**
     * Returns the widget the player is hovering with his mouse.
     *
     * @return {Entity} The hovered widget.
     */
    public GetHoveredWidget(): Entity;

    /**
     * Gets the bottom base and the top base size of the player's hull.
     *
     * @return {Vector} Player's hull bottom base size.
     * @return {Vector} Player's hull top base size.
     */
    public GetHull(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Gets the bottom base and the top base size of the player's crouch hull.
     *
     * @return {Vector} Player's crouch hull bottom base size.
     * @return {Vector} Player's crouch hull top base size.
     */
    public GetHullDuck(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Retrieves the value of a client-side ConVar. The ConVar must have a FCVAR_USERINFO flag for this to work.
     *
     * @arg string cVarName - The name of the client-side ConVar.
     * @return {string} The value of the ConVar.
     */
    public GetInfo(cVarName: string): string;

    /**
     * Retrieves the numeric value of a client-side convar, returns nil if value is not convertible to a number. The ConVar must have a FCVAR_USERINFO flag for this to work.
     *
     * @arg string cVarName - The name of the ConVar to query the value of
     * @arg number def - Default value if we failed to retrieve the number.
     * @return {number} The value of the ConVar or the default value
     */
    public GetInfoNum(cVarName: string, def: number): number;

    /**
     * Returns the jump power of the player
     *
     * @return {number} Jump power
     */
    public GetJumpPower(): number;

    /**
     * Returns the player's ladder climbing speed.
     *
     * @return {number} The ladder climbing speed.
     */
    public GetLadderClimbSpeed(): number;

    /**
     * Returns the timescale multiplier of the player movement.
     *
     * @return {number} The timescale multiplier, defaults to 1.
     */
    public GetLaggedMovementValue(): number;

    /**
     * Returns the maximum amount of armor the player should have. Default value is 100.
     *
     * @return {number} The new max armor value
     */
    public GetMaxArmor(): number;

    /**
     * Returns the player's maximum movement speed.
     *
     * @return {number} The maximum movement speed the player can go at.
     */
    public GetMaxSpeed(): number;

    /**
     * Returns the player's name, this is an alias of Player:Nick.
     *
     * @return {string} The player's name.
     */
    public GetName(): string;

    /**
     * Returns whenever the player is set not to collide with their teammates.
     *
     * @return {boolean} noCollideWithTeammates
     */
    public GetNoCollideWithTeammates(): boolean;

    /**
     * Returns the the observer mode of the player
     *
     * @return {number} Observe mode of that player, see Enums/OBS_MODE.
     */
    public GetObserverMode(): number;

    /**
     * Returns the entity the player is currently observing.
     *
     * @return {Entity} The entity the player is currently spectating, or NULL if the player has no target.
     */
    public GetObserverTarget(): Entity;

    /**
     * Returns a Player Data key-value pair from the SQL database. (sv.db when called on server,  cl.db when called on client)
     *
     * @arg string key - Name of the PData key
     * @arg any [def=nil] - Default value if PData key doesn't exist.
     * @return {string} The data in the SQL database or the default value given.
     */
    public GetPData(key: string, def?: any): string;

    /**
     * Returns a player model's color. The part of the model that is colored is determined by the model itself, and is different for each model. The format is Vector(r,g,b), and each color should be between 0 and 1.
     *
     * @return {Vector} color
     */
    public GetPlayerColor(): Vector;

    /**
     * Returns a table containing player information.
     *
     * @return {object} A table containing player information.
     */
    public GetPlayerInfo(): object;

    /**
     * Returns the preferred carry angles of an object, if any are set.
     *
     * @arg Entity carryEnt - Entity to retrieve the carry angles of.
     * @return {Angle} Carry angles or nil if the entity has no preferred carry angles.
     */
    public GetPreferredCarryAngles(carryEnt: Entity): Angle;

    /**
     * Returns the widget entity the player is using.
     *
     * @return {Entity} The pressed widget.
     */
    public GetPressedWidget(): Entity;

    /**
 * Returns the weapon the player previously had equipped.
* 
* @return {Entity} The previous weapon of the player.
This is not guaranteed to be a weapon entity so it should be checked with Entity:IsWeapon for safety.
 */
    public GetPreviousWeapon(): Entity;

    /**
     * Returns players screen punch effect angle. See Player:ViewPunch and Player:SetViewPunchAngles
     *
     * @return {Angle} The punch angle
     */
    public GetPunchAngle(): Angle;

    /**
 * Returns players death ragdoll. The ragdoll is created by Player:CreateRagdoll.
* 
* @return {Entity} The ragdoll.
Unlike normal clientside ragdolls (C_ClientRagdoll), this will be a C_HL2MPRagdoll on the client, and hl2mp_ragdoll on the server.
 */
    public GetRagdollEntity(): Entity;

    /**
     * Returns the render angles for the player.
     *
     * @return {Angle} The render angles of the player. Only yaw part of the angle seems to be present.
     */
    public GetRenderAngles(): Angle;

    /**
     * Returns the player's sprint speed.
     *
     * @return {number} The sprint speed
     */
    public GetRunSpeed(): number;

    /**
     * Returns the position of a Player's view
     *
     * @return {Vector} aim pos
     */
    public GetShootPos(): Vector;

    /**
     * Returns the player's slow walking speed, which is activated via +walk keybind.
     *
     * @return {number} The new slow walking speed.
     */
    public GetSlowWalkSpeed(): number;

    /**
     * Returns the maximum height player can step onto.
     *
     * @return {number} The maximum height player can get up onto without jumping, in hammer units.
     */
    public GetStepSize(): number;

    /**
     * Returns the player's HEV suit power.
     *
     * @return {number} The current suit power.
     */
    public GetSuitPower(): number;

    /**
     * Returns the number of seconds that the player has been timing out for. You can check if a player is timing out with Player:IsTimingOut.
     *
     * @return {number} Timeout seconds.
     */
    public GetTimeoutSeconds(): number;

    /**
     * Returns TOOL table of players current tool, or of the one specified.
     *
     * @arg string [mode="nil"] - Classname of the tool to retrieve. ( Filename of the tool in gmod_tool/stools/ )
     * @return {object} TOOL table, or nil if the table wasn't found or the player doesn't have a tool gun.
     */
    public GetTool(mode?: string): object;

    /**
     * Returns a player's unduck speed (in seconds)
     *
     * @return {number} unduck speed
     */
    public GetUnDuckSpeed(): number;

    /**
     * Returns the entity the player would use if they would press their +use keybind.
     *
     * @return {Entity} The entity that would be used or NULL.
     */
    public GetUseEntity(): Entity;

    /**
     * Returns the player's user group. By default, player user groups are loaded from garrysmod/settings/users.txt.
     *
     * @return {string} The user group of the player. This will return "user" if player has no user group.
     */
    public GetUserGroup(): string;

    /**
     * Gets the vehicle the player is driving, returns NULL ENTITY if the player is not driving.
     *
     * @return {Vehicle} vehicle
     */
    public GetVehicle(): Vehicle;

    /**
     * Returns the entity the player is using to see from (such as the player itself, the camera, or another entity).
     *
     * @return {Entity} The entity the player is using to see from
     */
    public GetViewEntity(): Entity;

    /**
     * Returns the player's view model entity by the index.
     * Each player has 3 view models by default, but only the first one is used.
     *
     * @arg number [index=0] - optional index of the view model to return, can range from 0 to 2
     * @return {Entity} The view model entity
     */
    public GetViewModel(index?: number): Entity;

    /**
     * Returns the view offset of the player which equals the difference between the players actual position and their view.
     *
     * @return {Vector} New view offset, must be local vector to players Entity:GetPos
     */
    public GetViewOffset(): Vector;

    /**
     * Returns the view offset of the player which equals the difference between the players actual position and their view when ducked.
     *
     * @return {Vector} New crouching view offset, must be local vector to players Entity:GetPos
     */
    public GetViewOffsetDucked(): Vector;

    /**
     * Returns players screen punch effect angle.
     *
     * @return {Angle} The punch angle
     */
    public GetViewPunchAngles(): Angle;

    /**
     * Returns client's view punch velocity. See Player:ViewPunch and Player:SetViewPunchVelocity
     *
     * @return {Angle} The current view punch angle velocity.
     */
    public GetViewPunchVelocity(): Angle;

    /**
     * Returns the current voice volume scale for given player on client.
     *
     * @return {number} The voice volume scale, where 0 is 0% and 1 is 100%.
     */
    public GetVoiceVolumeScale(): number;

    /**
     * Returns the player's normal walking speed. Not sprinting, not slow walking. (+walk)
     *
     * @return {number} The normal walking speed.
     */
    public GetWalkSpeed(): number;

    /**
     * Returns the weapon for the specified class
     *
     * @arg string className - Class name of weapon
     * @return {Weapon} The weapon for the specified class.
     */
    public GetWeapon(className: string): Weapon;

    /**
     * Returns a player's weapon color. The part of the model that is colored is determined by the model itself, and is different for each model. The format is Vector(r,g,b), and each color should be between 0 and 1.
     *
     * @return {Vector} color
     */
    public GetWeaponColor(): Vector;

    /**
     * Returns a table of the player's weapons.
     *
     * @return {object} All the weapons the player currently has.
     */
    public GetWeapons(): object;

    /**
     * Gives the player a weapon.
     *
     * @arg string weaponClassName - Class name of weapon to give the player
     * @arg boolean [bNoAmmo=false] - Set to true to not give any ammo on weapon spawn. (Reserve ammo set by DefaultClip)
     * @return {Weapon} The weapon given to the player, if one was given. It will return NULL if the player already has the weapon, or the weapon entity (entity with given classname) doesn't exist.
     */
    public Give(weaponClassName: string, bNoAmmo?: boolean): Weapon;

    /**
 * Gives ammo to a player
* 
* @arg number amount - Amount of ammo
* @arg string type - Type of ammo.
This can also be a number for ammo ID, useful for custom ammo types.
You can find a list of default ammo types here.
* @arg boolean [hidePopup=false] - Hide display popup when giving the ammo
* @return {number} Ammo given.
 */
    public GiveAmmo(amount: number, type: string, hidePopup?: boolean): number;

    /**
     * Disables god mode on the player.
     */
    public GodDisable(): void;

    /**
     * Enables god mode on the player.
     */
    public GodEnable(): void;

    /**
     * Returns whether the player has god mode or not, contolled by Player:GodEnable and Player:GodDisable.
     *
     * @return {boolean} Whether the player has god mode or not.
     */
    public HasGodMode(): boolean;

    /**
     * Returns if the player has the specified weapon
     *
     * @arg string className - Class name of the weapon
     * @return {boolean} True if the player has the weapon
     */
    public HasWeapon(className: string): boolean;

    /**
     * Returns if the player is in a vehicle
     *
     * @return {boolean} Whether the player is in a vehicle.
     */
    public InVehicle(): boolean;

    /**
     * Returns the player's IP address and connection port in ip:port form
     *
     * @return {string} The player's IP address and connection port
     */
    public IPAddress(): string;

    /**
     * Returns whether the player is an admin or not. It will also return true if the player is Player:IsSuperAdmin by default.
     *
     * @return {boolean} True if the player is an admin or a super admin.
     */
    public IsAdmin(): boolean;

    /**
     * Returns if the player is an bot or not
     *
     * @return {boolean} True if the player is a bot.
     */
    public IsBot(): boolean;

    /**
     * Returns true from the point when the player is sending client info but not fully in the game until they disconnect.
     *
     * @return {boolean} isConnected
     */
    public IsConnected(): boolean;

    /**
     * Used to find out if a player is currently 'driving' an entity (by which we mean 'right click > drive' ).
     *
     * @return {boolean} A value representing whether or not the player is 'driving' an entity.
     */
    public IsDrivingEntity(): boolean;

    /**
     * Returns whether the players movement is currently frozen, controlled by Player:Freeze.
     *
     * @return {boolean} Whether the players movement is currently frozen or not.
     */
    public IsFrozen(): boolean;

    /**
 * Returns whether the player identity was confirmed by the steam network.
* 
* @return {boolean} Whether the player has been fully authenticated or not.
This will always be true for singleplayer and the listen server host.
This will always be false for bots.
 */
    public IsFullyAuthenticated(): boolean;

    /**
 * Returns if a player is the host of the current session.
* 
* @return {boolean} True if the player is the listen server host, false otherwise.
This will always be true in single player, and false on a dedicated server.
 */
    public IsListenServerHost(): boolean;

    /**
     * Returns whether or not the player is muted locally.
     *
     * @return {boolean} whether or not the player is muted locally.
     */
    public IsMuted(): boolean;

    /**
     * Returns true if the player is playing a taunt.
     *
     * @return {boolean} Whether the player is playing a taunt.
     */
    public IsPlayingTaunt(): boolean;

    /**
     * Returns whenever the player is heard by the local player clientside, or if the player is speaking serverside.
     *
     * @return {boolean} Is the player speaking or not.
     */
    public IsSpeaking(): boolean;

    /**
     * Returns whether the player is currently sprinting or not, specifically if they are holding their sprint key and are allowed to sprint.
     *
     * @return {boolean} Is the player sprinting or not
     */
    public IsSprinting(): boolean;

    /**
     * Returns whenever the player is equipped with the suit item.
     *
     * @return {boolean} Is the suit equipped or not.
     */
    public IsSuitEquipped(): boolean;

    /**
     * Returns whether the player is a super admin.
     *
     * @return {boolean} True if the player is a super admin.
     */
    public IsSuperAdmin(): boolean;

    /**
     * Returns true if the player is timing out (i.e. is losing connection), false otherwise.
     *
     * @return {boolean} isTimingOut
     */
    public IsTimingOut(): boolean;

    /**
     * Returns whether the player is typing in their chat.
     *
     * @return {boolean} Whether the player is typing in their chat or not.
     */
    public IsTyping(): boolean;

    /**
     * Returns true/false if the player is in specified group or not. See Player:GetUserGroup for a way to get player's usergroup.
     *
     * @arg string groupname - Group to check the player for.
     * @return {boolean} isInUserGroup
     */
    public IsUserGroup(groupname: string): boolean;

    /**
     * Returns if the player can be heard by the local player.
     *
     * @return {boolean} isAudible
     */
    public IsVoiceAudible(): boolean;

    /**
     * Returns if the player is in the context menu.
     *
     * @return {boolean} Is the player world clicking or not.
     */
    public IsWorldClicking(): boolean;

    /**
     * Gets whether a key is down. This is not networked to other players, meaning only the local client can see the keys they are pressing.
     *
     * @arg number key - The key, see Enums/IN
     * @return {boolean} isDown ?
     */
    public KeyDown(key: number): boolean;

    /**
     * Gets whether a key was down one tick ago.
     *
     * @arg number key - The key, see Enums/IN
     * @return {boolean} Is key down ?
     */
    public KeyDownLast(key: number): boolean;

    /**
     * Gets whether a key was just pressed this tick.
     *
     * @arg number key - Corresponds to an Enums/IN
     * @return {boolean} Was pressed or not
     */
    public KeyPressed(key: number): boolean;

    /**
     * Gets whether a key was just released this tick.
     *
     * @arg number key - The key, see Enums/IN
     * @return {boolean} Was released or not
     */
    public KeyReleased(key: number): boolean;

    /**
 * Kicks the player from the server.
* 
* @arg string [reason="No reason given"] - Reason to show for disconnection.
This will be shortened to ~512 chars, though this includes the command itself and the player index so will realistically be more around ~498. It is recommended to avoid going near the limit to avoid truncation.
 */
    public Kick(reason?: string): void;

    /**
     * Kills a player and calls GM:PlayerDeath.
     */
    public Kill(): void;

    /**
     * Kills a player without notifying the rest of the server.
     */
    public KillSilent(): void;

    /**
     * This allows the server to mitigate the lag of the player by moving back all the entities that can be lag compensated to the time the player attacked with his weapon.
     *
     * @arg boolean lagCompensation - The state of the lag compensation, true to enable and false to disable.
     */
    public LagCompensation(lagCompensation: boolean): void;

    /**
     * Returns the hitgroup where the player was last hit.
     *
     * @return {number} Hitgroup, see Enums/HITGROUP
     */
    public LastHitGroup(): number;

    /**
     * Shows "limit hit" notification in sandbox.
     *
     * @arg string type - Type of hit limit.
     */
    public LimitHit(type: string): void;

    /**
     * Returns the direction a player is looking as a entity/local-oriented angle.
     *
     * @return {Angle} local eye angles
     */
    public LocalEyeAngles(): Angle;

    /**
     * Stops a player from using any inputs, such as moving, turning, or attacking. Key binds are still called. Similar to Player:Freeze but the player takes no damage.
     */
    public Lock(): void;

    /**
     * Returns the position of a Kinect bone.
     *
     * @arg number bone - Bone to get the position of. Must be from 0 to 19.
     * @return {Vector} Position of the bone.
     */
    public MotionSensorPos(bone: number): Vector;

    /**
     * Returns the players name. Identical to Player:Nick and Player:GetName.
     *
     * @return {string} Player's Steam name.
     */
    public Name(): string;

    /**
     * Returns the player's nickname.
     *
     * @return {string} Player's Steam name
     */
    public Nick(): string;

    /**
     * Returns the 64-bit SteamID aka CommunityID of the Steam Account that owns the Garry's Mod license this player is using. This is useful for detecting players using Steam Family Sharing.
     *
     * @return {string} The 64bit SteamID
     */
    public OwnerSteamID64(): string;

    /**
     * Returns the packet loss of the client. It is not networked so it only returns 0 when run clientside.
     *
     * @return {number} Packets lost
     */
    public PacketLoss(): number;

    /**
     * Unfreezes the props player is looking at. This is essentially the same as pressing reload with the physics gun, including double press for unfreeze all.
     *
     * @return {number} Number of props unfrozen.
     */
    public PhysgunUnfreeze(): number;

    /**
     * This makes the player hold ( same as pressing e on a small prop ) the provided entity.
     *
     * @arg Entity entity - Entity to pick up.
     */
    public PickupObject(entity: Entity): void;

    /**
     * Forces the player to pickup an existing weapon entity. The player will not pick up the weapon if they already own a weapon of given type, or if the player could not normally have this weapon in their inventory.
     *
     * @arg Weapon wep - The weapon to try to pick up.
     * @arg boolean [ammoOnly=false] - If set to true, the player will only attempt to pick up the ammo from the weapon. The weapon will not be picked up even if the player doesn't have a weapon of this type, and the weapon will be removed if the player picks up any ammo from it.
     * @return {boolean} Whether the player succeeded in picking up the weapon or not.
     */
    public PickupWeapon(wep: Weapon, ammoOnly?: boolean): boolean;

    /**
     * Returns the player's ping to server.
     *
     * @return {number} The player's ping.
     */
    public Ping(): number;

    /**
     * Plays the correct step sound according to what the player is staying on.
     *
     * @arg number volume - Volume for the sound, in range from 0 to 1
     */
    public PlayStepSound(volume: number): void;

    /**
     * Displays a message either in their chat, console, or center of the screen. See also PrintMessage.
     *
     * @arg number type - Which type of message should be sent to the player (Enums/HUD).
     * @arg string message - Message to be sent to the player.
     */
    public PrintMessage(type: number, message: string): void;

    /**
     * Removes all ammo from a certain player
     */
    public RemoveAllAmmo(): void;

    /**
     * Removes all weapons and ammo from the player.
     */
    public RemoveAllItems(): void;

    /**
     * Removes the amount of the specified ammo from the player.
     *
     * @arg number ammoCount - The amount of ammunition to remove.
     * @arg string ammoName - The name of the ammunition to remove from. This can also be a number ammoID.
     */
    public RemoveAmmo(ammoCount: number, ammoName: string): void;

    /**
     * Removes a Player Data key-value pair from the SQL database. (sv.db when called on server,  cl.db when called on client)
     *
     * @arg string key - Key to remove
     * @return {boolean} true is succeeded, false otherwise
     */
    public RemovePData(key: string): boolean;

    /**
     * Strips the player's suit item.
     */
    public RemoveSuit(): void;

    /**
     * Resets both normal and duck hulls to their default values.
     */
    public ResetHull(): void;

    /**
     * Forces the player to say whatever the first argument is. Works on bots too.
     *
     * @arg string text - The text to force the player to say.
     * @arg boolean [teamOnly=false] - Whether to send this message to our own team only.
     */
    public Say(text: string, teamOnly?: boolean): void;

    /**
 * Fades the screen
* 
* @arg number flags - Fade flags defined with Enums/SCREENFADE.
* @arg number [clr=NaN] - The color of the screenfade
* @arg number fadeTime - Fade(in/out) effect transition time ( From no fade to full fade and vice versa ).
This is limited to 7 bits integer part and 9 bits fractional part.
* @arg number fadeHold - Fade effect hold time.
This is limited to 7 bits integer part and 9 bits fractional part.
 */
    public ScreenFade(flags: number, clr?: number, fadeTime?: number, fadeHold?: number): void;

    /**
 * Sets the active weapon of the player by its class name.
* 
* @arg string className - The class name of the weapon to switch to.
If the player doesn't have the specified weapon, nothing will happen. You can use Player:Give to give the weapon first.
 */
    public SelectWeapon(className: string): void;

    /**
     * Sends a hint to a player.
     *
     * @arg string name - Name/class/index of the hint. The text of the hint will contain this value. ( "#Hint_" .. name ) An example is PhysgunFreeze.
     * @arg number delay - Delay in seconds before showing the hint
     */
    public SendHint(name: string, delay: number): void;

    /**
     * Executes a simple Lua string on the player.
     *
     * @arg string script - The script to execute.
     */
    public SendLua(script: string): void;

    /**
     * Sets the player's active weapon. You should use CUserCmd:SelectWeapon or Player:SelectWeapon, instead in most cases.
     *
     * @arg Weapon weapon - The weapon to equip.
     */
    public SetActiveWeapon(weapon: Weapon): void;

    /**
     * Sets the player's activity.
     *
     * @arg number act - The new activity to set. See Enums/ACT.
     */
    public SetActivity(act: number): void;

    /**
     * Set if the players' model is allowed to rotate around the pitch and roll axis.
     *
     * @arg boolean Allowed - Allowed to rotate
     */
    public SetAllowFullRotation(Allowed: boolean): void;

    /**
     * Allows player to use his weapons in a vehicle. You need to call this before entering a vehicle.
     *
     * @arg boolean allow - Show we allow player to use his weapons in a vehicle or not.
     */
    public SetAllowWeaponsInVehicle(allow: boolean): void;

    /**
     * Sets the amount of the specified ammo for the player.
     *
     * @arg number ammoCount - The amount of ammunition to set.
     * @arg any ammoType - The ammunition type. Can be either number ammo ID or string ammo name.
     */
    public SetAmmo(ammoCount: number, ammoType: any): void;

    /**
     * Sets the player armor to the argument.
     *
     * @arg number Amount - The amount that the player armor is going to be set to.
     */
    public SetArmor(Amount: number): void;

    /**
     * Pushes the player away from another player whenever it's inside the other players bounding box.
     *
     * @arg boolean avoidPlayers - Avoid or not avoid.
     */
    public SetAvoidPlayers(avoidPlayers: boolean): void;

    /**
     * Set if the player should be allowed to walk using the (default) alt key.
     *
     * @arg boolean abletowalk - True allows the player to walk.
     */
    public SetCanWalk(abletowalk: boolean): void;

    /**
     * Sets whether the player can use the HL2 suit zoom ("+zoom" bind) or not.
     *
     * @arg boolean canZoom - Whether to make the player able or unable to zoom.
     */
    public SetCanZoom(canZoom: boolean): void;

    /**
     * Sets the player's class id.
     *
     * @arg number classID - The class id the player is being set with.
     */
    public SetClassID(classID: number): void;

    /**
     * Sets the crouched walk speed multiplier.
     *
     * @arg number speed - The walk speed multiplier that crouch speed should be.
     */
    public SetCrouchedWalkSpeed(speed: number): void;

    /**
     * Sets the actual view offset which equals the difference between the players actual position and their view when standing.
     *
     * @arg Vector viewOffset - The new view offset.
     */
    public SetCurrentViewOffset(viewOffset: Vector): void;

    /**
     * Sets a player's death count
     *
     * @arg number deathcount - Number of deaths (positive or negative)
     */
    public SetDeaths(deathcount: number): void;

    /**
     * Sets the driving entity and driving mode.
     *
     * @arg Entity [drivingEntity=NULL] - The entity the player should drive.
     * @arg number drivingMode - The driving mode index.
     */
    public SetDrivingEntity(drivingEntity?: Entity, drivingMode?: number): void;

    /**
 * Applies the specified sound filter to the player.
* 
* @arg number soundFilter - The index of the sound filter to apply.
Pick from the list of DSP's.
* @arg boolean fastReset - If set to true the sound filter will be removed faster.
 */
    public SetDSP(soundFilter: number, fastReset: boolean): void;

    /**
     * Sets how quickly a player ducks.
     *
     * @arg number duckSpeed - How quickly the player will duck.
     */
    public SetDuckSpeed(duckSpeed: number): void;

    /**
     * Sets the local angle of the player's view (may rotate body too if angular difference is large)
     *
     * @arg Angle angle - Angle to set the view to
     */
    public SetEyeAngles(angle: Angle): void;

    /**
     * Set a player's FOV (Field Of View) over a certain amount of time.
     *
     * @arg number fov - the angle of perception (FOV). Set to 0 to return to default user FOV. ( Which is ranging from 75 to 90, depending on user settings )
     * @arg number [time=0] - the time it takes to transition to the FOV expressed in a floating point.
     * @arg Entity [requester=self] - The requester or "owner" of the zoom event.
     */
    public SetFOV(fov: number, time?: number, requester?: Entity): void;

    /**
     * Sets a player's frags (kills)
     *
     * @arg number fragcount - Number of frags (positive or negative)
     */
    public SetFrags(fragcount: number): void;

    /**
     * Sets the hands entity of a player.
     *
     * @arg Entity hands - The hands entity to set
     */
    public SetHands(hands: Entity): void;

    /**
     * Sets the widget that is currently hovered by the player's mouse.
     *
     * @arg Entity [widget=NULL] - The widget entity that the player is hovering.
     */
    public SetHoveredWidget(widget?: Entity): void;

    /**
     * Sets the mins and maxs of the AABB of the players collision.
     *
     * @arg Vector hullMins - The min coordinates of the hull.
     * @arg Vector hullMaxs - The max coordinates of the hull.
     */
    public SetHull(hullMins: Vector, hullMaxs: Vector): void;

    /**
     * Sets the mins and maxs of the AABB of the players collision when ducked.
     *
     * @arg Vector hullMins - The min coordinates of the hull.
     * @arg Vector hullMaxs - The max coordinates of the hull.
     */
    public SetHullDuck(hullMins: Vector, hullMaxs: Vector): void;

    /**
     * Sets the jump power, eg. the velocity the player will applied to when he jumps.
     *
     * @arg number jumpPower - The new jump velocity.
     */
    public SetJumpPower(jumpPower: number): void;

    /**
     * Sets the player's ladder climbing speed.
     *
     * @arg number speed - The ladder climbing speed.
     */
    public SetLadderClimbSpeed(speed: number): void;

    /**
     * Slows down the player movement simulation by the timescale, this is used internally in the HL2 weapon stripping sequence.
     *
     * @arg number timescale - The timescale multiplier.
     */
    public SetLaggedMovementValue(timescale: number): void;

    /**
     * Sets the hitgroup where the player was last hit.
     *
     * @arg number hitgroup - The hitgroup to set as the "last hit", see Enums/HITGROUP.
     */
    public SetLastHitGroup(hitgroup: number): void;

    /**
     * Sets the maximum amount of armor the player should have. This affects default built-in armor pickups, but not Player:SetArmor.
     *
     * @arg number maxarmor - The new max armor value.
     */
    public SetMaxArmor(maxarmor: number): void;

    /**
     * Sets the maximum speed which the player can move at.
     *
     * @arg number walkSpeed - The maximum speed.
     */
    public SetMaxSpeed(walkSpeed: number): void;

    /**
     * Sets if the player should be muted locally.
     *
     * @arg boolean mute - Mute or unmute.
     */
    public SetMuted(mute: boolean): void;

    /**
     * Sets whenever the player should not collide with their teammates.
     *
     * @arg boolean shouldNotCollide - True to disable, false to enable collision.
     */
    public SetNoCollideWithTeammates(shouldNotCollide: boolean): void;

    /**
     * Sets the players visibility towards NPCs.
     *
     * @arg boolean visibility - The visibility.
     */
    public SetNoTarget(visibility: boolean): void;

    /**
     * Sets the players observer mode. You must start the spectating first with Player:Spectate.
     *
     * @arg number mode - Spectator mode using Enums/OBS_MODE.
     */
    public SetObserverMode(mode: number): void;

    /**
     * Writes a Player Data key-value pair to the SQL database. (sv.db when called on server,  cl.db when called on client)
     *
     * @arg string key - Name of the PData key
     * @arg any value - Value to write to the key (must be an SQL valid data type, such as a string or integer)
     * @return {boolean} Whether the operation was successful or not
     */
    public SetPData(key: string, value: any): boolean;

    /**
     * Sets the player model's color. The part of the model that is colored is determined by the model itself, and is different for each model.
     *
     * @arg Vector Color - This is the color to be set. The format is Vector(r, g, b), and each color should be between 0 and 1.
     */
    public SetPlayerColor(Color: Vector): void;

    /**
     * Sets the widget that is currently in use by the player's mouse.
     *
     * @arg Entity [pressedWidget=NULL] - The widget the player is currently using.
     */
    public SetPressedWidget(pressedWidget?: Entity): void;

    /**
     * Sets the render angles of a player.
     *
     * @arg Angle ang - The new render angles to set
     */
    public SetRenderAngles(ang: Angle): void;

    /**
 * Sets the player's sprint speed.
* 
* @arg number runSpeed - The new sprint speed when sv_friction is below 10. Higher sv_friction values will result in slower speed.
Has to be 7 or above or the player won't be able to move.
 */
    public SetRunSpeed(runSpeed: number): void;

    /**
     * Sets the player's slow walking speed, which is activated via +walk keybind.
     *
     * @arg number speed - The new slow walking speed.
     */
    public SetSlowWalkSpeed(speed: number): void;

    /**
     * Sets the maximum height a player can step onto without jumping.
     *
     * @arg number stepHeight - The new maximum height the player can step onto without jumping
     */
    public SetStepSize(stepHeight: number): void;

    /**
     * Sets the player's HEV suit power.
     *
     * @arg number power - The new suit power.
     */
    public SetSuitPower(power: number): void;

    /**
     * Sets whenever to suppress the pickup notification for the player.
     *
     * @arg boolean doSuppress - Whenever to suppress the notice or not.
     */
    public SetSuppressPickupNotices(doSuppress: boolean): void;

    /**
     * Sets the player to the chosen team.
     *
     * @arg number Team - The team that the player is being set to.
     */
    public SetTeam(Team: number): void;

    /**
     * Sets how quickly a player un-ducks
     *
     * @arg number UnDuckSpeed - How quickly the player will un-duck
     */
    public SetUnDuckSpeed(UnDuckSpeed: number): void;

    /**
     * Sets up the players view model hands. Calls GM:PlayerSetHandsModel to set the model of the hands.
     *
     * @arg Entity ent - If the player is spectating an entity, this should be the entity the player is spectating, so we can use its hands model instead.
     */
    public SetupHands(ent: Entity): void;

    /**
     * Sets the usergroup of the player. Same as Player:SetNWString('UserGroup',string groupName).
     *
     * @arg string groupName - The user group of the player.
     */
    public SetUserGroup(groupName: string): void;

    /**
     * Attaches the players view to the position and angles of the specified entity.
     *
     * @arg Entity viewEntity - The entity to attach the player view to.
     */
    public SetViewEntity(viewEntity: Entity): void;

    /**
     * Sets the desired view offset which equals the difference between the players actual position and their view when standing.
     *
     * @arg Vector viewOffset - The new desired view offset when standing.
     */
    public SetViewOffset(viewOffset: Vector): void;

    /**
     * Sets the desired view offset which equals the difference between the players actual position and their view when crouching.
     *
     * @arg Vector viewOffset - The new desired view offset when crouching.
     */
    public SetViewOffsetDucked(viewOffset: Vector): void;

    /**
     * Sets client's view punch angle, but not the velocity. See Player:ViewPunch
     *
     * @arg Angle punchAngle - The angle to set.
     */
    public SetViewPunchAngles(punchAngle: Angle): void;

    /**
     * Sets client's view punch velocity. See Player:ViewPunch and Player:SetViewPunchAngles
     *
     * @arg Angle punchVel - The angle velocity to set.
     */
    public SetViewPunchVelocity(punchVel: Angle): void;

    /**
     * Sets the voice volume scale for given player on client. This value will persist from server to server, but will be reset when the game is shut down.
     *
     * @arg number arg1 - The voice volume scale, where 0 is 0% and 1 is 100%.
     */
    public SetVoiceVolumeScale(arg1: number): void;

    /**
 * Sets the player's normal walking speed. Not sprinting, not slow walking +walk.
* 
* @arg number walkSpeed - The new walk speed when sv_friction is below 10. Higher sv_friction values will result in slower speed.
Has to be 7 or above or the player won't be able to move.
 */
    public SetWalkSpeed(walkSpeed: number): void;

    /**
     * Sets the player weapon's color. The part of the model that is colored is determined by the model itself, and is different for each model.
     *
     * @arg Vector Color - This is the color to be set. The format is Vector(r,g,b), and each color should be between 0 and 1.
     */
    public SetWeaponColor(Color: Vector): void;

    /**
     * Returns whether the player's player model will be drawn at the time the function is called.
     *
     * @return {boolean} true if the player's playermodel is visible
     */
    public ShouldDrawLocalPlayer(): boolean;

    /**
     * Sets whether the player's current weapon should drop on death.
     *
     * @arg boolean drop - Whether to drop the player's current weapon or not
     */
    public ShouldDropWeapon(drop: boolean): void;

    /**
     * Opens the player steam profile page in the steam overlay browser.
     */
    public ShowProfile(): void;

    /**
     * Signals the entity that it was dropped by the gravity gun.
     *
     * @arg Entity ent - Entity that was dropped.
     */
    public SimulateGravGunDrop(ent: Entity): void;

    /**
     * Signals the entity that it was picked up by the gravity gun. This call is only required if you want to simulate the situation of picking up objects.
     *
     * @arg Entity ent - The entity picked up
     */
    public SimulateGravGunPickup(ent: Entity): void;

    /**
     * Starts spectate mode for given player. This will also affect the players movetype in some cases.
     *
     * @arg number mode - Spectate mode, see Enums/OBS_MODE.
     */
    public Spectate(mode: number): void;

    /**
     * Makes the player spectate the entity.
     *
     * @arg Entity entity - Entity to spectate.
     */
    public SpectateEntity(entity: Entity): void;

    /**
     * Makes a player spray their decal.
     *
     * @arg Vector sprayOrigin - The location to spray from
     * @arg Vector sprayEndPos - The location to spray to
     */
    public SprayDecal(sprayOrigin: Vector, sprayEndPos: Vector): void;

    /**
     * Disables the sprint on the player.
     */
    public SprintDisable(): void;

    /**
     * Enables the sprint on the player.
     */
    public SprintEnable(): void;

    /**
     * Doesn't appear to do anything.
     */
    public StartSprinting(): void;

    /**
     * When used in a GM:SetupMove hook, this function will force the player to walk, as well as preventing the player from sprinting.
     */
    public StartWalking(): void;

    /**
     * Returns the player's SteamID.
     *
     * @return {string} SteamID
     */
    public SteamID(): string;

    /**
     * Returns the player's 64-bit SteamID aka CommunityID.
     *
     * @return {string} Player's 64-bit SteamID aka CommunityID.
     */
    public SteamID64(): string;

    /**
     * When used in a GM:SetupMove hook, this function will prevent the player from sprinting.
     */
    public StopSprinting(): void;

    /**
     * When used in a GM:SetupMove hook, this function behaves unexpectedly by preventing the player from sprinting similar to Player:StopSprinting.
     */
    public StopWalking(): void;

    /**
     * Turns off the zoom mode of the player. (+zoom console command)
     */
    public StopZooming(): void;

    /**
     * Removes all ammo from the player.
     */
    public StripAmmo(): void;

    /**
     * Removes the specified weapon class from a certain player
     *
     * @arg string weapon - The weapon class to remove
     */
    public StripWeapon(weapon: string): void;

    /**
     * Removes all weapons from a certain player
     */
    public StripWeapons(): void;

    /**
     * Prevents a hint from showing up.
     *
     * @arg string name - Hint name/class/index to prevent from showing up
     */
    public SuppressHint(name: string): void;

    /**
     * Attempts to switch the player weapon to the one specified in the "cl_defaultweapon" convar, if the player does not own the specified weapon nothing will happen.
     */
    public SwitchToDefaultWeapon(): void;

    /**
     * Returns the player's team ID.
     *
     * @return {number} The player's team's index number, as in the Enums/TEAM or a custom team defined in team.SetUp.
     */
    public Team(): number;

    /**
     * Returns the time in seconds since the player connected.
     *
     * @return {number}
     */
    public TimeConnected(): number;

    /**
     * Performs a trace hull and applies damage to the entities hit, returns the first entity hit.
     *
     * @arg Vector startPos - The start position of the hull trace.
     * @arg Vector endPos - The end position of the hull trace.
     * @arg Vector mins - The minimum coordinates of the hull.
     * @arg Vector maxs - The maximum coordinates of the hull.
     * @arg number damage - The damage to be applied.
     * @arg number damageFlags - Bitflag specifying the damage type, see Enums/DMG.
     * @arg number damageForce - The force to be applied to the hit object.
     * @arg boolean damageAllNPCs - Whether to apply damage to all hit NPCs or not.
     * @return {Entity} The hit entity
     */
    public TraceHullAttack(
        startPos: Vector,
        endPos: Vector,
        mins: Vector,
        maxs: Vector,
        damage: number,
        damageFlags: number,
        damageForce: number,
        damageAllNPCs: boolean
    ): Entity;

    /**
     * Translates Enums/ACT according to the holdtype of players currently held weapon.
     *
     * @arg number act - The initial Enums/ACT
     * @return {number} Translated Enums/ACT
     */
    public TranslateWeaponActivity(act: number): number;

    /**
     * Unfreezes all objects the player has frozen with their Physics Gun. Same as double pressing R while holding Physics Gun.
     */
    public UnfreezePhysicsObjects(): void;

    /**
     * Returns a 32 bit integer that remains constant for a player across joins/leaves and across different servers. This can be used when a string is inappropriate - e.g. in a database primary key.
     *
     * @return {number} The player's Unique ID
     */
    public UniqueID(): number;

    /**
     * Returns a table that will stay allocated for the specific player between connects until the server shuts down.
     *
     * @arg any key - Unique table key.
     * @return {object} The table that contains any info you have put in it.
     */
    public UniqueIDTable(key: any): object;

    /**
     * Unlocks the player movement if locked previously.
     *
     * Will disable godmode for the player if locked previously.
     */
    public UnLock(): void;

    /**
     * Stops the player from spectating another entity.
     */
    public UnSpectate(): void;

    /**
     * Returns the player's ID.
     * You can use Player() to get the player by their ID.
     *
     * @return {number} The player's user ID
     */
    public UserID(): number;

    /**
     * Simulates a push on the client's screen. This adds view punch velocity, and does not touch the current view punch angle, for which you can use Player:SetViewPunchAngles.
     *
     * @arg Angle PunchAngle - The angle in which to push the player's screen.
     */
    public ViewPunch(PunchAngle: Angle): void;

    /**
     * Resets the player's view punch (and the view punch velocity, read more at Player:ViewPunch) effect back to normal.
     *
     * @arg number [tolerance=0] - Reset all ViewPunch below this threshold.
     */
    public ViewPunchReset(tolerance?: number): void;

    /**
     * Returns the players voice volume, how loud the player's voice communication currently is, as a normal number. Doesn't work on local player unless the voice_loopback convar is set to 1.
     *
     * @return {number} The voice volume.
     */
    public VoiceVolume(): number;
}

// Allow the class to be instantiated with a function call:

/**
 * Returns the player with the matching Player:UserID.
 *
 * @arg number playerIndex - The player index.
 * @return {Player} The retrieved player.
 */
declare function Player(playerIndex: number): Player;

declare type Player = __PlayerClass & Entity;
