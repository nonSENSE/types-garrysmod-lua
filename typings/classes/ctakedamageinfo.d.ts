declare class __CTakeDamageInfoClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Increases the damage by damageIncrease.
     *
     * @arg number damageIncrease - The damage to add.
     */
    public AddDamage(damageIncrease: number): void;

    /**
     * Returns the ammo type used by the weapon that inflicted the damage.
     *
     * @return {number} Ammo type ID
     */
    public GetAmmoType(): number;

    /**
     * Returns the attacker ( character who originated the attack ), for example a player or an NPC that shot the weapon.
     *
     * @return {Entity} The attacker
     */
    public GetAttacker(): Entity;

    /**
     * Returns the initial unmodified by skill level ( game.GetSkillLevel ) damage.
     *
     * @return {number} baseDamage
     */
    public GetBaseDamage(): number;

    /**
     * Returns the total damage.
     *
     * @return {number} damage
     */
    public GetDamage(): number;

    /**
     * Gets the current bonus damage.
     *
     * @return {number} Bonus damage
     */
    public GetDamageBonus(): number;

    /**
     * Gets the custom damage type. This is used by Day of Defeat: Source and Team Fortress 2 for extended damage info, but isn't used in Garry's Mod by default.
     *
     * @return {number} The custom damage type
     */
    public GetDamageCustom(): number;

    /**
     * Returns a vector representing the damage force.
     *
     * @return {Vector} The damage force
     */
    public GetDamageForce(): Vector;

    /**
     * Returns the position where the damage was or is going to be applied to.
     *
     * @return {Vector} The damage position
     */
    public GetDamagePosition(): Vector;

    /**
     * Returns a bitflag which indicates the damage type(s) of the damage.
     *
     * @return {number} Damage type(s), a combination of Enums/DMG
     */
    public GetDamageType(): number;

    /**
     * Returns the inflictor of the damage. This is not necessarily a weapon.
     *
     * @return {Entity} The inflictor
     */
    public GetInflictor(): Entity;

    /**
     * Returns the maximum damage. See CTakeDamageInfo:SetMaxDamage
     *
     * @return {number} maxDmg
     */
    public GetMaxDamage(): number;

    /**
     * Returns the initial, unmodified position where the damage occured.
     *
     * @return {Vector} position
     */
    public GetReportedPosition(): Vector;

    /**
     * Returns true if the damage was caused by a bullet.
     *
     * @return {boolean} isBulletDmg
     */
    public IsBulletDamage(): boolean;

    /**
     * Returns whenever the damageinfo contains the damage type specified.
     *
     * @arg number dmgType - Damage type to test. See Enums/DMG.
     * @return {boolean} Whether this damage contains specified damage type or not
     */
    public IsDamageType(dmgType: number): boolean;

    /**
     * Returns whenever the damageinfo contains explosion damage.
     *
     * @return {boolean} isExplDamage
     */
    public IsExplosionDamage(): boolean;

    /**
     * Returns whenever the damageinfo contains fall damage.
     *
     * @return {boolean} isFallDmg
     */
    public IsFallDamage(): boolean;

    /**
     * Scales the damage by the given value.
     *
     * @arg number scale - Value to scale the damage with.
     */
    public ScaleDamage(scale: number): void;

    /**
     * Changes the ammo type used by the weapon that inflicted the damage.
     *
     * @arg number ammoType - Ammo type ID
     */
    public SetAmmoType(ammoType: number): void;

    /**
     * Sets the attacker ( character who originated the attack ) of the damage, for example a player or an NPC.
     *
     * @arg Entity ent - The entity to be set as the attacker.
     */
    public SetAttacker(ent: Entity): void;

    /**
     * Sets the initial unmodified by skill level ( game.GetSkillLevel ) damage. This function will not update or touch CTakeDamageInfo:GetDamage.
     *
     * @arg number arg1 - baseDamage
     */
    public SetBaseDamage(arg1: number): void;

    /**
     * Sets the amount of damage.
     *
     * @arg number damage - The value to set the absolute damage to.
     */
    public SetDamage(damage: number): void;

    /**
     * Sets the bonus damage. Bonus damage isn't automatically applied, so this will have no outer effect by default.
     *
     * @arg number damage - The extra damage to be added.
     */
    public SetDamageBonus(damage: number): void;

    /**
     * Sets the custom damage type. This is used by Day of Defeat: Source and Team Fortress 2 for extended damage info, but isn't used in Garry's Mod by default.
     *
     * @arg number DamageType - Any integer - can be based on your own custom enums.
     */
    public SetDamageCustom(DamageType: number): void;

    /**
     * Sets the directional force of the damage.
     *
     * @arg Vector force - The vector to set the force to.
     */
    public SetDamageForce(force: Vector): void;

    /**
     * Sets the position of where the damage gets applied to.
     *
     * @arg Vector pos - The position where the damage will be applied.
     */
    public SetDamagePosition(pos: Vector): void;

    /**
     * Sets the damage type.
     *
     * @arg number type - The damage type, see Enums/DMG.
     */
    public SetDamageType(type: number): void;

    /**
     * Sets the inflictor of the damage for example a weapon.
     *
     * @arg Entity inflictor - The new inflictor.
     */
    public SetInflictor(inflictor: Entity): void;

    /**
     * Sets the maximum damage this damage event can cause.
     *
     * @arg number maxDamage - Maximum damage value.
     */
    public SetMaxDamage(maxDamage: number): void;

    /**
     * Sets the origin of the damage.
     *
     * @arg Vector pos - The location of where the damage is originating
     */
    public SetReportedPosition(pos: Vector): void;

    /**
     * Subtracts the specified amount from the damage.
     *
     * @arg number damage - Value to subtract.
     */
    public SubtractDamage(damage: number): void;
}

declare type CTakeDamageInfo = __CTakeDamageInfoClass;
