declare class __IGModAudioChannelClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Enables or disables looping of audio channel, requires noblock flag.
     *
     * @arg boolean enable - Enable or disable looping of this audio channel.
     */
    public EnableLooping(enable: boolean): void;

    /**
     * Computes the DFT (discrete Fourier transform) of the sound channel.
     *
     * @arg object tbl - The table to output the DFT magnitudes (numbers between 0 and 1) into. Indices start from 1.
     * @arg number size - The number of samples to use. See Enums/FFT
     * @return {number} The number of frequency bins that have been filled in the output table.
     */
    public FFT(tbl: object, size: number): number;

    /**
     * Returns 3D cone of the sound channel. See IGModAudioChannel:Set3DCone.
     *
     * @return {number} The angle of the inside projection cone in degrees.
     * @return {number} The angle of the outside projection cone in degrees.
     * @return {number} The delta-volume outside the outer projection cone.
     */
    public Get3DCone(): LuaMultiReturn<[number, number, number]>;

    /**
     * Returns if the sound channel is currently in 3D mode or not. This value will be affected by IGModAudioChannel:Set3DEnabled.
     *
     * @return {boolean} Is currently 3D or not.
     */
    public Get3DEnabled(): boolean;

    /**
     * Returns 3D fade distances of a sound channel.
     *
     * @return {number} The minimum distance. The channel's volume is at maximum when the listener is within this distance
     * @return {number} The maximum distance. The channel's volume stops decreasing when the listener is beyond this distance
     */
    public Get3DFadeDistance(): LuaMultiReturn<[number, number]>;

    /**
     * Returns the average bit rate of the sound channel.
     *
     * @return {number} The average bit rate of the sound channel.
     */
    public GetAverageBitRate(): number;

    /**
     * Retrieves the number of bits per sample of the sound channel.
     *
     * @return {number} Number of bits per sample, or 0 if unknown.
     */
    public GetBitsPerSample(): number;

    /**
     * Returns the filename for the sound channel.
     *
     * @return {string} The file name. This will not be always what you have put into the sound.PlayURL as first argument.
     */
    public GetFileName(): string;

    /**
     * Returns the length of sound played by the sound channel.
     *
     * @return {number} The length of the sound. This value seems to be less then 0 for continuous radio streams.
     */
    public GetLength(): number;

    /**
     * Returns the right and left levels of sound played by the sound channel.
     *
     * @return {number} The left sound level. The value is between 0 and 1.
     * @return {number} The right sound level. The value is between 0 and 1.
     */
    public GetLevel(): LuaMultiReturn<[number, number]>;

    /**
     * Gets the relative volume of the left and right channels.
     *
     * @return {number} Relative volume between the left and right channels. -1 means only in left channel, 0 is center and 1 is only in the right channel. 0 by default.
     */
    public GetPan(): number;

    /**
     * Returns the playback rate of the sound channel.
     *
     * @return {number} The current playback rate of the sound channel
     */
    public GetPlaybackRate(): number;

    /**
     * Returns position of the sound channel
     *
     * @return {Vector} The position of the sound channel, previously set by IGModAudioChannel:SetPos
     */
    public GetPos(): Vector;

    /**
     * Returns the sample rate for currently playing sound.
     *
     * @return {number} The sample rate in Hz. This should always be 44100.
     */
    public GetSamplingRate(): number;

    /**
     * Returns the state of a sound channel
     *
     * @return {number} The state of the sound channel, see Enums/GMOD_CHANNEL
     */
    public GetState(): number;

    /**
     * Retrieves HTTP headers from a bass stream channel created by sound.PlayURL, if available.
     *
     * @return {object} Returns a table of HTTP headers.Returns nil if no information is available.
     */
    public GetTagsHTTP(): object;

    /**
 * Retrieves the ID3 version 1 info from a bass channel created by sound.PlayFile or sound.PlayURL, if available.
* 
* @return {object} Returns a table containing the information.
Returns nil if no information is available.
 */
    public GetTagsID3(): object;

    /**
 * Retrieves meta stream info from a bass stream channel created by sound.PlayURL, if available.
* 
* @return {string} Returns a string containing the information.
Returns nil if no information is available.
 */
    public GetTagsMeta(): string;

    /**
 * Retrieves OGG media info tag, from a bass channel created by sound.PlayURL or sound.PlayFile, if available.
* 
* @return {object} Returns a table containing the information.
Returns nil if no information is available.
 */
    public GetTagsOGG(): object;

    /**
 * Retrieves OGG Vendor tag, usually containing the application that created the file, from a bass channel created by sound.PlayURL or sound.PlayFile, if available.
* 
* @return {string} Returns a string containing the information.
Returns nil if no information is available.
 */
    public GetTagsVendor(): string;

    /**
     * Returns the current time of the sound channel
     *
     * @return {number} The current time of the stream
     */
    public GetTime(): number;

    /**
     * Returns volume of a sound channel
     *
     * @return {number} The volume of the sound channel
     */
    public GetVolume(): number;

    /**
     * Returns if the sound channel is in 3D mode or not.
     *
     * @return {boolean} Is 3D or not.
     */
    public Is3D(): boolean;

    /**
     * Returns whether the audio stream is block streamed or not.
     *
     * @return {boolean} Is the audio stream block streamed or not.
     */
    public IsBlockStreamed(): boolean;

    /**
     * Returns if the sound channel is looping or not.
     *
     * @return {boolean} Is looping or not.
     */
    public IsLooping(): boolean;

    /**
     * Returns if the sound channel is streamed from the Internet or not.
     *
     * @return {boolean} Is online or not.
     */
    public IsOnline(): boolean;

    /**
     * Returns if the sound channel is valid or not.
     *
     * @return {boolean} Is the sound channel valid or not
     */
    public IsValid(): boolean;

    /**
     * Pauses the stream. It can be started again using IGModAudioChannel:Play
     */
    public Pause(): void;

    /**
     * Starts playing the stream.
     */
    public Play(): void;

    /**
 * Sets 3D cone of the sound channel.
* 
* @arg number innerAngle - The angle of the inside projection cone in degrees.
Range is from 0 (no cone) to 360 (sphere), -1 = leave current.
* @arg number outerAngle - The angle of the outside projection cone in degrees.
Range is from 0 (no cone) to 360 (sphere), -1 = leave current.
* @arg number outerVolume - The delta-volume outside the outer projection cone.
Range is from 0 (silent) to 1 (same as inside the cone), less than 0 = leave current.
 */
    public Set3DCone(innerAngle: number, outerAngle: number, outerVolume: number): void;

    /**
     * Sets the 3D mode of the channel. This will affect IGModAudioChannel:Get3DEnabled but not IGModAudioChannel:Is3D.
     *
     * @arg boolean enable - true to enable, false to disable 3D.
     */
    public Set3DEnabled(enable: boolean): void;

    /**
 * Sets 3D fade distances of a sound channel.
* 
* @arg number min - The minimum distance. The channel's volume is at maximum when the listener is within this distance.
0 or less = leave current.
* @arg number max - The maximum distance. The channel's volume stops decreasing when the listener is beyond this distance.
0 or less = leave current.
 */
    public Set3DFadeDistance(min: number, max: number): void;

    /**
     * Sets the relative volume of the left and right channels.
     *
     * @arg number pan - Relative volume between the left and right channels. -1 means only in left channel, 0 is center and 1 is only in the right channel.
     */
    public SetPan(pan: number): void;

    /**
     * Sets the playback rate of the sound channel. May not work with high values for radio streams.
     *
     * @arg number rate - Playback rate to set to. 1 is normal speed, 0.5 is half the normal speed, etc.
     */
    public SetPlaybackRate(rate: number): void;

    /**
     * Sets position of sound channel in case the sound channel has a 3d option set.
     *
     * @arg Vector pos - The position to put the sound into
     * @arg Vector [dir=Vector( 0, 0, 0 )] - The direction of the sound
     */
    public SetPos(pos: Vector, dir?: Vector): void;

    /**
     * Sets the sound channel to specified time ( Rewind to that position of the song ). Does not work on online radio streams.
     *
     * @arg number secs - The time to set the stream to, in seconds.
     * @arg boolean [dont_decode=false] - Set to true to skip decoding to set time, and instead just seek to it which is faster. Certain streams do not support seeking and have to decode to the given position.
     */
    public SetTime(secs: number, dont_decode?: boolean): void;

    /**
     * Sets the volume of a sound channel
     *
     * @arg number volume - Volume to set. 1 meaning 100% volume, 0.5 is 50% and 3 is 300%, etc.
     */
    public SetVolume(volume: number): void;

    /**
     * Stop the stream. It can be started again using IGModAudioChannel:Play.
     */
    public Stop(): void;
}

declare type IGModAudioChannel = __IGModAudioChannelClass;
