declare class __IMaterialClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the color of the specified pixel of the $basetexture, only works for materials created from PNG files.
     *
     * @arg number x - The X coordinate.
     * @arg number y - The Y coordinate.
     * @return {Color} The color of the pixel as a Color.
     */
    public GetColor(x: number, y: number): Color;

    /**
     * Returns the specified material value as a float, or nil if the value is not set.
     *
     * @arg string materialFloat - The name of the material value.
     * @return {number} float
     */
    public GetFloat(materialFloat: string): number;

    /**
     * Returns the specified material value as a int, rounds the value if its a float, or nil if the value is not set.
     *
     * @arg string materialInt - The name of the material integer.
     * @return {number} The retrieved value as an integer
     */
    public GetInt(materialInt: string): number;

    /**
     * Gets all the key values defined for the material.
     *
     * @return {object} The material's key values.
     */
    public GetKeyValues(): object;

    /**
     * Returns the specified material matrix as a int, or nil if the value is not set or is not a matrix.
     *
     * @arg string materialMatrix - The name of the material matrix.
     * @return {VMatrix} matrix
     */
    public GetMatrix(materialMatrix: string): VMatrix;

    /**
     * Returns the name of the material, in most cases the path.
     *
     * @return {string} Material name/path
     */
    public GetName(): string;

    /**
     * Returns the name of the materials shader.
     *
     * @return {string} shaderName
     */
    public GetShader(): string;

    /**
     * Returns the specified material string, or nil if the value is not set or if the value can not be converted to a string.
     *
     * @arg string materialString - The name of the material string.
     * @return {string} The value as a string
     */
    public GetString(materialString: string): string;

    /**
     * Returns an ITexture based on the passed shader parameter.
     *
     * @arg string param - The shader parameter to retrieve. This should normally be $basetexture.
     * @return {ITexture} The value of the shader parameter. Returns nothing if the param doesn't exist.
     */
    public GetTexture(param: string): ITexture;

    /**
     * Returns the specified material vector, or nil if the value is not set.
     *
     * @arg string materialVector - The name of the material vector.
     * @return {Vector} The color vector
     */
    public GetVector(materialVector: string): Vector;

    /**
     * Returns the specified material vector as a 4 component vector.
     *
     * @arg string name - The name of the material vector to retrieve.
     * @return {number} The x component of the vector.
     * @return {number} The y component of the vector.
     * @return {number} The z component of the vector.
     * @return {number} The w component of the vector.
     */
    public GetVector4D(name: string): LuaMultiReturn<[number, number, number, number]>;

    /**
     * Returns the specified material linear color vector, or nil if the value is not set.
     *
     * @arg string materialVector - The name of the material vector.
     * @return {Vector} The linear color vector
     */
    public GetVectorLinear(materialVector: string): Vector;

    /**
     * Returns the height of the member texture set for $basetexture.
     *
     * @return {number} height
     */
    public Height(): number;

    /**
     * Returns whenever the material is valid, i.e. whether it was not loaded successfully from disk or not.
     *
     * @return {boolean} Is this material the error material? (___error)
     */
    public IsError(): boolean;

    /**
     * Recomputes the material's snapshot. This needs to be called if you have changed variables on your material and it isn't changing.
     */
    public Recompute(): void;

    /**
     * Sets the specified material float to the specified float, does nothing on a type mismatch.
     *
     * @arg string materialFloat - The name of the material float.
     * @arg number float - The new float value.
     */
    public SetFloat(materialFloat: string, float: number): void;

    /**
     * Sets the specified material value to the specified int, does nothing on a type mismatch.
     *
     * @arg string materialInt - The name of the material int.
     * @arg number int - The new int value.
     */
    public SetInt(materialInt: string, int: number): void;

    /**
     * Sets the specified material value to the specified matrix, does nothing on a type mismatch.
     *
     * @arg string materialMatrix - The name of the material int.
     * @arg VMatrix matrix - The new matrix.
     */
    public SetMatrix(materialMatrix: string, matrix: VMatrix): void;

    /**
     * The functionality of this function was removed due to the amount of crashes it caused.
     *
     * @arg string shaderName - Name of the shader
     */
    public SetShader(shaderName: string): void;

    /**
     * Sets the specified material value to the specified string, does nothing on a type mismatch.
     *
     * @arg string materialString - The name of the material string.
     * @arg string string - The new string.
     */
    public SetString(materialString: string, string: string): void;

    /**
     * Sets the specified material texture to the specified texture, does nothing on a type mismatch.
     *
     * @arg string materialTexture - The name of the keyvalue on the material to store the texture on.
     * @arg ITexture texture - The new texture. This can also be a string, the name of the new texture.
     */
    public SetTexture(materialTexture: string, texture: ITexture): void;

    /**
     * Unsets the value for the specified material value.
     *
     * @arg string materialValueName - The name of the material value to be unset.
     */
    public SetUndefined(materialValueName: string): void;

    /**
     * Sets the specified material vector to the specified vector, does nothing on a type mismatch.
     *
     * @arg string MaterialVector - The name of the material vector.
     * @arg Vector vec - The new vector.
     */
    public SetVector(MaterialVector: string, vec: Vector): void;

    /**
     * Sets the specified material vector to the specified 4 component vector, does nothing on a type mismatch.
     *
     * @arg string name - The name of the material vector.
     * @arg number x - The x component of the new vector.
     * @arg number y - The y component of the new vector.
     * @arg number z - The z component of the new vector.
     * @arg number w - The w component of the new vector.
     */
    public SetVector4D(name: string, x: number, y: number, z: number, w: number): void;

    /**
     * Returns the width of the member texture set for $basetexture.
     *
     * @return {number} width
     */
    public Width(): number;
}

declare type IMaterial = __IMaterialClass;
