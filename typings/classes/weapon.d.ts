declare class __WeaponClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns whether the weapon allows to being switched from when a better ( Weapon:GetWeight ) weapon is being picked up.
     *
     * @return {boolean} Whether the weapon allows to being switched from.
     */
    public AllowsAutoSwitchFrom(): boolean;

    /**
     * Returns whether the weapon allows to being switched to when a better ( Weapon:GetWeight ) weapon is being picked up.
     *
     * @return {boolean} Whether the weapon allows to being switched to.
     */
    public AllowsAutoSwitchTo(): boolean;

    /**
 * Calls a SWEP function on client.
* 
* @arg string functionName - Name of function to call. If you want to call SWEP:MyFunc() on client, you type in "MyFunc"
* @arg string arguments - Arguments for the function, separated by spaces.
Only the second argument is passed as argument and must be a string
 */
    public CallOnClient(functionName: string, arguments: string): void;

    /**
     * Returns how much primary ammo is in the magazine.
     *
     * @return {number} The amount of primary ammo in the magazine.
     */
    public Clip1(): number;

    /**
     * Returns how much secondary ammo is in the magazine.
     *
     * @return {number} The amount of secondary ammo in the magazine.
     */
    public Clip2(): number;

    /**
     * Forces the weapon to reload while playing given animation.
     *
     * @arg number act - Sequence to use as reload animation. Uses the Enums/ACT.
     * @return {boolean} Did reloading actually take place
     */
    public DefaultReload(act: number): boolean;

    /**
     * Returns the sequence enumeration number that the weapon is playing.
     *
     * @return {number} Current activity, see Enums/ACT. Returns 0 if the weapon doesn't have active sequence.
     */
    public GetActivity(): number;

    /**
     * Returns the hold type of the weapon.
     *
     * @return {string} The hold type of the weapon. You can find a list of default hold types here.
     */
    public GetHoldType(): string;

    /**
     * Returns maximum primary clip size
     *
     * @return {number} Maximum primary clip size
     */
    public GetMaxClip1(): number;

    /**
     * Returns maximum secondary clip size
     *
     * @return {number} Maximum secondary clip size
     */
    public GetMaxClip2(): number;

    /**
     * Gets the next time the weapon can primary fire. ( Can call WEAPON:PrimaryAttack )
     *
     * @return {number} The time, relative to CurTime
     */
    public GetNextPrimaryFire(): number;

    /**
     * Gets the next time the weapon can secondary fire. ( Can call WEAPON:SecondaryAttack )
     *
     * @return {number} The time, relative to CurTime
     */
    public GetNextSecondaryFire(): number;

    /**
     * Gets the primary ammo type of the given weapon.
     *
     * @return {number} The ammo type ID, or -1 if not found.
     */
    public GetPrimaryAmmoType(): number;

    /**
     * Returns the non-internal name of the weapon, that should be for displaying.
     *
     * @return {string} The "nice" name of the weapon.
     */
    public GetPrintName(): string;

    /**
     * Gets the ammo type of the given weapons secondary fire.
     *
     * @return {number} The secondary ammo type ID, or -1 if not found.
     */
    public GetSecondaryAmmoType(): number;

    /**
     * Returns the slot of the weapon.
     *
     * @return {number} The slot of the weapon.
     */
    public GetSlot(): number;

    /**
     * Returns slot position of the weapon
     *
     * @return {number} The slot position of the weapon
     */
    public GetSlotPos(): number;

    /**
     * Returns the view model of the weapon.
     *
     * @return {string} The view model of the weapon.
     */
    public GetWeaponViewModel(): string;

    /**
     * Returns the world model of the weapon.
     *
     * @return {string} The world model of the weapon.
     */
    public GetWeaponWorldModel(): string;

    /**
     * Returns the "weight" of the weapon, which is used when deciding which Weapon is better by the engine.
     *
     * @return {number} The weapon "weight".
     */
    public GetWeight(): number;

    /**
     * Returns whether the weapon has ammo left or not. It will return false when there's no ammo left in the magazine and when there's no reserve ammo left.
     *
     * @return {boolean} Whether the weapon has ammo or not.
     */
    public HasAmmo(): boolean;

    /**
     * Returns whenever the weapon is carried by the local player.
     *
     * @return {boolean} Is the weapon is carried by the local player or not
     */
    public IsCarriedByLocalPlayer(): boolean;

    /**
     * Checks if the weapon is a SWEP or a built-in weapon.
     *
     * @return {boolean} Returns true if weapon is scripted ( SWEP ), false if not ( A built-in HL2 weapon )
     */
    public IsScripted(): boolean;

    /**
     * Returns whether the weapon is visible. The term visibility is not exactly what gets checked here, first it checks if the owner is a player, then checks if the active view model has EF_NODRAW flag NOT set.
     *
     * @return {boolean} Is visible or not
     */
    public IsWeaponVisible(): boolean;

    /**
     * Returns the time since this weapon last fired a bullet with Entity:FireBullets in seconds. It is not networked.
     *
     * @return {number} The time in seconds when the last bullet was fired.
     */
    public LastShootTime(): number;

    /**
     * Forces weapon to play activity/animation.
     *
     * @arg number act - Activity to play. See the Enums/ACT (specifically ACT_VM_).
     */
    public SendWeaponAnim(act: number): void;

    /**
     * Sets the activity the weapon is playing.
     *
     * @arg number act - The new activity to set, see Enums/ACT.
     */
    public SetActivity(act: number): void;

    /**
     * Lets you change the number of bullets in the given weapons primary clip.
     *
     * @arg number ammo - The amount of bullets the clip should contain
     */
    public SetClip1(ammo: number): void;

    /**
     * Lets you change the number of bullets in the given weapons secondary clip.
     *
     * @arg number ammo - The amount of bullets the clip should contain
     */
    public SetClip2(ammo: number): void;

    /**
     * Sets the hold type of the weapon. This function also calls WEAPON:SetWeaponHoldType and properly networks it to all clients.
     *
     * @arg string name - Name of the hold type. You can find all default hold types here
     */
    public SetHoldType(name: string): void;

    /**
     * Sets the time since this weapon last fired in seconds. Used in conjunction with Weapon:LastShootTime
     *
     * @arg number [time=NaN] - The time in seconds when the last time the weapon was fired.
     */
    public SetLastShootTime(time?: number): void;

    /**
     * Sets when the weapon can fire again. Time should be based on CurTime.
     *
     * @arg number time - Time when player should be able to use primary fire again
     */
    public SetNextPrimaryFire(time: number): void;

    /**
     * Sets when the weapon can alt-fire again. Time should be based on CurTime.
     *
     * @arg number time - Time when player should be able to use secondary fire again
     */
    public SetNextSecondaryFire(time: number): void;
}

declare type Weapon = __WeaponClass & Entity;
