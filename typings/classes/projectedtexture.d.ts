declare class __ProjectedTextureClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Returns the angle of the ProjectedTexture, which were previously set by ProjectedTexture:SetAngles
     *
     * @return {Angle} The angles of the ProjectedTexture.
     */
    public GetAngles(): Angle;

    /**
     * Returns the brightness of the ProjectedTexture, which was previously set by ProjectedTexture:SetBrightness
     *
     * @return {number} The brightness of the ProjectedTexture.
     */
    public GetBrightness(): number;

    /**
     * Returns the color of the ProjectedTexture, which was previously set by ProjectedTexture:SetColor.
     *
     * @return {Color} Color, the color of the ProjectedTexture.
     */
    public GetColor(): Color;

    /**
     * Returns the constant attenuation of the projected texture, which can also be set by ProjectedTexture:SetConstantAttenuation.
     *
     * @return {number} The constant attenuation
     */
    public GetConstantAttenuation(): number;

    /**
     * Returns whether shadows are enabled for this ProjectedTexture, which was previously set by ProjectedTexture:SetEnableShadows
     *
     * @return {boolean} Whether shadows are enabled.
     */
    public GetEnableShadows(): boolean;

    /**
     * Returns the projection distance of the ProjectedTexture, which was previously set by ProjectedTexture:SetFarZ
     *
     * @return {number} The projection distance of the ProjectedTexture.
     */
    public GetFarZ(): number;

    /**
     * Returns the horizontal FOV of the ProjectedTexture, which was previously set by ProjectedTexture:SetHorizontalFOV or ProjectedTexture:SetFOV
     *
     * @return {number} The horizontal FOV of the ProjectedTexture.
     */
    public GetHorizontalFOV(): number;

    /**
     * Returns the linear attenuation of the projected texture, which can also be set by ProjectedTexture:SetLinearAttenuation.
     *
     * @return {number} The linear attenuation.
     */
    public GetLinearAttenuation(): number;

    /**
     * Returns the NearZ value of the ProjectedTexture, which was previously set by ProjectedTexture:SetNearZ
     *
     * @return {number} NearZ of the ProjectedTexture.
     */
    public GetNearZ(): number;

    /**
     * Returns the current orthographic settings of the Projected Texture. To set these values, use ProjectedTexture:SetOrthographic.
     *
     * @return {boolean} Whether or not this projected texture is orthographic. When false, nothing else is returned.
     * @return {number} left
     * @return {number} top
     * @return {number} right
     * @return {number} bottom
     */
    public GetOrthographic(): LuaMultiReturn<[boolean, number, number, number, number]>;

    /**
     * Returns the position of the ProjectedTexture, which was previously set by ProjectedTexture:SetPos
     *
     * @return {Vector} The position of the ProjectedTexture.
     */
    public GetPos(): Vector;

    /**
     * Returns the quadratic attenuation of the projected texture, which can also be set by ProjectedTexture:SetQuadraticAttenuation.
     *
     * @return {number} The quadratic attenuation
     */
    public GetQuadraticAttenuation(): number;

    /**
     * Returns the shadow depth bias of the projected texture.
     *
     * @return {number} The current shadow depth bias.
     */
    public GetShadowDepthBias(): number;

    /**
     * Returns the shadow "filter size" of the projected texture. 0 is fully pixelated, higher values will blur the shadow more.
     *
     * @return {number} The current shadow filter size.
     */
    public GetShadowFilter(): number;

    /**
     * Returns the shadow depth slope scale bias of the projected texture.
     *
     * @return {number} The current shadow depth slope scale bias.
     */
    public GetShadowSlopeScaleDepthBias(): number;

    /**
     * Returns the target entity of this projected texture.
     *
     * @return {Entity} The current target entity.
     */
    public GetTargetEntity(): Entity;

    /**
     * Returns the texture of the ProjectedTexture, which was previously set by ProjectedTexture:SetTexture
     *
     * @return {ITexture} The texture of the ProjectedTexture.
     */
    public GetTexture(): ITexture;

    /**
     * Returns the texture frame of the ProjectedTexture, which was previously set by ProjectedTexture:SetTextureFrame
     *
     * @return {number} The texture frame.
     */
    public GetTextureFrame(): number;

    /**
     * Returns the vertical FOV of the ProjectedTexture, which was previously set by ProjectedTexture:SetVerticalFOV or ProjectedTexture:SetFOV
     *
     * @return {number} The vertical FOV of the ProjectedTexture.
     */
    public GetVerticalFOV(): number;

    /**
     * Returns true if the projected texture is valid (i.e. has not been removed), false otherwise.
     *
     * @return {boolean} Whether the projected texture is valid.
     */
    public IsValid(): boolean;

    /**
     * Removes the projected texture. After calling this, ProjectedTexture:IsValid will return false, and any hooks with the projected texture as the identifier will be automatically deleted.
     */
    public Remove(): void;

    /**
     * Sets the angles (direction) of the projected texture.
     *
     * @arg Angle angle
     */
    public SetAngles(angle: Angle): void;

    /**
     * Sets the brightness of the projected texture.
     *
     * @arg number brightness - The brightness to give the projected texture.
     */
    public SetBrightness(brightness: number): void;

    /**
 * Sets the color of the projected texture.
* 
* @arg Color color - Must be a Color.
Unlike other projected textures, this color can only go up to 255.
 */
    public SetColor(color: Color): void;

    /**
     * Sets the constant attenuation of the projected texture.
     *
     * @arg number constAtten
     */
    public SetConstantAttenuation(constAtten: number): void;

    /**
     * Enable or disable shadows cast from the projected texture.
     *
     * @arg boolean newState
     */
    public SetEnableShadows(newState: boolean): void;

    /**
     * Sets the distance at which the projected texture ends.
     *
     * @arg number farZ
     */
    public SetFarZ(farZ: number): void;

    /**
     * Sets the angle of projection.
     *
     * @arg number fov - Must be higher than 0 and lower than 180
     */
    public SetFOV(fov: number): void;

    /**
     * Sets the horizontal angle of projection without affecting the vertical angle.
     *
     * @arg number hFOV - The new horizontal Field Of View for the projected texture. Must be in range between 0 and 180.
     */
    public SetHorizontalFOV(hFOV: number): void;

    /**
     * Sets the linear attenuation of the projected texture.
     *
     * @arg number linearAtten
     */
    public SetLinearAttenuation(linearAtten: number): void;

    /**
     * Sets the distance at which the projected texture begins its projection.
     *
     * @arg number nearZ
     */
    public SetNearZ(nearZ: number): void;

    /**
     * Changes the current projected texture between orthographic and perspective projection.
     *
     * @arg boolean orthographic - When false, all other arguments are ignored and the texture is reset to perspective projection.
     * @arg number left - The amount of units left from the projected texture's origin to project.
     * @arg number top - The amount of units upwards from the projected texture's origin to project.
     * @arg number right - The amount of units right from the projected texture's origin to project.
     * @arg number bottom - The amount of units downwards from the projected texture's origin to project.
     */
    public SetOrthographic(orthographic: boolean, left: number, top: number, right: number, bottom: number): void;

    /**
     * Move the Projected Texture to the specified position.
     *
     * @arg Vector position
     */
    public SetPos(position: Vector): void;

    /**
     * Sets the quadratic attenuation of the projected texture.
     *
     * @arg number quadAtten
     */
    public SetQuadraticAttenuation(quadAtten: number): void;

    /**
     * Sets the shadow depth bias of the projected texture.
     *
     * @arg number bias - The shadow depth bias to set.
     */
    public SetShadowDepthBias(bias: number): void;

    /**
     * Sets the shadow "filter size" of the projected texture. 0 is fully pixelated, higher values will blur the shadow more. The initial value is the value of r_projectedtexture_filter ConVar.
     *
     * @arg number filter - The shadow filter size to set.
     */
    public SetShadowFilter(filter: number): void;

    /**
     * Sets the shadow depth slope scale bias of the projected texture.
     *
     * @arg number bias - The shadow depth slope scale bias to set.
     */
    public SetShadowSlopeScaleDepthBias(bias: number): void;

    /**
     * Sets the target entity for this projected texture, meaning it will only be lighting the given entity and the world.
     *
     * @arg Entity [target=NULL] - The target entity, or NULL to reset.
     */
    public SetTargetEntity(target?: Entity): void;

    /**
     * Sets the texture to be projected.
     *
     * @arg string texture - The name of the texture. Can also be an ITexture.
     */
    public SetTexture(texture: string): void;

    /**
     * For animated textures, this will choose which frame in the animation will be projected.
     *
     * @arg number frame - The frame index to use.
     */
    public SetTextureFrame(frame: number): void;

    /**
     * Sets the vertical angle of projection without affecting the horizontal angle.
     *
     * @arg number vFOV - The new vertical Field Of View for the projected texture. Must be in range between 0 and 180.
     */
    public SetVerticalFOV(vFOV: number): void;

    /**
     * Updates the Projected Light and applies all previously set parameters.
     */
    public Update(): void;
}

// Allow the class to be instantiated with a function call:

/**
 * Creates a new ProjectedTexture.
 *
 * @return {ProjectedTexture} Newly created projected texture.
 */
declare function ProjectedTexture(): ProjectedTexture;

declare type ProjectedTexture = __ProjectedTextureClass;
