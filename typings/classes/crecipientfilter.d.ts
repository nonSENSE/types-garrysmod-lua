declare class __CRecipientFilterClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds all players to the recipient filter.
     */
    public AddAllPlayers(): void;

    /**
     * Adds all players that are in the same PAS (Potentially Audible Set) as this position.
     *
     * @arg Vector pos - A position that players may be able to hear, usually the position of an entity the sound is playing played from.
     */
    public AddPAS(pos: Vector): void;

    /**
     * Adds a player to the recipient filter
     *
     * @arg Player Player - Player to add to the recipient filter.
     */
    public AddPlayer(Player: Player): void;

    /**
     * Adds all players that are in the same PVS(Potential Visibility Set) as this position.
     *
     * @arg Vector Position - PVS position that players may be able to see.
     */
    public AddPVS(Position: Vector): void;

    /**
     * Adds all players that are on the given team to the filter.
     *
     * @arg number teamid - Team index to add players from.
     */
    public AddRecipientsByTeam(teamid: number): void;

    /**
     * Returns the number of valid players in the recipient filter.
     *
     * @return {number} Number of valid players in the recipient filter.
     */
    public GetCount(): number;

    /**
     * Returns a table of all valid players currently in the recipient filter.
     *
     * @return {object} A table of all valid players currently in the recipient filter.
     */
    public GetPlayers(): object;

    /**
     * Removes all players from the recipient filter.
     */
    public RemoveAllPlayers(): void;

    /**
     * Removes all players from the filter that are in Potentially Audible Set for given position.
     *
     * @arg Vector position - The position to test
     */
    public RemovePAS(position: Vector): void;

    /**
     * Removes the player from the recipient filter.
     *
     * @arg Player Player - The player that should be in the recipient filter if you call this function.
     */
    public RemovePlayer(Player: Player): void;

    /**
     * Removes all players that can see this PVS from the recipient filter.
     *
     * @arg Vector pos - Position that players may be able to see.
     */
    public RemovePVS(pos: Vector): void;

    /**
     * Removes all players that are on the given team from the filter.
     *
     * @arg number teamid - Team index to remove players from.
     */
    public RemoveRecipientsByTeam(teamid: number): void;

    /**
     * Removes all players that are not on the given team from the filter.
     *
     * @arg number teamid - Team index.
     */
    public RemoveRecipientsNotOnTeam(teamid: number): void;
}

declare type CRecipientFilter = __CRecipientFilterClass;
