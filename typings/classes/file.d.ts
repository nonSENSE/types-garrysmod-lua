declare class __FileClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Dumps the file changes to disk and closes the file handle which makes the handle useless.
     */
    public Close(): void;

    /**
     * Returns whether the File object has reached the end of file or not.
     *
     * @return {boolean} Whether the file has reached end or not.
     */
    public EndOfFile(): boolean;

    /**
     * Dumps the file changes to disk and saves the file.
     */
    public Flush(): void;

    /**
     * Reads the specified amount of chars and returns them as a binary string.
     *
     * @arg number [length=NaN] - Reads the specified amount of chars. If not set, will read the entire file.
     * @return {string}
     */
    public Read(length?: number): string;

    /**
     * Reads one byte of the file and returns whether that byte was not 0.
     *
     * @return {boolean} val
     */
    public ReadBool(): boolean;

    /**
     * Reads one unsigned 8-bit integer from the file.
     *
     * @return {number} The unsigned 8-bit integer from the file.
     */
    public ReadByte(): number;

    /**
     * Reads an 8-byte little-endian IEEE-754 floating point double from the file.
     *
     * @return {number} The double-precision floating point value read from the file.
     */
    public ReadDouble(): number;

    /**
     * Reads an IEEE 754 little-endian 4-byte float from the file.
     *
     * @return {number} The read value
     */
    public ReadFloat(): number;

    /**
     * Returns the contents of the file from the current position up until the end of the current line.
     *
     * @return {string} The string of data from the read line.
     */
    public ReadLine(): string;

    /**
     * Reads a signed little-endian 32-bit integer from the file.
     *
     * @return {number} A signed 32-bit integer
     */
    public ReadLong(): number;

    /**
     * Reads a signed little-endian 16-bit integer from the file.
     *
     * @return {number} int16
     */
    public ReadShort(): number;

    /**
     * Reads an unsigned little-endian 32-bit integer from the file.
     *
     * @return {number} An unsigned 32-bit integer
     */
    public ReadULong(): number;

    /**
     * Reads an unsigned little-endian 16-bit integer from the file.
     *
     * @return {number} The 16-bit integer
     */
    public ReadUShort(): number;

    /**
     * Sets the file pointer to the specified position.
     *
     * @arg number pos - Pointer position, in bytes.
     */
    public Seek(pos: number): void;

    /**
     * Returns the size of the file in bytes.
     *
     * @return {number}
     */
    public Size(): number;

    /**
     * Moves the file pointer by the specified amount of chars.
     *
     * @arg number amount - The amount of chars to skip, can be negative to skip backwards.
     * @return {number} amount
     */
    public Skip(amount: number): number;

    /**
     * Returns the current position of the file pointer.
     *
     * @return {number} pos
     */
    public Tell(): number;

    /**
     * Writes the given string into the file.
     *
     * @arg string data - Binary data to write to the file.
     */
    public Write(data: string): void;

    /**
     * Writes a boolean value to the file as one byte.
     *
     * @arg boolean bool - The bool to be written to the file.
     */
    public WriteBool(bool: boolean): void;

    /**
     * Write an 8-bit unsigned integer to the file.
     *
     * @arg number uint8 - The 8-bit unsigned integer to be written to the file.
     */
    public WriteByte(uint8: number): void;

    /**
     * Writes an 8-byte little-endian IEEE-754 floating point double to the file.
     *
     * @arg number double - The double to be written to the file.
     */
    public WriteDouble(double: number): void;

    /**
     * Writes an IEEE 754 little-endian 4-byte float to the file.
     *
     * @arg number float - The float to be written to the file.
     */
    public WriteFloat(float: number): void;

    /**
     * Writes a signed little-endian 32-bit integer to the file.
     *
     * @arg number int32 - The 32-bit signed integer to be written to the file.
     */
    public WriteLong(int32: number): void;

    /**
     * Writes a signed little-endian 16-bit integer to the file.
     *
     * @arg number int16 - The 16-bit signed integer to be written to the file.
     */
    public WriteShort(int16: number): void;

    /**
     * Writes an unsigned little-endian 32-bit integer to the file.
     *
     * @arg number uint32 - The unsigned 32-bit integer to be written to the file.
     */
    public WriteULong(uint32: number): void;

    /**
     * Writes an unsigned little-endian 16-bit integer to the file.
     *
     * @arg number uint16 - The unsigned 16-bit integer to the file.
     */
    public WriteUShort(uint16: number): void;
}

declare type File = __FileClass;
