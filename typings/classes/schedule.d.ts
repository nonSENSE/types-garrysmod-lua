declare class __ScheduleClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds a task to the schedule. See also Schedule:AddTaskEx if you wish to customize task start and run function names.
     *
     * @arg string taskname - Custom task name
     * @arg any taskdata - Task data to be passed into the NPC's functions
     */
    public AddTask(taskname: string, taskdata: any): void;

    /**
     * Adds a task to the schedule with completely custom function names.
     *
     * @arg string start - The full name of a function on the entity's table to be ran when the task is started.
     * @arg string run - The full name of a function on the entity's table to be ran when the task is continuously running.
     * @arg number data - Task data to be passed into the NPC's functions
     */
    public AddTaskEx(start: string, run: string, data: number): void;

    /**
     * Adds an engine task to the schedule.
     *
     * @arg string taskname - Task name, see ai_task.h
     * @arg number taskdata - Task data, can be a float.
     */
    public EngTask(taskname: string, taskdata: number): void;

    /**
     * Returns the task at the given index.
     *
     * @arg number num - Task index.
     */
    public GetTask(num: number): void;

    /**
     * Initialises the Schedule. Called by ai_schedule.New when the Schedule is created.
     *
     * @arg string debugName - The name passed from ai_schedule.New.
     */
    public Init(debugName: string): void;

    /**
     * Returns the number of tasks in the schedule.
     *
     * @return {number} The number of tasks in this schedule.
     */
    public NumTasks(): number;
}

declare type Schedule = __ScheduleClass;
