declare class __PanelClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Set to true by the dragndrop system when the panel is being drawn for the drag'n'drop.
     *
     * @return {boolean} Set to true if drawing for the transparent dragging render.
     */
    public PaintingDragging(): boolean;

    /**
 * Adds the specified object to the panel.
* 
* @arg Panel object - The panel to be added (parented). Can also be:

string Class Name - creates panel with the specified name and adds it to the panel.
table PANEL table - creates a panel from table and adds it to the panel.
* @return {Panel} New panel
 */
    public Add(object: Panel): Panel;

    /**
     * This function does nothing.
     */
    public AddText(): void;

    /**
     * Aligns the panel on the bottom of its parent with the specified offset.
     *
     * @arg number [offset=0] - The align offset.
     */
    public AlignBottom(offset?: number): void;

    /**
     * Aligns the panel on the left of its parent with the specified offset.
     *
     * @arg number [offset=0] - The align offset.
     */
    public AlignLeft(offset?: number): void;

    /**
     * Aligns the panel on the right of its parent with the specified offset.
     *
     * @arg number [offset=0] - The align offset.
     */
    public AlignRight(offset?: number): void;

    /**
     * Aligns the panel on the top of its parent with the specified offset.
     *
     * @arg number [offset=0] - The align offset.
     */
    public AlignTop(offset?: number): void;

    /**
 * Uses animation to transition the current alpha value of a panel to a new alpha, over a set period of time and after a specified delay.
* 
* @arg number alpha - The alpha value (0-255) to approach.
* @arg number duration - The time in seconds it should take to reach the alpha.
* @arg number [delay=0] - The delay before the animation starts.
* @arg GMLua.CallbackNoContext callback - The function to be called once the animation finishes. Arguments are:

table animData - The AnimationData that was used. See Structures/AnimationData
Panel pnl - The panel object whose alpha was changed.
 */
    public AlphaTo(alpha: number, duration: number, delay?: number, callback?: GMLua.CallbackNoContext): void;

    /**
     * Performs the per-frame operations required for panel animations.
     */
    public AnimationThinkInternal(): void;

    /**
     * Returns the SysTime value when all animations for this panel object will end.
     *
     * @return {number} The system time value when all animations will end for this panel.
     */
    public AnimTail(): number;

    /**
     * Appends text to a RichText element. This does not automatically add a new line.
     *
     * @arg string txt - The text to append (add on).
     */
    public AppendText(txt: string): void;

    /**
     * Used by Panel:LoadGWENFile and Panel:LoadGWENString to apply a GWEN controls table to a panel object.
     *
     * @arg object GWENTable - The GWEN controls table to apply to the panel.
     */
    public ApplyGWEN(GWENTable: object): void;

    /**
     * Centers the panel on its parent.
     */
    public Center(): void;

    /**
     * Centers the panel horizontally with specified fraction.
     *
     * @arg number [fraction=0.5] - The center fraction.
     */
    public CenterHorizontal(fraction?: number): void;

    /**
     * Centers the panel vertically with specified fraction.
     *
     * @arg number [fraction=0.5] - The center fraction.
     */
    public CenterVertical(fraction?: number): void;

    /**
     * Returns the amount of children of the of panel.
     *
     * @return {number} The amount of children the panel has.
     */
    public ChildCount(): number;

    /**
     * Returns the width and height of the space between the position of the panel (upper-left corner) and the max bound of the children panels (farthest reaching lower-right corner).
     *
     * @return {number} The children size width.
     * @return {number} The children size height.
     */
    public ChildrenSize(): LuaMultiReturn<[number, number]>;

    /**
     * Marks all of the panel's children for deletion.
     */
    public Clear(): void;

    /**
     * Fades panels color to specified one. It won't work unless panel has SetColor function.
     *
     * @arg object color - The color to fade to
     * @arg number length - Length of the animation
     * @arg number delay - Delay before start fading
     * @arg GMLua.CallbackNoContext callback - Function to execute when finished
     */
    public ColorTo(color: object, length: number, delay: number, callback: GMLua.CallbackNoContext): void;

    /**
     * Sends an action command signal to the panel. The response is handled by PANEL:ActionSignal.
     *
     * @arg string command - The command to send to the panel.
     */
    public Command(command: string): void;

    /**
     * Updates a panel object's associated console variable. This must first be set up with Derma_Install_Convar_Functions, and have a ConVar set using Panel:SetConVar.
     *
     * @arg string newValue - The new value to set the associated console variable to.
     */
    public ConVarChanged(newValue: string): void;

    /**
     * A think hook for Panels using ConVars as a value. Call it in the Think hook. Sets the panel's value should the convar change.
     */
    public ConVarNumberThink(): void;

    /**
     * A think hook for panels using ConVars as a value. Call it in the Think hook. Sets the panel's value should the convar change.
     */
    public ConVarStringThink(): void;

    /**
     * Gets the size, position and dock state of the passed panel object, and applies it to this one.
     *
     * @arg Panel srcPanel - The panel to copy the boundary and dock settings from.
     */
    public CopyBase(srcPanel: Panel): void;

    /**
     * Copies position and size of the panel.
     *
     * @arg Panel base - The panel to copy size and position from.
     */
    public CopyBounds(base: Panel): void;

    /**
     * Copies the height of the panel.
     *
     * @arg Panel base - Panel to copy the height from.
     */
    public CopyHeight(base: Panel): void;

    /**
     * Copies the position of the panel.
     *
     * @arg Panel base - Panel to position the width from.
     */
    public CopyPos(base: Panel): void;

    /**
     * Performs the "CONTROL + C" key combination effect ( Copy selection to clipboard ) on selected text.
     */
    public CopySelected(): void;

    /**
     * Copies the width of the panel.
     *
     * @arg Panel base - Panel to copy the width from.
     */
    public CopyWidth(base: Panel): void;

    /**
     * Returns the cursor position relative to the top left of the panel.
     *
     * @return {number} X coordinate of the cursor, relative to the top left of the panel.
     * @return {number} Y coordinate of the cursor, relative to the top left of the panel.
     */
    public CursorPos(): LuaMultiReturn<[number, number]>;

    /**
     * Performs the "CONTROL + X" ( delete text and copy it to clipboard buffer ) action on selected text.
     */
    public CutSelected(): void;

    /**
     * Deletes a cookie value using the panel's cookie name ( Panel:GetCookieName ) and the passed extension.
     *
     * @arg string cookieName - The unique cookie name to delete.
     */
    public DeleteCookie(cookieName: string): void;

    /**
     * Resets the panel object's Panel:SetPos method and removes its animation table (Panel.LerpAnim). This effectively undoes the changes made by Panel:LerpPositions.
     */
    public DisableLerp(): void;

    /**
     * Returns the linear distance from the center of this panel object and another. Both panels must have the same parent for this function to work properly.
     *
     * @arg Panel tgtPanel - The target object with which to compare position.
     * @return {number} The linear (straight-line) distance between the center of the two objects.
     */
    public Distance(tgtPanel: Panel): number;

    /**
     * Returns the distance between the center of this panel object and a specified point local to the parent panel.
     *
     * @arg number posX - The horizontal (x) position in pixels of the point to compare with. Local to the parent panel, or container, not the panel the function is called on.
     * @arg number posY - The vertical (y) position in pixels of the point to compare with. Local to the parent panel, or container, not the panel the function is called on.
     * @return {number} The linear (straight-line) distance between the specified point local to parent panel and the center of this panel object.
     */
    public DistanceFrom(posX: number, posY: number): number;

    /**
     * Sets the dock type for the panel, making the panel "dock" in a certain direction, modifying it's position and size.
     *
     * @arg number dockType - Dock type using Enums/DOCK.
     */
    public Dock(dockType: number): void;

    /**
     * Sets the dock margin of the panel.
     *
     * @arg number marginLeft - The left margin to the parent.
     * @arg number marginTop - The top margin to the parent.
     * @arg number marginRight - The right margin to the parent.
     * @arg number marginBottom - The bottom margin to the parent.
     */
    public DockMargin(marginLeft: number, marginTop: number, marginRight: number, marginBottom: number): void;

    /**
     * Sets the dock padding of the panel.
     *
     * @arg number paddingLeft - The left padding to the parent.
     * @arg number paddingTop - The top padding to the parent.
     * @arg number paddingRight - The right padding to the parent.
     * @arg number paddingBottom - The bottom padding to the parent.
     */
    public DockPadding(paddingLeft: number, paddingTop: number, paddingRight: number, paddingBottom: number): void;

    /**
     * Makes the panel "lock" the screen until it is removed. All input will be directed to the given panel.
     */
    public DoModal(): void;

    /**
     * Called by Panel:DragMouseRelease when a user clicks one mouse button whilst dragging with another.
     *
     * @return {boolean} Always returns true.
     */
    public DragClick(): boolean;

    /**
     * Called by dragndrop.HoverThink to perform actions on an object that is dragged and hovered over another.
     *
     * @arg number HoverTime - If this time is greater than 0.1, PANEL:DragHoverClick is called, passing it as an argument.
     */
    public DragHover(HoverTime: number): void;

    /**
     * Called to end a drag and hover action. This resets the panel's PANEL:PaintOver method, and is primarily used by dragndrop.StopDragging.
     */
    public DragHoverEnd(): void;

    /**
     * Called to inform the dragndrop that a mouse button is being held down on a panel object.
     *
     * @arg number mouseCode - The code for the mouse button pressed, passed by, for example, PANEL:OnMousePressed. See the Enums/MOUSE.
     */
    public DragMousePress(mouseCode: number): void;

    /**
     * Called to inform the dragndrop that a mouse button has been depressed on a panel object.
     *
     * @arg number mouseCode - The code for the mouse button pressed, passed by, for example, PANEL:OnMouseReleased. See the Enums/MOUSE.
     * @return {boolean} true if an object was being dragged, otherwise false.
     */
    public DragMouseRelease(mouseCode: number): boolean;

    /**
     * Called to draw the drop target when an object is being dragged across another. See Panel:SetDropTarget.
     *
     * @arg number x - The x coordinate of the top-left corner of the drop area.
     * @arg number y - The y coordinate of the top-left corner of the drop area.
     * @arg number width - The width of the drop area.
     * @arg number height - The height of the drop area.
     */
    public DrawDragHover(x: number, y: number, width: number, height: number): void;

    /**
     * Draws a coloured rectangle to fill the panel object this method is called on. The colour is set using surface.SetDrawColor. This should only be called within the object's PANEL:Paint or PANEL:PaintOver hooks, as a shortcut for surface.DrawRect.
     */
    public DrawFilledRect(): void;

    /**
     * Draws a hollow rectangle the size of the panel object this method is called on, with a border width of 1 px. The border colour is set using surface.SetDrawColor. This should only be called within the object's PANEL:Paint or PANEL:PaintOver hooks, as a shortcut for surface.DrawOutlinedRect.
     */
    public DrawOutlinedRect(): void;

    /**
     * Used to draw the magenta highlight colour of a panel object when it is selected. This should be called in the object's PANEL:PaintOver hook. Once this is implemented, the highlight colour will be displayed only when the object is selectable and selected. This is achieved using Panel:SetSelectable and Panel:SetSelected respectively.
     */
    public DrawSelections(): void;

    /**
     * Used to draw the text in a DTextEntry within a derma skin. This should be called within the SKIN:PaintTextEntry skin hook.
     *
     * @arg object textCol - The colour of the main text.
     * @arg object highlightCol - The colour of the selection highlight (when selecting text).
     * @arg object cursorCol - The colour of the text cursor (or caret).
     */
    public DrawTextEntryText(textCol: object, highlightCol: object, cursorCol: object): void;

    /**
     * Draws a textured rectangle to fill the panel object this method is called on. The texture is set using surface.SetTexture or surface.SetMaterial. This should only be called within the object's PANEL:Paint or PANEL:PaintOver hooks, as a shortcut for surface.DrawTexturedRect.
     */
    public DrawTexturedRect(): void;

    /**
     * Makes this panel droppable. This is used with Panel:Receiver to create drag and drop events.
     *
     * @arg string name - Name of your droppable panel
     * @return {object} Blank table stored on the panel itself under pnl.m_DragSlot[ name ]. Is reset every time this function is called and does not appear to be used or exposed anywhere else.
     */
    public Droppable(name: string): object;

    /**
     * Completes a box selection. If the end point of the selection box is within the selection canvas, mouse capture is disabled for the panel object, and the selected state of each child object within the selection box is toggled.
     *
     * @return {boolean} Whether the end point of the selection box was within the selection canvas.
     */
    public EndBoxSelection(): boolean;

    /**
     * Used to run commands within a DHTML window.
     *
     * @arg string cmd - The command to be run.
     */
    public Exec(cmd: string): void;

    /**
     * Finds a panel in its children(and sub children) with the given name.
     *
     * @arg string panelName - The name of the panel that should be found.
     * @return {Panel} foundPanel
     */
    public Find(panelName: string): Panel;

    /**
     * Focuses the next panel in the focus queue.
     */
    public FocusNext(): void;

    /**
     * Focuses the previous panel in the focus queue.
     */
    public FocusPrevious(): void;

    /**
     * Returns the alpha multiplier for this panel.
     *
     * @return {number} alphaMul
     */
    public GetAlpha(): number;

    /**
     * Returns the background color of a panel such as a RichText, Label or DColorCube.
     *
     * @return {number} The Color structure
     */
    public GetBGColor(): number;

    /**
     * Returns the position and size of the panel.
     *
     * @return {number} The x coordinate of the panel, relative to its parent's top left.
     * @return {number} The y coordinate of the panel, relative to its parent's top left.
     * @return {number} The width of the panel.
     * @return {number} The height of the panel.
     */
    public GetBounds(): LuaMultiReturn<[number, number, number, number]>;

    /**
     * Returns the position/offset of the caret (or text cursor) in a text-based panel object.
     *
     * @return {number} The caret position/offset from the start of the text. A value of 0 means the caret sits before the first character.
     */
    public GetCaretPos(): number;

    /**
 * Gets a child by its index.
* 
* @arg number childIndex - The index of the child to get.
This index starts at 0, except when you use this on a DMenu.
 */
    public GetChild(childIndex: number): void;

    /**
     * Gets a child object's position relative to this panel object. The number of levels is not relevant; the child may have many parents between itself and the object on which the method is called.
     *
     * @arg Panel pnl - The panel to get the position of.
     * @return {number} The horizontal (x) position of the child relative to this panel object.
     * @return {number} The vertical (y) position of the child relative to this panel object.
     */
    public GetChildPosition(pnl: Panel): LuaMultiReturn<[number, number]>;

    /**
     * Returns a table with all the child panels of the panel.
     *
     * @return {object} children
     */
    public GetChildren(): object;

    /**
     * Returns a table of all visible, selectable children of the panel object that lie at least partially within the specified rectangle.
     *
     * @arg number x - The horizontal (x) position of the top-left corner of the rectangle, relative to the panel object.
     * @arg number y - The vertical (y) position of the top-left corner of the rectangle, relative to the panel object.
     * @arg number w - The width of the rectangle.
     * @arg number h - The height of the rectangle.
     * @return {object} A table of panel objects that lie at least partially within the specified rectangle.
     */
    public GetChildrenInRect(x: number, y: number, w: number, h: number): object;

    /**
     * Returns the class name of the panel.
     *
     * @return {string} className
     */
    public GetClassName(): string;

    /**
     * Returns the child of this panel object that is closest to the specified point. The point is relative to the object on which the method is called. The distance the child is from this point is also returned.
     *
     * @arg number x - The horizontal (x) position of the point.
     * @arg number y - The vertical (y) position of the point.
     * @return {Panel} The child object that was closest to the specified point.
     * @return {number} The distance that this child was from the point.
     */
    public GetClosestChild(x: number, y: number): LuaMultiReturn<[Panel, number]>;

    /**
     * Gets the size of the content/children within a panel object.
     *
     * @return {number} The content width of the object.
     * @return {number} The content height of the object.
     */
    public GetContentSize(): LuaMultiReturn<[number, number]>;

    /**
     * Gets the value of a cookie stored by the panel object. This can also be done with cookie.GetString, using the panel's cookie name, a fullstop, and then the actual name of the cookie.
     *
     * @arg string cookieName - The name of the cookie from which to retrieve the value.
     * @arg string def - The default value to return if the cookie does not exist.
     * @return {string} The value of the stored cookie, or the default value should the cookie not exist.
     */
    public GetCookie(cookieName: string, def: string): string;

    /**
 * Gets the name the panel uses to store cookies. This is set with Panel:SetCookieName.
* 
* @return {string} The name the panel uses when reading or writing cookies. The format used is as follows:
panelCookieName.individualCookieName
 */
    public GetCookieName(): string;

    /**
     * Gets the value of a cookie stored by the panel object, as a number. This can also be done with cookie.GetNumber, using the panel's cookie name, a fullstop, and then the actual name of the cookie.
     *
     * @arg string cookieName - The name of the cookie from which to retrieve the value.
     * @arg number def - The default value to return if the cookie does not exist.
     * @return {number} The number value of the stored cookie, or the default value should the cookie not exist.
     */
    public GetCookieNumber(cookieName: string, def: number): number;

    /**
     * Returns a dock enum for the panel's current docking type.
     *
     * @return {number} The dock enum for the panel. See Enums/DOCK.
     */
    public GetDock(): number;

    /**
     * Returns the docked margins of the panel. (set by Panel:DockMargin)
     *
     * @return {number} Left margin.
     * @return {number} Top margin.
     * @return {number} Right margin.
     * @return {number} Bottom margin.
     */
    public GetDockMargin(): LuaMultiReturn<[number, number, number, number]>;

    /**
     * Returns the docked padding of the panel. (set by Panel:DockPadding)
     *
     * @return {number} Left padding.
     * @return {number} Top padding.
     * @return {number} Right padding.
     * @return {number} Bottom padding.
     */
    public GetDockPadding(): LuaMultiReturn<[number, number, number, number]>;

    /**
     * Returns the foreground color of the panel.
     *
     * @return {Color} A color structure. See Color
     */
    public GetFGColor(): Color;

    /**
     * Returns the name of the font that the panel renders its text with.
     *
     * @return {string} fontName
     */
    public GetFont(): string;

    /**
     * Returns the panel's HTML material. Only works with Awesomium, HTML and DHTML panels that have been fully loaded.
     *
     * @return {IMaterial} The HTML material used by the panel. Typically starts with "_vgui_texture" followed by an incremental number.
     */
    public GetHTMLMaterial(): IMaterial;

    /**
     * Returns the current maximum character count.
     *
     * @return {number} The maximum amount of characters this panel is allowed to contain.
     */
    public GetMaximumCharCount(): number;

    /**
     * Returns the internal name of the panel.
     *
     * @return {string} name
     */
    public GetName(): string;

    /**
     * Returns the number of lines in a RichText. You must wait a couple frames before calling this after using Panel:AppendText or Panel:SetText, otherwise it will return the number of text lines before the text change.
     *
     * @return {number} The number of lines.
     */
    public GetNumLines(): number;

    /**
     * Returns the parent of the panel, returns nil if there is no parent.
     *
     * @return {Panel} The parent of given panel
     */
    public GetParent(): Panel;

    /**
     * Returns the position of the panel relative to its Panel:GetParent.
     *
     * @return {number} X coordinate, relative to this panels parents top left corner.
     * @return {number} Y coordinate, relative to this panels parents top left corner.
     */
    public GetPos(): LuaMultiReturn<[number, number]>;

    /**
     * Returns a table of all children of the panel object that are selected. This is recursive, and the returned table will include tables for any child objects that also have children. This means that not all first-level members in the returned table will be of type Panel.
     *
     * @return {object} A table of any child objects that are selected, including tables for children of the child objects (These tables may also contain table members, as the method is recursive).
     */
    public GetSelectedChildren(): object;

    /**
     * Returns the currently selected range of text.
     *
     * @return {number} The start of the range. If no text is selected it may be 0 and/or equal to the end range.
     * @return {number} The end of the range. If no text is selected it may be 0 and/or equal to the start range.
     */
    public GetSelectedTextRange(): LuaMultiReturn<[number, number]>;

    /**
     * Returns the panel object (self) if it has been enabled as a selection canvas. This is achieved using Panel:SetSelectionCanvas.
     *
     * @return {Panel} The panel object this method was called on if enabled as a selection canvas, otherwise nil.
     */
    public GetSelectionCanvas(): Panel;

    /**
     * Returns the size of the panel.
     *
     * @return {number} width
     * @return {number} height
     */
    public GetSize(): LuaMultiReturn<[number, number]>;

    /**
     * Returns the table for the derma skin currently being used by this panel object.
     *
     * @return {object} The derma skin table currently being used by this object.
     */
    public GetSkin(): object;

    /**
     * Returns the internal Lua table of the panel.
     *
     * @return {object} A table containing all the members of given panel object.
     */
    public GetTable(): object;

    /**
     * Returns the height of the panel.
     *
     * @return {number} height
     */
    public GetTall(): number;

    /**
     * Returns the panel's text (where applicable).
     *
     * @return {string} The panel's text.
     */
    public GetText(): string;

    /**
     * Gets the left and top text margins of a text-based panel object, such as a DButton or DLabel. This is set with Panel:SetTextInset.
     *
     * @return {number} The left margin of the text, in pixels.
     * @return {number} The top margin of the text, in pixels.
     */
    public GetTextInset(): LuaMultiReturn<[number, number]>;

    /**
     * Gets the size of the text within a Label derived panel.
     *
     * @return {number} The width of the text in the DLabel.
     * @return {number} The height of the text in the DLabel.
     */
    public GetTextSize(): LuaMultiReturn<[number, number]>;

    /**
     * Gets valid receiver slot of currently dragged panel.
     *
     * @return {Panel} The panel this was called on if a valid receiver slot exists, otherwise false.
     * @return {object} The slot table.
     */
    public GetValidReceiverSlot(): LuaMultiReturn<[Panel, object]>;

    /**
     * Returns the value the panel holds.
     *
     * @return {any} The value the panel holds.
     */
    public GetValue(): any;

    /**
     * Returns the width of the panel.
     *
     * @return {number} width
     */
    public GetWide(): number;

    /**
     * Returns the X position of the panel relative to its Panel:GetParent.
     *
     * @return {number} X coordinate.
     */
    public GetX(): number;

    /**
     * Returns the Y position of the panel relative to its Panel:GetParent.
     *
     * @return {number} Y coordinate.
     */
    public GetY(): number;

    /**
     * Returns the Z position of the panel.
     *
     * @return {number} The Z order position of the panel.
     */
    public GetZPos(): number;

    /**
     * Goes back one page in the HTML panel's history if available.
     */
    public GoBack(): void;

    /**
     * Goes forward one page in the HTML panel's history if available.
     */
    public GoForward(): void;

    /**
     * Goes to the page in the HTML panel's history at the specified relative offset.
     *
     * @arg number offset - The offset in the panel's back/forward history, relative to the current page, that you would like to skip to. Because this is relative, 0 = current page while negative goes back and positive goes forward. For example, -2 will go back 2 pages in the history.
     */
    public GoToHistoryOffset(offset: number): void;

    /**
     * Causes a RichText element to scroll to the bottom of its text.
     */
    public GotoTextEnd(): void;

    /**
     * Causes a RichText element to scroll to the top of its text.
     */
    public GotoTextStart(): void;

    /**
     * Used by Panel:ApplyGWEN to apply the CheckboxText property to a DCheckBoxLabel. This does exactly the same as Panel:GWEN_SetText, but exists to cater for the seperate GWEN properties.
     *
     * @arg string txt - The text to be applied to the DCheckBoxLabel.
     */
    public GWEN_SetCheckboxText(txt: string): void;

    /**
     * Used by Panel:ApplyGWEN to apply the ControlName property to a panel. This calls Panel:SetName.
     *
     * @arg string name - The new name to apply to the panel.
     */
    public GWEN_SetControlName(name: string): void;

    /**
 * Used by Panel:ApplyGWEN to apply the Dock property to a  panel object. This calls Panel:Dock.
* 
* @arg string dockState - The dock mode to pass to the panel's Dock method. This reads a string and applies the approriate Enums/DOCK.

Right: Dock right.
Left: Dock left.
Bottom: Dock at the bottom.
Top: Dock at the top.
Fill: Fill the parent panel.
 */
    public GWEN_SetDock(dockState: string): void;

    /**
 * Used by Panel:ApplyGWEN to apply the HorizontalAlign property to a  panel object. This calls Panel:SetContentAlignment.
* 
* @arg string hAlign - The alignment, as a string, to pass to Panel:SetContentAlignment. Accepts:

Right: Align mid-right.
Left: Align mid-left.
Center: Align mid-center.
 */
    public GWEN_SetHorizontalAlign(hAlign: string): void;

    /**
 * Used by Panel:ApplyGWEN to apply the Margin property to a  panel object. This calls Panel:DockMargin.
* 
* @arg object margins - A four-membered table containing the margins as numbers:

number left - The left margin.
number top - The top margin.
number right - The right margin.
number bottom - The bottom margin.
 */
    public GWEN_SetMargin(margins: object): void;

    /**
     * Used by Panel:ApplyGWEN to apply the Max property to a  DNumberWang, Slider, DNumSlider or DNumberScratch. This calls SetMax on one of the previously listed methods.
     *
     * @arg number maxValue - The maximum value the element is to permit.
     */
    public GWEN_SetMax(maxValue: number): void;

    /**
     * Used by Panel:ApplyGWEN to apply the Min property to a  DNumberWang, Slider, DNumSlider or DNumberScratch. This calls SetMin on one of the previously listed methods.
     *
     * @arg number minValue - The minimum value the element is to permit.
     */
    public GWEN_SetMin(minValue: number): void;

    /**
 * Used by Panel:ApplyGWEN to apply the Position property to a  panel object. This calls Panel:SetPos.
* 
* @arg object pos - A two-membered table containing the x and y coordinates as numbers:

number x - The x coordinate.
number y - The y coordinate.
 */
    public GWEN_SetPosition(pos: object): void;

    /**
 * Used by Panel:ApplyGWEN to apply the Size property to a  panel object. This calls Panel:SetSize.
* 
* @arg object size - A two-membered table containing the width and heights as numbers:

number w - The width.
number h - The height.
 */
    public GWEN_SetSize(size: object): void;

    /**
     * Used by Panel:ApplyGWEN to apply the Text property to a panel.
     *
     * @arg string txt - The text to be applied to the panel.
     */
    public GWEN_SetText(txt: string): void;

    /**
     * Returns whenever the panel has child panels.
     *
     * @return {boolean} hasChilds
     */
    public HasChildren(): boolean;

    /**
     * Returns if the panel is focused.
     *
     * @return {boolean} hasFocus
     */
    public HasFocus(): boolean;

    /**
     * Returns if the panel or any of its children(sub children and so on) has the focus.
     *
     * @return {boolean} hasHierarchicalFocus
     */
    public HasHierarchicalFocus(): boolean;

    /**
     * Returns whether the panel is a descendent of the given panel.
     *
     * @arg Panel parentPanel
     * @return {boolean} True if the panel is contained within parentPanel.
     */
    public HasParent(parentPanel: Panel): boolean;

    /**
     * Makes a panel invisible.
     */
    public Hide(): void;

    /**
     * Marks the end of a clickable text segment in a RichText element, started with Panel:InsertClickableTextStart.
     */
    public InsertClickableTextEnd(): void;

    /**
     * Starts the insertion of clickable text for a RichText element. Any text appended with Panel:AppendText between this call and Panel:InsertClickableTextEnd will become clickable text.
     *
     * @arg string signalValue - The text passed as the action signal's value.
     */
    public InsertClickableTextStart(signalValue: string): void;

    /**
     * Inserts a color change in a RichText element, which affects the color of all text added with Panel:AppendText until another color change is applied.
     *
     * @arg number r - The red value (0 - 255).
     * @arg number g - The green value (0 - 255).
     * @arg number b - The blue value (0 - 255).
     * @arg number a - The alpha value (0 - 255).
     */
    public InsertColorChange(r: number, g: number, b: number, a: number): void;

    /**
 * Begins a text fade for a RichText element where the last appended text segment is fully faded out after a specific amount of time, at a specific speed.
* 
* @arg number sustain - The number of seconds the text remains visible.
* @arg number length - The number of seconds it takes the text to fade out.
If set lower than sustain, the text will not begin fading out until (sustain - length) seconds have passed.
If set higher than sustain, the text will begin fading out immediately at a fraction of the base alpha.
If set to -1, the text doesn't fade out.
 */
    public InsertFade(sustain: number, length: number): void;

    /**
     * Invalidates the layout of this panel object and all its children. This will cause these objects to re-layout immediately, calling PANEL:PerformLayout. If you want to perform the layout in the next frame, you will have loop manually through all children, and call Panel:InvalidateLayout on each.
     *
     * @arg boolean [recursive=false] - If true, the method will recursively invalidate the layout of all children. Otherwise, only immediate children are affected.
     */
    public InvalidateChildren(recursive?: boolean): void;

    /**
     * Causes the panel to re-layout in the next frame. During the layout process  PANEL:PerformLayout will be called on the target panel.
     *
     * @arg boolean [layoutNow=false] - If true the panel will re-layout instantly and not wait for the next frame.
     */
    public InvalidateLayout(layoutNow?: boolean): void;

    /**
     * Calls Panel:InvalidateLayout on the panel's parent. This function will silently fail if the panel has no parent.
     *
     * @arg boolean [layoutNow=false] - If true, the re-layout will occur immediately, otherwise it will be performed in the next frame.
     */
    public InvalidateParent(layoutNow?: boolean): void;

    /**
     * Determines whether the mouse cursor is hovered over one of this panel object's children. This is a reverse process using vgui.GetHoveredPanel, and looks upward to find the parent.
     *
     * @arg boolean [immediate=false] - Set to true to check only the immediate children of given panel ( first level )
     * @return {boolean} Whether or not one of this panel object's children is being hovered over.
     */
    public IsChildHovered(immediate?: boolean): boolean;

    /**
     * Returns whether this panel is draggable ( if user is able to drag it ) or not.
     *
     * @return {boolean} Whether this panel is draggable ( if user is able to drag it ) or not.
     */
    public IsDraggable(): boolean;

    /**
     * Returns whether this panel is currently being dragged or not.
     *
     * @return {boolean} Whether this panel is currently being dragged or not.
     */
    public IsDragging(): boolean;

    /**
     * Returns whether the the panel is enabled or disabled.
     *
     * @return {boolean} Whether the panel is enabled or disabled.
     */
    public IsEnabled(): boolean;

    /**
     * Returns whether the mouse cursor is hovering over this panel or not
     *
     * @return {boolean} true if the panel is hovered
     */
    public IsHovered(): boolean;

    /**
     * Returns true if the panel can receive keyboard input.
     *
     * @return {boolean} keyboardInputEnabled
     */
    public IsKeyboardInputEnabled(): boolean;

    /**
     * Determines whether or not a HTML or DHTML element is currently loading a page.
     *
     * @return {boolean} Whether or not the (D)HTML object is loading.
     */
    public IsLoading(): boolean;

    /**
     * Returns if the panel is going to be deleted in the next frame.
     *
     * @return {boolean} markedForDeletion
     */
    public IsMarkedForDeletion(): boolean;

    /**
     * Returns whether the panel was made modal or not. See Panel:DoModal.
     *
     * @return {boolean} True if the panel is modal.
     */
    public IsModal(): boolean;

    /**
     * Returns true if the panel can receive mouse input.
     *
     * @return {boolean} mouseInputEnabled
     */
    public IsMouseInputEnabled(): boolean;

    /**
     * Returns whether the panel contains the given panel, recursively.
     *
     * @arg Panel childPanel
     * @return {boolean} True if the panel contains childPanel.
     */
    public IsOurChild(childPanel: Panel): boolean;

    /**
     * Returns if the panel was made popup or not. See Panel:MakePopup
     *
     * @return {boolean} true if the panel was made popup.
     */
    public IsPopup(): boolean;

    /**
     * Determines if the panel object is selectable (like icons in the Spawn Menu, holding ⇧ shift). This is set with Panel:SetSelectable.
     *
     * @return {boolean} Whether the panel is selectable or not.
     */
    public IsSelectable(): boolean;

    /**
     * Returns if the panel object is selected (like icons in the Spawn Menu, holding ⇧ shift). This can be set in Lua using Panel:SetSelected.
     *
     * @return {boolean} Whether the panel object is selected or not. Always returns false if the object is not selectable. This can be modified using Panel:SetSelectable.
     */
    public IsSelected(): boolean;

    /**
     * Determines if the panel object is a selection canvas or not. This is set with Panel:SetSelectionCanvas.
     *
     * @return {any} The value (if any) set by Panel:SetSelectionCanvas.
     */
    public IsSelectionCanvas(): any;

    /**
     * Returns if the panel is valid and not marked for deletion.
     *
     * @return {boolean} True if the object is valid.
     */
    public IsValid(): boolean;

    /**
     * Returns if the panel is visible. This will NOT take into account visibility of the parent.
     *
     * @return {boolean} true if the panel ls visible, false otherwise.
     */
    public IsVisible(): boolean;

    /**
     * Returns if a panel allows world clicking set by Panel:SetWorldClicker.
     *
     * @return {boolean} If the panel allows world clicking.
     */
    public IsWorldClicker(): boolean;

    /**
     * Remove the focus from the panel.
     */
    public KillFocus(): void;

    /**
 * Redefines the panel object's Panel:SetPos method to operate using frame-by-frame linear interpolation (Lerp). When the panel's position is changed, it will move to the target position at the speed defined. You can undo this with Panel:DisableLerp.
* 
* @arg number speed - The speed at which to move the panel. This is affected by the value of easeOut. Recommended values are:

0.1 - 10 when easeOut is false.
0.1 - 1 when easeOut is true.
* @arg boolean easeOut - This causes the panel object to 'jump' at the target, slowing as it approaches. This affects the speed value significantly, see above.
 */
    public LerpPositions(speed: number, easeOut: boolean): void;

    /**
     * Similar to Panel:LoadControlsFromString but loads controls from a file.
     *
     * @arg string path - The path to load the controls from.
     */
    public LoadControlsFromFile(path: string): void;

    /**
     * Loads controls(positions, etc) from given data. This is what the default options menu uses.
     *
     * @arg string data - The data to load controls from. Format unknown.
     */
    public LoadControlsFromString(data: string): void;

    /**
 * Loads a .gwen file (created by GWEN Designer) and calls Panel:LoadGWENString with the contents of the loaded file.
* 
* @arg string filename - The file to open. The path is relative to garrysmod/garrysmod/.
* @arg string [path="GAME"] - The path used to look up the file.

"GAME" Structured like base folder (garrysmod/), searches all the mounted content (main folder, addons, mounted games etc)
"LUA" or "lsv" - All Lua folders (lua/) including gamesmodes and addons
"DATA" Data folder (garrysmod/data)
"MOD" Strictly the game folder (garrysmod/), ignores mounting.
 */
    public LoadGWENFile(filename: string, path?: string): void;

    /**
     * Loads controls for the panel from a JSON string.
     *
     * @arg string str - JSON string containing information about controls to create.
     */
    public LoadGWENString(str: string): void;

    /**
 * Sets a new image to be loaded by a TGAImage.
* 
* @arg string imageName - The file path.
* @arg string strPath - The PATH to search in. See File Search Paths.
This isn't used internally.
 */
    public LoadTGAImage(imageName: string, strPath: string): void;

    /**
     * Returns the cursor position local to the position of the panel (usually the upper-left corner).
     *
     * @return {number} The x coordinate
     * @return {number} The y coordinate
     */
    public LocalCursorPos(): LuaMultiReturn<[number, number]>;

    /**
     * Gets the absolute screen position of the position specified relative to the panel.
     *
     * @arg number posX - The X coordinate of the position on the panel to translate.
     * @arg number posY - The Y coordinate of the position on the panel to translate.
     * @return {number} The X coordinate relative to the screen.
     * @return {number} The Y coordinate relative to the screen.
     */
    public LocalToScreen(posX: number, posY: number): LuaMultiReturn<[number, number]>;

    /**
     * Focuses the panel and enables it to receive input.
     */
    public MakePopup(): void;

    /**
     * Allows the panel to receive mouse input even if the mouse cursor is outside the bounds of the panel.
     *
     * @arg boolean doCapture - Set to true to enable, set to false to disable.
     */
    public MouseCapture(doCapture: boolean): void;

    /**
     * Places the panel above the passed panel with the specified offset.
     *
     * @arg Panel panel - Panel to position relatively to.
     * @arg number [offset=0] - The align offset.
     */
    public MoveAbove(panel: Panel, offset?: number): void;

    /**
     * Places the panel below the passed panel with the specified offset.
     *
     * @arg Panel panel - Panel to position relatively to.
     * @arg number [offset=0] - The align offset.
     */
    public MoveBelow(panel: Panel, offset?: number): void;

    /**
 * Moves the panel by the specified coordinates using animation.
* 
* @arg number moveX - The number of pixels to move by in the horizontal (x) direction.
* @arg number moveY - The number of pixels to move by in the vertical (y) direction.
* @arg number time - The time (in seconds) in which to perform the animation.
* @arg number [delay=0] - The delay (in seconds) before the animation begins.
* @arg number [ease=-1] - The easing of the start and/or end speed of the animation. See Panel:NewAnimation for how this works.
* @arg GMLua.CallbackNoContext [callback=nil] - The function to be called once the animation is complete. Arguments are:

table animData - The AnimationData that was used.
Panel pnl - The panel object that was moved.
 */
    public MoveBy(
        moveX: number,
        moveY: number,
        time: number,
        delay?: number,
        ease?: number,
        callback?: GMLua.CallbackNoContext
    ): void;

    /**
     * Places the panel left to the passed panel with the specified offset.
     *
     * @arg Panel panel - Panel to position relatively to.
     * @arg number [offset=0] - The align offset.
     */
    public MoveLeftOf(panel: Panel, offset?: number): void;

    /**
     * Places the panel right to the passed panel with the specified offset.
     *
     * @arg Panel panel - Panel to position relatively to.
     * @arg number [offset=0] - The align offset.
     */
    public MoveRightOf(panel: Panel, offset?: number): void;

    /**
 * Moves the panel to the specified position using animation.
* 
* @arg number posX - The target x coordinate of the panel.
* @arg number posY - The target y coordinate of the panel.
* @arg number time - The time to perform the animation within.
* @arg number [delay=0] - The delay before the animation starts.
* @arg number [ease=-1] - The easing of the start and/or end speed of the animation. See Panel:NewAnimation for how this works.
* @arg GMLua.CallbackNoContext callback - The function to be called once the animation finishes. Arguments are:

table animData - The Structures/AnimationData that was used.
Panel pnl - The panel object that was moved.
 */
    public MoveTo(
        posX: number,
        posY: number,
        time: number,
        delay?: number,
        ease?: number,
        callback?: GMLua.CallbackNoContext
    ): void;

    /**
     * Moves this panel object in front of the specified sibling (child of the same parent) in the render order, and shuffles up the Z-positions of siblings now behind.
     *
     * @arg Panel siblingPanel - The panel to move this one in front of. Must be a child of the same parent panel.
     * @return {boolean} false if the passed panel is not a sibling, otherwise nil.
     */
    public MoveToAfter(siblingPanel: Panel): boolean;

    /**
     * Moves the panel object behind all other panels on screen. If the panel has been made a pop-up with Panel:MakePopup, it will still draw in front of any panels that haven't.
     */
    public MoveToBack(): void;

    /**
     * Moves this panel object behind the specified sibling (child of the same parent) in the render order, and shuffles up the Panel:SetZPos of siblings now in front.
     *
     * @arg Panel siblingPanel - The panel to move this one behind. Must be a child of the same parent panel.
     * @return {boolean} false if the passed panel is not a sibling, otherwise nil.
     */
    public MoveToBefore(siblingPanel: Panel): boolean;

    /**
     * Moves the panel in front of all other panels on screen. Unless the panel has been made a pop-up using Panel:MakePopup, it will still draw behind any that have.
     */
    public MoveToFront(): void;

    /**
 * Creates a new animation for the panel object.
* 
* @arg number length - The length of the animation in seconds.
* @arg number [delay=0] - The delay before the animation starts.
* @arg number [ease=-1] - The power/index to use for easing.

Positive values greater than 1 will ease in; the higher the number, the sharper the curve's gradient (less linear).
A value of 1 removes all easing.
Positive values between 0 and 1 ease out; values closer to 0 increase the curve's gradient (less linear).
A value of 0 will break the animation and should be avoided.
Any value less than zero will ease in/out; the value has no effect on the gradient.
* @arg GMLua.CallbackNoContext [callback=nil] - The function to be called when the animation ends. Arguments passed are:

table animTable - The Structures/AnimationData that was used.
Panel tgtPanel - The panel object that was animated.
* @return {AnimationDataStruct} Partially filled Structures/AnimationData with members:

number EndTime - Equal to length and delay arguments added together, plus either the SysTime if there is no other animation queued or the end time of the last animation in the queue.
number StartTime - Equal to the delay argument, plus either the SysTime if there is no other animation queued or the end time of the last animation in the queue.
number Ease - Equal to the ease argument.
function OnEnd - Equal to the callback argument.
 */
    public NewAnimation(
        length: number,
        delay?: number,
        ease?: number,
        callback?: GMLua.CallbackNoContext
    ): AnimationDataStruct;

    /**
     *
     *
     * @arg string objectName
     */
    public NewObject(objectName: string): void;

    /**
     *
     *
     * @arg string objectName
     * @arg string callbackName
     */
    public NewObjectCallback(objectName: string, callbackName: string): void;

    /**
     * Sets whether this panel's drawings should be clipped within the parent panel's bounds.
     *
     * @arg boolean clip - Whether to clip or not.
     */
    public NoClipping(clip: boolean): void;

    /**
     * Returns the number of children of the panel object that are selected. This is equivalent to calling Panel:IsSelected on all child objects and counting the number of returns that are true.
     *
     * @return {number} The number of child objects that are currently selected. This does not include the parent object you are calling the method from.
     */
    public NumSelectedChildren(): number;

    /**
 * Instructs a HTML control to download and parse a HTML script using the passed URL.
* 
* @arg string URL - URL to open. It has to start or be one of the following:

http://
https://
asset://
about:blank
chrome://credits/
 */
    public OpenURL(URL: string): void;

    /**
     * Paints a ghost copy of the panel at the given position.
     *
     * @arg number posX - The x coordinate to draw the panel from.
     * @arg number posY - The y coordinate to draw the panel from.
     */
    public PaintAt(posX: number, posY: number): void;

    /**
     * Paints the panel at its current position. To use this you must call Panel:SetPaintedManually(true).
     */
    public PaintManual(): void;

    /**
     * Parents the panel to the HUD.
     * Makes it invisible on the escape-menu and disables controls.
     */
    public ParentToHUD(): void;

    /**
     * Only works for TextEntries.
     */
    public Paste(): void;

    /**
     * Sets the width and position of a DLabel and places the passed panel object directly to the right of it. Returns the y value of the bottom of the tallest object. The panel on which this method is run is not relevant; only the passed objects are affected.
     *
     * @arg number lblWidth - The width to set the label to.
     * @arg number x - The horizontal (x) position at which to place the label.
     * @arg number y - The vertical (y) position at which to place the label.
     * @arg Panel lbl - The label to resize and position.
     * @arg Panel panelObj - The panel object to place to the right of the label.
     * @return {number} The distance from the top of the parent panel to the bottom of the tallest object (the y position plus the height of the label or passed panel, depending on which is tallest).
     */
    public PositionLabel(lblWidth: number, x: number, y: number, lbl: Panel, panelObj: Panel): number;

    /**
     * Sends a command to the panel.
     *
     * @arg string messageName - The name of the message.
     * @arg string valueType - The type of the variable to post.
     * @arg string value - The value to post.
     */
    public PostMessage(messageName: string, valueType: string, value: string): void;

    /**
     * Installs Lua defined functions into the panel.
     */
    public Prepare(): void;

    /**
     * Enables the queue for panel animations. If enabled, the next new animation will begin after all current animations have ended. This must be called before Panel:NewAnimation to work, and only applies to the next new animation. If you want to queue many, you must call this before each.
     */
    public Queue(): void;

    /**
     * Causes a SpawnIcon to rebuild its model image.
     */
    public RebuildSpawnIcon(): void;

    /**
 * Re-renders a spawn icon with customized cam data.
* 
* @arg object data - A four-membered table containing the information needed to re-render:

Vector cam_pos - The relative camera position the model is viewed from.
Angle cam_ang - The camera angle the model is viewed from.
number cam_fov - The camera's field of view (FOV).
Entity ent - The entity object of the model.
See the example below for how to retrieve these values.
 */
    public RebuildSpawnIconEx(data: object): void;

    /**
 * Allows the panel to receive drag and drop events. Can be called multiple times with different names to receive multiple different draggable panel events.
* 
* @arg string name - Name of DnD panels to receive. This is set on the drag'n'drop-able panels via  Panel:Droppable
* @arg GMLua.CallbackNoContext func - This function is called whenever a panel with valid name is hovering above and dropped on this panel. It has next arguments:

Panel pnl - The receiver panel
table tbl - A table of panels dropped onto receiver panel
boolean dropped - False if hovering over, true if dropped onto
number menuIndex - Index of clicked menu item from third argument of Panel:Receiver
number x - Cursor pos, relative to the receiver
number y - Cursor pos, relative to the receiver
* @arg object menu - A table of strings that will act as a menu if drag'n'drop was performed with a right click
 */
    public Receiver(name: string, func: GMLua.CallbackNoContext, menu: object): void;

    /**
     * Refreshes the HTML panel's current page.
     *
     * @arg boolean [ignoreCache=false] - If true, the refresh will ignore cached content similar to "ctrl+f5" in most browsers.
     */
    public Refresh(ignoreCache?: boolean): void;

    /**
     * Marks a panel for deletion so it will be deleted on the next frame.
     */
    public Remove(): void;

    /**
     * Attempts to obtain focus for this panel.
     */
    public RequestFocus(): void;

    /**
     * Resets all text fades in a RichText element made with Panel:InsertFade.
     *
     * @arg boolean hold - True to reset fades, false otherwise.
     * @arg boolean expiredOnly - Any value equating to true will reset fades only on text segments that are completely faded out.
     * @arg number newSustain - The new sustain value of each faded text segment. Set to -1 to keep the old sustain value.
     */
    public ResetAllFades(hold: boolean, expiredOnly: boolean, newSustain: number): void;

    /**
     * Runs/Executes a string as JavaScript code in a panel.
     *
     * @arg string js - Specify JavaScript code to be executed.
     */
    public RunJavascript(js: string): void;

    /**
     * Saves the current state (caret position and the text inside) of a TextEntry as an undo state.
     */
    public SaveUndoState(): void;

    /**
     * Translates global screen coordinate to coordinates relative to the panel.
     *
     * @arg number screenX - The x coordinate of the screen position to be translated.
     * @arg number screenY - The y coordinate of the screed position be to translated.
     * @return {number} Relativeposition X
     * @return {number} Relativeposition Y
     */
    public ScreenToLocal(screenX: number, screenY: number): LuaMultiReturn<[number, number]>;

    /**
     * Selects all items within a panel or object. For text-based objects, selects all text.
     */
    public SelectAll(): void;

    /**
     * If called on a text entry, clicking the text entry for the first time will automatically select all of the text ready to be copied by the user.
     */
    public SelectAllOnFocus(): void;

    /**
     * Selects all the text in a panel object. Will not select non-text items; for this, use Panel:SelectAll.
     */
    public SelectAllText(): void;

    /**
     * Deselects all items in a panel object. For text-based objects, this will deselect all text.
     */
    public SelectNone(): void;

    /**
     * Sets the achievement to be displayed by AchievementIcon.
     *
     * @arg number id - Achievement number ID
     */
    public SetAchievement(id: number): void;

    /**
 * Used in Button to call a function when the button is clicked and in Slider when the value changes.
* 
* @arg GMLua.CallbackNoContext func - Function to call when the Button is clicked or the Slider value is changed.
Arguments given are:

Panel self - The panel itself
string action - "Command" on button press, "SliderMoved" on slider move.
number val - The new value of the Slider. Will always equal 0 for buttons.
number zed - Always equals 0.
 */
    public SetActionFunction(func: GMLua.CallbackNoContext): void;

    /**
     * Configures a text input to allow user to type characters that are not included in the US-ASCII (7-bit ASCII) character set.
     *
     * @arg boolean allowed - Set to true in order not to restrict input characters.
     */
    public SetAllowNonAsciiCharacters(allowed: boolean): void;

    /**
     * Sets the alpha multiplier for the panel
     *
     * @arg number alpha - The alpha value in the range of 0-255.
     */
    public SetAlpha(alpha: number): void;

    /**
     * Enables or disables animations for the panel object by overriding the PANEL:AnimationThink hook to nil and back.
     *
     * @arg boolean enable - Whether to enable or disable animations.
     */
    public SetAnimationEnabled(enable: boolean): void;

    /**
     * Sets whenever the panel should be removed if the parent was removed.
     *
     * @arg boolean autoDelete - Whenever to delete if the parent was removed or not.
     */
    public SetAutoDelete(autoDelete: boolean): void;

    /**
     * Sets the background color of a panel such as a RichText, Label or DColorCube.
     *
     * @arg number r_or_color - The red channel of the color, or a Color. If you pass the latter, the following three arguments are ignored.
     * @arg number g - The green channel of the color.
     * @arg number b - The blue channel of the color.
     * @arg number a - The alpha channel of the color.
     */
    public SetBGColor(r_or_color: number, g: number, b: number, a: number): void;

    /**
     * Sets the background color of the panel.
     *
     * @arg number r - The red channel of the color.
     * @arg number g - The green channel of the color.
     * @arg number b - The blue channel of the color.
     * @arg number a - The alpha channel of the color.
     */
    public SetBGColorEx(r: number, g: number, b: number, a: number): void;

    /**
     * Sets the position of the caret (or text cursor) in a text-based panel object.
     *
     * @arg number offset - Caret position/offset from the start of text. A value of 0 places the caret before the first character.
     */
    public SetCaretPos(offset: number): void;

    /**
     * Sets the action signal command that's fired when a Button is clicked. The hook PANEL:ActionSignal is called as the click response.
     */
    public SetCommand(): void;

    /**
 * Sets the alignment of the contents.
* 
* @arg number alignment - The direction of the content, based on the number pad.
1: bottom-left 
2: bottom-center 
3: bottom-right 
4: middle-left 
5: center 
6: middle-right 
7: top-left 
8: top-center 
9: top-right
 */
    public SetContentAlignment(alignment: number): void;

    /**
     * Sets this panel's convar. When the convar changes this panel will update automatically.
     *
     * @arg string convar - The console variable to check.
     */
    public SetConVar(convar: string): void;

    /**
     * Stores a string in the named cookie using Panel:GetCookieName as prefix.
     *
     * @arg string cookieName - The unique name used to retrieve the cookie later.
     * @arg string value - The value to store in the cookie. This can be retrieved later as a string or number.
     */
    public SetCookie(cookieName: string, value: string): void;

    /**
     * Sets the panel's cookie name. Calls PANEL:LoadCookies if defined.
     *
     * @arg string name - The panel's cookie name. Used as prefix for Panel:SetCookie
     */
    public SetCookieName(name: string): void;

    /**
 * Sets the appearance of the cursor. You can find a list of all available cursors with image previews here.
* 
* @arg string cursor - The cursor to be set. Can be one of the following:

arrow
beam
hourglass
waitarrow
crosshair
up
sizenwse
sizenesw
sizewe
sizens
sizeall
no
hand
blank

Set to anything else to set it to "none", the default fallback. Do note that a value of "none" does not, as one might assume, result in no cursor being drawn - hiding the cursor requires a value of "blank" instead.
 */
    public SetCursor(cursor: string): void;

    /**
     * Sets the drag parent.
     *
     * @arg Panel parent - The panel to set as drag parent.
     */
    public SetDragParent(parent: Panel): void;

    /**
     * Sets the visibility of the language selection box in a TextEntry when typing in non-English mode.
     *
     * @arg boolean visible - true to make it visible, false to hide it.
     */
    public SetDrawLanguageID(visible: boolean): void;

    /**
     * Sets where to draw the language selection box.
     *
     * @arg boolean left - true = left, false = right
     */
    public SetDrawLanguageIDAtLeft(left: boolean): void;

    /**
     * Makes the panel render in front of all others, including the spawn menu and main menu.
     *
     * @arg boolean [drawOnTop=false] - Whether or not to draw the panel in front of all others.
     */
    public SetDrawOnTop(drawOnTop?: boolean): void;

    /**
     * Sets the target area for dropping when an object is being dragged around this panel using the dragndrop.
     *
     * @arg number x - The x coordinate of the top-left corner of the drop area.
     * @arg number y - The y coordinate of the top-left corner of the drop area.
     * @arg number width - The width of the drop area.
     * @arg number height - The height of the drop area.
     */
    public SetDropTarget(x: number, y: number, width: number, height: number): void;

    /**
     * Sets the enabled state of a disable-able panel object, such as a DButton or DTextEntry.
     *
     * @arg boolean enable - Whether to enable or disable the panel object.
     */
    public SetEnabled(enable: boolean): void;

    /**
     * Adds a shadow falling to the bottom right corner of the panel's text. This has no effect on panels that do not derive from Label.
     *
     * @arg number distance - The distance of the shadow from the panel.
     * @arg Color Color - The color of the shadow. Uses the Color.
     */
    public SetExpensiveShadow(distance: number, Color: Color): void;

    /**
     * Sets the foreground color of a panel.
     *
     * @arg number r_or_color - The red channel of the color, or a Color. If you pass the latter, the following three arguments are ignored.
     * @arg number g - The green channel of the color.
     * @arg number b - The blue channel of the color.
     * @arg number a - The alpha channel of the color.
     */
    public SetFGColor(r_or_color: number, g: number, b: number, a: number): void;

    /**
     * Sets the foreground color of the panel.
     *
     * @arg number r - The red channel of the color.
     * @arg number g - The green channel of the color.
     * @arg number b - The blue channel of the color.
     * @arg number a - The alpha channel of the color.
     */
    public SetFGColorEx(r: number, g: number, b: number, a: number): void;

    /**
     * Sets the panel that owns this FocusNavGroup to be the root in the focus traversal hierarchy. This function will only work on EditablePanel class panels and its derivatives.
     *
     * @arg boolean state
     */
    public SetFocusTopLevel(state: boolean): void;

    /**
 * Sets the font used to render this panel's text.
* 
* @arg string fontName - The name of the font.
See here for a list of existing fonts.
Alternatively, use surface.CreateFont to create your own custom font.
 */
    public SetFontInternal(fontName: string): void;

    /**
     * Sets the height of the panel.
     *
     * @arg number height - The height to be set.
     */
    public SetHeight(height: number): void;

    /**
     * Allows you to set HTML code within a panel.
     *
     * @arg string HTML_code - The code to set.
     */
    public SetHTML(HTML_code: string): void;

    /**
     * Enables or disables the keyboard input for the panel.
     *
     * @arg boolean keyboardInput - Whether to enable or disable keyboard input.
     */
    public SetKeyBoardInputEnabled(keyboardInput: boolean): void;

    /**
     * Sets the maximum character count this panel should have.
     *
     * @arg number maxChar - The new maximum amount of characters this panel is allowed to contain.
     */
    public SetMaximumCharCount(maxChar: number): void;

    /**
     * Sets the minimum dimensions of the panel or object.
     *
     * @arg number [minW=NaN] - The minimum width of the object.
     * @arg number [minH=NaN] - The minimum height of the object.
     */
    public SetMinimumSize(minW?: number, minH?: number): void;

    /**
     * Sets the model to be displayed by SpawnIcon.
     *
     * @arg string ModelPath - The path of the model to set
     * @arg number [skin=0] - The skin to set
     * @arg string bodygroups - The body groups to set. Each single-digit number in the string represents a separate bodygroup, This argument must be 9 characters in total.
     */
    public SetModel(ModelPath: string, skin?: number, bodygroups?: string): void;

    /**
     * Enables or disables the mouse input for the panel.
     *
     * @arg boolean mouseInput - Whenever to enable or disable mouse input.
     */
    public SetMouseInputEnabled(mouseInput: boolean): void;

    /**
     * Sets the internal name of the panel.
     *
     * @arg string name - The new name of the panel.
     */
    public SetName(name: string): void;

    /**
     * Sets whenever all the default background of the panel should be drawn or not.
     *
     * @arg boolean paintBackground - Whenever to draw the background or not.
     */
    public SetPaintBackgroundEnabled(paintBackground: boolean): void;

    /**
     * Sets whenever all the default border of the panel should be drawn or not.
     *
     * @arg boolean paintBorder - Whenever to draw the border or not.
     */
    public SetPaintBorderEnabled(paintBorder: boolean): void;

    /**
     * Enables or disables painting of the panel manually with Panel:PaintManual.
     *
     * @arg boolean paintedManually - True if the panel should be painted manually.
     */
    public SetPaintedManually(paintedManually: boolean): void;

    /**
     * This function does nothing.
     */
    public SetPaintFunction(): void;

    /**
     * Sets the parent of the panel.
     *
     * @arg Panel parent - The new parent of the panel.
     */
    public SetParent(parent: Panel): void;

    /**
     * Used by AvatarImage to load an avatar for given player.
     *
     * @arg Player player - The player to use avatar of.
     * @arg number size - The size of the avatar to use. Acceptable sizes are 32, 64, 184.
     */
    public SetPlayer(player: Player, size: number): void;

    /**
     * If this panel object has been made a popup with Panel:MakePopup, this method will prevent it from drawing in front of other panels when it receives input focus.
     *
     * @arg boolean stayAtBack - If true, the popup panel will not draw in front of others when it gets focus, for example when it is clicked.
     */
    public SetPopupStayAtBack(stayAtBack: boolean): void;

    /**
     * Sets the position of the panel's top left corner.
     *
     * @arg number posX - The x coordinate of the position.
     * @arg number posY - The y coordinate of the position.
     */
    public SetPos(posX: number, posY: number): void;

    /**
     * Sets whenever the panel should be rendered in the next screenshot.
     *
     * @arg boolean renderInScreenshot - Whenever to render or not.
     */
    public SetRenderInScreenshots(renderInScreenshot: boolean): void;

    /**
     * Sets whether the panel object can be selected or not (like icons in the Spawn Menu, holding ⇧ shift). If enabled, this will affect the function of a DButton whilst ⇧ shift is pressed. Panel:SetSelected can be used to select/deselect the object.
     *
     * @arg boolean selectable - Whether the panel object should be selectable or not.
     */
    public SetSelectable(selectable: boolean): void;

    /**
     * Sets the selected state of a selectable panel object. This functionality is set with Panel:SetSelectable and checked with Panel:IsSelectable.
     *
     * @arg boolean [selected=false] - Whether the object should be selected or deselected. Panel:IsSelected can be used to determine the selected state of the object.
     */
    public SetSelected(selected?: boolean): void;

    /**
     * Enables the panel object for selection (much like the spawn menu).
     *
     * @arg any selCanvas - Any value other than nil or false will enable the panel object for selection. It is recommended to pass true.
     */
    public SetSelectionCanvas(selCanvas: any): void;

    /**
     * Sets the size of the panel.
     *
     * @arg number width - The width of the panel.
     * @arg number height - The height of the panel.
     */
    public SetSize(width: number, height: number): void;

    /**
     * Sets the derma skin that the panel object will use, and refreshes all panels with derma.RefreshSkins.
     *
     * @arg string skinName - The name of the skin to use. The default derma skin is Default.
     */
    public SetSkin(skinName: string): void;

    /**
     * Sets the .png image to be displayed on a  SpawnIcon or the panel it is based on - ModelImage.
     *
     * @arg string icon - A path to the .png material, for example one of the Silkicons shipped with the game.
     */
    public SetSpawnIcon(icon: string): void;

    /**
     * Used by AvatarImage panels to load an avatar by its 64-bit Steam ID (community ID).
     *
     * @arg string steamid - The 64bit SteamID of the player to load avatar of
     * @arg number size - The size of the avatar to use. Acceptable sizes are 32, 64, 184.
     */
    public SetSteamID(steamid: string, size: number): void;

    /**
     * When TAB is pressed, the next selectable panel in the number sequence is selected.
     *
     * @arg number position
     */
    public SetTabPosition(position: number): void;

    /**
     * Sets height of a panel. An alias of Panel:SetHeight.
     *
     * @arg number height - Desired height to set
     */
    public SetTall(height: number): void;

    /**
     * Removes the panel after given time in seconds.
     *
     * @arg number delay - Delay in seconds after which the panel should be removed.
     */
    public SetTerm(delay: number): void;

    /**
     * Sets the text value of a panel object containing text, such as a Label, TextEntry or  RichText and their derivatives, such as DLabel, DTextEntry or DButton.
     *
     * @arg string text - The text value to set.
     */
    public SetText(text: string): void;

    /**
     * Sets the left and top text margins of a text-based panel object, such as a DButton or DLabel.
     *
     * @arg number insetX - The left margin for the text, in pixels. This will only affect centered text if the margin is greater than its x-coordinate.
     * @arg number insetY - The top margin for the text, in pixels.
     */
    public SetTextInset(insetX: number, insetY: number): void;

    /**
     * Sets the height of a RichText element to accommodate the text inside.
     */
    public SetToFullHeight(): void;

    /**
     * Sets the tooltip to be displayed when a player hovers over the panel object with their cursor.
     *
     * @arg string str - The text to be displayed in the tooltip. Set false to disable it.
     */
    public SetTooltip(str: string): void;

    /**
     * Sets the panel to be displayed as contents of a DTooltip when a player hovers over the panel object with their cursor. See Panel:SetTooltipPanelOverride if you are looking to override DTooltip itself.
     *
     * @arg Panel [tooltipPanel=nil] - The panel to use as the tooltip.
     */
    public SetTooltipPanel(tooltipPanel?: Panel): void;

    /**
 * Sets the panel class to be created instead of DTooltip when the player hovers over this panel and a tooltip needs creating.
* 
* @arg string override - The panel class to override the default DTooltip. The new panel class must have the following methods:

SetText - If you are using Panel:SetTooltip.
SetContents - If you are using Panel:SetTooltipPanel.
OpenForPanel - A "hook" type function that gets called shortly after creation (and after the above 2) to open and position the tooltip. You can see this logic in lua/includes/util/tooltips.lua.
 */
    public SetTooltipPanelOverride(override: string): void;

    /**
 * Sets the underlined font for use by clickable text in a RichText. See also Panel:InsertClickableTextStart
* 
* @arg string fontName - The name of the font.
See here for a list of existing fonts.
Alternatively, use surface.CreateFont to create your own custom font.
 */
    public SetUnderlineFont(fontName: string): void;

    /**
     * Sets the URL of a link-based panel such as DLabelURL.
     *
     * @arg string url - The URL to set. It must begin with either http:// or https://.
     */
    public SetURL(url: string): void;

    /**
     * Sets the visibility of the vertical scrollbar.
     *
     * @arg boolean [display=false] - True to display the vertical text scroll bar, false to hide it.
     */
    public SetVerticalScrollbarEnabled(display?: boolean): void;

    /**
     * Sets the "visibility" of the panel.
     *
     * @arg boolean visible - The visibility of the panel.
     */
    public SetVisible(visible: boolean): void;

    /**
     * Sets width of a panel. An alias of Panel:SetWidth.
     *
     * @arg number width - Desired width to set
     */
    public SetWide(width: number): void;

    /**
     * Sets the width of the panel.
     *
     * @arg number width - The new width of the panel.
     */
    public SetWidth(width: number): void;

    /**
     * This makes it so that when you're hovering over this panel you can click on the world. Your viewmodel will aim etc. This is primarily used for the Sandbox context menu.
     *
     * @arg boolean enabled
     */
    public SetWorldClicker(enabled: boolean): void;

    /**
     * Sets whether text wrapping should be enabled or disabled on Label and DLabel panels.
     * Use DLabel:SetAutoStretchVertical to automatically correct vertical size; Panel:SizeToContents will not set the correct height.
     *
     * @arg boolean wrap - True to enable text wrapping, false otherwise.
     */
    public SetWrap(wrap: boolean): void;

    /**
     * Sets the X position of the panel.
     *
     * @arg number x - The X coordinate of the position.
     */
    public SetX(x: number): void;

    /**
     * Sets the Y position of the panel.
     *
     * @arg number y - The Y coordinate of the position.
     */
    public SetY(y: number): void;

    /**
 * Sets the panels z position which determines the rendering order.
* 
* @arg number zIndex - The z position of the panel. 
Can't be lower than -32768 or higher than 32767.
 */
    public SetZPos(zIndex: number): void;

    /**
     * Makes a panel visible.
     */
    public Show(): void;

    /**
 * Uses animation to resize the panel to the specified size.
* 
* @arg number [sizeW=0] - The target width of the panel. Use -1 to retain the current width.
* @arg number [sizeH=0] - The target height of the panel. Use -1 to retain the current height.
* @arg number time - The time to perform the animation within.
* @arg number [delay=0] - The delay before the animation starts.
* @arg number [ease=-1] - Easing of the start and/or end speed of the animation. See Panel:NewAnimation for how this works.
* @arg GMLua.CallbackNoContext callback - The function to be called once the animation finishes. Arguments are:

table animData - The Structures/AnimationData that was used.
Panel pnl - The panel object that was resized.
 */
    public SizeTo(
        sizeW?: number,
        sizeH?: number,
        time?: number,
        delay?: number,
        ease?: number,
        callback?: GMLua.CallbackNoContext
    ): void;

    /**
     * Resizes the panel to fit the bounds of its children.
     *
     * @arg boolean [sizeW=false] - Resize with width of the panel.
     * @arg boolean [sizeH=false] - Resize the height of the panel.
     */
    public SizeToChildren(sizeW?: boolean, sizeH?: boolean): void;

    /**
     * Resizes the panel so that its width and height fit all of the content inside.
     */
    public SizeToContents(): void;

    /**
     * Resizes the panel object's width to accommodate all child objects/contents.
     *
     * @arg number [addVal=0] - The number of extra pixels to add to the width. Can be a negative number, to reduce the width.
     */
    public SizeToContentsX(addVal?: number): void;

    /**
     * Resizes the panel object's height to accommodate all child objects/contents.
     *
     * @arg number [addVal=0] - The number of extra pixels to add to the height.
     */
    public SizeToContentsY(addVal?: number): void;

    /**
     * Slides the panel in from above.
     *
     * @arg number Length - Time to complete the animation.
     */
    public SlideDown(Length: number): void;

    /**
     * Slides the panel out to the top.
     *
     * @arg number Length - Time to complete the animation.
     */
    public SlideUp(Length: number): void;

    /**
     * Begins a box selection, enables mouse capture for the panel object, and sets the start point of the selection box to the mouse cursor's position, relative to this object. For this to work, either the object or its parent must be enabled as a selection canvas. This is set using Panel:SetSelectionCanvas.
     */
    public StartBoxSelection(): void;

    /**
     * Stops all panel animations by clearing its animation list. This also clears all delayed animations.
     */
    public Stop(): void;

    /**
     * Resizes the panel object's height so that its bottom is aligned with the top of the passed panel. An offset greater than zero will reduce the panel's height to leave a gap between it and the passed panel.
     *
     * @arg Panel tgtPanel - The panel to align the bottom of this one with.
     * @arg number [offset=0] - The gap to leave between this and the passed panel. Negative values will cause the panel's height to increase, forming an overlap.
     */
    public StretchBottomTo(tgtPanel: Panel, offset?: number): void;

    /**
     * Resizes the panel object's width so that its right edge is aligned with the left of the passed panel. An offset greater than zero will reduce the panel's width to leave a gap between it and the passed panel.
     *
     * @arg Panel tgtPanel - The panel to align the right edge of this one with.
     * @arg number [offset=0] - The gap to leave between this and the passed panel. Negative values will cause the panel's width to increase, forming an overlap.
     */
    public StretchRightTo(tgtPanel: Panel, offset?: number): void;

    /**
     * Sets the dimensions of the panel to fill its parent. It will only stretch in directions that aren't nil.
     *
     * @arg number offsetLeft - The left offset to the parent.
     * @arg number offsetTop - The top offset to the parent.
     * @arg number offsetRight - The right offset to the parent.
     * @arg number offsetBottom - The bottom offset to the parent.
     */
    public StretchToParent(offsetLeft: number, offsetTop: number, offsetRight: number, offsetBottom: number): void;

    /**
     * Toggles the selected state of a selectable panel object. This functionality is set with Panel:SetSelectable and checked with Panel:IsSelectable. To check whether the object is selected or not, Panel:IsSelected is used.
     */
    public ToggleSelection(): void;

    /**
     * Toggles the visibility of a panel and all its children.
     */
    public ToggleVisible(): void;

    /**
     * Restores the last saved state (caret position and the text inside) of a TextEntry. Should act identically to pressing CTRL+Z in a TextEntry.
     */
    public Undo(): void;

    /**
     * Recursively deselects this panel object and all of its children. This will cascade to all child objects at every level below the parent.
     */
    public UnselectAll(): void;

    /**
     * Forcibly updates the panels' HTML Material, similar to when Paint is called on it.This is only useful if the panel is not normally visible, i.e the panel exists purely for its HTML Material.
     */
    public UpdateHTMLTexture(): void;

    /**
     * Returns if a given panel is valid or not.
     *
     * @return {boolean} Whether the panel is valid or not, true being it is, false being it isn't.
     */
    public Valid(): boolean;
}

declare type Panel = __PanelClass;
