declare class __IRestoreClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Ends current data block started with IRestore:StartBlock and returns to the parent block.
     */
    public EndBlock(): void;

    /**
     * Reads next bytes from the restore object as an Angle.
     *
     * @return {Angle} The angle that has been read
     */
    public ReadAngle(): Angle;

    /**
     * Reads next bytes from the restore object as a boolean.
     *
     * @return {boolean} The boolean that has been read
     */
    public ReadBool(): boolean;

    /**
     * Reads next bytes from the restore object as an Entity.
     *
     * @return {Entity} The entity that has been read.
     */
    public ReadEntity(): Entity;

    /**
     * Reads next bytes from the restore object as a floating point number.
     *
     * @return {number} The read floating point number.
     */
    public ReadFloat(): number;

    /**
     * Reads next bytes from the restore object as an integer number.
     *
     * @return {number} The read integer number.
     */
    public ReadInt(): number;

    /**
     * Reads next bytes from the restore object as a string.
     *
     * @return {string} The read string. Maximum length is 1024.
     */
    public ReadString(): string;

    /**
     * Reads next bytes from the restore object as a Vector.
     *
     * @return {Vector} The read vector.
     */
    public ReadVector(): Vector;

    /**
     * Loads next block of data to be read inside current block. Blocks must be ended with IRestore:EndBlock.
     *
     * @return {string} The name of the next data block to be read.
     */
    public StartBlock(): string;
}

declare type IRestore = __IRestoreClass;
