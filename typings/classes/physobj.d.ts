declare class __PhysObjClass {
    [index: string | number | symbol]: any;
    private constructor();

    /**
     * Adds the specified angular velocity velocity to the current PhysObj.
     *
     * @arg Vector angularVelocity - Additional velocity.
     */
    public AddAngleVelocity(angularVelocity: Vector): void;

    /**
     * Adds one or more bit flags.
     *
     * @arg number flags - Bitflag, see Enums/FVPHYSICS.
     */
    public AddGameFlag(flags: number): void;

    /**
     * Adds the specified velocity to the current.
     *
     * @arg Vector velocity - Additional velocity.
     */
    public AddVelocity(velocity: Vector): void;

    /**
     * Rotates the object so that it's angles are aligned to the ones inputted.
     *
     * @arg Angle from
     * @arg Angle to
     * @return {Angle}
     */
    public AlignAngles(from: Angle, to: Angle): Angle;

    /**
     * Applies the specified force to the physics object (in Newtons).
     *
     * @arg Vector force - The force to be applied.
     */
    public ApplyForceCenter(force: Vector): void;

    /**
     * Applies the specified force on the physics object at the specified position
     *
     * @arg Vector force - The force to be applied.
     * @arg Vector position - The position in world coordinates where the force is applied to the physics object.
     */
    public ApplyForceOffset(force: Vector, position: Vector): void;

    /**
     * Applies specified angular impulse to the physics object. See PhysObj:CalculateForceOffset
     *
     * @arg Vector torque - The angular impulse to be applied in kg * degrees / s (verification needed).
     */
    public ApplyTorqueCenter(torque: Vector): void;

    /**
     * Calculates the force and torque on the center of mass for an offset force impulse. The outputs can be directly passed to PhysObj:ApplyForceCenter and PhysObj:ApplyTorqueCenter, respectively.
     *
     * @arg Vector force - The initial force
     * @arg Vector pos - The location of the force in world coordinates
     * @return {Vector} The calculated force on the physics object's center of mass
     * @return {Vector} The calculated torque on the physics object's center of mass
     */
    public CalculateForceOffset(force: Vector, pos: Vector): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Calculates the linear and angular velocities on the center of mass for an offset force impulse. The outputs can be directly passed to PhysObj:AddVelocity and PhysObj:AddAngleVelocity, respectively.
     *
     * @arg Vector force - The initial force
     * @arg Vector pos - The location of the force in world coordinates
     * @return {Vector} The calculated linear velocity from the force on the physics object's center of mass
     * @return {Vector} The calculated angular velocity from the force on the physics object's center of mass
     */
    public CalculateVelocityOffset(force: Vector, pos: Vector): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Removes one of more specified flags.
     *
     * @arg number flags - Bitflag, see Enums/FVPHYSICS.
     */
    public ClearGameFlag(flags: number): void;

    /**
     * Allows you to move a PhysObj to a point and angle in 3D space.
     *
     * @arg object shadowparams - The parameters for the shadow. See example code to see how its used.
     */
    public ComputeShadowControl(shadowparams: object): void;

    /**
     * Sets whether the physics object should collide with anything or not, including world.
     *
     * @arg boolean enable - True to enable, false to disable.
     */
    public EnableCollisions(enable: boolean): void;

    /**
     * Sets whenever the physics object should be affected by drag.
     *
     * @arg boolean enable - True to enable, false to disable.
     */
    public EnableDrag(enable: boolean): void;

    /**
     * Sets whether the PhysObject should be affected by gravity
     *
     * @arg boolean enable - True to enable, false to disable.
     */
    public EnableGravity(enable: boolean): void;

    /**
     * Sets whether the physobject should be able to move or not.
     *
     * @arg boolean enable - True to enable, false to disable.
     */
    public EnableMotion(enable: boolean): void;

    /**
     * Returns the mins and max of the physics object.
     *
     * @return {Vector} Mins
     * @return {Vector} Maxs
     */
    public GetAABB(): LuaMultiReturn<[Vector, Vector]>;

    /**
     * Returns the angles of the physics object.
     *
     * @return {Angle} The angles of the physics object.
     */
    public GetAngles(): Angle;

    /**
     * Gets the angular velocity of the object in degrees per second as a local vector. You can use dot product to read the magnitude from a specific axis.
     *
     * @return {Vector} The angular velocity
     */
    public GetAngleVelocity(): Vector;

    /**
     * Returns the contents flag of the PhysObj.
     *
     * @return {number} The Enums/CONTENTS.
     */
    public GetContents(): number;

    /**
     * Returns the linear and angular damping of the physics object.
     *
     * @return {number} The linear damping
     * @return {number} The angular damping
     */
    public GetDamping(): LuaMultiReturn<[number, number]>;

    /**
     * Returns the sum of the linear and rotational kinetic energies of the physics object.
     *
     * @return {number} The kinetic energy
     */
    public GetEnergy(): number;

    /**
     * Returns the parent entity of the physics object.
     *
     * @return {Entity} The entity this physics object belongs to
     */
    public GetEntity(): Entity;

    /**
 * Returns the friction snapshot of this physics object. This is useful for determining if an object touching ground for example.
* 
* @return {PhysObj} A table of tables containing the following data:

PhysObj Other - The other physics object we came in contact with
number EnergyAbsorbed -
number FrictionCoefficient -
number NormalForce -
Vector Normal - Direction of the friction event
Vector ContactPoint - Contact point of the friction event
number Material - Surface Property ID of our physics obj
number MaterialOther - Surface Property ID of the physics obj we came in contact with
 */
    public GetFrictionSnapshot(): PhysObj;

    /**
     * Returns the directional inertia of the physics object.
     *
     * @return {Vector} directionalInertia
     */
    public GetInertia(): Vector;

    /**
     * Returns 1 divided by the inertia.
     *
     * @return {number} The inverted inertia
     */
    public GetInvInertia(): number;

    /**
     * Returns 1 divided by the physics object's mass.
     *
     * @return {number} The inverted mass.
     */
    public GetInvMass(): number;

    /**
     * Returns the mass of the physics object.
     *
     * @return {number} The mass in kilograms.
     */
    public GetMass(): number;

    /**
     * Returns the center of mass of the physics object as a local vector.
     *
     * @return {Vector} The center of mass of the physics object.
     */
    public GetMassCenter(): Vector;

    /**
     * Returns the physical material of the physics object.
     *
     * @return {string} The physical material
     */
    public GetMaterial(): string;

    /**
     * Returns the physics mesh of the object which is used for physobj-on-physobj collision.
     *
     * @return {MeshVertexStruct} Table of Structures/MeshVertexs where each three vertices represent a triangle. Returns nil if the physics object is a sphere.
     */
    public GetMesh(): MeshVertexStruct;

    /**
     * Returns all convex physics meshes of the object. See Entity:PhysicsInitMultiConvex for more information.
     *
     * @return {MeshVertexStruct} Table of Structures/MeshVertexs where each Structures/MeshVertex is an independent convex mesh and each three vertices represent a triangle. Returns nil if the physics object is a sphere.
     */
    public GetMeshConvexes(): MeshVertexStruct;

    /**
     * Returns the name of the physics object.
     *
     * @return {string} The name of the physics object.
     */
    public GetName(): string;

    /**
     * Returns the position of the physics object.
     *
     * @return {Vector} The position
     */
    public GetPos(): Vector;

    /**
     * Returns the position and angle of the physics object as a 3x4 matrix (VMatrix is 4x4 so the fourth row goes unused). The first three columns store the angle as a rotation matrix, and the fourth column stores the position vector.
     *
     * @return {VMatrix} The position and angle matrix.
     */
    public GetPositionMatrix(): VMatrix;

    /**
     * Returns the rotation damping of the physics object.
     *
     * @return {number} The rotation damping
     */
    public GetRotDamping(): number;

    /**
     * Returns the angles of the PhysObj shadow. See PhysObj:UpdateShadow.
     *
     * @return {Angle} The angles of the PhysObj shadow.
     */
    public GetShadowAngles(): Angle;

    /**
     * Returns the position of the PhysObj shadow. See PhysObj:UpdateShadow.
     *
     * @return {Vector} The position of the PhysObj shadow.
     */
    public GetShadowPos(): Vector;

    /**
     * Returns the speed damping of the physics object.
     *
     * @return {number} speedDamping
     */
    public GetSpeedDamping(): number;

    /**
     * Returns the stress of the entity.
     *
     * @return {number} exertedStress
     */
    public GetStress(): number;

    /**
     * Returns the surface area of the physics object in source-units². Or nil if the PhysObj is a generated sphere or box.
     *
     * @return {number} The surface area or nil if the PhysObj is a generated sphere or box.
     */
    public GetSurfaceArea(): number;

    /**
     * Returns the absolute directional velocity of the physobject.
     *
     * @return {Vector} velocity
     */
    public GetVelocity(): Vector;

    /**
     * Returns the world velocity of a point in world coordinates about the object. This is useful for objects rotating around their own axis/origin.
     *
     * @arg Vector point - A point to test in world space coordinates
     * @return {Vector} Velocity at the given point
     */
    public GetVelocityAtPoint(point: Vector): Vector;

    /**
     * Returns the volume in source units³. Or nil if the PhysObj is a generated sphere or box.
     *
     * @return {number} The volume or nil if the PhysObj is a generated sphere or box.
     */
    public GetVolume(): number;

    /**
     * Returns whenever the specified flag(s) is/are set.
     *
     * @arg number flags - Bitflag, see Enums/FVPHYSICS.
     * @return {boolean} If flag was set or not
     */
    public HasGameFlag(flags: number): boolean;

    /**
     * Returns whether the physics object is "sleeping".
     *
     * @return {boolean} Whether the physics object is sleeping.
     */
    public IsAsleep(): boolean;

    /**
     * Returns whenever the entity is able to collide or not.
     *
     * @return {boolean} isCollisionEnabled
     */
    public IsCollisionEnabled(): boolean;

    /**
     * Returns whenever the entity is affected by drag.
     *
     * @return {boolean} dragEnabled
     */
    public IsDragEnabled(): boolean;

    /**
     * Returns whenever the entity is affected by gravity.
     *
     * @return {boolean} true if the gravity is enabled, false otherwise
     */
    public IsGravityEnabled(): boolean;

    /**
     * Returns if the physics object can move itself (by velocity, acceleration)
     *
     * @return {boolean} true if the motion is enabled, false otherwise.
     */
    public IsMotionEnabled(): boolean;

    /**
     * Returns whenever the entity is able to move.
     *
     * @return {boolean} movable
     */
    public IsMoveable(): boolean;

    /**
     * Returns whenever the physics object is penetrating another physics object.
     *
     * @return {boolean} Whether the physics object is penetrating another object.
     */
    public IsPenetrating(): boolean;

    /**
     * Returns if the physics object is valid/not NULL.
     *
     * @return {boolean} isValid
     */
    public IsValid(): boolean;

    /**
     * Mapping a vector in local frame of the physics object to world frame.
     *
     * @arg Vector LocalVec - A vector in the physics object's local frame
     * @return {Vector} The corresponding vector in world frame
     */
    public LocalToWorld(LocalVec: Vector): Vector;

    /**
     * Rotate a vector from the local frame of the physics object to world frame.
     *
     * @arg Vector LocalVec - A vector in the physics object's local frame
     * @return {Vector} The corresponding vector in world frame
     */
    public LocalToWorldVector(LocalVec: Vector): Vector;

    /**
     * Prints debug info about the state of the physics object to the console.
     */
    public OutputDebugInfo(): void;

    /**
     * Call this when the collision filter conditions change due to this object's state (e.g. changing solid type or collision group)
     */
    public RecheckCollisionFilter(): void;

    /**
     * A convinience function for Angle:RotateAroundAxis.
     *
     * @arg Vector dir - Direction, around which we will rotate
     * @arg number ang - Amount of rotation, in degrees
     * @return {Angle} The resulting angle
     */
    public RotateAroundAxis(dir: Vector, ang: number): Angle;

    /**
     * Sets the amount of drag to apply to a physics object when attempting to rotate.
     *
     * @arg number coefficient - Drag coefficient. The bigger this value is, the slower the angles will change.
     */
    public SetAngleDragCoefficient(coefficient: number): void;

    /**
     * Sets the angles of the physobject.
     *
     * @arg Angle angles - The new angles of the physobject.
     */
    public SetAngles(angles: Angle): void;

    /**
     * Sets the specified angular velocity on the this PhysObj
     *
     * @arg Vector angularVelocity - The new velocity to set velocity.
     */
    public SetAngleVelocity(angularVelocity: Vector): void;

    /**
     * Sets the specified instantaneous angular velocity on the this PhysObj
     *
     * @arg Vector angularVelocity - The new velocity to set velocity.
     */
    public SetAngleVelocityInstantaneous(angularVelocity: Vector): void;

    /**
     * Sets the buoyancy ratio of the physics object. (How well it floats in water)
     *
     * @arg number buoyancy - Buoyancy ratio, where 0 is not buoyant at all (like a rock), and 1 is very buoyant (like wood). You can set values larger than 1 for greater effect.
     */
    public SetBuoyancyRatio(buoyancy: number): void;

    /**
     * Sets the contents flag of the PhysObj.
     *
     * @arg number contents - The Enums/CONTENTS.
     */
    public SetContents(contents: number): void;

    /**
     * Sets the linear and angular damping of the physics object.
     *
     * @arg number linearDamping - Linear damping.
     * @arg number angularDamping - Angular damping.
     */
    public SetDamping(linearDamping: number, angularDamping: number): void;

    /**
 * Modifies how much drag (air resistance) affects the object.
* 
* @arg number drag - The drag coefficient
It can be positive or negative.
 */
    public SetDragCoefficient(drag: number): void;

    /**
 * Sets the directional inertia.
* 
* @arg Vector directionalInertia - The directional inertia of the object.
A value of Vector(0,0,0) makes the physobject go invalid.
 */
    public SetInertia(directionalInertia: Vector): void;

    /**
     * Sets the mass of the physics object.
     *
     * @arg number mass - The mass in kilograms.
     */
    public SetMass(mass: number): void;

    /**
     * Sets the material of the physobject.
     *
     * @arg string materialName - The name of the phys material to use. From this list: Valve Developer
     */
    public SetMaterial(materialName: string): void;

    /**
     * Sets the position of the physobject.
     *
     * @arg Vector position - The new position of the physobject.
     * @arg boolean [teleport=false]
     */
    public SetPos(position: Vector, teleport?: boolean): void;

    /**
     * Sets the velocity of the physics object for the next iteration.
     *
     * @arg Vector velocity - The new velocity of the physics object.
     */
    public SetVelocity(velocity: Vector): void;

    /**
     * Sets the velocity of the physics object.
     *
     * @arg Vector velocity - The new velocity of the physics object.
     */
    public SetVelocityInstantaneous(velocity: Vector): void;

    /**
     * Makes the physics object "sleep".
     */
    public Sleep(): void;

    /**
 * Unlike PhysObj:SetPos and PhysObj:SetAngles, this allows the movement of a physobj while leaving physics interactions intact.
* This is used internally by the motion controller of the Gravity Gun , the +use pickup and the Physics Gun, and entities such as the crane.
* 
* @arg Vector targetPosition - The position we should move to.
* @arg Angle targetAngles - The angle we should rotate towards.
* @arg number frameTime - The frame time to use for this movement, can be generally filled with FrameTime or ENTITY:PhysicsSimulate with the deltaTime.
Can be set to 0 when you need to update the physics object just once.
 */
    public UpdateShadow(targetPosition: Vector, targetAngles: Angle, frameTime: number): void;

    /**
     * Wakes the physics object.
     */
    public Wake(): void;

    /**
     * Converts a vector to a relative to the physics object coordinate system.
     *
     * @arg Vector vec - The vector in world space coordinates.
     * @return {Vector} The vector local to PhysObj:GetPos.
     */
    public WorldToLocal(vec: Vector): Vector;

    /**
     * Rotate a vector from the world frame to the local frame of the physics object.
     *
     * @arg Vector WorldVec - A vector in the world frame
     * @return {Vector} The corresponding vector relative to the PhysObj
     */
    public WorldToLocalVector(WorldVec: Vector): Vector;
}

declare type PhysObj = __PhysObjClass;
